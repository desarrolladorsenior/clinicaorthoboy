/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     14/3/2018 10:29:20                           */
/*==============================================================*/

/*==============================================================*/
/* Table: DEPARTAMENTO                                          */
/*==============================================================*/
create table DEPARTAMENTO (
   ID_DEPARTAMENTO    SERIAL                  not null,
   NOMBRE            VARCHAR(150)		not null,
   constraint PK_DEPARTAMENTO primary key (ID_DEPARTAMENTO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_DEPARTAMENTO                         */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_DEPARTAMENTO on DEPARTAMENTO (
NOMBRE
);


/*==============================================================*/
/* Table: MUNICIPIO                                          */
/*==============================================================*/
create table MUNICIPIO (
   ID      		INT8                 not null,
   NOMBRE            VARCHAR(150)	     not null,
   ID_DEPARTAMENTO    INT8                   not null,
   constraint PK_MUNICIPIO primary key (ID)
);


/*==============================================================*/
/* Table: CLINICA                                          */
/*==============================================================*/
create table CLINICA (
   ID_CLINICA        SERIAL                     not null,
   NIT               VARCHAR(100)		not null,
   NOMBRE            VARCHAR(150)		not null,
   EMAIL             VARCHAR(75)		not null,
   MOVIL	     INT8			not null,
   DIRECCION 	     VARCHAR(150)		    null,
   TELEFONO	     VARCHAR(20)		    null,
   DIRECCION_WEB     VARCHAR(150)		    null,
   LOGO		     VARCHAR(250)		    null,
   ID_SEDE_CLINICA   INT8			    null,
   ID_MUNICIPIO	     INT8			    null,
   ESTADO_SEDE	     BOOL			not null default true,
   constraint PK_CLINICA primary key (ID_CLINICA)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_CLINICA                         */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_CLINICA on CLINICA (
NOMBRE
); 

/*==============================================================*/
/* Table: CONSULTORIO                                          */
/*==============================================================*/
create table CONSULTORIO (
   ID_CONSULTORIO        SERIAL                     not null,
   NOMBRE            VARCHAR(150)		not null,
   ID_CLINICA	     INT8			not null,
   ESTADO	     BOOL			not null default true,
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_CONSULTORIO primary key (ID_CONSULTORIO)
);

/*==============================================================*/
/* Table: EPS                                            */
/*==============================================================*/
create table EPS (
   ID_EPS                   SERIAL                 not null,
   NIT  		    VARCHAR(100)	   not null,
   NOMBRE                   VARCHAR(150)           not null,
   DESCRIPCION		    TEXT         	       null,
   ESTADO		    BOOL		   not null default true,
   TELEFONO		    VARCHAR(20)		       null,
   MOVIL		    INT8		   not null,
   NOMBRE_CONTACTO          VARCHAR(150)           not null,
   DIRECCION		    VARCHAR(150)	   not null,
   EMAIL		    VARCHAR(75)		   not null,
   ID_MUNICIPIO		    INT8		   not null,
   ID_SEDE_EPS		    INT8		       null,
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_EPS primary key (ID_EPS)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_EPS                         */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_EPS on EPS (
NOMBRE
); 

/*==============================================================*/
/* Table: CONVENIO                                            */
/*==============================================================*/
create table CONVENIO (
   ID_CONVENIO          SERIAL               not null,
   NOMBRE               VARCHAR(150)         not null,
   PORCENTAJE_REMISION  INT8                     null,
   ID_CLINICA           INT8         	     not null,
   FECHA_APERTURA	DATE		     not null,
   ID_EPS		INT8		     not null,
   PORCENTAJE_DESCUENTO INT8		     	 null,
   OBSERVACION		TEXT		         null,
   ESTADO		BOOL		     not null default true,
   ID_USUARIO_INGRESA   INT8           	     not null,
   FECHA_FINALIZACION   DATE		     not null,
   constraint PK_CONVENIO primary key (ID_CONVENIO)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_CONVENIO                        */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_CONVENIO on CONVENIO (
NOMBRE
); 


/*==============================================================*/
/* Table: TIPO_DOCUMENTO                                          */
/*==============================================================*/
create table TIPO_DOCUMENTO (
   ID		     SERIAL                  not null,
   NOMBRE            VARCHAR(50)		not null,
   NOMENCLATURA      VARCHAR(20) 		not null,
   constraint PK_TIPO_DOCUMENTO primary key (ID)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_TIPO_DOCUMENTO                   */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_TIPO_DOCUMENTO on TIPO_DOCUMENTO (
NOMBRE
); 

/*==============================================================*/
/* Index: INDEX_UNQ_NOMENCLATURA_TIPO_DOCUMENTO                   */
/*==============================================================*/
create unique index INDEX_UNQ_NOMENCLATURA_TIPO_DOCUMENTO on TIPO_DOCUMENTO (
NOMENCLATURA
); 


/*==============================================================*/
/* Table: PACIENTE                                            */
/*==============================================================*/
create table PACIENTE (
   ID_PACIENTE                   SERIAL        	     	     not null,
   TIPO_IDENTIFICACION           INT8  		     	     not null,
   NUMERO_IDENTIFICACION	 VARCHAR(50)  		     not null,
   EXPEDICION_DOCUMENTO		 VARCHAR(150)  		     not null,
   NOMBRES      		 VARCHAR(75)  		     not null,
   PRIMER_APELLIDO		 VARCHAR(75)  		     not null,
   SEGUNDO_APELLIDO		 VARCHAR(75)  		     not null,
   FECHA_NACIMIENTO 		 TIMESTAMP WITHOUT TIME ZONE not null,
   LUGAR_NACIMIENTO		 VARCHAR(150)  		     not null,
   ESTADO_CIVIL		 	 VARCHAR(25)  		     not null,
   SEXO		 		 VARCHAR(20)  		     not null,
   EMAIL		 	 VARCHAR(75)                     null,
   DIRRECION_DOMICILIO		 VARCHAR(75)                 not null,
   TELEFONO_DOMICILIO		 VARCHAR(20)                     null,
   ID_MUNICIPIO		 	 INT8      		     not null,
   CELULAR			 VARCHAR(150)		     not null,
   NOMBRE_P_RESPONSABLE		 VARCHAR(150) 		     not null,
   TELEFONO_PERSONA_RESPONSABLE  INT8      		     not null,
   PARENTESCO		 	 VARCHAR(50)   		     not null,
   OCUPACION		 	 VARCHAR(150)		     not null,
   ID_EPS			 INT8			         null,
   REGIMEN			 INT8		                 null,
   NOMBRE_ACOMPANANTE		 VARCHAR(150)		     not null,
   MOVIL_ACOMPANANTE		 INT8		     	     not null,
   FECHA_APERTURA		 TIMESTAMP WITHOUT TIME ZONE not null, 	 
   ESTADO                        BOOL	         not null default true,	
   ID_USUARIO_INGRESA            INT8          	             not null,
   constraint PK_PACIENTE primary key (ID_PACIENTE)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NUMERO_IDENTIFICACION_PACIENTE                         */
/*==============================================================*/
create unique index INDEX_UNQ_NUMERO_IDENTIFICACION_PACIENTE on PACIENTE (
NUMERO_IDENTIFICACION
);


/*==============================================================*/
/* Table: AREA                             */
/*==============================================================*/
create table AREA (
   ID_AREA              SERIAL                  not null,
   NOMBRE       	VARCHAR(150)             not null,
   ESTADO 		BOOL                    not null default true,
   constraint PK_AREA primary key (ID_AREA)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_AREA                        */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_AREA on AREA (
NOMBRE
); 

/*==============================================================*/
/* Table: ANTECEDENTE                             */
/*==============================================================*/
create table ANTECEDENTE (
   ID_ANTECEDENTE       SERIAL     	         not null,
   NOMBRE       	VARCHAR(150)             not null,
   DESCRIPCION       	TEXT                         null,
   ESTADO 		BOOL                    not null default true,
   constraint PK_ANTECEDENTE primary key (ID_ANTECEDENTE)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_ANTECEDENTE                       */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_ANTECEDENTE on ANTECEDENTE (
NOMBRE
); 

/*==============================================================*/
/* Table: MEDICAMENTO                             */
/*==============================================================*/
create table MEDICAMENTO (
   ID_MEDICAMENTO       SERIAL     	         not null,
   NOMBRE       	VARCHAR(150)             not null,
   DESCRIPCION       	TEXT                         null,
   ESTADO 		BOOL                    not null default true,
   constraint PK_MEDICAMENTO primary key (ID_MEDICAMENTO)
);



/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_MEDICAMENTO                       */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_MEDICAMENTO on MEDICAMENTO (
NOMBRE
); 
/*==============================================================*/
/* Table: HABITO                             */
/*==============================================================*/
create table HABITO (
   ID_HABITO       SERIAL     	         not null,
   NOMBRE       	VARCHAR(150)             not null,
   DESCRIPCION       	TEXT                         null,
   ESTADO 		BOOL                    not null default true,
   constraint PK_HABITO primary key (ID_HABITO)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_HABITO                        */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_HABITO on HABITO (
NOMBRE
); 

/*==============================================================*/
/* Table: RIESGO                             */
/*==============================================================*/
create table RIESGO (
   ID_RIESGO       SERIAL     	         not null,
   NOMBRE       	VARCHAR(150)             not null,
   DESCRIPCION       	TEXT                         null,
   ESTADO 		BOOL                    not null default true,
   constraint PK_RIESGO primary key (ID_RIESGO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_RIESGO                        */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_RIESGO on RIESGO (
NOMBRE
); 

/*==============================================================*/
/* Table: ALERGIA                             */
/*==============================================================*/
create table ALERGIA (
   ID_ALERGIA           SERIAL     	         not null,
   NOMBRE       	VARCHAR(150)             not null,
   DESCRIPCION       	TEXT                         null,
   ESTADO 		BOOL                    not null default true,
   constraint PK_ALERGIA primary key (ID_ALERGIA)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_ALERGIA                        */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_ALERGIA on ALERGIA (
NOMBRE
); 
/*==============================================================*/
/* Table: PROPIEDAD                             */
/*==============================================================*/
create table PROPIEDAD (
   ID_PROPIEDAD           SERIAL     	         not null,
   NOMBRE       	VARCHAR(150)             not null,
   ESTADO 		BOOL                     not null default true,
   constraint PK_PROPIEDAD primary key (ID_PROPIEDAD)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_PROPIEDAD                        */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_PROPIEDAD on PROPIEDAD (
NOMBRE
); 

/*==============================================================*/
/* Table: PROVOCADO                            */
/*==============================================================*/
create table PROVOCADO (
   ID_PROVOCADO           SERIAL     	         not null,
   NOMBRE       	VARCHAR(150)             not null,
   ESTADO 		BOOL                     not null default true,
   constraint PK_PROVOCADO primary key (ID_PROVOCADO)
);



/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_PROVOCADO                       */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_PROVOCADO on PROVOCADO (
NOMBRE
); 
/*==============================================================*/
/* Table: EVOLUCION_ENDODONCIA                            */
/*==============================================================*/
create table EVOLUCION_ENDODONCIA (
   ID_EVOLUCION_ENDODONCIA           SERIAL     	         not null,
   NOMBRE       	VARCHAR(150)             not null,
   ESTADO 		BOOL                     not null default true,
   constraint PK_EVOLUCION_EDO primary key (ID_EVOLUCION_ENDODONCIA)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_EVOLUCION_ENDODONCIA                       */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_EVOLUCION_ENDODONCIA on EVOLUCION_ENDODONCIA (
NOMBRE
); 

/*==============================================================*/
/* Table: SUPERFICIE                            */
/*==============================================================*/
create table SUPERFICIE (
   ID_SUPERFICIE           SERIAL     	         not null,
   NOMBRE       	VARCHAR(150)             not null,
   CONVENCION	        CHAR (10)                not null,
   ESTADO 		BOOL                     not null default true,
   constraint PK_SUPERFICIE primary key (ID_SUPERFICIE)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_SUPERFICIE                        */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_SUPERFICIE on SUPERFICIE (
NOMBRE
); 


/*==============================================================*/
/* Index: INDEX_UNQ_CONVENCION_SUPERFICIE                       */
/*==============================================================*/
create unique index INDEX_UNQ_CONVENCION_SUPERFICIE on SUPERFICIE (
CONVENCION
); 

/*==============================================================*/
/* Table: PIEZA_DENTAL                            */
/*==============================================================*/
create table PIEZA_DENTAL (
   ID_PIEZA_DENTAL          SERIAL     	         not null,
   CODIGO_PIEZA       	VARCHAR(250)             not null,
   constraint PK_PIEZA_DENTAL primary key (ID_PIEZA_DENTAL)
);


/*==============================================================*/
/* Index: INDEX_UNQ_CODIGO_PIEZA_PIEZA_DENTAL                        */
/*==============================================================*/
create unique index INDEX_UNQ_CODIGO_PIEZA_PIEZA_DENTAL on PIEZA_DENTAL (
CODIGO_PIEZA
); 

/*==============================================================*/
/* Table: CIE_RIP                            */
/*==============================================================*/
create table CIE_RIP (
   ID_CIE_RIP          SERIAL 				not null,
   CODIGO       	VARCHAR(75) 		        not null,		
   NOMBRE_CIE_RIP 	VARCHAR(250)           		not null,
   DESRIPCION		TEXT		       		    null,
   SEXO 		CHAR(10)                   	not null,
   LIMITE_SUPERIOR	INT8				not null,
   LIMITE_INFERIOR      INT8				not null,
   constraint PK_CIE_RIP primary key (ID_CIE_RIP)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_NOMBRE_CIE_RIP_CIE_RIP                        */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_CIE_RIP_CIE_RIP on CIE_RIP (
NOMBRE_CIE_RIP
); 

/*==============================================================*/
/* Index: INDEX_UNQ_CODIGO_CIE_RIP                        */
/*==============================================================*/
create unique index INDEX_UNQ_CODIGO_CIE_RIP on CIE_RIP (
CODIGO
); 

/*==============================================================*/
/* Table: CUPS                            */
/*==============================================================*/
create table CUPS (
   ID_CUPS              INT8 				not null,		
   NOMBRE 	        VARCHAR(250)           		not null,
   constraint PK_CUPS primary key (ID_CUPS)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_CUPS                       */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_CUPS on CUPS (
NOMBRE
); 

/*==============================================================*/
/* Index: INDEX_UNQ_ID_CUPS_CUPS                       */
/*==============================================================*/
create unique index INDEX_UNQ_ID_CUPS_CUPS on CUPS (
ID_CUPS
); 

/*==============================================================*/
/* Table: AMBITO_REALIZACION                            */
/*==============================================================*/
create table AMBITO_REALIZACION (
   ID_AMBITO_REALIZACION SERIAL 			not null,		
   NOMBRE 	        VARCHAR(150)           		not null,
   constraint PK_AMBITO_REALIZACION primary key (ID_AMBITO_REALIZACION)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_AMBITO_REALIZACION                       */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_AMBITO_REALIZACION on AMBITO_REALIZACION (
NOMBRE
); 

/*==============================================================*/
/* Table: FINALIDAD_PROCEDIMIENTO                            */
/*==============================================================*/
create table FINALIDAD_PROCEDIMIENTO (
   ID_FINALIDAD_PROCEDIMIENTO     SERIAL 		not null,		
   NOMBRE 	        VARCHAR(150)           		not null,
   constraint PK_FINALIDAD_PROCEDIMIENTO primary key (ID_FINALIDAD_PROCEDIMIENTO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_FINALIDAD_PROCEDIMIENTO                       */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_FINALIDAD_PROCEDIMIENTO on FINALIDAD_PROCEDIMIENTO (
NOMBRE
); 

/*==============================================================*/
/* Table: PERSONA_ATIENDE                            */
/*==============================================================*/
create table PERSONA_ATIENDE (
   ID_PERSONA_ATIENDE     SERIAL     		        not null,		
   NOMBRE 	        VARCHAR(150)           		not null,
   constraint PK_PERSONA_ATIENDE primary key (ID_PERSONA_ATIENDE)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_PERSONA_ATIENDE                     */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_PERSONA_ATIENDE on PERSONA_ATIENDE (
NOMBRE
); 

/*==============================================================*/
/* Table: FORMA_REALIZACION                            */
/*==============================================================*/
create table FORMA_REALIZACION (
   ID_FORMA_REALIZACION     SERIAL     		        not null,		
   NOMBRE 	        VARCHAR(150)           		not null,
   constraint PK_FORMA_REALIZACION primary key (ID_FORMA_REALIZACION)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_FORMA_REALIZACION                     */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_FORMA_REALIZACION on FORMA_REALIZACION (
NOMBRE
); 

/*==============================================================*/
/* Table: FINALIDAD_CONSULTA*/
/*==============================================================*/
create table FINALIDAD_CONSULTA (
   ID_FINALIDAD_CONSULTA    SERIAL     		        not null,		
   NOMBRE 	        VARCHAR(150)           		not null,
   constraint PK_FINALIDAD_CONSULTA primary key (ID_FINALIDAD_CONSULTA)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_FINALIDAD_CONSULTA                     */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_FINALIDAD_CONSULTA on FINALIDAD_CONSULTA (
NOMBRE
); 

/*==============================================================*/
/* Table: TIPO_USUARIO*/
/*==============================================================*/
create table TIPO_USUARIO (
   ID_TIPO_USUARIO    SERIAL     		        not null,		
   NOMBRE 	        VARCHAR(150)           		not null,
   constraint PK_TIPO_USUARIO primary key (ID_TIPO_USUARIO)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_TIPO_USUARIO                     */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_TIPO_USUARIO on TIPO_USUARIO (
NOMBRE
); 

/*==============================================================*/
/* Table: CAUSA_EXTERNA                            */
/*==============================================================*/
create table CAUSA_EXTERNA (
   ID_CAUSA_EXTERNA     SERIAL 				not null,		
   NOMBRE 	        VARCHAR(150)           		not null,
   constraint PK_CAUSA_EXTERNA primary key (ID_CAUSA_EXTERNA)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_CAUSA_EXTERNA                     */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_CAUSA_EXTERNA on CAUSA_EXTERNA (
NOMBRE
); 


/*==============================================================*/
/* Table: CUP_DIAGNOSTICO                            */
/*==============================================================*/
create table CUP_DIAGNOSTICO (
   ID_CUP_DIAGNOSTICO     SERIAL			not null,		
   ID_CUPS 	          INT8           		not null,
   ID_RIPS 	          INT8           		    null,
   ID_DXR1 	          INT8           		    null,
   ID_DXR2 	          INT8           		    null,
   ID_DXR3 	          INT8           		    null,
   ID_CAUSA_EXTERNA       INT8           		    null,
   ID_AMBITO_REALIZACION  INT8           		    null,
   ID_FORMA_REALIZACION   INT8           		    null,
   ID_PERSONA_ATIENDE     INT8           		    null,
   ID_FINALIDAD_PROCEDIMIENTO   INT8           		    null,
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_CUP_DIAGNOSTICO primary key (ID_CUP_DIAGNOSTICO)
);


/*==============================================================*/
/* Table: DIAGNOSTICO_ODONTOLOGICO                            */
/*==============================================================*/
create table DIAGNOSTICO_ODONTOLOGICO (
   ID_DIAGNOSTICO_ODONTOLOGICO          SERIAL not null,
   CODIGO       	  INT8 		       not null,		
   NOMBRE 	        VARCHAR(150)           not null,
   DESCRIPCION		TEXT		       not null,
   ESTADO 		BOOL                   not null default true,
   constraint PK_DIAGNOSTICO_ODONTOLOGICO primary key (ID_DIAGNOSTICO_ODONTOLOGICO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_DIAGNOSTICO_ODONTOLOGICO                     */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_DIAGNOSTICO_ODONTOLOGICO on DIAGNOSTICO_ODONTOLOGICO (
NOMBRE
); 

/*==============================================================*/
/* Index: INDEX_UNQ_CODIGO_DIAGNOSTICO_ODONTOLOGICO                     */
/*==============================================================*/
create unique index INDEX_UNQ_CODIGO_DIAGNOSTICO_ODONTOLOGICO on DIAGNOSTICO_ODONTOLOGICO (
CODIGO
); 

/*==============================================================*/
/* Table: DIAGNOSTICO_PRINCIPAL                           */
/*==============================================================*/
create table DIAGNOSTICO_PRINCIPAL (
   ID_DIAGNOSTICO_PRINCIPAL  SERIAL	      not null,	
   NOMBRE 	        VARCHAR(75)           not null,
   constraint PK_DIAGNOSTICO_PRINCIPAL primary key (ID_DIAGNOSTICO_PRINCIPAL)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_DIAGNOSTICO_PRINCIPAL                     */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_DIAGNOSTICO_PRINCIPAL on DIAGNOSTICO_PRINCIPAL (
NOMBRE
); 

/*==============================================================*/
/* Table: TIPO_DIAGNOSTICO                          */
/*==============================================================*/
create table TIPO_DIAGNOSTICO (
   ID_TIPO_DIAGNOSTICO  SERIAL	      not null,	
   NOMBRE 	        VARCHAR(150)  not null,
   ID_AREA              INT8	      not null,	
   constraint PK_TIPO_DIAGNOSTICO primary key (ID_TIPO_DIAGNOSTICO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_TIPO_DIAGNOSTICO                    */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_TIPO_DIAGNOSTICO on TIPO_DIAGNOSTICO (
NOMBRE
); 

/*==============================================================*/
/* Table: CIE_DIAGNOSTICO_TIPO                          */
/*==============================================================*/
create table CIE_DIAGNOSTICO_TIPO (
   ID_CIE_DIAGNOSTICO_TIPO  SERIAL	              not null,	
   ID_CIE_RIP           	INT8	      		  null,	
   ID_DIAGNOSTICO_ODONTOLOGICO  INT8	      		  null,	
   ID_TIPO_DIAGNOSTICO         	INT8	      	      not null,
   SIMBOLO		VARCHAR(250)			  null,
   COLOR		VARCHAR(50)			  null,
   ESTADO 		BOOL                   not null default true,
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_CIE_DIAGNOSTICO_TIPO primary key (ID_CIE_DIAGNOSTICO_TIPO)
);


/*==============================================================*/
/* Table: CATEGORIA_TRATAMIENTO                                          */
/*==============================================================*/
create table CATEGORIA_TRATAMIENTO (
   ID_CATEGORIA_TRATAMIENTO    SERIAL                  not null,
   ID_AREA                     INT8                    not null,
   NOMBRE            	VARCHAR(250)         	       not null,
   ESTADO 		BOOL                    not null default true,
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_CATEGORIA_TRATAMIENTO primary key (ID_CATEGORIA_TRATAMIENTO)
);



/*==============================================================*/
/* Table: TRATAMIENTO                                          */
/*==============================================================*/
create table TRATAMIENTO (
   ID_TRATAMIENTO      SERIAL                  not null,
   NOMBRE            VARCHAR(250)		not null,
   ID_CATEGORIA_TRATAMIENTO              INT8                  not null,
   VALOR                INT8                  not null,
   ESTADO 		BOOL                    not null default true,
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_TRATAMIENTO primary key (ID_TRATAMIENTO)
);



/*==============================================================*/
/* Table: TRATAMIENTO_CONVENIO                                          */
/*==============================================================*/
create table TRATAMIENTO_CONVENIO (
   ID_TRATAMIENTO_CONVENIO     SERIAL           not null,
   PORCENTAJE_TRATAMIENTO      INT8    		not null,
   PORCENTAJE_REMISION 	       INT8             not null,
   ID_TRATAMIENTO              INT8             not null,   
   ID_CONVENIO		       INT8		not null,
   ESTADO 		       BOOL             not null default true,   
   ID_USUARIO_INGRESA          INT8          	not null,
   FECHA_APERTURA              DATE		not null,
   constraint PK_TRATAMIENTO_CONVENIO primary key (ID_TRATAMIENTO_CONVENIO)
);

/*==============================================================*/
/* Table: AGENDA_USUARIO                                          */
/*==============================================================*/
create table AGENDA_USUARIO (
   ID_AGENDA_USUARIO    SERIAL                  not null,
   FECHA_APERTURA       TIMESTAMP WITHOUT TIME ZONE not null,
   HORA_INICIO          TIME 			not null,
   HORA_FIN             TIME			not null,
   DIA            	VARCHAR(20)         	not null,
   ESTADO 		BOOL                    not null default true,
   ID_USUARIO_INGRESA     INT8          	    null,
   HORA_ALMUERZO	 VARCHAR(50)		    null,
   constraint PK_AGENDA_USUARIO primary key (ID_AGENDA_USUARIO)
);



/*==============================================================*/
/* Table: ACUERDO_PAGO                                          */
/*==============================================================*/
create table ACUERDO_PAGO (
   ID_ACUERDO_PAGO      SERIAL                  not null,
   TIPO                 VARCHAR(75)		not null,
   PORCENTAJE		INT8                        null,
   VALOR		INT8                        null,
   ID_TRATAMIENTO       INT8                        null,
   ESTADO 		BOOL                    not null default true,
   constraint PK_ACUERDO_PAGO primary key (ID_ACUERDO_PAGO)
);

/*==============================================================*/
/* Table: PERMISO                                          */
/*==============================================================*/
create table PERMISO (
   ID_PERMISO           SERIAL                  not null,
   NOMBRE               VARCHAR(150)		not null,
   DESCRIPCION		TEXT			    null,
   MODULO 		VARCHAR(20)		not null,
   constraint PK_PERMISO primary key (ID_PERMISO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_PERMISO                            */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_PERMISO on PERMISO (
NOMBRE
);


/*==============================================================*/
/* Table: PERFIL*/
/*==============================================================*/
create table PERFIL (
   ID_PERFIL           SERIAL                  not null,
   NOMBRE               VARCHAR(150)		not null,
   ESTADO 		BOOL                    not null default true,
   ID_USUARIO_INGRESA     INT8          	    null,
   constraint PK_PERFIL primary key (ID_PERFIL)
);
/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_PERFIL                            */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_PERFIL on PERFIL (
NOMBRE
);


/*==============================================================*/
/* Table: PERFIL_PERMISO*/
/*==============================================================*/
create table PERFIL_PERMISO (
   ID_PERFIL_PERMISO    SERIAL                not null,
   ID_PERMISO           INT8                  not null,
   ID_PERFIL            INT8                  not null,
   constraint PK_PERFIL_PERMISO primary key (ID_PERFIL_PERMISO)
);


/*==============================================================*/
/* Table: ESPECIALIDAD*/
/*==============================================================*/
create table ESPECIALIDAD (
   ID_ESPECIALIDAD           SERIAL                  not null,
   NOMBRE               VARCHAR(300)		not null,
   constraint PK_ESPECIALIDAD primary key (ID_ESPECIALIDAD)
);
/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_ESPECIALIDAD                           */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_ESPECIALIDAD on ESPECIALIDAD (
NOMBRE
);


/*==============================================================*/
/* Table: USUARIO                                            */
/*==============================================================*/
create table USUARIO (
   ID_USUARIO                    SERIAL       	     	     not null,
   TIPO_IDENTIFICACION           INT8  		     	     not null,
   NOMBRES      		 VARCHAR(150)  		     not null,
   APELLIDOS		         VARCHAR(150)  		     not null,
   NUMERO_IDENTIFICACION	 VARCHAR(50)  		     not null,
   FECHA_NACIMIENTO 		 TIMESTAMP WITHOUT TIME ZONE not null,
   EMAIL  			 VARCHAR(75)      	     not null,
   USERNAME		 	 VARCHAR(150)  		     not null,
   PASSWORD             	 VARCHAR(250)                not null,
   GENERO		 	 VARCHAR(20)                 not null,
   ID_ESPECIALIDAD		 INT8                            null,
   ID_ACUERDO_PORC		 INT8                            null,
   ID_ACUERDO_VALOR		 INT8                            null,
   ID_AGENDA_USUARIO	         INT8                        not null,
   ID_PERFIL		 	 INT8 		     	     not null,
   ID_SUCURSAL  		 INT8     		     not null,
   COLOR_AGENDA		         VARCHAR(150)                not null,
   DIRECCION		 	 VARCHAR(75)      	     not null,
   NUMERO_CONTACTO       	 INT8  		     		 null,
   NOMBRE_CONTACTO		 VARCHAR(150) 		         null,
   FECHA_APERTURA                TIMESTAMP WITHOUT TIME ZONE not null,
   MOVIL		 	 INT8			     not null,
   FIRMA_PROFESIONAL             VARCHAR(250)                    null,   
   ESTADO		         BOOL           not null default true,
   INTENTO       	 INT8  		     	     not null,   
   ID_USUARIO_INGRESA     INT8          	         null,
   TIPO_USUARIO		         VARCHAR(75) 		     not null,
   IMAGEN_USUARIO             VARCHAR(250)                    null,
   constraint PK_USUARIO primary key (ID_USUARIO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NUMERO_IDENTIFICACION_USUARIO                          */
/*==============================================================*/
create unique index INDEX_UNQ_NUMERO_IDENTIFICACION_USUARIO on USUARIO (
NUMERO_IDENTIFICACION
);

/*==============================================================*/
/* Index: INDEX_UNQ_EMAIL_USUARIO                          */
/*==============================================================*/
create unique index INDEX_UNQ_EMAIL_USUARIO on USUARIO (
EMAIL
);

/*==============================================================*/
/* Table: ESTADO_GENERAL                             */
/*==============================================================*/
create table ESTADO_GENERAL (
   ID_ESTADO_GENERAL    SERIAL                   not null,
   NOMBRE       	VARCHAR(150)             not null,
   DESCRIPCION          TEXT                         null,
   constraint PK_ESTADO_GENERAL primary key (ID_ESTADO_GENERAL)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_ESTADO_GENERAL                          */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_ESTADO_GENERAL on ESTADO_GENERAL (
NOMBRE
);

/*==============================================================*/
/* Table: ESTADO_CITA                        */
/*==============================================================*/
create table ESTADO_CITA (
   ID_ESTADO_CITA       SERIAL                   not null,
   NOMBRE       	VARCHAR(150)             not null,
   ICONO                VARCHAR(150)             not null,
   ENVIAR_CORREO 	BOOL                    not null default true,
   constraint PK_ESTADO_CITA primary key (ID_ESTADO_CITA)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_ESTADO_CITA                          */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_ESTADO_CITA on ESTADO_CITA (
NOMBRE
);


/*==============================================================*/
/* Table: ESTADO_MANTENIMIENTO                */
/*==============================================================*/
create table ESTADO_MANTENIMIENTO (
   ID_ESTADO_MANTENIMIENTO SERIAL                not null,
   NOMBRE       	VARCHAR(150)             not null,
   constraint PK_ESTADO_MANTENIMIENTO primary key (ID_ESTADO_MANTENIMIENTO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_ESTADO_MANTENIMIENTO                         */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_ESTADO_MANTENIMIENTO on ESTADO_MANTENIMIENTO (
NOMBRE
);
/*==============================================================*/
/* Table: ESTADO_PAGO              */
/*==============================================================*/
create table ESTADO_PAGO (
   ID_ESTADO_PAGO       SERIAL                not null,
   NOMBRE       	VARCHAR(150)             not null,
   constraint PK_ESTADO_PAGO primary key (ID_ESTADO_PAGO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_ESTADO_PAGO                         */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_ESTADO_PAGO on ESTADO_PAGO (
NOMBRE
);

/*==============================================================*/
/* Table: MOTIVO_CONSULTA                                            */
/*==============================================================*/
create table MOTIVO_CONSULTA (
   ID_MOTIVO_CONSULTA          SERIAL         not null,
   NOMBRE               VARCHAR(150)         not null,
   TIEMPO_ESTIMADO      INT8                 not null,
   ID_PROCEDIMIENTO     INT8         	     not null,  
   ESTADO		BOOL		     not null default true,
   constraint PK_MOTIVO_CONSULTA primary key (ID_MOTIVO_CONSULTA)
); 

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_MOTIVO_CONSULTA                    */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_MOTIVO_CONSULTA on MOTIVO_CONSULTA (
NOMBRE
);

/*==============================================================*/
/* Table: CITA                                            */
/*==============================================================*/
create table CITA (
   ID_CITA         SERIAL         not null,
   ID_PACIENTE          INT8         	     not null,  
   ID_CLINICA           INT8		     not null,
   ID_USUARIO_PROFESIONAL INT8		     not null,
   ID_ESTADO_CITA	INT8		     not null,
   ID_MOTIVO_CONSULTA    INT8		     not null,   
   ID_CONSULTORIO       INT8		     not null,   
   ID_USUARIO_GESTIONA    INT8		         null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE not null,
   FECHA_FIN           TIMESTAMP WITHOUT TIME ZONE not null,
   constraint PK_CITA primary key (ID_CITA)
); 


/*==============================================================*/
/* Table: CATEGORIA_INVENTARIO              */
/*==============================================================*/
create table CATEGORIA_INVENTARIO (
   ID_CATEGORIA_INVENTARIO SERIAL                not null,
   NOMBRE       	VARCHAR(150)             not null,
   ESTADO 	BOOL                    not null default true,
   constraint PK_CATEGORIA_INVENTARIO primary key (ID_CATEGORIA_INVENTARIO)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_CATEGORIA_INVENTARIO                     */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_CATEGORIA_INVENTARIO on CATEGORIA_INVENTARIO (
NOMBRE
);

/*==============================================================*/
/* Table: NOMBRE_INSUMO              */
/*==============================================================*/
create table NOMBRE_INSUMO (
   ID_NOMBRE_INSUMO SERIAL                not null,
   NOMBRE       	VARCHAR(150)             not null,
   ESTADO 	BOOL                    not null default true,
   constraint PK_NOMBRE_INSUMO primary key (ID_NOMBRE_INSUMO)
);



/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_ESTADO_NOMBRE_INSUMO                  */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_NOMBRE_INSUMO on NOMBRE_INSUMO (
NOMBRE
); 

/*==============================================================*/
/* Table: MARCA              */
/*==============================================================*/
create table MARCA (
   ID_MARCA 		SERIAL                   not null,
   NOMBRE       	VARCHAR(150)             not null,
   ESTADO 	BOOL                    not null default true,
   constraint PK_MARCA primary key (ID_MARCA)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_MARCA                 */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_MARCA on MARCA (
NOMBRE
); 


/*==============================================================*/
/* Table: PRESENTACION              */
/*==============================================================*/
create table PRESENTACION (
   ID_PRESENTACION	SERIAL                   not null,
   NOMBRE_PRESENTACION     VARCHAR(150)          not null,
   UNIDAD		   VARCHAR(150)			 not null,
   ESTADO 	BOOL                    not null default true,
   constraint PK_PRESENTACION primary key (ID_PRESENTACION)
);



/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_PRESENTACION                */
/*==============================================================*/
/*create unique index INDEX_UNQ_NOMBRE_PRESENTACION on PRESENTACION (
NOMBRE_PRESENTACION
); */

/*==============================================================*/
/* Table: INSUMO              */
/*==============================================================*/
create table INSUMO (
   ID_INSUMO 		   SERIAL                not null,
   ID_CATEGORIA    	   INT8			 not null,
   ID_MARCA		   INT8			 not null,
   ID_PRESENTACION    	   INT8			 not null,
   ID_NOMBRE_INSUMO	   INT8			 not null,
   DESCRIPCION             TEXT                      null,
   FUENTE		   VARCHAR(150)		     null,
   VOLTAJE		   INT8			     null,
   POTENCIA		   INT8			     null,
   CORRIENTE		   INT8			     null,
   ESTADO 	BOOL                    not null default true,
   constraint PK_INSUMO primary key (ID_INSUMO)
);

/*==============================================================*/
/* Table: PROVEEDOR              */
/*==============================================================*/
create table PROVEEDOR (
   ID_PROVEEDOR		   SERIAL                not null,
   ID_TIPO_DOCUMENTO   	   INT8			 not null,
   ID_MUNICIPIO		   INT8			 not null,
   IDENTIFICACION    	   VARCHAR(150) 	 not null,
   NOMBRE	   	   VARCHAR(150)		 not null,
   DIRECCION               VARCHAR(150)          not null,
   MOVIL		   INT8			 not null,
   TELEFONO		   VARCHAR(20)		     null,
   EMAIL		   VARCHAR(75)	         not null,
   NOMBRE_CONTACTO	   VARCHAR(150)		 not null,
   REPRESENTANTE_LEGAL	   VARCHAR(150)		 not null,
   ESTADO 	BOOL                    not null default true,
   constraint PK_PROVEEDOR primary key (ID_PROVEEDOR)
);


/*==============================================================*/
/* Index: INDEX_UNQ_IDENTIFICACION_PROVEEDOR               */
/*==============================================================*/
create unique index INDEX_UNQ_IDENTIFICACION_PROVEEDOR on PROVEEDOR (
IDENTIFICACION
); 

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_PROVEEDOR               */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_PROVEEDOR on PROVEEDOR (
NOMBRE
); 


/*==============================================================*/
/* Table: REQUERIMIENTO              */
/*==============================================================*/
create table REQUERIMIENTO (
   ID_REQUERIMIENTO	   SERIAL                not null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE not null,
   ID_SEDE_CLINICA	   INT8		   	 not null, 
   ID_USUARIO_INGRESA     INT8          	    not null,
   ESTADO	     VARCHAR(75)                 not null,
   constraint PK_REQUERIMIENTO primary key (ID_REQUERIMIENTO)
);

/*==============================================================*/
/* Table: ELEMENTO_REQUERIMIENTO             */
/*==============================================================*/
create table ELEMENTO_REQUERIMIENTO (
   ID_ELEMENTO_REQUERIMIENTO SERIAL              not null,
   ID_REQUERIMIENTO        INT8			 not null,
   ID_NOMBRE_INSUMO        INT8			 not null,
   ID_MARCA		   INT8		 	     null,
   ID_PRESENTACION         INT8			     null,
   CANTIDAD                INT8			 not null, 
   ESTADO		VARCHAR(150)             not null,
   constraint PK_ENTRADA_ELEMENTO_REQUERIMIENTO primary key (ID_ELEMENTO_REQUERIMIENTO)
);

/*==============================================================*/
/* Table: ORDEN_COMPRA              */
/*==============================================================*/
create table ORDEN_COMPRA (
   ID_ORDEN_COMPRA	   SERIAL                not null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE not null,
   ID_PROVEEDOR	   	  INT8  		    not null, 
   ID_USUARIO_INGRESA     INT8          	    not null,
   ESTADO_ENVIO_CORREO  	BOOL                not null,   
   constraint PK_ORDEN_COMPRA primary key (ID_ORDEN_COMPRA)
);

/*==============================================================*/
/* Table: ELEMENTO_ORDEN_COMPRA             */
/*==============================================================*/
create table ELEMENTO_ORDEN_COMPRA (
   ID_ELEMENTO_ORDEN_COMPRA 	SERIAL               not null,
   ID_ELEMENTO_REQUERIMIENTO    INT8   		     not null,
   ID_ORDEN_COMPRA 	        INT8               not null,
   constraint PK_ENTRADA_ELEMENTO_ORDEN_COMPRA primary key (ID_ELEMENTO_ORDEN_COMPRA)
); 

/*==============================================================*/
/* Table: PEDIDO              */
/*==============================================================*/
create table PEDIDO (
   ID_PEDIDO 		   SERIAL                not null,
   NUMERO_FACTURA    	   VARCHAR(75)		 not null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE not null,
   FECHA_FACTURA       TIMESTAMP WITHOUT TIME ZONE not null,
   ID_PROVEEDOR	           INT8			 not null,
   ESTADO 	BOOL                    not null default true,   
   ID_USUARIO_INGRESA     INT8          	    not null,
   ID_ORDEN_COMPRA	   INT8                	     null,
   IMAGEN           VARCHAR(250) 		     null,  
   constraint PK_PEDIDO primary key (ID_PEDIDO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NUMERO_FACTURA_PEDIDO            */
/*==============================================================*/
create unique index INDEX_UNQ_NUMERO_FACTURA_PEDIDO on PEDIDO (
NUMERO_FACTURA
); 

/*==============================================================*/
/* Table: ENTRADA_PEDIDO              */
/*==============================================================*/
create table ENTRADA_PEDIDO (
   ID_ENTRADA_PEDIDO       SERIAL                not null,
   ID_PEDIDO	           INT8			 not null,
   ID_INSUMO	           INT8			 not null,
   ID_CLINICA_UBICACION    INT8		 	 not null,
   CANTIDAD		   INT8			 not null,
   UNIDAD_MEDIDA         VARCHAR(150) 		 not null,  
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE not null,
   FECHA_VENCIMIENTO      TIMESTAMP WITHOUT TIME ZONE  null,
   REGISTRO_INVIMA	VARCHAR(150)                   null,
   LOTE 		VARCHAR(50)		       null,
   MODELO		VARCHAR(150)                   null,
   INTERVALO_MANTENIMIENTO VARCHAR(150)		       null,
   GARANTIA  		TIMESTAMP WITHOUT TIME ZONE    null,   
   ESTADO 	         BOOL            not null default true,   
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_ENTRADA_PEDIDO primary key (ID_ENTRADA_PEDIDO)
);

/*==============================================================*/
/* Table: MANTENIMIENTO              */
/*==============================================================*/
create table MANTENIMIENTO (
   ID_MANTENIMIENTO        SERIAL                not null,
   ID_ENTRADA_PEDIDO       INT8			 not null,
   ID_ESTADO_MANTENIMIENTO INT8			 not null,
   ID_USUARIO_APRUEBA      INT8		 	     null,
   TIPO_MANTENIMIENTO	 VARCHAR(150) 		 not null,
   RESPONSABLE          VARCHAR(150) 		     null,  
   FECHA_PROGRAMADA     TIMESTAMP WITHOUT TIME ZONE not null,
   FECHA_EJECUCION      TIMESTAMP WITHOUT TIME ZONE  null,
   DESCRIPCION	            TEXT                       null,
   OBSERVACION 		TEXT			       null,  
   constraint PK_MANTENIMIENTO primary key (ID_MANTENIMIENTO)
);

/*==============================================================*/
/* Table: SALIDA              */
/*==============================================================*/
create table SALIDA (
   ID_SALIDA             SERIAL                   not null,
   ID_ENTRADA_PEDIDO     INT8		          not null,
   ID_CONSULTORIO 	 INT8		              null,
   ID_PACIENTE      	 INT8		 	      null,
   CANTIDAD		 INT8		          not null,
   FECHA_APERTURA     TIMESTAMP WITHOUT TIME ZONE not null, 
   ID_USUARIO_INGRESA     INT8          	    not null, 
   constraint PK_SALIDA primary key (ID_SALIDA)
);


/*==============================================================*/
/* Table: SALIDA_CONSULTORIO              */
/*==============================================================*/
create table SALIDA_CONSULTORIO (
   ID_SALIDA_CONSULTORIO SERIAL                   not null,
   ID_SALIDA     	 INT8		          not null,
   ID_CONSULTORIO 	 INT8		              null,
   CANTIDAD		 INT8		          not null,
   FECHA_APERTURA     TIMESTAMP WITHOUT TIME ZONE not null,     
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_SALIDA_CONSULTORIO primary key (ID_SALIDA_CONSULTORIO)
);

/*==============================================================*/
/* Table: STOCK              */
/*==============================================================*/
create table STOCK (
   ID_STOCK              SERIAL                   not null,
   ID_NOMBRE_INSUMO      INT8		          not null,
   ID_CLINICA	 	 INT8		          not null,
   CANTIDAD		 INT8		          not null,
   ESTADO 	         BOOL            not null default true,
   constraint PK_STOCK primary key (ID_STOCK)
);

/*==============================================================*/
/* Table: TIPO_RADIOGRAFIA              */
/*==============================================================*/
create table TIPO_RADIOGRAFIA  (
   ID_TIPO_RADIOGRAFIA   SERIAL                   not null,
   NOMBRE  		VARCHAR(150)	          not null,
   DESCRIPCION	 	 TEXT		              null,
   ID_SEDE_TIPO_RADIOGRAFIA	INT8          	      null,
   ESTADO 	         BOOL            not null default true,
   constraint PK_TIPO_RADIOGRAFIA primary key (ID_TIPO_RADIOGRAFIA)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_TIPO_RADIOGRAFIA           */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_TIPO_RADIOGRAFIA on TIPO_RADIOGRAFIA (
NOMBRE
); 

/*==============================================================*/
/* Table: RADIOGRAFIA              */
/*==============================================================*/
create table RADIOGRAFIA (
   ID_RADIOGRAFIA       SERIAL                  not null,
   NOMBRE  		VARCHAR(150)	        not null,
   HALLAZGO	 	 TEXT		        not null,
   ID_PACIENTE   	 INT8          	      	not null,
   ID_TIPO_RADIOGRAFIA	 INT8          	      	not null,
   IMAGEN 	         VARCHAR(255)           not null,
   FECHA_APERTURA   TIMESTAMP WITHOUT TIME ZONE not null,
   ID_USUARIO   	 INT8          	      	    null,   
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_RADIOGRAFIA primary key (ID_RADIOGRAFIA)
);


/*==============================================================*/
/* Table: IMAGEN              */
/*==============================================================*/
create table IMAGEN (
   ID_IMAGEN   		SERIAL                    not null,
   NOMBRE  		VARCHAR(150)	          not null,
   OBSERVACION	 	 TEXT		          not null,
   ID_PACIENTE           INT8          	          not null,
   FECHA_APERTURA   TIMESTAMP WITHOUT TIME ZONE   not null,   
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_IMAGEN primary key (ID_IMAGEN)
);

/*==============================================================*/
/* Table: ANEXO_PACIENTE              */
/*==============================================================*/
create table ANEXO_PACIENTE (
   ID_ANEXO_PACIENTE       SERIAL               not null,
   NOMBRE  		VARCHAR(150)	        not null,
   DESCRIPCION	 	 TEXT		            null,
   ID_PACIENTE   	 INT8          	      	not null,
   FECHA_APERTURA       TIMESTAMP WITHOUT TIME ZONE not null,
   ID_USUARIO_INGRESA     INT8          	    not null,
   ID_REMISION            INT8          	        null,
   ARCHIVO  		VARCHAR(250)	            null,
   constraint PK_ANEXO_PACIENTE primary key (ID_ANEXO_PACIENTE)
);


/*==============================================================*/
/* Table: REMISION              */
/*==============================================================*/
create table REMISION (
   ID_REMISION   	 SERIAL                     not null,
   ID_PACIENTE  	 INT8	          	    not null,
   OBSERVACION	 	 TEXT		            not null,
   ID_USUARIO_REMITE     INT8          	            not null,
   ID_USUARIO_REMITIDO     INT8          	              null,
   FECHA_APERTURA       TIMESTAMP WITHOUT TIME ZONE not null,
   FECHA_REALIZAR       TIMESTAMP WITHOUT TIME ZONE not null,
   ID_PROCEDIMIENTO     INT8          	            not null,
   EMAIL  	      VARCHAR(75)	                null,
   constraint PK_REMISION primary key (ID_REMISION)
);

/*==============================================================*/
/* Table: FORMULA              */
/*==============================================================*/
create table FORMULA (
   ID_FORMULA       SERIAL                      not null,
   NOMBRE  	      VARCHAR(150)	        not null,
   ID_USUARIO            INT8          	        not null,
   OBSERVACION	 	 TEXT		        not null,
   FECHA_APERTURA   TIMESTAMP WITHOUT TIME ZONE not null,
   ID_PACIENTE   	 INT8          	      	not null,
   CANTIDAD	   	 INT8          	      	not null,
   ID_CIE		 INT8			not null,
   constraint PK_FORMULA primary key (ID_FORMULA)
);

/*==============================================================*/
/* Table: TIPO_CONSENTIMIENTO              */
/*==============================================================*/
create table TIPO_CONSENTIMIENTO (
   ID_TIPO_CONSENTIMIENTO  SERIAL                 not null,
   NOMBRE  		VARCHAR(75)	          not null,
   CONTENIDO	 	 TEXT		          not null,
   ID_CIE                INT8          	          not null,
   RIESGO                VARCHAR(300)             not null,
   constraint PK_TIPO_CONSENTIMIENTO primary key (ID_TIPO_CONSENTIMIENTO)
);

/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_TIPO_CONSENTIMIENTO           */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_TIPO_CONSENTIMIENTO on TIPO_CONSENTIMIENTO (
NOMBRE
); 

/*==============================================================*/
/* Table: CONSENTIMIENTO              */
/*==============================================================*/
create table CONSENTIMIENTO (
   ID_CONSENTIMIENTO       SERIAL               not null,
   ID_USUARIO            INT8          	        not null,
   ID_PACIENTE   	 INT8          	      	not null,
   ID_TIPO_CONSENTIMIENTO INT8                  not null,
   FECHA_APERTURA   TIMESTAMP WITHOUT TIME ZONE not null,
   FIRMA_PACIENTE  	VARCHAR(250)	            null,
   NOMBRE_ACUDIENTE  	VARCHAR(150)	            null,
   CEDULAR_ACUDIENTE    VARCHAR(50)	            null,   
   PARENTESCO  	        VARCHAR(75)	            null,
   CEDULAR_RESPONSABLE  VARCHAR(50)	            null, 
   HUELLA_PACIENTE  	VARCHAR(250)	            null,
   constraint PK_CONSENTIMIENTO primary key (ID_CONSENTIMIENTO)
);

/*==============================================================*/
/* Table: PLAN_TRATAMIENTO              */
/*==============================================================*/
create table PLAN_TRATAMIENTO (
   ID_PLAN_TRATAMIENTO   SERIAL               not null,
   ID_ESTADO_GENERAL     INT8          	        not null,
   ID_PACIENTE   	 INT8          	      	not null,
   FECHA_APERTURA   TIMESTAMP WITHOUT TIME ZONE not null,
   NUMERO_CUOTA  	INT8    	            null,
   constraint PK_PLAN_TRATAMIENTO primary key (ID_PLAN_TRATAMIENTO)
); 

/*==============================================================*/
/* Table: PRESUPUESTO              */
/*==============================================================*/
create table PRESUPUESTO (
   ID_PRESUPUESTO         SERIAL                  not null,
   ID_PLAN_TRATAMIENTO   INT8                     not null,
   ID_ESTADO_GENERAL     INT8          	          not null,
   ID_TRATAMIENTO        INT8                     not null,
   ID_PIEZA              INT8                     not null,
   ID_SUPERFICIE         INT8                     not null,
   ID_PROCEDIMIENTO      INT8                     not null,
   ID_CONVENIO		 INT8			  not null,
   IVA  		 INT8   	          not null,
   TOTAL_VALOR_IVA       INT8			      null,
   VALOR_APROBADO	 INT8		          not null,
   PORCENTAJE_DESCUENTO  INT8          	          not null,   
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_PRESUPUESTO primary key (ID_PRESUPUESTO)
);

/*==============================================================*/
/* Table: CONCEPTO_PAGO              */
/*==============================================================*/
create table CONCEPTO_PAGO (
   ID_CONCEPTO_PAGO     SERIAL                 not null,
   NOMBRE  		VARCHAR(150)	          not null,
   ESTADO 	         BOOL            not null default true,
   constraint PK_CONCEPTO_PAGO primary key (ID_CONCEPTO_PAGO)
);



/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_CONCEPTO_PAGO          */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_CONCEPTO_PAGO on CONCEPTO_PAGO (
NOMBRE
); 

/*==============================================================*/
/* Table: FORMA_PAGO           					*/
/*==============================================================*/
create table FORMA_PAGO (
   ID_FORMA_PAGO        SERIAL                 not null,
   NOMBRE  		VARCHAR(150)	          not null,
   ESTADO 	         BOOL            not null default true,
   constraint PK_FORMA_PAGO primary key (ID_FORMA_PAGO)
);



/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_FORMA_PAGO          */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_FORMA_PAGO on FORMA_PAGO (
NOMBRE
); 


/*==============================================================*/
/* Table: SOPORTE_PAGO           					*/
/*==============================================================*/
create table SOPORTE_PAGO (
   ID_SOPORTE_PAGO        SERIAL                 not null,
   NOMBRE  		VARCHAR(150)	          not null,
   ESTADO 	         BOOL            not null default true,
   constraint PK_SOPORTE_PAGO primary key (ID_SOPORTE_PAGO)
);



/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_SOPORTE_PAGO          */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_SOPORTE_PAGO on SOPORTE_PAGO (
NOMBRE
); 

/*==============================================================*/
/* Table: PAGO           					*/
/*==============================================================*/
create table PAGO (
   ID_PAGO              SERIAL                 not null,
   ID_PACIENTE   	 INT8          	       not null,
   ID_PLAN_TRATAMIENTO   INT8                  not null,
   ID_SOPORTE_PAGO    	 INT8          	       not null,
   ID_FORMA_PAGO         INT8          	       not null,
   ID_CONCEPTO_PAGO      INT8                  not null,
   VALOR_PAGO            INT8          	       not null,
   ID_ESTADO_PAGO        INT8                  not null,
   OBSERVACION  	 TEXT	                   null,
   FECHA_PAGO      TIMESTAMP WITHOUT TIME ZONE not null,   
   ID_USUARIO_INGRESA     INT8          	    not null,
   constraint PK_PAGO primary key (ID_PAGO)
);

/*==============================================================*/
/* Table: ESTERILIZACION           					*/
/*==============================================================*/
create table ESTERILIZACION (
   ID_ESTERILIZACION         SERIAL                 not null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE  not null,
   FECHA_VECIMIENTO    TIMESTAMP WITHOUT TIME ZONE  not null,
   CARGA		 INT8 	       	            not null,
   LOTE_PROVETA         VARCHAR(150)                not null,
   ID_USUARIO_REPORTA   INT8                        not null,
   CANTIDAD_PAQUETE     INT8                        not null,  
   TIEMPO_ESTERILIZACION VARCHAR(20)  		    not null,
   TIEMPO_SECADO 	 VARCHAR(20)		    not null,
   TEMPERATURA		 VARCHAR(20)		        null,
   PRESION		 VARCHAR(20)			null,
   TIPO_ESTERILIZACION   VARCHAR(75)		    not null,
   ESTADO 	         BOOL            not null default true,
   constraint PK_ESTERILIZACION primary key (ID_ESTERILIZACION)
);


/*==============================================================*/
/* Index: INDEX_UNQ_LOTE_PROVETA_ESTERILIZACION          */
/*==============================================================*/
create unique index INDEX_UNQ_LOTE_PROVETA_ESTERILIZACION on ESTERILIZACION (
LOTE_PROVETA
); 


/*==============================================================*/
/* Table: TIPO_PAQUETE           					*/
/*==============================================================*/
create table TIPO_PAQUETE (
   ID_TIPO_PAQUETE         SERIAL            not null,
   NOMBRE               VARCHAR(150)                not null,
   DESCRIPCION          TEXT                            null,
   constraint PK_TIPO_PAQUETE primary key (ID_TIPO_PAQUETE)
);



/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_TIPO_PAQUETE          */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_TIPO_PAQUETE on TIPO_PAQUETE (
NOMBRE
); 


/*==============================================================*/
/* Table: PAQUETE_ESTERILIZACION           					*/
/*==============================================================*/
create table PAQUETE_ESTERILIZACION (
   ID_PAQUETE_ESTERILIZACION      SERIAL            not null,
   CODIGO                         INT8              not null,
   ID_TIPO_PAQUETE                INT8              not null,
   ID_ESTERILIZACION              INT8              not null,
   ESTADO 	         BOOL            not null default true,
   constraint PK_PAQUETE_ESTERILIZACION primary key (ID_PAQUETE_ESTERILIZACION)
); 

 
/*==============================================================*/
/* Table: EVOLUCION           					*/
/*==============================================================*/
create table EVOLUCION (
   ID_EVOLUCION         SERIAL                 not null,
   ID_PACIENTE   	 INT8          	       not null,
   ID_PLAN_TRATAMIENTO   INT8                  not null,
   NOMBRE_ACOMPANANTE   VARCHAR(150)               null,
   MOVIL_ACOMPANANTE    VARCHAR(150)               null,   
   FIRMA_PACIENTE  	VARCHAR(250)	        not null,
   EVOLUCION     	 TEXT	                   null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE not null,   
   ID_USUARIO_PROFESIONAL     INT8          	    not null,
   constraint PK_EVOLUCION primary key (ID_EVOLUCION)
);


/*==============================================================*/
/* Table: EVOLUCION_PAQUETE_ESTERILIZACION           					*/
/*==============================================================*/
create table EVOLUCION_PAQUETE_ESTERILIZACION (
   ID_EVOLUCION_PAQUETE_ESTERILIZACION  SERIAL not null,
   ID_EVOLUCION                   INT8            not null,
   ID_PAQUETE_ESTERILIZACION      INT8            not null,
   constraint PK_EVOLUCION_PAQUETE_ESTERILIZACION primary key (ID_EVOLUCION_PAQUETE_ESTERILIZACION)
);

/*==============================================================*/
/* Table: EXAMEN_GENERAL           					*/
/*==============================================================*/
create table EXAMEN_GENERAL (
   ID_EXAMEN_GENERAL         SERIAL                 not null,
   ID_PACIENTE   	 INT8          	            not null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE  not null,
   EDUCACION_GRUPAL        BOOL                     not null,
   VALORACION_MEDICINA     BOOL                     not null,
   SELLANTE        	   BOOL                     not null,
   DETARTRAJE   	   BOOL                     not null,
   FLUOR  		   BOOL                     not null,
   REMOCION_PLACA      	   BOOL                     not null,
   MOTIVO_CONSULTA         TEXT                     not null,
   EMBARAZO    		   BOOL                     not null,   
   DOLOR_MUSCULAR  	   TEXT	        		null,
   DOLOR_ARTILAR  	   TEXT	        		null,
   RUIDO_ARTILAR  	   TEXT	        		null,
   ALTERACION_MOVIMIENTO   TEXT	        		null,
   MAL_OCLUSION  	   TEXT	        		null,
   CRECIMIENTO_DESARROLLO  TEXT	        		null,
   FASETA_DESGASTE  	   TEXT	        		null,
   N_DIENTE_T_P  	   TEXT	        		null,
   ESTADO_ESTRUCTURAL  	   VARCHAR(300)        		null,
   ALTERACION_FLUROSIS     TEXT	        		null,
   REHABILITACION  	   TEXT	        		null,
   SALIVA  	           TEXT	        		null,
   PLACA  	   	   TEXT	        		null,
   FUNCION_TEJIDO  	   TEXT	        		null,
   ANTECEDENTE_MEDICO  	   TEXT	        		null,
   ANTECEDENTE_FAMILIAR    TEXT	        		null,
   MEDICAMENTO		   TEXT	        		null,
   ALERGIA 		   TEXT	        		null,
   FACTOR_RIESGO	   TEXT	        		null,
   DETERMINANTE_SOCIAL 	   TEXT	        	        null,
   POBLACION_VULNERABLE	   TEXT	        		null,
   ANTECEDENTE_SISTEMICO   TEXT	        		null,
   HABITO 		   TEXT	        		null,
   OTRO 		   TEXT	        		null,
   CARA 		   TEXT	        		null,
   GANGLIOS		   TEXT	        		null,
   MAXILAR_MANDIBULA 	   TEXT	        		null,
   MUSCULO		   TEXT	        		null,
   LABIO 		   TEXT	        		null,
   CARILLO		   TEXT	        		null,
   ENCIA 		   TEXT	        		null,
   LENGUA		   TEXT	        		null,
   PISO_BOCAL 		   TEXT	        		null,  
   PALADAR		   TEXT	        		null,
   ORFARINGE 		   TEXT	        		null,   
   ID_USUARIO_PROFESIONAL  INT8          	    not null,
   constraint PK_EXAMEN_GENERAL primary key (ID_EXAMEN_GENERAL)
);



/*==============================================================*/
/* Table: ODONTOGRAMA           					*/
/*==============================================================*/
create table ODONTOGRAMA (
   ID_ODONTOGRAMA         SERIAL                 not null,
   ID_PACIENTE   	 INT8          	            not null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE  not null,
   NUMERO_ODONTOGRAMA      INT4                     not null,
   OBSERVACION 		   TEXT	        		null,
   constraint PK_ODONTOGRAMA primary key (ID_ODONTOGRAMA)
);

/*==============================================================*/
/* Table: ODONTOGRAMA_PACIENTE           					*/
/*==============================================================*/
create table ODONTOGRAMA_PACIENTE (
   ID_ODONTOGRAMA_PACIENTE         SERIAL                 not null,
   ID_ODONTOGRAMA   	 INT8          	            not null,
   ID_PIEZA   	         INT8          	            not null, 
   ID_SUPERFICIE   	 INT8          	            not null,
   ID_RIP_TIPO_ODON      INT8          	            not null, 
   ID_DIAG_PRINCIPAL   	 INT8          	            not null,
   ID_USUARIO_PROFESIONAL     INT8          	    not null,
   constraint PK_ODONTOGRAMA_PACIENTE primary key (ID_ODONTOGRAMA_PACIENTE)
);


/*==============================================================*/
/* Table: PERIODONTOGRAMA           					*/
/*==============================================================*/
create table PERIODONTOGRAMA (
   ID_PERIODONTOGRAMA         SERIAL                 not null,
   ID_PACIENTE   	 INT8          	            not null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE  not null,
   ESTADO 	       VARCHAR(25)		    not null,   
   ID_USUARIO_PROFESIONAL     INT8          	    not null,
   constraint PK_PERIODONTOGRAMA primary key (ID_PERIODONTOGRAMA)
);

/*==============================================================*/
/* Table: PERIODONTOGRAMA_PACIENTE           					*/
/*==============================================================*/
create table PERIODONTOGRAMA_PACIENTE (
   ID_PERIODONTOGRAMA_PACIENTE SERIAL               not null,
   ID_PERIODONTOGRAMA 	 INT8          	            not null,
   ID_PIEZA   	         INT8          	            not null, 
   SANGRAMIENTO   	 VARCHAR(10)                    null,
   SUPURACION   	 VARCHAR(10)                    null,
   MOVILIDAD    	 INT8                    null,
   FURCA   	         VARCHAR(10)                    null,  
   POSICION_ENCIA   	 VARCHAR(10)                    null,
   PROFUNDIDAD_SACO   	 VARCHAR(10)                    null,
   FACTOR   	         VARCHAR(150)                   null,
   EFECTO_PROVOCADO   	 VARCHAR(150)                   null,
   TAMANO   	         VARCHAR(150)                   null,
   POSICION   	         VARCHAR(150)                   null,
   FORMA	   	 VARCHAR(150)                   null,
   COLOR   		 VARCHAR(150)                   null,
   NIVEL_INSERCION   	 VARCHAR(10)                    null,
   constraint PK_PERIODONTOGRAMA_PACIENTE primary key (ID_PERIODONTOGRAMA_PACIENTE)
);



/*==============================================================*/
/* Table: DIAGNOSTICO_PERIODONTOGRAMA           					*/
/*==============================================================*/
create table DIAGNOSTICO_PERIODONTOGRAMA (
   ID_DIAGNOSTICO_PERIODONTOGRAMA SERIAL            not null,
   ID_PERIODONTOGRAMA 	 INT8          	            not null,
   DIAGNOSTICO   	 TEXT		            not null,
   FUNDAMENTO	   	 TEXT		            not null,
   PLAN_TRATAMIENTO   	 TEXT		            not null,
   constraint PK_DIAGNOSTICO_PERIODONTOGRAMA primary key (ID_DIAGNOSTICO_PERIODONTOGRAMA)
);


/*==============================================================*/
/* Table: INDICE           					*/
/*==============================================================*/
create table INDICE (
   ID_INDICE 		SERIAL		            not null,
   ID_PERIODONTOGRAMA 	 INT8          	            not null,
   D18GI   		VARCHAR(20)                     null,
   D18HI	   	 	VARCHAR(20)                     null,
   D17GI   		VARCHAR(20)                     null,
   D17HI	   	 	VARCHAR(20)                     null,
   D16GI   		VARCHAR(20)                     null,
   D16HI	   	 	VARCHAR(20)                     null,
   D15GI   		VARCHAR(20)                     null,
   D15HI	   	 	VARCHAR(20)                     null,
   D14GI   		VARCHAR(20)                     null,
   D14HI	   	 	VARCHAR(20)                     null,
   D13GI   		VARCHAR(20)                     null,
   D13HI	   	 	VARCHAR(20)                     null,
   D12GI   		VARCHAR(20)                     null,
   D12HI	   	 	VARCHAR(20)                     null,
   D11GI   		VARCHAR(20)                     null,
   D11HI	   	 	VARCHAR(20)                     null,
   D21GI   		VARCHAR(20)                     null,
   D21HI	   	 	VARCHAR(20)                     null,
   D22GI   		VARCHAR(20)                     null,
   D22HI	   	 	VARCHAR(20)                     null,
   D23GI   		VARCHAR(20)                     null,
   D23HI	   	 	VARCHAR(20)                     null,
   D24GI   		VARCHAR(20)                     null,
   D24HI	   	 	VARCHAR(20)                     null,
   D25GI   		VARCHAR(20)                     null,
   D25HI	   	 	VARCHAR(20)                     null,
   D26GI   		VARCHAR(20)                     null,
   D26HI	   	 	VARCHAR(20)                     null,
   D27GI   		VARCHAR(20)                     null,
   D27HI	   	 	VARCHAR(20)                     null,
   D28GI   		VARCHAR(20)                     null,
   D28HI	   	 	VARCHAR(20)                     null,
   D55GI   		VARCHAR(20)                     null,
   D55HI	   	 	VARCHAR(20)                     null,
   D54GI   		VARCHAR(20)                     null,
   D54HI	   	 	VARCHAR(20)                     null,
   D53GI   		VARCHAR(20)                     null,
   D53HI	   	 	VARCHAR(20)                     null,
   D52GI   		VARCHAR(20)                     null,
   D52HI	   	 	VARCHAR(20)                     null,
   D51GI   		VARCHAR(20)                     null,
   D51HI	   	 	VARCHAR(20)                     null,
   D61GI   		VARCHAR(20)                     null,
   D61HI	   	 	VARCHAR(20)                     null,
   D62GI   		VARCHAR(20)                     null,
   D62HI	   	 	VARCHAR(20)                     null,
   D63GI   		VARCHAR(20)                     null,
   D63HI	   	 	VARCHAR(20)                     null,
   D64GI   		VARCHAR(20)                     null,
   D64HI	   	 	VARCHAR(20)                     null,
   D65GI   		VARCHAR(20)                     null,
   D65HI	   	 	VARCHAR(20)                     null,
   D85GI   		VARCHAR(20)                     null,
   D85HI	   	 	VARCHAR(20)                     null,
   D84GI   		VARCHAR(20)                     null,
   D84HI	   	 	VARCHAR(20)                     null,
   D83GI   		VARCHAR(20)                     null,
   D83HI	   	 	VARCHAR(20)                     null,
   D82GI   		VARCHAR(20)                     null,
   D82HI	   	 	VARCHAR(20)                     null,
   D81GI   		VARCHAR(20)                     null,
   D81HI	   	 	VARCHAR(20)                     null,
   D71GI   		VARCHAR(20)                     null,
   D71HI	   	 	VARCHAR(20)                     null,
   D72GI   		VARCHAR(20)                     null,
   D72HI	   	 	VARCHAR(20)                     null,
   D73GI   		VARCHAR(20)                     null,
   D73HI	   	 	VARCHAR(20)                     null,
   D74GI   		VARCHAR(20)                     null,
   D74HI	   	 	VARCHAR(20)                     null,
   D75GI   		VARCHAR(20)                     null,
   D75HI	   	 	VARCHAR(20)                     null,
   D48GI   		VARCHAR(20)                     null,
   D48HI	   	 	VARCHAR(20)                     null,
   D47GI   		VARCHAR(20)                     null,
   D47HI	   	 	VARCHAR(20)                     null,
   D46GI   		VARCHAR(20)                     null,
   D46HI	   	 	VARCHAR(20)                     null,
   D45GI   		VARCHAR(20)                     null,
   D45HI	   	 	VARCHAR(20)                     null,
   D44GI   		VARCHAR(20)                     null,
   D44HI	   	 	VARCHAR(20)                     null,
   D43GI   		VARCHAR(20)                     null,
   D43HI	   	 	VARCHAR(20)                     null,
   D42GI   		VARCHAR(20)                     null,
   D42HI	   	 	VARCHAR(20)                     null,
   D41GI   		VARCHAR(20)                     null,
   D41HI	   	 	VARCHAR(20)                     null,
   D31GI   		VARCHAR(20)                     null,
   D31HI	   	 	VARCHAR(20)                     null,
   D32GI   		VARCHAR(20)                     null,
   D32HI	   	 	VARCHAR(20)                     null,
   D33GI   		VARCHAR(20)                     null,
   D33HI	   	 	VARCHAR(20)                     null,
   D34GI   		VARCHAR(20)                     null,
   D34HI	   	 	VARCHAR(20)                     null,
   D35GI   		VARCHAR(20)                     null,
   D35HI	   	 	VARCHAR(20)                     null,
   D36GI   		VARCHAR(20)                     null,
   D36HI	   	 	VARCHAR(20)                     null,
   D37GI   		VARCHAR(20)                     null,
   D37HI	   	 	VARCHAR(20)                     null,
   D38GI   		VARCHAR(20)                     null,
   D38HI	   	 	VARCHAR(20)                     null,
   constraint PK_INDICE primary key (ID_INDICE)
);

/*==============================================================*/
/* Table: ENDODONCIA           					*/
/*==============================================================*/
create table ENDODONCIA (
   ID_ENDODONCIA         SERIAL                 not null,
   ID_PACIENTE   	 INT8          	            not null,
   TIPO     	         VARCHAR(150)			null,
   ID_PIEZA      	 INT8          	            not null,
   DERIVADO_POR          VARCHAR(150)                   null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE  not null,
   MOTIVO_CONSULTA	   TEXT	        		null,   
   OBSERVACION_HISTORICA   TEXT	        		null,
   PROPIEDAD		   TEXT	        		null,
   PROVOCADO	           TEXT	        		null,
   EVOLUCION_END	   TEXT	        		null,
   OTRO_SINTOMA_DOLOSO	   TEXT	        		null,
   AUMENTO_VOLUMEN         BOOL			    not	null,
   FISTULO		   BOOL			    not null,
   ENDENOPATIA             BOOL	        	    not	null,
   EXAMEN_INTRAORAL	   TEXT	        	        null,
   EXAMEN_PERIODONTAL	   TEXT	        		null,
   ESPACIO_PERIODONTAL_APICAL VARCHAR(150)     		null,
   HUESO_ALVEOLAR_APICAL    VARCHAR(150)       		null,
   AUSENTE                  VARCHAR(150)		null,
   SINTOMATOLOGIA_D_CALOR     VARCHAR(150)		null,
   SINTOMATOLOGIA_D_FRIO      VARCHAR(150)		null,
   SINTOMATOLOGIA_D_ELECTRICO VARCHAR(150)		null,
   SINTOMATOLOGIA_D_PERCUSION VARCHAR(150)		null,
   SINTOMATOLOGIA_D_EXPLORACION VARCHAR(150)		null,
   SINTOMATOLOGIA_D_CAVITARIA VARCHAR(150)		null,
   SINTOMATOLOGIA_C_CONDUCTO VARCHAR(150)		null,
   SINTOMATOLOGIA_C_TENTATIVA VARCHAR(150)		null,
   SINTOMATOLOGIA_C_DEFINITIVA VARCHAR(150)		null,
   SINTOMATOLOGIA_C_REFERENCIA VARCHAR(150)		null,
   SINTOMATOLOGIA_C_LIMAPICAL VARCHAR(150)		null,
   SINTOMATOLOGIA_C_DESOBTURACION VARCHAR(150)		null,
   SINTOMATOLOGIA_C_OTROS	TEXT 			null,
   DIAGNOSTICO		   TEXT				null,
   TRATAMIENTO		   TEXT				null,
   FECHA_ALTA	TIMESTAMP WITHOUT TIME ZONE 		null,     
   ID_USUARIO_PROFESIONAL     INT8          	    not null, 
   constraint PK_ENDODONCIA primary key (ID_ENDODONCIA)
);

/*==============================================================*/
/* Table: TIPO_CONTRATO           					*/
/*==============================================================*/
create table TIPO_CONTRATO (
   ID_TIPO_CONTRATO         SERIAL                 not null,
   NOMBRE   	 	 VARCHAR(150)               not null,
   CONTENIDO      	 VARCHAR(150)               not null,
   ESTADO     		 BOOL			    not null default TRUE,
   constraint PK_TIPO_CONTRATO primary key (ID_TIPO_CONTRATO)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_TIPO_CONTRATO       */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_TIPO_CONTRATO on TIPO_CONTRATO (
NOMBRE
); 

/*==============================================================*/
/* Table: TIPO_ARCHIVO           					*/
/*==============================================================*/
create table TIPO_ARCHIVO (
   ID_TIPO_ARCHIVO         SERIAL                   not null,
   NOMBRE   	 	 VARCHAR(150)               not null, 
   constraint PK_TIPO_ARCHIVO primary key (ID_TIPO_ARCHIVO)
);


/*==============================================================*/
/* Index: INDEX_UNQ_NOMBRE_TIPO_ARCHIVO      */
/*==============================================================*/
create unique index INDEX_UNQ_NOMBRE_TIPO_ARCHIVO on TIPO_ARCHIVO (
NOMBRE
); 

/*==============================================================*/
/* Table: CONTRATO           					*/
/*==============================================================*/
create table CONTRATO (
   ID_CONTRATO         SERIAL                 	    not null,
   ID_TIPO_ARCHIVO   	 INT8          	            not null,
   ID_TIPO_CONTRATO    	 INT8          	            not null,
   ID_PLAN_TRATAMIENTO   INT8          	    	    null,
   ID_PACIENTE    	 INT8          	            not null,
   ID_USUARIO    	 INT8          	            not null,
   FECHA_APERTURA      TIMESTAMP WITHOUT TIME ZONE  not null,
   FIRMA_PACIENTE	 TEXT		        null,
   ESTADO     		 BOOL			    not null default TRUE,
   NOMBRE_ACUDIENTE  	VARCHAR(150)	            null,
   CEDULAR_ACUDIENTE    VARCHAR(50)	            null,   
   PARENTESCO  	        VARCHAR(75)	            null,
   CEDULAR_RESPONSABLE  VARCHAR(50)	            null, 
   HUELLA_PACIENTE  	TEXT		            null,
   constraint PK_CONTRATO primary key (ID_CONTRATO)
);


CREATE TABLE public.recuperacion_cuenta(
	id serial NOT NULL,
	username character varying(50) NOT NULL,
	email character varying(100) NOT NULL,
	codigoverificacion character varying(5) NOT NULL,
	created_at timestamp NOT NULL,
	CONSTRAINT pk_recuperacion_cuenta PRIMARY KEY (id)

);

CREATE TABLE public.sessions(
	id serial NOT NULL,
	sess_id character varying(128) NOT NULL,
	sess_data bytea NOT NULL,
	sess_lifetime integer NOT NULL,
	sess_time integer NOT NULL,
	sess_user_name_cliente character varying(100) NOT NULL,
	CONSTRAINT sessions_pkey PRIMARY KEY (sess_id)

);
alter table MUNICIPIO
   add constraint FK_MUNICIPIO_REFERENCE_DEPARTAMENTO foreign key (ID_DEPARTAMENTO)
      references DEPARTAMENTO (ID_DEPARTAMENTO)
      on delete restrict on update restrict;

alter table CLINICA
   add constraint FK_CLINICA_REFERENCE_MUNICIPIO foreign key (ID_MUNICIPIO)
      references  MUNICIPIO (ID)
      on delete restrict on update restrict;

alter table CLINICA
   add constraint FK_CLINICA_REFERENCE_CLINICA foreign key (ID_SEDE_CLINICA)
      references  CLINICA (ID_CLINICA)
      on delete restrict on update restrict;


alter table CONSULTORIO
   add constraint FK_CONSULTORIO_REFERENCE_CLINICA foreign key (ID_CLINICA)
      references  CLINICA (ID_CLINICA)
      on delete restrict on update restrict;

alter table CONSULTORIO
   add constraint FK_CONSULTORIO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;


alter table EPS
   add constraint FK_EPS_REFERENCE_MUNICIPIO foreign key (ID_MUNICIPIO)
      references  MUNICIPIO (ID)
      on delete restrict on update restrict;

alter table EPS
   add constraint FK_EPS_REFERENCE_EPS foreign key (ID_SEDE_EPS)
      references  EPS (ID_EPS)
      on delete restrict on update restrict;

alter table EPS
   add constraint FK_EPS_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table CONVENIO
   add constraint FK_CONVENIO_REFERENCE_CLINICA foreign key (ID_CLINICA)
      references  CLINICA (ID_CLINICA)
      on delete restrict on update restrict;

alter table CONVENIO
   add constraint FK_CONVENIO_REFERENCE_EPS foreign key (ID_EPS)
      references  EPS (ID_EPS)
      on delete restrict on update restrict;

alter table CONVENIO
   add constraint FK_CONVENIO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table PACIENTE
   add constraint FK_PACIENTE_REFERENCE_TIPO_DOCUMENTO foreign key (TIPO_IDENTIFICACION)
      references  TIPO_DOCUMENTO (ID)
      on delete restrict on update restrict;
 
alter table PACIENTE
   add constraint FK_PACIENTE_REFERENCE_MUNICIPIO foreign key (ID_MUNICIPIO)
      references  MUNICIPIO (ID)
      on delete restrict on update restrict;

alter table PACIENTE
   add constraint FK_PACIENTE_REFERENCE_EPS foreign key (ID_EPS)
      references  EPS (ID_EPS)
      on delete restrict on update restrict;


alter table PACIENTE
   add constraint FK_PACIENTE_REFERENCE_TIPO_USUARIO foreign key (REGIMEN)
      references  TIPO_USUARIO (ID_TIPO_USUARIO)
      on delete restrict on update restrict;

alter table PACIENTE
   add constraint FK_PACIENTE_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;


alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_CUPS foreign key (ID_CUPS)
      references  CUPS (ID_CUPS)
      on delete restrict on update restrict;

alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_CIE_RIP foreign key (ID_RIPS)
      references  CIE_RIP (ID_CIE_RIP)
      on delete restrict on update restrict;

alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_CIE_RIP1 foreign key (ID_DXR1)
      references  CIE_RIP (ID_CIE_RIP)
      on delete restrict on update restrict;

alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_CIE_RIP2 foreign key (ID_DXR2)
      references  CIE_RIP (ID_CIE_RIP)
      on delete restrict on update restrict;

alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_CIE_RIP3 foreign key (ID_DXR3)
      references  CIE_RIP (ID_CIE_RIP)
      on delete restrict on update restrict; 	     
    	     
alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_CAUSA_EXTERNA foreign key (ID_CAUSA_EXTERNA)
      references  CAUSA_EXTERNA (ID_CAUSA_EXTERNA)
      on delete restrict on update restrict;   

alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_AMBITO_REALIZACION foreign key (ID_AMBITO_REALIZACION)
      references AMBITO_REALIZACION (ID_AMBITO_REALIZACION)
      on delete restrict on update restrict;  

alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_FORMA_REALIZACION foreign key (ID_FORMA_REALIZACION)
      references FORMA_REALIZACION (ID_FORMA_REALIZACION)
      on delete restrict on update restrict;  

alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_PERSONA_ATIENDE foreign key (ID_PERSONA_ATIENDE)
      references PERSONA_ATIENDE (ID_PERSONA_ATIENDE)
      on delete restrict on update restrict;

alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_FINALIDAD_PROCEDIMIENTO foreign key (ID_FINALIDAD_PROCEDIMIENTO)
      references FINALIDAD_PROCEDIMIENTO (ID_FINALIDAD_PROCEDIMIENTO)
      on delete restrict on update restrict;

alter table CUP_DIAGNOSTICO
   add constraint FK_CUP_DIAGNOSTICO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table TIPO_DIAGNOSTICO
   add constraint FK_TIPO_DIAGNOSTICO_REFERENCE_AREA foreign key (ID_AREA)
      references AREA (ID_AREA)
      on delete restrict on update restrict;

alter table CIE_DIAGNOSTICO_TIPO
   add constraint FK_CIE_DIAGNOSTICO_TIPO_REFERENCE_CIE_RIP foreign key (ID_CIE_RIP)
      references CIE_RIP (ID_CIE_RIP)
      on delete restrict on update restrict;

alter table CIE_DIAGNOSTICO_TIPO
   add constraint FK_CIE_DIAGNOSTICO_TIPO_REFERENCE_DIAGNOSTICO_ODONTOLOGICO foreign key (ID_DIAGNOSTICO_ODONTOLOGICO)
      references DIAGNOSTICO_ODONTOLOGICO (ID_DIAGNOSTICO_ODONTOLOGICO)
      on delete restrict on update restrict;

alter table CIE_DIAGNOSTICO_TIPO
   add constraint FK_CIE_DIAGNOSTICO_TIPO_REFERENCE_TIPO_DIAGNOSTICO foreign key (ID_TIPO_DIAGNOSTICO)
      references TIPO_DIAGNOSTICO (ID_TIPO_DIAGNOSTICO)
      on delete restrict on update restrict;

alter table CIE_DIAGNOSTICO_TIPO
   add constraint FK_CIE_DIAGNOSTICO_TIPO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table CATEGORIA_TRATAMIENTO
   add constraint FK_CATEGORIA_TRATAMIENTO_REFERENCE_AREA foreign key (ID_AREA)
      references AREA (ID_AREA)
      on delete restrict on update restrict;

alter table CATEGORIA_TRATAMIENTO
   add constraint FK_CATEGORIA_TRATAMIENTO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table TRATAMIENTO
   add constraint FK_TRATAMIENTO_REFERENCE_CATEGORIA_TRATAMIENTO foreign key (ID_CATEGORIA_TRATAMIENTO)
      references CATEGORIA_TRATAMIENTO (ID_CATEGORIA_TRATAMIENTO)
      on delete restrict on update restrict;

alter table TRATAMIENTO
   add constraint FK_TRATAMIENTO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;


alter table TRATAMIENTO_CONVENIO
   add constraint FK_TRATAMIENTO_CONVENIO_REFERENCE_TRATAMIENTO foreign key (ID_TRATAMIENTO)
      references TRATAMIENTO (ID_TRATAMIENTO)
      on delete restrict on update restrict;

alter table TRATAMIENTO_CONVENIO
   add constraint FK_TRATAMIENTO_CONVENIO_REFERENCE_CONVENIO foreign key (ID_CONVENIO)
      references CONVENIO (ID_CONVENIO)
      on delete restrict on update restrict;

alter table TRATAMIENTO_CONVENIO
   add constraint FK_TRATAMIENTO_CONVENIO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table ACUERDO_PAGO
   add constraint FK_ACUERDO_PAGO_REFERENCE_TRATAMIENTO foreign key (ID_TRATAMIENTO)
      references TRATAMIENTO (ID_TRATAMIENTO)
      on delete restrict on update restrict;

alter table AGENDA_USUARIO
   add constraint FK_AGENDA_USUARIO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table PERFIL
   add constraint FK_PERFIL_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table PERFIL_PERMISO
   add constraint FK_PERFIL_PERMISO_REFERENCE_PERFIL foreign key (ID_PERFIL)
      references PERFIL (ID_PERFIL)
      on delete restrict on update restrict;

alter table PERFIL_PERMISO
   add constraint FK_PERFIL_PERMISO_REFERENCE_PERMISO foreign key (ID_PERMISO)
      references PERMISO (ID_PERMISO)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_TIPO_DOCUMENTO foreign key (TIPO_IDENTIFICACION)
      references  TIPO_DOCUMENTO (ID)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_ESPECIALIDAD foreign key (ID_ESPECIALIDAD)
      references  ESPECIALIDAD (ID_ESPECIALIDAD)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_ACUERDO_PAGO_PORC foreign key (ID_ACUERDO_PORC)
      references  ACUERDO_PAGO (ID_ACUERDO_PAGO)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_ACUERDO_PAGO_VALOR foreign key (ID_ACUERDO_VALOR)
      references  ACUERDO_PAGO (ID_ACUERDO_PAGO)
      on delete restrict on update restrict;


alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_ACUERDO_AGENDA_USUARIO foreign key (ID_AGENDA_USUARIO)
      references  AGENDA_USUARIO (ID_AGENDA_USUARIO)
      on delete restrict on update restrict;


alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_ACUERDO_PERFIL foreign key (ID_PERFIL)
      references  PERFIL (ID_PERFIL)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_CLINICA foreign key (ID_SUCURSAL)
      references  CLINICA (ID_CLINICA)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_UAURIO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table MOTIVO_CONSULTA
   add constraint FK_MOTIVO_CONSULTA_REFERENCE_CUPS foreign key (ID_PROCEDIMIENTO)
      references  CUPS (ID_CUPS)
      on delete restrict on update restrict;

          
alter table CITA
   add constraint FK_CITA_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table CITA
   add constraint FK_CITA_REFERENCE_CLINICA foreign key (ID_CLINICA)
      references CLINICA (ID_CLINICA)
      on delete restrict on update restrict;

alter table CITA
   add constraint FK_CITA_REFERENCE_USUARIO_PROFESIONAL foreign key (ID_USUARIO_PROFESIONAL)
      references USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table CITA
   add constraint FK_CITA_REFERENCE_ESTADO_CITA foreign key (ID_ESTADO_CITA)
      references ESTADO_CITA (ID_ESTADO_CITA)
      on delete restrict on update restrict;


alter table CITA
   add constraint FK_CITA_REFERENCE_MOTIVO_CONSULTA foreign key (ID_MOTIVO_CONSULTA)
      references  MOTIVO_CONSULTA (ID_MOTIVO_CONSULTA)
      on delete restrict on update restrict;

alter table CITA
   add constraint FK_CITA_REFERENCE_CONSULTORIO foreign key (ID_CONSULTORIO)
      references  CONSULTORIO (ID_CONSULTORIO)
      on delete restrict on update restrict;

alter table CITA
   add constraint FK_CITA_REFERENCE_USUARIO_GESTIONA foreign key (ID_USUARIO_GESTIONA)
      references USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table INSUMO
   add constraint FK_INSUMO_REFERENCE_CATEGORIA_INVENTARIO foreign key (ID_CATEGORIA)
      references CATEGORIA_INVENTARIO (ID_CATEGORIA_INVENTARIO)
      on delete restrict on update restrict;

alter table INSUMO
   add constraint FK_INSUMO_REFERENCE_MARCA foreign key (ID_MARCA)
      references MARCA (ID_MARCA)
      on delete restrict on update restrict;

alter table INSUMO
   add constraint FK_INSUMO_REFERENCE_PRESENTACION foreign key (ID_PRESENTACION)
      references PRESENTACION (ID_PRESENTACION)
      on delete restrict on update restrict;
  
alter table INSUMO
   add constraint FK_INSUMO_REFERENCE_NOMBRE_INSUMO foreign key (ID_NOMBRE_INSUMO)
      references NOMBRE_INSUMO (ID_NOMBRE_INSUMO)
      on delete restrict on update restrict;  
   
alter table PROVEEDOR
   add constraint FK_PROVEEDOR_REFERENCE_TIPO_DOCUMENTO foreign key (ID_TIPO_DOCUMENTO)
      references  TIPO_DOCUMENTO (ID)
      on delete restrict on update restrict;

alter table PROVEEDOR
   add constraint FK_PROVEEDOR_REFERENCE_MUNICIPIO foreign key (ID_MUNICIPIO)
      references  MUNICIPIO (ID)
      on delete restrict on update restrict;

alter table PEDIDO
   add constraint FK_PEDIDO_REFERENCE_PROVEEDOR foreign key (ID_PROVEEDOR)
      references  PROVEEDOR (ID_PROVEEDOR)
      on delete restrict on update restrict;

alter table PEDIDO
   add constraint FK_PEDIDO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;


alter table ENTRADA_PEDIDO
   add constraint FK_ENTRADA_PEDIDO_REFERENCE_PEDIDO foreign key (ID_PEDIDO)
      references  PEDIDO (ID_PEDIDO)
      on delete restrict on update restrict;

alter table ENTRADA_PEDIDO
   add constraint FK_ENTRADA_PEDIDO_REFERENCE_INSUMO foreign key (ID_INSUMO)
      references  INSUMO (ID_INSUMO)
      on delete restrict on update restrict;

alter table ENTRADA_PEDIDO
   add constraint FK_ENTRADA_PEDIDO_REFERENCE_CLINICA foreign key (ID_CLINICA_UBICACION)
      references  CLINICA (ID_CLINICA)
      on delete restrict on update restrict;

alter table ENTRADA_PEDIDO
   add constraint FK_ENTRADA_PEDIDO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIMIENTO_REFERENCE_ENTRADA_PEDIDO foreign key (ID_ENTRADA_PEDIDO)
      references  ENTRADA_PEDIDO (ID_ENTRADA_PEDIDO)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIMIENTO_REFERENCE_ESTADO_MANTENIMIENTO foreign key (ID_ESTADO_MANTENIMIENTO)
      references  ESTADO_MANTENIMIENTO (ID_ESTADO_MANTENIMIENTO)
      on delete restrict on update restrict;

alter table MANTENIMIENTO
   add constraint FK_MANTENIMIENTO_REFERENCE_USUARIO foreign key (ID_USUARIO_APRUEBA)
      references USUARIO (ID_USUARIO)
      on delete restrict on update restrict;
   

alter table SALIDA
   add constraint FK_SALIDA_REFERENCE_ENTRADA_PEDIDO foreign key (ID_ENTRADA_PEDIDO)
      references ENTRADA_PEDIDO (ID_ENTRADA_PEDIDO)
      on delete restrict on update restrict;

alter table SALIDA
   add constraint FK_SALIDA_REFERENCE_CONSULTORIO foreign key (ID_CONSULTORIO)
      references CONSULTORIO (ID_CONSULTORIO)
      on delete restrict on update restrict;

alter table SALIDA
   add constraint FK_SALIDA_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table SALIDA
   add constraint FK_SALIDA_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table SALIDA_CONSULTORIO
   add constraint FK_SALIDA_CONSULTORIO_REFERENCE_SALIDA foreign key (ID_SALIDA)
      references SALIDA (ID_SALIDA)
      on delete restrict on update restrict;

alter table SALIDA_CONSULTORIO
   add constraint FK_SALIDA_CONSULTORIO_REFERENCE_CONSULTORIO foreign key (ID_CONSULTORIO)
      references CONSULTORIO (ID_CONSULTORIO)
      on delete restrict on update restrict;

alter table SALIDA_CONSULTORIO
   add constraint FK_SALIDA_CONSULTORIO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table STOCK
   add constraint FK_STOCK_REFERENCE_NOMBRE_INSUMO foreign key (ID_NOMBRE_INSUMO)
      references  NOMBRE_INSUMO (ID_NOMBRE_INSUMO)
      on delete restrict on update restrict;

alter table STOCK
   add constraint FK_STOCK_REFERENCE_CLINICA foreign key (ID_CLINICA)
      references  CLINICA (ID_CLINICA)
      on delete restrict on update restrict;

alter table TIPO_RADIOGRAFIA
   add constraint FK_TIPO_RADIOGRAFIA_REFERENCE_TIPO_RADIOGRAFIA foreign key (ID_SEDE_TIPO_RADIOGRAFIA)
      references  TIPO_RADIOGRAFIA (ID_TIPO_RADIOGRAFIA)
      on delete restrict on update restrict;


alter table RADIOGRAFIA
   add constraint FK_RADIOGRAFIA_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table RADIOGRAFIA
   add constraint FK_RADIOGRAFIA_REFERENCE_TIPO_RADIOGRAFIA foreign key (ID_TIPO_RADIOGRAFIA)
      references  TIPO_RADIOGRAFIA (ID_TIPO_RADIOGRAFIA)
      on delete restrict on update restrict;

alter table RADIOGRAFIA
   add constraint FK_RADIOGRAFIA_REFERENCE_USUARIO foreign key (ID_USUARIO)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table RADIOGRAFIA
   add constraint FK_RADIOGRAFIA_REFERENCE_USUARIO_INGRESO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table IMAGEN
   add constraint FK_IMAGEN_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table IMAGEN
   add constraint FK_IMAGEN_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;


alter table REMISION
   add constraint FK_REMISION_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;


alter table REMISION
   add constraint FK_REMISION_REFERENCE_USUARIO_REMITE foreign key (ID_USUARIO_REMITE)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table REMISION
   add constraint FK_REMISION_REFERENCE_USUARIO_REMITIDO foreign key (ID_USUARIO_REMITIDO)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table REMISION
   add constraint FK_REMISION_REFERENCE_CUPS foreign key (ID_PROCEDIMIENTO)
      references  CUPS (ID_CUPS)
      on delete restrict on update restrict;

alter table ANEXO_PACIENTE
   add constraint FK_ANEXO_PACIENTE_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table ANEXO_PACIENTE
   add constraint FK_ANEXO_PACIENTE_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;


alter table ANEXO_PACIENTE
   add constraint FK_ANEXO_PACIENTE_REFERENCE_REMISION foreign key (ID_REMISION)
      references  REMISION (ID_REMISION)
      on delete restrict on update restrict;

alter table FORMULA
   add constraint FK_FORMULA_REFERENCE_USUARIO foreign key (ID_USUARIO)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table FORMULA
   add constraint FK_FORMULA_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table FORMULA
   add constraint FK_FORMULA_REFERENCE_CIE_RIP foreign key (ID_CIE)
      references  CIE_RIP (ID_CIE_RIP)
      on delete restrict on update restrict;


alter table TIPO_CONSENTIMIENTO
   add constraint FK_TIPO_CONSENTIMIENTO_REFERENCE_CIE_RIP foreign key (ID_CIE)
      references  CIE_RIP (ID_CIE_RIP)
      on delete restrict on update restrict;
   
alter table CONSENTIMIENTO
   add constraint FK_CONSENTIMIENTO_REFERENCE_USUARIO foreign key (ID_USUARIO)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;


alter table CONSENTIMIENTO
   add constraint FK_CONSENTIMIENTO_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table CONSENTIMIENTO
   add constraint FK_CONSENTIMIENTO_REFERENCE_TIPO_CONSENTIMIENTO foreign key (ID_TIPO_CONSENTIMIENTO)
      references  TIPO_CONSENTIMIENTO (ID_TIPO_CONSENTIMIENTO)
      on delete restrict on update restrict;

alter table PLAN_TRATAMIENTO
   add constraint FK_PLAN_TRATAMIENTO_REFERENCE_ESTADO_GENERAL foreign key (ID_ESTADO_GENERAL)
      references  ESTADO_GENERAL (ID_ESTADO_GENERAL)
      on delete restrict on update restrict;

alter table PLAN_TRATAMIENTO
   add constraint FK_PLAN_TRATAMIENTO_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table PRESUPUESTO
   add constraint FK_PRESUPUESTO_REFERENCE_PLAN_TRATAMIENTO foreign key (ID_PLAN_TRATAMIENTO)
      references  PLAN_TRATAMIENTO (ID_PLAN_TRATAMIENTO)
      on delete restrict on update restrict;

alter table PRESUPUESTO
   add constraint FK_PRESUPUESTO_REFERENCE_ESTADO_GENERAL foreign key (ID_ESTADO_GENERAL)
      references  ESTADO_GENERAL (ID_ESTADO_GENERAL)
      on delete restrict on update restrict;

alter table PRESUPUESTO
   add constraint FK_PRESUPUESTO_REFERENCE_CONVENIO foreign key (ID_CONVENIO)
      references  CONVENIO (ID_CONVENIO)
      on delete restrict on update restrict;

alter table PRESUPUESTO
   add constraint FK_PRESUPUESTO_REFERENCE_TRATAMIENTO foreign key (ID_TRATAMIENTO)
      references  TRATAMIENTO (ID_TRATAMIENTO)
      on delete restrict on update restrict;


alter table PRESUPUESTO
   add constraint FK_PRESUPUESTO_REFERENCE_PIEZA_DENTAL foreign key (ID_PIEZA)
      references  PIEZA_DENTAL (ID_PIEZA_DENTAL)
      on delete restrict on update restrict;

alter table PRESUPUESTO
   add constraint FK_PRESUPUESTO_REFERENCE_SUPERFICIE foreign key (ID_SUPERFICIE)
      references  SUPERFICIE (ID_SUPERFICIE)
      on delete restrict on update restrict;


alter table PRESUPUESTO
   add constraint FK_PRESUPUESTO_REFERENCE_CUPS foreign key (ID_PROCEDIMIENTO)
      references  CUPS (ID_CUPS)
      on delete restrict on update restrict;

alter table PRESUPUESTO
   add constraint FK_PRESUPUESTO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;


alter table PAGO
   add constraint FK_PAGO_REFERENCE_PLAN_TRATAMIENTO foreign key (ID_PLAN_TRATAMIENTO)
      references  PLAN_TRATAMIENTO (ID_PLAN_TRATAMIENTO)
      on delete restrict on update restrict;


alter table PAGO
   add constraint FK_PAGO_REFERENCE_SOPORTE_PAGO foreign key (ID_SOPORTE_PAGO)
      references  SOPORTE_PAGO (ID_SOPORTE_PAGO)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_REFERENCE_FORMA_PAGO foreign key (ID_FORMA_PAGO)
      references  FORMA_PAGO (ID_FORMA_PAGO)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_REFERENCE_CONCEPTO_PAGO foreign key (ID_CONCEPTO_PAGO)
      references  CONCEPTO_PAGO (ID_CONCEPTO_PAGO)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_REFERENCE_ESTADO_PAGO foreign key (ID_ESTADO_PAGO)
      references  ESTADO_PAGO (ID_ESTADO_PAGO)
      on delete restrict on update restrict;

alter table PAGO
   add constraint FK_PAGO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table ESTERILIZACION
   add constraint FK_ESTERILIZACION_REFERENCE_USUARIO foreign key (ID_USUARIO_REPORTA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table PAQUETE_ESTERILIZACION
   add constraint FK_PAQUETE_ESTERILIZACION_REFERENCE_TIPO_PAQUETE foreign key (ID_TIPO_PAQUETE)
      references  TIPO_PAQUETE (ID_TIPO_PAQUETE)
      on delete restrict on update restrict;


alter table PAQUETE_ESTERILIZACION
   add constraint FK_PAQUETE_ESTERILIZACION_REFERENCE_ESTERILIZACION foreign key (ID_ESTERILIZACION)
      references  ESTERILIZACION (ID_ESTERILIZACION)
      on delete restrict on update restrict;
 

alter table EVOLUCION
   add constraint FK_EVOLUCION_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table EVOLUCION
   add constraint FK_EVOLUCION_REFERENCE_PLAN_TRATAMIENTO foreign key (ID_PLAN_TRATAMIENTO)
      references  PLAN_TRATAMIENTO (ID_PLAN_TRATAMIENTO)
      on delete restrict on update restrict;

alter table EVOLUCION
   add constraint FK_EVOLUCION_REFERENCE_USUARIO foreign key (ID_USUARIO_PROFESIONAL)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table EVOLUCION_PAQUETE_ESTERILIZACION
   add constraint FK_EVOLUCION_PAQUETE_ESTERILIZACION_REFERENCE_EVOLUCION foreign key (ID_EVOLUCION)
      references  EVOLUCION (ID_EVOLUCION)
      on delete restrict on update restrict;

alter table EVOLUCION_PAQUETE_ESTERILIZACION
   add constraint FK_EVOLUCION_PAQUETE_REFERENCE_PAQUETE_ESTERILIZACION foreign key (ID_PAQUETE_ESTERILIZACION)
      references  PAQUETE_ESTERILIZACION (ID_PAQUETE_ESTERILIZACION)
      on delete restrict on update restrict;

alter table EXAMEN_GENERAL
   add constraint FK_EXAMEN_GENERAL_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table EXAMEN_GENERAL
   add constraint FK_EXAMEN_GENERAL_REFERENCE_USUARIO foreign key (ID_USUARIO_PROFESIONAL)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table ODONTOGRAMA
   add constraint FK_ODONTOGRAMA_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;
 
alter table ODONTOGRAMA_PACIENTE
   add constraint FK_ODONTOGRAMA_PACIENTE_REFERENCE_ODONTOGRAMA foreign key (ID_ODONTOGRAMA)
      references  ODONTOGRAMA (ID_ODONTOGRAMA)
      on delete restrict on update restrict;

alter table ODONTOGRAMA_PACIENTE
   add constraint FK_ODONTOGRAMA_PACIENTE_REFERENCE_PIEZA_DENTAL foreign key (ID_PIEZA)
      references  PIEZA_DENTAL (ID_PIEZA_DENTAL)
      on delete restrict on update restrict;


alter table ODONTOGRAMA_PACIENTE
   add constraint FK_ODONTOGRAMA_PACIENTE_REFERENCE_SUPERFICIE foreign key (ID_SUPERFICIE)
      references  SUPERFICIE (ID_SUPERFICIE)
      on delete restrict on update restrict;

alter table ODONTOGRAMA_PACIENTE
   add constraint FK_ODONTOGRAMA_PACIENTE_REFERENCE_CIE_DIAGNOSTICO_TIPO foreign key (ID_RIP_TIPO_ODON)
      references  CIE_DIAGNOSTICO_TIPO (ID_CIE_DIAGNOSTICO_TIPO)
      on delete restrict on update restrict;

alter table ODONTOGRAMA_PACIENTE
   add constraint FK_ODONTOGRAMA_PACIENTE_REFERENCE_DIAGNOSTICO_PRINCIPAL foreign key (ID_DIAG_PRINCIPAL)
      references  DIAGNOSTICO_PRINCIPAL (ID_DIAGNOSTICO_PRINCIPAL)
      on delete restrict on update restrict;

alter table ODONTOGRAMA_PACIENTE
   add constraint FK_ODONTOGRAMA_PACIENTE_REFERENCE_USUARIO foreign key (ID_USUARIO_PROFESIONAL)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table PERIODONTOGRAMA
   add constraint FK_PERIODONTOGRAMA_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table PERIODONTOGRAMA
   add constraint FK_PERIODONTOGRAMA_REFERENCE_USUARIO foreign key (ID_USUARIO_PROFESIONAL)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table PERIODONTOGRAMA_PACIENTE
   add constraint FK_PERIODONTOGRAMA_PACIENTE_REFERENCE_PERIODONTOGRAMA foreign key (ID_PERIODONTOGRAMA)
      references  PERIODONTOGRAMA (ID_PERIODONTOGRAMA)
      on delete restrict on update restrict;


alter table PERIODONTOGRAMA_PACIENTE
   add constraint FK_PERIODONTOGRAMA_PACIENTE_REFERENCE_PIEZA_DENTAL foreign key (ID_PIEZA)
      references  PIEZA_DENTAL (ID_PIEZA_DENTAL)
      on delete restrict on update restrict;

alter table DIAGNOSTICO_PERIODONTOGRAMA
   add constraint FK_DIAGNOSTICO_PERIODONTOGRAMA_REFERENCE_PERIODONTOGRAMA foreign key (ID_PERIODONTOGRAMA)
      references  PERIODONTOGRAMA (ID_PERIODONTOGRAMA)
      on delete restrict on update restrict;

alter table INDICE
   add constraint FK_INDICE_REFERENCE_PERIODONTOGRAMA foreign key (ID_PERIODONTOGRAMA)
      references  PERIODONTOGRAMA (ID_PERIODONTOGRAMA)
      on delete restrict on update restrict;



alter table ENDODONCIA
   add constraint FK_ENDODONCIA_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table ENDODONCIA
   add constraint FK_ENDODONCIA_REFERENCE_PIEZA_DENTAL foreign key (ID_PIEZA)
      references  PIEZA_DENTAL (ID_PIEZA_DENTAL)
      on delete restrict on update restrict;


alter table ENDODONCIA
   add constraint FK_ENDODONCIA_REFERENCE_USUARIO foreign key (ID_USUARIO_PROFESIONAL)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table CONTRATO
   add constraint FK_CONTRATO_REFERENCE_PACIENTE foreign key (ID_PACIENTE)
      references  PACIENTE (ID_PACIENTE)
      on delete restrict on update restrict;

alter table CONTRATO
   add constraint FK_CONTRATO_REFERENCE_TIPO_ARCHIVO foreign key (ID_TIPO_ARCHIVO)
      references  TIPO_ARCHIVO (ID_TIPO_ARCHIVO)
      on delete restrict on update restrict;

alter table CONTRATO
   add constraint FK_CONTRATO_REFERENCE_TIPO_CONTRATO foreign key (ID_TIPO_CONTRATO)
      references  TIPO_CONTRATO (ID_TIPO_CONTRATO)
      on delete restrict on update restrict;

alter table CONTRATO
   add constraint FK_CONTRATO_REFERENCE_PLAN_TRATAMIENTO foreign key (ID_PLAN_TRATAMIENTO)
      references  PLAN_TRATAMIENTO (ID_PLAN_TRATAMIENTO)
      on delete restrict on update restrict;


alter table CONTRATO
   add constraint FK_CONTRATO_REFERENCE_USUARIO foreign key (ID_USUARIO)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table REQUERIMIENTO
   add constraint FK_REQUERIMIENTO_REFERENCE_CLINICA foreign key (ID_SEDE_CLINICA)
      references  CLINICA (ID_CLINICA)
      on delete restrict on update restrict;

alter table REQUERIMIENTO
   add constraint FK_REQUERIMIENTO_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references  USUARIO (ID_USUARIO)
      on delete restrict on update restrict;


alter table ELEMENTO_REQUERIMIENTO
   add constraint FK_ELEMENTO_REQUERIMIENTO_REFERENCE_REQUERIMIENTO foreign key (ID_REQUERIMIENTO)
      references  REQUERIMIENTO  (ID_REQUERIMIENTO)
      on delete restrict on update restrict;

alter table ELEMENTO_REQUERIMIENTO
   add constraint FK_ELEMENTO_REQUERIMIENTO_REFERENCE_NOMBRE_INSUMO foreign key (ID_NOMBRE_INSUMO)
      references  NOMBRE_INSUMO  (ID_NOMBRE_INSUMO)
      on delete restrict on update restrict;

alter table ELEMENTO_REQUERIMIENTO
   add constraint FK_ELEMENTO_REQUERIMIENTO_REFERENCE_MARCA foreign key (ID_MARCA)
      references  MARCA  (ID_MARCA)
      on delete restrict on update restrict;

alter table ELEMENTO_REQUERIMIENTO
   add constraint FK_ELEMENTO_REQUERIMIENTO_REFERENCE_PRESENTACION foreign key (ID_PRESENTACION)
      references PRESENTACION  (ID_PRESENTACION)
      on delete restrict on update restrict;


alter table ORDEN_COMPRA
   add constraint FK_ORDEN_COMPRA_REFERENCE_PROVEEDOR foreign key (ID_PROVEEDOR)
      references PROVEEDOR  (ID_PROVEEDOR)
      on delete restrict on update restrict;

alter table ORDEN_COMPRA
   add constraint FK_ORDEN_COMPRA_REFERENCE_USUARIO foreign key (ID_USUARIO_INGRESA)
      references USUARIO (ID_USUARIO)
      on delete restrict on update restrict;

alter table ELEMENTO_ORDEN_COMPRA
   add constraint FK_ORDEN_COMPRA_REFERENCE_ELEMENTO_REQUERIMIENTO foreign key (ID_ELEMENTO_REQUERIMIENTO)
      references ELEMENTO_REQUERIMIENTO (ID_ELEMENTO_REQUERIMIENTO)
      on delete restrict on update restrict;

ALTER TABLE public.ELEMENTO_ORDEN_COMPRA
  ADD CONSTRAINT FK_ORDEN_COMPRA_REFERENCE_ORDEN_COMPRA FOREIGN KEY (id_orden_compra)
      REFERENCES public.ORDEN_COMPRA (id_orden_compra) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


alter table PEDIDO
   add constraint FK_PEDIDO_REFERENCE_ORDEN_COMPRA foreign key (ID_ORDEN_COMPRA)
      references ORDEN_COMPRA (ID_ORDEN_COMPRA)
      on delete restrict on update restrict;

/*Procedimiento de cambio de estado cuando fecha de finalizacion sea inferior a la fecha actual el estado pasara a a finalizado*/
CREATE OR REPLACE FUNCTION CAMBIO_ESTADO_CONVENIO_Y_TRATAMIENTO_CONVENIO()
RETURNS trigger AS 
$BODY$
DECLARE
    registro convenio%ROWTYPE;
BEGIN
	FOR registro IN  select id_convenio from CONVENIO where to_char(CURRENT_DATE,'YYYY-MM-DD') > to_char(fecha_finalizacion,'YYYY-MM-DD') LOOP 
	    UPDATE CONVENIO SET estado = false WHERE id_convenio in( registro.id_convenio);	        
	    UPDATE TRATAMIENTO_CONVENIO SET estado = false WHERE id_convenio in( registro.id_convenio) ;	
	END LOOP;	
	RETURN NEW;
	
END $BODY$ LANGUAGE plpgsql;

CREATE TRIGGER TG_CAMBIO_ESTADO_CONVENIO_Y_TRATAMIENTO_CONVENIO AFTER INSERT OR UPDATE ON CONVENIO
	FOR EACH ROW 
	WHEN (pg_trigger_depth() = 0) --evitando la recursividad
	EXECUTE PROCEDURE CAMBIO_ESTADO_CONVENIO_Y_TRATAMIENTO_CONVENIO();


 /* 1. Habilitar la extensi\F3n pgcrypto para encriptar el password en la BD */
	CREATE EXTENSION pgcrypto;
    commit;
/*aLTERA SEQUENCIA DE LAS TABLAS Y LAS INICIA A 1*/
alter sequence EMPLEADO_id_seq restart with 1; 

/* Cagando datos iniciales a la BD*/
delete from permiso;

alter sequence permiso_id_permiso_seq restart with 1; 
/*Comando para importar datos de la base de datos */
copy perfil_permiso to '/var/log/postgresql/permisoPerfil.csv' delimiter ',' csv header;
/*Final comando*/


copy permiso(nombre,modulo) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/permisos.csv' WITH DELIMITER AS',';
copy perfil(nombre,estado) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/perfil.csv' WITH DELIMITER AS',';

copy perfil_permiso(id_permiso,id_perfil) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/permisoPerfil.csv' WITH DELIMITER AS',';

copy departamento(nombre) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/departamentos.csv' WITH DELIMITER AS',';

copy municipio(id,nombre,id_departamento) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/municipio.csv' WITH DELIMITER AS',';

copy especialidad(nombre) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/titulos_universitarios.csv' WITH DELIMITER AS',';

copy agenda_usuario(fecha_apertura,hora_inicio,hora_fin,dia,estado) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/AgendaUsuario.csv' WITH DELIMITER AS',';
 
copy tipo_documento(nombre,nomenclatura) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/tipoDocumento.csv' WITH DELIMITER AS',';

copy diagnostico_principal(nombre) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/diagnosticoPrincipal.csv' WITH DELIMITER AS',';

copy estado_mantenimiento(nombre) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/estadoMantenimiento.csv' WITH DELIMITER AS',';


copy clinica(nit,nombre,email,movil,estado_Sede) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/clinica.csv' WITH DELIMITER AS',';

copy usuario(tipo_identificacion,nombres,apellidos,numero_identificacion,
fecha_nacimiento,email,username,password,genero,id_agenda_usuario,
id_perfil,id_sucursal,color_agenda,direccion,fecha_apertura,movil,estado) FROM '/home/desarrollo/Documentos/Documentos/Desarrollo_Odontologia/DB_CLINICA/DatosIniciales/tipoDocumento.csv' WITH DELIMITER AS','; 


CREATE EXTENSION pgcrypto;
update usuario set password = crypt(cast('admin' as text), gen_salt('bf',12)) where id_usuario = 1;

/*   comando para generar orm de la base de datos 
php bin/console doctrine:mapping:convert xml src/AppBundle/Resources/Config/doctrines/ --from-database --force
*/



