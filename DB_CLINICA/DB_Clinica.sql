/*==============================================================*/
/* Table: USUARIO                                          */
/*==============================================================*/
CREATE TABLE public.usuario
(
  ID             SERIAL            not null,
  USERNAME       VARCHAR(150)      not null,
  ROLE           VARCHAR(150)      not null,
  PASSWORD 	 VARCHAR(255)      not null,
  CONSTRAINT PK_USUARIO PRIMARY KEY (id)
);

create unique index INDEX_UNQ_USERNAME_USUARIO on USUARIO (
USERNAME
);

/*==============================================================*/
/* Table: DEPARTAMENTO                                          */
/*==============================================================*/
create table DEPARTAMENTO (
   ID                   SERIAL               not null,
   NOMBRE               VARCHAR(150)         not null,
   constraint PK_DEPARTAMENTO primary key (ID)
);

/*==============================================================*/
/* Table: MUNICIPIO                                             */
/*==============================================================*/
create table MUNICIPIO (
   ID                   INT4                 not null,
   NOMBRE               VARCHAR(150)         not null,
   ID_DEPARTAMENTO      INT4                 not null,
   constraint PK_MUNICIPIO primary key (ID)
);

alter table MUNICIPIO add constraint FK_MUNICIPIO_REFERENCE_DEPARTAMENTO foreign key (ID_DEPARTAMENTO) references DEPARTAMENTO (ID) on delete restrict on update cascade;


/* 1. Habilitar la extensi\F3n pgcrypto para encriptar el password en la BD */
	CREATE EXTENSION pgcrypto;
    commit;
    

/* 2. Cambiar la codificaci\F3n del cliente para cargar archivos CSV */
	set client_encoding ='LATIN1';
    	commit;

copy departamento(nombre) FROM '/home/desarrollo/Documentos/Documentos/DatosIniciales/archivos_precargar/CARGAR_archivos_bd/departamento.csv' WITH DELIMITER AS',';


copy municipio(id,nombre,id_departamento) FROM '/home/desarrollo/Documentos/Documentos/DatosIniciales/archivos_precargar/CARGAR_archivos_bd/municipios.csv' WITH DELIMITER AS',';


/*aLTERA SEQUENCIA DE LAS TABLAS Y LAS INICIA A 1*/
alter sequence EMPLEADO_id_seq restart with 1;
/*BORRA CAMPOS DE LA TABLA QUE SE DESEE*/
DELETE FROM EMPLEADO;
	
