/*==============================================================*/
/* Table: USUARIO                                          */
/*==============================================================*/
CREATE TABLE public.usuario
(
  ID             SERIAL            not null,
  USERNAME       VARCHAR(150)      not null,
  ROLE           VARCHAR(150)      not null,
  PASSWORD 	 VARCHAR(255)      not null,
  CONSTRAINT PK_USUARIO PRIMARY KEY (id)
);

create unique index INDEX_UNQ_USERNAME_USUARIO on USUARIO (
USERNAME
);

/*==============================================================*/
/* Table: DEPARTAMENTO                                          */
/*==============================================================*/
create table DEPARTAMENTO (
   ID                   SERIAL               not null,
   NOMBRE               VARCHAR(150)         not null,
   constraint PK_DEPARTAMENTO primary key (ID)
);

/*==============================================================*/
/* Table: MUNICIPIO                                             */
/*==============================================================*/
create table MUNICIPIO (
   ID                   INT4                 not null,
   NOMBRE               VARCHAR(150)         not null,
   ID_DEPARTAMENTO      INT4                 not null,
   constraint PK_MUNICIPIO primary key (ID)
);

alter table MUNICIPIO add constraint FK_MUNICIPIO_REFERENCE_DEPARTAMENTO foreign key (ID_DEPARTAMENTO) references DEPARTAMENTO (ID) on delete restrict on update cascade;

alter table MUNICIPIO add constraint FK_MUNICIPIO_REFERENCE_DEPARTAMENTO foreign key (ID_DEPARTAMENTO) references DEPARTAMENTO (ID) on delete restrict on update cascade;


/*==============================================================*/
/* Table: PACIENTE                                            */
/*==============================================================*/
create table PACIENTE (
   ID_PACIENTE                   INT          	     	     not null,
   TIPO_IDENTIFICACION           INT4  		     	     not null,
   NUMERO_IDENTIFICACION	 VARCHAR(50)  		     not null,
   EXPEDICION_DOCUMENTO		 TEXT   		     not null,
   NOMBRES      		 VARCHAR(75)  		     not null,
   PRIMER_APELLIDO		 VARCHAR(75)  		     not null,
   SEGUNDO_APELLIDO		 VARCHAR(75)  		     not null,
   FECHA_NACIMIENTO 		 TIMESTAMP WITHOUT TIME ZONE not null,
   LUGAR_NACIMIENTO		 VARCHAR      		     not null,
   ESTADO_CIVIL		 	 VARCHAR(10)  		     not null,
   SEXO		 		 VARCHAR(20)  		     not null,
   EMAIL		 	 VARCHAR(75)                     null,
   DIRRECION_DOMICILIO		 VARCHAR(75)                 not null,
   TELEFONO_DOMICILIO		 VARCHAR(20)                 not null,
   ID_MUNICIPIO		 	 INT4      		     not null,
   CELULAR			 VARCHAR(150)		     not null,
   NOMBRE_P_RESPONSABLE		 VARCHAR(150) 		     not null,
   TELEFONO_PERSONA_RESPONSABLE  BIGINT      		     not null,
   PARENTESCO		 	 TEXT     		     not null,
   OCUPACION		 	 VARCHAR(150)		     not null,
   ID_CONVENIO		 	 INT4		     	 	 null,
   ID_EPS			 INT4			     not null,
   REGIMEN			 VARCHAR(50)		     not null,
   NOMBRE_ACOMPANANTE		 VARCHAR(150)		     not null,
   MOVIL_ACOMPANANTE		 BIGINT		     	     not null,
   FECHA_APERTURA		 TIMESTAMP WITHOUT TIME ZONE not null, 	    	   
   ESTADO		         BOOL		             not null,			  			
   constraint PK_PACIENTE primary key (ID_PACIENTE)
);

alter table PACIENTE add constraint FK_PACIENTE_REFERENCE_MUNICIPIO foreign key (ID_MUNICIPIO) references MUNICIPIO (ID) on delete restrict on update cascade;

///alter table PACIENTE add constraint FK_PACIENTE_REFERENCE_CONVENIO foreign key (ID_CONVENIO) references CONVENIO (ID_CONVENIO) on delete restrict on update cascade;//// comentado

alter table PACIENTE add constraint FK_PACIENTE_REFERENCE_EPS foreign key (ID_EPS) references EPS (ID_EPS) on delete restrict on update cascade;

alter table PACIENTE add constraint FK_PACIENTE_REFERENCE_TIPO_DOCUMENTO foreign key (TIPO_IDENTIFICACION) references TIPO_DOCUMENTO (ID) on delete restrict on update cascade;

/*==============================================================*/
/* Table: TIPO_DOCUMENTO                                          */
/*==============================================================*/
create table TIPO_DOCUMENTO (
   ID          	       SERIAL                not null,
   NOMBRE  	       VARCHAR(50)           not null,
   NOMENCLATURA        VARCHAR(20)           not null,
   constraint PK_TIPO_DOCUMENTO primary key (ID)
);


/*==============================================================*/
/* Table: EPS                                            */
/*==============================================================*/
create table EPS (
   ID_EPS                   SERIAL                 not null,
   NOMBRE                   VARCHAR(150)           not null,
   DESCRIPCION		    TEXT         	   null,
   ESTADO		    BOOL		   not null,
   TELEFONO		    VARCHAR(20)		   null,
   MOVIL		    INT8		   not null,
   NOMBRE_CONTACTO          VARCHAR(150)           not null,
   DIRECCION		    VARCHAR(150)	   not null,
   EMAIL		    VARCHAR(75)		   not null,
   ID_MUNICIPIO		    INT4		   not null,
   ID_SEDE		    INT4		   not null,
   constraint PK_EPS primary key (ID_EPS)
);

alter table EPS add constraint FK_EPS_REFERENCE_MUNICIPIO foreign key (ID_MUNICIPIO) references MUNICIPIO (ID) on delete restrict on update cascade;

alter table EPS add constraint FK_EPS_REFERENCE_EPS foreign key (ID_SEDE_EPS) references EPS (ID_EPS) on delete restrict on update cascade;

alter table public.eps add constraint UQ_eps_nombre unique (nombre);

select * from public.eps ORDER BY COALESCE(id_sede_eps, id_eps), id_eps, id_sede_eps;

// comentado// alter table EPS add constraint FK_EPS_REFERENCE_PACIENTE foreign key (ID_EPS) references PACIENTE (ID_PACIENTE) on delete restrict on update cascade; ////


/*==============================================================*/
/* Table: CONVENIO                                            */
/*==============================================================*/
create table CONVENIO (
   ID_CONVENIO          SERIAL               not null,
   NOMBRE               VARCHAR(150)         not null,
   PORCENTAJE_COMISION  INT4                 not null,
   ID_CLINICA           INT4         	     not null,
   FECHA_APERTURA	TIMESTAMP 	     not null,
   ID_EPS		INT4		     not null,
   PORCENTAJE_DESCUENTO INT4		     not null,
   OBSERVACION		TEXT		         null,
   ESTADO		BOOL		     not null,
   constraint PK_CONVENIO primary key (ID_CONVENIO)
);

alter table CONVENIO add constraint FK_CONVENIO_REFERENCE_EPS foreign key (ID_EPS) references EPS (ID_EPS) on delete restrict on update cascade;

alter table CONVENIO add constraint FK_CONVENIO_REFERENCE_CLINICA foreign key (ID_CLINICA) references CLINICA (ID_CLINICA) on delete restrict on update cascade;

// comentado// alter table CONVENIO add constraint FK_CONVENIO_REFERENCE_PACIENTE foreign key (ID_CONVENIO) references PACIENTE (ID_PACIENTE) on delete restrict on update cascade; ////


/*==============================================================*/
/* Table: CLINICA                                            */
/*==============================================================*/
create table CLINICA (
   ID_CLINICA          SERIAL                not null,
   NIT                 VARCHAR(150)          not null,
   NOMBRE  	       VARCHAR(150)          not null,
   DIRECCION           VARCHAR(150)              null,
   TELEFONO	       VARCHAR(20) 	         null,
   CELULAR	       INT4		     not null,
   EMAIL	       VARCHAR(75)	     not null,
   DIRECCION_WEB       VARCHAR(150)	         null,
   LOGO		       VARCHAR(150)	         null,
   ESTADO_SEDE	       BOOL		     not null,
   constraint PK_CLINICA primary key (ID_CLINICA)
);

alter table CLINICA add 
constraint FK_CLINICA_REFERENCE_EPS foreign key (ID_EPS)
 references EPS (ID_EPS) 
on delete restrict on update cascade;

alter table CLINICA add constraint FK_CLINICA_REFERENCE_CLINICA foreign key (ID_SEDE_CLINICA) references CLINICA (ID_CLINICA) on delete restrict on update cascade;

// comentado// alter table CONVENIO add constraint FK_CONVENIO_REFERENCE_PACIENTE foreign key (ID_CONVENIO) references PACIENTE (ID_PACIENTE) on delete restrict on update cascade; ////


/* 1. Habilitar la extensi\F3n pgcrypto para encriptar el password en la BD */
	CREATE EXTENSION pgcrypto;
    commit;
    

/* 2. Cambiar la codificaci\F3n del cliente para cargar archivos CSV */
	set client_encoding ='LATIN1';
    	commit;

copy departamento(nombre) FROM '/home/desarrollo/Documentos/Documentos/DatosIniciales/archivos_precargar/CARGAR_archivos_bd/departamento.csv' WITH DELIMITER AS',';


copy municipio(id,nombre,id_departamento) FROM '/home/desarrollo/Documentos/Documentos/DatosIniciales/archivos_precargar/CARGAR_archivos_bd/municipios.csv' WITH DELIMITER AS',';


/*aLTERA SEQUENCIA DE LAS TABLAS Y LAS INICIA A 1*/
alter sequence EMPLEADO_id_seq restart with 1;
/*BORRA CAMPOS DE LA TABLA QUE SE DESEE*/
DELETE FROM EMPLEADO;
	
