$(document).ready(function(){
	"use strict";
	
  var $validatorExamenGeneral= $('#formExamenGeneral').validate({
    rules: {
      'examen_general[motivoConsulta]':{
        required:true,
        minlength: 10,
        maxlength:2000
      },
      'examen_general[antecedenteMedico]':{          
        required:false,
        minlength: 1,
        maxlength:2000
      },
      'examen_general[antecedenteFamiliar]':{
        required:false,
        minlength: 1,
        maxlength:2000
      },
      'examen_general[medicamento]':{
        required:false,
        minlength: 1,
        maxlength:2000
      },
      'examen_general[alergia]':{
        required:false,
        minlength:1,
        maxlength:2000,
        letraNum:true
      },
      'examen_general[factorRiesgo]':{
        minlength: 1,
        maxlength:2000,
        required:false
      },
      'examen_general[determinanteSocial]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[poblacionVulnerable]':{
        minlength: 1,
        maxlength:2000,
        required:false
      },
      'examen_general[antecedenteSistemico]':{
        minlength: 1,
        maxlength:2000,
        required:false
      },
      'examen_general[habito]':{
        minlength: 1,
        maxlength:2000,
        required:false
      },
      'examen_general[otro]':{
        minlength: 4,
        maxlength:2000,
        required:false
      },
      'examen_general[embarazo]':{
        required:false
      },
      'examen_general[cara]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[ganglios]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[maxilarMandibula]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[musculo]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[labio]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[carillo]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[encia]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[lengua]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[pisoBocal]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[paladar]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[orfaringe]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[dolorMuscular]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[dolorArtilar]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[ruidoArtilar]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[alteracionMovimiento]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[malOclusion]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[crecimientoDesarrollo]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[fasetaDesgaste]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[nDienteTP]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[estadoEstructural]':{
        required:false
      },
      'examen_general[alteracionFlurosis]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[rehabilitacion]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[saliva]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[funcionTejido]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[placa]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },      
      'examen_general[remocionPlaca]':{
        required:false
      },
      'examen_general[fluor]':{
        required:false
      },
      'examen_general[detartraje]':{
        required:false
      },
      'examen_general[sellante]':{
        required:false
      },
      'examen_general[valoracionMedicina]':{
        required:false
      },
      'examen_general[educacionGrupal]':{
        required:false
      }
    }
  });
	$("#basicwizard").bootstrapWizard(),
	$("#progressbarwizard").bootstrapWizard({

		onNext:function(t,r,a){
			var $valid = $("#formExamenGeneral").valid();
			var o=$($(t).data("targetForm"));
			if(o&&(o.addClass("was-validated"),!1===o[0].checkValidity()) && !$valid ){
				
				return event.preventDefault(),event.stopPropagation(),!1
			}
	  		/*if(!$valid) {
	  			$validatorExamenGeneral.focusInvalid();
	  			//return false;
	  			return event.preventDefault(),event.stopPropagation(),!1
	  		}*/
			
		},
		onTabShow:function(t,r,a){
			var o=(a+1)/r.find("li").length*100;$("#progressbarwizard").find(".bar").css({width:o+"%"})					
			if (o == 100) {

				//alert('okis');
				$('.guardarExamanG').removeAttr('hidden');							
				$('.btnSiguiente').attr('hidden',true);
				$('.btnAnterior').removeAttr('hidden');
			}else if(o < 34){

				$('.btnAnterior').attr('hidden',true);
				$('.guardarExamanG').attr('hidden',true);
				$('.btnSiguiente').removeAttr('hidden');
			}else{
				$('.guardarExamanG').attr('hidden',true);
				$('.btnSiguiente').removeAttr('hidden');
				$('.btnAnterior').removeAttr('hidden');
			}
		}

	}),
	$("#btnwizard").bootstrapWizard({
		nextSelector:".button-next",previousSelector:".button-previous",firstSelector:".button-first",lastSelector:".button-last"
	}),
	$("#rootwizard").bootstrapWizard({
		onNext:function(t,r,a){var o=$($(t).data("targetForm"));
			if(o&&(o.addClass("was-validated"),!1===o[0].checkValidity()))return event.preventDefault(),event.stopPropagation(),!1}
		}
	)
});