$('#nestable_list_2').nestable('collapseAll');
!function(t){
	"use strict";
	var e=function(){};

	e.prototype.updateOutput=function(e){
		var a=e.length?e:t(e.target),n=a.data("output");
		window.JSON?n.val(window.JSON.stringify(a.nestable("serialize"))):n.val("JSON browser support required for this demo.")},
	e.prototype.init=function(){
		t("#nestable_list_1").nestable({group:1}).on("change",
			this.updateOutput),
		t("#nestable_list_2").nestable({group:1}).on("change",
			this.updateOutput),
		t("#nestable_list_3").nestable({group:1}).on("change",
			this.updateOutput),
		t("#nestable_list_4").nestable({group:1}).on("change",
			this.updateOutput),
		t("#nestable_list_5").nestable({group:1}).on("change",
			this.updateOutput),
		t("#nestable_list_6").nestable({group:1}).on("change",
			this.updateOutput),
		t("#nestable_list_7").nestable({group:1}).on("change",
			this.updateOutput),
		t("#nestable_list_8").nestable({group:1}).on("change",
			this.updateOutput),
		t("#nestable_list_9").nestable({group:1}).on("change",
			this.updateOutput),
		t("#nestable_list_10").nestable({group:1}).on("change",
			this.updateOutput),

		this.updateOutput(t("#nestable_list_1").data(
			"output",t("#nestable_list_1_output")
		)),
		this.updateOutput(t("#nestable_list_2").data(
			"output",t("#nestable_list_2_output")
		)),
		this.updateOutput(t("#nestable_list_3").data(
			"output",t("#nestable_list_3_output")
		)),
		this.updateOutput(t("#nestable_list_4").data(
			"output",t("#nestable_list_4_output")
		)),
		this.updateOutput(t("#nestable_list_5").data(
			"output",t("#nestable_list_5_output")
		)),
		this.updateOutput(t("#nestable_list_6").data(
			"output",t("#nestable_list_6_output")
		)),
		this.updateOutput(t("#nestable_list_7").data(
			"output",t("#nestable_list_7_output")
		)),
		this.updateOutput(t("#nestable_list_8").data(
			"output",t("#nestable_list_8_output")
		)),
		this.updateOutput(t("#nestable_list_9").data(
			"output",t("#nestable_list_9_output")
		)),
		this.updateOutput(t("#nestable_list_10").data(
			"output",t("#nestable_list_10_output")
		)),


		t("#nestable_list_menu").on("click",function(e){
			var a=t(e.target).data("action");
			"expand-all"===a&&t(".dd").nestable("expandAll"),
			"collapse-all"===a&&t(".dd").nestable("collapseAll")
		}),
		t("#nestable_list_3").nestable()
	},
	t.Nestable=new e,
	t.Nestable.Constructor=e
}(window.jQuery),function(t){
	"use strict";t.Nestable.init()
}(window.jQuery);