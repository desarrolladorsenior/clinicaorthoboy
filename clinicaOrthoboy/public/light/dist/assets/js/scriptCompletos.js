

/*textarea sirve para que n se deace as abajo */
/*$('input[type="datetime-local"]').val(
    moment(this).format("YYYY-MM-DDTkk:mm")
);
$('input[type="datetime-local"]').val(
    moment(this).format('dddd, MMMM Do YYYY, HH:mm')
);
console.log($('input[type="date-local"]').datetimepicker());*/
$(function(){
  setTextareaHeight($('textarea').css({'width':'100%','min-width':'100%'}));
});

function setTextareaHeight(textareas) {
    textareas.each(function () {
        var textarea = $(this);
 
        if ( !textarea.hasClass('autoHeightDone') ) {
            textarea.addClass('autoHeightDone');
 
            var extraHeight = parseInt(textarea.css('padding-top')) + parseInt(textarea.css('padding-bottom')), // to set total height - padding size
                h = textarea[0].scrollHeight - extraHeight;
 
            // init height
            textarea.height('auto').height(h);
 
            textarea.bind('keyup', function() {
 
                textarea.removeAttr('style'); // no funciona el height auto
 
                h = textarea.get(0).scrollHeight - extraHeight;
 
                textarea.height(h+'px'); // set new height
            });
        }
    })
}
/*Manejo de subir archivos en toda la plantilla*/
$(function() {
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "No es un archivo valido",
        placement:'bottom'
    });
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text("Archivo"); 
    }); 
    $(".image-preview-input input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });     
        var file = this.files[0];
        var reader = new FileReader();
        var files = this.files;
        //Cantidad Maxima de Archivos
        if (files.length <= 1) {
           var conteoSize = 0;
        //Manejo de peso de archivos que se estan seleccionando
        for(var i=0;i<this.files.length;i++) {
            conteoSize += (this.files[i].size);
        }
        if (conteoSize<=1000000) {
             //alert("files"+conteoSize);
             reader.onload = function (e) {
            $(".image-preview-input-title").text("Cambio");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            }        
            reader.readAsDataURL(file);
        }else{
            //conversion de bytes a megaBytes
            conversion=conteoSize/1000000;
            //maneja restriccion decimales
            var numMB = conversion.toFixed(1); 

            //Mensaje de error por causa de exceso de archivos
             swal({
                title: "¿Error?",
                text: "El peso de los archivos superan las expectativas; Recuerde que solo se permite un tamaño máximo de 1MB, en el momento se registra actividad del archivo con peso de '"+numMB+"' MB.",
                type: 'warning',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK!',
                allowEscapeKey: false,
                allowOutsideClick: false
                 });
                $('.image-preview-input input:file').val("");
                $(".image-preview-filename").val(""); 
              return false; 
        }       
        
        }else{
            //Mensaje de error superar la cantidad de archivos a cargar
             swal({
                title: "¿Error?",
                text: "No se pueden cargar mas de 1 archivo; Usted tiene '"+files.length+"'",
                type: 'warning',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK!',
                allowEscapeKey: false,
                allowOutsideClick: false
                 });
                $('.image-preview-input input:file').val("");
                $(".image-preview-filename").val(""); 
              return false; 
        }
    });
});
//Anexo externo o anexo normal 
$(function() {
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
        content: "No es un archivo valido",
        placement:'bottom'
    });
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').val("");
        $('.image-preview-clear').hide();
        $('.image-preview-input-anexo input:file').val("");
        $(".image-preview-input-title").text("Archivo"); 
    }); 
    $(".image-preview-input-anexo input:file").change(function (){     
        var img = $('<img/>', {
            id: 'dynamic',
            width:250,
            height:200
        });     
        var file = this.files[0];
        var reader = new FileReader();
        var files = this.files;
        //Cantidad Maxima de Archivos
        if (files.length <= 1) {
           var conteoSize = 0;
        //Manejo de peso de archivos que se estan seleccionando
        for(var i=0;i<this.files.length;i++) {
            conteoSize += (this.files[i].size);
        }
        if (conteoSize<=25000000) {
             //alert("files"+conteoSize);
             reader.onload = function (e) {
            $(".image-preview-input-title").text("Cambio");
            $(".image-preview-clear").show();
            $(".image-preview-filename").val(file.name);            
            img.attr('src', e.target.result);
            }        
            reader.readAsDataURL(file);
        }else{
            //conversion de bytes a megaBytes
            conversion=conteoSize/1000000;
            //maneja restriccion decimales
            var numMB = conversion.toFixed(1); 

            //Mensaje de error por causa de exceso de archivos
             swal({
                title: "¿Error?",
                text: "El peso de los archivos superan las expectativas; Recuerde que solo se permite un tamaño máximo de 25MB, en el momento se registra actividad del archivo con peso de '"+numMB+"' MB.",
                type: 'warning',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK!',
                allowEscapeKey: false,
                allowOutsideClick: false
                 });
                $('.image-preview-input-anexo input:file').val("");
                $(".image-preview-filename").val(""); 
              return false; 
        }       
        
        }else{
            //Mensaje de error superar la cantidad de archivos a cargar
             swal({
                title: "¿Error?",
                text: "No se pueden cargar mas de 1 archivo; Usted tiene '"+files.length+"'",
                type: 'warning',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK!',
                allowEscapeKey: false,
                allowOutsideClick: false
                 });
                $('.image-preview-input-anexo input:file').val("");
                $(".image-preview-filename").val(""); 
              return false; 
        }
    });
});
/*QUitando clases para que sirva correctamente los archivos el ingreso*/
$('div').removeClass('custom-file');
$('label').removeClass('custom-file-label');
$('input').removeClass('custom-file-input');

$(document).ready(function(){
    /*Clinica campos*/
    var nit= $('#clinica_nit').val();
    if (typeof(nit) != "undefined") {
        if (nit != '') {
            var separacion= nit.split("-");
            $('#niClinica').val(separacion[0]);
            $('#numId').val(separacion[1]);
        }
    }    
    var telefono= $('#clinica_telefono').val();
    if (typeof(telefono) != "undefined") {
        if (telefono != '') {
            var separacion= telefono.split("-");
            $('#indicativo').val(separacion[0]);
            $('#telefono').val(separacion[1]);
        }
    }    

    /*Datos Perfil*/
    var nombrePerfil= $('#perfil_nombre').val();
    if (typeof(nombrePerfil) != "undefined") {
        if (nombrePerfil != '') {
            var separacion= nombrePerfil.split("_");
            $('#perfil_nombre').val(separacion[1]);
        }
    }  

    /*eps campos*/  
    var nitEps= $('#eps_nit').val();
    if (typeof(nitEps) != "undefined") {
        if (nitEps != '') {
            var separacion= nitEps.split("-");
            $('#niEps').val(separacion[0]);
            $('#numId').val(separacion[1]);
        }
    }   

    var nitEpsEdit= $('#formEditEps').children().children('input#eps_nit').val();
    if (typeof(nitEpsEdit) != "undefined") {
        if (nitEpsEdit != '') {
            var separacion= nitEpsEdit.split("-");
            $('#formEditEps').children().children().children('#niEps').val(separacion[0]);
            $('#formEditEps').children().children().children('#numId').val(separacion[1]);
        }
    }   

    //formEditEps

    var telefonoEps= $('#eps_telefono').val();
    if (typeof(telefonoEps) != "undefined") {
        if (telefonoEps != '') {

            var separacion= telefonoEps.split("-");
            $('#indicativo').val(separacion[0]);
            $('#telefono').val(separacion[1]);
        }
    }  

    var telefonoEpsEdit= $('#formEditEps').children().children('input#eps_telefono').val();
    if (typeof(telefonoEpsEdit) != "undefined") {
        if (telefonoEpsEdit != '') {

            var separacion= telefonoEpsEdit.split("-");
            $('#formEditEps').children().children().children('#indicativo').val(separacion[0]);
            $('#formEditEps').children().children().children('#telefono').val(separacion[1]);
        }
    }  
    var telefonoNuevoSedeEdit= $('#formEditEpsSede').children().children('input#eps_telefono').val();
    if (typeof(telefonoNuevoSedeEdit) != "undefined") {
        if (telefonoNuevoSedeEdit != '') {

            var separacion= telefonoNuevoSedeEdit.split("-");
            $('#formEditEpsSede').children().children().children('#indicativo').val(separacion[0]);
            $('#formEditEpsSede').children().children().children('#telefono').val(separacion[1]);
        }
    }   


    /*proveedor campos*/  
    var nitProveedor= $('#proveedor_identificacion').val();
    if (typeof(nitProveedor) != "undefined") {
        if (nitProveedor != '') {
            var separacion= nitProveedor.split("-");
            $('#niProveedor').val(separacion[0]);
            $('#numIdProveedor').val(separacion[1]);
        }
    }   

    var nitProveedorEdit= $('#formProveedorEdit').children().children('input#proveedor_identificacion').val();
    if (typeof(nitProveedorEdit) != "undefined") {
        if (nitProveedorEdit != '') {
            var separacion= nitProveedorEdit.split("-");
            $('#formProveedorEdit').children().children().children('input#niProveedor').val(separacion[0]);
            $('#formProveedorEdit').children().children().children('input#numIdProveedor').val(separacion[1]);
        }
    }   

    //formEditProveedor

    var telefonoProveedor= $('#proveedor_telefono').val();
    if (typeof(telefonoProveedor) != "undefined") {
        if (telefonoProveedor != '') {

            var separacion= telefonoProveedor.split("-");
            $('#indicativoProvee').val(separacion[0]);
            $('#telefonoProvee').val(separacion[1]);
        }
    }  

    var telefonoProveedorEdit= $('#formProveedorEdit').children().children('input#proveedor_telefono').val();
    if (typeof(telefonoProveedorEdit) != "undefined") {
        if (telefonoProveedorEdit != '') {

            var separacion= telefonoProveedorEdit.split("-");
            $('#formProveedorEdit').children().children().children('input#indicativoProvee').val(separacion[0]);
            $('#formProveedorEdit').children().children().children('input#telefonoProvee').val(separacion[1]);
        }
    }  

    var telefonoPaciente= $("input[name='paciente[telefonoDomicilio]']").val();
    if (typeof(telefonoPaciente) != "undefined") {
        if (telefonoPaciente != '') {
            var separacion= telefonoPaciente.split("-");
            $("input[name='indicativo']").val(separacion[0]);
            $("input[name='telefono']").val(separacion[1]);
        }
    }  

});


/*Manejo configuracion perfiles*/
   
    var contadorEdit=0;
    $('.configuracionEdit').click(function(){
        contadorEdit=contadorEdit+1;
        if (contadorEdit ==1 ) {
            $('.permisos').removeAttr('hidden');
            $('.dd').nestable('expandAll');
        }else if(contadorEdit==2){
            $('.permisos').attr('hidden', 'true');
            $('.dd').nestable('expandAll');
            contadorEdit=0;

        }
    });

    var contador = 0;    
    $('.configuracion').click(function(){
        contador=contador+1;
        if (contador ==1 ) {
            $('.permisos').removeAttr('hidden');
            $('.dd').nestable('collapseAll');
        }else if(contador==2){
            $('.permisos').attr('hidden', 'true');
            $('.dd').nestable('expandAll');
            contador=0;

        }
    });
    var contador2 = 0; 
    $('.configuracionMediaPaciente').click(function(){
        contador2=contador2+1;
        if (contador2 ==1 ) {
            $('.permisosMedioPaciente').removeAttr('hidden');
        }else if(contador2==2){
            $('.permisosMedioPaciente').attr('hidden', 'true');
            contador2=0;

        }
    });

    var contador3 = 0; 
    $('.configuracionMediaAdministracion').click(function(){
        contador3=contador3+1;
        if (contador3 ==1 ) {
            $('.permisosMedioAdministracion').removeAttr('hidden');
        }else if(contador3==2){
            $('.permisosMedioAdministracion').attr('hidden', 'true');
            contador3=0;

        }
    });

    var contador4 = 0; 
    $('.configuracionMediaReportes').click(function(){
        contador4=contador4+1;
        if (contador4 ==1 ) {
            $('.permisosMedioReportes').removeAttr('hidden');
        }else if(contador4==2){
            $('.permisosMedioReportes').attr('hidden', 'true');
            contador4=0;

        }
    });

     var contador5 = 0; 
    $('.configuracionMediaConfiguracion').click(function(){
        contador5=contador5+1;
        if (contador5 ==1 ) {
            $('.permisosMedioConfiguracion').removeAttr('hidden');
        }else if(contador5==2){
            $('.permisosMedioConfiguracion').attr('hidden', 'true');
            contador5=0;

        }
    });

     var contador6 = 0; 
    $('.configuracionMediaAgenda').click(function(){
        contador6=contador6+1;
        if (contador6 ==1 ) {
            $('.permisosMedioAgenda').removeAttr('hidden');
        }else if(contador6==2){
            $('.permisosMedioAgenda').attr('hidden', 'true');
            contador6=0;

        }
    });

/**Administracion de checkbox*/    
$(function() {

  $('input[type="checkbox"]').change(checkboxChanged);

  function checkboxChanged() {
    var $this = $(this),
        checked = $this.prop("checked"),
        container = $this.parent(),
        siblings = container.siblings();

    container.find('input[type="checkbox"]')
    .prop({
        indeterminate: false,
        checked: checked
    })
    .siblings('label')
    .removeClass('custom-checked custom-unchecked custom-indeterminate')
    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

    checkSiblings(container, checked);
  }

  function checkSiblings($el, checked) {
    var parent = $el.parent().parent(),
        all = true,
        indeterminate = false;

    $el.siblings().each(function() {
      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
    });

    if (all && checked) {
      parent.children('input[type="checkbox"]')
      .prop({
          indeterminate: false,
          checked: checked
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(checked ? 'custom-checked' : 'custom-unchecked');

      checkSiblings(parent, checked);
    } 
    else if (all && !checked) {
      indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

      parent.children('input[type="checkbox"]')
      .prop("checked", checked)
      .prop("indeterminate", indeterminate)
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

      checkSiblings(parent, checked);
    } 
    else {
      $el.parents("li").children('input[type="checkbox"]')
      .prop({
          indeterminate: true,
          checked: false
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass('custom-indeterminate');
    }
  }
});