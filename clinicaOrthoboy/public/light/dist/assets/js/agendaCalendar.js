$(document).ready(function() {
  var initialLocaleCode = 'es';

  $('#calendar').fullCalendar({
     header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listMonth'
    },
    views: {
      listMonth: {buttonText: 'Agenda Mes'}
    },
    buttonIcons: false, // muestra  texto anterior y siguiente
    weekNumbers: true,
    navLinks: true, // Da Click permitiendo dia/semana para la navegacion
    businessHours: true, // muestra la hora de cada evento
    defaultView: 'month', //Vista en la que inicia
    displayEventTime:false,
    timezone: ('America/Bogota'),
    toLocaleDateString:'UTC',
    locale: 'es',
    ignoreTimezone: true,
    axisFormat: 'H(:mm)',
    allDaySlot: false,
    eventOverlap: false, // will cause the event to take up entire resource height
    editable: true,
    selectable: true,
    eventLimit: true, // permitir "más" enlaces cuando demasiados eventos
    events: [],
  });
});