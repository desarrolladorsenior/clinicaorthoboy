$(document).ready(function(){
  var validatorLogin = $('#form_login').validate({
    rules: {
      '_username':{
        minlength:1,
        maxlength:30
      },
      '_password':{
        minlength:8,
        maxlength:8
      }
    },
    submitHandler: function(form) {     
      $(form)[0].submit();    
    }
  });

  var validatorRecuperacion=$('#form_recuperacion').validate({
    rules: {
      'correoVerificacion':{
        minlength:10,
        maxlength:75,
        email:true
      }
    },
    submitHandler: function(form) {     
      $(form)[0].submit();    
    }
  });

  var validatorVerificacion=$('#form_verificacion').validate({
    rules: {
      'codigo':{
        minlength:5,
        maxlength:5
      }
    },
    submitHandler: function(form) {     
      $(form)[0].submit();    
    }
  });

  $.validator.addMethod('caracter', function(value, element, param){
      if (/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])\S{8,8}$/i.test(value)){
       return true;
      } else {
       return false;
      } 
  }, 'Este campo debe contener mínimo un caracter especial, un número y un caracter en mayúscula.');

  $.validator.addMethod('igualdad', function(value, element, param){
   // alert($('#passwordConfir').val());
    if (typeof($('#clave').val()) != "undefined") {
      valorActual=$('#clave').val();
    }else{
      valorActual=$('#passwordConfir').val();
    }
      
      if (valorActual == value){
       return true;
      } else {
       return false;
      } 
  }, 'Los campos no coinciden.');

  $.validator.addMethod('fechaMayor', function(value, element, param){

      valorAperturaCrear= new Date($('#convenio_fechaApertura').val());
      valorAperturaEdicion= new Date($('#formConvenioEdit').children().children().children('input#convenio_fechaApertura').val());
      valorApertura2= new Date($('#fechaAperturaConvenio').val());
      valorFinalizacion= new Date($('#fechaFinalizacionConvenio').val());
      if ($('#convenio_fechaApertura').val()!= "") {
        valorActual= new Date(value);
        if (valorActual > valorAperturaCrear){
         return true;
        } else {
         return false;
        } 
      }
      else if ($('#formConvenioEdit').children().children().children('input#convenio_fechaApertura').val() != "" && typeof($('#formConvenioEdit').children().children().children('input#convenio_fechaApertura').val()) != "undefined") {
        valorActual= new Date(value);
        if (valorActual > valorAperturaEdicion){
         return true;
        } else {
         return false;
        } 
      }
      else if ($('#fechaAperturaConvenio').val() != "") {
        valorActual= new Date(value);
        if(valorActual > valorApertura2 && valorActual < valorFinalizacion){
         return true;
        }
        else if(valorActual > valorApertura2 && valorActual === valorFinalizacion ){
          return true;
        } 
        else {
         return false;
        } 
      }
      
  }, 'Este campo debe ser mayor que la fecha de apertura o finalización del convenio .');

   $.validator.addMethod('fechaMayorIgualMante', function(value, element, param){
      valorFechaProgramadaNuevo= new Date($('#formMantenimiento').children().children().children('input#mantenimiento_fechaProgramada').val());
      valorFechaProgramadaEdit= new Date($('#formMantenimientoEdit').children().children().children('input#mantenimiento_fechaProgramada').val());
      valorActual= new Date(value);
      //alert(        $('#formMantenimientoEdit').children().children().children('input#mantenimiento_fechaProgramada').val());
      if ($('#formMantenimiento').children().children().children('input#mantenimiento_fechaProgramada').val() !=  "" && value != '') {
        if (valorActual >= valorFechaProgramadaNuevo){
         return true;
        } else {
         return false;
        } 
      }else if ($('#formMantenimientoEdit').children().children().children('input#mantenimiento_fechaProgramada').val() !=  "" && value != '') {
        if (valorActual >= valorFechaProgramadaEdit){
         return true;
        } else {
         return false;
        } 
      }else{
        return true;
      }       
  }, 'Este campo debe ser mayor que la fecha de apertura o finalización del convenio .');


//Lote entrada pedido//
 $.validator.addMethod('lotePedido', function(value, element, param){
      if (/^[A-Za-z0-9-]*$/i.test(value)){
       return true;
      } else {
       return false;
      } 
  }, 'Este campo puede contener números, letras y guiones.');
 //Letras y numeros unicamente
 $.validator.addMethod('letraNum', function(value, element, param){
      if (/^[A-Za-z0-9]*$/i.test(value)){
       return true;
      } else {
       return false;
      } 
  }, 'Este campo puede contener números y letras.');

  $.validator.addMethod('maxApproval', function(value, element, param){
    valorDato= new Date(value);
    fechaActual= new Date();
    if(valorDato <= fechaActual){
     return true;
    }
    else {
     return false;
    } 
  }, 'Este campo debe ser menor o igual a la fecha actual.');

  var validatorRecuperarClave=$('#form_cambioClave').validate({
    rules: {
      'clave':{
        minlength:8,
        maxlength:8,
        caracter:true
      },
      'confimarClave':{
        minlength:8,
        maxlength:8,
        caracter:true,
        igualdad:true
      }
    },
    submitHandler: function(form) { 
      $(form)[0].submit();    
    }
  });



  var validatorRegistro = $('#formRegistroClinica').validate({
    rules: {
      'niClinica':{
        minlength: 7,
        maxlength:15,
        digits:true
      },
      'numId':{
        minlength: 1,
        maxlength:1,
        digits:true,
        required:false
      },
      'form[nombre]':{
        minlength: 3,
        maxlength:150
      },
      'form[email]':{
        minlength: 10,
        maxlength:75,
        email:true,
      },
      'form[movil]':{
        minlength: 10,
        maxlength:15,
        digits:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      var nit=$('#niClinica').val();      
      var numeroVerificacion=$('#numId').val();
      if (numeroVerificacion != '') {
        var valorNIT=nit+'-'+numeroVerificacion;
        $(form).children().children('input#form_nit').val(valorNIT);    
      }else{
        $(form).children().children('input#form_nit').val(nit);    
      }
      $(form)[0].submit();
      
    }
  });

  var validator = $('#formGeneralPR').validate({
    rules: {
      'tipo_radiografia[nombre]':{
        minlength: 3,
        maxlength:150
      },
      'tipo_radiografia[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
      
    }
  });

  var validator2 = $('#formGeneralTR').validate({
      rules: {
        'tipo_radiografia[nombre]':{
          minlength: 3,
          maxlength:150
        },
        'tipo_radiografia[idSedeTipoRadiografia]':{
          required: true
        },
        'tipo_radiografia[descripcion]':{
          required: false,
          minlength: 3,
          maxlength:2000
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        var idT =$('#idTipoR').val();
        if (typeof(idT) != "undefined") {
          $(form).attr('action', "/tipoRadiografia/"+idT+"/edit");
          $(form)[0].submit();    
        }else{
          $(form).attr('action', "/tipoRadiografia/newSubtipo");
          $(form)[0].submit();
        } 
        
        
      }
    });
});
$(document).ready(function(){
  var validator3 = $('#formEditPR').validate({
      rules: {
        'tipo_radiografia[nombre]':{
          minlength: 3,
          maxlength:150
        },
        'tipo_radiografia[descripcion]':{
          required: false,
          minlength: 3,
          maxlength:2000
        },
        'tipo_radiografia[idSedeTipoRadiografia]':{
          required: false
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        var idT =$('#idTipoR').val();
        $(form).attr('action', "/tipoRadiografia/"+idT+"/edit");
        $(form)[0].submit();
        
      }
    });

    var validatorEditarClinica= $('#formEditarClinica').validate({
      rules: {
        'clinica[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'niClinica':{
          minlength: 7,
          maxlength:15,
          digits:true
        },
        'numId':{
          minlength: 1,
          maxlength:1,
          digits:true,
          required:false
        },
        'clinica[direccion]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'indicativo':{
          minlength: 3,
          maxlength:3,
          digits:true,
          required:false
        },
        'telefono':{
          minlength: 7,
          maxlength:7,
          digits:true,
          required:false
        },
        'clinica[movil]':{
          minlength: 10,
          maxlength:15,
          digits:true
        },
        'clinica[email]':{
          minlength: 10,
          maxlength:75,
          email:true,
        },
        'clinica[idMunicipio]':{
          required:true
        },
        'clinica[direccionWeb]':{
          minlength: 10,
          maxlength:150,
          required:false
        },
      },
      submitHandler: function(form) {
        var idClinica = $('#idClinica').val();
        var nit=$('#niClinica').val();
        var numeroVerificacion=$('#numId').val();
        if (numeroVerificacion != '') {
          var valorNIT=nit+'-'+numeroVerificacion;
          $(form).children().children('input#clinica_nit').val(valorNIT); 
          
        }else{
          $(form).children().children('input#clinica_nit').val(nit);    
        }             
        var indicativo= $('#indicativo').val();
        var telefono=   $('#telefono').val();
        if (telefono != '' && indicativo != '') {
          var valorTelefono=indicativo+'-'+telefono;
          $(form).children().children('input#clinica_telefono').val(valorTelefono);   

          $('body').addClass('loading');
          $(form).attr('action', "/clinica/"+idClinica+"/edit");
          $(form)[0].submit();        
        }else if(telefono != '' || indicativo != ''){
          $('.telclinica').html('Registre el indicativo y el número telefónico.');
        }else{
          $('body').addClass('loading');
          $(form).attr('action', "/clinica/"+idClinica+"/edit");
          $(form)[0].submit();
        }
        
      }
    });   
});
$(document).ready(function(){
  var validatorCrearSedeClinica= $('#formCrearSede').validate({
      rules: {
        'clinica[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'clinica[direccion]':{
          required: false,
          minlength: 3,
          maxlength:150,
          required:true
        },
        'indicativo':{
          minlength: 3,
          maxlength:3,
          digits:true,
          required:false
        },
        'telefono':{
          minlength: 7,
          maxlength:7,
          digits:true,
          required:false
        },
        'clinica[movil]':{
          minlength: 10,
          maxlength:15,
          digits:true
        },
        'clinica[email]':{
          minlength: 10,
          maxlength:75,
          email:true,
        },
        'clinica[idMunicipio]':{
          required:true
        },
        'clinica[direccionWeb]':{
          minlength: 10,
          maxlength:150,
          required:false
        },
      },
      submitHandler: function(form) {        
        var indicativo= $('#indicativo').val();
        var telefono=   $('#telefono').val();
        if (telefono != '' && indicativo != '') {
          var valorTelefono=indicativo+'-'+telefono;
          $(form).children().children('input#clinica_telefono').val(valorTelefono);  

          $('body').addClass('loading');
          $(form).attr('action', "/clinica/newSedeClinica");
          $(form)[0].submit();         
        }else if(telefono != '' || indicativo != ''){
          $('.telsede').html('Registre el indicativo y el número telefónico.');
        }else{
          $('body').addClass('loading');
          $(form).attr('action', "/clinica/newSedeClinica");
          $(form)[0].submit();
        }
      }
    });

  var validatorPerfil = $('#formPerfil').validate({
      rules: {
        'perfil[nombre]':{
          minlength: 3,
          maxlength:150
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        var nombre =$('#perfil_nombre').val();
        var nombreCompleto='ROLE_'+nombre;
        $(form).children().children().children('input#perfil_nombre').val(nombreCompleto);
        $(form)[0].submit();
        
      }
    });

  var validatorPerfilPermiso = $('#form_permiso_perfil').validate({
      submitHandler: function(form) {
        //console.log($(form).serialize());
        if ($(form).serialize() != "") {
          $('body').addClass('loading');      
          $(form)[0].submit();    
        }else{
          swal("", "Debe seleccionar mínimo una función para el perfil.", "warning");
          return false;
        }
      }
    });
  var validatorPerfilPermisoEdit = $('#form_permiso_perfil_edit').validate({
      submitHandler: function(form) {
        if ($(form).serialize() != "") {
          $('body').addClass('loading');      
          $(form)[0].submit();    
        }else{
          swal("", "Debe seleccionar mínimo una función para el perfil.", "warning");
          return false;
        }
      }
    });
});
$(document).ready(function(){
  var validatorConsultorio= $('#formConsultorio').validate({
    rules: {
        'consultorio[nombre]':{
          minlength: 3,
          maxlength:150
        },
        'consultorio[idClinica]':{
          required: true
        },
        'consultorio[idUsuarioIngresa]':{
          required: true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
          var url="/consultorio/new";
          $.ajax({
            type: 'POST',
            url:url,
            data:$(form).serialize(),
            beforeSend: function () {
              $('body').addClass('loading'); //Agregamos la clase loading al body
            },
            success: function(respuesta){
              $('body').removeClass('loading');
              if (respuesta.mensaje) {
                $('.respConsultorioError').attr('hidden',true);
                $('.respConsultorioMensaje').removeAttr('hidden');
                $('.respConsultorioMensaje').html(respuesta.mensaje);    
                setTimeout(function(){ location.reload();  }, 3000);
              }else if(respuesta.error){
                $('.respConsultorioError').removeAttr('hidden');
                $('.respConsultorioError').html(respuesta.error);    
              }
            },
       
            // código a ejecutar si la petición falla;
            error : function(xhr, status) {
              $('body').removeClass('loading');
              $('.respConsultorio').removeAttr('hidden');
              $('.respConsultorio').html('Disculpe, existió un problema en la conexión.');
            },
          }); 
      }
  });

  var validatorConsultorioEdit= $('#formConsultorioEdit').validate({
    rules: {
      'consultorio[nombre]':{
        minlength: 3,
        maxlength:150
      },
      'consultorio[idClinica]':{
        required: true
      },
      'consultorio[idUsuarioIngresa]':{
          required: true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      var idConsultorio=$('#idConsultorio').val();
      var url="/consultorio/"+idConsultorio+"/editar";
      $.ajax({
        type: 'POST',
        url:url,
        data:$(form).serialize(),
        beforeSend: function () {
          $('body').addClass('loading'); //Agregamos la clase loading al body
        },
        success: function(respuesta){
          $('body').removeClass('loading');
          if (respuesta.mensaje) {
            $('.respConsultorioError').attr('hidden',true);
            $('.respConsultorioMensaje').removeAttr('hidden');
            $('.respConsultorioMensaje').html(respuesta.mensaje);    
            setTimeout(function(){ location.reload();  }, 3000);
          }else if(respuesta.error){
            $('.respConsultorioError').removeAttr('hidden');
            $('.respConsultorioError').html(respuesta.error);    
          }
        },
   
        // código a ejecutar si la petición falla;
        error : function(xhr, status) {
          $('body').removeClass('loading');
          $('.respConsultorio').removeAttr('hidden');
          $('.respConsultorio').html('Disculpe, existió un problema en la conexión.');
        },
      });   
    }
  });
  var validatorCrearAnexoExterno = $('#formCrearAnexoExterno').validate({
      rules: {
        'anexo_paciente[nombre]':{
          minlength: 3,
          maxlength:150
        },
        'anexo_paciente[descripcion]':{
          required: false,
          minlength: 3,
          maxlength:2000
        },
        'anexo_paciente[idPaciente]':{
          required: false
        },
        'anexo_paciente[idUsuarioIngresa]':{
          required: false
        },
        'anexo_paciente[idRemision]':{
          required: false
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });
});

$(document).ready(function(){
  var validatorCrearRemision = $('#crearform_remision').validate({
    rules: {
        'tipoRemision':{
           required: true
        },
        'remision[idUsuarioRemitido]':{
          required: false
        },
        'remision[email]':{
          required: false,
          minlength: 10,
          maxlength:75,
          email:true
        },
        'remision[idProcedimiento]':{
          required: true
        },
        'remision[observacion]':{
          minlength: 3,
          maxlength: 2000,
          required: true
        },
        'remision_fechaRealizar_final':{
          required: true
        }
    },
    submitHandler: function(form) {
      if ($('#tipoRemision').val() == 'INTERNA') {
        if ($('#remision_idUsuarioRemitido').val() == '') {
          $('.usuarioRemi').html('Este campo es obligatorio.');
        }else{
          $('#remision_email').val();
        }
      }else{
        if ($('#remision_email').val() == '') {
         $('.emailRemi').html('Este campo es obligatorio.');
        }else{
          $('#remision_idUsuarioRemitido').val('');
        }
        
      }      
      $('body').addClass('loading');
      $(form)[0].submit();
      
    }
  });

  var validatorFinalidadConsulta= $('#formFinalidadConsulta').validate({
    rules: {
        'finalidad_consulta[nombre]':{
          minlength: 3,
          maxlength:150,
          required: true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();  
      }
  });

  var validatorFinalidadConsultaEditar= $('#formFinalidadConsultaEdit').validate({
    rules: {
        'finalidad_consulta[nombre]':{
          minlength: 3,
          maxlength:150,
          required: true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/finalidadConsulta/'+$('#idFinalidadConsulta').val()+'/edit')
        $(form)[0].submit();  
      }
  });

  var validatorMotivoConsulta= $('#formMotivoConsulta').validate({
    rules: {
        'motivo_consulta[nombre]':{
          minlength: 3,
          maxlength:150,
          required: true
        },
        'motivo_consulta[tiempoEstimado]':{
          minlength: 1,
          maxlength:5,
          required: true,
          digits:true
        },
        'motivo_consulta[idProcedimiento]':{
          required: true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();  
      }
  });

  var validatorMotivoConsultaEditar= $('#formMotivoConsultaEdit').validate({
    rules: {
        'motivo_consulta[nombre]':{
          minlength: 3,
          maxlength:150,
          required: true
        },
        'motivo_consulta[tiempoEstimado]':{
          minlength: 3,
          maxlength:5,
          required: true,
          digits:true
        },
        'motivo_consulta[idProcedimiento]':{
          required: true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/motivoConsulta/'+$('#idMotivoConsulta').val()+'/edit');
        $(form)[0].submit();  
      }
  });

  var validatorAreaMedica= $('#formArea').validate({
    rules: {
        'area[nombre]':{
          minlength: 3,
          maxlength:150,
          required: true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();  
      }
  });

  var validatorAreaMedicaEditar= $('#formAreaMedicaEdit').validate({
    rules: {
        'area[nombre]':{
          minlength: 3,
          maxlength:150,
          required: true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/area/'+$('#idAreaMedica').val()+'/edit')
        $(form)[0].submit();  
      }
  });
});

$(document).ready(function(){
  var validatorEspecialidad= $('#formEspecialidad').validate({
    rules: {
        'especialidad[nombre]':{
          minlength: 3,
          maxlength:150,
          required: true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();  
      }
  });

  var validatorEspecialidadEditar= $('#formEspecialidadEdit').validate({
    rules: {
        'especialidad[nombre]':{
          minlength: 3,
          maxlength:150,
          required: true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/especialidad/'+$('#idEspecialidad').val()+'/edit')
        $(form)[0].submit();  
      }
  });


  var validatorEpsCrear = $('#form_epscrear').validate({
    rules: {
      'eps[nombre]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'niEps':{
          minlength: 7,
          maxlength:15,
          digits:true
        },
      'numId':{
        minlength: 1,
        maxlength:1,
        digits:true
      },
      'eps[direccion]':{
        required: true,
        minlength: 6,
        maxlength:150
      },
      'indicativo':{
        minlength: 3,
        maxlength:3,
        digits:true,
        required:false
      },
      'telefono':{
        minlength: 7,
        maxlength:7,
        digits:true,
        required:false
      },      
      'eps[movil]':{
        required: true,
        minlength: 10,
        maxlength:15,
        digits:true
      },
      'eps[email]':{
        required: true,        
        minlength: 10,
        maxlength:75,
        email:true,
      },
      'eps[idMunicipio]':{
        required: true
      },
      'eps[nombreContacto]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'eps[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {      
      var nit=$('#niEps').val();
      var numeroVerificacion=$('#numId').val();
      if (numeroVerificacion != '') {
        var valorNIT=nit+'-'+numeroVerificacion;
        $(form).children().children('input#eps_nit').val(valorNIT); 
        
      }else{
        $(form).children().children('input#eps_nit').val(nit);    
      }          
      var indicativo= $('#indicativo').val();
      var telefono=   $('#telefono').val();
      if (telefono != '' && indicativo != '') {
        var valorTelefono=indicativo+'-'+telefono;
        $(form).children().children('input#eps_telefono').val(valorTelefono); 
        $('body').addClass('loading');
        $(form)[0].submit();          
      }else if(telefono != '' || indicativo != ''){
        $('.tel').html('Registre el indicativo y el número telefónico.');        
      }else{
        $('body').addClass('loading');
        $(form)[0].submit();
      }
      
      
    }
  });

});

$(document).ready(function(){
  var validatorEpsEdit = $('#formEditEps').validate({
      rules: {      
      'eps[nombre]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'niEps':{
          minlength: 7,
          maxlength:15,
          digits:true
        },
      'numId':{
        minlength: 1,
        maxlength:1,
        digits:true
      },
      'eps[direccion]':{
        required: true,
        minlength: 6,
        maxlength:150
      },
      'indicativo':{
        minlength: 3,
        maxlength:3,
        digits:true,
        required:false
      },
      'telefono':{
        minlength: 7,
        maxlength:7,
        digits:true,
        required:false
      },      
      'eps[movil]':{
        required: true,
        minlength: 10,
        maxlength:15,
        digits:true
      },
      'eps[email]':{
        required: true,        
        minlength: 10,
        maxlength:75,
        email:true,
      },
      'eps[idMunicipio]':{
        required: true
      },
      'eps[nombreContacto]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'eps[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {      
      var nit=$(form).children().children().children('#niEps').val();
      var numeroVerificacion=$(form).children().children().children('#numId').val();
      var valorNIT=nit+'-'+numeroVerificacion;
      $(form).children().children('input#eps_nit').val(valorNIT); 
        
      var indicativo= $(form).children().children().children('#indicativo').val();
      var telefono=   $(form).children().children().children('#telefono').val();
      if (telefono != '' && indicativo != '') {
        var valorTelefono=indicativo+'-'+telefono;        
        $(form).children().children('input#eps_telefono').val(valorTelefono);  

        $('body').addClass('loading');
        var idT =$('#idEpsPrincipal').val();
        $(form).attr('action', "/eps/"+idT+"/edit");
        $(form)[0].submit();        
      }else if(telefono != '' || indicativo != ''){
        $('.tel').html('Registre el indicativo y el número telefónico.');
      }else{
        var idT =$('#idTipoR').val();
        $('body').addClass('loading');
        $(form).attr('action', "/eps/"+idT+"/edit");
        $(form)[0].submit(); 
      }
            
    }
  });
});

$(document).ready(function(){
  var validatorEpsSedeCrear = $('#formGeneralSedeEps').validate({
    rules: {
      'eps[nombre]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'eps[direccion]':{
        required: true,
        minlength: 6,
        maxlength:150
      },
      'indicativo':{
        minlength: 3,
        maxlength:3,
        digits:true,
        required:false
      },
      'telefono':{
        minlength: 7,
        maxlength:7,
        digits:true,
        required:false
      },      
      'eps[movil]':{
        required: true,
        minlength: 10,
        maxlength:15,
        digits:true
      },
      'eps[email]':{
        required: true,        
        minlength: 10,
        maxlength:75,
        email:true,
      },
      'eps[idMunicipio]':{
        required: true
      },
      'eps[nombreContacto]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'eps[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('#formGeneralSedeEps').children().children('input#eps_nit').val($("#nitEPS").val());  
      var indicativo= $(form).children().children().children('#indicativo').val();
      var telefono=   $(form).children().children().children('#telefono').val();
      if (telefono != '' && indicativo != '') {
        var valorTelefono=indicativo+'-'+telefono;             
        $(form).children().children('input#eps_telefono').val(valorTelefono); 
        $('body').addClass('loading');
        $(form).attr('action', "/eps/newSedeEps");
        $(form)[0].submit();
      }else if(telefono != '' || indicativo != ''){
        $('.tel').html('Registre el indicativo y el número telefónico.');
      }else{
        $('body').addClass('loading');
        $(form).attr('action', "/eps/newSedeEps");
        $(form)[0].submit();
      }    
    }

  });
  var validatorEpsSedeEdit = $('#formEditEpsSede').validate({
    rules: {
      'eps[nombre]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'eps[direccion]':{
        required: true,
        minlength: 6,
        maxlength:150
      },
      'indicativo':{
        minlength: 3,
        maxlength:3,
        digits:true,
        required:false
      },
      'telefono':{
        minlength: 7,
        maxlength:7,
        digits:true,
        required:false
      },      
      'eps[movil]':{
        required: true,
        minlength: 10,
        maxlength:15,
        digits:true
      },
      'eps[email]':{
        required: true,        
        minlength: 10,
        maxlength:75,
        email:true,
      },
      'eps[idMunicipio]':{
        required: true
      },
      'eps[nombreContacto]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'eps[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
        var idSEdeEps =$('#idEpSede').val();
        var indicativo= $(form).children().children().children('#indicativo').val();
        var telefono=   $(form).children().children().children('#telefono').val();
        if (telefono != '' && indicativo != '') {
          var valorTelefono=indicativo+'-'+telefono;             
          $(form).children().children('input#eps_telefono').val(valorTelefono); 
          $('body').addClass('loading');
          $(form).attr('action', "/eps/"+idSEdeEps+"/editSede");
          $(form)[0].submit();
        }else if(telefono != '' || indicativo != ''){
          $('.tel').html('Registre el indicativo y el número telefónico.');
        }else{
          $('body').addClass('loading');
          $(form).children().children('input#eps_telefono').val(''); 
          $(form).attr('action', "/eps/"+idSEdeEps+"/editSede");
          $(form)[0].submit();
        }    
        
    }
  });
 });

$(document).ready(function(){
  var validacionConvenioEps = $('#formConvenioCrearE').validate({
    rules: {
      'convenio[nombre]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'convenio[idEps]':{
        required: true
      },
      'convenio[porcentajeDescuento]':{
        required: false,
        minlength: 1,
        maxlength:3
      },
      'convenio[porcentajeRemision]':{
        required: false,
        minlength: 1,
        maxlength:3
      },
      'convenio[observacion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      },
      'convenio[fechaApertura]':{
        required: true
      },
      'convenio[fechaFinalizacion]':{
        required: true,
        fechaMayor:true
      }
    },
    submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action', "/eps/newConvenio");
        $(form)[0].submit(); 
    }
  });


  var validacionDepartamento = $('#formDepartamento').validate({
    rules: {
      'departamento[nombre]':{
        required: true,
        minlength: 3,
        maxlength:75
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();  
    }
  });


  var validacionEditDepartamento = $('#formeditDepartamento').validate({
    rules: {
      'departamento[nombre]':{
        required: true,
        minlength: 3,
        maxlength:75
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action', "/departamento/"+$('#idDep').val()+"/edit");
      $(form)[0].submit();
    }
  });

  var validatorCrearMunicipio = $('#formGeneralMunicipios').validate({
    rules: {
      'municipio[id]':{
        minlength: 3,
        maxlength:150,
        required: true,
        digits:true
      },
      'municipio[nombre]':{
        required: true,
        minlength: 3,
        maxlength:150
      }
    },
    submitHandler: function(form) {
        $('body').addClass('loading');
        var idT =$('#idTipoR').val();
        if (typeof(idT) != "undefined") {
          $(form).attr('action', "/departamento/newMunicipio");
          $(form)[0].submit();    
        }else{
          $(form).attr('action', "/departamento/newMunicipio");
          $(form)[0].submit();
        }  
      }
  });

  var validatorEditMunicipio = $('#formEditMunicipio').validate({
    rules: {
      'municipio[id]':{
        minlength: 3,
        maxlength:150,
        required: true,
        digits:true
      },
      'municipio[nombre]':{
        required: true,
        minlength: 3,
        maxlength:150
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action', "/departamento/"+$('#idMun').val()+"/editMunicipio");
      $(form)[0].submit();  
    }
  });

  var validatorCrearTratamientoConvenio=$('#formTratamientoConvenio').validate({
    rules: {
      'tratamiento_convenio[idTratamiento]':{
        required: true
      },
      'tratamiento_convenio[porcentajeTratamiento]':{
        minlength: 1,
        maxlength: 3,
        required: true,
        digits:true
      },
      'tratamiento_convenio[porcentajeRemision]':{
        required: true,
        minlength: 1,
        maxlength: 3
      },
      'tratamiento_convenio[fechaApertura]':{
        required: true,
        fechaMayor:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action', "/convenio/newTratamientoConvenio");
      $(form)[0].submit();  
    }
  });
});

$(document).ready(function(){
  var validacionConvenioCrear = $('#formconvenioCrear').validate({
    rules: {
      'convenio[nombre]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'convenio[idEps]':{
        required: true
      },
      'convenio[porcentajeDescuento]':{
        required: false,
        minlength: 1,
        maxlength:3
      },
      'convenio[porcentajeRemision]':{
        required: false,
        minlength: 1,
        maxlength:3
      },
      'convenio[observacion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      },
      'convenio[fechaApertura]':{
        required: true
      },
      'convenio[fechaFinalizacion]':{
        required: true,
        fechaMayor:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
      
    }
  });

  var validacionConvenioEdit = $('#formConvenioEdit').validate({
    rules: {
      'convenio[nombre]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'convenio[idEps]':{
        required: true
      },
      'convenio[porcentajeDescuento]':{
        required: false,
        minlength: 1,
        maxlength:3
      },
      'convenio[porcentajeRemision]':{
        required: false,
        minlength: 1,
        maxlength:3
      },
      'convenio[observacion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      },
      'convenio[fechaApertura]':{
        required: true
      },
      'convenio[fechaFinalizacion]':{
        required: true,
        fechaMayor:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action', "/convenio/"+$('#idConvenio').val()+"/edit");
      $(form)[0].submit(); 
    }
  });

  var validatorTratamientoConvenioEdit=$('#formTratamientoConvenioEdit').validate({
    rules:{
      'tratamiento_convenio[idTratamiento]':{
        required: true
      },
      'tratamiento_convenio[porcentajeTratamiento]':{
        minlength: 1,
        maxlength: 3,
        required: true,
        digits:true
      },
      'tratamiento_convenio[porcentajeRemision]':{
        required: true,
        minlength: 1,
        maxlength: 3
      },
      'tratamiento_convenio[fechaApertura]':{
        required: true,
        fechaMayor:true
      }
    },
    submitHandler:function(form){
      $('body').addClass('loading');
      $(form).attr('action','/tratamientoConvenio/'+$('#idConvenioTratamiento').val()+'/edit');
      $(form)[0].submit();
    }
  });

});
$(document).ready(function(){
  var validatoralergiaCrear = $('#formAlergia').validate({
      rules: {
        'alergia[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'alergia[descripcion]':{
          required: false,
          minlength: 3,
          maxlength:2000
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });
  var validatoralergiaEditar = $('#formAlergiaEdit').validate({
      rules: {
        'alergia[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'alergia[descripcion]':{
          required: false,
          minlength: 3,
          maxlength:2000
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/alergia/'+$('#idAlergia').val()+'/edit');
        $(form)[0].submit();
        
      }
  });

  var validatorAntecedenteCrear = $('#formAntecedente').validate({
    rules:{
      'antecedente[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'antecedente[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorAntecedenteEditar = $('#formAntecedenteEdit').validate({
    rules:{
      'antecedente[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'antecedente[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/antecedente/'+$('#idAntecedente').val()+'/edit');
      $(form)[0].submit();
    }
  });

  var validatorHabitoCrear = $('#formHabito').validate({
    rules:{
      'habito[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'habito[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorHabitoEditar = $('#formHabitoEdit').validate({
    rules:{
      'habito[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'habito[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/habito/'+$('#idHabito').val()+'/edit');
      $(form)[0].submit();
    }
  });
});  
$(document).ready(function(){
  var validatorMedicamentoCrear = $('#formMedicamento').validate({
    rules:{
      'medicamento[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'medicamento[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorMedicamentoEditar = $('#formMedicamentoEdit').validate({
    rules:{
      'medicamento[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'medicamento[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/medicamento/'+$('#idMedicamento').val()+'/edit');
      $(form)[0].submit();
    }
  });  

  var validatorRiesgoCrear = $('#formRiesgo').validate({
    rules:{
      'riesgo[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'riesgo[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorRiesgoEditar = $('#formRiesgoEdit').validate({
    rules:{
      'riesgo[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'riesgo[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/riesgo/'+$('#idRiesgo').val()+'/edit');
      $(form)[0].submit();
    }
  });

  var validatorEvolucionEndoCrear = $('#formEvolucionEndo').validate({
    rules:{
      'evolucion_endodoncia[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorEvolucionEndoEditar = $('#formAevolucionEndodonciaEdit').validate({
    rules:{
      'evolucion_endodoncia[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/evolucionEndodoncia/'+$('#idEvolucionEndodoncia').val()+'/edit');
      $(form)[0].submit();
    }
  });

   var validatorPropiedadCrear = $('#formPropiedad').validate({
    rules:{
      'propiedad[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorPropiedadEditar = $('#formPropiedadEdit').validate({
    rules:{
      'propiedad[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/propiedad/'+$('#idPropiedad').val()+'/edit');
      $(form)[0].submit();
    }
  });

  var validatorProvocadoCrear = $('#formProvocado').validate({
    rules:{
      'provocado[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorProvocadoEditar = $('#formProvocadoEdit').validate({
    rules:{
      'provocado[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/provocado/'+$('#idProvocado').val()+'/edit');
      $(form)[0].submit();
    }
  }); 
});  

$(document).ready(function(){
   var validatorSuperficieCrear = $('#formSuperficie').validate({
    rules:{
      'superficie[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'superficie[convencion]':{
        minlength: 1,
        maxlength:10,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorSuperficieEditar = $('#formSuperficieEdit').validate({
    rules:{
      'superficie[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'superficie[convencion]':{
        minlength: 1,
        maxlength:10,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/superficie/'+$('#idSuperficie').val()+'/edit');
      $(form)[0].submit();
    }
  });

  var validatorTipoDiagnosticoCrear = $('#formTipoDiagnostico').validate({
    rules:{
      'tipo_diagnostico[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'tipo_diagnostico[idArea]':{
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorTipoDiagnosticoEditar = $('#formTipoDiagnosticoEdit').validate({
    rules:{
      'tipo_diagnostico[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'tipo_diagnostico[idArea]':{
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/tipoDiagnostico/'+$('#idTipoDiagnostico').val()+'/edit');
      $(form)[0].submit();
    }
  });

  var validatorDiagnosticoOdontologicoCrear = $('#formDiagnosticoOdontologico').validate({
    rules:{
      'diagnostico_odontologico[codigo]':{
        minlength: 1,
        maxlength:10,
        required:true
      },
      'diagnostico_odontologico[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'diagnostico_odontologico[descripcion]':{
        required: true,
        minlength: 3,
        maxlength:2000,
      } 
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorDiagnosticoOdontologicoEditar = $('#formDiagnosticoOdontologicoEdit').validate({
    rules:{
      'diagnostico_odontologico[codigo]':{
        minlength: 1,
        maxlength:10,
        required:true
      },
      'diagnostico_odontologico[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'diagnostico_odontologico[descripcion]':{
        required: true,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/diagnosticoOdontologico/'+$('#idDiagnosticoOdontologico').val()+'/edit');
      $(form)[0].submit();
    }
  });
  var validatorDiagnosticoPrincipalCrear = $('#formDiagnosticoPrincipal').validate({
    rules:{
      'diagnostico_principal[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorDiagnosticoPrincipalEditar = $('#formDiagnosticoPrincipalEdit').validate({
    rules:{
      'diagnostico_principal[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/diagnosticoPrincipal/'+$('#idDiagnosticoPrincipal').val()+'/edit');
      $(form)[0].submit();
    }
  }); 

  var validatorDiagnosticoCieTipoCrear = $('#formCieDiagnosticoTipo').validate({
    rules:{
      'cie_diagnostico_tipo[idTipoDiagnostico]':{
        required:true
      },
      'tipoVisualizacion':{
        required:true
      }
    },
    submitHandler: function(form) {
      valorActual=$('#cie_diagnostico_tipo_idTipoDiagnostico').val();
      if (valorActual == 1) {
        if ($('#cie_diagnostico_tipo_idCieRip').val() != '') {
          $('#cie_diagnostico_tipo_idDiagnosticoOdontologico').val()
          valorVisual= $('#tipoVisualizacion').val();
          if (valorVisual == 'ICONO') {
            if ($('#cie_diagnostico_tipo_simbolo').val() != '') {
              $('#cie_diagnostico_tipo_color').val('');
              var idT =$('#idCieDiagnosticoTipo').val();
              if (idT == '') {
                $('body').addClass('loading');
                $(form)[0].submit();    

              }else{
                $('body').addClass('loading');
                $(form).attr('action', '/cieDiagnosticoTipo/'+idT+'/edit');
                $(form)[0].submit();
              } 
            }else{
              $('#simboloCieOdonto').html('Este campo es obligatorio');
            }
          }else if (valorVisual == 'COLOR') {
            if ($('#cie_diagnostico_tipo_color').val() != '') {
              $('#cie_diagnostico_tipo_simbolo').val('');
              var idT =$('#idCieDiagnosticoTipo').val();
              if (idT == '') {
                $('body').addClass('loading');
                $(form)[0].submit();    

              }else{
                $('body').addClass('loading');
                $(form).attr('action', '/cieDiagnosticoTipo/'+idT+'/edit');
                $(form)[0].submit();
              } 
            }else{
              $('#colorCieOdonto').html('Este campo es obligatorio');
            }
          }
        }else{
          $('.cie').html('Este campo es obligatorio.');
        }
      }else if (valorActual == 2) {
        if ($(form).children().children().children('select#cie_diagnostico_tipo_idDiagnosticoOdontologico').val() != '') {
          $('#cie_diagnostico_tipo_idCieRip').val('');
          valorVisual= $('#tipoVisualizacion').val();
          if (valorVisual == 'ICONO') {
            if ($('#cie_diagnostico_tipo_simbolo').val() != '') {
              $('#cie_diagnostico_tipo_color').val('');
              var idT =$('#idCieDiagnosticoTipo').val();
              if (idT == '') {
                $('body').addClass('loading');
                $(form)[0].submit();    

              }else{
                $('body').addClass('loading');
                $(form).attr('action', '/cieDiagnosticoTipo/'+idT+'/edit');
                $(form)[0].submit();
              } 
            }else{
              $('#simboloCieOdonto').html('Este campo es obligatorio');
            }
          }else if (valorVisual == 'COLOR') {
            if ($('#cie_diagnostico_tipo_color').val() != '') {
              $('#cie_diagnostico_tipo_simbolo').val('');
                var idT =$('#idCieDiagnosticoTipo').val();
                if (idT == '') {
                  $('body').addClass('loading');
                  $(form)[0].submit();    

                }else{
                  $('body').addClass('loading');
                  $(form).attr('action', '/cieDiagnosticoTipo/'+idT+'/edit');
                  $(form)[0].submit();
                } 
            }else{
              $('#colorCieOdonto').html('Este campo es obligatorio');
            }
          }
        }else{
           $('.odonto').html('Este campo es obligatorio.');
        }
      }      
    }
  });
});
$(document).ready(function(){
  var validatorTipoArchivoCrear = $('#formTipoArchivo').validate({
    rules:{
      'tipo_archivo[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatortipoArchivoEditar = $('#formTipoArchivoEdit').validate({
    rules:{
      'tipo_archivo[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/tipoArchivo/'+$('#idTipoArchivo').val()+'/edit');
      $(form)[0].submit();
    }
  }); 

  var validatorTipoConsentimientoCrear = $('#formTipoConsentimiento').validate({
    rules:{
      'tipo_consentimiento[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'tipo_consentimiento[idCie]':{
        required:true
      },
      'tipo_consentimiento[riesgo]':{
        minlength: 3,
        maxlength:300,
        required:true
      },
      'tipo_consentimiento[contenido]':{
        minlength: 3,
        maxlength:2000,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatortipoConsentimientoEditar = $('#formTipoConsentimientoEdit').validate({
    rules:{
      'tipo_consentimiento[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'tipo_consentimiento[idCie]':{
        required:true
      },
      'tipo_consentimiento[riesgo]':{
        minlength: 3,
        maxlength:300,
        required:true
      },
      'tipo_consentimiento[contenido]':{
        minlength: 3,
        maxlength:2000,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/tipoConsentimiento/'+$('#idTipoConsentimiento').val()+'/edit');
      $(form)[0].submit();
    }
  }); 

  var validatorTipoContratoCrear = $('#formTipoContrato').validate({
    rules:{
      'tipo_contrato[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'tipo_contrato[contenido]':{
        minlength: 3,
        maxlength:2000,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatortipoContratoEditar = $('#formTipoContratoEdit').validate({
    rules:{
      'tipo_contrato[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'tipo_contrato[contenido]':{
        minlength: 3,
        maxlength:2000,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/tipoContrato/'+$('#idTipoContrato').val()+'/edit');
      $(form)[0].submit();
    }
  }); 
});
$(document).ready(function(){
  var validatorEstadoCitaNuevo = $('#formEstadoCita').validate({
    rules:{
      'estado_cita[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'estado_cita[enviarCorreo]':{
        required:true
      },
      'estado_cita[icono]':{
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();    
    }
  }); 

   var validatorEstadoCitaEditar = $('#formEstadoCitaEdit').validate({
    rules:{
      'estado_cita[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'estado_cita[enviarCorreo]':{
        required:true
      },
      'estado_cita[icono]':{
        required:true
      }
    },
    submitHandler: function(form) {
      var idEstado=$('#idEstadoCita').val();
      if (idEstado != '') {       
        $('body').addClass('loading');
        $(form).attr('action', '/estadoCita/'+idEstado+'/edit');
        $(form)[0].submit();
      } 
    }
  }); 

  var validatorEstadoGeneralCrear = $('#formEstadoGeneral').validate({
    rules:{
      'estado_general[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'estado_general[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorEstadoGeneralEditar = $('#formEstadoGeneralEdit').validate({
    rules:{
      'estado_general[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'estado_general[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/estadoGeneral/'+$('#idEstadoGeneral').val()+'/edit');
      $(form)[0].submit();
    }
  });

  var validatorEstadoMantenimientoCrear = $('#formEstadoMantenimiento').validate({
    rules:{
      'estado_mantenimiento[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorEstadoMantenimientoEditar = $('#formEstadoMantenimientoEdit').validate({
    rules:{
      'estado_mantenimiento[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/estadoMantenimiento/'+$('#idEstadoMantenimiento').val()+'/edit');
      $(form)[0].submit();
    }
  });

  var validatorEstadoPagoCrear = $('#formEstadoPago').validate({
    rules:{
      'estado_pago[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorEstadoPagoEditar = $('#formEstadoPagoEdit').validate({
    rules:{
      'estado_pago[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/estadoPago/'+$('#idEstadoPago').val()+'/edit');
      $(form)[0].submit();
    }
  });

  var validatorTipoPaqueteCrear = $('#formTipoPaquete').validate({
    rules:{
      'tipo_paquete[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'tipo_paquete[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
    }
  });

  var validatorTipoPaqueteEditar = $('#formTipoPaqueteEdit').validate({
    rules:{
      'tipo_paquete[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'tipo_paquete[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/tipoPaquete/'+$('#idTipoPaquete').val()+'/edit');
      $(form)[0].submit();
    }
  });
});
//Manejo verificación de errores en ingreso de nuevos registros
$(document).ready(function(){

  var validatorEsterilizacion = $('#formEsterilizacion').validate({
    rules:{
      'esterilizacion[fechaApertura]':{
        required:true
      },
      'esterilizacion[fechaVencimiento]':{
        required: true
      },
       'esterilizacion[carga]':{
        required:true
      },
       'esterilizacion[loteProveta]':{
        required:true,
        minlength: 8,
        maxlength:30
      },
       'esterilizacion[cantidadPaquete]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:true
      },
       'esterilizacion[idUsuarioReporta]':{
        required:true
      },
      'esterilizacion[tipoEsterilizacion]':{
        required:true
      },
      'esterilizacion[tiempoEsterilizacion]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required: true
      },
       'esterilizacion[tiempoSecado]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:true
      },
       'esterilizacion[temperatura]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:false
      },
       'esterilizacion[presion]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:false
      }
    },
    submitHandler: function(form) {
      valorAperturaCrear= new Date($(form).children().children().children().children('input#esterilizacion_fechaApertura').val());
      valorVencimiento= new Date($(form).children().children().children().children('input#esterilizacion_fechaVecimiento').val());
      if(valorAperturaCrear < valorVencimiento ){
        $('body').addClass('loading');
        $(form)[0].submit();
      }else {
       $('.vencimiento').html('La fecha de vencimiento no puede ser menor que la fecha de inicio.'); 
       return false;
      }
    }
  });

  var validatorEsterilizacion = $('#formEsterilizacionEdit').validate({
    rules:{
      'esterilizacion[fechaApertura]':{
        required:true
      },
      'esterilizacion[fechaVencimiento]':{
        required: true
      },
       'esterilizacion[carga]':{
        required:true
      },
       'esterilizacion[loteProveta]':{
        required:true,
        minlength: 8,
        maxlength:30
      },
       'esterilizacion[cantidadPaquete]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:true
      },
       'esterilizacion[idUsuarioReporta]':{
        required:true
      },
      'esterilizacion[tipoEsterilizacion]':{
        required:true
      },
      'esterilizacion[tiempoEsterilizacion]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required: true
      },
       'esterilizacion[tiempoSecado]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:true
      },
       'esterilizacion[temperatura]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:false
      },
       'esterilizacion[presion]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:false
      }
    },
    submitHandler: function(form) {
      valorAperturaCrear= new Date($(form).children().children().children().children('input#esterilizacion_fechaApertura').val());
      valorVencimiento= new Date($(form).children().children().children().children('input#esterilizacion_fechaVecimiento').val());
      if(valorAperturaCrear < valorVencimiento ){
        $('body').addClass('loading');
        $(form).attr('action','/esterilizacion/'+$('#idEsterilizacion').val()+'/edit');
        $(form)[0].submit();
      }else {
       $('.vencimiento').html('La fecha de vencimiento no puede ser menor que la fecha de inicio.'); 
       return false;
      }
      
    }
  });
  
  var validatorNuevoPaquete = $('#formNuevoPaquete').validate({
    rules:{
      'paquete_esterilizacion[codigo]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:true
      },
      'paquete_esterilizacion[idTipoPaquete]':{
        required: true
      },
      'paquete_esterilizacion[idEsterilizacion]':{
        required: true
      },
      'paquete_esterilizacion[estado]':{
        required: true
      }
    },
    submitHandler: function(form) {
      var url = '/esterilizacion/'+$(form).children().children('select#paquete_esterilizacion_idEsterilizacion').val()+'/paqueteEsterilizacion';  
      $.ajax({
          // especifica si será una petición POST o GET
          type: 'POST',
           // la información a enviar
          data: $(form).serialize(),
          //Url de petición envio
          url: url,          
          // el tipo de información que se espera de respuesta
          dataType : 'json',
          beforeSend: function () {
            $('body').addClass('loading'); //Agregamos la clase loading al body
          },
          // código a ejecutar si la petición es satisfactoria;
          success : function(data) {
            $('body').removeClass('loading');   
            if (data.error == 'vacio') {                                 
             $('.resp').removeAttr('hidden').removeClass('alert-danger').addClass('alert-success').html(data.mensaje);
             $(form).children().children().children('input#paquete_esterilizacion_codigo').val('');
             $(form).children().children().children('select#paquete_esterilizacion_idTipoPaquete').val('');
             $(form).children().children().children('select#paquete_esterilizacion_idTipoPaquete').change();
             $("#datos").html(data.paquetes);
            }else{
              $('.resp').removeAttr('hidden').removeClass('alert-success').addClass('alert-danger').html(data.error);
              $(form).children().children().children('input#paquete_esterilizacion_codigo').val('');
              $(form).children().children().children('select#paquete_esterilizacion_idTipoPaquete').val('');
              $(form).children().children().children('select#paquete_esterilizacion_idTipoPaquete').change();
            } 
          }
        
      });    
    }  
  });  

  var validatorModificarPaquete = $('#formPaqueteEsterilizacionEdit').validate({
    rules:{
      'paquete_esterilizacion[codigo]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:true
      },
      'paquete_esterilizacion[idTipoPaquete]':{
        required: true
      },
      'paquete_esterilizacion[idEsterilizacion]':{
        required: true
      },
      'paquete_esterilizacion[estado]':{
        required: true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/paqueteEsterilizacion/'+$('#idPaqueteEsterilizacion').val()+'/edit');
      $(form)[0].submit();
    }  
  });  
});
$(document).ready(function(){
  var validatorNuevoAcuerdoPago = $('#formAcuerdoPago').validate({
    rules:{
      'acuerdo_pago[idTratamiento]':{
        required: false
      },
      'acuerdo_pago[tipo]':{
        required: true
      },
      'acuerdo_pago[valor]':{
        required: false,
        minlength: 1,
        maxlength:20,
        digits:true
      },
      'acuerdo_pago[porcentaje]':{
        required: false,
        minlength: 1,
        maxlength:20,
        digits:true
      }
    },
    submitHandler: function(form) { 
      if ($(form).children().children().children('select#acuerdo_pago_tipo').val() == 'VALOR') {
        if ($(form).children().children().children('input#acuerdo_pago_valor').val() != '') {
          $(form).children().children().children('input#acuerdo_pago_porcentaje').val('');
          $('body').addClass('loading');
          $(form)[0].submit();
        }else{
          $('#valorError').html('Este campo es obligatorio.');
        }
      }else if ($(form).children().children().children('select#acuerdo_pago_tipo').val() == 'PORCENTAJE') {
        if ($(form).children().children().children('input#acuerdo_pago_porcentaje').val() != '') {
          $(form).children().children().children('input#acuerdo_pago_valor').val('');
          $('body').addClass('loading');
          $(form)[0].submit();
        }else{
          $('#porcentajeError').html('Este campo es obligatorio.');
        }
      }
      
     
    }  
  });  

  var validatorModificarAcuerdoPago = $('#formAcuerdoPagoEdit').validate({
    rules:{
      'acuerdo_pago[idTratamiento]':{
        required: false
      },
      'acuerdo_pago[tipo]':{
        required: true
      },
      'acuerdo_pago[valor]':{
        required: false,
        minlength: 1,
        maxlength:20,
        digits:true
      },
      'acuerdo_pago[porcentaje]':{
        required: false,
        minlength: 1,
        maxlength:20,
        digits:true
      }
    },
    submitHandler: function(form) { 
      if ($(form).children().children().children('select#acuerdo_pago_tipo').val() == 'VALOR') {
        if ($(form).children().children().children('input#acuerdo_pago_valor').val() != '') {
          $(form).children().children().children('input#acuerdo_pago_porcentaje').val('');
          $('body').addClass('loading');
          $(form).attr('action','/acuerdoPago/'+$('#idAcuerdoPago').val()+'/edit');
          $(form)[0].submit();
        }else{
          $('#valorError').html('Este campo es obligatorio.');
        }
      }else if ($(form).children().children().children('select#acuerdo_pago_tipo').val() == 'PORCENTAJE') {
        if ($(form).children().children().children('input#acuerdo_pago_porcentaje').val() != '') {
          $(form).children().children().children('input#acuerdo_pago_valor').val('');
          $('body').addClass('loading');
          $(form).attr('action','/acuerdoPago/'+$('#idAcuerdoPago').val()+'/edit');
          $(form)[0].submit();
        }else{
          $('#porcentajeError').html('Este campo es obligatorio.');
        }
      }
      
     
    }  
  });  
  var validatorNuevoConceptoPago = $('#formConceptoPago').validate({
    rules:{
      'concepto_pago[nombre]':{        
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarconceptoPago = $('#formConceptoPagoEdit').validate({
    rules:{
      'concepto_pago[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/conceptoPago/'+$('#idConceptoPago').val()+'/edit');
      $(form)[0].submit();
    }  
  });  

  var validatorNuevoFormaPago = $('#formFormaPago').validate({
    rules:{
      'forma_pago[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarFormaPago = $('#formFormaPagoEdit').validate({
    rules:{
      'forma_pago[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/formaPago/'+$('#idFormaPago').val()+'/edit');
      $(form)[0].submit();
    }  
  });  

  var validatorNuevoSoportePago = $('#formSoportePago').validate({
    rules:{
      'soporte_pago[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarSoportePago = $('#formSoportePagoEdit').validate({
    rules:{
      'soporte_pago[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/soportePago/'+$('#idSoportePago').val()+'/edit');
      $(form)[0].submit();
    }  
  });

  var validatorNuevoCategoriaInventario = $('#formCategoriaInventario').validate({
    rules:{
      'categoria_inventario[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarCategoriaInventario = $('#formCategoriaInventarioEdit').validate({
    rules:{
      'categoria_inventario[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/categoriaInventario/'+$('#idCategoriaInventario').val()+'/edit');
      $(form)[0].submit();
    }  
  });  

  var validatorNuevoMarca = $('#formMarca').validate({
    rules:{
      'marca[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarMarca = $('#formMarcaEdit').validate({
    rules:{
      'marca[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/marca/'+$('#idMarca').val()+'/edit');
      $(form)[0].submit();
    }  
  }); 

  var validatorNuevoNombreInsumo = $('#formNombreInsumo').validate({
    rules:{
      'nombre_insumo[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarNombreInsumo = $('#formNombreInsumoEdit').validate({
    rules:{
      'nombre_insumo[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/nombreInsumo/'+$('#idNombreInsumo').val()+'/edit');
      $(form)[0].submit();
    }  
  }); 

  var validatorPresentacion = $('#formPresentacion').validate({
    rules:{
      'presentacion[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'cantidad':{
        minlength: 1,
        maxlength:10,
        required: true
      },
      'unidades':{
        required :true
      }
    },
    submitHandler: function(form) {
      var valorFinal=$(form).children().children().children('input#cantidad').val()+' / '+$(form).children().children().children('select#unidades').val();
      $(form).children().children('input#presentacion_unidad').val(valorFinal);
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarPresentacion = $('#formPresentacionEdit').validate({
    rules:{
      'presentacion[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'cantidad':{
        minlength: 1,
        maxlength:10,
        required: true
      },
      'unidades':{
        required :true
      }
    },
    submitHandler: function(form) { 
      var valorFinal=$(form).children().children().children('input#cantidad').val()+' / '+$(form).children().children().children('select#unidades').val();
      $(form).children().children('input#presentacion_unidad').val(valorFinal);
      alert($(form).children().children('input#presentacion_unidad').val());
      $('body').addClass('loading');
      $(form).attr('action','/presentacion/'+$('#idPresentacion').val()+'/edit');
      $(form)[0].submit();
    }  
  });  

  var validatorStock = $('#formStock').validate({
    rules:{
      'stock[idNombreInsumo]':{
        required: true
      },
      'stock[cantidad]':{
        minlength: 1,
        maxlength:10,
        required: true,
        digits:true
      },
      'stock[idClinica]':{
        required :true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarStock = $('#formStockEdit').validate({
    rules:{
      'stock[idNombreInsumo]':{
        required: true
      },
      'stock[cantidad]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required: true
      },
      'stock[idClinica]':{
        required :true
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/stock/'+$('#idStock').val()+'/edit');
      $(form)[0].submit();
    }  
  });  

  var validatorInsumo = $('#formInsumo').validate({
    rules:{
      'insumo[idNombreInsumo]':{
        required: true
      },
      'insumo[idCategoria]':{
        required: true
      },
      'insumo[idMarca]':{
        required: true
      },
      'insumo[idPresentacion]':{
        required: true
      },
      'insumo[potencia]':{
        required :false,
        minlength: 1,
        maxlength:10,
        digits:true
      },
      'insumo[corriente]':{
        required :false,
        minlength: 1,
        maxlength:10,
        digits:true
      }      ,
      'insumo[voltaje]':{
        required :false,
        minlength: 1,
        maxlength:10,
        digits:true
      },
      'insumo[fuente]':{
        required :false,
        minlength: 1,
        maxlength:120
      },
      'insumo[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarInsumo = $('#formInsumoEdit').validate({
    rules:{
      'insumo[idNombreInsumo]':{
        required: true
      },
      'insumo[idCategoria]':{
        required: true
      },
      'insumo[idMarca]':{
        required: true
      },
      'insumo[idPresentacion]':{
        required: true
      },
      'insumo[potencia]':{
        required :false,
        minlength: 1,
        maxlength:10,
        digits:true
      },
      'insumo[corriente]':{
        required :false,
        minlength: 1,
        maxlength:10,
        digits:true
      }      ,
      'insumo[voltaje]':{
        required :false,
        minlength: 1,
        maxlength:10,
        digits:true
      },
      'insumo[fuente]':{
        required :false,
        minlength: 1,
        maxlength:10
      },
      'insumo[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/insumo/'+$('#idInsumo').val()+'/edit');
      $(form)[0].submit();
    }  
  }); 

  var validatorNuevoProveedor = $('#formProveedor').validate({
    rules:{
      'proveedor[idTipoDocumento]':{
        required: true
      },
      'niProveedor':{
        minlength: 7,
        maxlength:15,
        digits:true,
        required: true
      },
      'numIdProveedor':{
        minlength: 1,
        maxlength:1,
        digits:true,
        required:false
      },
      'proveedor[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'proveedor[nombreContacto]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'proveedor[movil]':{
        minlength: 10,
        maxlength:15,
        digits:true
      }      ,
      'indicativoProvee':{
        minlength: 3,
        maxlength:3,
        digits:true,
        required:false
      },
      'telefonoProvee':{
        minlength: 7,
        maxlength:7,
        digits:true,
        required:false
      },
      'proveedor[representanteLegal]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'proveedor[email]':{
        minlength: 10,
        maxlength:75,
        email:true,
      },
      'proveedor[direccion]':{
        minlength: 6,
        maxlength:150,
        required:true
      },
      'proveedor[idMunicipio]':{
        required: true
      }
    },
    submitHandler: function(form) {
      var nit=$(form).children().children().children('input#niProveedor').val();
      var numeroVerificacion=$(form).children().children().children('input#numIdProveedor').val();
      if (numeroVerificacion != '') {
        var valorNIT=nit+'-'+numeroVerificacion;
        $(form).children().children('input#proveedor_identificacion').val(valorNIT);         
      }else{
        $(form).children().children('input#proveedor_identificacion').val(nit);    
      }             
      var indicativo= $(form).children().children().children('input#indicativoProvee').val();
      var telefono=   $(form).children().children().children('input#telefonoProvee').val();
      if (telefono != '' && indicativo != '') {
        var valorTelefono=indicativo+'-'+telefono;
        $(form).children().children('input#proveedor_telefono').val(valorTelefono);   
        $('body').addClass('loading');
        $(form)[0].submit();        
      }else if(telefono != '' || indicativo != ''){
        $('.telProvee').html('Registre el indicativo y el número telefónico.');
      }else{
        $('body').addClass('loading');
        $(form)[0].submit();
      }
        
    }  
  });  

  var validatorModificarProveedor = $('#formProveedorEdit').validate({
    rules:{
      'proveedor[idTipoDocumento]':{
        required: true
      },
      'niProveedor':{
        minlength: 7,
        maxlength:15,
        digits:true
      },
      'numIdProveedor':{
        minlength: 1,
        maxlength:1,
        digits:true,
        required:false
      },
      'proveedor[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'proveedor[nombreContacto]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'proveedor[movil]':{
        minlength: 10,
        maxlength:15,
        digits:true
      }      ,
      'indicativoProvee':{
        minlength: 3,
        maxlength:3,
        digits:true,
        required:false
      },
      'telefonoProvee':{
        minlength: 7,
        maxlength:7,
        digits:true,
        required:false
      },
      'proveedor[representanteLegal]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'proveedor[email]':{
        minlength: 10,
        maxlength:75,
        email:true,
      },
      'proveedor[direccion]':{
        minlength: 6,
        maxlength:150,
        required:true
      },
      'proveedor[idMunicipio]':{
        required: true
      }
    },
    submitHandler: function(form) { 
      var idProveedor = $('#idProveedor').val();
      var nit=$(form).children().children().children('input#niProveedor').val();
      var numeroVerificacion=$(form).children().children().children('input#numIdProveedor').val();
      if (numeroVerificacion != '') {
        var valorNIT=nit+'-'+numeroVerificacion;
        $(form).children().children('input#proveedor_identificacion').val(valorNIT);         
      }else{
        $(form).children().children('input#proveedor_identificacion').val(nit);    
      }             
      var indicativo= $(form).children().children().children('input#indicativoProvee').val();
      var telefono=   $(form).children().children().children('input#telefonoProvee').val();
      if (telefono != '' && indicativo != '') {
        var valorTelefono=indicativo+'-'+telefono;
        $(form).children().children('input#proveedor_telefono').val(valorTelefono);   
        $('body').addClass('loading');
        $(form).attr('action', "/proveedor/"+idProveedor+"/edit");
        $(form)[0].submit();        
      }else if(telefono != '' || indicativo != ''){
        $('.telProvee').html('Registre el indicativo y el número telefónico.');
      }else{
        $('body').addClass('loading');
        $(form).attr('action', "/proveedor/"+idProveedor+"/edit");
        $(form)[0].submit();
      }
    }  
  });  

  var validatorPedido = $('#formPedido').validate({
    rules:{
      'pedido[idProveedor]':{
        required: true
      },
      'pedido[numeroFactura]':{
        minlength: 9,
        maxlength:17,
        required: true
      },
      'pedido[fechaFactura]':{
        required :true
      },
      'pedido[idOrdenCompra]':{
        required :false
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorModificarPedido = $('#formPedidoEdit').validate({
    rules:{
      'pedido[idProveedor]':{
        required: true
      },
      'pedido[numeroFactura]':{
        minlength: 9,
        maxlength:17,
        required: true
      },
      'pedido[fechaFactura]':{
        required :true
      },
      'pedido[idOrdenCompra]':{
        required :false
      }
    },
    submitHandler: function(form) { 
      $('body').addClass('loading');
      $(form).attr('action','/pedido/'+$('#idPedido').val()+'/edit');
      $(form)[0].submit();
    }  
  });  

  var validatorEntradaPedido = $('#formNuevaEntradaPedido').validate({
    rules:{
      'entrada_pedido[idInsumo]':{
        required: true
      },
      'entrada_pedido[registroInvima]':{
        minlength: 9,
        maxlength:17,
        lotePedido: true,
        required: false
      },
      'entrada_pedido[lote]':{
        minlength: 4,
        maxlength: 20,
        lotePedido: true,
        required: false
      },
      'entrada_pedido[fechaVencimiento]':{
        required: false
      },
      'entrada_pedido[cantidad]':{
        required: true, 
        minlength: 1,
        maxlength: 10,
        digits:true
      },
      'entrada_pedido[unidadMedida]':{
        required: true
      },
      'entrada_pedido[idClinicaUbicacion]':{
        required: true
      },
      'entrada_pedido[modelo]':{
        required: false,
        minlength: 3,
        maxlength: 150
      },
      'tiempoMG':{
        required: false
      },
      'cantidadG':{
        required: false,
        minlength: 1,
        maxlength: 3,
        digits: true
      },
      'tiempoM':{
        required: false
      },
      'cantidaM':{
        required: false,
        minlength: 1,
        maxlength: 3,
        digits: true
      }
    },
    submitHandler: function(form) {
     if ($(form).children().children().children('select#tiempoM').val() != '' || $(form).children().children().children('input#cantidaM').val() != '') {
        if ($(form).children().children().children('select#tiempoM').val() != '' && $(form).children().children().children('input#cantidaM').val() != '') {
          $('.errorTiempoM').empty();
          $(form).children().children('input#entrada_pedido_intervaloMantenimiento').val($(form).children().children().children('select#tiempoM').val()+'_'+$(form).children().children().children('input#cantidaM').val())
        }else{
          $('.errorTiempoM').html('Registre el tiempo y cantidad de mantenimiento.');
        }
      }
      //validación de producto pedido
      if($(form).children().children().children('select#tiempoMG').val() != '' || $(form).children().children().children('input#cantidadG').val() != '') {
        if ($(form).children().children().children('select#tiempoMG').val() != '' && $(form).children().children().children('input#cantidadG').val() != '') {
          $('.errorTiempoMG').empty();
          var cantidad=$(form).children().children().children('input#cantidadG').val();
          var tiempo=$(form).children().children().children('select#tiempoMG').val();
          //captura la fecha de facturacion
          var fechaFact= $(form).children().children("input#fechaFacturacion").val();//'{{pedido.fechaFactura | date()}}';
          //Pasa a tiempo la fecha de facturacion
          fechaParseada=Date.parse(fechaFact);  
          //Pasa a fecha la fecha de facturacion                    
          fecha = new Date(fechaParseada);
          if (tiempo == 'DIA') {
              var cantidadFin=Number(cantidad); 
              //Suma los dias a la fecha 
              fecha.setDate(fecha.getDate() + cantidadFin);
              //Da formato de timestamp en la fecha 
              finalFecha=new Date(fecha);//.toUTCString();
              var FF=moment(finalFecha).format();
              $(form).children().children().children('input#entrada_pedido_garantia').val(FF);
              //alert($(form).children().children().children('input#entrada_pedido_garantia').val());
          }else if (tiempo == 'MES') {  
              var cantidadFin=Number(cantidad); 
              //Suma los meses a la fecha 
              fecha.setMonth(fecha.getMonth()+cantidadFin);
              //Da formato de timestamp en la fecha 
              finalFecha=new Date(fecha);//.toUTCString();
              var FF=moment(finalFecha).format();
              $(form).children().children().children('input#entrada_pedido_garantia').val(FF);
          }else if (tiempo == 'ANIO') {
              var cantidadFin=Number(cantidad); 
              //Suma los anios a la fecha 
              fecha.setFullYear(fecha.getFullYear()+cantidadFin);
              //Da formato de timestamp en la fecha 
              finalFecha=new Date(fecha);//.toLocaleString();
              var FF=moment(finalFecha).format();
              $(form).children().children().children('input#entrada_pedido_garantia').val(FF);
          }    
        }else{
          $('.errorTiempoMG').html('Registre el tiempo y cantidad de garantia.');
        }
      }

      
      var url = '/entradaPedido/'+$(form).children().children('select#entrada_pedido_idPedido').val()+'/pedido';  
      $.ajax({
          // especifica si será una petición POST o GET
          type: 'POST',
           // la información a enviar
          data: $(form).serialize(),
          //Url de petición envio
          url: url,          
          // el tipo de información que se espera de respuesta
          dataType : 'json',
          beforeSend: function () {
            $('body').addClass('loading'); //Agregamos la clase loading al body
          },
          // código a ejecutar si la petición es satisfactoria;
          success : function(data) {
            $('body').removeClass('loading'); 
            $('.resp').removeAttr('hidden').removeClass('alert-danger').addClass('alert-success').html(data.mensaje);
            $(form).children().children().children('select#entrada_pedido_idInsumo').val('').change();
            $(form).children().children().children('input#entrada_pedido_registroInvima').val('');
            $(form).children().children().children('input#entrada_pedido_lote').val('');
            $(form).children().children().children().children('input#entrada_pedido_fechaVencimiento').val('');
            $(form).children().children().children('input#entrada_pedido_cantidad').val('');
            $(form).children().children().children('select#entrada_pedido_unidadMedida').val('').change();
            $(form).children().children().children('select#tiempoM').val('').change();
            $(form).children().children().children('select#tiempoMG').val('').change();
            $(form).children().children().children('input#cantidadG').val('');
            $(form).children().children().children('input#cantidaM').val('');
            $(form).children().children().children('input#entrada_pedido_modelo').val('');
            $(form).children().children().children('select#entrada_pedido_idClinicaUbicacion').val('').change();
            $("#datosEntradaPedido").html(data.entradasPedidos);
          }
      }); 
    }  
  });



  var validatorEntradaPedidoModificacion = $('#formEntradaPedidoEdit').validate({
    rules:{
      'entrada_pedido[idInsumo]':{
        required: true
      },
      'entrada_pedido[registroInvima]':{
        minlength: 9,
        maxlength:17,
        lotePedido: true,
        required: false
      },
      'entrada_pedido[lote]':{
        minlength: 4,
        maxlength: 20,
        lotePedido: true,
        required: false
      },
      'entrada_pedido[fechaVencimiento]':{
        required: false
      },
      'entrada_pedido[cantidad]':{
        required: true, 
        minlength: 1,
        maxlength: 10,
        digits:true
      },
      'entrada_pedido[unidadMedida]':{
        required: true
      },
      'entrada_pedido[idClinicaUbicacion]':{
        required: true
      },
      'entrada_pedido[modelo]':{
        required: false,
        minlength: 3,
        maxlength: 150
      },
      'tiempoMGEdt':{
        required: false
      },
      'cantidadGEdt':{
        required: false,
        minlength: 1,
        maxlength: 3,
        digits: true
      },
      'tiempoMEdt':{
        required: false
      },
      'cantidaMEdt':{
        required: false,
        minlength: 1,
        maxlength: 3,
        digits: true
      }
    },
    submitHandler: function(form) {
      if ($(form).children().children().children('select#tiempoMEdt').val() != '' || $(form).children().children().children('input#cantidaMEdt').val() != '') {
        if ($(form).children().children().children('select#tiempoMEdt').val() != '' && $(form).children().children().children('input#cantidaMEdt').val() != '') {
          $('.errorTiempoMEdt').empty();
          $(form).children().children('input#entrada_pedido_intervaloMantenimiento').val($(form).children().children().children('select#tiempoMEdt').val()+'_'+$(form).children().children().children('input#cantidaMEdt').val())
        }else{
          $('.errorTiempoMEdt').html('Registre el tiempo y cantidad de mantenimiento.');
        }
      }else{
        $(form).children().children('input#entrada_pedido_intervaloMantenimiento').val('');
      }
      //validación de producto pedido
      if($(form).children().children().children('select#tiempoMGEdt').val() != '' || $(form).children().children().children('input#cantidadGEdt').val() != '') {
        if ($(form).children().children().children('select#tiempoMGEdt').val() != '' && $(form).children().children().children('input#cantidadGEdt').val() != '') {
          $('.errorTiempoMGEdt').empty();
          var cantidad=$(form).children().children().children('input#cantidadGEdt').val();
          var tiempo=$(form).children().children().children('select#tiempoMGEdt').val();
          //captura la fecha de facturacion
          var fechaFact= $(form).children().children("input#fechaFacturacion").val();//'{{pedido.fechaFactura | date()}}';
          //Pasa a tiempo la fecha de facturacion
          fechaParseada=Date.parse(fechaFact);  
          //Pasa a fecha la fecha de facturacion                    
          fecha = new Date(fechaParseada);
          if (tiempo == 'DIA') {
              var cantidadFin=Number(cantidad); 
              //Suma los dias a la fecha 
              fecha.setDate(fecha.getDate() + cantidadFin);
              //Da formato de timestamp en la fecha 
              finalFecha=new Date(fecha);//.toUTCString();
              var FF=moment(finalFecha).format();
              $(form).children().children('input#entrada_pedido_garantia').val(FF);
          }else if (tiempo == 'MES') {  
              var cantidadFin=Number(cantidad); 
              //Suma los meses a la fecha 
              fecha.setMonth(fecha.getMonth()+cantidadFin);
              //Da formato de timestamp en la fecha 
              finalFecha=new Date(fecha);//.toUTCString();
              var FF=moment(finalFecha).format();
              $(form).children().children('input#entrada_pedido_garantia').val(FF);
          }else if (tiempo == 'ANIO') {
              var cantidadFin=Number(cantidad); 
              //Suma los anios a la fecha 
              fecha.setFullYear(fecha.getFullYear()+cantidadFin);
              //Da formato de timestamp en la fecha 
              finalFecha=new Date(fecha);//.toLocaleString();
              var FF=moment(finalFecha).format();
              $(form).children().children('input#entrada_pedido_garantia').val(FF);
          }    
        }else{
          $('.errorTiempoMG').html('Registre el tiempo y cantidad de garantia.');
        }
      }else{
        $(form).children().children('input#entrada_pedido_garantia').val('').change();
      }      
      $('body').addClass('loading');
      $(form).attr('action','/entradaPedido/'+$('#idEntradaPedido').val()+'/edit');
      $(form)[0].submit();    
    }  
  });  


});

$(document).ready(function(){
  var validatorSalidaClinica = $('#formSalidaClinica').validate({
    rules:{
      'salida[idEntradaPedido]':{
        required: true
      },
      'salida[cantidad]':{
        minlength: 1,
        maxlength:10,
        required: true,
        digits: true
      },
      'salida[idConsultorio]':{
        required: false
      },
      'salida[idPaciente]':{
        required: false
      }
    },
    submitHandler: function(form) {
      if ($(form).children().children().children().children('input#ckeckPaciente').attr('checked') == 'checked') {
        valor=$(form).children().children().children('select#salida_idPaciente').val();
        if (valor != '') {
          $('.errorIdPaciente').empty();
          $('body').addClass('loading');
          $(form)[0].submit();     
        }else{
          $('.errorIdPaciente').html('Este campo es obligatorio.');
        }
      }else{
        valor=$(form).children().children().children('select#salida_idConsultorio').val();
        if (valor != '') {
          $('.errorIdConsultorio').empty();
          $('body').addClass('loading');
          $(form)[0].submit();     
        }else{
          $('.errorIdConsultorio').html('Este campo es obligatorio.');
        }
      }
    }  
  });  

  var validatorModificarSalidaClinica = $('#formSalidaClinicaEdit').validate({
    rules:{
      'salida[idEntradaPedido]':{
        required: true
      },
      'salida[cantidad]':{
        minlength: 1,
        maxlength:10,
        required: true,
        digits: true
      },
      'salida[idConsultorio]':{
        required: false
      },
      'salida[idPaciente]':{
        required: false
      }
    },
    submitHandler: function(form) { 
      if ($(form).children().children().children().children('input#ckeckPaciente').attr('checked') == 'checked') {
        valor=$(form).children().children().children('select#salida_idPaciente').val();
        if (valor != '') {
          $('.errorIdPaciente').empty();
          $('body').addClass('loading');
          $(form).attr('action','/salida/'+$('#idSalida').val()+'/edit');
          $(form)[0].submit();     
        }else{
          $('.errorIdPaciente').html('Este campo es obligatorio.');
        }
      }else{
        valor=$(form).children().children().children('select#salida_idConsultorio').val();
        if (valor != '') {
          $('.errorIdConsultorio').empty();
          $('body').addClass('loading');
          $(form).attr('action','/salida/'+$('#idSalida').val()+'/edit');
          $(form)[0].submit();     
        }else{
          $('.errorIdConsultorio').html('Este campo es obligatorio.');
        }
      }
      
    }  
  });  

  var validatorSalidaConsultorio = $('#formSalidaConsultorio').validate({
    rules:{
      'salida_consultorio[idSalida]':{
        required: true
      },
      'salida_consultorio[cantidad]':{
        minlength: 1,
        maxlength:10,
        required: true,
        digits: true
      },
      'salida_consultorio[idConsultorio]':{
        required: true
      },
      'salida_consultorio[idUsuarioIngresa]':{
        required: false
      }
    },
    submitHandler: function(form) {
      $(form).attr('action','/salidaConsultorio/new');
      $('body').addClass('loading');
      $(form)[0].submit();   
    }  
  });  

  var validatorModificarSalidaConsultorio = $('#formSalidaConsultorioEdit').validate({
    rules:{
      'salida_consultorio[idSalida]':{
        required: true
      },
      'salida_consultorio[cantidad]':{
        minlength: 1,
        maxlength:10,
        required: true,
        digits: true
      },
      'salida_consultorio[idConsultorio]':{
        required: true
      },
      'salida_consultorio[idUsuarioIngresa]':{
        required: false
      }
    },
    submitHandler: function(form) { 
      $(form).attr('action','/salidaConsultorio/'+$('#idSalidaConsultorio').val()+'/edit');
      $('body').addClass('loading');
      $(form)[0].submit(); 
    }  
  });  
  var validatorRequerimiento = $('#formRequerimiento').validate({
    rules:{
      'requerimiento[idSedeClinica]':{
        required:true
      },
      'requerimiento[idUsuarioIngresa]':{
        required: true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  


  var validatorRequerimientoModificar = $('#formRequerimientoEdit').validate({
    rules:{
      'requerimiento[idSedeClinica]':{
        required:true
      },
      'requerimiento[idUsuarioIngresa]':{
        required: true
      }
    },
    submitHandler: function(form) {
      $(form).attr('action','/requerimiento/'+$('#idRequerimiento').val()+'/edit');
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  });  

  var validatorEntradaRequerimiento = $('#formNuevaEntradaRequerimiento').validate({
    rules:{
      'elemento_requerimiento[idNombreInsumo]':{
        required: true
      },
      'elemento_requerimiento[idPresentacion]':{
        required: false
      },
      'elemento_requerimiento[idMarca]':{
        required: false
      },
      'elemento_requerimiento[cantidad]':{
        required: true, 
        minlength: 1,
        maxlength: 10,
        digits:true
      }
    },
    submitHandler: function(form) {
      
      var url = '/elementoRequerimiento/'+$(form).children().children('select#elemento_requerimiento_idRequerimiento').val()+'/requerimiento';  
      $.ajax({
          // especifica si será una petición POST o GET
          type: 'POST',
           // la información a enviar
          data: $(form).serialize(),
          //Url de petición envio
          url: url,          
          // el tipo de información que se espera de respuesta
          dataType : 'json',
          beforeSend: function () {
            $('body').addClass('loading'); //Agregamos la clase loading al body
          },
          // código a ejecutar si la petición es satisfactoria;
          success : function(data) {
            $('body').removeClass('loading'); 
            if (data.error) {  
              $(form).children().children().children().children('select#elemento_requerimiento_idNombreInsumo').val('').change();
              $(form).children().children().children().children('select#elemento_requerimiento_idMarca').val('').change();
              $(form).children().children().children().children('select#elemento_requerimiento_idPresentacion').val('').change();
              $(form).children().children().children().children('input#elemento_requerimiento_cantidad').val('');
              $("#datosEntradaRequerimiento").html(data.elementoRequerimientos);
              $('.resp').removeAttr('hidden').html(data.error);
            }else{
              $('.resp').removeAttr('hidden').removeClass('alert-danger').addClass('alert-success').html(data.mensaje);
              $(form).children().children().children().children('select#elemento_requerimiento_idNombreInsumo').val('').change();
              $(form).children().children().children().children('select#elemento_requerimiento_idMarca').val('').change();
              $(form).children().children().children().children('select#elemento_requerimiento_idPresentacion').val('').change();
              $(form).children().children().children().children('input#elemento_requerimiento_cantidad').val('');
              $("#datosEntradaRequerimiento").html(data.elementoRequerimientos);
              if (data.elementoRequerimientos.length < 800) {
                location.reload();
              }
            }
          }
      }); 
    }  
  });


  var validatorEntradaRequerimientoModificar = $('#formEntradaRequerimientoEdit').validate({
    rules:{
      'elemento_requerimiento[idNombreInsumo]':{
        required: true
      },
      'elemento_requerimiento[idPresentacion]':{
        required: false
      },
      'elemento_requerimiento[idMarca]':{
        required: false
      },
      'elemento_requerimiento[cantidad]':{
        required: true, 
        minlength: 1,
        maxlength: 10,
        digits:true
      }
    },
    submitHandler: function(form) {
      $(form).attr('action','/elementoRequerimiento/'+$('#idEntradaRequerimiento').val()+'/edit');
      $('body').addClass('loading');
      $(form)[0].submit();       
    }  
  });


  var validatorEstadoConfirmacionRequerimiento = $('#formEstadoConfirmacionRequerimiento').validate({
    rules:{
      'estadoValorConfirmacion':{
        required: true
      },
      'idProveedorOrden':{
        required: false
      }
    },
    submitHandler: function(form) {
      $(form).attr('action','/elementoRequerimiento/'+$('#idElementoRequerimiento').val()+'/editarEstadoConfirmacion');
      if ($(form).children().children().children('select#estadoValorConfirmacion').val() == 'CONFIRMADO') {
        if ($(form).children().children().children('select#idProveedorOrden').val() != '') {
          $('body').addClass('loading');
          $(form)[0].submit(); 
        }else{
          $('.idProveedorOrdenError').html('Este campo es obligatorio.');
        }
      }else{        
        $('body').addClass('loading');
        $(form)[0].submit(); 
      }
    }  
  });

  var validatorMantenimiento = $('#formMantenimiento').validate({
    rules:{
      'mantenimiento[idEntradaPedido]':{
        required: true
      },
      'mantenimiento[fechaProgramada]':{
        required: true
      },
      'mantenimiento[fechaEjecucion]':{
        required: false,
        fechaMayorIgualMante: true
      },
      'mantenimiento[tipoMantenimiento]':{
        required: true
      },
      'mantenimiento[idUsuarioAprueba]':{
        required: false
      },
      'mantenimiento[responsable]':{
        required: false,
        minlength: 3,
        maxlength:150
      },
      'mantenimiento[idEstadoMantenimiento]':{
        required: true
      },
      'mantenimiento[descripcion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      },
      'mantenimiento[observacion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      if ($(form).children().children().children().children('input#mantenimiento_fechaEjecucion').val() != '') {
        if ($(form).children().children().children('select#mantenimiento_idUsuarioAprueba').val() != '') {
          if ($(form).children().children().children('input#mantenimiento_responsable').val() != '') {
            if ($(form).children().children().children('textarea#mantenimiento_descripcion').val() != '') {
              $('body').addClass('loading');
              $(form)[0].submit(); 
            }else{
              $('.errorDescripcion').html('Este campo es obligatorio.');
              $('.labelDescripcionObli').removeAttr('hidden');
            } 
          }else{
            $('.errorResponsable').html('Este campo es obligatorio.');
            $('.labelResponsableObli').removeAttr('hidden');
          }
        }else{
          $('.errorUsuarioAprueba').html('Este campo es obligatorio.');
          $('.labelUsuariobleObli').removeAttr('hidden');
        }
      }else{        
        $('body').addClass('loading');
        $(form)[0].submit(); 
      }
    }  
  });
  var validatorMantenimientoModificar = $('#formMantenimientoEdit').validate({
    rules:{
      'mantenimiento[idEntradaPedido]':{
        required: true
      },
      'mantenimiento[fechaProgramada]':{
        required: true
      },
      'mantenimiento[fechaEjecucion]':{
        required: true,
        fechaMayorIgualMante: true
      },
      'mantenimiento[tipoMantenimiento]':{
        required: true
      },
      'mantenimiento[idUsuarioAprueba]':{
        required: true
      },
      'mantenimiento[responsable]':{
        required: true,
        minlength: 3,
        maxlength:150
      },
      'mantenimiento[idEstadoMantenimiento]':{
        required: true
      },
      'mantenimiento[descripcion]':{
        required: true,
        minlength: 3,
        maxlength:2000
      },
      'mantenimiento[observacion]':{
        required: false,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $(form).attr('action','/mantenimiento/'+$('#idMantenimiento').val()+'/edit');
      $('body').addClass('loading');
      $(form)[0].submit();       
    }  
  });


  var validatorAmbitoRealizacionCrear = $('#formAmbitoRealizacion').validate({
      rules: {
        'ambitoRealizacion[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });

  var validatorAmbitoRealizacionEditar = $('#formAmbitoRealizacionEdit').validate({
      rules: {
        'ambitoRealizacion[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/ambitoRealizacion/'+$('#idAmbitoRealizacion').val()+'/edit');
        $(form)[0].submit();
        
      }
  });

  var validatorCausaExternaCrear = $('#formCausaExterna').validate({
      rules: {
        'causaExterna[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });

  var validatorCausaExternaEditar = $('#formCausaExternaEdit').validate({
      rules: {
        'causaExterna[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/causaExterna/'+$('#idCausaExterna').val()+'/edit');
        $(form)[0].submit();
        
      }
  });

  var validatorCupsCrear = $('#formCups').validate({
      rules: {
        'cups[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });

  var validatorCupsEditar = $('#formCupsEdit').validate({
      rules: {
        'cups[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/cups/'+$('#idCups').val()+'/edit');
        $(form)[0].submit();
        
      }
  });

  var validatorFinalidadProcedimientoCrear = $('#formFinalidadProcedimiento').validate({
      rules: {
        'finalidadProcedimiento[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });

  var validatorFinalidadProcedimientoEditar = $('#formFinalidadProcedimientoEdit').validate({
      rules: {
        'finalidadProcedimiento[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/finalidadProcedimiento/'+$('#idFinalidadProcedimiento').val()+'/edit');
        $(form)[0].submit();
        
      }
  });

  var validatorFormaRealizacionCrear = $('#formFormaRealizacion').validate({
      rules: {
        'formaRealizacion[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });

  var validatorFormaRealizacionEditar = $('#formFormaRealizacionEdit').validate({
      rules: {
        'formaRealizacion[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/formaRealizacion/'+$('#idFormaRealizacion').val()+'/edit');
        $(form)[0].submit();
        
      }
  });

  var validatorPersonaAtiendeCrear = $('#formPersonaAtiende').validate({
      rules: {
        'personaAtiende[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });

  var validatorPersonaAtiendeEditar = $('#formPersonaAtiendeEdit').validate({
      rules: {
        'personaAtiende[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/personaAtiende/'+$('#idPersonaAtiende').val()+'/edit');
        $(form)[0].submit();
        
      }
  });

  var validatorCupsCrear = $('#formCups').validate({
      rules: {
        'cups[idCups]':{
          minlength: 1,
          maxlength:10,
          required:true,
          digits: true
        },
        'cups[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });

  var validatorCupsEditar = $('#formCupsEdit').validate({
      rules: {
        'cups[idCups]':{
          minlength: 1,
          maxlength:10,
          required:true,
          digits: true
        },
        'cups[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/cups/'+$('#idCups').val()+'/edit');
        $(form)[0].submit();
        
      }
  });

  var validatorCupDiagnosticoCrear = $('#formCupDiagnostico').validate({
      rules: {
        'cup_diagnostico[idCups]':{
          required:true
        },
        'cup_diagnostico[idRips]':{
          required:true
        },
        'cup_diagnostico[idDxr1]':{
          required: false
        },
        'cup_diagnostico[idDxr2]':{
          required: false
        },
        'cup_diagnostico[idDxr3]':{
          required: false
        },
        'cup_diagnostico[idCausaExterna]':{
          required: false
        },
        'cup_diagnostico[idAmbitoRealizacion]':{
          required: false
        },
        'cup_diagnostico[idFormaRealizacion]':{
          required: false
        },
        'cup_diagnostico[idPersonaAtiende]':{
          required: false
        },
        'cup_diagnostico[idFinalidadProcedimiento]':{
          required: false
        },
        'cup_diagnostico[idUsuarioIngresa]':{
          required: true
        }
      },
      submitHandler: function(form) {
        $(form).attr('action','/cupDiagnostico/new');
        var idRip=$(form).children().children().children('select#cup_diagnostico_idRips').val();
        var idDxr1=$(form).children().children().children('select#cup_diagnostico_idDxr1').val();
        var idDxr2=$(form).children().children().children('select#cup_diagnostico_idDxr2').val();
        var idDxr3=$(form).children().children().children('select#cup_diagnostico_idDxr3').val();
        if (idRip == idDxr1 && idDxr1 != '') {
          $('.idDxr2Error').empty();
          $('.idDxr3Error').empty();
          $('.idDxr1Error').html('Este campo no puede ser igual al cie principal.');
        }else if (idRip == idDxr2 && idDxr2 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr3Error').empty();
          $('.idDxr2Error').html('Este campo no puede ser igual al cie principal.');
        }else if (idRip == idDxr3 && idDxr3 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr2Error').empty();
          $('.idDxr3Error').html('Este campo no puede ser igual al cie principal.');
        }else if (idDxr1 == idDxr2 && idDxr1 != '' && idDxr2 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr3Error').empty();
          $('.idDxr2Error').html('Este campo no puede ser igual al cie realizado I.');
        }else if (idDxr1 == idDxr3 && idDxr1 != ''  && idDxr3 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr2Error').empty();
          $('.idDxr3Error').html('Este campo no puede ser igual al cie realizado I.');          
        }else if (idDxr2 == idDxr3  && idDxr2 != '' && idDxr3 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr2Error').empty();
          $('.idDxr3Error').html('Este campo no puede ser igual al cie realizado II.');          
        }
        if (idRip != idDxr1 && idRip != idDxr2 && idRip != idDxr3 && idDxr1 != idDxr2 && idDxr2 != idDxr3  && idDxr1 != ''  && idDxr2 != ''  && idDxr3 != '') {
          $('body').addClass('loading');
          $(form)[0].submit();
        }else if (idRip != idDxr1 && idRip != idDxr2 && idDxr1 != idDxr2 && idDxr1 != ''  && idDxr2 != ''  && idDxr3 == '') {
          $('body').addClass('loading');
          $(form)[0].submit();
        }else if (idRip != idDxr1 && idDxr1 != ''  && idDxr2 == ''  && idDxr3 == '') {
          $('body').addClass('loading');
          $(form)[0].submit();
        }else if (idDxr1 == ''  && idDxr2 == ''  && idDxr3 == '') {
          $('body').addClass('loading');
          $(form)[0].submit();
        }
      }
  });

  var validatorCupDiagnosticoEditar = $('#formCupDiagnosticoEdit').validate({
      rules: {
        'cup_diagnostico[idCups]':{
          required:true
        },
        'cup_diagnostico[idRips]':{
          required:true
        },
        'cup_diagnostico[idDxr1]':{
          required: false
        },
        'cup_diagnostico[idDxr2]':{
          required: false
        },
        'cup_diagnostico[idDxr3]':{
          required: false
        },
        'cup_diagnostico[idCausaExterna]':{
          required: false
        },
        'cup_diagnostico[idAmbitoRealizacion]':{
          required: false
        },
        'cup_diagnostico[idFormaRealizacion]':{
          required: false
        },
        'cup_diagnostico[idPersonaAtiende]':{
          required: false
        },
        'cup_diagnostico[idFinalidadProcedimiento]':{
          required: false
        },
        'cup_diagnostico[idUsuarioIngresa]':{
          required: true
        }
      },
      submitHandler: function(form) {
        $(form).attr('action','/cupDiagnostico/'+$('#idCupDiagnostico').val()+'/edit');
        var idRip=$(form).children().children().children('select#cup_diagnostico_idRips').val();
        var idDxr1=$(form).children().children().children('select#cup_diagnostico_idDxr1').val();
        var idDxr2=$(form).children().children().children('select#cup_diagnostico_idDxr2').val();
        var idDxr3=$(form).children().children().children('select#cup_diagnostico_idDxr3').val();
        if (idRip == idDxr1 && idDxr1 != '') {
          $('.idDxr2Error').empty();
          $('.idDxr3Error').empty();
          $('.idDxr1Error').html('Este campo no puede ser igual al cie principal.');
        }else if (idRip == idDxr2 && idDxr2 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr3Error').empty();
          $('.idDxr2Error').html('Este campo no puede ser igual al cie principal.');
        }else if (idRip == idDxr3 && idDxr3 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr2Error').empty();
          $('.idDxr3Error').html('Este campo no puede ser igual al cie principal.');
        }else if (idDxr1 == idDxr2 && idDxr1 != '' && idDxr2 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr3Error').empty();
          $('.idDxr2Error').html('Este campo no puede ser igual al cie realizado I.');
        }else if (idDxr1 == idDxr3 && idDxr1 != ''  && idDxr3 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr2Error').empty();
          $('.idDxr3Error').html('Este campo no puede ser igual al cie realizado I.');          
        }else if (idDxr2 == idDxr3  && idDxr2 != '' && idDxr3 != '') {
          $('.idDxr1Error').empty();
          $('.idDxr2Error').empty();
          $('.idDxr3Error').html('Este campo no puede ser igual al cie realizado II.');          
        }
        if (idRip != idDxr1 && idRip != idDxr2 && idRip != idDxr3 && idDxr1 != idDxr2 && idDxr2 != idDxr3  && idDxr1 != ''  && idDxr2 != ''  && idDxr3 != '') {
          $('body').addClass('loading');
          $(form)[0].submit();
        }else if (idRip != idDxr1 && idRip != idDxr2 && idDxr1 != idDxr2 && idDxr1 != ''  && idDxr2 != ''  && idDxr3 == '') {
          $('body').addClass('loading');
          $(form)[0].submit();
        }else if (idRip != idDxr1 && idDxr1 != ''  && idDxr2 == ''  && idDxr3 == '') {
          $('body').addClass('loading');
          $(form)[0].submit();
        }else if (idDxr1 == ''  && idDxr2 == ''  && idDxr3 == '') {
          $('body').addClass('loading');
          $(form)[0].submit();
        }
        
      }
  });


  var validatorCategoriaTratamientoCrear = $('#formCategoriaTratamiento').validate({
      rules: {
        'categoria_tratamiento[idArea]':{
          required:true
        },
        'categoria_tratamiento[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });

  var validatorCategoriaTratamientoEditar = $('#formCategoriaTratamientoEdit').validate({
      rules: {
        'categoria_tratamiento[idArea]':{
          required:true
        },
        'categoria_tratamiento[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form).attr('action','/categoriaTratamiento/'+$('#idCategoriaTratamiento').val()+'/edit');
        $(form)[0].submit();
        
      }
  });

  var validatorTratamiento = $('#formTratamiento').validate({
    rules:{
      'tratamiento[valor]':{
        minlength: 1,
        maxlength:10,
        digits:true,
        required:true
      },
      'tratamiento[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'tratamiento[idCategoriaTratamiento]':{
        required: true
      },
      'tratamiento[idUsuarioIngresa]':{
        required: true
      },
      'tratamiento[estado]':{
        required: true
      }
    },
    submitHandler: function(form) {
      var url = '/tratamiento/'+$(form).children().children('select#tratamiento_idCategoriaTratamiento').val()+'/new';  
      $.ajax({
          // especifica si será una petición POST o GET
          type: 'POST',
           // la información a enviar
          data: $(form).serialize(),
          //Url de petición envio
          url: url,          
          // el tipo de información que se espera de respuesta
          dataType : 'json',
          beforeSend: function () {
            $('body').addClass('loading'); //Agregamos la clase loading al body
          },
          // código a ejecutar si la petición es satisfactoria;
          success : function(data) {
            $('body').removeClass('loading');   
            if (data.error == 'vacio') {                                 
             $('.resp').removeAttr('hidden').removeClass('alert-danger').addClass('alert-success').html(data.mensaje);
             $(form).children().children().children('input#tratamiento_nombre').val('');
             $(form).children().children().children('input#tratamiento_valor').val('');
             $("#datosTratamientos").html(data.tratamientos);
            }else{
              $('.resp').removeAttr('hidden').removeClass('alert-success').addClass('alert-danger').html(data.error);
              $(form).children().children().children('input#tratamiento_nombre').val('');
              $(form).children().children().children('input#tratamiento_valor').val('');
            } 
          }
        
      });    
    }  
  });  


});


$(document).ready(function(){
  var validatorCie = $('#formCieRip').validate({
    rules:{
      'cie_rip[codigo]':{
        required: true,
        minlength: 2,
        maxlength:75
      },
      'cie_rip[nombreCieRip]':{
        minlength: 2,
        maxlength:250,
        required: true
      },
      'cie_rip[limiteInferior]':{
        minlength: 1,
        maxlength:10,
        required: true,
        digits: true
      },
      'cie_rip[limiteSuperior]':{
        minlength: 1,
        maxlength:10,
        required: true,
        digits: true
      },
      'cie_rip[sexo]':{
        required: true
      },
      'cie_rip[desripcion]':{
        minlength: 3,
        maxlength:2000,
        required: false
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();   
    }  
  });  

  var validatorModificarCieRip = $('#formCieRipEdit').validate({
    rules:{
      'cie_rip[codigo]':{
        required: true,
        minlength: 2,
        maxlength:75
      },
      'cie_rip[nombreCieRip]':{
        minlength: 2,
        maxlength:250,
        required: true
      },
      'cie_rip[limiteInferior]':{
        minlength: 1,
        maxlength:10,
        required: true,
        digits: true
      },
      'cie_rip[limiteSuperior]':{
        minlength: 1,
        maxlength:10,
        required: true,
        digits: true
      },
      'cie_rip[sexo]':{
        required: true
      },
      'cie_rip[desripcion]':{
        minlength: 3,
        maxlength:2000,
        required: false
      }
    },
    submitHandler: function(form) { 
      $(form).attr('action','/cie/'+$('#idCieRip').val()+'/edit');
      $('body').addClass('loading');
      $(form)[0].submit(); 
    } 
  });

  var validatorAgendaUsuario = $('#formAgendaUsuario').validate({
      rules: {
        'agenda_usuario[dia]':{
          required:true
        },
        'agenda_usuario[horaInicio]':{
          required:true
        }
        ,
        'agenda_usuario[horaFin]':{
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });
  var validatorAgendaUsuarioModificar = $('#formAgendaUsuarioEdit').validate({
      rules: {
        'agenda_usuario[dia]':{
          required:true
        },
        'agenda_usuario[horaInicio]':{
          required:true
        }
        ,
        'agenda_usuario[horaFin]':{
          required:true
        }
      },
      submitHandler: function(form) {
        if (typeof($(form).children().children().children().children('input#agenda_usuario_horaInicio').attr('disabled')) == "undefined") {
          $('.respuesta').attr('hidden','hidden');
          $(form).attr('action','/agendaUsuario/'+$('#idAgendaUsuario').val()+'/edit');
          $('body').addClass('loading');
          $(form)[0].submit();
        }else{
          $('.respuesta').removeAttr('hidden').html('No ha modificado la agenda!..');
        }
        
        
      }
  });
  //Modificar la agenda selecionada por el usuario
  var validatorUsuarioModificarAgenda = $('#cambioAgenda').validate({
      rules: {
        'idAgenda':{
          required:true
        },
        'idUsuario':{
          required:true
        }
      },
      submitHandler: function(form) {  
        $('body').addClass('loading');
        $(form)[0].submit();
      }
  });
  //Modificar el acuerdo porcentaje selecionada por el usuario
  var validatorUsuarioModificarAcuerdoP = $('#cambioAcuerdoPorcentaje').validate({
      rules: {
        'idAcuerdoPorcentaje':{
          required:false
        },
        'idUsuario':{
          required:true
        }
      },
      submitHandler: function(form) {  
        $('body').addClass('loading');
        $(form)[0].submit();
      }
  });
  //Modificar el acuerdo valor selecionada por el usuario
  var validatorUsuarioModificarAcuerdoV = $('#cambioAcuerdoValor').validate({
      rules: {
        'idAcuerdoValor':{
          required:false
        },
        'idUsuario':{
          required:true
        }
      },
      submitHandler: function(form) {  
        $('body').addClass('loading');
        $(form)[0].submit();
      }
  });
});


$(document).ready(function(){
  var validatorTipoDocumento = $('#formTipoDocumento').validate({
      rules: {
        'tipo_documento[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'tipo_documento[nomenclatura]':{
          minlength: 1,
          maxlength:20,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });
  var validatorTipoDocumentoModificar = $('#formTipoDocumentoEdit').validate({
      rules: {
        'tipo_documento[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'tipo_documento[nomenclatura]':{
          minlength: 1,
          maxlength:20,
          required:true
        }
      },
      submitHandler: function(form) {
        $(form).attr('action','/tipoDocumento/'+$('#idT').val()+'/edit');
        $('body').addClass('loading');
        $(form)[0].submit();
      }
  });

  var validatorTipoUsuario = $('#formTipoUsuario').validate({
      rules: {
        'tipo_usuario[nombre]':{
          minlength: 3,
          maxlength:150,
          required:true
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        $(form)[0].submit();
        
      }
  });
  var validatorTipoUsuarioModificar = $('#formTipoUsuarioEdit').validate({
      rules: {
        'agenda_usuario[dia]':{
          required:true
        },
        'agenda_usuario[horaInicio]':{
          required:true
        }
        ,
        'agenda_usuario[horaFin]':{
          required:true
        }
      },
      submitHandler: function(form) {
        $(form).attr('action','/tipoUsuario/'+$('#idTipoUsuario').val()+'/edit');
        $('body').addClass('loading');
        $(form)[0].submit();
      }
  });
});

$(document).ready(function(){
  var validatorUsuario = $('#formUsuario').validate({
      rules: {
        'usuario[nombres]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'usuario[apellidos]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'usuario[fechaNacimiento]':{
          required:true
        },
        'usuario[tipoentificacion]':{
          required:true
        },
        'usuario[numeroIdentificacion]':{
          required:true,
          minlength: 9,
          maxlength:15,
          digits:true
        },
        'usuario[email]':{
          minlength: 10,
          maxlength:75,
          email:true,
          required: true
        },
        'usuario[username]':{
          minlength: 1,
          maxlength:150,
          required:true
        },
        'usuario[idPerfil]':{
          required:true
        },
        'usuario[genero]':{
          required:true
        },
        'usuario[movil]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:true
        },
        'usuario[idAgendaUsuario]':{
          required:true
        },
        'usuario[colorAgenda]':{
          required:true
        },
        'usuario[direccion]':{
          minlength: 6,
          maxlength:150,
          required:true
        },
        'usuario[numeroContacto]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:false
        },
        'usuario[nombreContacto]':{
          minlength: 3,
          maxlength:150,
          required:false
        },
        'usuario[idSucursal]':{
          required:true
        },
        'usuario[idAcuerdoValor]':{
          required:false
        },
        'usuario[idAcuerdoPorc]':{
          required:false
        },
        'usuario[idEspecialidad]':{
          required:false
        },
        'usuario[tipoUsuario]':{
          required:true
        },
        'usuario[password]':{
          required:false
        },
        'usuario[firmaProfesional]':{
          required:false
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');
        //alert($(form).serilize());
        $(form)[0].submit();
        
      }
  });
  var validatorUsuarioModificar = $('#formUsuarioEdit').validate({
      rules: {
        'usuario[nombres]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'usuario[apellidos]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'usuario[fechaNacimiento]':{
          required:true
        },
        'usuario[tipoentificacion]':{
          required:true
        },
        'usuario[numeroIdentificacion]':{
          required:true,
          minlength: 9,
          maxlength:15,
          digits:true
        },
        'usuario[email]':{
          minlength: 10,
          maxlength:75,
          email:true,
          required: true
        },
        'usuario[username]':{
          minlength: 1,
          maxlength:150,
          required:true
        },
        'usuario[idPerfil]':{
          required:true
        },
        'usuario[genero]':{
          required:true
        },
        'usuario[movil]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:true
        },
        'usuario[idAgendaUsuario]':{
          required:true
        },
        'usuario[colorAgenda]':{
          required:true
        },
        'usuario[direccion]':{
          minlength: 6,
          maxlength:150,
          required:true
        },
        'usuario[numeroContacto]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:false
        },
        'usuario[nombreContacto]':{
          minlength: 3,
          maxlength:150,
          required:false
        },
        'usuario[idSucursal]':{
          required:true
        },
        'usuario[idAcuerdoValor]':{
          required:false
        },
        'usuario[idAcuerdoPorc]':{
          required:false
        },
        'usuario[idEspecialidad]':{
          required:false
        },
        'usuario[tipoUsuario]':{
          required:true
        },
        'usuario[firmaProfesional]':{
          required:false
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');

        //$(form).attr('action','/usuario/'+$('#idUsuario').val()+'/edit');
        //alert($(form).serilize());
        $(form)[0].submit();
        
      }
  });


  var validatorFirmaUsuario= $('#formularioFirma').validate({
    rules: {
      'idUsuario':{
        required:true
      },
      'firmaProfesional':{
        required:true
      }
    },
    submitHandler: function(form) {
      if ($('#firmaProfesional').val() != '') {        
        //alert($(form).serialize());
        $('body').addClass('loading');
        $(form)[0].submit();
      }else{
        $('.errorFirmaUsuario').html('Este campo es obligatorio.');
      }
      //$('body').addClass('loading');
      //$(form)[0].submit();
    }
  });

  var validatorCambioPass= $('#formCambiarCredencial').validate({
    rules: {
      'password':{
        minlength:8,
        maxlength:8,
        required:true,
        caracter:true
      },
      'passwordConfir':{
        minlength:8,
        maxlength:8,
        required:true,
        caracter:true,
        igualdad:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      //alert($(form).serialize());
      $(form)[0].submit();
    }
  });
  var validatorCambioImgPerfil= $('#formCambiarPerfil').validate({
    rules: {
      'form[imagenUsuario]':{
        required:true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/usuario/'+$('#idUser').val()+'/cambiarPerfil');
      //alert($(form).attr('action'));
      $(form)[0].submit();
    }
  });
});


$(document).ready(function(){
  var validatorPaciente = $('#formPaciente').validate({
      rules: {
        'paciente[fechaNacimiento]':{
          required:true
        },
        'paciente[lugarNacimiento]':{
          required:true,
          minlength: 3,
          maxlength:150
        },
        'paciente[tipoIdentificacion]':{
          required:true
        },
        'paciente[numeroIdentificacion]':{
          required:true,
          minlength: 8,
          maxlength:20,
          letraNum:true
        },
        'paciente[nombres]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[primerApellidos]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[segundoApellidos]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[expedicionDocumento]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[dirrecionDomicilio]':{
          minlength: 6,
          maxlength:150,
          required:true
        },
        'paciente[idMunicipio]':{
          required:true
        },
        'indicativo':{
          minlength: 3,
          maxlength:3,
          digits:true,
          required:false
        },
        'telefono':{
          minlength: 7,
          maxlength:7,
          digits:true,
          required:false
        },
        'paciente[sexo]':{
          required:true
        },
        'paciente[email]':{
          minlength: 10,
          maxlength:75,
          email:true,
          required: false
        },
        'paciente[celular]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:true
        },
        'paciente[ocupacion]':{
          required:true,
          minlength: 3,
          maxlength:150,
        },
        'paciente[idEps]':{
          required:false
        },
        'paciente[regimen]':{
          required:false
        },
        'paciente[estadoCivil]':{
          required:true
        },
        'paciente[nombrePResponsable]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[telefonoPersonaResponsable]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:true
        },
        'paciente[parentesco]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[nombreAcompanante]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[movilAcompanante]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:true
        }
      },
      submitHandler: function(form) {

        var indicativo= $(form).children().children().children('#indicativo').val();
        var telefono=   $(form).children().children().children('#telefono').val();
        if (telefono != '' && indicativo != '') {
          var valorTelefono=indicativo+'-'+telefono;        
          $(form).children().children('input#paciente_telefonoDomicilio').val(valorTelefono);  

          $('body').addClass('loading');
          //var idT =$('#idEpsPrincipal').val();
          //$(form).attr('action', "/eps/"+idT+"/edit");
          $(form)[0].submit();        
        }else if(telefono != '' || indicativo != ''){
          $('.telPaciente').html('Registre el indicativo y el número telefónico.');
        }else{
          //var idT =$('#idTipoR').val();
          $('body').addClass('loading');
          //$(form).attr('action', "/eps/"+idT+"/edit");
          $(form)[0].submit(); 
        }
      }
  });
  var validatorUsuarioModificar = $('#formUsuarioEdit').validate({
      rules: {
        'usuario[nombres]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'usuario[apellidos]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'usuario[fechaNacimiento]':{
          required:true
        },
        'usuario[tipoentificacion]':{
          required:true
        },
        'usuario[numeroIdentificacion]':{
          required:true,
          minlength: 9,
          maxlength:15,
          digits:true
        },
        'usuario[email]':{
          minlength: 10,
          maxlength:75,
          email:true,
          required: true
        },
        'usuario[username]':{
          minlength: 1,
          maxlength:150,
          required:true
        },
        'usuario[idPerfil]':{
          required:true
        },
        'usuario[genero]':{
          required:true
        },
        'usuario[movil]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:true
        },
        'usuario[idAgendaUsuario]':{
          required:true
        },
        'usuario[colorAgenda]':{
          required:true
        },
        'usuario[direccion]':{
          minlength: 6,
          maxlength:150,
          required:true
        },
        'usuario[numeroContacto]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:false
        },
        'usuario[nombreContacto]':{
          minlength: 3,
          maxlength:150,
          required:false
        },
        'usuario[idSucursal]':{
          required:true
        },
        'usuario[idAcuerdoValor]':{
          required:false
        },
        'usuario[idAcuerdoPorc]':{
          required:false
        },
        'usuario[idEspecialidad]':{
          required:false
        },
        'usuario[tipoUsuario]':{
          required:true
        },
        'usuario[firmaProfesional]':{
          required:false
        }
      },
      submitHandler: function(form) {
        $('body').addClass('loading');

        //$(form).attr('action','/usuario/'+$('#idUsuario').val()+'/edit');
        //alert($(form).serilize());
        $(form)[0].submit();
        
      }
  });

  var validatorPacienteModificar = $('#formPacienteEdit').validate({
      rules: {
        'paciente[fechaNacimiento]':{
          required:true
        },
        'paciente[lugarNacimiento]':{
          required:true,
          minlength: 3,
          maxlength:150
        },
        'paciente[tipoIdentificacion]':{
          required:true
        },
        'paciente[numeroIdentificacion]':{
          required:true,
          minlength: 8,
          maxlength:20,
          letraNum:true
        },
        'paciente[nombres]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[primerApellidos]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[segundoApellidos]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[expedicionDocumento]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[dirrecionDomicilio]':{
          minlength: 6,
          maxlength:150,
          required:true
        },
        'paciente[idMunicipio]':{
          required:true
        },
        'indicativo':{
          minlength: 3,
          maxlength:3,
          digits:true,
          required:false
        },
        'telefono':{
          minlength: 7,
          maxlength:7,
          digits:true,
          required:false
        },
        'paciente[sexo]':{
          required:true
        },
        'paciente[email]':{
          minlength: 10,
          maxlength:75,
          email:true,
          required: false
        },
        'paciente[celular]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:true
        },
        'paciente[ocupacion]':{
          required:true,
          minlength: 3,
          maxlength:150,
        },
        'paciente[idEps]':{
          required:false
        },
        'paciente[regimen]':{
          required:false
        },
        'paciente[estadoCivil]':{
          required:true
        },
        'paciente[nombrePResponsable]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[telefonoPersonaResponsable]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:true
        },
        'paciente[parentesco]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[nombreAcompanante]':{
          minlength: 3,
          maxlength:150,
          required:true
        },
        'paciente[movilAcompanante]':{
          minlength: 10,
          maxlength:15,
          digits:true,
          required:true
        }
      },
      submitHandler: function(form) {

        var indicativo= $(form).children().children().children('#indicativo').val();
        var telefono=   $(form).children().children().children('#telefono').val();
        if (telefono != '' && indicativo != '') {
          var valorTelefono=indicativo+'-'+telefono;        
          $(form).children().children('input#paciente_telefonoDomicilio').val(valorTelefono);  

          $('body').addClass('loading');
          //var idT =$('#idEpsPrincipal').val();
          //$(form).attr('action', "/eps/"+idT+"/edit");
          $(form)[0].submit();        
        }else if(telefono != '' || indicativo != ''){
          $('.telPaciente').html('Registre el indicativo y el número telefónico.');
        }else{
          //var idT =$('#idTipoR').val();
          $('body').addClass('loading');
          //$(form).attr('action', "/eps/"+idT+"/edit");
          $(form)[0].submit(); 
        }
      }
  });
  var validatorExamenGeneral= $('#formExamenGeneral').validate({
    rules: {
      'examen_general[motivoConsulta]':{
        required:true,
        minlength: 10,
        maxlength:2000
      },
      'examen_general[antecedenteMedico]':{          
        required:false,
        minlength: 1,
        maxlength:2000
      },
      'examen_general[antecedenteFamiliar]':{
        required:false,
        minlength: 1,
        maxlength:2000
      },
      'examen_general[medicamento]':{
        required:false,
        minlength: 1,
        maxlength:2000
      },
      'examen_general[alergia]':{
        required:false,
        minlength:1,
        maxlength:2000,
        letraNum:true
      },
      'examen_general[factorRiesgo]':{
        minlength: 1,
        maxlength:2000,
        required:false
      },
      'examen_general[determinanteSocial]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[poblacionVulnerable]':{
        minlength: 1,
        maxlength:2000,
        required:false
      },
      'examen_general[antecedenteSistemico]':{
        minlength: 1,
        maxlength:2000,
        required:false
      },
      'examen_general[habito]':{
        minlength: 1,
        maxlength:2000,
        required:false
      },
      'examen_general[otro]':{
        minlength: 4,
        maxlength:2000,
        required:false
      },
      'examen_general[embarazo]':{
        required:false
      },
      'examen_general[cara]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[ganglios]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[maxilarMandibula]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[musculo]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[labio]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[carillo]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[encia]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[lengua]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[pisoBocal]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[paladar]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[orfaringe]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[dolorMuscular]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[dolorArtilar]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[ruidoArtilar]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[alteracionMovimiento]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[malOclusion]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[crecimientoDesarrollo]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[fasetaDesgaste]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[nDienteTP]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[estadoEstructural]':{
        required:false
      },
      'examen_general[alteracionFlurosis]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[rehabilitacion]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[saliva]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[funcionTejido]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },
      'examen_general[placa]':{
        minlength: 10,
        maxlength:2000,
        required:false
      },      
      'examen_general[remocionPlaca]':{
        required:false
      },
      'examen_general[fluor]':{
        required:false
      },
      'examen_general[detartraje]':{
        required:false
      },
      'examen_general[sellante]':{
        required:false
      },
      'examen_general[valoracionMedicina]':{
        required:false
      },
      'examen_general[educacionGrupal]':{
        required:false
      }
    },
    submitHandler: function(form) {
      $('#formExamenGeneral').children().children().children().children().children().children().children("textarea[name='examen_general[antecedenteMedico]'],textarea[name='examen_general[antecedenteFamiliar]'],textarea[name='examen_general[alergia]'],textarea[name='examen_general[factorRiesgo]'],textarea[name='examen_general[poblacionVulnerable]'],textarea[name='examen_general[antecedenteSistemico]'],textarea[name='examen_general[habito]'],textarea[name='examen_general[medicamento]']").removeAttr('disabled');
      //alert($(form).attr('action'));
      //alert($(form).serialize());
      var motivo=$(form).children().children().children().children().children().children().children('textarea#examen_general_motivoConsulta').val();
      var paciente=$(form).children().children('select#examen_general_idPaciente').val();
      if (motivo != '') { 
        //alert(paciente);
        if (motivo.length > 9 && motivo.length < 2001) { 
          $('.resp').attr('hidden', true);        
          $('body').addClass('loading');

          $(form).attr('action','/examenGeneral/new/'+paciente);
          $(form)[0].submit(); 
        } else{
          $('.resp').removeAttr('hidden').html('El limite mínimo de caracteres requerido es 10 y el máximo de caracteres es 2000. En campo motivo consulta');
        }
      } else{
        $('.resp').removeAttr('hidden').html('Es obligatorio el campo motivo consulta');
      }
      
    }
  });
  
  var validatorOdontogramaPaciente= $('#formOdontogramaPaciente').validate({
    rules: {            
      'odontograma_paciente[idDiagPrincipal]"':{
        required:true
      },
      'odontograma_paciente[idRipTipoOdon]':{
        required:true
      },
      'odontograma_paciente[idOdontograma]':{
        required:true
      },
      'odontograma_paciente[idPieza]':{
        required:true
      },
      'odontograma_paciente[idUsuarioProfesional]':{
        required:true
      },
      'odontograma_paciente[idSuperficie]':{
        required:true
      }
    },
    submitHandler: function(form) {
      
      superficie=$("select[name='odontograma_paciente[idSuperficie]'").val();
      if (superficie != '') {
        $('.idSuper').empty();
        url="/odontogramaPaciente/new/"+$("input[name='idPacienteOdontograma']").val();
        //alert('holap'+url);
        $.ajax({
          // especifica si será una petición POST o GET
          type: 'POST',
           // la información a enviar
          data: $(form).serialize(),
          //Url de petición envio
          url: url,          
          // el tipo de información que se espera de respuesta
          dataType : 'json',
          beforeSend: function () {
            $('body').addClass('loading'); //Agregamos la clase loading al body
          },
          // código a ejecutar si la petición es satisfactoria;
          success : function(data) {
            $('body').removeClass('loading'); 
            $(".gCV").removeClass().addClass('trapecio-top-smallGeneral gCV').css('border-bottom', '12px solid #ECECEC');
            $(".gV").removeClass().addClass('trapecio-topGeneral gV').css('border-bottom', '25px solid #ECECEC');
            $(".gM").removeClass().addClass('trapecio-leftGeneral gM').css('border-right', '25px solid #ECECEC');
            $(".gO").removeClass().addClass('circuloGeneral gO flex-child text-center').css('background','#ECECEC');;
            $(".gD").removeClass().addClass('trapecio-rightGeneral gD rightVertical').css('border-left', '25px solid #ECECEC');
            $(".gL").removeClass().addClass('trapecio-bottomGeneral gL super-child').css('border-top', '25px solid #ECECEC');
            $(".gCL").removeClass().addClass('trapecio-bottom-smallGeneral gCL super-child').css('border-top', '12px solid #ECECEC');
            $(".piezaCompleta").removeClass().addClass('text-center piezaCompleta');
            $(".piezaCompleta").empty();
            $(".piezaCompleta").append("<small class='text-secondary'>&nbsp;&nbsp;Pieza Completa</small>");
            //alert(data.error);
            if (data.error) {
              //alert(data.error);
              $(".resp").empty();
              $('.resp').removeClass('alert-success alert-dismissable').addClass('alert-danger alert-dismissable').removeAttr('hidden').html(data.error).append('<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
              $("#datosPiezaDental").empty();
              $("#datosPiezaDental").html(data.piezaDentalOdontograma);
            }else{
              //alert('hol');
              $(".resp").empty();
              $('.resp').removeClass('alert-danger alert-dismissable').addClass('alert-success alert-dismissable').removeAttr('hidden').html(data.mensaje).append('<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
              $("#datosPiezaDental").empty();
              $("#datosPiezaDental").html(data.piezaDentalOdontograma);
              $("#odontoPacienteGeneral").empty();
              $("#odontoPacienteGeneral").html(data.odontoPacientes);
              //console.log(data.odontoPacientes);
            }
          }
        });         
      }else{
        $('.idSuper').html("Este campo es obligatorio");
      }
    }
  });
});
$(document).ready(function(){
 var validatorImagen = $('#formImagen').validate({
    rules:{
      'imagen[nombre]':{
        required: true
      },
      'imagen[observacion]':{
        minlength: 10,
        maxlength:2000,
        required: true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();     
    }  
  }); 
  var validatorAnexo=$('#formAnexoPaciente').validate({
    rules:{
      'anexo_paciente[nombre]':{
        minlength: 3,
        maxlength:150,
        required: true
      },
      'anexo_paciente[idRemision]':{
        required: false
      },
      'anexo_paciente[descripcion]':{
        minlength: 10,
        maxlength:2000,
        required: false
      },
      'anexoPaciente[archivo]':{
        required: false
      }
    },
    submitHandler:function(form){
      $('body').addClass('loading');
      $(form)[0].submit(); 
    }
  });
  var validatorAnexoEdit=$('#formAnexoPacienteEdit').validate({
    rules:{
      'anexo_paciente[nombre]':{
        minlength: 3,
        maxlength:150,
        required: true
      },
      'anexo_paciente[idRemision]':{
        required: false
      },
      'anexo_paciente[descripcion]':{
        minlength: 10,
        maxlength:2000,
        required: false
      },
      'anexoPaciente[archivo]':{
        required: false
      }
    },
    submitHandler:function(form){
      $(form).attr('action','/anexoPaciente/'+$('#idAnexoPaciente').val()+'/edit');
      $('body').addClass('loading');
      //alert($(form).attr('action'))
      $(form)[0].submit(); 
    }
  });

  var validatorFormula=$('#formFormula').validate({
    rules:{
      'formula[nombre]':{
        minlength: 3,
        maxlength:150,
        required: true
      },
      'formula[observacion]':{
        minlength: 10,
        maxlength:2000,
        required: true
      },
      'formula[idCie]':{
        required: true
      },
      
      'formula[cantidad]':{
        required: true
      }
    },
    submitHandler:function(form){
      $('body').addClass('loading');
      $(form)[0].submit(); 
    }
  });
  var validatorFormulaModif=$('#formFormulaEdit').validate({
    rules:{
      'formula[nombre]':{
        minlength: 3,
        maxlength:150,
        required: true
      },
      'formula[observacion]':{
        minlength: 10,
        maxlength:2000,
        required: true
      },
      'formula[idCie]':{
        required: true
      },
      
      'formula[cantidad]':{
        required: true
      }
    },
    submitHandler:function(form){
      $('body').addClass('loading');
      $(form).attr('action','/formula/'+$('#idFormula').val()+'/edit');
      $(form)[0].submit(); 
    }
  });

  var validatorFirmaConsentimiento= $('#formularioFirmaPacienteConsentimiento').validate({
    rules: {
      'firmaPaciente':{
        required:true
      },
      'idConsentimiento':{
        required:true
      }
    },
    submitHandler: function(form) {
      if ($('#firmaPaciente').val() != '') {        
        //alert($(form).serialize());
        $('body').addClass('loading');
        $(form)[0].submit();
      }else{
        $('.errorFirmaPacienteCons').html('Este campo es obligatorio.');
      }
    }
  });

  var validatorHuellaCosentimiento=$('#formularioHuellaPacienteConsentimiento').validate({
    rules: {
      'huella':{
        required:true
      },
      'idConsentimiento':{
        required:true
      }
    },
    submitHandler: function(form) {
      if ($('#huella').val() != '') {        
        //console.log($(form).serialize());
        $('body').addClass('loading');
        $(form)[0].submit();
      }else{
        $('.errorHuellaPacienteCons').html('Este campo es obligatorio.');
      }
    }
  });
  var validatorConsentimientoModif=$('#formConsentimiento').validate({
    rules:{
      'consentimiento[idTipoConsentimiento]':{
        required: true
      },
      'consentimiento[nombreAcudiente]':{        
        minlength: 3,
        maxlength:150,
        required: true
      },
      'consentimiento[cedularAcudiente]':{
        minlength: 7,
        maxlength:15,
        digits:true,
        required: true
      },
      'consentimiento[parentesco]':{        
        minlength: 3,
        maxlength:150,
        required: true
      },
      'consentimiento[cedularResponsable]':{        
        minlength: 7,
        maxlength:15,
        digits: true,
        required: false
      },      
      'consentimiento[huellaPaciente]':{
        required: false
      },      
      'consentimiento[firmaPaciente]':{
        required: false
      }
    },
    submitHandler:function(form){
      var memo=document.getElementsByName('firmanteConsentimiento');
      var firmanteConsentimiento='';
      for(i=0; i<memo.length; i++){
        if(memo[i].checked){
          //var memory=memo[i].checked;
          firmanteConsentimiento=memo[i].value;
        
        }
      }

      var memoConfirma=document.getElementsByName('confirmacionConsentimiento');
      var confirmacionConsentimiento='';
        for(i=0; i<memoConfirma.length; i++){
          if(memoConfirma[i].checked){
            //var memory=memo[i].checked;
            confirmacionConsentimiento=memoConfirma[i].value;
          
          }
        }
      //alert(firmanteConsentimiento);
      
      if (firmanteConsentimiento == 'RESPONSABLE') {
        if ($("input[name='consentimiento[cedularResponsable]']").val() != '') {
          //if (confirmacionConsentimiento == 'FIRMA') {
          //  if ($("input[name='consentimiento[firmaPaciente]']").val() != '') {
              $('body').addClass('loading');
              $(form)[0].submit(); 
          //  }else{
          //    $('.mgsConfirmacion').html('Este campo es obligatorio.');
          //  }
          //}else if(confirmacionConsentimiento == 'HUELLA'){    
          //  if ($("input[name='consentimiento[huellaPaciente]']").val() != '') {
          //    $('body').addClass('loading');
          //    $(form)[0].submit(); 
          //  }else{
          //    $('.mgsConfirmacion').html('Este campo es obligatorio.');
          //  }
          //}
        }else{
          $('.mgsResponsable').html('Este campo es obligatorio.');
        }
      }else if(firmanteConsentimiento == 'PACIENTE'){    
        if ($("input[name='consentimiento[cedularResponsable]']").val() == '') {
          /*if (confirmacionConsentimiento == 'FIRMA') {
            if ($("input[name='consentimiento[firmaPaciente]']").val() != '') {*/
              $('body').addClass('loading');
              $(form)[0].submit(); 

          /*  }else{
              $('.mgsConfirmacion').html('Este campo es obligatorio.');
            }
          }else if(confirmacionConsentimiento == 'HUELLA'){    
            if ($("input[name='consentimiento[huellaPaciente]']").val() != '') {
              $('body').addClass('loading');
              $(form)[0].submit(); 
            }else{
              $('.mgsConfirmacion').html('Este campo es obligatorio.');
            }
          }*/
        }else{
          $('.mgsResponsable').html('Este no debe tener datos.');
        }
      }

    }
  });

  var validatorContratoRegistro =$('#formContrato').validate({
    rules:{
      'contrato[idTipoContrato]':{
        required: true
      },
      'contrato[idTipoArchivo]':{
        required: true
      },
      'contrato[nombreAcudiente]':{        
        minlength: 3,
        maxlength:150,
        required: true
      },
      'contrato[cedularAcudiente]':{
        minlength: 7,
        maxlength:15,
        digits:true,
        required: true
      },
      'contrato[parentesco]':{        
        minlength: 3,
        maxlength:150,
        required: true
      },
      'contrato[cedularResponsable]':{        
        minlength: 7,
        maxlength:15,
        digits: true,
        required: false
      }
    },
    submitHandler:function(form){     
      
      var tipoArchivo=$("select[name='contrato[idTipoArchivo]']").val();
      //alert(tipoArchivo);
      if (tipoArchivo == 1 || tipoArchivo == 2 ) {
        if ($("select[name='contrato[idPlanTratamiento]']").val() == '') {
       // alert(tipoArchivo);
          $('.msgPlan').html('Este campo es obligatorio para retiro o contrato.');
        }
      }

      var memo=document.getElementsByName('firmanteContrato');
      var firmanteContrato='';
      for(i=0; i<memo.length; i++){
        if(memo[i].checked){
          firmanteContrato=memo[i].value;
        }
      }

      var memoConfirma=document.getElementsByName('confirmacionContrato');
      var confirmacionContrato='';
        for(i=0; i<memoConfirma.length; i++){
          if(memoConfirma[i].checked){
            confirmacionContrato=memoConfirma[i].value;
          }
        }
      
      if (firmanteContrato == 'RESPONSABLE') {
          //alert('mundo'+firmanteContrato);
        if ($("input[name='contrato[cedularResponsable]']").val() != '') {
              $('body').addClass('loading');
              $(form)[0].submit(); 
        }else{
          $('.mgsResponsableContrato').html('Este campo es obligatorio.');
        }
      }else if(firmanteContrato == 'PACIENTE'){    
        if ($("input[name='contrato[cedularResponsable]']").val() == '') {
              $('body').addClass('loading');
              $(form)[0].submit();
        }else{
          $('.mgsResponsableContrato').html('Este no debe tener datos.');
        }
      }

    }
  });

  var validatorFirmaContrato= $('#formularioFirmaPacienteContrato').validate({
    rules: {
      'firmaPaciente':{
        required:true
      },
      'idContrato':{
        required:true
      }
    },
    submitHandler: function(form) {
      if ($('#firmaPaciente').val() != '') {   
        $(form).attr('action','firma');     
        $('body').addClass('loading');
        $(form)[0].submit();
        //alert($(form).serialize());
      }else{
        $('.errorFirmaPacienteContrato').html('Este campo es obligatorio.');
      }
    }
  });

  var validatorHuellaContrato=$('#formularioHuellaPacienteContrato').validate({
    rules: {
      'huella':{
        required:true
      },
      'idContrato':{
        required:true
      }
    },
    submitHandler: function(form) {
      if ($('#huella').val() != '') {        
        console.log($(form).attr('action'));
        $('body').addClass('loading');
        $(form)[0].submit();
      }else{
        $('.errorHuellaPacienteContrato').html('Este campo es obligatorio.');
      }
    }
  });

  var validatorRadiografia = $('#formRadiografia').validate({
    rules: {
      'radiografia[nombre]':{
        minlength: 3,
        maxlength:150,
        required:true
      },
      'radiografia[fechaApertura]':{
        required: true,
        maxApproval: true
      },
      'radiografia[idUsuario]':{
        required: false
      },
      'radiografia[idTipoRadiografia]':{
        required: true
      },
      'radiografia[imagen]':{
        required: true
      },
      'radiografia[hallazgo]':{
        required: true,
        minlength: 3,
        maxlength:2000
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form)[0].submit();
      
    }
  });

  var validatorPresupuesto = $('#formPresupuestos').validate({
    rules: {
      'presupuesto[idPlanTratamiento]':{
        required:false
      },
      'presupuesto[idTratamiento]':{
        required:true
      },
      'presupuesto[idConvenio]':{
        required: false
      },
      'presupuesto[idPieza]':{
        required: true
      },
      'presupuesto[idSuperficie]':{
        required: true
      },
      'presupuesto[idProcedimiento]':{
        required: true
      },
      'presupuesto[iva]':{
        minlength: 1,
        maxlength:2,
        digits:true,
        required: true
      },
      'presupuesto[valorAprobado]':{
        minlength: 4,
        maxlength:10,
        digits:true,
        required: true
      },
      'presupuesto[porcentajeDescuento]':{
        minlength: 1,
        maxlength:2,
        digits:true,
        required: true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      //alert($(form).serialize());
      $(form)[0].submit();
      
    }
  });

  var validatorSimulacion = $('#formSimulador').validate({
    rules: {
      'valorCuotaInicial':{
        required:true,
        minlength: 1,
        maxlength:10,
        digits:true,
      },
      'numeroCuotas':{
        required: true,
        minlength: 1,
        maxlength:3,
        digits:true,
      },
      'planTotal':{
        required: true,
        minlength: 4,
        maxlength:10,
        digits:true,
      }
    },
    submitHandler: function(form) {
      //$('body').addClass('loading');
      valorplan=$('input[name=planTotal]').val();
      valorCuotainicial=$('input[name=valorCuotaInicial]').val();
      numeroCuotas=$('input[name=numeroCuotas]').val();
      if (numeroCuotas > 0) {
        $('.errorNumeroCuotas').empty();
        valorMensual=(valorplan-valorCuotainicial)/numeroCuotas;
        //alert($(form).serialize()+' ---->>>>>'+valorMensual);
        $('.cuotaMensual').removeAttr('hidden');
        $('input[name=valorCuotaMensual]').val(valorMensual);
        $('.simul').attr('hidden',true);
        $('input[name=valorCuotaInicial]').attr('disabled',true);
        $('input[name=numeroCuotas]').attr('disabled',true);
      }else{
        $('.errorNumeroCuotas').html("El valor no puede ser menos que 1");
      }
     // $(form)[0].submit();
      
    }
  });
  validatorPresupuestoAprobado=$('#formPresupuestoAprobado').validate({
    rules: {
      'presupuesto[idPlanTratamiento]':{
        required:true
      },
      'presupuesto[idTratamiento]':{
        required:true
      },
      'presupuesto[idConvenio]':{
        required: false
      },
      'presupuesto[idPieza]':{
        required: true
      },
      'presupuesto[idSuperficie]':{
        required: true
      },
      'presupuesto[idProcedimiento]':{
        required: true
      },
      'presupuesto[iva]':{
        minlength: 1,
        maxlength:2,
        digits:true,
        required: true
      },
      'presupuesto[valorAprobado]':{
        minlength: 4,
        maxlength:10,
        digits:true,
        required: true
      },
      'presupuesto[porcentajeDescuento]':{
        minlength: 1,
        maxlength:2,
        digits:true,
        required: true
      }
    },
    submitHandler: function(form) {
      $('body').addClass('loading');
      $(form).attr('action','/plan/nuevo/presupuesto');
      //alert('hola____>____>'+$(form).serialize()+'__>>>'+$(form).attr('action'));
      $(form)[0].submit();
      
    }
  });
   
});

//Envio de informacion de formularios de huella y firma de un consentimiento
function envioFormFirmaConsentimiento(){
  $('body').addClass('loading');
  $('#formEnvioCons').submit();
  setTimeout(function(){location.reload(); }, 20000);
  //alert($('#formEnvioCons').serialize()+'miles');
  
} 

function envioFormHuellaConsentimiento(){
  $('#formEnvioHuellaCons').submit();
  //setTimeout(function(){location.reload(); }, 20000);
  //alert($('#formEnvioCons').serialize()+'miles');
  
} 
//Envio de informacion de formularios de huella y firma de un contrato
function envioFormFirmaContrato(){
  $('body').addClass('loading');
  $('#formEnvioFirmaContrato').submit();
  setTimeout(function(){location.reload(); }, 20000);
  //alert($('#formEnvioCons').serialize()+'miles');
  
} 

function envioFormHuellaContrato(){
  $('#formEnvioHuellaContrato').submit();
 // setTimeout(function(){location.reload(); }, 20000);
  //alert($('#formEnvioCons').serialize()+'miles');
  
} 
$( ".cambiarEstado").click( function(){
  var idUsuario= $(this).data('id');
   var tipo = 'usuario';
  $('.modal-body #id').val(idUsuario);
  $('.modal-body #tipo').val(tipo);

});
$(document).ready(function(){
  //Modulo Administracion /
  //Submodulo gestión configuración general 
  //-------------- Cargar modal de requerimiento inmediato si tiene errores---------//
  var validacionesTR= $('#crearRequerimiento').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearRequerimiento').modal('show');        
  }
  //-------------- Cargar modal de area medica inmediato si tiene errores---------//
  var validacionesTR= $('#crearAreaMedica').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearAreaMedica').modal('show');        
  }
  //-------------- Cargar modal de especialidad inmediato si tiene errores---------//
  var validacionesTR= $('#crearEspecialidad').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearEspecialidad').modal('show');        
  }
  //-------------- Cargar modal de departamento inmediato si tiene errores---------//
  var validacionesTR= $('#crearDepartamento').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearDepartamento').modal('show');        
  }
  //-------------- Cargar modal de eps inmediato si tiene errores---------//
  var validacionesTR= $('#crearEps').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearEps').modal('show');        
  }

  //Submodulo gestión consulta 

  //-------------- Cargar modal de finalidad consulta inmediato si tiene errores---------//
  var validacionesTR= $('#crearFinalidadConsulta').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearFinalidadConsulta').modal('show');        
  }

  //-------------- Cargar modal de motivo consulta inmediato si tiene errores---------//
  var validacionesTR= $('#crearMotivoConsulta').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearMotivoConsulta').modal('show');        
  }
  //Submodulo gestión convenio //

  //-------------- Cargar modal de convenio inmediato si tiene errores---------//
  var validacionesTR= $('#crearConvenio').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearConvenio').modal('show');        
  }

  //Submodulo gestión datos clinicos //

  var validacionesTR= $('#crearAlergia').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearAlergia').modal('show');        
  }

  //-------------- Cargar modal de antecedente inmediato si tiene errores---------//
  var validacionesTR= $('#crearAntecedente').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearAntecedente').modal('show');        
  }

  //-------------- Cargar modal de habito inmediato si tiene errores---------//
  var validacionesTR= $('#crearHabito').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearHabito').modal('show');        
  }

  //-------------- Cargar modal de medicamento inmediato si tiene errores---------//
  var validacionesTR= $('#crearMedicamento').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearMedicamento').modal('show');        
  }
  
  //-------------- Cargar modal de Riesgo inmediato si tiene errores---------//
  var validacionesTR= $('#crearRiesgo').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearRiesgo').modal('show');        
  }

  //-------------- Cargar modal de evolucion endodoncia inmediato si tiene errores---------//
  var validacionesTR= $('#crearEvolucionEndo').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearEvolucionEndo').modal('show');      
  }

  //-------------- Cargar modal de propiedad inmediato si tiene errores---------//
  var validacionesTR= $('#crearPropiedad').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearPropiedad').modal('show');        
  }

  //-------------- Cargar modal de provocado inmediato si tiene errores---------//
  var validacionesTR= $('#crearProvocado').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearProvocado').modal('show');        
  }

  //-------------- Cargar modal de Superficie inmediato si tiene errores---------//
  var validacionesTR= $('#crearSuperficie').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearSuperficie').modal('show');        
  }

  //Submodulo gestión diagnosticos //

  //-------------- Cargar modal de categoria diagnostico inmediato si tiene errores---------//
  var validacionesTR= $('#crearTipoDiagnostico').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearTipoDiagnostico').modal('show');        
  }

  //-------------- Cargar modal de diagnostico odontologico inmediato si tiene errores---------//
  var validacionesTR= $('#crearDiagnosticoOdontologico').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearDiagnosticoOdontologico').modal('show');        
  }

  //-------------- Cargar modal de diagnostico cie tipo inmediato si tiene errores---------//
  var validacionesTR= $('#crearDiagCieOdontologico').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearDiagCieOdontologico').modal('show');        
  }

  //-------------- Cargar modal de diagnostico principal  inmediato si tiene errores---------//
  var validacionesTR= $('#crearDiagnosticoPrincipal').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearDiagnosticoPrincipal').modal('show');        
  }
  
  //Submodulo de documentación//

  //-------------- Cargar modal de tipo archivo inmediato si tiene errores---------//
  var validacionesTR= $('#crearTipoArchivo').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearTipoArchivo').modal('show');        
  }

  //-------------- Cargar modal de tipo consentimiento inmediato si tiene errores---------//
  var validacionesTR= $('#crearTipoConsentimiento').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearTipoConsentimiento').modal('show');        
  }

  //-------------- Cargar modal de tipo contrato inmediato si tiene errores---------//
  var validacionesTR= $('#crearTipoContrato').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearTipoContrato').modal('show');        
  }
  
  //-------------- Cargar modal de tipo radiografia inmediato si tiene errores---------//
  var validacionesTR= $('#crearTipoRadigrafia').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearTipoRadigrafia').modal('show');        
  } 

  var validacionesTR= $('#crearSubtipo').children().children().children().children().children().children().children(".invalid-feedback").children().children().children().children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearSubtipo').modal('show');        
  } 

   var validacionesTR= $('#crearEstadoCita').children().children().children().children().children().children().children(".invalid-feedback").children().children().children().val(); 
   
  if (typeof(validacionesTR) != "undefined") {
      $('#crearEstadoCita').modal('show');
  } 

  //-------------- Cargar modal de estado general inmediato si tiene errores---------//
  var validacionesTR= $('#crearEstadoGeneral').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearEstadoGeneral').modal('show');        
  }
  //-------------- Cargar modal de estado mantenimiento inmediato si tiene errores---------//
  var validacionesTR= $('#crearEstadoMantenimiento').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearEstadoMantenimiento').modal('show');        
  }
  //-------------- Cargar modal de estado pago inmediato si tiene errores---------//
  var validacionesTR= $('#crearEstadoPago').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearEstadoPago').modal('show');        
  }

  //-------------- Cargar modal de tipo paquete inmediato si tiene errores---------//
  var validacionesTR= $('#crearTipoPaquete').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearTipoPaquete').modal('show');        
  }

   //-------------- Cargar modal de tipo paquete inmediato si tiene errores---------//
  var validacionesTR= $('#crearEsterilizacion').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearEsterilizacion').modal('show');        
  }
  //-------------- Cargar modal de concepto pago inmediato si tiene errores---------//
  var validacionesTR= $('#crearConceptoPago').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearConceptoPago').modal('show');        
  }

  //-------------- Cargar modal de forma pago inmediato si tiene errores---------//
  var validacionesTR= $('#crearFormaPago').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearFormaPago').modal('show');        
  }

  //-------------- Cargar modal de soporte pago inmediato si tiene errores---------//
  var validacionesTR= $('#crearSoportePago').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearSoportePago').modal('show');        
  }

  //-------------- Cargar modal de categoría inventario inmediato si tiene errores---------//
  var validacionesTR= $('#crearCategoriaInventario').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearCategoriaInventario').modal('show');        
  }

  //-------------- Cargar modal de marca inmediato si tiene errores---------//
  var validacionesTR= $('#crearMarca').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearMarca').modal('show');        
  }

  //-------------- Cargar modal de nombre insumo inmediato si tiene errores---------//
  var validacionesTR= $('#crearNombreInsumo').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearNombreInsumo').modal('show');        
  }

   //-------------- Cargar modal de stock inmediato si tiene errores---------//
  var validacionesTR= $('#crearStock').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearStock').modal('show');        
  }

   //-------------- Cargar modal de insumo inmediato si tiene errores---------//
  var validacionesTR= $('#crearInsumo').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearInsumo').modal('show');        
  }

  //-------------- Cargar modal de proveedor inmediato si tiene errores---------//
  var validacionesTR= $('#crearProveedor').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearProveedor').modal('show');        
  }

  //-------------- Cargar modal de pedido inmediato si tiene errores---------//
  var validacionesTR= $('#crearPedido').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearPedido').modal('show');        
  }

  //-------------- Cargar modal de salida clinica inmediato si tiene errores---------//
  var validacionesTR= $('#crearSalidaClinica').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearSalidaClinica').modal('show');        
  }

  //-------------- Cargar modal de cie rip inmediato si tiene errores---------//
  var validacionesTR= $('#crearCie').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearCie').modal('show');        
  }
   //-------------- Cargar modal de ambito realizacion inmediato si tiene errores---------//
  var validacionesTR= $('#crearAmbitoRealizacion').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearAmbitoRealizacion').modal('show');        
  }

  //-------------- Cargar modal de cups inmediato si tiene errores---------//
  var validacionesTR= $('#crearCups').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearCups').modal('show');        
  }

   //-------------- Cargar modal de finalidad procedimiento inmediato si tiene errores---------//
  var validacionesTR= $('#crearFinalidadProcedimiento').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearFinalidadProcedimiento').modal('show');        
  }


   //-------------- Cargar modal de forma realizacion inmediato si tiene errores---------//
  var validacionesTR= $('#crearFormaRealizacion').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearFormaRealizacion').modal('show');        
  }
  

   //-------------- Cargar modal de persona atiende inmediato si tiene errores---------//
  var validacionesTR= $('#crearPersonaAtiende').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearPersonaAtiende').modal('show');        
  }

  //-------------- Cargar modal de CATEGORÍA TRATAMIENTO inmediato si tiene errores---------//
  var validacionesTR= $('#crearCategoriaTratamiento').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearCategoriaTratamiento').modal('show');        
  }
  //-------------- Cargar modal de AGENDA USUARIO inmediato si tiene errores---------//
  var validacionesTR= $('#crearAgendaUsuario').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearAgendaUsuario').modal('show');        
  }

  //-------------- Cargar modal de TIPO USUARIO inmediato si tiene errores---------//
  var validacionesTR= $('#crearTipoUsuario').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearTipoUsuario').modal('show');        
  }

  //-------------- Cargar modal de TIPO DOCUMENTO inmediato si tiene errores---------//
  var validacionesTR= $('#crearTipoDocumento').children().children().children().children().children().children().children("div.invalid-feedback").children().children().val(); 
  if (typeof(validacionesTR) != "undefined") {
      $('#crearTipoDocumento').modal('show');        
  }
});


//Remisiones//
$(document).ready(function(){
    $('#crearRemision').click(function(){
       estado=$('#home').children('div').css('display');
      if (estado == "none") {
        visible=$('#home').children('div').css('display','block');       
       }else{
         invisible=$('#home').children('div').css('display','none');       
       }
     
    });
    contador=0;

    $("select[name=tipoRemision]").change(function(){
    if($('select[name=tipoRemision]').val() == 'INTERNA'){
      $('.idUserRemi').removeAttr('hidden');
      $('.idUserRemiLabel').removeAttr('hidden');
      $('.emailLabel').attr('hidden',true);
      $('.emailAttrib').attr('hidden',true);
      $('#remision_email').val('');
    }else if($('select[name=tipoRemision]').val() == 'EXTERNA'){
      $('.emailLabel').removeAttr('hidden');
      $('.emailAttrib').removeAttr('hidden');
      $('.idUserRemi').attr('hidden',true);
      $('.idUserRemiLabel').attr('hidden',true);
      $('#remision_idUsuarioRemitido').val('').change();
    }else{
      $('.idUserRemi').attr('hidden',true);
      $('.idUserRemiLabel').attr('hidden',true);
      $('.emailLabel').attr('hidden',true);
      $('.emailAttrib').attr('hidden',true);
    }
  });
    //Manejo de cie odontologico tipo//
    $(document).ready(function(){        
      arrayIconos=['fas fa-circle:f111','fas fa-circle-notch:f1ce','fas fa-hospital-alt:f47d','fas fa-square-full:f45c','fas fa-exclamation-triangle:f071','fas fa-ambulance:f0f9','fas fa-times:f00d','fas fa-ban:f05e','fas fa-band-aid:f462','fas fa-bars:f0c9','fas fa-blind:f29d','fas fa-bomb:f1e2','fas fa-briefcase-medical:f469','fas fa-capsules:f46b',      'fas fa-burn:f46a','fas fa-check:f00c','fas fa-check-circle:f058','fas fa-compact-disc:f51f','fas fa-crop:f125','fas fa-crosshairs:f05b','fas fa-diagnoses:f470','fas fa-dot-circle:f192','fas fa-exclamation-circle:f06a','fas fa-feather:f52d','fas fa-fire:f06d','fas fa-gem:f3a5','fas fa-genderless:f22d','fas fa-i-cursor:f246','fas fa-magic:f0d0','fas fa-long-arrow-alt-up:f30c','fas fa-not-equal:f53e','fas fa-notes-medical:f481','fas fa-paint-brush:f1fc','fas fa-people-carry:f4ce','fas fa-procedures:f487','fas fa-quidditch:f458','fas fa-screwdriver:f54a','fas fa-stethoscope:f0f1','fas fa-star:f005','fas fa-star-half:f089','fas fa-stop-circle:f28d','fas fa-syringe:f48e','fas fa-thermometer:f491','fas fa-thermometer-empty:f2cb','fas fa-times-circle:f057','fas fa-unlink:f127','fas fa-user-md:f0f0','fas fa-x-ray:f497','far fa-circle:f111','far fa-clipboard:f328','far fa-compass:f14e','far fa-dot-circle:f192','far fa-heart:f004','far fa-square:f0c8','far fa-star:f005','far fa-star-half:f089','far fa-stop-circle:f28d','far fa-times-circle:f057','fab fa-fulcrum:f50b','fab fa-asymmetrik:f372','fab fa-autoprefixer:f41c','fab fa-bity:f37a','fab fa-centercode:f380','fab fa-chrome:f268','fab fa-connectdevelop:f20e','fab fa-digital-ocean:f391','fab fa-discourse:f393','fab fa-empire:f1d1','fab fa-envira:f299','fab fa-ethereum:f42e','fab fa-gratipay:f184','fab fa-medrt:f3c8','fab fa-quinscape:f459','fab fa-ravelry:f2d9','fab fa-react:f41b','fab fa-schlix:f3ea','fab fa-scribd:f28a','fab fa-superpowers:f2dd','fab fa-themeisle:f2b2','fab fa-yelp:f1e9','fab fa-wpexplorer:f2de'];
      $("#cie_diagnostico_tipo_simbolo").empty();
      $('#cie_diagnostico_tipo_simbolo').append("<option value=''>Seleccione un icono.</option>");
      for (var i = 0; i < arrayIconos.length ; i++) {
        var opciones =arrayIconos[i];
        var datos=opciones.split(':');
        $('#cie_diagnostico_tipo_simbolo').append("<option value='"+datos[0]+"'>&#x"+datos[1]+";&nbsp;&nbsp;"+datos[0]+"</option>");
      }
      $('#cie_diagnostico_tipo_simbolo').trigger("chosen:updated");
    });

    $('#crearDiagCieOdontologico').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var idCieDiagnosticoTipo = button.data('idciediagnosticotipo');
      var idTipoDignostico = button.data('idtipodiagnostico');
      var idCie = button.data('idcie');
      var idDignosticoOdontologico = button.data('iddiagnosticoodontologico');  
      var color = button.data('color'); 
      var simbolo = button.data('simbolo');
      // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      if (typeof(idCieDiagnosticoTipo) != "undefined") {
        modal.find('.modal-title').text('ACTUALIZAR RIPS - ODONTOLÓGICO ');
        modal.find('.modal-body #idCieDiagnosticoTipo').val(idCieDiagnosticoTipo);
        modal.find('.modal-header .eliminarDato').removeAttr('hidden');
        modal.find('.modal-header .eliminarDato').attr('action','/cieDiagnosticoTipo/'+idCieDiagnosticoTipo+'/delete');
      }  
      modal.find('.modal-body #cie_diagnostico_tipo_idTipoDiagnostico').val(idTipoDignostico).trigger('change.select2');
      modal.find('.modal-body #cie_diagnostico_tipo_idTipoDiagnostico option[value]').attr("selected",false);
      modal.find('.modal-body #cie_diagnostico_tipo_idTipoDiagnostico option[value='+ idTipoDignostico +']').attr("selected",true);
      if (idTipoDignostico == 1) {
        modal.find('.modal-body #cie_diagnostico_tipo_idCieRip').val(idCie).trigger('change.select2');
        modal.find('.modal-body #cie_diagnostico_tipo_idCieRip option[value]').attr("selected",false);
        modal.find('.modal-body #cie_diagnostico_tipo_idCieRip option[value='+ idCie +']').attr("selected",true);
        $('.cieRip').removeAttr('hidden');
        $('.diagOdontologico').attr('hidden',true);
      }else if(idTipoDignostico == 2){
        modal.find('.modal-body #cie_diagnostico_tipo_idDiagnosticoOdontologico').val(idDignosticoOdontologico).trigger('change.select2');
        modal.find('.modal-body #cie_diagnostico_tipo_idDiagnosticoOdontologico option[value]').attr("selected",false);
        modal.find('.modal-body #cie_diagnostico_tipo_idDiagnosticoOdontologico option[value='+ idDignosticoOdontologico +']').attr("selected",true);
        $('.diagOdontologico').removeAttr('hidden');
        $('.cieRip').attr('hidden',true);
      }

      if (simbolo != '') {
        modal.find('.modal-body #tipoVisualizacion').val('ICONO').trigger('change.select2');
        modal.find('.modal-body #tipoVisualizacion option[value]').attr("selected",false);
        modal.find('.modal-body #tipoVisualizacion option[value=ICONO]').attr("selected",true);
        $('.iconoAttrib').removeAttr('hidden');
        $('.iconoLabel').removeAttr('hidden');
        $('.colorAttrib').attr('hidden',true);
        $('.colorLabel').attr('hidden',true);    
        modal.find('.modal-body #cie_diagnostico_tipo_simbolo').val(simbolo);
      }else if (color != '') {
        modal.find('.modal-body #tipoVisualizacion').val('COLOR').trigger('change.select2');
        modal.find('.modal-body #tipoVisualizacion option[value]').attr("selected",false);
        modal.find('.modal-body #tipoVisualizacion option[value=COLOR]').attr("selected",true);
        $('.colorAttrib').removeAttr('hidden');
        $('.colorLabel').removeAttr('hidden');
        $('.iconoAttrib').attr('hidden',true);
        $('.iconoLabel').attr('hidden',true);
        modal.find('.modal-body #cie_diagnostico_tipo_color').val(color);
      }
    });

    $("select[name='tipoVisualizacion']").change(function(){    
      valor2=$(this).val();
    //contadorDos=0;
    ////$('#select2-tipoVisualizacion-container').click(function(){
    //  contadorDos=1+contadorDos;
    //  if (contadorDos == 1) {
        //valor=$(this).attr('title');
        //valor2=$(this).val();
        if(valor2 == 'ICONO'){
          $('.iconoAttrib').removeAttr('hidden');
          $('.iconoLabel').removeAttr('hidden');

          $('.colorAttrib').attr('hidden',true);
          $('.colorLabel').attr('hidden',true);
          //contadorDos=0;        
        }else if(valor2 == 'COLOR'){                   
          
          $('.colorAttrib').removeAttr('hidden');
          $('.colorLabel').removeAttr('hidden');
          $('.iconoAttrib').attr('hidden',true);
          $('.iconoLabel').attr('hidden',true);
          //contadorDos=0;
        }else{
          $('.colorAttrib').attr('hidden',true);
          $('.colorLabel').attr('hidden',true);
          $('.iconoLabel').attr('hidden',true);
          $('.iconoAttrib').attr('hidden',true);
          //contadorDos=0;
        }
    //  }
    });

    $("select[name='cie_diagnostico_tipo[simbolo]']").change(function(){    
      valor2=$(this).val();
      if(valor2 != ''){
        $("select[name='cie_diagnostico_tipo[color]']").val('').change();
      }
    });

    $("select[name='cie_diagnostico_tipo[color]']").change(function(){    
      valor2=$(this).val();
      if(valor2 != ''){
        $("select[name='cie_diagnostico_tipo[simbolo]']").val('').change();
      }
    });

  $("select[name='cie_diagnostico_tipo[idTipoDiagnostico]']").change(function(){    
    valor=$(this).val();
  //contadorTres=0;
  //$('#select2-cie_diagnostico_tipo_idTipoDiagnostico-container').click(function(){
  //  contadorTres=1+contadorTres;
  //  if (contadorTres == 1) {
  //    valor=$('#cie_diagnostico_tipo_idTipoDiagnostico').val();
      if(valor == 1){
        $('.cieRip').removeAttr('hidden');
        $('.diagOdontologico').attr('hidden',true);
        //contadorTres=0;
      }else if(valor == 2){         
        
        $('.diagOdontologico').removeAttr('hidden');
        $('.cieRip').attr('hidden',true);
        //contadorTres=0;
      }else{
        $('.diagOdontologico').attr('hidden',true);
        $('.cieRip').attr('hidden',true);
        //contadorTres=0;
      }
  //  }
      
  });
  //contadorCuatro=0;
  $("select[name='acuerdo_pago[tipo]']").change(function(){    
    valor=$(this).val();
  //$('#select2-acuerdo_pago_tipo-container').click(function(){
    //contadorCuatro=1+contadorCuatro;
    //if (contadorCuatro == 1) {
      //valor=$('#acuerdo_pago_tipo').val();
      if(valor == 'PORCENTAJE'){
        $('.datosPorcentaje').removeAttr('hidden');
        $('.datosValor').attr('hidden',true);
        //contadorCuatro=0;
      }else if(valor == 'VALOR'){         
        
        $('.datosValor').removeAttr('hidden');
        $('.datosPorcentaje').attr('hidden',true);
        //contadorCuatro=0;
      }else{
        $('.datosValor').attr('hidden',true);
        $('.datosPorcentaje').attr('hidden',true);
        //contadorCuatro=0;
      }
    //}      
  });

  /**$('#formAcuerdoPagoEdit').children().children().children().children().children().children('#select2-acuerdo_pago_tipo-container').click(function(){
    contadorCuatro=1+contadorCuatro;
    if (contadorCuatro == 1) {
      valorEdit=$('#formAcuerdoPagoEdit').children().children().children('select#acuerdo_pago_tipo').val();
       if(valorEdit == 'PORCENTAJE'){
        $('#formAcuerdoPagoEdit').children('.datosPorcentaje').removeAttr('hidden');
        $('#formAcuerdoPagoEdit').children('.datosValor').attr('hidden',true);
        contadorCuatro=0;
      }else if(valorEdit == 'VALOR'){         
        
        $('#formAcuerdoPagoEdit').children('.datosValor').removeAttr('hidden');
        $('#formAcuerdoPagoEdit').children('.datosPorcentaje').attr('hidden',true);
        contadorCuatro=0;
      }else{
         $('#formAcuerdoPagoEdit').children('.datosValor').removeAttr('hidden',true);
        $('#formAcuerdoPagoEdit').children('.datosPorcentaje').attr('hidden',true);
        contadorCuatro=0;
      }
    }
      
  });**/

  //Manejo de tipo en acuerdo de pago
  valor=$('#formAcuerdoPago').children().children().children('select#acuerdo_pago_tipo').val();
  valorEdit=$('#formAcuerdoPagoEdit').children().children().children('select#acuerdo_pago_tipo').val();
  if(valorEdit == 'PORCENTAJE'){
    $('#formAcuerdoPagoEdit').children('.datosPorcentaje').removeAttr('hidden');
    $('#formAcuerdoPagoEdit').children('.datosValor').attr('hidden',true);
  }else if(valorEdit == 'VALOR'){     
    $('#formAcuerdoPagoEdit').children('.datosValor').removeAttr('hidden');
    $('#formAcuerdoPagoEdit').children('.datosPorcentaje').attr('hidden',true);
  }else if(valor == 'VALOR'){         
    $('#formAcuerdoPago').children('.datosValor').removeAttr('hidden');
    $('#formAcuerdoPago').children('.datosPorcentaje').attr('hidden',true);
    contadorCuatro=0;
  }else if(valor == 'PORCENTAJE'){
    $('#formAcuerdoPago').children('.datosPorcentaje').removeAttr('hidden');
    $('#formAcuerdoPago').children('.datosValor').attr('hidden',true);
  }
  
  //Manejo de unidad de pago
  valorPresentacion=$('#formPresentacionEdit').children().children('input#presentacion_unidad').val();
  if (valorPresentacion != "" && typeof(valorPresentacion) != "undefined") {
    var separacion= valorPresentacion.split(" / ");
    $('#formPresentacionEdit').children().children().children('input#cantidad').val(separacion[0]);
    $('#formPresentacionEdit').children().children().children('select#unidades').val(separacion[1]);
    $('#formPresentacionEdit').children().children().children('select#unidades').change();
  }      
});

//Manejo de estado cita icono//

    $(document).ready(function(){      
      arrayIconos=['fas fa-circle:f111','fas fa-circle-notch:f1ce','fas fa-hospital-alt:f47d','fas fa-square-full:f45c','fas fa-exclamation-triangle:f071','fas fa-ambulance:f0f9','fas fa-times:f00d','fas fa-ban:f05e','fas fa-band-aid:f462','fas fa-bars:f0c9','fas fa-blind:f29d','fas fa-bomb:f1e2','fas fa-briefcase-medical:f469','fas fa-capsules:f46b',      'fas fa-burn:f46a','fas fa-check:f00c','fas fa-check-circle:f058','fas fa-compact-disc:f51f','fas fa-crop:f125','fas fa-crosshairs:f05b','fas fa-diagnoses:f470','fas fa-dot-circle:f192','fas fa-exclamation-circle:f06a','fas fa-feather:f52d','fas fa-fire:f06d','fas fa-gem:f3a5','fas fa-genderless:f22d','fas fa-i-cursor:f246','fas fa-magic:f0d0','fas fa-long-arrow-alt-up:f30c','fas fa-not-equal:f53e','fas fa-notes-medical:f481','fas fa-paint-brush:f1fc','fas fa-people-carry:f4ce','fas fa-procedures:f487','fas fa-quidditch:f458','fas fa-screwdriver:f54a','fas fa-stethoscope:f0f1','fas fa-star:f005','fas fa-star-half:f089','fas fa-stop-circle:f28d','fas fa-syringe:f48e','fas fa-thermometer:f491','fas fa-thermometer-empty:f2cb','fas fa-times-circle:f057','fas fa-unlink:f127','fas fa-user-md:f0f0','fas fa-x-ray:f497','far fa-circle:f111','far fa-clipboard:f328','far fa-compass:f14e','far fa-dot-circle:f192','far fa-heart:f004','far fa-square:f0c8','far fa-star:f005','far fa-star-half:f089','far fa-stop-circle:f28d','far fa-times-circle:f057','fab fa-fulcrum:f50b','fab fa-asymmetrik:f372','fab fa-autoprefixer:f41c','fab fa-bity:f37a','fab fa-centercode:f380','fab fa-chrome:f268','fab fa-connectdevelop:f20e','fab fa-digital-ocean:f391','fab fa-discourse:f393','fab fa-empire:f1d1','fab fa-envira:f299','fab fa-ethereum:f42e','fab fa-gratipay:f184','fab fa-medrt:f3c8','fab fa-quinscape:f459','fab fa-ravelry:f2d9','fab fa-react:f41b','fab fa-schlix:f3ea','fab fa-scribd:f28a','fab fa-superpowers:f2dd','fab fa-themeisle:f2b2','fab fa-yelp:f1e9','fab fa-wpexplorer:f2de'];
      $("#estado_cita_icono").empty();
      $('#estado_cita_icono').append("<option value=''>Seleccione un icono.</option>");
      for (var i = 0; i < arrayIconos.length ; i++) {
        var opciones =arrayIconos[i];
        var datos=opciones.split(':');
        $('#estado_cita_icono').append("<option value='"+datos[0]+"'>&#x"+datos[1]+";&nbsp;&nbsp;"+datos[0]+"</option>");
      }
      $('#estado_cita_icono').trigger("chosen:updated");
      if (typeof($("#formEstadoCitaEdit").children().children().children('select#estado_cita_icono').val()) != "undefined") {
        confirmacionValorActual=$('#envioCorreo').val();
        if (confirmacionValorActual ) {
          confirmacionValorActual='TRUE';
        }else{
          confirmacionValorActual='FALSE';
        }
        $("#formEstadoCitaEdit").children().children().children('select#estado_cita_enviarCorreo').val(confirmacionValorActual).trigger('change.select2');      
        $("#formEstadoCitaEdit").children().children().children('select#estado_cita_enviarCorreo option[value]').attr("selected",false); 
        $("#formEstadoCitaEdit").children().children().children('select#estado_cita_enviarCorreo option[value="'+confirmacionValorActual+'"]').attr("selected",true); 

        iconoValorActual=$("#formEstadoCitaEdit").children().children().children('select#estado_cita_icono').val();
        $("#formEstadoCitaEdit").children().children().children('select#estado_cita_icono').empty();
        $("#formEstadoCitaEdit").children().children().children('select#estado_cita_icono').append("<option value=''>Seleccione un icono.</option>");
         for (var i = 0; i < arrayIconos.length ; i++) {
          var opciones =arrayIconos[i];
          var datos=opciones.split(':');
         $("#formEstadoCitaEdit").children().children().children('select#estado_cita_icono').append("<option value='"+datos[0]+"'>&#x"+datos[1]+";&nbsp;&nbsp;"+datos[0]+"</option>");
        }
        $("#formEstadoCitaEdit").children().children().children('select#estado_cita_icono').trigger("chosen:updated");
        $("#formEstadoCitaEdit").children().children().children('select#estado_cita_icono').val(iconoValorActual).trigger('change.select2');      
        $("#formEstadoCitaEdit").children().children().children('select#estado_cita_icono option[value]').attr("selected",false); 
        $("#formEstadoCitaEdit").children().children().children('select#estado_cita_icono option[value="'+iconoValorActual+'"]').attr("selected",true); 


      }
    });

    $('#datosEstadoCita').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget); // Button that triggered the modal
      var idEstadoCita = button.data('idestadocita');
      var nombre = button.data('nombre');
      var enviarCorreo = button.data('enviarcorreo');
      var icono = button.data('icono');      
      // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      modal.find('.modal-body #estado_cita_enviarCorreo').val(enviarCorreo).trigger('change.select2');
      modal.find('.modal-body #estado_cita_enviarCorreo option[value]').attr("selected",false);
      modal.find('.modal-body #estado_cita_enviarCorreo option[value='+ enviarCorreo +']').attr("selected",true);
      modal.find('.modal-body #estado_cita_icono').val(icono).trigger('change.select2');      
      modal.find('.modal-body #estado_cita_icono option[value]').attr("selected",false);      
      modal.find('.modal-body #estado_cita_icono option[value="'+icono+'"]').attr("selected",true);
      modal.find('.modal-body #estado_cita_nombre').val(nombre);
      
    }); 

  $('#formSalidaClinica,#formSalidaClinicaEdit').children().children().children().children('input#ckeckConsultorio').click(function(){
    $('#formSalidaClinica,#formSalidaClinicaEdit').children().children().children().children('input#ckeckConsultorio').attr('checked',true);
    $('#formSalidaClinica,#formSalidaClinicaEdit').children().children().children().children('input#ckeckPaciente').removeAttr('checked');
    $('#formSalidaClinica,#formSalidaClinicaEdit').children('div.idConsultorio').removeAttr('hidden');
    $('#formSalidaClinica,#formSalidaClinicaEdit').children('div.idPaciente').attr('hidden',true);
    $('#formSalidaClinica,#formSalidaClinicaEdit').children().children().children('select#salida_idPaciente').val('').change();

  });

  $('#formSalidaClinica,#formSalidaClinicaEdit').children().children().children().children('input#ckeckPaciente').click(function(){
    $('#formSalidaClinica,#formSalidaClinicaEdit').children().children().children().children('input#ckeckPaciente').attr('checked',true);
    $('#formSalidaClinica,#formSalidaClinicaEdit').children().children().children().children('input#ckeckConsultorio').removeAttr('checked');
    $('#formSalidaClinica,#formSalidaClinicaEdit').children('div.idPaciente').removeAttr('hidden');
    $('#formSalidaClinica,#formSalidaClinicaEdit').children('div.idConsultorio').attr('hidden',true);
    $('#formSalidaClinica,#formSalidaClinicaEdit').children().children().children('select#salida_idConsultorio').val('').change();
  });


  //Datos de estado confirmacion requerimientos//
  $("select[name=estadoValorConfirmacion]").change(function(){
    if($('select[name=estadoValorConfirmacion]').val() == 'CONFIRMADO'){
      $('.datoProveedor').removeAttr('hidden'); 
      }else{
      $('.datoProveedor').attr('hidden',true);
      $('#idProveedor').val('').change();
    }
  });

  //Configuración de diagnostico cups //
  $("select[name='cup_diagnostico[idDxr1]']").change(function(){
    if($("select[name='cup_diagnostico[idDxr1]']").val() != ''){
      $('.idDxr2Campo').removeAttr('hidden'); 
    }else{
      $('.idDxr2Campo').attr('hidden',true);
      $('.idDxr3Campo').attr('hidden',true);
      $("select[name='cup_diagnostico[idDxr2]']").val('').change();
      $("select[name='cup_diagnostico[idDxr3]']").val('').change();
    }
  });
  $("select[name='cup_diagnostico[idDxr2]']").change(function(){
    if($("select[name='cup_diagnostico[idDxr2]']").val() != ''){
      $('.idDxr3Campo').removeAttr('hidden'); 
    }else{
      $('.idDxr3Campo').attr('hidden',true);
      $("select[name='cup_diagnostico[idDxr2]']").val('').change();
      $("select[name='cup_diagnostico[idDxr3]']").val('').change();
    }
  });
  //Entrada inicial para edición //
  if($("select[name='cup_diagnostico[idDxr1]']").val() != ''){
    $('.idDxr2Campo').removeAttr('hidden');
  }
  if($("select[name='cup_diagnostico[idDxr2]']").val() != ''){
    $('.idDxr3Campo').removeAttr('hidden');
  }

  /**Usuario */
  //Agenda

  $('#modificarAgendaUsuario').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var idUsuario = button.data('id');
    var idAgenda = button.data('agenda');
    // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)  
    modal.find('.modal-body #idAgenda').val(idAgenda).change();
    modal.find('.modal-body #idUsuario').val(idUsuario);
  });
   
  //AcuerdoP
  $('#modificarAcuerdoPor').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var idUsuario = button.data('id');
    var idAcuerdoP = button.data('acuerdop');
    // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)  
    modal.find('.modal-body #idAcuerdoPorcentaje').val(idAcuerdoP).change();
    modal.find('.modal-body #idUsuario').val(idUsuario);
  });
  //AcuerdoV
  $('#modificarAcuerdoValor').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var idUsuario = button.data('id');
    var idAcuerdoV = button.data('acuerdov');
    // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)  
    modal.find('.modal-body #idAcuerdoValor').val(idAcuerdoV).change();
    modal.find('.modal-body #idUsuario').val(idUsuario);
  });

  /*División de username para usuario según correo electronico*/
  $("input[name='usuario[email]']").change(function(){

    if($(this).val() != ''){
      valor=$(this).val();
      separacion=valor.split('@');
      $('#resulUser').val(separacion[0]);
      $("input[name='usuario[username]']").val(separacion[0]).change(); 
    }
  });

  //Manipulación de fecha de nacimiento en pacientes para mostrar el tipo de documento con el que cuenta el individuo
  $("input[name='paciente[fechaNacimiento]']").change(function(){
    if($(this).val() != ''){
      valor=$(this).val();
      url='/paciente/tipoIdentificacion';
      $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',

      // la URL para la petición
      url:url,
      // la información a enviar
      data: {'fechaN':valor},
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading');
        if (data.length >0) {
          $("select[name='paciente[tipoIdentificacion]']").empty();
          $("select[name='paciente[tipoIdentificacion]']").append("<option value=''>Seleccione un tipo de documento</option>");
          for (var i=0; i<data.length; i++) { 
            //console.log(data[i]);
            datos=data[i];
            var separacion= datos.split(';');
            $("select[name='paciente[tipoIdentificacion]']").append("<option value='"+separacion[0]+"'>"+separacion[1]+"</option>");
            //msgForNormal = msgForNormal + dato[i] + ' - '; 
          }
          $("select[name='paciente[tipoIdentificacion]']").trigger("chosen:updated");        
        }      
      },

      // código a ejecutar si la petición falla;
      error : function(xhr, status) {
        $('body').removeClass('loading');
        $('#respu').html("Disculpe, existió un problema en la conexión.").attr( 'class', 'alert alert-danger alert-dismissable'); 
      },
    });
      //separacion=valor.split('@');
      //$('#resulUser').val(separacion[0]);
      //$("input[name='usuario[username]']").val(separacion[0]).change(); 
    }
  });

/**Panel superior de usuario*/
function subirFoto(id) {
  // alert(id); 
  var url = "/usuario/"+id+"/cambiarPerfil";
    $.ajax({
      type: 'POST',
      url:url,
      dataType: 'html',
      beforeSend: function () {
            $('body').addClass('loading'); //Agregamos la clase loading al body
          },
      success: function(respuesta){
        $('body').removeClass('loading');
            $('#modificarImgenUsuario').modal('show'); 
            $('#modificarImgenUsuario').html(respuesta);
      },
      // código a ejecutar si la petición falla;
            error : function(xhr, status) {
                $('body').removeClass('loading');
                $('.resp').removeAttr('hidden');
                $('.resp').html('Disculpe, existió un problema en la conexión.');
            },
    });
}

function detalleCuenta(id){
    var url = "/usuario/"+id;
    $.ajax({
      type: 'POST',
      url:url,
      dataType: 'html',
      beforeSend: function () {
            $('body').addClass('loading'); //Agregamos la clase loading al body
          },
      success: function(respuesta){
        $('body').removeClass('loading');
            $('#modificarImgenUsuario').modal('show'); 
            $('#modificarImgenUsuario').html(respuesta);
      },
      // código a ejecutar si la petición falla;
            error : function(xhr, status) {
                $('body').removeClass('loading');
                $('.resp').removeAttr('hidden');
                $('.resp').html('Disculpe, existió un problema en la conexión.');
            },
    });
}
//Mamejo examen general con checkValidate y con linea de proceso y habiliatcion de campos a utilizar
$("textarea[name='examen_general[antecedenteMedico]'],textarea[name='examen_general[antecedenteFamiliar]'],textarea[name='examen_general[alergia]'],textarea[name='examen_general[factorRiesgo]'],textarea[name='examen_general[determinanteSocial]'],textarea[name='examen_general[poblacionVulnerable]'],textarea[name='examen_general[antecedenteSistemico]'],textarea[name='examen_general[habito]'],textarea[name='examen_general[otro]'],input[name='examen_general[embarazo]'],textarea[name='examen_general[medicamento]']").removeAttr('required');
$("textarea[name='examen_general[cara]'],textarea[name='examen_general[ganglios]'],textarea[name='examen_general[maxilarMandibula]'],textarea[name='examen_general[musculo]'],textarea[name='examen_general[labio]'],textarea[name='examen_general[carillo]'],textarea[name='examen_general[encia]'],textarea[name='examen_general[lengua]'],textarea[name='examen_general[pisoBocal]'],textarea[name='examen_general[paladar]'],textarea[name='examen_general[orfaringe]'],textarea[name='examen_general[dolorMuscular]'],textarea[name='examen_general[dolorArtilar]'],textarea[name='examen_general[ruidoArtilar]'],textarea[name='examen_general[alteracionMovimiento]'],textarea[name='examen_general[malOclusion]'],textarea[name='examen_general[crecimientoDesarrollo]'],textarea[name='examen_general[fasetaDesgaste]'],textarea[name='examen_general[nDienteTP]'],select[name='examen_general[estadoEstructural]'],textarea[name='examen_general[alteracionFlurosis]'],textarea[name='examen_general[rehabilitacion]'],textarea[name='examen_general[saliva]'],textarea[name='examen_general[funcionTejido]'],textarea[name='examen_general[placa]']").removeAttr('required');
$("input[name='examen_general[remocionPlaca]'],input[name='examen_general[fluor]'],input[name='examen_general[detartraje]'],input[name='examen_general[sellante]'],input[name='examen_general[valoracionMedicina]'],input[name='examen_general[educacionGrupal]']").removeAttr('required');
function habilitar(){
   $('#formExamenGeneral').children().children().children().children().children().children().children().removeAttr('disabled');
   $('#formExamenGeneral').children().children().children().children().children().children().children().children().removeAttr('disabled');
   $('#formExamenGeneral').children().children().children().children().children().children().children("textarea[name='examen_general[antecedenteMedico]'],textarea[name='examen_general[antecedenteFamiliar]'],textarea[name='examen_general[alergia]'],textarea[name='examen_general[factorRiesgo]'],textarea[name='examen_general[poblacionVulnerable]'],textarea[name='examen_general[antecedenteSistemico]'],textarea[name='examen_general[habito]'],textarea[name='examen_general[medicamento]']").attr('disabled',true);
   $('.guardarExamanG').removeAttr('disabled');
   $('.botonMas').removeAttr('disabled');
}
//pasar tipo a modal de antecedentes
$('#antecedenteActuales').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var tipo = button.data('tipo');
    var datos= button.data('datos');
    var separacion=datos.split(',');
    // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)  
    modal.find('.modal-body #tipo').val(tipo).change();
    //alert(tipo);
    if (tipo == 'antMedico') {
      modal.find('.modal-title').text('LISTADO ANTECEDENTES MÉDICO');
      modal.find('.modal-body .aM').removeAttr('hidden');
      modal.find('.modal-body .aF').attr('hidden',true);
      modal.find('.modal-body .aS').attr('hidden',true);
      modal.find('.modal-body .poVul').attr('hidden',true);
      modal.find('.modal-body .facRies').attr('hidden',true);
      modal.find('.modal-body .alerg').attr('hidden',true);
      modal.find('.modal-body .habit').attr('hidden',true);
      modal.find('.modal-body .medi').attr('hidden',true);
      //alert(modal.find('.modal-body #antecedenteMedicoSelect').val());
      /*for (var i =  - 1; i >= 0; i--) {
        
      }*/
      
      //for ( var i=0; i <= separacion.length + 1; i++) { 
       // console.log(separacion);
        modal.find('.modal-body #antecedenteMedicoSelect').val(separacion).change();
      //}

    }else if (tipo == 'antFam') {
      modal.find('.modal-title').text('LISTADO ANTECEDENTES FAMILIARES');
      modal.find('.modal-body .aF').removeAttr('hidden');
      modal.find('.modal-body .aM').attr('hidden',true);
      modal.find('.modal-body .aS').attr('hidden',true);
      modal.find('.modal-body .poVul').attr('hidden',true);
      modal.find('.modal-body .facRies').attr('hidden',true);
      modal.find('.modal-body .alerg').attr('hidden',true);
      modal.find('.modal-body .habit').attr('hidden',true);
      modal.find('.modal-body .medi').attr('hidden',true);      
      modal.find('.modal-body #antecedenteFamiliarSelect').val(separacion).change();
    }else if (tipo == 'medimt') {
      modal.find('.modal-title').text('LISTADO MEDICAMENTOS');
      modal.find('.modal-body .medi').removeAttr('hidden');
      modal.find('.modal-body .aS').attr('hidden',true);
      modal.find('.modal-body .aM').attr('hidden',true);
      modal.find('.modal-body .aF').attr('hidden',true);
      modal.find('.modal-body .poVul').attr('hidden',true);
      modal.find('.modal-body .facRies').attr('hidden',true);
      modal.find('.modal-body .alerg').attr('hidden',true);
      modal.find('.modal-body .habit').attr('hidden',true);
      modal.find('.modal-body #medicamentoSelect').val(separacion).change();
    }else if (tipo == 'alergs') {
      modal.find('.modal-title').text('LISTADO ALERGIAS');
      modal.find('.modal-body .alerg').removeAttr('hidden');
      modal.find('.modal-body .aM').attr('hidden',true);
      modal.find('.modal-body .aF').attr('hidden',true);
      modal.find('.modal-body .aS').attr('hidden',true);
      modal.find('.modal-body .poVul').attr('hidden',true);
      modal.find('.modal-body .facRies').attr('hidden',true);
      modal.find('.modal-body .habit').attr('hidden',true);
      modal.find('.modal-body .medi').attr('hidden',true);
      modal.find('.modal-body #alergiaSelect').val(separacion).change();
    }else if (tipo == 'factRg') {
      modal.find('.modal-title').text('LISTADO FACTOR RIESGO');
      modal.find('.modal-body .facRies').removeAttr('hidden');
      modal.find('.modal-body .aM').attr('hidden',true);
      modal.find('.modal-body .aF').attr('hidden',true);
      modal.find('.modal-body .aS').attr('hidden',true);
      modal.find('.modal-body .poVul').attr('hidden',true);
      modal.find('.modal-body .alerg').attr('hidden',true);
      modal.find('.modal-body .habit').attr('hidden',true);
      modal.find('.modal-body .medi').attr('hidden',true);
      modal.find('.modal-body #factorRiesgoSelect').val(separacion).change();
    }else if (tipo == 'pobVul') {
      modal.find('.modal-title').text('LISTADO POBLACIÓN VULNERABLE');
      modal.find('.modal-body .poVul').removeAttr('hidden');
      modal.find('.modal-body .aM').attr('hidden',true);
      modal.find('.modal-body .aF').attr('hidden',true);
      modal.find('.modal-body .aS').attr('hidden',true);
      modal.find('.modal-body .facRies').attr('hidden',true);
      modal.find('.modal-body .alerg').attr('hidden',true);
      modal.find('.modal-body .habit').attr('hidden',true);
      modal.find('.modal-body .medi').attr('hidden',true);
      modal.find('.modal-body #poblacionVulnerableSelect').val(separacion).change();
    }else if (tipo == 'antSis') {
      modal.find('.modal-title').text('LISTADO ANTECEDENTES SISTEMICOS');
      modal.find('.modal-body .aS').removeAttr('hidden');
      modal.find('.modal-body .aM').attr('hidden',true);
      modal.find('.modal-body .aF').attr('hidden',true);
      modal.find('.modal-body .poVul').attr('hidden',true);
      modal.find('.modal-body .facRies').attr('hidden',true);
      modal.find('.modal-body .alerg').attr('hidden',true);
      modal.find('.modal-body .habit').attr('hidden',true);
      modal.find('.modal-body .medi').attr('hidden',true);
      modal.find('.modal-body #antecedenteSistemicoSelect').val(separacion).change();
    }else if (tipo == 'habit') {
      modal.find('.modal-title').text('LISTADO HABITOS');
      modal.find('.modal-body .habit').removeAttr('hidden',true);
      modal.find('.modal-body .aM').attr('hidden',true);
      modal.find('.modal-body .aF').attr('hidden',true);
      modal.find('.modal-body .aS').attr('hidden',true);
      modal.find('.modal-body .poVul').attr('hidden',true);
      modal.find('.modal-body .facRies').attr('hidden',true);
      modal.find('.modal-body .alerg').attr('hidden',true);
      modal.find('.modal-body .medi').attr('hidden',true);
      modal.find('.modal-body #habitoSelect').val(separacion).change();
    }
  });
//Antecedentes
/*function antecedenteActual(id) {
  // alert(id); 
  var url = "/examenGeneral/antecedenteTotal/"+id;
  $.ajax({
    type: 'POST',
    url:url,
    dataType: 'html',
    beforeSend: function () {
          $('body').addClass('loading'); //Agregamos la clase loading al body
        },
    success: function(respuesta){
      $('body').removeClass('loading');
      $('#datosActualesMas').modal('show'); 
      $('#datosActualesMas').html(respuesta);
    },
    // código a ejecutar si la petición falla;
          error : function(xhr, status) {
              $('body').removeClass('loading');
              $('.resp').removeAttr('hidden');
              $('.resp').html('Disculpe, existió un problema en la conexión.');
          },
  });
}*/
//Guarda los datos segun la casilla que se seleccione en cada select de la emulacion de antecedentes como otros tipos de datos necesario 
//el la finalizacion del formulario
function guardarAnteceMedico(){
  $('.btnGuardarAnteMedico').attr('disabled',true);
  tipo = $('#tipo').val();
  if (tipo == 'antMedico') {
    $("textarea[name='examen_general[antecedenteMedico]']").val($("select[name='antecedenteMedicoSelect[]']").val())
  }else if (tipo == 'antFam') {
    $("textarea[name='examen_general[antecedenteFamiliar]']").val($("select[name='antecedenteFamiliarSelect[]']").val())
  }else if (tipo == 'medimt') {
    $("textarea[name='examen_general[medicamento]']").val($("select[name='medicamentoSelect[]']").val())
  }else if (tipo == 'alergs') {
    $("textarea[name='examen_general[alergia]']").val($("select[name='alergiaSelect[]']").val())
  }else if (tipo == 'factRg') {
    $("textarea[name='examen_general[factorRiesgo]']").val($("select[name='factorRiesgoSelect[]']").val())
  }else if (tipo == 'pobVul') {
    $("textarea[name='examen_general[poblacionVulnerable]']").val($("select[name='poblacionVulnerableSelect[]']").val())
  }else if (tipo == 'antSis') {
    $("textarea[name='examen_general[antecedenteSistemico]']").val($("select[name='antecedenteSistemicoSelect[]']").val())
  }else if (tipo == 'habit') {
    $("textarea[name='examen_general[habito]']").val($("select[name='habitoSelect[]']").val())
  }
  
  //alert('hola'+$("textarea[name='examen_general[antecedenteMedico]']").val() );
  $('#antecedenteActuales').modal('hide');
  $('.fade').removeClass('modal-backdrop show'); 
  $('.btnGuardarAnteMedico').removeAttr('disabled');
  //$('#antecedenteActuales .close').click();
  //$('.fade').remove();
  //$('body').removeClass('modal-open');

}


//Utilizado para evidenciar todos los examenes que posee el paciente que se esta observando 
function examenGeneralTotal(id){
  //alert(id); 
  var url = "/examenGeneral/total/"+id;
  $.ajax({
    type: 'POST',
    url:url,
    dataType: 'html',
    beforeSend: function () {
          $('body').addClass('loading'); //Agregamos la clase loading al body
        },
    success: function(respuesta){
      $('body').removeClass('loading');
      $('#datosActualesMas').modal('show'); 
      $('#datosActualesMas').html(respuesta);
    },
    // código a ejecutar si la petición falla;
          error : function(xhr, status) {
              $('body').removeClass('loading');
              $('.resp').removeAttr('hidden');
              $('.resp').html('Disculpe, existió un problema en la conexión.');
          },
  });
}

function envioExamenG(idExamen){
  $('body').addClass('loading');
  $('#formEnvioVista_'+idExamen).submit();
}

//eliminacion de examengeneral
function examenGConfirmDelete(id){
  swal({
    title: "¿Estás seguro?",
    text: "Estás por borrar un registro, este no se podrá recuperar más adelante.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Eliminarlo!',
    cancelButtonText: "Cancelar",
    allowEscapeKey: false,
    allowOutsideClick: false
  }).then(function (isConfirm) {
    if (isConfirm) {        
      swal({
        title: 'Eliminado!',
        text: 'Se ha eliminado correctamente el registro.',
        type: 'success',
        allowEscapeKey: false,
        allowOutsideClick: false
      }).then(function (isConfirm) {
        if (isConfirm) {
          //alert('sv'+$('.eliminarDato').attr('action'));
          //alert($('.eliminarDato').serialize());
          $('body').addClass('loading');
          $('.eliminarDato_'+id).submit();
        }
      });          

    }
  }
  , function (dismiss) {
    if (dismiss === 'cancel') {
      swal("Cancelado!", " Se ha cancelado exitosamente.", "error");
    };
  }
  );
  return false;
}
//Manejo de odontograma
function creacionOdontograma(idPieza,nombre,idOdontograma){
  //alert(idPieza);
  $(".formOdontograma").removeAttr('hidden');
  $('.idDiente').text(nombre);
  $("select[name='odontograma_paciente[idPieza]']").val(idPieza).change();
  $("select[name='odontograma_paciente[idSuperficie]']").val('').change();
  $("select[name='odontograma_paciente[idRipTipoOdon]']").val('').change();
  $("select[name='odontograma_paciente[idDiagPrincipal]']").val('').change();
  $('.idSuper').empty();
  $(".gCV").removeClass().addClass('trapecio-top-smallGeneral gCV').css('border-bottom', '12px solid #ECECEC');
  $(".gV").removeClass().addClass('trapecio-topGeneral gV').css('border-bottom', '25px solid #ECECEC');
  $(".gM").removeClass().addClass('trapecio-leftGeneral gM').css('border-right', '25px solid #ECECEC');
  $(".gO").removeClass().addClass('circuloGeneral gO flex-child text-center').css('background','#ECECEC');;
  $(".gD").removeClass().addClass('trapecio-rightGeneral gD rightVertical').css('border-left', '25px solid #ECECEC');
  $(".gL").removeClass().addClass('trapecio-bottomGeneral gL super-child').css('border-top', '25px solid #ECECEC');
  $(".gCL").removeClass().addClass('trapecio-bottom-smallGeneral gCL super-child').css('border-top', '12px solid #ECECEC');
  $(".piezaCompleta").removeClass().addClass('text-center piezaCompleta');
  $(".piezaCompleta").empty().css({'background':'white','border':'1px solid #D5CFCF'});
  $(".piezaCompleta").append("<small class='text-secondary'>&nbsp;&nbsp;Pieza Completa</small>");
  var url = '/odontogramaPaciente/datospiezaDental';  
  //alert(idOdontograma);
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
       // la información a enviar
      data: {'idOdontograma':idOdontograma,'idPieza':idPieza},
      //Url de petición envio
      url: url,          
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading'); 
        $("#datosPiezaDental").html(data.piezaDentalOdontograma);
      }
  }); 
}
$('.gO').click(function(){
  idOdontograma=$("select[name='odontograma_paciente[idOdontograma]']").val();
  idPieza=$("select[name='odontograma_paciente[idPieza]']").val();
  idSuperficie=1;
  $("select[name='odontograma_paciente[idSuperficie]']").val(1).change();
  $(this).css('background','yellow');
  url='/odontogramaPaciente/datoSuperficie';
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
       // la información a enviar
      data: {'idOdontograma':idOdontograma,'idSuperficie':idSuperficie,'idPieza':idPieza},
      //Url de petición envio
      url: url,          
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading');
        $("select[name='odontograma_paciente[idRipTipoOdon]']").val(data.diagnostico).change();
        $("select[name='odontograma_paciente[idDiagPrincipal]']").val(data.tipo).change();
      }
  }); 
});
$('.gV').click(function(){
  idOdontograma=$("select[name='odontograma_paciente[idOdontograma]']").val();
  idPieza=$("select[name='odontograma_paciente[idPieza]']").val();
  idSuperficie=2;
  $("select[name='odontograma_paciente[idSuperficie]']").val(2).change();
  $(this).css('border-bottom', '25px solid yellow');
  url='/odontogramaPaciente/datoSuperficie';
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
       // la información a enviar
      data: {'idOdontograma':idOdontograma,'idSuperficie':idSuperficie,'idPieza':idPieza},
      //Url de petición envio
      url: url,          
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading'); 
        $("select[name='odontograma_paciente[idRipTipoOdon]']").val(data.diagnostico).change();
        $("select[name='odontograma_paciente[idDiagPrincipal]']").val(data.tipo).change();
      }
  }); 
});
$('.gL').click(function(){
  idOdontograma=$("select[name='odontograma_paciente[idOdontograma]']").val();
  idPieza=$("select[name='odontograma_paciente[idPieza]']").val();
  idSuperficie=3;
  $("select[name='odontograma_paciente[idSuperficie]']").val(3).change();
  $(this).css('border-top', '25px solid yellow');
  url='/odontogramaPaciente/datoSuperficie';
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
       // la información a enviar
      data: {'idOdontograma':idOdontograma,'idSuperficie':idSuperficie,'idPieza':idPieza},
      //Url de petición envio
      url: url,          
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading');         
        $("select[name='odontograma_paciente[idRipTipoOdon]']").val(data.diagnostico).change();
        $("select[name='odontograma_paciente[idDiagPrincipal]']").val(data.tipo).change();
      }
  }); 
});
$('.gM').click(function(){
  idOdontograma=$("select[name='odontograma_paciente[idOdontograma]']").val();
  idPieza=$("select[name='odontograma_paciente[idPieza]']").val();
  idSuperficie=4;
  $("select[name='odontograma_paciente[idSuperficie]']").val(4).change();
  $(this).css('border-right', '25px solid yellow');
  url='/odontogramaPaciente/datoSuperficie';
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
       // la información a enviar
      data: {'idOdontograma':idOdontograma,'idSuperficie':idSuperficie,'idPieza':idPieza},
      //Url de petición envio
      url: url,          
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading');         
        $("select[name='odontograma_paciente[idRipTipoOdon]']").val(data.diagnostico).change();
        $("select[name='odontograma_paciente[idDiagPrincipal]']").val(data.tipo).change();
      }
  }); 
});
$('.gD').click(function(){
  idOdontograma=$("select[name='odontograma_paciente[idOdontograma]']").val();
  idPieza=$("select[name='odontograma_paciente[idPieza]']").val();
  idSuperficie=5;
  $("select[name='odontograma_paciente[idSuperficie]']").val(5).change();
  $(this).css('border-left', '25px solid yellow');
  url='/odontogramaPaciente/datoSuperficie';
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
       // la información a enviar
      data: {'idOdontograma':idOdontograma,'idSuperficie':idSuperficie,'idPieza':idPieza},
      //Url de petición envio
      url: url,          
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading'); 
        $("select[name='odontograma_paciente[idRipTipoOdon]']").val(data.diagnostico).change();
        $("select[name='odontograma_paciente[idDiagPrincipal]']").val(data.tipo).change();
      }
  }); 
});
$('.gCV').click(function(){
  idOdontograma=$("select[name='odontograma_paciente[idOdontograma]']").val();
  idPieza=$("select[name='odontograma_paciente[idPieza]']").val();
  idSuperficie=6;
  $("select[name='odontograma_paciente[idSuperficie]']").val(6).change();
  $(this).css('border-bottom', '12px solid yellow');
  url='/odontogramaPaciente/datoSuperficie';
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
       // la información a enviar
      data: {'idOdontograma':idOdontograma,'idSuperficie':idSuperficie,'idPieza':idPieza},
      //Url de petición envio
      url: url,          
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading'); 
        $("select[name='odontograma_paciente[idRipTipoOdon]']").val(data.diagnostico).change();
        $("select[name='odontograma_paciente[idDiagPrincipal]']").val(data.tipo).change();
      }
  }); 
});
$('.gCL').click(function(){
  idOdontograma=$("select[name='odontograma_paciente[idOdontograma]']").val();  
  idPieza=$("select[name='odontograma_paciente[idPieza]']").val();
  idSuperficie=7;
  $("select[name='odontograma_paciente[idSuperficie]']").val(7).change();
  $(this).css('border-top', '12px solid yellow');
  url='/odontogramaPaciente/datoSuperficie';
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
       // la información a enviar
      data: {'idOdontograma':idOdontograma,'idSuperficie':idSuperficie,'idPieza':idPieza},
      //Url de petición envio
      url: url,          
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading'); 
        $("select[name='odontograma_paciente[idRipTipoOdon]']").val(data.diagnostico).change();
        $("select[name='odontograma_paciente[idDiagPrincipal]']").val(data.tipo).change();
      }
  }); 
});
$('.piezaCompleta').click(function(){
  idOdontograma=$("select[name='odontograma_paciente[idOdontograma]']").val();
  idPieza=$("select[name='odontograma_paciente[idPieza]']").val();
  idSuperficie=8;
  $("select[name='odontograma_paciente[idSuperficie]']").val(8).change();
  $(this).css('background','yellow');
  url='/odontogramaPaciente/datoSuperficie';
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
       // la información a enviar
      data: {'idOdontograma':idOdontograma,'idSuperficie':idSuperficie,'idPieza':idPieza},
      //Url de petición envio
      url: url,          
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading'); 
        $("select[name='odontograma_paciente[idRipTipoOdon]']").val(data.diagnostico).change();
        $("select[name='odontograma_paciente[idDiagPrincipal]']").val(data.tipo).change();
      }
  }); 
});

function confirmDeleteOdontoPaciente(id){
  swal({
      title: "¿Estás seguro?",
      text: "Estás por borrar un registro, este no se podrá recuperar más adelante.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminarlo!',
      cancelButtonText: "Cancelar",
      allowEscapeKey: false,
      allowOutsideClick: false
       }).then(function (isConfirm) {
        if (isConfirm) {
            
           swal({
              title: 'Eliminado!',
              text: 'Se ha eliminado correctamente el registro.',
              type: 'success',
              allowEscapeKey: false,
              allowOutsideClick: false
           }).then(function (isConfirm) {
                if (isConfirm) {
                  //alert('sv'+$('.eliminarDatoPaciente_'+id).attr('action'));
                  //alert($('.eliminarDatoPaciente_'+id).serialize());
                  //$('body').addClass('loading');
                  //$('.eliminarDatoPaciente_'+id).submit();
                  $.ajax({
                    // especifica si será una petición POST o GET
                    type: 'DELETE',
                    // la información a enviar
                    data: $('.eliminarDatoPaciente_'+id).serialize(),
                    //Url de petición envio
                    url: $('.eliminarDatoPaciente_'+id).attr('action'),          
                    // el tipo de información que se espera de respuesta
                    dataType : 'json',
                    beforeSend: function () {
                      $('body').addClass('loading'); //Agregamos la clase loading al body
                    },
                    // código a ejecutar si la petición es satisfactoria;
                    success : function(data) {
                      $('body').removeClass('loading'); 
                      //alert('si sis is si sis ');
                      $('body').removeClass('loading'); 
                      $(".gCV").removeClass().addClass('trapecio-top-smallGeneral gCV').css('border-bottom', '12px solid #ECECEC');
                      $(".gV").removeClass().addClass('trapecio-topGeneral gV').css('border-bottom', '25px solid #ECECEC');
                      $(".gM").removeClass().addClass('trapecio-leftGeneral gM').css('border-right', '25px solid #ECECEC');
                      $(".gO").removeClass().addClass('circuloGeneral gO flex-child text-center').css('background','#ECECEC');;
                      $(".gD").removeClass().addClass('trapecio-rightGeneral gD rightVertical').css('border-left', '25px solid #ECECEC');
                      $(".gL").removeClass().addClass('trapecio-bottomGeneral gL super-child').css('border-top', '25px solid #ECECEC');
                      $(".gCL").removeClass().addClass('trapecio-bottom-smallGeneral gCL super-child').css('border-top', '12px solid #ECECEC');
                      $(".piezaCompleta").removeClass().addClass('text-center piezaCompleta');
                      $(".piezaCompleta").empty();
                      $(".piezaCompleta").append("<small class='text-secondary'>&nbsp;&nbsp;Pieza Completa</small>");
                      //alert(data.error);
                      if (data.error) {
                        //alert(data.error);
                        $(".resp").empty();
                        $('.resp').removeClass('alert-success alert-dismissable').addClass('alert-danger alert-dismissable').removeAttr('hidden').html(data.error).append('<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
                        $("#datosPiezaDental").empty();
                        $("#datosPiezaDental").html(data.piezaDentalOdontograma);
                      }else{
                        //alert('hol');
                        $(".resp").empty();
                        $('.resp').removeClass('alert-danger alert-dismissable').addClass('alert-success alert-dismissable').removeAttr('hidden').html(data.mensaje).append('<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
                        $("#datosPiezaDental").empty();
                        $("#datosPiezaDental").html(data.piezaDentalOdontograma);
                        $("#odontoPacienteGeneral").empty();
                        $("#odontoPacienteGeneral").html(data.odontoPacientes);
                        //console.log(data.odontoPacientes);
                      }
                    }
                  });
                }});          

          }}
        , function (dismiss) {
              if (dismiss === 'cancel') {
                swal("Cancelado!", " Se ha cancelado exitosamente.", "error");
              };
            }
        );
    return false;
}


$('.terminacionOdontoInicial').click(function(){
  $('body').addClass('loading');
  $(this).attr('disabled',true);
});


function detalleOdontograma(id){
  $.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',
      //Url de petición envio
      url: "/odontogramaPaciente/listadoOdontograma/"+id,          
      // el tipo de información que se espera de respuesta
      dataType : 'html',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(respuesta) {
        $('body').removeClass('loading'); 
        $('#datosOdontogramas').modal('show'); 
        $('#datosOdontogramas').html(respuesta);
      },
      // código a ejecutar si la petición falla;
      error : function(xhr, status) {
        $('body').removeClass('loading');
        $('.resp').removeAttr('hidden');
        $('.resp').html('Disculpe, existió un problema en la conexión.');
      },
  }); 
}

$(document).ready(function(){
  //añejo de datatable 
  newTable('#table_odontograma');   
  newTable('#table_anexosPacientes');
  newTable('#table_formulas');
  newTable('#table_consentimientos');
  newTable('#table_contratos');
  newTable('#table_radiografias');
  newTable('#table_presupuestos');
  newTable('#table_plan');
});

$('#custom-modal').on('show.bs.modal', function (event) {
  //alert('holap');
  var button = $(event.relatedTarget); // Button that triggered the modal
  var id = button.data('id');
  //alert('hopla'+id);
  var modal = $(this)
  if (typeof(id) != "undefined") {
    modal.find('.modal-body .imgGrande').attr('src','/light/dist/assets/uploads/imagen/'+id);
  }  
});

function confirmDeleteImagen(id){
  swal({
      title: "¿Estás seguro?",
      text: "Estás por borrar un registro, este no se podrá recuperar más adelante.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminarlo!',
      cancelButtonText: "Cancelar",
      allowEscapeKey: false,
      allowOutsideClick: false
       }).then(function (isConfirm) {
        if (isConfirm) {
            
           swal({
              title: 'Eliminado!',
              text: 'Se ha eliminado correctamente el registro.',
              type: 'success',
              allowEscapeKey: false,
              allowOutsideClick: false
           }).then(function (isConfirm) {
                if (isConfirm) {
                  //alert($('.eliminarDatoImagen_'+id).attr('action'));
                  //$('.eliminarDatoImagen_'+id).attr('action');
                  //alert('sv'+$('.eliminarDatoPaciente_'+id).attr('action'));
                  //alert($('.eliminarDatoPaciente_'+id).serialize());
                  $('body').addClass('loading');
                  $('.eliminarDatoImagen_'+id).submit();
                 
                }});          

          }}
        , function (dismiss) {
              if (dismiss === 'cancel') {
                swal("Cancelado!", " Se ha cancelado exitosamente.", "error");
              };
            }
        );
    return false;
}

/*cambio de responsable de firmar el consentimiento*/
$("input[name='firmanteConsentimiento']").change(function(){
  valor=$(this).val();
  if (valor == 'RESPONSABLE') {
    $('.responsableCC').removeAttr('hidden');
    $('.pacienteCC').attr('hidden',true);
  }else if(valor == 'PACIENTE'){    
    $('.pacienteCC').removeAttr('hidden');
    $('.responsableCC').attr('hidden',true);    
    $("input[name='consentimiento[cedularResponsable]']").val('');
  }
});
if ($("input[name='firmanteConsentimiento']").val() == 'RESPONSABLE') {
    $('.responsableCC').removeAttr('hidden');
    $('.pacienteCC').attr('hidden',true);
    $("input[name='consentimiento[huellaPaciente]']").val('');
  }else if($("input[name='firmanteConsentimiento']").val() == 'PACIENTE'){    
    $('.pacienteCC').removeAttr('hidden');
    $('.responsableCC').attr('hidden',true);
    $("input[name='consentimiento[cedularResponsable]']").val('');
  }

/*cambio de confirmacion del consentimiento por firma o huella*/
$("input[name='confirmacionConsentimiento']").change(function(){
  valor=$(this).val();
  if (valor == 'FIRMA') {
    $('.datoFirma').removeAttr('hidden');
    $('.datoHuella').attr('hidden',true);
    $("input[name='consentimiento[huellaPaciente]']").val('');
  }else if(valor == 'HUELLA'){    
    $('.datoHuella').removeAttr('hidden');
    $('.datoFirma').attr('hidden',true);
    $("input[name='consentimiento[firmaPaciente]']").val('');
  }
});
if ($("input[name='confirmacionConsentimiento']").val() == 'FIRMA') {
  $('.datoFirma').removeAttr('hidden');
  $('.datoHuella').attr('hidden',true);
  $("input[name='consentimiento[huellaPaciente]']").val('');
}else if($("input[name='confirmacionConsentimiento']").val() == 'HUELLA'){    
  $('.datoHuella').removeAttr('hidden');
  $('.datoFirma').attr('hidden',true);
  $("input[name='consentimiento[firmaPaciente]']").val('');
}


/*cambio de responsable de firmar el contrato*/
$("input[name='firmanteContrato']").change(function(){
  valor=$(this).val();
  if (valor == 'RESPONSABLE') {
    $('.responsableCContrato').removeAttr('hidden');
    $('.pacienteCContrato').attr('hidden',true);
  }else if(valor == 'PACIENTE'){    
    $('.pacienteCContrato').removeAttr('hidden');
    $('.responsableCContrato').attr('hidden',true);    
    $("input[name='contrato[cedularResponsable]']").val('');
  }
});
if ($("input[name='firmanteContrato']").val() == 'RESPONSABLE') {
    $('.responsableCContrato').removeAttr('hidden');
    $('.pacienteCContrato').attr('hidden',true);
    //$("input[name='consentimiento[huellaPaciente]']").val('');
  }else if($("input[name='firmanteContrato']").val() == 'PACIENTE'){    
    $('.pacienteCContrato').removeAttr('hidden');
    $('.responsableCContrato').attr('hidden',true);
    $("input[name='contrato[cedularResponsable]']").val('');
  }


/*cambio de tipo de archivo contrato*/
$("select[name='contrato[idTipoArchivo]']").change(function(){
  var tipoArchivo=$(this).val();
  //alert(tipoArchivo);
  if (tipoArchivo == 1 || tipoArchivo == 2 ) {
    if ($("select[name='contrato[idPlanTratamiento]']").val() == '') {
   // alert(tipoArchivo);
      $('.msgPlan').html('Este campo es obligatorio para retiro o contrato.');
    }
  }
});
      /*if (tipoArchivo == 1 || tipoArchivo == 2 ) {
        alert(tipoArchivo);
        if ($("input[name='contrato[idPlanTratamiento]']").val() == '') {
          $('.msgPlan').html('Este campo es obligatorio para retiro o contrato.');
        }
      }*/

/*Función de manejo de contenido del tipo de consentimiento*/ 
 
$("select[name='consentimiento[idTipoConsentimiento]']").change(function(){
  valor=$(this).val();
  if (valor != '') {
      $.ajax({
          type: 'POST',
          url:'/consentimiento/'+valor+'/contenido',
          dataType: 'json',
          beforeSend: function () {
            $('body').addClass('loading'); //Agregamos la clase loading al body
          },
          success: function(respuesta){
            $('body').removeClass('loading');  
            $('.contenidoTipoConsentimiento').text(respuesta.contenido);
          },
                  // código a ejecutar si la petición falla;
          error : function(xhr, status) {
            $('body').removeClass('loading');
            $('.resp').removeAttr('hidden');
            $('.resp').html('Disculpe, existió un problema en la conexión.');
          },  
      });
  
  }else{
      $('.contenidoTipoConsentimiento').text('');
  }
});


/*Función de manejo de contenido del tipo de contrato*/ 
 
$("select[name='contrato[idTipoContrato]']").change(function(){
  valor=$(this).val();
  if (valor != '') {
      $.ajax({
          type: 'POST',
          url:'/contrato/'+valor+'/contenido',
          dataType: 'json',
          beforeSend: function () {
            $('body').addClass('loading'); //Agregamos la clase loading al body
          },
          success: function(respuesta){
            $('body').removeClass('loading');  
            $('.contenidoTipoContrato').text(respuesta.contenido);
          },
                  // código a ejecutar si la petición falla;
          error : function(xhr, status) {
            $('body').removeClass('loading');
            $('.resp').removeAttr('hidden');
            $('.resp').html('Disculpe, existió un problema en la conexión.');
          },  
      });
  
  }else{
      $('.contenidoTipoContrato').text('');
  }
});

/*pieza=$("select[name='presupuesto[idPieza]']").val();
if (pieza == 53 || pieza == 54 || pieza == 55 || pieza == 56 || pieza == 57 || pieza == 58) {
  $("select[name='areBucalPresupuesto']").val('maxilar').change();
  $("select[name='presupuesto[idPieza]']").removeAttr('disabled');
  
}else{
  $("select[name='areBucalPresupuesto']").val('pieza').change();
  $("select[name='presupuesto[idPieza]']").removeAttr('disabled');
}*/

// Selección de las piezas dentales segun lo que se requiera en el tratamiento
$("select[name='areBucalPresupuesto']").change(function(){
  valor=$(this).val();
  if (valor != '') {
    $.ajax({
        type: 'POST',
        url:'/plan/datoPiezaDental',
        data:{'dato':valor},
        dataType: 'json',
        beforeSend: function () {
          $('body').addClass('loading'); //Agregamos la clase loading al body
        },
        success: function(respuesta){
          $('body').removeClass('loading');  
          if (respuesta.length >0) {
          $("select[name='presupuesto[idPieza]']").empty();
          if (valor == 'pieza') {
            //alert();
            $("select[name='presupuesto[idPieza]']").append("<option value=''>Seleccione pieza dental</option>");
            $('.superficie').removeAttr('hidden');
            $('.nombrePiezaDental').text('Pieza dental');
            $('.proceSelect').removeClass('col-xg-10 col-lg-10 col-md-10').addClass('col-xg-4 col-lg-4 col-md-4');
          }else if (valor == 'maxilar') {
            $("select[name='presupuesto[idPieza]']").append("<option value=''>Seleccione área maxilar</option>");
            $('.superficie').attr('hidden',true);
            $('.nombrePiezaDental').text('Área Maxilar');
            $("select[name='presupuesto[idSuperficie]']").val(8).change();
            $('.proceSelect').removeClass('col-xg-4 col-lg-4 col-md-4').addClass('col-xg-10 col-lg-10 col-md-10');
          }
          for (var i=0; i<respuesta.length; i++) { 
            datos=respuesta[i];
            var separacion= datos.split(';');
            //console.log(separacion);
            $("select[name='presupuesto[idPieza]']").append("<option value='"+separacion[0]+"'>"+separacion[1]+"</option>");
            //msgForNormal = msgForNormal + dato[i] + ' - '; 
          }
          $("select[name='presupuesto[idPieza]']").trigger("chosen:updated");
          $("select[name='presupuesto[idPieza]']").removeAttr("disabled");         
          $('.selecPieza').removeAttr('hidden');  
          $('.areaBucal').attr('hidden',true);  

        }   
          //console.log(respuesta);
        },
                // código a ejecutar si la petición falla;
        error : function(xhr, status) {
          $('body').removeClass('loading');
          $('.resp').removeAttr('hidden');
          $('.resp').html('Disculpe, existió un problema en la conexión.');
        },  
    });
  }else{
      $('.selecPieza').attr('hidden',true);
      $("select[name='presupuesto[idPieza]']").attr("disabled",true);
      $('.superficie').removeAttr('hidden');
      $('.nombrePiezaDental').text('Pieza');
      $("select[name='presupuesto[idPieza]']").empty();
      $('.proceSelect').removeClass('col-xg-10 col-lg-10 col-md-10').addClass('col-xg-4 col-lg-4 col-md-4');
  }
});
//Simluación efectiva de plan
$('.guardarsimulacion').click(function(){
   //planTotal=$("input[name='planTotal']").val();
   //alert('hola'+planTotal);
  planTotal=$("input[name='planTotal']").val();
  valorCuotaInicial=$("input[name='valorCuotaInicial']").val();
  numeroCuotas=$("input[name='numeroCuotas']").val();
  if (valorCuotaInicial != '') {
    if (numeroCuotas != '') {      
      swal({
        title: "¿Estás seguro?",
        text: "Está por confirmar el plan de tratamiento, lo que indica que se acoge a los costos evaluados y condiciones ya explicadas.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Confirmar!',
        cancelButtonText: "Cancelar",
        allowEscapeKey: false,
        allowOutsideClick: false
      }).then(function (isConfirm) {
        if (isConfirm) {
          swal({
            title: 'Confirmado!',
            text: 'Se ha colocado el plan de tratamiento en proceso.',
            type: 'success',
            allowEscapeKey: false,
            allowOutsideClick: false
          }).then(function (isConfirm) {
            if (isConfirm) {
              //alert('holap'+$("#formSimulador").serialize());
              $("#formSimulador").attr('action','/plan/cambioPlanEstado');
              $('input[name=numeroCuotas]').removeAttr('disabled');
              $('body').addClass('loading');
              $('#formSimulador')[0].submit();    
            }
          }); 
        }
      },function (dismiss) {
          if (dismiss === 'cancel') {
            swal("Cancelado!", " Se ha cancelado exitosamente.", "error");
          };
        }
      );
    return false;
    }else{
      $('.errorNumeroCuotas').html("Este campo es obligatorio.");
    }
  }else{
    $('.errorValorCuotaInicial').html("Este campo es obligatorio.");
  }
    
});
//Vovler a permitir simulacion con nuevos datos
$('.refrescoSimulacion').click(function(){
  $("input[name='valorCuotaInicial']").removeAttr('disabled');
  $("input[name='numeroCuotas']").removeAttr('disabled');
  $(".simul").removeAttr('hidden');
  $(".cuotaMensual").attr('hidden',true);
});
/*$.ajax({
      // especifica si será una petición POST o GET
      type: 'POST',

      // la URL para la petición
      url:url,
      // la información a enviar
      data: {'fechaN':valor},
      // el tipo de información que se espera de respuesta
      dataType : 'json',
      beforeSend: function () {
        $('body').addClass('loading'); //Agregamos la clase loading al body
      },
      // código a ejecutar si la petición es satisfactoria;
      success : function(data) {
        $('body').removeClass('loading');
        if (data.length >0) {
          $("select[name='paciente[tipoIdentificacion]']").empty();
          $("select[name='paciente[tipoIdentificacion]']").append("<option value=''>Seleccione un tipo de documento</option>");
          for (var i=0; i<data.length; i++) { 
            //console.log(data[i]);
            datos=data[i];
            var separacion= datos.split(';');
            $("select[name='paciente[tipoIdentificacion]']").append("<option value='"+separacion[0]+"'>"+separacion[1]+"</option>");
            //msgForNormal = msgForNormal + dato[i] + ' - '; 
          }
          $("select[name='paciente[tipoIdentificacion]']").trigger("chosen:updated");        
        }      
      },

      // código a ejecutar si la petición falla;
      error : function(xhr, status) {
        $('body').removeClass('loading');
        $('#respu').html("Disculpe, existió un problema en la conexión.").attr( 'class', 'alert alert-danger alert-dismissable'); 
      },
    });*/


/**
 Se cambia el envio de formularios de $(form).ajaxSubmit(); por $(form)[0].submit(); se realiza el cambio a causa
  del manejo de necesita de ajax para evitar este envio la solucion para este error de no encontrar la funcion de ajaxSubmit()
  fue con este nuevo js llamado <script src="http://malsup.github.com/jquery.form.js"></script> 
 Error generado por el envia del controlador de jquery producido por $(form).submit(); 
  Para evitar el error se debe enviar solo el primer componenete del array $(form)[0].submit();
  Uncaught RangeError: Maximum call stack size exceeded
    at jquery.js:2277
    at c (jquery.js:2498)
    at oe.select (jquery.js:2677)
    at Function.oe [as find] (jquery.js:845)
    at _.fn.init.find (jquery.js:2873)
    at $.validator.elements (jquery.validate.js:646)
    at $.validator.checkForm (jquery.validate.js:466)
    at $.validator.form (jquery.validate.js:454)
    at HTMLFormElement.<anonymous> (jquery.validate.js:105)
    at HTMLFormElement.dispatch (jquery.js:5183)
**/