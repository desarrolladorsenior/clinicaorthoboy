function confirmDelete(){
    swal({
      title: "¿Estás seguro?",
      text: "Estás por borrar un registro, este no se podrá recuperar más adelante.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminarlo!',
      cancelButtonText: "Cancelar",
      allowEscapeKey: false,
      allowOutsideClick: false
       }).then(function (isConfirm) {
        if (isConfirm) {
            
           swal({
              title: 'Eliminado!',
              text: 'Se ha eliminado correctamente el registro.',
              type: 'success',
              allowEscapeKey: false,
              allowOutsideClick: false
           }).then(function (isConfirm) {
                if (isConfirm) {
                  alert('sv'+$('.eliminarDato').attr('action'));
                  //alert($('.eliminarDato').serialize());
                  $('body').addClass('loading');
                  $('.eliminarDato').submit();
                }});          

          }}
        , function (dismiss) {
              if (dismiss === 'cancel') {
                swal("Cancelado!", " Se ha cancelado exitosamente.", "error");
              };
            }
        );
    return false;
    }