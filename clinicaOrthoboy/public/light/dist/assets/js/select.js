
/*$('[data-toggle="select2"]').select2();*/
$('select').select2({  width:'100%'});
/*$('select').chosen({width:'100%'});*/


/*Manejo de datos de datatime*/
/*.flatpickr({
    locale: 'es',
    enableTime:!0,
    dateFormat:"Y-m-d H:i"
});*/
$('.dateInitial').datetimepicker({
    locale: 'es-es',
   // format: 'yyyy-MM-dd  HH:mm',
    format: "yyyy-mm-dd HH:mm:ss.SS",
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    //modal: true,
    footer: true
});


$('.dateEnd').datetimepicker({
    locale: 'es-es',
   // format: 'yyyy-MM-dd  HH:mm',
    format: "yyyy-mm-dd HH:mm:ss.SS",
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    //modal: true,
    footer: true,
    datepicker: { 
        disableDates:  function (date) {
           //alert(date+' fecha apertura'+$('#esterilizacion_fechaApertura').val());
           if ($('#esterilizacion_fechaApertura').val() != '') {
            const currentDate = new Date($('#esterilizacion_fechaApertura').val());
            return date > currentDate-1 ? true : false;
            }else{
                const currentDate = new Date();
                return date > currentDate-1 ? true : false;
            }
        },
    }, 
});
/*Manejo de horas para agenda usuario*/
$('.clockTime').clockpicker({
    placement: 'bottom',
    align: 'left',
    donetext: 'Aceptar'
});
$('form').children().children().children('input.dateInitial').datetimepicker({
    locale: 'es-es',
   // format: 'yyyy-MM-dd  HH:mm',
    format: "yyyy-mm-dd HH:mm:ss.SS",
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    //modal: true,
    footer: true
});
$('#formEsterilizacionEdit').children().children().children('input.dateEnd').datetimepicker({
    locale: 'es-es',
   // format: 'yyyy-MM-dd  HH:mm',
    format: "yyyy-mm-dd HH:mm:ss.SS",
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    //modal: true,
    footer: true,
    datepicker: { 
        disableDates:  function (date) {
           //alert(date+' fecha apertura'+$('#esterilizacion_fechaApertura').val());
           if ($('#esterilizacion_fechaApertura').val() != '') {
            const currentDate = new Date($('#esterilizacion_fechaApertura').val());
            return date > currentDate-1 ? true : false;
            }else{
                const currentDate = new Date();
                return date > currentDate-1 ? true : false;
            }
        },
    }, 
});

$('.datetimes').datetimepicker({
    locale: 'es-es',
   // format: 'yyyy-MM-dd  HH:mm',
    format: "yyyy-mm-dd HH:mm:ss.SS",
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    //modal: true,
    footer: true,
    datepicker: { 
        disableDates:  function (date) {
           // alert(date);
            const currentDate = new Date();
            return date > currentDate-1 ? true : false;
        },
    },   
});

$('.dateAprobation').datetimepicker({
    locale: 'es-es',
   // format: 'yyyy-MM-dd  HH:mm',
    format: "yyyy-mm-dd HH:mm:ss.SS",
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    //modal: true,
    footer: true,
    datepicker: { 
        disableDates:  function (date) {
           // alert(date);
            const currentDate = new Date();
            return date <= currentDate ? true : false;
        },
    },   
});


$(".js-datepicker").flatpickr({
    locale: 'es',
    dateFormat:"Y-m-d"
});

$(".nacimiento").flatpickr({
    locale: 'es',
    format: "Y-m-d",
    maxDate: "today"
});
