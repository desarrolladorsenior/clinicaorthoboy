(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ES (Spanish; Español)
 */
$.extend( $.validator.messages, {
	required: "Este campo es obligatorio.",
	maxlength: $.validator.format( "El limite máximo de caracteres permitido es {0}." ),
	minlength: $.validator.format( "El limite mínimo de caracteres requerido es {0} caracteres o más." ),
	remote: "Por favor, rellene este campo.",
	email: "Registre una dirección de correo válida.",
	url: "Por favor, escribir una URL válida.",
	date: "Por favor, escribir una fecha válida.",
	dateISO: "Por favor, escribir una fecha (ISO) válida.",
	number: "Registre datos numéricos y como excepción un punto.",
	digits: "Solo se permite datos numéricos.",
	creditcard: "Por favor, escribir un número de tarjeta válido.",
	equalTo: "Por favor, escribir el mismo valor de nuevo.",
	extension: "Por favor, escribir un valor con una extensión aceptada.",	
	rangelength: $.validator.format( "Por favor, escribir un valor entre {0} y {1} caracteres." ),
	range: $.validator.format( "Por favor, escribir un valor entre {0} y {1}." ),
	max: $.validator.format( "Por favor, escribir un valor menor o igual a {0}." ),
	min: $.validator.format( "Por favor, escribir un valor mayor o igual a {0}." ),
	nifES: "Por favor, escribir un NIF válido.",
	nieES: "Por favor, escribir un NIE válido.",
	cifES: "Por favor, escribir un CIF válido."
} );
return $;
}));