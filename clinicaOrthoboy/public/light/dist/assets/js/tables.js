
function newTable(id){
  $(id).DataTable({ 
       responsive: true,
       dom:'frtilp',
       "pageLength": 5,
       ordering:  false,
       "language": 
          {
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados.",
              "sEmptyTable":     "Ningún dato disponible en esta tabla.",              
              "sInfo":           "Mostrando <b>_START_</b> a <b>_END_</b> de <b>_TOTAL_</b> registros.",
              "sInfoEmpty":      "Mostrando 0 a 0 de 0 datos.",
              "sInfoFiltered":   "(Filtro total <b>_MAX_</b> entrante.)",
              "sInfoPostFix":    "",
              "sDecimal":        "",
              "sThousands":      ",",
              "sLengthMenu":     "Mostrar <b>_MENU_</b> registros.",
              "sProcessing":     "Procesando...",             
              "sSearchPlaceholder": "Buscar",
              "sSearch" :        "",
              "sUrl":             "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate":{
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Sig",
                "sPrevious": "Ant"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
          
        },
  });  
}
