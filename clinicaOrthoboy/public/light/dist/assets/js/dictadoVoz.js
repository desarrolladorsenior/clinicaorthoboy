 console.log(window.hasOwnProperty('SpeechRecognition'));
 var recognition;
    var recognizing = false;
    if ('SpeechRecognition' in window &&  !('webkitSpeechRecognition' in window)) {
          //alert("¡API soportada! SpeechRecognition");
        //  console.log(window);
        var SpeechRecognition = SpeechRecognition //|| webkitSpeechRecognition
        var SpeechGrammarList = SpeechGrammarList //|| webkitSpeechGrammarList
        var SpeechRecognitionEvent = SpeechRecognitionEvent// || webkitSpeechRecognitionEvent

        var colors = [ 'aqua' , 'azure' , 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral', 'crimson', 'cyan', 'fuchsia', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green', 'indigo', 'ivory', 'khaki', 'lavender', 'lime', 'linen', 'magenta', 'maroon', 'moccasin', 'navy', 'olive', 'orange', 'orchid', 'peru', 'pink', 'plum', 'purple', 'red', 'salmon', 'sienna', 'silver', 'snow', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'white', 'yellow'];
        var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;'

        var recognition = new SpeechRecognition();
        var speechRecognitionList = new SpeechGrammarList();
        speechRecognitionList.addFromString(grammar, 1);
        recognition.grammars = speechRecognitionList;
        recognition.lang = 'en-US';
        recognition.interimResults = true;
        recognition.maxAlternatives = 1;

        var diagnostic = document.querySelector('.output');
        var bg = document.querySelector('html');
        var hints = document.querySelector('.hints');

        var colorHTML= '';
        colors.forEach(function(v, i, a){
          console.log(v, i);
          colorHTML += '<span style="background-color:' + v + ';"> ' + v + ' </span>';
        });
        hints.innerHTML = 'Tap/click then say a color to change the background color of the app. Try '+ colorHTML + '.';

        recognition.onresult = function(event) {

          var last = event.results.length - 1;
          var color = event.results[last][0].transcript;

          diagnostic.textContent = 'Result received: ' + color + '.';
          bg.style.backgroundColor = color;
          console.log('Confidence: ' + event.results[0][0].confidence);
        }

        recognition.onspeechend = function() {
          recognition.stop();
        }

        recognition.onnomatch = function(event) {
          diagnostic.textContent = "I didn't recognise that color.";
        }

        recognition.onerror = function(event) {
          diagnostic.textContent = 'Error occurred in recognition: ' + event.error;
        }


        $('#texto').click(function() {        
            if (recognizing == false) {
                recognition.start();          
                recognizing = true;                
                $('#procesar').attr( 'class', 'btn btn-danger'); 
            } else {
                recognition.stop();
                recognizing = false;                
                $('#procesar').attr( 'class', 'btn btn-success'); 
            }
           
        });
    
        /*$( "body .navbar-custom" ).click(function() {   
            if (recognizing == false) {
                recognition.start();          
                recognizing = true;
                $('#miModal').modal('show');
                          
            } 
        });

        $( "body .modal-body" ).click(function() {   
            if (recognizing == true) {
                recognition.stop();
                recognizing = false; 
                $('#miModal').modal('hide');             
            }
        });*/
    
    
    } else if (('webkitSpeechRecognition' in window)) {
        // alert("¡API soportada! webkitSpeechRecognition");
        recognition = new webkitSpeechRecognition();
        recognition.lang = "es-VE";
        recognition.continuous = true;
        recognition.interimResults = true;

        recognition.onstart = function() {
            recognizing = true;
            console.log("empezando a escuchar");
        }
        recognition.onresult = function(event) {

            for (var i = event.resultIndex; i < event.results.length; i++) {
                if(event.results[i].isFinal){
                //alert($('#procesar').attr( 'class') );
                    if (event.results[i][0].transcript == 'paciente') {
                        location.href="panelPrincipal";
                    }else if (event.results[i][0].transcript == 'salir') {
                        location.href="logout";
                    }else if ($('#procesar').attr( 'class') == 'btn btn-danger' ) {
                       document.getElementById("texto").value += event.results[i][0].transcript; 
                    }
                }    
            }
            
            //texto
        }
        recognition.onerror = function(event) {
        }
        recognition.onend = function() {
            recognizing = false;
            $('#procesar').attr( 'class', 'btn btn-success'); 
            console.log("terminó de escuchar, llegó a su fin");

        }

        $('#texto').click(function() {        
            if (recognizing == false) {
                recognition.start();          
                recognizing = true;                
                $('#procesar').attr( 'class', 'btn btn-danger'); 
            } else {
                recognition.stop();
                recognizing = false;                
                $('#procesar').attr( 'class', 'btn btn-success'); 
            }
           
        });
    
        /*$( "body .navbar-custom" ).click(function() {   
            if (recognizing == false) {
                recognition.start();          
                recognizing = true;
                $('#miModal').modal('show');
                          
            } 
        });

        $( "body .modal-body" ).click(function() {   
            if (recognizing == true) {
                recognition.stop();
                recognizing = false; 
                $('#miModal').modal('hide');             
            }
        });*/


        
    }else{
        alert("¡API no soportada!");
    }

