<?php

namespace App\Controller;

use App\Entity\EstadoMantenimiento;
use App\Form\EstadoMantenimientoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de estado mantenimiento utilizado para el manejo de los estados en los mantenimiento de los elementos que lo requieran.
 *
 * @Route("/estadoMantenimiento")
 */
class EstadoMantenimientoController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de un estado mantenimiento y registro del mismo.
     *
     * @Route("/", name="estadoMantenimiento_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'estado de mantenimiento');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlestadoMantenimiento ="SELECT estadoMantenimiento FROM App:EstadoMantenimiento estadoMantenimiento
                    ORDER BY estadoMantenimiento.idEstadoMantenimiento DESC";  
                    $queryestadoMantenimiento = $entityManager->createQuery($dqlestadoMantenimiento);
                    $estadoMantenimientos= $queryestadoMantenimiento->getResult();
                    
                    $estadoMantenimiento = new EstadoMantenimiento();
                    $form = $this->createForm(EstadoMantenimientoType::class, $estadoMantenimiento);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($estadoMantenimiento);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('estadoMantenimiento_index');
                    }
                    return $this->render('estadoMantenimiento/index.html.twig', [
                        'estadoMantenimientos' => $estadoMantenimientos,
                        'estadoMantenimiento' => $estadoMantenimiento,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

     /**
     * Metodo utilizado para la edición de un estado de mantenimiento.
     *
     * @Route("/{idEstadoMantenimiento}/edit", name="estadoMantenimiento_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EstadoMantenimiento $estadoMantenimiento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'estado de mantenimiento');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    
                    $form = $this->createForm(EstadoMantenimientoType::class, $estadoMantenimiento);
                    $form->handleRequest($request);
                    if ($estadoMantenimiento->getIdEstadoMantenimiento() == 1 or $estadoMantenimiento->getIdEstadoMantenimiento() == 2 or $estadoMantenimiento->getIdEstadoMantenimiento() == 3 or $estadoMantenimiento->getNombre() == 'PROGRAMADO' or $estadoMantenimiento->getNombre() == 'REALIZADO' or $estadoMantenimiento->getNombre() == 'CANCELADO') {
                        $this->addFlash('mensaje', 'Este registro no puede ser modificado!');
                        return $this->redirectToRoute('estadoMantenimiento_index');                        
                    }else{
                        if ($form->isSubmitted() && $form->isValid()) {          
                            $entityManager->persist($estadoMantenimiento);
                            $entityManager->flush();                       
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                            return $this->redirectToRoute('estadoMantenimiento_index');
                        }
                    }                    

                    return $this->render('estadoMantenimiento/modalEdit.html.twig', [
                        'estadoMantenimiento' => $estadoMantenimiento,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de un estado mantenimiento.
     *
     * @Route("/{idEstadoMantenimiento}", name="estadoMantenimiento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EstadoMantenimiento $estadoMantenimiento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'estado de mantenimiento');  
              if (sizeof($permisos) > 0) {
               // var_dump($estadoMantenimiento->getIdEstadoMantenimiento());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($estadoMantenimiento);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('estadoMantenimiento_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('estadoMantenimiento_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('estadoMantenimiento_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }
}
