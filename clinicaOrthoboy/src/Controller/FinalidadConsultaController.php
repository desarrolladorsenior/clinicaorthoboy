<?php

namespace App\Controller;

use App\Entity\FinalidadConsulta;
use App\Form\FinalidadConsultaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;

/**
 * Controlador de finalidad consulta utilizado para el la generación del diagnostico de cups, es decir el manejo de los informes que se deben evaluar según las normas.
 *
 * @Route("/finalidadConsulta")
 */
class FinalidadConsultaController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de todas las finalidades de consultas y registro de las mismas.
     *
     * @Route("/", name="finalidadConsulta_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual;               
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'finalidad consulta');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $entityManager = $this->getDoctrine()->getManager();
                $dqlFinalidadConsulta ="SELECT finalidadConsulta FROM App:FinalidadConsulta finalidadConsulta
                ORDER BY finalidadConsulta.idFinalidadConsulta DESC";  
                $queryFinalidadConsulta = $entityManager->createQuery($dqlFinalidadConsulta);
                $finalidadesConsulta= $queryFinalidadConsulta->getResult();

                $finalidadConsulta = new FinalidadConsulta();
                $form = $this->createForm(FinalidadConsultaType::class, $finalidadConsulta);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager->persist($finalidadConsulta);
                    $entityManager->flush();
                    $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                    return $this->redirectToRoute('finalidadConsulta_index');
                }

                return $this->render('finalidadConsulta/index.html.twig', [
                    'finalidadesConsultas' => $finalidadesConsulta,
                    'finalidadConsulta' => $finalidadConsulta,
                    'form' => $form->createView(),
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }  
                
            }
        }    
    }


    /**
     * Metodo utilizado para la edición de una finalidad de consultas.
     *
     * @Route("/{idFinalidadConsulta}/edit", name="finalidadConsulta_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FinalidadConsulta $finalidadConsulta,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual;               
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'finalidad consulta');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $form = $this->createForm(FinalidadConsultaType::class, $finalidadConsulta);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $this->getDoctrine()->getManager()->flush();
                    $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                    return $this->redirectToRoute('finalidadConsulta_index');
                }
                return $this->render('finalidadConsulta/modalEdit.html.twig', [
                    'finalidadConsulta' => $finalidadConsulta,
                    'form' => $form->createView(),
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }  
                
            }
        }    
    }

    /**
     * Metodo utilizado para la eliminación de una finalidad de consulta.
     *
     * @Route("/{idFinalidadConsulta}", name="finalidadConsulta_delete", methods={"DELETE"})
     */
    public function delete(Request $request, FinalidadConsulta $finalidadConsulta,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
        return $this->redirectToRoute('usuario_logout'); 
      } else{
        // La variable $_SESSION['usuario'] es un ejemplo. 
        //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
        if(isset($_SESSION['time'])){ 
          $tiempo = $_SESSION['time']; 
        }else{ 
          $tiempo = strtotime(date("Y-m-d H:i:s"));
        } 
        $inactividad =1800;   //Exprecion en segundos. 
        $actual =  strtotime(date("Y-m-d H:i:s")); 
        $tiempoTranscurrido=($actual-$tiempo);
        if(  $tiempoTranscurrido >= $inactividad){ 
          $session->invalidate();
          return $this->redirectToRoute('usuario_logout');
         // En caso que este sea mayor del tiempo seteado lo deslogea. 
        }else{ 
          $_SESSION['time'] =$actual; 
          $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
          $permisos=$permisoPerfil->verificacionPermisos($perfil,'finalidad consulta');  
          if (sizeof($permisos) > 0) {
            try{
              if ($this->isCsrfTokenValid('delete'.$finalidadConsulta->getIdFinalidadConsulta(), $request->request->get('_token'))) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $entityManager->remove($finalidadConsulta);
                  $entityManager->flush();
                  $successMessage= 'Se ha eliminado de forma correcta!';
                  $this->addFlash('mensaje',$successMessage);
                  return $this->redirectToRoute('finalidadConsulta_index');
              }
            }catch (\Doctrine\DBAL\DBALException $e) {
              if ($e->getCode() == 0){
                  if ($e->getPrevious()->getCode() == 23503){
                      $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                      $this->addFlash('error',$successMessage1);
                      return $this->redirectToRoute('finalidadconsulta_index');
                  }else{
                      throw $e;
                  }
              }else{
                  throw $e;
              }
            } 
            return $this->redirectToRoute('finalidadConsulta_index');
          }else{
            $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
            return $this->redirectToRoute('panel_principal');
          }    
        }
      }      
    }
}
