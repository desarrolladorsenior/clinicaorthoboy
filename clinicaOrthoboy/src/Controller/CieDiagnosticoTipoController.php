<?php

namespace App\Controller;

use App\Entity\CieDiagnosticoTipo;
use App\Form\CieDiagnosticoTipoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de cie Diagnostico Tipo utilizado para el manejo del odontograma de un paciente, donde con el mismo se muestran los simbolos que 
 * acogen el diagnostico o los colores utilizados para la representación, ademas para escoger el diagnostcio que tiene la pieza o superficie dental.
 *
 * @Route("/cieDiagnosticoTipo")
 */
class CieDiagnosticoTipoController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de los diagnosticos que se exponen para la ejecućión del odontograma, además se manejan con el registro 
     * de nuevos diagnosticos.
     *
     * @Route("/", name="cieDiagnosticoTipo_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipo diagnostico - RIP');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlCieDiagnosticoTipo ="SELECT cieDiagnosticoTipo FROM App:CieDiagnosticoTipo cieDiagnosticoTipo
                    ORDER BY cieDiagnosticoTipo.idCieDiagnosticoTipo DESC";  
                    $queryCieDiagnosticoTipo = $entityManager->createQuery($dqlCieDiagnosticoTipo);
                    $cieDiagnosticoTipos= $queryCieDiagnosticoTipo->getResult();
                    
                    $cieDiagnosticoTipo = new CieDiagnosticoTipo();
                    $cieDiagnosticoTipo->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(CieDiagnosticoTipoType::class, $cieDiagnosticoTipo);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $cieDiagnosticoTipo->setEstado(true);
                        if ($cieDiagnosticoTipo->getSimbolo() != '') {
                           $cieDiagnosticoTipo->setColor(null) ;
                        }
                        $entityManager->persist($cieDiagnosticoTipo);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('cieDiagnosticoTipo_index');
                    }
                    return $this->render('cieDiagnosticoTipo/index.html.twig', [
                        'cieDiagnosticoTipos' => $cieDiagnosticoTipos,
                        'cieDiagnosticoTipo' => $cieDiagnosticoTipo,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la edición de estado de diagnosticos.
     *
     * @Route("/editarEstadoCieOdontologicoTipo", name="cieDiagnosticoTipo_estadoCambio", methods={"GET","POST"})
     */
    public function editEstadoCieOdontologicoTipo(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipo diagnostico - RIP');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $em=$this->getDoctrine()->getManager();
                $response = new JsonResponse();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }
                $dqlModificarEstado="UPDATE App:CieDiagnosticoTipo cieDiagnosticoTipo SET cieDiagnosticoTipo.estado= :estado WHERE cieDiagnosticoTipo.idCieDiagnosticoTipo= :idCieDiagnosticoTipo";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idCieDiagnosticoTipo', $id);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    

    }

    /**
     * Metodo utilizado para la edición de diagnosticos para el odontograma.
     * 
     * @Route("/{idCieDiagnosticoTipo}/edit", name="cieDiagnosticoTipo_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CieDiagnosticoTipo $cieDiagnosticoTipo,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipo diagnostico - RIP');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                     $dqlCieDiagnosticoTipo ="SELECT cieDiagnosticoTipo FROM App:CieDiagnosticoTipo cieDiagnosticoTipo
                                where cieDiagnosticoTipo.idCieDiagnosticoTipo = :id";  
                    $queryCieDiagnosticoTipo = $entityManager->createQuery($dqlCieDiagnosticoTipo)->setParameter('id',$cieDiagnosticoTipo->getIdCieDiagnosticoTipo());
                    $cieDiagnosticoTipos= $queryCieDiagnosticoTipo->getResult();
                    foreach ($cieDiagnosticoTipos as $selectDatos) {
                        $estado=$selectDatos->getEstado();
                    }
                    $form = $this->createForm(CieDiagnosticoTipoType::class, $cieDiagnosticoTipo);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        if ($cieDiagnosticoTipo->getSimbolo() != '') {
                           $cieDiagnosticoTipo->setColor(null) ;
                        }
                        $cieDiagnosticoTipo->setEstado($estado);
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('cieDiagnosticoTipo_index');
                    }

                    
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de diagnosticos de odontogrma, donde solo se efectua en caso que no se esten utilizando en diagnosticos 
     * dados a pacientes.
     *
     * @Route("/{idCieDiagnosticoTipo}/delete", name="cieDiagnosticoTipo_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CieDiagnosticoTipo $cieDiagnosticoTipo,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipo diagnostico - RIP');  
              if (sizeof($permisos) > 0) {
                var_dump($cieDiagnosticoTipo->getIdCieDiagnosticoTipo());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($cieDiagnosticoTipo);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('cieDiagnosticoTipo_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('cieDiagnosticoTipo_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('cieDiagnosticoTipo_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }
}
