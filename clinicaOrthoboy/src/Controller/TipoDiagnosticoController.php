<?php

namespace App\Controller;

use App\Entity\TipoDiagnostico;
use App\Form\TipoDiagnosticoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de tipo diagnostico utilizado para el manejo de los mismos dentro de el diagnostico final que se genera para un odontograma.
 *
 * @Route("/tipoDiagnostico")
 */
class TipoDiagnosticoController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de todos los tipo Diagnostico y registro del mismo.
     *
     * @Route("/", name="tipoDiagnostico_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipo de diagnosticos');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlTipoDiagnostico ="SELECT tipoDiagnostico FROM App:TipoDiagnostico tipoDiagnostico
                    ORDER BY tipoDiagnostico.idTipoDiagnostico DESC";  
                    $queryTipoDiagnostico = $entityManager->createQuery($dqlTipoDiagnostico);
                    $tipoDiagnosticos= $queryTipoDiagnostico->getResult();
                    
                    $tipoDiagnostico = new TipoDiagnostico();
                    $form = $this->createForm(TipoDiagnosticoType::class, $tipoDiagnostico);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($tipoDiagnostico);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('tipoDiagnostico_index');
                    }
                    return $this->render('tipoDiagnostico/index.html.twig', [
                        'tipoDiagnosticos' => $tipoDiagnosticos,
                        'tipoDiagnostico' => $tipoDiagnostico,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }   
    }


    /**
     * Metodo utilizado para la edición de un tipo Diagnostico.
     *
     * @Route("/{idTipoDiagnostico}/edit", name="tipoDiagnostico_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TipoDiagnostico $tipoDiagnostico,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipo de diagnosticos');  
                if (sizeof($permisos) > 0) {
                    $form = $this->createForm(TipoDiagnosticoType::class, $tipoDiagnostico);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('tipoDiagnostico_index');
                    }
                    return $this->render('tipoDiagnostico/modalEdit.html.twig', [
                        'tipoDiagnostico' => $tipoDiagnostico,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de un tipo Diagnostico.
     *
     * @Route("/{idTipoDiagnostico}", name="tipoDiagnostico_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TipoDiagnostico $tipoDiagnostico,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipo de diagnosticos');  
              if (sizeof($permisos) > 0) {
                try{
                    if ($tipoDiagnostico->getIdTipoDiagnostico() == 1 || $tipoDiagnostico->getIdTipoDiagnostico() == 2) {
                        $successMessage= '¡.. Error al eliminar el registro. Este es predeterminado por el sistema.!';
                        $this->addFlash('error',$successMessage);
                    }else{
                        if ($this->isCsrfTokenValid('delete'.$tipoDiagnostico->getIdTipoDiagnostico(), $request->request->get('_token'))) {
                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->remove($tipoDiagnostico);
                            $entityManager->flush();
                            $successMessage= 'Se ha eliminado de forma correcta!';
                            $this->addFlash('mensaje',$successMessage);
                            return $this->redirectToRoute('tipoDiagnostico_index');
                        }  
                    }
                   

                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('tipoDiagnostico_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('tipoDiagnostico_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }    
    }
}
