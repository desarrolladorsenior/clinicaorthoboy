<?php

namespace App\Controller;

use App\Entity\SalidaConsultorio;
use App\Form\SalidaConsultorioType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

/**
 * Controlador de salida consultorio utilizado para el manejo de las entradas de un consultorio y solventar las salidas que pueden ser generadas  en cada lugar.
 *
 * @Route("/salidaConsultorio")
 */
class SalidaConsultorioController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de todas las salidas de los consultorios y si es un rol diferente al super administrador se 
     * visualizaran las salidas del consultorio dependiendo el usuario en sistema a que sede pertenezca.
     *
     * @Route("/", name="salidaConsultorio_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Salidas de pedido consultorio');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $sedeClinica=$this->get('security.token_storage')->getToken()->getUser()->getIdSucursal()->getIdClinica();
                    $nombrePerfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil()->getNombre();
                    
                    if ($nombrePerfil != 'ROLE_SUPERADMINISTRADOR') {
                        $dqlSalida ="SELECT salidaC FROM App:SalidaConsultorio salidaC INNER JOIN App:Salida salida WITH salida.idSalida=salidaC.idSalida INNER JOIN App:EntradaPedido entradaPedido WITH entradaPedido.idEntradaPedido=salida.idEntradaPedido INNER JOIN App:Clinica clinica WITH clinica.idClinica=entradaPedido.idClinicaUbicacion WHERE clinica.idClinica = :idClinica
                        ORDER BY salidaC.idSalidaConsultorio DESC";  
                        $querySalida = $entityManager->createQuery($dqlSalida)->setParameter('idClinica',$sedeClinica);
                        $salidasConsultorio= $querySalida->getResult();
                    }else{
                        $dqlSalida ="SELECT salidaC FROM App:SalidaConsultorio salidaC ORDER BY salidaC.idSalidaConsultorio DESC";  
                        $querySalida = $entityManager->createQuery($dqlSalida);
                        $salidasConsultorio= $querySalida->getResult();                       
                    }
                    return $this->render('salidaConsultorio/index.html.twig', [
                        'salidasConsultorio' => $salidasConsultorio
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }
    /**
     * Metodo utilizado para la visualización de todas las salidas de los consultorios y si es un rol diferente al super administrador se 
     * visualizaran las salidas del consultorio dependiendo el usuario en sistema a que sede pertenezca.
     *
     * @Route("/new", name="salidaConsultorio_new", methods={"GET","POST"})
     */
    public function new(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Salidas de pedido consultorio');  
                if (sizeof($permisos) > 0) {

                    $salidaConsultorio = new SalidaConsultorio();
                    $entityManager = $this->getDoctrine()->getManager();
                    if (!empty($request->request->get('idSalida'))) {
                        $dqlSalida ="SELECT salida FROM App:Salida salida WHERE salida.idSalida= :idSalida";  
                        $querySalida = $entityManager->createQuery($dqlSalida)->setParameter('idSalida',$request->request->get('idSalida'));
                        $salidas= $querySalida->getResult();
                        foreach ($salidas as $seleccionSalida) {
                            $consultorioSalida=$seleccionSalida->getIdConsultorio();
                            $salidaConsultorio->setIdSalida($seleccionSalida);
                            $salidaConsultorio->setIdConsultorio($consultorioSalida);
                        }
                    }
                    
                    $salidaConsultorio->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(SalidaConsultorioType::class,$salidaConsultorio);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {

                        $dqlCantidadSalida ="SELECT sum(salida.cantidad) as sumaCantidad FROM App:SalidaConsultorio salida WHERE salida.idSalida= :idSalida";  
                        $queryCantidadSalida = $entityManager->createQuery($dqlCantidadSalida)->setParameter('idSalida',$salidaConsultorio->getIdSalida()->getIdSalida());
                        $cantidadSalida= $queryCantidadSalida->getResult();
                        foreach ($cantidadSalida as $seleccionSalida) {
                            $cantidadTotalSalida=$seleccionSalida['sumaCantidad'];
                        }
                        if ($cantidadTotalSalida != '' ){
                            $cantidadDisponibleActual=$salidaConsultorio->getIdSalida()->getCantidad()-$cantidadTotalSalida;
                            if( $cantidadDisponibleActual == 0) {
                                $this->addFlash('error', 'No se puede generar la salida no tiene cantidades disponibles el elemento.');
                                return $this->redirectToRoute('salida_index'); 
                            }else{
                                if ($salidaConsultorio->getCantidad() == 0) {
                                    $this->addFlash('error', 'No se puede registro una la salida con cantidad igual a cero.');
                                    return $this->redirectToRoute('salidaConsultorio_index'); 
                                }else{
                                    if( $salidaConsultorio->getCantidad() <= $cantidadDisponibleActual) {
                                       $entityManager->persist($salidaConsultorio);
                                        $entityManager->flush();                        
                                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                        return $this->redirectToRoute('salida_index');
                                    }else{
                                        $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                        return $this->redirectToRoute('salida_index'); 
                                    }
                                }
                            }
                        }else{
                            if ($salidaConsultorio->getCantidad() == 0) {
                                $this->addFlash('error', 'No se puede registro una la salida con cantidad igual a cero.');
                                return $this->redirectToRoute('salidaConsultorio_index'); 
                            }else{
                                if( $salidaConsultorio->getCantidad() <= $salidaConsultorio->getIdSalida()->getCantidad()) {
                                   $entityManager->persist($salidaConsultorio);
                                    $entityManager->flush();                        
                                    $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                    return $this->redirectToRoute('salida_index');
                                }else{
                                    $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                    return $this->redirectToRoute('salida_index'); 
                                }
                            }
                        }
                        
                    }
                    return $this->render('salidaConsultorio/modalNew.html.twig', [
                        'salidaConsultorio' => $salidaConsultorio,
                        'form' => $form->createView()
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

  
    /**
     * Metodo utilizado para la edición de una salida de consultorio donde se evaluan que las cantidades actuales del ingreso al consultorio permitan 
     * dar salidas según lo que se tenga en el consultorio, ademas verifica que las salidas no tengan cantidades de cero en su ingreso.
     *
     * @Route("/{idSalidaConsultorio}/edit", name="salidaConsultorio_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SalidaConsultorio $salidaConsultorio,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Salidas de pedido consultorio');  
                if (sizeof($permisos) > 0) {

                    $entityManager = $this->getDoctrine()->getManager();
                    $form = $this->createForm(SalidaConsultorioType::class,$salidaConsultorio);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {

                        $dqlCantidadSalida ="SELECT sum(salida.cantidad) as sumaCantidad FROM App:SalidaConsultorio salida WHERE salida.idSalida= :idSalida and salida.idSalidaConsultorio != :idSalidaConsultorio";  
                        $queryCantidadSalida = $entityManager->createQuery($dqlCantidadSalida)->setParameter('idSalida',$salidaConsultorio->getIdSalida()->getIdSalida())->setParameter('idSalidaConsultorio',$salidaConsultorio->getIdSalidaConsultorio());
                        $cantidadSalida= $queryCantidadSalida->getResult();
                        foreach ($cantidadSalida as $seleccionSalida) {
                            $cantidadTotalSalida=$seleccionSalida['sumaCantidad'];
                        }
                        if ($cantidadTotalSalida != '' ){
                            $cantidadDisponibleActual=$salidaConsultorio->getIdSalida()->getCantidad()-$cantidadTotalSalida;
                            if( $cantidadDisponibleActual == 0) {
                                $this->addFlash('error', 'No se puede generar la salida no tiene cantidades disponibles el elemento.');
                                return $this->redirectToRoute('salidaConsultorio_index'); 
                            }else{
                                if ($salidaConsultorio->getCantidad() == 0) {
                                    $this->addFlash('error', 'No se puede actualizar la salida a una cantidad igual a cero.');
                                    return $this->redirectToRoute('salidaConsultorio_index'); 
                                }else{
                                    if( $salidaConsultorio->getCantidad() <= $cantidadDisponibleActual) {

                                       $entityManager->persist($salidaConsultorio);
                                        $entityManager->flush();                        
                                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                        return $this->redirectToRoute('salidaConsultorio_index');
                                    }else{
                                        $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                        return $this->redirectToRoute('salidaConsultorio_index'); 
                                    }
                                }
                            }
                        }else{
                            if ($salidaConsultorio->getCantidad() == 0) {
                                    $this->addFlash('error', 'No se puede actualizar la salida a una cantidad igual a cero.');
                                    return $this->redirectToRoute('salidaConsultorio_index'); 
                            }else{
                                if( $salidaConsultorio->getCantidad() <= $salidaConsultorio->getIdSalida()->getCantidad()) {
                                   $entityManager->persist($salidaConsultorio);
                                    $entityManager->flush();                        
                                    $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                    return $this->redirectToRoute('salidaConsultorio_index');
                                }else{
                                    $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                    return $this->redirectToRoute('salidaConsultorio_index'); 
                                }
                            }
                        }
                        
                    }
                    return $this->render('salidaConsultorio/modalEdit.html.twig', [
                        'salidaConsultorio' => $salidaConsultorio,
                        'form' => $form->createView()
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }
    }       
    /**
     * Metodo utilizado para la eliminación de una salida de consultorios.
     *
     * @Route("/{idSalidaConsultorio}", name="salidaConsultorio_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SalidaConsultorio $salidaConsultorio,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'Salidas de pedido consultorio');
              if (sizeof($permisos) > 0) {
                //var_dump($estadoMantenimiento->getIdEstadoMantenimiento());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($salidaConsultorio);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('salidaConsultorio_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('salidaConsultorio_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
               return $this->redirectToRoute('salidaConsultorio_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }
}
