<?php

namespace App\Controller;
use App\Entity\Clinica;
use App\Entity\Usuario;
use App\Entity\RecuperacionCuenta;
use App\Form\UsuarioType;
use App\Form\ClinicaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/*
* Controlador de seguridad utilizado para el manejo del ingreso al sistema y las primeras redirecciones de las vistas segun el usuario que ingrese al sistema. Ademas 
* de permitir la actualización de los intentos del mismo al sistema. Además del primer ingreso al sistema y las recuperación de las credenciales por un usuario.
*
*/
class SecurityController extends AbstractController
{
    /**
     * Metodo utilizado para el ingreso al sistema manejando las condiciones de seguridad que ofrece symfony para el manejo de autenticacion. 
     * Igualmente en la verificación de logueo se verifica la veces que tengan intentos erroneos advirtiendo que lleven 3 intentos donde se visualiza 
     * un mensaje que con el proximo intento erroneo el usuario quedara inactivado, y en caso que el usuario no acote las indicaciones y pase a dicho * intento este 
     * quedara inactivo y se reportara al administradopr del sistema y a al mismo usuario de como poder activarse nuevamente.
     *
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils,Request $request, \Swift_Mailer $mailer)
    {
      $roleNeto=$this->get('security.token_storage')->getToken()->getUser();
        if($roleNeto == 'anon.'){ 
          $session = $request->getSession();
          $session->start(); 
          $error = $authenticationUtils->getLastAuthenticationError();
          $lastUsername = $authenticationUtils->getLastUsername();
          if ($lastUsername != null) {
                     
            if ( $error != null ) 
            { 
              $em = $this->getDoctrine()->getManager();
              $dqlUsuario = "SELECT usuario FROM App:Usuario usuario where usuario.username= :username";
              $queryUsuario = $em->createQuery($dqlUsuario)->setParameter('username',strtoupper($lastUsername));
              $usuarios = $queryUsuario->getResult();
              foreach ($usuarios as $usuarioSeleccionado ){
                $idUsuario=$usuarioSeleccionado->getIdUsuario();
                $intentos=$usuarioSeleccionado->getIntento();
                $nombres=$usuarioSeleccionado->getNombres().' '.$usuarioSeleccionado->getApellidos();
                $correo=$usuarioSeleccionado->getEmail();
              } 
              if (isset($intentos)) {                  
                if (($intentos+1) === 3 ) {
                $this->addFlash('error', 'Ha superado el limite de intentos permitidos. En el siguiente intento errado el sistema bloqueara el acceso.!');
                }elseif (($intentos+1) >= 4) {
                  $this->addFlash('error', 'El acceso al sistema ha sido bloqueado contactece con el administrador del sistema.!');
                }

                $intentos=$intentos+1;
                if ($intentos == 4) {
                  $dqlModificarUsuario="UPDATE App:Usuario usuario SET usuario.intento =:intento,usuario.estado =:estado WHERE usuario.idUsuario=:id";
                  $queryModificarUsuario=$em->createQuery($dqlModificarUsuario)->setParameter('intento',$intentos)->setParameter('estado',false)->setParameter('id',$idUsuario);
                  $modificarUsuario=$queryModificarUsuario->execute();

                  $dqlUsuarios = "SELECT usuario FROM App:Usuario usuario where usuario.idPerfil= :idPerfil";
                  $queryUsuarios = $em->createQuery($dqlUsuarios)->setParameter('idPerfil',1);
                  $usuarioSuperAdmin = $queryUsuarios->getResult();
                  $arrayEmail=array();
                  foreach ($usuarioSuperAdmin as $usuarioSeleccionadoAdm ){
                    $email=$usuarioSeleccionadoAdm->getEmail();
                    array_push($arrayEmail,$email);
                  }  
                  $para = $arrayEmail;
                  //(new \Swift_Message('Hello Email'))
                    $message = (new \Swift_Message())
                      ->setSubject('Cuenta bloqueada sistema clinico.')
                      ->setFrom('anamileidy.rodriguez@upt.edu.co')
                      ->setTo($para)  
                      ->setBody('<html>
                          <head>
                            <style>
                              p{
                                 font-family: "Cerebri Sans,sans-serif";
                                 font-size:15px;
                                 font-style:normal;
                                 color: #6c757d;
                              }
                              b{
                                color:black;
                                font-size:15px;
                                font-style:normal;
                              }
                              img{
                                position: absolute; 
                                align-self: center; 
                                width: 3.5in; 
                                height:1.5in;
                              }
                            </style>
                          </head>
                            <body>                                
                              <p>Buen día</p><br>
                              <p> El sistema a dectado que el usuario <b>'.$nombres.'</b> a excedido el número máximo de intentos permitidos. <b>El usuario ha sido bloqueado.</b></p>
                              <p>Confirme con el usuario dicho ingreso y si considera proporcione nuevamente los permisos para acceder al sistema.</p>
                              <p> Cordialmente,</p><br>       

                              <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                                                  
                              <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br>                                           
                              </body>
                            </html>', 'text/html') ;                
                          try {
                              $mailer->send($message);  
                          } catch (\Swift_TransportException  $e) {
                              
                               $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                          }  

                  //(new \Swift_Message('Hello Email'))
                    $message = (new \Swift_Message())
                      ->setSubject('Cuenta bloqueada sistema clinico.')
                      ->setFrom('anamileidy.rodriguez@upt.edu.co')
                      ->setTo($correo)  
                      ->setBody('<html>
                          <head>
                            <style>
                              p{
                                 font-family: "Cerebri Sans,sans-serif";
                                 font-size:15px;
                                 font-style:normal;
                                 color: #6c757d;
                              }
                              b{
                                color:black;
                                font-size:15px;
                                font-style:normal;
                              }
                              img{
                                position: absolute; 
                                align-self: center; 
                                width: 3.5in; 
                                height:1.5in;
                              }
                            </style>
                          </head>
                            <body>                                
                              <p>Buen día</p><br>    
                              <p> Señor(a) <b>'.$nombres.'</b> ha excedido el número máximo de intentos permitidos para el acceso al sistema clinico. Su usuario ha sido bloqueado.</p>
                              <p> Para más información contactece con el administrador del sistema.</p>
                              <p> Cordialmente,</p><br>       

                              <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                                                  
                              <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br>                                           
                              </body>
                            </html>', 'text/html') ;                
                    try {
                        $mailer->send($message);  
                    } catch (\Swift_TransportException  $e) {
                        
                         $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                    }      
                }else{
                  $dqlModificarUsuario="UPDATE App:Usuario usuario SET usuario.intento =:intento WHERE usuario.idUsuario=:id";
                  $queryModificarUsuario=$em->createQuery($dqlModificarUsuario)->setParameter('intento',$intentos)->setParameter('id',$idUsuario);
                  $modificarUsuario=$queryModificarUsuario->execute();
                }
              }
            }
          }
        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
          ]);        
         
      }else{
        $this->addFlash('error', "No cuenta con autorización para acceder a este modulo.");
            return $this->redirectToRoute('panel_principal');
      }
    }

    /**
     * Metodo utilizado para la redirección del primer ingreso al sistema o de los ingresos generales del sistema donde se devuelven los intentos de 
     * los usuario a cero por el ingreso efectivo del sistema. Además se verifica que el usuario que ingreso no cuente con sesiones abiertas y si es * el caso lo 
     * desloguea y le informa que la sesión se encuentra abierta y que el tiempo de sesionamiento no supera los treinta minutos 
     * establecidos para la sessión sea cerrada inmediatamente. 
     *
     * @Route("/", name="login_check")
     */
    public function loginCheck(Request $request)
    {
      $em = $this->getDoctrine()->getManager(); 
      $sessionCodigo = $request->getSession()->getId();
      $roleNeto=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil()->getNombre();

      $usuarioID=$this->get('security.token_storage')->getToken()->getUser()->getIdUsuario();
      if($roleNeto == 'anon.'){
          return $this->redirectToRoute('usuario_logout'); 
      }else{
           
          $dqlModificarUsuario="UPDATE App:Usuario usuario SET usuario.intento =:intento WHERE usuario.idUsuario=:id";
          $queryModificarUsuario=$em->createQuery($dqlModificarUsuario)->setParameter('intento',0)->setParameter('id',$usuarioID);
          $modificarUsuario=$queryModificarUsuario->execute();
        
          $arrayUserName= array();
          $arrayTiempoID= array();
          $sessionUserName=strtoupper($this->get('security.token_storage')->getToken()->getUser()->getUsername());
          var_dump($sessionUserName);
          $sqlCliente="SELECT session FROM App:Sessions session where session.sessId != :codigoSession";
          $consulta = $em->createQuery($sqlCliente)->setParameter('codigoSession',$sessionCodigo);
          $consultas = $consulta->getResult();
          foreach ($consultas as $datos) {
              $datoTiempoDuracion= $datos->getSessTime();
              $datosSessionId=$datos->getSessId();
              $fechaTiempoDuracion= date('Y-m-d H:i:s', $datoTiempoDuracion);
              array_push($arrayTiempoID, array($datosSessionId,$fechaTiempoDuracion ));
          }
          $fechaActual=date('Y-m-d H:i:s');
          if (!empty($arrayTiempoID)) {
              foreach ($arrayTiempoID as $dato) {
                  $AFecha=strtotime('+30 minute', strtotime($dato[1]));
                  $AFecha=date('Y-m-d H:i:s',$AFecha);  
                  $fechaTiempoDuracionGuardado = strtotime($AFecha);
                  $fechaActualF = strtotime($fechaActual);
                  if ($fechaTiempoDuracionGuardado < $fechaActualF) {
                       //Modificacion de ingreso de sessionamiento
                      $dqlModificarSessions="UPDATE App:Sessions session SET session.sessUserNameCliente =:nameCliente WHERE session.sessId=:sessionId";
                      $queryModificacionSession=$em->createQuery($dqlModificarSessions)->setParameter('nameCliente','insert')->setParameter('sessionId',$dato[0]);
                      $modificacionSession=$queryModificacionSession->execute();

                  }
              }
          }
          
          $sqlClienteActual="SELECT session FROM App:Sessions session where session.sessId != :codigoSession";
          $consultaActualizada = $em->createQuery($sqlClienteActual)->setParameter('codigoSession',$sessionCodigo);
          $consultasActualizadas = $consultaActualizada->getResult();
          foreach ($consultasActualizadas as $datoActualizado) {
              $datosUserName=$datoActualizado->getSessUserNameCliente();
              array_push($arrayUserName,$datosUserName); 
          }
          $buscar=in_array($sessionUserName,$arrayUserName);
          if ($buscar == true){
             // 
              $successMessage='Usted tiene abierta una sesión en otro dispositivo, por favor cierrela si desea ingresar desde este dispositivo.';
              $this->addFlash('error',$successMessage);
             return $this->redirectToRoute('usuario_logout');
             
          }else{      
            $dqlModificarNombreCliente2="UPDATE App:Sessions session SET session.sessUserNameCliente = :nombreCliente WHERE session.sessId = :sessionId";
            $queryModificacionNombreCliente2=$em->createQuery($dqlModificarNombreCliente2)->setParameter('nombreCliente',$sessionUserName)->setParameter('sessionId',$sessionCodigo);
            $modificacionClienteNombre2=$queryModificacionNombreCliente2->execute();
            return $this->redirectToRoute('panel_principal');
          }
      }  
    }
    /**
    * Metodo utilizado para el cierre inmediato de symfony.
    *
    * @Route("/logout",name="usuario_logout")
    */
     public function logout()
    {}


    /**
     * Metodo utilizado para el registro inicial de la clinica que desea utilizar el sistema. Donde se opta a realizar un registro inicial de un 
     * usuario y a su vez indica que dicho usuario pertenece a la clinica que se esta generando, donde este usuario se le enviara un correo 
     * electronico al correo asignado de la clinica. Luego de este proceso el correo indica que se debe activar el usuario a traves de un link dada  
     * al mismo. 
     *
     * @Route("/registro", name="app_registro")
     */
    public function registro(Request $request,UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        $roleNeto=$this->get('security.token_storage')->getToken()->getUser();

        
        if ($roleNeto == 'anon.') {
          $dqlClinica = "SELECT clinica FROM App:Clinica clinica";
          $queryClinica = $em->createQuery($dqlClinica);
          $clinicas = $queryClinica->getResult(); 
          foreach ($clinicas as $clinicaSeleccionada ) {   
           $clinicaNombre= $clinicaSeleccionada->getNombre();  
          }

           $dqlUsuario = "SELECT usuario FROM App:Usuario usuario where usuario.idPerfil= :idPerfil";
          $queryUsuario = $em->createQuery($dqlUsuario)->setParameter('idPerfil',1);
          $usuarios = $queryUsuario->getResult(); 
          foreach ($usuarios as $usuarioSeleccinado ) {   
           $perfilUsuario= $usuarioSeleccinado->getIdPerfil();  
          }
          if (isset($clinicaNombre) or isset($perfilUsuario)  ) {
            $this->addFlash('error', 'El registro ya existe.');
            return $this->redirectToRoute('app_login'); 
          }else{
              $clinica =new Clinica();
              $usuario = new Usuario();
              $dqlPerfil = "SELECT perfil FROM App:Perfil perfil where perfil.idPerfil= :idPerfil";
              $queryPerfil = $em->createQuery($dqlPerfil)->setParameter('idPerfil',1);
              $perfiles = $queryPerfil->getResult(); 
              foreach ($perfiles as $perfilSelecciado ) {   
               $usuario->getIdPerfil($usuario->setIdPerfil($perfilSelecciado));
              }

              $dqlTipoDc = "SELECT tipoDoc FROM App:TipoDocumento tipoDoc where tipoDoc.id= :id";
              $queryTipoDoc = $em->createQuery($dqlTipoDc)->setParameter('id',1);
              $tipoDocumentos = $queryTipoDoc->getResult(); 
              foreach ($tipoDocumentos as $tipoDocSeleccionado ) {   
               $usuario->getTipoentificacion($usuario->setTipoentificacion($tipoDocSeleccionado));
              }


              $dqlAgendaUsuario = "SELECT agenda FROM App:AgendaUsuario agenda where agenda.idAgendaUsuario= :id";
              $queryAgendaUsuario = $em->createQuery($dqlAgendaUsuario)->setParameter('id',1);
              $agendaUsuarios = $queryAgendaUsuario->getResult(); 
              foreach ($agendaUsuarios as $agendaUsuarioSeleccionado ) {   
               $usuario->getIdAgendaUsuario($usuario->setIdAgendaUsuario($agendaUsuarioSeleccionado));
              }

              $registroClinicaForm = $this ->createFormBuilder($clinica)
                ->add('nit',TextType::class)
                ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el campo permite mínimo 3 y máximo 150 caracteres.','data-placement'=>'right','data-html'=>'true'),))
                ->add('email',EmailType::class, array('label' => false,'attr'=>array('data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el email permite mínimo 10 y máximo 75 caracteres. El formato es: “ejemplo@mail.com”.','data-placement'=>'right','data-color'=> 'black', 'data-html'=>'true')))
                ->add('movil',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el campo permite mínimo 10 y máximo 15 caracteres.','data-placement'=>'right','data-html'=>'true'),))
                ->add('estadoSede')
             ->getForm();
            $registroClinicaForm->handleRequest($request);
            if ($registroClinicaForm->isSubmitted() && $registroClinicaForm->isValid()) {
                $clinica->getEstadoSede($clinica->setEstadoSede(true));
                $em->persist($clinica);
                $em->flush();

                $usuario->setNombres('admin');
                $usuario->setApellidos('admin');
                $usuario->setNumeroIdentificacion('admin');
                $fechaActual=date('d M Y H:i:s');
                $fechaFinal=new  \DateTime($fechaActual);
                $usuario->setFechaNacimiento($fechaFinal);
                $usuario->setEmail($clinica->getEmail());
                //Trabajo de username con email
                $divicion=$clinica->getEmail();
                $user = explode("@", $divicion);
                $usuario->setUsername($user[0]);
                /*Trato Contraseña genracin y encriptación*/
                $contraseña=$this->generarCodigo(8);
                //$encoder= new UserPasswordEncoderInterface();
                $password= $encoder->encodePassword($usuario, $contraseña);
                $usuario->setPassword($password);
                $usuario->setGenero('NO ESPECIFICA');
                $usuario->setColorAgenda('#F9F9F9');
                $usuario->setDireccion('calle 0 N° 0-0');
                $usuario->setMovil($clinica->getMovil());
                $usuario->setEstado(false);
                $usuario->setIdSucursal($clinica);                
                $usuario->setIntento(0);
                $usuario->setTipoUsuario('INTERNO');
                $em->persist($usuario);
                $em->flush();

                $username=$usuario->getUsername();

                $para = $usuario->getEmail();
                  $message = (new \Swift_Message())
                    ->setSubject('Solicitud activar cuenta sistema clinico.')
                    ->setFrom('anamileidy.rodriguez@upt.edu.co')
                    ->setTo($para)  
                    ->setBody('<html>
                        <head>
                          <style>
                            p{
                               font-family: "Cerebri Sans,sans-serif";
                               font-size:15px;
                               font-style:normal;
                               color: #6c757d;
                            }
                            b{
                              color:black;
                              font-size:15px;
                              font-style:normal;
                            }
                            img{
                              position: absolute; 
                              align-self: center; 
                              width: 3.5in; 
                              height:1.5in;
                            }
                          </style>
                        </head>
                          <body>                                
                            <p>Buen día</p><br>
                            <p> Te damos la bienvenida al sistema gestión clinico, para activar su cuenta es necesario que de clic en el siguiente enlace "<b><a href="http://localhost:8000/activarCuenta?dato='.$para.'">Activar cuenta</a> </b>".</p>
                            <p>Sus datos de acceso son:</p>
                            <p> Nombre de usuario:  "<b>'.$username.'</b>"</p>
                            <p> Contraseña: "<b>'.$contraseña.'</b>"</p>
                            <p> Si requiere mayor información por favor comuniquese con el administrador de la plataforma. </p>
                            <p> Cordialmente,</p><br>       
                            <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                            <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br> 
                            </body>
                          </html>', 'text/html') ;                
                        try {
                            $mailer->send($message);  
                        } catch (\Swift_TransportException  $e) {
                            
                             $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                        }
                  $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');         
                return $this->redirectToRoute('app_login');  

            }
           
              
          }
        }
       
        return $this->render('security/registro.html.twig', [
           'formClinica' => $registroClinicaForm->createView(),
            'clinica'    => $clinica, 
        ]);
    }

    /**
     * Metodo utilizado para la activación de cuentas en el sistema. 
     *
     * @Route("/activarCuenta", name="app_verificacion", methods={"GET","POST"})
     */
     public function activacionCuenta(Request $request)
    {   
        $roleNeto=$this->get('security.token_storage')->getToken()->getUser();
        if($roleNeto == 'anon.'){
            $em = $this->getDoctrine()->getManager();  
            $dato = trim(strip_tags($request->get('dato'))); 
           
            if ($dato != "") {
                $dqlUsuario = "SELECT usuario FROM App:Usuario usuario  WHERE usuario.email =:email";
                $queryUsuario = $em->createQuery($dqlUsuario)->setParameter('email',$dato)->setMaxResults(1);
                 $usuarios = $queryUsuario->getResult();

                if (empty($usuarios)){

                    $this->addFlash('error', "El correo de verificación  ingresado '".$dato."'. No se encuentra registrado en nuestro sistema. Es necesario que se contacte con el administrador de la plataforma.");
                     return $this->redirectToRoute('app_login');

                 }else{
                    foreach ($usuarios as $usuarioSeleccionado) {
                      $usuarioID=$usuarioSeleccionado->getIdUsuario();
                    }

                    $dqlModificarUsuario="UPDATE App:Usuario usuario SET usuario.estado =:estado WHERE usuario.idUsuario=:id";
                    $queryModificarUsuario=$em->createQuery($dqlModificarUsuario)->setParameter('estado',TRUE)->setParameter('id',$usuarioID);
                    $modificarUsuario=$queryModificarUsuario->execute();
                    $this->addFlash('mensaje', 'Se activado correctamente su cuenta, puede acceder con los datos enviados al correo electrónico.!');         
                    return $this->redirectToRoute('app_login');  
                    
                 }
            }  
        }else{
             $this->addFlash('error', "No cuenta con autorización para acceder a este modulo.");
            return $this->redirectToRoute('panel_principal');
        }         
    }

    /**
    * Metodo utilizado para la generación de codigos para la recuperación de contraseñas del sistema y para la contraseña inicial del primer usuario 
    * del sistema. 
    *
    */
    function generarCodigo($longitud) {
       $key = '';
       $pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNQRSTUVWXYZ';
       $max = strlen($pattern)-1;
       for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
       return $key;
    } 

    /**
     * Metodo utilizado para la recuperación de contraseña de un usuario, donde se verifica a traves del correo electronico la existencia del usuario 
     * y según el estado del usuario se envian las instrucciones para que el usuario pueda llegar a recuperar la contraseña. Además registra las 
     * recuperaciones de contraseña del usuario para luego poder verificar que el codigo que este ingresando el usuario sea correspondiente al 
     * generado por el sistema.
     *
     * @Route("/recuperarCuenta", name="app_recuperarCuenta", methods={"GET","POST"})
     */
    public function recuperarCuenta(Request $request, \Swift_Mailer $mailer)
    {  
        $roleNeto=$this->get('security.token_storage')->getToken()->getUser();
        if($roleNeto == 'anon.'){
             $em = $this->getDoctrine()->getManager();

              $emails=trim(strip_tags($request->request->get('correoVerificacion')));
            if ($emails != "") {
                  $dql = "SELECT usuario FROM App:Usuario usuario  WHERE usuario.email =:emails";
                $queryemails = $em->createQuery($dql)->setParameter('emails',$emails);
                 $usuarios = $queryemails->getResult();
                if (empty($usuarios)){
                    $successMessage= "El correo  ingresado '".$emails."'. No se encuentra registrado en nuestro sistema. Valide las credenciales e intente nuevamente";
                    $this->addFlash('error',$successMessage);
                    return $this->redirectToRoute('app_login');
                }else{
                  foreach ($usuarios as $usuarioSeleccionado) {
                    $username=$usuarioSeleccionado->getUsername();
                    $emailGuardado=$usuarioSeleccionado->getEmail(); 
                    $rolUsuario= $usuarioSeleccionado->getIdPerfil()->getNombre(); 
                    $codigoUsuario=$usuarioSeleccionado->getIdUsuario();
                    $nombres=$usuarioSeleccionado->getNombres().' '.$usuarioSeleccionado->getApellidos();
                    $estado=$usuarioSeleccionado->getEstado();
                  }
                  if ($estado == true) {
                    //Generando codigo de verificacion
                   $codigo=$this->generarCodigo(5);
                   $recuperacion = new RecuperacionCuenta(); 
                   $recuperacion->setUsername($username);
                   $recuperacion->setEmail($emailGuardado);
                   $recuperacion->setCodigoVerificacion($codigo);
                   $em->persist($recuperacion);
                   $em->flush();
                   $para = array($emailGuardado);
                   $message = (new \Swift_Message())
                    ->setSubject('Recuperación contraseña.')
                    ->setFrom('anamileidy.rodriguez@upt.edu.co')
                    ->setTo($para)  
                    ->setBody('<html>
                        <head>
                          <style>
                            p{
                               font-family: "Cerebri Sans,sans-serif";
                               font-size:15px;
                               font-style:normal;
                               color: #6c757d;
                            }
                            b{
                              color:black;
                              font-size:15px;
                              font-style:normal;
                            }
                            img{
                              position: absolute; 
                              align-self: center; 
                              width: 3.5in; 
                              height:1.5in;
                            }
                          </style>
                        </head>
                          <body>                                
                            <p>Buen día</p><br>
                            <p> Señor(a) <b>'.$nombres.'</b></p><br>
                            <p>Para recuperar los datos de acceso al sistema debe dar clic en el siguiente enlace <a href="http://localhost:8000/verificacionCodigo?dato='.$emailGuardado.'">verificación</a> y registrar el siguiente código:"<b>'.$codigo.'</b>."</p>
                            <p>Tenga en cuenta que el código caducará en 12 horas, transcurrido este tiempo tendrá que volver a realizar el proceso.</p> 
                            <p> Si usted no ha efectuado esta solicitud omita el correo. </p>
                            <p> Cordialmente,</p><br>  
                            <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>    
                            <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br>                                           
                            </body>
                          </html>', 'text/html') ;                
                      try {
                          $mailer->send($message);  
                      } catch (\Swift_TransportException  $e) {
                          
                           $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                      }
                    $this->addFlash('mensaje', 'Se ha enviado un correo electrónico con las instrucciones de recuperación valide su cuenta.');
                    return $this->redirectToRoute('app_login');
                  }else{
                    $this->addFlash('error', 'El usuario se encuentra inactivo, no podemos darle trazabilidad a esta solicitud contactese con el administrador de la plataforma.');
                    return $this->redirectToRoute('app_login');
                  }
                
                        
                }
            }   
         }else{
           $this->addFlash('error', "No cuenta con autorización para acceder a este modulo.");
            return $this->redirectToRoute('panel_principal');
         }  
       return $this->render('security/envioCorreoVerificacion.html.twig');     
          
    }

    /**
     * Metodo utilizado para la verificación del codigo que se envia a traves de un correo electronico al usuario, donde se verifica que este codigo 
     * no halla superado 12 horas de generación y si es asi se evidencia un mensaje el cual indica que no tiene un codigo que no supere las 12 horas, 
     * en caso que tenga mas de un codigo se valida con el ultimo y si este ingreso no corresponde al ultimo se le indica  al usuario igualmente si 
     * el codigo es correctose redirecciona a una pantalla que permite el ingreso de la nueva contraseña.
     *
     *@Route("/verificacionCodigo", name="app_verificacionCodigo", methods={"GET","POST"})
     */
    public function verificacionCodigo(Request $request, \Swift_Mailer $mailer)
    { 
      $roleNeto=$this->get('security.token_storage')->getToken()->getUser();
        if($roleNeto == 'anon.'){
            $em = $this->getDoctrine()->getManager();  
            $emails = trim(strip_tags($request->get('dato'))); 
            $codigoVerificacion = trim(strip_tags($request->get('codigo'))); 
            if ($emails != ""  && $codigoVerificacion != "" ) {
                 $fechaActual = date("Y-m-d H:i:s"); 
                 $date_now = date('Y-m-d H:i:s');
                 $date_past = strtotime('-12 hours', strtotime($date_now));
                 $date_past = date('Y-m-d H:i:s', $date_past);

                $dqlRecuperacion = "SELECT recuperacion FROM App:RecuperacionCuenta recuperacion  WHERE recuperacion.email =:emails and recuperacion.createdAt >= :fechaanterior  and recuperacion.createdAt <= :fechaActual ORDER BY recuperacion.createdAt DESC";
                $queryrecuperacion = $em->createQuery($dqlRecuperacion)->setParameter('emails',$emails)->setParameter('fechaActual',$fechaActual)->setParameter('fechaanterior',$date_past)->setMaxResults(1);
                 $recuperacionCuentas = $queryrecuperacion->getResult();

                if (empty($recuperacionCuentas)){

                    $this->addFlash('error', "Disculpe usted no tiene códigos solicitas en las ultimas 12 horas");
                      return $this->redirectToRoute('app_login');

                 }else{
                    foreach ($recuperacionCuentas as $recuperacionCuenta) {
                      $codigoGuardado=$recuperacionCuenta->getCodigoVerificacion();
                    }
                    if ($codigoGuardado == $codigoVerificacion) {
                       $successMessage= 'Por favor ingrese su nueva contraseña.';
                        $this->addFlash('mensaje',$successMessage);
                        
                        return $this->redirectToRoute('app_cambioClave', array('codigo' => $codigoVerificacion));
                    }else{
                        $successMessage= 'El código no corresponde a la ultima solicitud.';
                        $this->addFlash('error',$successMessage);
                        
                        return $this->redirectToRoute('app_verificacionCodigo', array('dato' => $emails));
                    }
                 }
            }  
        }else{
             $this->addFlash('error', "No cuenta con autorización para acceder a este modulo.");
            return $this->redirectToRoute('panel_principal');
        }
          return $this->render('security/codigoVerificacion.html.twig',[
            'dato' => $emails
          ]);    

    }
    /**
     * Metodo utilizado para el cambio de clave de un usuario.
     *
     *@Route("/cambioClave", name="app_cambioClave", methods={"GET","POST"})
     */
    public function cambioClaveAction(Request $request,UserPasswordEncoderInterface $encoder){
        $roleNeto=$this->get('security.token_storage')->getToken()->getUser();
        if($roleNeto == 'anon.'){
            $em = $this->getDoctrine()->getManager();
            $codigo = trim(strip_tags($request->get('codigo'))); 
            $contra = trim(strip_tags($request->get('clave')));
            if ($codigo != "" && $contra != "") {
                $fechaActual = date("Y-m-d H:i:s"); 
                $date_now = date('Y-m-d H:i:s');
                $date_past = strtotime('-12 hours', strtotime($date_now));
                $date_past = date('Y-m-d H:i:s', $date_past);
                $dqlRecuperacion = "SELECT recuperacion FROM App:RecuperacionCuenta recuperacion  WHERE recuperacion.createdAt <=:fechaActual and recuperacion.codigoverificacion =:codigoverificacion  and recuperacion.createdAt >= :fechaanterior";
                $queryrecuperacion = $em->createQuery($dqlRecuperacion)->setParameter('fechaActual',$fechaActual)->setParameter('fechaanterior',$date_past)->setParameter('codigoverificacion',$codigo);
                $recuperacionCuentas = $queryrecuperacion->getResult();
                foreach ($recuperacionCuentas as $recu ) {   
                    $emailrecuperacion= $recu->getEmail();  
                }
                
                $dql = "SELECT usuario FROM App:Usuario usuario  WHERE usuario.email =:emails";
                $queryemails = $em->createQuery($dql)->setParameter('emails',$emailrecuperacion);
                $usuarios = $queryemails->getResult();  
                foreach ($usuarios as $usuario ) {  
                   $password= $encoder->encodePassword($usuario, $contra);
                   $usuario->setPassword($password);
                   $em->persist($usuario);
                   $em->flush($usuario);
                } 
                $this->addFlash('mensaje', 'Se ha realizado la recuperación de contraseña con exito.');
                return $this->redirectToRoute('app_login');
            }
        }else{
         $this->addFlash('error', "No cuenta con autorización para acceder a este modulo.");
        return $this->redirectToRoute('panel_principal');

        }
        return $this->render('security/cambioContrasenia.html.twig',[
          'codigo'=>$codigo
        ]); 
    }

     /**
     * Metodo usado para  mostrar un mensaje segun tiempo transcurrido de 28 minutos o desloguear al usuario si es autorizado automaticamente.
     *
     * @Route("/setear", name="app_setear", methods={"GET","POST"})
     */      
    public function setear(Request $request){
     
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
        return $this->redirectToRoute('usuario_logout'); 
      }else{
         $response = new JsonResponse();
         if (!empty($request->get('seteo'))) {
          $seteo=$request->get('seteo');       
           if ($seteo == "cero") {
              $_SESSION['time']=strtotime(date("Y-m-d H:i:s"));//time();
              $tiempo = $_SESSION['time']; 
              session_reset(); 
             $response->setData("Se ha seteado correctamente la session");
             return $response;
           }else if ($seteo == "uno") {

              $session=$request->getSession();
              $session->invalidate();
               $this->addFlash('error', "El campo correo  ya se encuentra registrado.");
              return $this->redirectToRoute('usuario_logout');  
                  
           }
        }
      }   
     
    }
}