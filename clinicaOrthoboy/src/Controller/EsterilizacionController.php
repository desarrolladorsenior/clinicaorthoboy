<?php

namespace App\Controller;
use App\Entity\PaqueteEsterilizacion;
use App\Entity\Esterilizacion;

use App\Form\EsterilizacionType;
use App\Form\PaqueteEsterilizacionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Controlador de esterilización utilizado para el manejo de las esterilizaciones generadas a los elementos de la clinica qie lo requieran.
 *
 * @Route("/esterilizacion")
 */
class EsterilizacionController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de las esterilizaciones y paquetes, además del registro de esterilizaciones.
     *
     * @Route("/", name="esterilizacion_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'esterilización principal');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlEsterilizacion ="SELECT esterilizacion FROM App:Esterilizacion esterilizacion
                    ORDER BY esterilizacion.idEsterilizacion DESC";  
                    $queryEsterilizacion = $entityManager->createQuery($dqlEsterilizacion);
                    $esterilizaciones= $queryEsterilizacion->getResult();
                   

                    $dqlPaqueteEsterilizacion ="SELECT paqueteEsterilizacion FROM App:PaqueteEsterilizacion paqueteEsterilizacion ORDER BY paqueteEsterilizacion.idPaqueteEsterilizacion DESC";  
                    $queryPaqueteEsterilizacion = $entityManager->createQuery($dqlPaqueteEsterilizacion);
                    $paqueteEsterilizaciones= $queryPaqueteEsterilizacion->getResult();

                    $esterilizacion = new Esterilizacion();
                    $form = $this->createForm(EsterilizacionType::class, $esterilizacion);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $esterilizacion->getEstado($esterilizacion->setEstado(true));    
                        $entityManager->persist($esterilizacion);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('esterilizacion_nuevoPaquetEsterilizacion', [
                            'idEsterilizacion' => $esterilizacion->getIdEsterilizacion(),
                        ]);
                    }
                    return $this->render('esterilizacion/index.html.twig', [
                        'esterilizaciones' => $esterilizaciones,
                        'esterilizacion' => $esterilizacion,
                        'form' => $form->createView(),
                        'paqueteEsterilizaciones'=>$paqueteEsterilizaciones
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para el registro de paquetes, hasta cumplir con las cantidades ingresadas en la esterilización y listado de los paquetes. 
     *
     * @Route("/{idEsterilizacion}/paqueteEsterilizacion", name="esterilizacion_nuevoPaquetEsterilizacion", methods={"GET","POST"})
     */
    public function AgregarPaquetesEsterilizacion(Esterilizacion $esterilizacion,Request $request,PerfilGenerator $permisoPerfil): Response
    {   
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'esterilización principal');  
                if (sizeof($permisos) > 0) {
                    $response= new JsonResponse();
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlPaqueteEsterilizacion ="SELECT paqueteEsterilizacion FROM App:PaqueteEsterilizacion paqueteEsterilizacion WHERE paqueteEsterilizacion.idEsterilizacion= :idEsterilizacion ORDER BY paqueteEsterilizacion.idPaqueteEsterilizacion DESC";  
                    $queryPaqueteEsterilizacion = $entityManager->createQuery($dqlPaqueteEsterilizacion)->setParameter('idEsterilizacion',$esterilizacion->getIdEsterilizacion());
                    $paqueteEsterilizaciones= $queryPaqueteEsterilizacion->getResult();

                    $paqueteEsterilizacion=new PaqueteEsterilizacion();
                    $paqueteEsterilizacion->setIdEsterilizacion($esterilizacion);
                    $paqueteEsterilizacion->setEstado(true);
                    $form = $this->createForm(PaqueteEsterilizacionType::class, $paqueteEsterilizacion);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $cantidadPaquetesActual=$entityManager->createQueryBuilder();
                        $cantidadPaquetesActual->select('count(paquete)')
                            ->from('App:PaqueteEsterilizacion', 'paquete')
                            ->where('paquete.idEsterilizacion = :idEsterilizacion');
                        $queryCanPaquetes=$entityManager->createQuery($cantidadPaquetesActual)->setParameter('idEsterilizacion',$esterilizacion->getIdEsterilizacion());
                        $cantidadPaquetes= $queryCanPaquetes->getResult();
                        foreach ($cantidadPaquetes as $key => $value) {
                             foreach ($value as $cantElemento ) {
                                $numPaquetesActuales=$cantElemento;
                            }                
                        }
                        if ($numPaquetesActuales < $esterilizacion->getCantidadPaquete() ) {
                        
                            $entityManager->persist($paqueteEsterilizacion);
                            $entityManager->flush();       
                            $dqlPaqueteEsterilizacion ="SELECT paqueteEsterilizacion FROM App:PaqueteEsterilizacion paqueteEsterilizacion WHERE paqueteEsterilizacion.idEsterilizacion= :idEsterilizacion ORDER BY paqueteEsterilizacion.idPaqueteEsterilizacion DESC";  
                            $queryPaqueteEsterilizacion = $entityManager->createQuery($dqlPaqueteEsterilizacion)->setParameter('idEsterilizacion',$esterilizacion->getIdEsterilizacion());
                            $paqueteEsterilizacionesDos= $queryPaqueteEsterilizacion->getResult();
                            $template = $this->render('esterilizacion/datos.html.twig', [
                            'paquetes'=>$paqueteEsterilizacionesDos
                            ])->getContent();
                            return new JsonResponse(['mensaje'=> 'Se ha realizado el registro de forma correcta!','paquetes'=>$template,'error'=>'vacio']); 
                        }else{
                            return new JsonResponse(['error'=>'No se pueden registrar más paquetes, cumplio el limite establecido en la esterilización. Cantidad de Paquetes maximos para la esterilización '.$esterilizacion->getCantidadPaquete().'.']); 
                        } 
                    }
                    return $this->render('esterilizacion/new_sterile_package.html.twig', [
                        'esterilizacion' => $esterilizacion,
                        'paqueteEsterilizacion'=>$paqueteEsterilizacion,
                        'form'=>$form->createView(),
                        'paquetes'=>$paqueteEsterilizaciones
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }


     /**
     * Metodo utilizado para la edición del estado de la esterilización.
     *
     * @Route("/editarEstadoEsterilizacion", name="esterilizacion_editestado", methods={"GET","POST"})
     */
    public function editEstado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'esterilización principal');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $id=$request->request->get('id');
                    $estado=$request->request->get('estado');
                    if($estado == 'ACTIVO'){
                        $estado = true;
                    }else{
                        $estado = false;
                    }

                    /*$dqlEvolucionEsterilizacion ="SELECT count(evolucion) FROM App:EvolucionPaqueteEsterilizacion evolucion INNER JOIN App:PaqueteEsterilizacion paquete with paquete.idPaqueteEsterilizacion=evolucion.idPaqueteEsterilizacion WHERE paquete.idEsterilizacion = :idEsterilizacion";
                    $queryEvoluciones= $em->createQuery($dqlEvolucionEsterilizacion)->setParameter('idEsterilizacion',$id);
                    $evoluciones= $queryEvoluciones->getResult();  
                    foreach ($evoluciones as $key => $value) {
                             foreach ($value as $cantElemento ) {
                                $numEvoluciones=$cantElemento;
                            }                
                        }
                    if ($numEvoluciones == 0) {*/
                        $dqlModificarEstado="UPDATE App:Esterilizacion esterilizacion SET esterilizacion.estado= :estado WHERE esterilizacion.idEsterilizacion= :idEsterilizacion";
                        $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idEsterilizacion', $id);
                        $modificarEstado=$queryModificarEstado->execute();

                        $dqlModificarEstadoPaqueteEsterilizacion="UPDATE App:PaqueteEsterilizacion pEsterilizacion SET pEsterilizacion.estado= :estado WHERE pEsterilizacion.idEsterilizacion= :idEsterilizacion";
                        $queryModificarEstadoPaqueteEsterilizacion=$em->createQuery($dqlModificarEstadoPaqueteEsterilizacion)->setParameter('estado', $estado)->setParameter('idEsterilizacion', $id);
                        $modificarEstadoPaqueteEsterilizacion=$queryModificarEstadoPaqueteEsterilizacion->execute();
                        $response->setData('Se ha cambiado el estado de forma correcta!');
                        return $response;
                   /* }else{
                        $response->setData('La esterilización tiene evoluciones de pacientes asignadas no se puede realizar el cambio de estado de la esterilización.');
                        return $response;
                    }*/
                             
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la visualiza de una esterilización y sus paquetes.
     *
     * @Route("/{idEsterilizacion}", name="esterilizacion_show", methods={"GET","POST"})
     */
    public function show(Esterilizacion $esterilizacion,PerfilGenerator $permisoPerfil,Request $request): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'esterilización principal');  
                if (sizeof($permisos) > 0) {
                    return $this->render('esterilizacion/modalShow.html.twig', [
                        'esterilizacion' => $esterilizacion,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la edición de una esterilización.
     *
     * @Route("/{idEsterilizacion}/edit", name="esterilizacion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Esterilizacion $esterilizacion,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'esterilización principal');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    
                    $form = $this->createForm(EsterilizacionType::class, $esterilizacion);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $cantidadPaquetesActual=$entityManager->createQueryBuilder();
                        $cantidadPaquetesActual->select('count(paquete)')
                            ->from('App:PaqueteEsterilizacion', 'paquete')
                            ->where('paquete.idEsterilizacion = :idEsterilizacion');
                        $queryCanPaquetes=$entityManager->createQuery($cantidadPaquetesActual)->setParameter('idEsterilizacion',$esterilizacion->getIdEsterilizacion());
                        $cantidadPaquetes= $queryCanPaquetes->getResult();
                        foreach ($cantidadPaquetes as $key => $value) {
                             foreach ($value as $cantElemento ) {
                                $numPaquetesActuales=$cantElemento;
                            }                
                        }
                        if ($numPaquetesActuales < $esterilizacion->getCantidadPaquete() or $numPaquetesActuales == $esterilizacion->getCantidadPaquete() ) {   
                        //Selección de evolución paquete que pertenezcan a paquetes y pertenecen a esta evolución 

                            $dqlEvolucionPaquetes ="SELECT evolucionPaquete FROM App:EvolucionPaqueteEsterilizacion evolucionPaquete INNER JOIN App:PaqueteEsterilizacion paquete with paquete.idPaqueteEsterilizacion=evolucionPaquete.idPaqueteEsterilizacion WHERE paquete.idEsterilizacion = :idEsterilizacion";
                            $queryEvolucionesPaquetes= $entityManager->createQuery($dqlEvolucionPaquetes)->setParameter('idEsterilizacion',$esterilizacion->getIdEsterilizacion());
                            $evolucionesPaquetes= $queryEvolucionesPaquetes->getResult();
                            $arrayNumeroEvolucion= array();
                            foreach ($evolucionesPaquetes as $seleccionEvolucion) {
                              $idEvolucion= $seleccionEvolucion->getIdEvolucion();
                              array_push($arrayNumeroEvolucion,$idEvolucion);
                            } 
                            if (sizeof($evolucionesPaquetes) > 0) { //original
                                $fechaApeEsterilizacion = strtotime($esterilizacion->getFechaApertura()->format('d M Y H:i:s'));
                                $fechaVenEsterilizacion = strtotime($esterilizacion->getFechaVecimiento()->format('d M Y H:i:s'));
                                $dqlEvoluciones ="SELECT evolucion FROM App:Evolucion evolucion WHERE evolucion.idEvolucion in(:idEvolucion)";
                                $queryEvoluciones= $entityManager->createQuery($dqlEvoluciones)->setParameter('idEvolucion',$arrayNumeroEvolucion);
                                $evoluciones= $queryEvoluciones->getResult();
                                foreach ($evoluciones as $seleccionEvolucion) {
                                  $fechaAperturaEvolucion= strtotime($seleccionEvolucion->getFechaApertura()->format('d M Y H:i:s'));
                                  if ($fechaApeEsterilizacion > $fechaAperturaEvolucion or $fechaAperturaEvolucion > $fechaVenEsterilizacion) {
                                      $this->addFlash('error', 'No se ha actualizado, la fecha de inicio o vencimiento de la esterilización es menor a la fecha de apertura de la evolución número "'.$seleccionEvolucion->getIdEvolucion().'" realizada al paciente nombrado "'.$seleccionEvolucion->getIdPaciente());
                                      return $this->redirectToRoute('esterilizacion_index');
                                  }
                                } 
                                $entityManager->persist($esterilizacion);
                                $entityManager->flush();                       
                                $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                return $this->redirectToRoute('esterilizacion_index');
                            }else{
                                $entityManager->persist($esterilizacion);
                                $entityManager->flush();                       
                                $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                return $this->redirectToRoute('esterilizacion_index');
                            } 
                        }else{
                            $this->addFlash('error', 'No se pudo actualizar la cantidad de paquetes es inferior a los existentes!');
                            return $this->redirectToRoute('esterilizacion_index');
                        }
                    }

                    return $this->render('esterilizacion/modalEdit.html.twig', [
                        'esterilizacion' => $esterilizacion,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de una esterilización.
     *
     * @Route("/{idEsterilizacion}", name="esterilizacion_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Esterilizacion $esterilizacion,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'esterilización principal');  
              if (sizeof($permisos) > 0) {
                var_dump($esterilizacion->getIdEsterilizacion());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($esterilizacion);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('esterilizacion_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('esterilizacion_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('esterilizacion_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }
}
