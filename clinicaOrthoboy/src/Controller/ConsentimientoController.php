<?php

namespace App\Controller;

use App\Entity\Consentimiento;
use App\Entity\TipoConsentimiento;
use App\Entity\Paciente;
use App\Form\ConsentimientoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de consentimiento utilizado para el manejo de la confirmación o aprobación de los tratamientos que se le vallan a efectuar a un paciente el cual busca 
 * que a traves de una firma capturada en una tableta y/o pad, huellero se apruebe el tratamiento a realizar.
 *
 * @Route("/consentimiento")
 */
class ConsentimientoController extends Controller
{
  /**
     * Metodo utilizado para el ingreso de huella al consentimiento. Además o tiene  los parametros deseguridad , ni de cambio de tiempo de sesionamiento a causa que 
     * no cuenta con sesionaniento, ni cuenta con autenticación previa.
     *
     * @Route("/huella", name="consentimiento_huella", methods={"GET","POST"})
     */
    public function huellaPacienteConsentimiento(Request $request,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
          return $this->redirectToRoute('usuario_logout'); 
      } else{
          // La variable $_SESSION['usuario'] es un ejemplo. 
          //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
          if(isset($_SESSION['time'])){ 
            $tiempo = $_SESSION['time']; 
          }else{ 
            $tiempo = strtotime(date("Y-m-d H:i:s"));
          } 
          $inactividad =1800;   //Exprecion en segundos. 
          $actual =  strtotime(date("Y-m-d H:i:s")); 
          $tiempoTranscurrido=($actual-$tiempo);
          if(  $tiempoTranscurrido >= $inactividad){ 
            $session->invalidate();
            return $this->redirectToRoute('usuario_logout');
           // En caso que este sea mayor del tiempo seteado lo deslogea. 
          }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'consentimientos');  
              if (sizeof($permisos) > 0) { 
                //$idUsuario=$request->get('inf');
                $huellaActual=$request->get('huellaActual');
                $idConsentimiento=$request->get('idConsentimiento');
                //$idUsuarioForm=$request->request->get('idUsuario');
                $huellaForm=$request->request->get('huella');
                if (isset($huellaForm) && isset($idConsentimiento)) {
                 // $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                  //return $this->redirectToRoute('usuario_index');
                  $entityManager=$this->getDoctrine()->getManager();        
                  $dqlModificarConsentimiento="UPDATE App:Consentimiento consentimiento SET consentimiento.huellaPaciente= :huella WHERE consentimiento.idConsentimiento= :idConsentimiento";
                  $queryModificarConsentimiento=$entityManager->createQuery($dqlModificarConsentimiento)->setParameter('huella', $huellaForm)->setParameter('idConsentimiento', $idConsentimiento);
                  $modificarConsentimiento=$queryModificarConsentimiento->execute();
                  //echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
                  $this->addFlash('mensaje', 'Se ha realizado el registro de la huella correctamente!');
                  return $this->redirectToRoute('consentimiento_show',['idConsentimiento'=>$idConsentimiento]);
                }
                return $this->render('consentimiento/capturaHuella.html.twig', [
                    //'idUsuario' => $idUsuario,
                    'huellaActual'=>$huellaActual,
                    'dato'=>$idConsentimiento
                ]);
          }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
          }    
        }
      }      
    }

    /**
     * Metodo utilizado para el ingreso de firma al consentimiento. Además o tiene  los parametros deseguridad , ni de cambio de tiempo de sesionamiento a causa que 
     * no cuenta con sesionaniento, ni cuenta con autenticación previa.
     *
     * @Route("/firma", name="consentimiento_firma", methods={"GET","POST"})
     */
    public function firmaPacienteConsentimiento(Request $request): Response
    {
      //$idUsuario=$request->get('inf');
      $firmaActual=$request->get('firmaActual');

      $dato=$request->get('dato');
      $idConsentimiento=$request->get('idConsentimiento');
      //$idUsuarioForm=$request->request->get('idUsuario');
      $firmaForm=$request->request->get('firmaPaciente');
      if (isset($firmaForm) && isset($idConsentimiento)) {
       // $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
        //return $this->redirectToRoute('usuario_index');
        $entityManager=$this->getDoctrine()->getManager();        
        $dqlModificarConsentimiento="UPDATE App:Consentimiento consentimiento SET consentimiento.firmaPaciente= :firma WHERE consentimiento.idConsentimiento= :idConsentimiento";
        $queryModificarConsentimiento=$entityManager->createQuery($dqlModificarConsentimiento)->setParameter('firma', $firmaForm)->setParameter('idConsentimiento', $idConsentimiento);
        $modificarConsentimiento=$queryModificarConsentimiento->execute();
        echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
      }
      return $this->render('consentimiento/firmaPaciente.html.twig', [
          //'idUsuario' => $idUsuario,
          'firmaActual'=>$firmaActual,
          'dato'=>$dato
      ]);
    }
    /**
     * Metodo utilizado para obtener el contenido del tipo de consentiento 'para la visualización en el registro de un consentimiento.
     * 
     * @Route("/{idTipoConsentimiento}/contenido", name="consentimiento_contenido", methods={"GET","POST"})
    */
    public function contenidoTipoConsentimiento(TipoConsentimiento $tipoConsentimiento,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'consentimientos');  
                if (sizeof($permisos) > 0) { 
                    $response = new JsonResponse(['contenido'=>ucwords(strtolower($tipoConsentimiento->getContenido()))]);
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }
     /**
     * Metodo utilizado para la generación de un pdf, sin embargo se encuentra a la espera de los prototipos.
     *
     * @Route("/{idConsentimiento}/pdf", name="consentimiento_pdf", methods={"GET","POST"})
     */
    public function pdf(Consentimiento $consentimiento,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'consentimientos');  
                if (sizeof($permisos) > 0) { 
                     $html =$this->renderView('consentimiento/pdfConsentimiento.html.twig',
                    array(
                    'consentimiento' => $consentimiento
                    ));
                     
                    $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                    [   'encoding' => 'UTF-8',
                        'images' => false,
                        'enable-javascript' => true,
                        'print-media-type' => true,
                        'outline-depth' => 8,
                        'orientation' => 'Portrait',
                        'header-right'=>'Pag. [page] de [toPage]',
                        'page-size' => 'A4',
                        'viewport-size' => '1280x1024',
                        'margin-left' => '10mm',
                        'margin-right' => '10mm',
                        'margin-top' => '30mm',
                        'margin-bottom' => '25mm',
                        'user-style-sheet'=> 'light/dist/assets/css/bootstrap.min.css',
                        //'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
                        //'background'     => 'light/dist/assets/images/users/user-1.jpg',
                        //--javascript-delay 3000
                    ]);
                    $filename="Consentimiento_N°_".$consentimiento->getIdConsentimiento()."Paciente_".$consentimiento->getIdPaciente()."_fecha_". date("Y_m_d") .".pdf";
                    $response = new Response ($pdf,
                    200,
                    array('Content-Type' => 'aplicattion/pdf',
                          'Content-Disposition' => 'inline; filename="'.$filename.'"'
                    ,));
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }
    /**
     * Metodo utilizado para la visualización de consentimientos, registro de los mismos y exposición de datos de la clinica y edad del paciente.
     *
     * @Route("/{idPaciente}", name="consentimiento_index", methods={"GET","POST"})
     */
    public function index(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'consentimientos');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlConsentimiento ="SELECT consentimiento FROM App:Consentimiento consentimiento WHERE consentimiento.idPaciente = :idPaciente ORDER BY consentimiento.idConsentimiento DESC";  
                    $queryConsentimiento = $entityManager->createQuery($dqlConsentimiento)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $consentimientos= $queryConsentimiento->getResult();

                    $dqlClinica="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                    $queryClinica=$entityManager->createQuery($dqlClinica);
                    $clinicas=$queryClinica->getResult();

                    $idPac=$paciente->getIdPaciente();
                    $consentimiento = new Consentimiento();
                    $consentimiento->setIdPaciente($paciente);
                    $consentimiento->setIdUsuario($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(ConsentimientoType::class, $consentimiento);                   
                    $form->handleRequest($request);

                    $fechaActual=date('d M Y H:i:s');
                    $fechaFinal=new  \DateTime($fechaActual);
                    $fechaNacimientoPaciente = $paciente->getFechaNacimiento();
                    $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                    $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';

                    if ($form->isSubmitted() && $form->isValid()) {
                       
                        $entityManager->persist($consentimiento);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('consentimiento_show',['idConsentimiento'=>$consentimiento->getIdConsentimiento()]);
                    }

                    
                    return $this->render('consentimiento/index.html.twig', [
                        'consentimientos' => $consentimientos,
                        'paciente'=>$paciente,
                        'consentimiento' => $consentimiento,
                        'form' => $form->createView(),
                        'clinicas'=>$clinicas,
                        'edad'=>$edad,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    
    /**
     * Metodo utilizado para la visualización de un consentimiento.
     *
     * @Route("/{idConsentimiento}/detalle", name="consentimiento_show", methods={"GET"})
     */
    public function show(Consentimiento $consentimiento,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'consentimientos');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlClinica="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                    $queryClinica=$entityManager->createQuery($dqlClinica);
                    $clinicas=$queryClinica->getResult();


                    $fechaActual=date('d M Y H:i:s');
                    $fechaFinal=new  \DateTime($fechaActual);
                    $fechaNacimientoPaciente = $consentimiento->getIdPaciente()->getFechaNacimiento();
                    $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                    $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';

                    return $this->render('consentimiento/show.html.twig', [
                        'consentimiento' => $consentimiento,
                        'clinicas'=>$clinicas,
                        'edad'=>$edad,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * @Route("/{idConsentimiento}/edit", name="consentimiento_edit", methods={"GET","POST"})
     */
    /*public function edit(Request $request, Consentimiento $consentimiento): Response
    {
        $form = $this->createForm(ConsentimientoType::class, $consentimiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('consentimiento_index', [
                'idConsentimiento' => $consentimiento->getIdConsentimiento(),
            ]);
        }

        return $this->render('consentimiento/edit.html.twig', [
            'consentimiento' => $consentimiento,
            'form' => $form->createView(),
        ]);
    }*/

    /**
     * Metodo utilizado para la eliminación de un consentimiento, no se encuentra en funcionamiento actualmente.
     *
     * @Route("/{idConsentimiento}", name="consentimiento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Consentimiento $consentimiento): Response
    {
        if ($this->isCsrfTokenValid('delete'.$consentimiento->getIdConsentimiento(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($consentimiento);
            $entityManager->flush();
        }

        return $this->redirectToRoute('consentimiento_index');
    }
}
