<?php

namespace App\Controller;

use App\Entity\AgendaUsuario;
use App\Form\AgendaUsuarioType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de agenda usuario que se utiliza para los gestionar los horarios en los que los usuarios podran trabajar para el manejo de las citas 
 * que se les asignen, con las agenda se busca que las citas medicas se midan a traves de los horarios asignados a los profesionales.
 *
 * @Route("/agendaUsuario")
 */
class AgendaUsuarioController extends AbstractController
{
    /**
     * Visualización de agendas de usuario ingresadas en el sistema y ingreso de nuevas agendas, ademas que tiene como en todos los metodos todo el 
     * proceso de actualización del tiempo de sesionamiento.
     *
     * @Route("/", name="agendaUsuario_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'agenda de usuario');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    /*Agenda usuarios datos completos de table */
                    $dqlAgendaUsuario ="SELECT agendaUsuario FROM App:AgendaUsuario agendaUsuario
                    ORDER BY agendaUsuario.idAgendaUsuario DESC";  
                    $queryAgendaUsuario = $entityManager->createQuery($dqlAgendaUsuario);
                    $agendaUsuarios= $queryAgendaUsuario->getResult();
                    /*Inicialización de nuevo formulario*/
                    $agendaUsuario = new AgendaUsuario();
                    $agendaUsuario->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $agendaUsuario->setEstado(true);
                    $form = $this->createForm(AgendaUsuarioType::class, $agendaUsuario);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($agendaUsuario);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('agendaUsuario_index');
                    }

                    return $this->render('agendaUsuario/index.html.twig', [
                        'agendaUsuarios' => $agendaUsuarios,
                        'agendaUsuario' => $agendaUsuario,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
        
    }
    /**
     * Pruebas de coneccion de pad o table de wacom.
     *
     * @Route("/tabled", name="agendaUsuario_tabled", methods={"GET","POST"})
     */
    public function tabled(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        echo exec('cd /home/desarrollo/STU-SDK-Linux-2.15.3/sdk/Wacom-STU-SDK/Java/samples/DemoButtons ; ls;java -cp ../../jar/Linux-x86_64/wgssSTU.jar:. -Djava.library.path=../../jar/Linux-x86_64/ Nuevo');
        return $this->render('agendaUsuario/tabled.html.twig');
    }

    /**
     * Pruebas de coneccion de pad o table de wacom
     *
     * @Route("/tabledDos", name="agendaUsuario_tabledos", methods={"GET","POST"})
     */
    public function tabledDos(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        //echo exec('cd /home/desarrollo/STU-SDK-Linux-2.15.3/sdk/Wacom-STU-SDK/Java/samples/DemoButtons ; ls;java -cp ../../jar/Linux-x86_64/wgssSTU.jar:. -Djava.library.path=../../jar/Linux-x86_64/ Nuevo');
        return $this->render('agendaUsuario/tabled2.html.twig');
    }
    /**
     *    Metodo utilizado para la edición del estado de una agenda
     *
     * @Route("/editarEstadoAgendaUsuario", name="agendaUsuario_editarestado", methods={"GET","POST"})
     */
    public function editEstadoAgendaUsuario(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'agenda de usuario');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $em=$this->getDoctrine()->getManager();
                $response = new JsonResponse();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }
                $dqlModificarEstado="UPDATE App:AgendaUsuario agenda SET agenda.estado= :estado WHERE agenda.idAgendaUsuario= :idAgendaUsuario";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idAgendaUsuario', $id);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    

    }


    /**
     * Metodo utilizado para la edición de una agenda
     *
     * @Route("/{idAgendaUsuario}/edit", name="agendaUsuario_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AgendaUsuario $agendaUsuario,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'agenda de usuario');  
                if (sizeof($permisos) > 0) {
                    $form = $this->createForm(AgendaUsuarioType::class, $agendaUsuario);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('agendaUsuario_index');
                    }

                    return $this->render('agendaUsuario/modalEdit.html.twig', [
                        'agendaUsuario' => $agendaUsuario,
                        'form' => $form->createView(),
                    ]);

                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de una agenda
     *
     * @Route("/{idAgendaUsuario}", name="agendaUsuario_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AgendaUsuario $agendaUsuario,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'agenda de usuario');  
              if (sizeof($permisos) > 0) {
                try{

                    if ($this->isCsrfTokenValid('delete'.$agendaUsuario->getIdAgendaUsuario(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($agendaUsuario);
                        $entityManager->flush();
                        $successMessage= 'Se ha eliminado de forma correcta!';
                        $this->addFlash('mensaje',$successMessage);
                        return $this->redirectToRoute('agendaUsuario_index');
                    }

                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('agendaUsuario_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('agendaUsuario_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
        
    }
}
