<?php

namespace App\Controller;

use App\Entity\TipoUsuario;
use App\Form\TipoUsuarioType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de tipo usuario utilizado para el manejo del tipo que se desa de generar para el ingreso de un usuario al sistema.
 *
 * @Route("/tipoUsuario")
 */
class TipoUsuarioController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de todos los tipo usuario y registro de los mismos.
     *
     * @Route("/", name="tipoUsuario_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipos de usuario');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlTipoUsuario ="SELECT tipoUsuario FROM App:TipoUsuario tipoUsuario
                    ORDER BY tipoUsuario.idTipoUsuario DESC";  
                    $queryTipoUsuario = $entityManager->createQuery($dqlTipoUsuario);
                    $tipoUsuarios= $queryTipoUsuario->getResult();
                    /*Datos formulario*/
                    $tipoUsuario = new TipoUsuario();
                    $form = $this->createForm(TipoUsuarioType::class, $tipoUsuario);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($tipoUsuario);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('tipoUsuario_index');
                    }

                    return $this->render('tipoUsuario/index.html.twig', [
                        'tipoUsuarios' => $tipoUsuarios,
                        'tipoUsuario' => $tipoUsuario,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
        
    }
    /**
     * Metodo utilizado para la edición de un tipo usuario.
     *
     * @Route("/{idTipoUsuario}/edit", name="tipoUsuario_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TipoUsuario $tipoUsuario,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipos de usuario');  
                if (sizeof($permisos) > 0) {
                    $form = $this->createForm(TipoUsuarioType::class, $tipoUsuario);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->getDoctrine()->getManager()->flush();

                        return $this->redirectToRoute('tipoUsuario_index', [
                            'idTipoUsuario' => $tipoUsuario->getIdTipoUsuario(),
                        ]);
                    }

                    return $this->render('tipoUsuario/modalEdit.html.twig', [
                        'tipoUsuario' => $tipoUsuario,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de un tipo usuario.
     *
     * @Route("/{idTipoUsuario}", name="tipoUsuario_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TipoUsuario $tipoUsuario,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tipos de usuario');  
                if (sizeof($permisos) > 0) {
                    try{

                        if ($this->isCsrfTokenValid('delete'.$tipoUsuario->getIdTipoUsuario(), $request->request->get('_token'))) {
                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->remove($tipoUsuario);
                            $entityManager->flush();
                            $successMessage= 'Se ha eliminado de forma correcta!';
                            $this->addFlash('mensaje',$successMessage);
                            return $this->redirectToRoute('tipoUsuario_index');
                        }

                    }catch (\Doctrine\DBAL\DBALException $e) {
                        if ($e->getCode() == 0){
                            if ($e->getPrevious()->getCode() == 23503){
                                $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                                $this->addFlash('error',$successMessage1);
                                return $this->redirectToRoute('tipoUsuario_index');
                            }else{
                                throw $e;
                            }
                        }else{
                            throw $e;
                        }
                    } 

                    return $this->redirectToRoute('tipoUsuario_index');
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }
}
