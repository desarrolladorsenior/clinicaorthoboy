<?php

namespace App\Controller;

use App\Entity\TratamientoConvenio;
use App\Form\TratamientoConvenioType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de tratamiento convenio utilizado para el manejo de los tratamientos que se pueden llegar a asociar a un convenio.
 *
 * @Route("/tratamientoConvenio")
 */
class TratamientoConvenioController extends AbstractController
{

    /**
     * Metodo utilizado para la edición de un tratamiento de un convenio.
     *
     * @Route("/{idTratamientoConvenio}/edit", name="tratamientoConvenio_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TratamientoConvenio $tratamientoConvenio,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tratamientos - EPS');  
                if (sizeof($permisos) > 0) {
                    $form = $this->createForm(TratamientoConvenioType::class, $tratamientoConvenio);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('convenio_index');
                    }
                    return $this->render('tratamientoConvenio/modalEdit.html.twig', [
                        'tratamientoConvenio' => $tratamientoConvenio,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

     /**
     * Metodo utilizado para la edición del estado de un tratamiento de un convenio.
     *
     * @Route("/editarEstadoTratamientoConvenio", name="tratamientoConvenio_editestado", methods={"GET","POST"})
     */
    public function editEstado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tratamientos - EPS');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $id=$request->request->get('id');
                    $estado=$request->request->get('estado');
                    if($estado == 'ACTIVO'){
                        $estado = true;
                    }else{
                        $estado = false;
                    }
                    $dqlModificarEstadoTratamiento="UPDATE App:TratamientoConvenio tConvenio SET tConvenio.estado= :estado WHERE tConvenio.idTratamientoConvenio= :idTConvenio";
                    $queryModificarEstadoTratamiento=$em->createQuery($dqlModificarEstadoTratamiento)->setParameter('estado', $estado)->setParameter('idTConvenio', $id);
                    $modificarEstadoCTratamiento=$queryModificarEstadoTratamiento->execute();
                    $response->setData('Se ha cambiado el estado de forma correcta!');
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }



    /**
     * Metodo utilizado para la eliminación de un tratamiento de un convenio.
     *
     * @Route("/{idTratamientoConvenio}", name="tratamientoConvenio_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TratamientoConvenio $tratamientoConvenio,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tratamientos - EPS');  
                if (sizeof($permisos) > 0) {
                    try{
                        if ($this->isCsrfTokenValid('delete'.$tratamientoConvenio->getIdTratamientoConvenio(), $request->request->get('_token'))) {
                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->remove($tratamientoConvenio);
                            $entityManager->flush();
                            $successMessage= 'Se ha eliminado de forma correcta!';
                            $this->addFlash('mensaje',$successMessage);
                            return $this->redirectToRoute('convenio_index');
                        }
                    }catch (\Doctrine\DBAL\DBALException $e) {
                        if ($e->getCode() == 0){
                            if ($e->getPrevious()->getCode() == 23503){
                                $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                                $this->addFlash('error',$successMessage1);
                                return $this->redirectToRoute('convenio_index');
                            }else{
                                throw $e;
                            }
                        }else{
                            throw $e;
                        }
                    } 
                    return $this->redirectToRoute('convenio_index');
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }
}
