<?php

namespace App\Controller;

use App\Entity\EntradaPedido;
use App\Form\EntradaPedidoType;
use App\Entity\Pedido;
use App\Form\PedidoType;
use App\Entity\Mantenimiento;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de entrada pedido utilizado para el manejo de pedidos de la clinica o sedes y conocer los elementos que ingresan en un pedido.
 *
 * @Route("/entradaPedido")
 */
class EntradaPedidoController extends AbstractController
{
    /**
     * Metodo utilizado para la edición del estado de una entrada de pedido.
     * 
     * @Route("/editarEstadoEntradaPedido", name="entradapedido_editestado", methods={"GET","POST"})
     */
    public function editEstadoEntradaPedido(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pedidos');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $id=$request->request->get('id');
                    $estado=$request->request->get('estado');
                    if($estado == 'ACTIVO'){
                        $estado = true;
                    }else{
                        $estado = false;
                    }

                    $dqlModificarEstadoEntradaPedido="UPDATE App:EntradaPedido ePedido SET ePedido.estado= :estado WHERE ePedido.idEntradaPedido= :idEntradaPedido";
                    $queryModificarEstadoEntradaPedido=$em->createQuery($dqlModificarEstadoEntradaPedido)->setParameter('estado', $estado)->setParameter('idEntradaPedido', $id);
                    $modificarEstadoEntradaPedido=$queryModificarEstadoEntradaPedido->execute();
                    $response->setData('Se ha cambiado el estado de forma correcta!');
                    return $response;                             
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la visualización de las entradas de pedidos, además del registro de las mismsas junto con el ingreso del primer mantenimiento si lo llega 
     * a tener. Igualmente visualiza las entradas generarles de elementos al pedido en cuestión.
     *
     * @Route("/{idPedido}/pedido", name="entradaPedido_index", methods={"GET","POST"})
     */
    public function index(Pedido $pedido,Request $request,PerfilGenerator $permisoPerfil): Response
    {   
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pedidos');  
                if (sizeof($permisos) > 0) {
                    $response= new JsonResponse();
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlEntradasPedidos ="SELECT ePedido FROM App:EntradaPedido ePedido WHERE ePedido.idPedido= :idPedido ORDER BY ePedido.idEntradaPedido DESC";  
                    $queryEntradasPedidos = $entityManager->createQuery($dqlEntradasPedidos)->setParameter('idPedido',$pedido->getIdPedido());
                    $entradasPedidos= $queryEntradasPedidos->getResult();

                    $entradaPedido=new EntradaPedido();
                    $entradaPedido->setIdPedido($pedido);
                    $entradaPedido->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $entradaPedido->setEstado(true);
                    $form = $this->createForm(EntradaPedidoType::class, $entradaPedido);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        if ($entradaPedido->getIntervaloMantenimiento() != '' ) {                        
                            $entityManager->persist($entradaPedido);
                            $entityManager->flush();     

                            $fechaAperturaPedido = $entradaPedido->getFechaApertura()->format('d M Y H:i:s');
                            $separacionIntervalo = explode("_", $entradaPedido->getIntervaloMantenimiento());
                            $mantenimiento= new Mantenimiento();
                            if ($separacionIntervalo[0]== 'DIA') {
                                //sumo cantidad dias
                                $dias=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." days");
                                $fecha=new \DateTime(date("m/d/Y h:i:s A T",$dias)); 
                                $mantenimiento->setFechaProgramada($fecha);
                            }else if ($separacionIntervalo[0]=='MES') {
                                //sumo cantidad meses
                                $meses=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." month");
                                $fecha=new \DateTime(date("m/d/Y h:i:s A T",$meses)); 
                                $mantenimiento->setFechaProgramada($fecha);
                            }else if ($separacionIntervalo[0] == 'ANIO') {
                                //sumo cantidad años
                                $anios=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." year");
                                 $fecha=new \DateTime(date("m/d/Y h:i:s A T",$anios)); 
                                $mantenimiento->setFechaProgramada($fecha);
                            }
                            $mantenimiento->setIdEntradaPedido($entradaPedido);
                            $mantenimiento->setTipoMantenimiento('PREVENTIVO');
                            $dqlEstadoMantenimiento ="SELECT eMantenimiento FROM App:EstadoMantenimiento eMantenimiento WHERE eMantenimiento.idEstadoMantenimiento= :idEstadoMantenimiento";  
                            $queryEstadoMantenimiento = $entityManager->createQuery($dqlEstadoMantenimiento)->setParameter('idEstadoMantenimiento',1);
                            $EstadoMantenimientos= $queryEstadoMantenimiento->getResult();
                            foreach ($EstadoMantenimientos as $seleccionEstado ) {
                                $mantenimiento->setIdEstadoMantenimiento($seleccionEstado);
                            }                                                   
                            $entityManager->persist($mantenimiento);
                            $entityManager->flush();  

                            $dqlEntradasPedido ="SELECT ePedido FROM App:EntradaPedido ePedido WHERE ePedido.idPedido= :idPedido ORDER BY ePedido.idPedido DESC";  
                            $queryEntradasPedidos = $entityManager->createQuery($dqlEntradasPedido)->setParameter('idPedido',$pedido->getIdPedido());
                            $entradasPedidosDos= $queryEntradasPedidos->getResult();
                            $template = $this->render('entradaPedido/datosEntradaPedido.html.twig', [
                            'entradasPedidos'=>$entradasPedidosDos
                            ])->getContent();
                            return new JsonResponse(['mensaje'=> 'Se ha realizado el registro de forma correcta!','entradasPedidos'=>$template]); 
                        }else{
                            $entityManager->persist($entradaPedido);
                            $entityManager->flush(); $dqlEntradasPedido ="SELECT ePedido FROM App:EntradaPedido ePedido WHERE ePedido.idPedido= :idPedido ORDER BY ePedido.idPedido DESC";  
                            $queryEntradasPedidos = $entityManager->createQuery($dqlEntradasPedido)->setParameter('idPedido',$pedido->getIdPedido());
                            $entradasPedidosDos= $queryEntradasPedidos->getResult();
                            $template = $this->render('entradaPedido/datosEntradaPedido.html.twig', [
                            'entradasPedidos'=>$entradasPedidosDos
                            ])->getContent();
                            return new JsonResponse(['mensaje'=> 'Se ha realizado el registro de forma correcta!','entradasPedidos'=>$template]); 
                        } 
                    }
                    return $this->render('entradaPedido/index.html.twig', [
                        'pedido' => $pedido,
                        'entradaPedido'=>$entradaPedido,
                        'form'=>$form->createView(),
                        'entradasPedidos'=>$entradasPedidos
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }
     


    /**
     * Metodo utilizado para la visualización de una entrada pedido.
     *
     * @Route("/{idEntradaPedido}", name="entradaPedido_show", methods={"GET","POST"})
     */
    public function show(EntradaPedido $entradaPedido,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pedidos');  
                if (sizeof($permisos) > 0) {
                    return $this->render('entradaPedido/modalShow.html.twig', [
                        'entradaPedido' => $entradaPedido,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la edición de una entrada de pedido, además si tiene mantenimiento dicha entrada se editara el mantenimiento.
     * Igualmente la edición de entrada del mantenimiento se dara solo en caso que la cantidad a editar no supere las salidadque se le den al producto en el moemnto 
     * de lo contrario se evidenciara la restricción. 
     *
     * @Route("/{idEntradaPedido}/edit", name="entradaPedido_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EntradaPedido $entradaPedido,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pedidos');  
                if (sizeof($permisos) > 0) {                    
                    $entityManager = $this->getDoctrine()->getManager();
                    /*Mantenimiento actual*/
                    $interMantenimientoGuardado=$entradaPedido->getIntervaloMantenimiento();
                    /*Cantidad actual*/
                    $cantidadGuardado=$entradaPedido->getCantidad();
                    /*Intervalo de garantía si se maneja.*/
                    $fechaFactura = $entradaPedido->getIdPedido()->getFechaFactura();
                    $fechaGarantiaActual = $entradaPedido->getGarantia();
                    if ($fechaGarantiaActual != '') {
                       $intervalo = $fechaFactura->diff($fechaGarantiaActual);
                        $seteoGarantia = $intervalo->format('%a');
                        if ($seteoGarantia < 30) {
                            $seteoGarantia=$seteoGarantia.'_DIA';
                        }else if ($seteoGarantia > 30 ) {
                            $seteoGarantia=$seteoGarantia/30;
                            if (round($seteoGarantia) < 30) {
                                $seteoGarantia=round($seteoGarantia).'_MES';
                            }else{
                                $seteoGarantia=round($seteoGarantia).'_ANIO';
                            }
                        }
                    }else{
                        $seteoGarantia='';
                    }
                    $form = $this->createForm(EntradaPedidoType::class, $entradaPedido);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        if ($entradaPedido->getEstado() != false ) {
                            $interMantenimientoEditado=$entradaPedido->getIntervaloMantenimiento();
                            $cantidadEditado=$entradaPedido->getCantidad();
                            if ($cantidadEditado != $cantidadGuardado) {
                               $dqlCantidadSalida ="SELECT sum(salida.cantidad) as sumaCantidad FROM App:Salida salida WHERE salida.idEntradaPedido= :idEntradaPedido";  
                                $queryCantidadSalida = $entityManager->createQuery($dqlCantidadSalida)->setParameter('idEntradaPedido',$entradaPedido->getIdEntradaPedido());
                                $cantidadSalida= $queryCantidadSalida->getResult();
                                foreach ($cantidadSalida as $seleccionSalida) {
                                    $cantidadTotalSalida=$seleccionSalida['sumaCantidad'];
                                }
                                if ($cantidadTotalSalida != '' and $cantidadTotalSalida > $cantidadEditado) {
                                    $this->addFlash('error', 'No se ha actualizado, la cantidad saliente supera la cantidad que desea establecer. Cantidad saliente '.$cantidadTotalSalida.' unidades.');
                                    return $this->redirectToRoute('entradaPedido_index', [
                                        'idPedido' => $entradaPedido->getIdPedido()->getIdPedido(),
                                    ]); 
                                }                                
                            }
                            if ($interMantenimientoGuardado != '' and $interMantenimientoEditado != '') {
                                if ($interMantenimientoEditado == $interMantenimientoGuardado) {
                                    /**Realizando el cambio de datos de entrada de pedido o elemento en caso que son iguales los intervalos del antiguo y el nuevo*/
                                    $entityManager->persist($entradaPedido);
                                    $entityManager->flush();                       
                                    $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                    return $this->redirectToRoute('entradaPedido_index', [
                                        'idPedido' => $entradaPedido->getIdPedido()->getIdPedido(),
                                    ]); 
                                }else{
                                    /*Consulta para conocer la cantidad de mantenimientos que existen de una entrada especifica*/
                                    $dqlCantidadMantenimiento ="SELECT count(mantenimiento) as cantidad FROM App:Mantenimiento mantenimiento WHERE mantenimiento.idEntradaPedido= :idEntradaPedido";  
                                    $queryCantidadMantenimiento = $entityManager->createQuery($dqlCantidadMantenimiento)->setParameter('idEntradaPedido',$entradaPedido->getIdEntradaPedido());
                                    $cantidadMantenimientosTotales= $queryCantidadMantenimiento->getResult();
                                    foreach ($cantidadMantenimientosTotales as $seleccionCantidad) {
                                        $cantidadMantenimientos=$seleccionCantidad['cantidad'];
                                    }
                                    /*Si no tiene mantenimientos y se estan ingresando los datos de mantenimiento se ingresa el mantenimiento*/
                                    if ($cantidadMantenimientos == 0) {
                                        $fechaAperturaPedido = $entradaPedido->getFechaApertura()->format('d M Y H:i:s');
                                        $separacionIntervalo = explode("_", $entradaPedido->getIntervaloMantenimiento());
                                        $mantenimiento= new Mantenimiento();
                                        if ($separacionIntervalo[0]== 'DIA') {
                                            //sumo cantidad dias
                                            $dias=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." days");
                                            $fecha=new \DateTime(date("m/d/Y h:i:s A T",$dias)); 
                                            $mantenimiento->setFechaProgramada($fecha);
                                        }else if ($separacionIntervalo[0]=='MES') {
                                            //sumo cantidad meses
                                            $meses=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." month");
                                            $fecha=new \DateTime(date("m/d/Y h:i:s A T",$meses)); 
                                            $mantenimiento->setFechaProgramada($fecha);
                                        }else if ($separacionIntervalo[0] == 'ANIO') {
                                            //sumo cantidad años
                                            $anios=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." year");
                                             $fecha=new \DateTime(date("m/d/Y h:i:s A T",$anios)); 
                                            $mantenimiento->setFechaProgramada($fecha);
                                        }
                                        $mantenimiento->setIdEntradaPedido($entradaPedido);
                                        $mantenimiento->setTipoMantenimiento('PREVENTIVO');
                                        $dqlEstadoMantenimiento ="SELECT eMantenimiento FROM App:EstadoMantenimiento eMantenimiento WHERE eMantenimiento.idEstadoMantenimiento= :idEstadoMantenimiento";  
                                        $queryEstadoMantenimiento = $entityManager->createQuery($dqlEstadoMantenimiento)->setParameter('idEstadoMantenimiento',1);
                                        $EstadoMantenimientos= $queryEstadoMantenimiento->getResult();
                                        foreach ($EstadoMantenimientos as $seleccionEstado ) {
                                            $mantenimiento->setIdEstadoMantenimiento($seleccionEstado);
                                        }                                                   
                                        $entityManager->persist($mantenimiento);
                                        $entityManager->flush();  

                                        /**Realizando el cambio de datos de entrada de pedido o elemento*/
                                        $entityManager->persist($entradaPedido);
                                        $entityManager->flush();                       
                                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                        return $this->redirectToRoute('entradaPedido_index', [
                                            'idPedido' => $entradaPedido->getIdPedido()->getIdPedido(),
                                        ]); 

                                    }else if ($cantidadMantenimientos == 1) {
                                        $fechaAperturaPedido = $entradaPedido->getFechaApertura()->format('d M Y H:i:s');
                                        $separacionIntervalo = explode("_", $entradaPedido->getIntervaloMantenimiento());
                                        if ($separacionIntervalo[0]== 'DIA') {
                                            //sumo cantidad dias
                                            $dias=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." days");
                                            $fechaProgramada=new \DateTime(date("m/d/Y h:i:s A T",$dias));
                                        }else if ($separacionIntervalo[0]=='MES') {
                                            //sumo cantidad meses
                                            $meses=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." month");
                                            $fechaProgramada=new \DateTime(date("m/d/Y h:i:s A T",$meses)); 
                                        }else if ($separacionIntervalo[0] == 'ANIO') {
                                            //sumo cantidad años
                                            $anios=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." year");
                                             $fechaProgramada=new \DateTime(date("m/d/Y h:i:s A T",$anios)); 
                                        }
                                        $dqlModificarMantenimiento="UPDATE App:Mantenimiento mantenimiento SET mantenimiento.fechaProgramada= :fechaProgramada WHERE mantenimiento.idEntradaPedido= :idEntradaPedido";
                                        $queryModificarMantenimiento=$entityManager->createQuery($dqlModificarMantenimiento)->setParameter('fechaProgramada', $fechaProgramada)->setParameter('idEntradaPedido', $entradaPedido->getIdEntradaPedido());
                                        $modificarMantenimiento=$queryModificarMantenimiento->execute();
                                        /**Realizando el cambio de datos de entrada de pedido o elemento*/
                                        $entityManager->persist($entradaPedido);
                                        $entityManager->flush();                       
                                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                        return $this->redirectToRoute('entradaPedido_index', [
                                            'idPedido' => $entradaPedido->getIdPedido()->getIdPedido(),
                                        ]); 
                                    }else{
                                        /**Realizando el cambio de datos de entrada de pedido o elemento*/
                                        $entityManager->persist($entradaPedido);
                                        $entityManager->flush();                       
                                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                        return $this->redirectToRoute('entradaPedido_index', [
                                            'idPedido' => $entradaPedido->getIdPedido()->getIdPedido(),
                                        ]); 
                                    }
                                }
                            }else if($interMantenimientoGuardado != '' and $interMantenimientoEditado == ''){
                                $dqlCantidadMantenimiento ="SELECT count(mantenimiento) as cantidad FROM App:Mantenimiento mantenimiento WHERE mantenimiento.idEntradaPedido= :idEntradaPedido";  
                                    $queryCantidadMantenimiento = $entityManager->createQuery($dqlCantidadMantenimiento)->setParameter('idEntradaPedido',$entradaPedido->getIdEntradaPedido());
                                    $cantidadMantenimientosTotales= $queryCantidadMantenimiento->getResult();
                                    foreach ($cantidadMantenimientosTotales as $seleccionCantidad) {
                                        $cantidadMantenimientos=$seleccionCantidad['cantidad'];
                                    }
                                    /*Si no tiene mantenimientos y se estan ingresando los datos de mantenimiento se ingresa el mantenimiento*/
                                    if ($cantidadMantenimientos == 0 or $cantidadMantenimientos > 1 ) {
                                        /*Si el mantenimiento solo tiene mas de un mantenimiento o ninguno y se esta quitando el mantenimiento no se debe realizar ninguna acción unicamente guardar los cambios del elemento.*/
                                        $entityManager->persist($entradaPedido);
                                        $entityManager->flush();                       
                                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                        return $this->redirectToRoute('entradaPedido_index', [
                                            'idPedido' => $entradaPedido->getIdPedido()->getIdPedido(),
                                        ]);
                                    }else if ($cantidadMantenimientos == 1) {
                                        /*Si el mantenimiento solo tiene un mantenimiento y se esta quitando el mantenimiento se debe eliminar el mantenimiento antiguo programado.*/
                                        $mantenimientos = $entityManager->getRepository('App:Mantenimiento')->findOneByIdEntradaPedido($entradaPedido->getIdEntradaPedido());
                                        $entityManager->remove($mantenimientos);
                                        $entityManager->flush();   
                                        /**Realizando el cambio de datos de entrada de pedido o elemento*/
                                        $entityManager->persist($entradaPedido);
                                        $entityManager->flush();                       
                                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                        return $this->redirectToRoute('entradaPedido_index', [
                                            'idPedido' => $entradaPedido->getIdPedido()->getIdPedido(),
                                        ]);   

                                    }
                            }else if($interMantenimientoGuardado == '' and $interMantenimientoEditado != ''){
                                /*Como no tuvo un mantenimiento y ahora se le esta colocando se debe ingresar el inicial para generar el primer mantenimiento*/
                                $fechaAperturaPedido = $entradaPedido->getFechaApertura()->format('d M Y H:i:s');
                                $separacionIntervalo = explode("_", $entradaPedido->getIntervaloMantenimiento());
                                $mantenimiento= new Mantenimiento();
                                if ($separacionIntervalo[0]== 'DIA') {
                                    //sumo cantidad dias
                                    $dias=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." days");
                                    $fecha=new \DateTime(date("m/d/Y h:i:s A T",$dias)); 
                                    $mantenimiento->setFechaProgramada($fecha);
                                }else if ($separacionIntervalo[0]=='MES') {
                                    //sumo cantidad meses
                                    $meses=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." month");
                                    $fecha=new \DateTime(date("m/d/Y h:i:s A T",$meses)); 
                                    $mantenimiento->setFechaProgramada($fecha);
                                }else if ($separacionIntervalo[0] == 'ANIO') {
                                    //sumo cantidad años
                                    $anios=strtotime($fechaAperturaPedido."+ ".$separacionIntervalo[1]." year");
                                     $fecha=new \DateTime(date("m/d/Y h:i:s A T",$anios)); 
                                    $mantenimiento->setFechaProgramada($fecha);
                                }
                                $mantenimiento->setIdEntradaPedido($entradaPedido);
                                $mantenimiento->setTipoMantenimiento('PREVENTIVO');
                                $dqlEstadoMantenimiento ="SELECT eMantenimiento FROM App:EstadoMantenimiento eMantenimiento WHERE eMantenimiento.idEstadoMantenimiento= :idEstadoMantenimiento";  
                                $queryEstadoMantenimiento = $entityManager->createQuery($dqlEstadoMantenimiento)->setParameter('idEstadoMantenimiento',1);
                                $EstadoMantenimientos= $queryEstadoMantenimiento->getResult();
                                foreach ($EstadoMantenimientos as $seleccionEstado ) {
                                    $mantenimiento->setIdEstadoMantenimiento($seleccionEstado);
                                }                                                   
                                $entityManager->persist($mantenimiento);
                                $entityManager->flush();  

                                /**Realizando el cambio de datos de entrada de pedido o elemento*/
                                $entityManager->persist($entradaPedido);
                                $entityManager->flush();                       
                                $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                return $this->redirectToRoute('entradaPedido_index', [
                                    'idPedido' => $entradaPedido->getIdPedido()->getIdPedido(),
                                ]);    
                            }
                            
                            
                        }else{                                
                            $this->addFlash('error', 'No se pudo actualizar estado inactivo!');
                            return $this->redirectToRoute('entradaPedido_index', [
                                'idPedido' => $entradaPedido->getIdPedido()->getIdPedido(),
                            ]);
                        }
                    }

                    return $this->render('entradaPedido/modalEdit.html.twig', [
                        'entradaPedido' => $entradaPedido,
                        'form' => $form->createView(),
                        'seteoGarantia'=>$seteoGarantia
                    ]);
                
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de la entrada de un mantenimiento.
     *
     * @Route("/{idEntradaPedido}", name="entradaPedido_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EntradaPedido $entradaPedido,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'pedidos');  
              if (sizeof($permisos) > 0) {
                //var_dump($estadoMantenimiento->getIdEstadoMantenimiento());
                try{
                    $idPedido=$entradaPedido->getIdPedido()->getIdPedido();
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($entradaPedido);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('entradaPedido_index', [
                            'idPedido' => $idPedido ,
                        ]);
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('entradaPedido_index', [
                                'idPedido' => $idPedido ,
                            ]);
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('entradaPedido_index', [
                    'idPedido' => $idPedido ,
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }
}
