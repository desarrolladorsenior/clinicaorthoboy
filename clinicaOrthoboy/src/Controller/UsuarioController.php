<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Form\UsuarioType;
use App\Controller\SecurityController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * Controlador de usuario utilizado para el manejo de los mismos en la mayoria de las acciones del sitema a causa que estos son requeridos para llevar el control de 
 * lo que se genera en el sistema.
 *
 * @Route("/usuario")
 */
class UsuarioController extends AbstractController
{

  /**
  * Metodo utilizado para la cambiar la imagen de perfil de un usuario.
  *
  * @Route("/{idUsuario}/cambiarPerfil", name="usuario_cambiarPerfil", methods={"GET","POST"})
  */
  public function cambiarPerfilImg(Request $request,PerfilGenerator $permisoPerfil,Usuario $usuario,UserPasswordEncoderInterface $encoder): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
          return $this->redirectToRoute('usuario_logout'); 
      } else{
          // La variable $_SESSION['usuario'] es un ejemplo. 
          //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
          if(isset($_SESSION['time'])){ 
            $tiempo = $_SESSION['time']; 
          }else{ 
            $tiempo = strtotime(date("Y-m-d H:i:s"));
          } 
          $inactividad =1800;   //Exprecion en segundos. 
          $actual =  strtotime(date("Y-m-d H:i:s")); 
          $tiempoTranscurrido=($actual-$tiempo);
          if(  $tiempoTranscurrido >= $inactividad){ 
            $session->invalidate();
            return $this->redirectToRoute('usuario_logout');
           // En caso que este sea mayor del tiempo seteado lo deslogea. 
          }else{ 
            $_SESSION['time'] =$actual; 
            $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
            $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
            //var_dump('expression',sizeof( $permisos));
            if (sizeof($permisos) > 0) {  
              $entityManager=$this->getDoctrine()->getManager();
                $fotoForm = $this->createFormBuilder($usuario)
                  ->add('imagenUsuario', FileType::class,array('required'=> false,'attr' =>array('accept'=> 'image/png,image/jpg,image/jpeg,image/vmp,image/tif,image/tiff','data-toggle'=>"tooltip",'data-placement'=>'right', 'data-html'=>'true'),'data_class' => null, 'property_path' => 'imagenUsuario'))
                   ->getForm();
                $fotoForm->handleRequest($request);
                
                if ($fotoForm->isSubmitted() && $fotoForm->isValid()) {
                  $file =  $fotoForm->get('imagenUsuario')->getData();
                  $usuario->uploadUno($file);  
                  $recoverPass = $this->recoverPass($usuario);
                  $usuario->setPassword($recoverPass[0]['password']); 
                  $entityManager->persist($usuario);
                  $entityManager->flush();
                  $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                  return $this->redirectToRoute('panel_principal');
                }
              
              return $this->render('usuario/modalImagenUsuario.html.twig', [
                    'usuario' => $usuario,
                    'form' => $fotoForm->createView(),
              ]);
              
            }else{
              $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
              return $this->redirectToRoute('panel_principal');
            }    
          }
      }    
  }

  /**
  * Metodo utilizado para la cambiar la contraseña de un usuario y notificarlo via correo electronico del cambio.
  *
  * @Route("/{idUsuario}/cambiarPass", name="usuario_cambiarContra", methods={"GET","POST"})
  */
  public function cambiarPass(Request $request,PerfilGenerator $permisoPerfil,Usuario $usuario,UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer ): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
          return $this->redirectToRoute('usuario_logout'); 
      } else{
          // La variable $_SESSION['usuario'] es un ejemplo. 
          //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
          if(isset($_SESSION['time'])){ 
            $tiempo = $_SESSION['time']; 
          }else{ 
            $tiempo = strtotime(date("Y-m-d H:i:s"));
          } 
          $inactividad =1800;   //Exprecion en segundos. 
          $actual =  strtotime(date("Y-m-d H:i:s")); 
          $tiempoTranscurrido=($actual-$tiempo);
          if(  $tiempoTranscurrido >= $inactividad){ 
            $session->invalidate();
            return $this->redirectToRoute('usuario_logout');
           // En caso que este sea mayor del tiempo seteado lo deslogea. 
          }else{ 
            $_SESSION['time'] =$actual; 
            $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
            $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
            //var_dump('expression',sizeof( $permisos));
            if (sizeof($permisos) > 0) {  
              $entityManager=$this->getDoctrine()->getManager();
              $contraseña=$request->get('passwordConfir');
              if (isset($contraseña)) {
                //$encoder= new UserPasswordEncoderInterface();
                $passwordFinal= $encoder->encodePassword($usuario, $contraseña);
                //modificación acuerdo porcentjae para el usuario
                $dqlModificarGenerarPass="UPDATE App:Usuario usuario SET usuario.password= :password WHERE usuario.idUsuario= :idUsuario";
                $queryModificarGenerarPass=$entityManager->createQuery($dqlModificarGenerarPass)->setParameter('password', $passwordFinal)->setParameter('idUsuario', $usuario->getIdUsuario());
                $modificarGenerarPass=$queryModificarGenerarPass->execute();
                $username=$usuario->getUsername();
                $para = $usuario->getEmail();
                $message = (new \Swift_Message())
                ->setSubject('Actualización cuenta del sistema clinico.')
                ->setFrom('anamileidy.rodriguez@upt.edu.co')
                ->setTo($para)  
                ->setBody('<html>
                  <head>
                    <style>
                      p{
                         font-family: "Cerebri Sans,sans-serif";
                         font-size:15px;
                         font-style:normal;
                         color: #6c757d;
                      }
                      b{
                        color:black;
                        font-size:15px;
                        font-style:normal;
                      }
                      img{
                        position: absolute; 
                        align-self: center; 
                        width: 3.5in; 
                        height:1.5in;
                      }
                    </style>
                  </head>
                  <body>                                
                    <p>Buen día</p><br>
                    <p> Se han actualizado los datos de ingreso al sistema de gestión clinico.</p>
                    <p>Sus datos de acceso son:</p>
                    <p> Nombre de usuario:  "<b>'.$username.'</b>"</p>
                    <p> Contraseña: "<b>'.$contraseña.'</b>"</p>
                    <p> Si requiere mayor información por favor comuniquese con el administrador de la plataforma. </p>
                    <p> Cordialmente,</p><br>       
                    <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                    <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br> 
                    </body>
                  </html>', 'text/html') ;                
                try {
                    $mailer->send($message);  
                } catch (\Swift_TransportException  $e) {
                    
                     $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                }
                $this->addFlash('mensaje', 'Se ha modificado la contraseña de forma correcta!');
                return $this->redirectToRoute('usuario_index');
              }
              return $this->render('usuario/cambioContraseña.html.twig', [
                    'dato' => $usuario->getIdUsuario()
                ]);
              
            }else{
              $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
              return $this->redirectToRoute('panel_principal');
            }    
          }
      }    
  }

  /**
  * Metodo utilizado para la cambiar la contraseña de un usuario genrando la misma automaticamente y notificar via correo electronico del cambio.
  *
  * @Route("/{idUsuario}/generarPass", name="usuario_generar", methods={"GET","POST"})
  */
  public function generarPass(Request $request,PerfilGenerator $permisoPerfil,Usuario $usuario,UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer ): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $entityManager=$this->getDoctrine()->getManager();
                
                $security=new SecurityController();
                $contraseñaGenerada=$security->generarCodigo(8);
                //$encoder= new UserPasswordEncoderInterface();
                $passwordFinal= $encoder->encodePassword($usuario, $contraseñaGenerada);
                //modificación acuerdo porcentjae para el usuario
                $dqlModificarGenerarPass="UPDATE App:Usuario usuario SET usuario.password= :password WHERE usuario.idUsuario= :idUsuario";
                $queryModificarGenerarPass=$entityManager->createQuery($dqlModificarGenerarPass)->setParameter('password', $passwordFinal)->setParameter('idUsuario', $usuario->getIdUsuario());
                $modificarGenerarPass=$queryModificarGenerarPass->execute();
                $username=$usuario->getUsername();
                $para = $usuario->getEmail();
                $message = (new \Swift_Message())
                ->setSubject('Actualización cuenta sistema clinico.')
                ->setFrom('anamileidy.rodriguez@upt.edu.co')
                ->setTo($para)  
                ->setBody('<html>
                  <head>
                    <style>
                      p{
                         font-family: "Cerebri Sans,sans-serif";
                         font-size:15px;
                         font-style:normal;
                         color: #6c757d;
                      }
                      b{
                        color:black;
                        font-size:15px;
                        font-style:normal;
                      }
                      img{
                        position: absolute; 
                        align-self: center; 
                        width: 3.5in; 
                        height:1.5in;
                      }
                    </style>
                  </head>
                  <body>                                
                    <p>Buen día</p><br>
                    <p> Se han actualizado los datos de ingreso al sistema de gestión clinico.</p>
                    <p>Sus datos de acceso son:</p>
                    <p> Nombre de usuario:  "<b>'.$username.'</b>"</p>
                    <p> Contraseña: "<b>'.$contraseñaGenerada.'</b>"</p>
                    <p> Si requiere mayor información por favor comuniquese con el administrador de la plataforma. </p>
                    <p> Cordialmente,</p><br>       
                    <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                    <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br> 
                    </body>
                  </html>', 'text/html') ;                
                try {
                    $mailer->send($message);  
                } catch (\Swift_TransportException  $e) {
                    
                     $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                }
                $this->addFlash('mensaje', 'Se ha generado la nueva contraseña de forma correcta!');
                return $this->redirectToRoute('usuario_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    
    }

    /**
     * Metodo utilizado para el ingreso de la firma a traves de una tableta, a causa de ello la ruta no tiene sistema de logueo y tampoco soliictud
     * de renovación de tiempo de sesionamiento.
     *
     * @Route("/tabledDos", name="usuario_tabledos", methods={"GET","POST"})
     */
    public function tabledDos(Request $request): Response
    {
      $idUsuario=$request->get('inf');
      $firmaActual=$request->get('firmaActual');
      $idUsuarioForm=$request->request->get('idUsuario');
      $firmaForm=$request->request->get('firmaProfesional');
      if (isset($firmaForm) && isset($idUsuarioForm)) {
       // $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
        //return $this->redirectToRoute('usuario_index');
        $entityManager=$this->getDoctrine()->getManager();        
        $dqlModificarUsuario="UPDATE App:Usuario usuario SET usuario.firmaProfesional= :firma WHERE usuario.idUsuario= :idUsuario";
        $queryModificarUsuario=$entityManager->createQuery($dqlModificarUsuario)->setParameter('firma', $firmaForm)->setParameter('idUsuario', $idUsuarioForm);
        $modificarUsuario=$queryModificarUsuario->execute();
        echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
      }
      return $this->render('usuario/tabledDos.html.twig', [
          'idUsuario' => $idUsuario,
          'firmaActual'=>$firmaActual
      ]);
    }
    /**
     * Metodo utilizado para la visualización de todos los usuarios y los datos de las agendas, acuerdoPorcentajes, acuerdoValors.
     *
     * @Route("/", name="usuario_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
                if (sizeof($permisos) > 0) {

                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlUsuarios="SELECT usuario FROM App:Usuario usuario
                    ORDER BY usuario.idUsuario DESC";  
                    $queryUsuarios = $entityManager->createQuery($dqlUsuarios);
                    $usuarios= $queryUsuarios->getResult();

                    $dqlAgendas="SELECT agendaUsuario FROM App:AgendaUsuario agendaUsuario where agendaUsuario.estado = :estado
                    ORDER BY agendaUsuario.idAgendaUsuario DESC";  
                    $queryAgendas = $entityManager->createQuery($dqlAgendas)->setParameter('estado',true);
                    $agendas= $queryAgendas->getResult();

                    $dqlAcuerdoPorcentaje="SELECT acuerdoP FROM App:AcuerdoPago acuerdoP where acuerdoP.estado = :estado and acuerdoP.porcentaje is not null and acuerdoP.valor is null
                    ORDER BY acuerdoP.idAcuerdoPago DESC";  
                    $queryAcuerdoPorcentaje = $entityManager->createQuery($dqlAcuerdoPorcentaje)->setParameter('estado',true);
                    $acuerdoPorcentajes= $queryAcuerdoPorcentaje->getResult();

                    $dqlAcuerdoValor="SELECT acuerdoP FROM App:AcuerdoPago acuerdoP where acuerdoP.estado = :estado and acuerdoP.porcentaje is null and acuerdoP.valor is not null
                    ORDER BY acuerdoP.idAcuerdoPago DESC";  
                    $queryAcuerdoValor = $entityManager->createQuery($dqlAcuerdoValor)->setParameter('estado',true);
                    $acuerdoValors= $queryAcuerdoValor->getResult();

                    return $this->render('usuario/index.html.twig', [
                        'usuarios' => $usuarios,
                        'agendas'=>$agendas,
                        'acuerdoPorcentajes'=>$acuerdoPorcentajes,
                        'acuerdoValors'=>$acuerdoValors
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Dentro de este metodo de modifica la selección que se halla efecetuado sobre una agenda para el manejo
     * de horarios del profesional de la clinica.
     *
     * @Route("/editarAgendaUsuario", name="usuario_editaragendausuario", methods={"GET","POST"})
    */
    public function editAgendaUsuario(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $entityManager=$this->getDoctrine()->getManager();
                $idUsuario=$request->request->get('idUsuario');
                $idAgenda=$request->request->get('idAgenda');
                $dqlModificarAgendaUsuario="UPDATE App:Usuario usuario SET usuario.idAgendaUsuario= :idAgendaUsuario WHERE usuario.idUsuario= :idUsuario";
                $queryModificarAgendaUsuario=$entityManager->createQuery($dqlModificarAgendaUsuario)->setParameter('idAgendaUsuario', $idAgenda)->setParameter('idUsuario', $idUsuario);
                $modificarAgendaUsuario=$queryModificarAgendaUsuario->execute();
                $this->addFlash('mensaje', 'Se ha cambiado la agenda del usuario de forma correcta!');
                        return $this->redirectToRoute('usuario_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    
    }

    /**
     *Dentro de este metodo de modifica la selección que se halla efecetuado sobre una acuerdo de porcentaje sobre sus atenciones medicas a clientes de la clinica
     *
     * @Route("/editarAcuerdoPor", name="usuario_editaracuerdoporcentaje", methods={"GET","POST"})
    */
    public function editAcuerdoPorc(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $entityManager=$this->getDoctrine()->getManager();
                $idUsuario=$request->request->get('idUsuario');
                $idAcuerdoPorcentaje=$request->request->get('idAcuerdoPorcentaje');
                if ($idAcuerdoPorcentaje == '') {
                    $idAcuerdoPorcentaje =null;
                }
                //modificación acuerdo porcentjae para el usuario
                $dqlModificarAcuerdoPorcentaje="UPDATE App:Usuario usuario SET usuario.idAcuerdoPorc= :idAcuerdoPorcentaje WHERE usuario.idUsuario= :idUsuario";
                $queryModificarAcuerdoPorcentaje=$entityManager->createQuery($dqlModificarAcuerdoPorcentaje)->setParameter('idAcuerdoPorcentaje', $idAcuerdoPorcentaje)->setParameter('idUsuario', $idUsuario);
                $modificarAcuerdoPorcentaje=$queryModificarAcuerdoPorcentaje->execute();
                $this->addFlash('mensaje', 'Se ha cambiado el acuerdo del porcentaje de forma correcta!');
                        return $this->redirectToRoute('usuario_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    
    }

    /**
     *Dentro de este metodo de modifica la selección que se halla efecetuado sobre una acuerdo de valor sobre sus atenciones medicas a clientes de la clinica
     *
     * @Route("/editarAcuerdoValor", name="usuario_editaracuerdovalor", methods={"GET","POST"})
    */
    public function editAcuerdoValorUsuario(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $entityManager=$this->getDoctrine()->getManager();
                $idUsuario=$request->request->get('idUsuario');
                $idAcuerdoValor=$request->request->get('idAcuerdoValor');
                if ($idAcuerdoValor == '') {
                    $idAcuerdoValor =null;
                }
                //modificación acuerdo porcentjae para el usuario
                $dqlModificarAcuerdoValor="UPDATE App:Usuario usuario SET usuario.idAcuerdoValor= :idAcuerdoValor WHERE usuario.idUsuario= :idUsuario";
                $queryModificarAcuerdoValor=$entityManager->createQuery($dqlModificarAcuerdoValor)->setParameter('idAcuerdoValor', $idAcuerdoValor)->setParameter('idUsuario', $idUsuario);
                $modificarAcuerdoValor=$queryModificarAcuerdoValor->execute();
                $this->addFlash('mensaje', 'Se ha cambiado el acuerdo del valor de forma correcta!');
                        return $this->redirectToRoute('usuario_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    
    }
    
    /**
     * Metodo utilizado para la edición del estado de un usuarios.
     *
     * @Route("/editarEstadoUsuario", name="usuario_editarestadousuario", methods={"GET","POST"})
     */
    public function editEstadoUsuario(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $em=$this->getDoctrine()->getManager();
                $response = new JsonResponse();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }
                $dqlModificarEstado="UPDATE App:Usuario usuario SET usuario.estado= :estado WHERE usuario.idUsuario= :idUsuario";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idUsuario', $id);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    

    }

    /**
     * Metodo utilizado para el registro de un usuario y notificación de correo electronico.
     *
     * @Route("/new", name="usuario_new", methods={"GET","POST"})
     */
    public function new(Request $request,PerfilGenerator $permisoPerfil,UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $entityManager = $this->getDoctrine()->getManager();
                $usuario = new Usuario();

                $usuario->setEstado(TRUE);               
                $usuario->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                $usuario->setIntento(0);
                $form = $this->createForm(UsuarioType::class, $usuario);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                  /*Trato Contraseña genracin y encriptación*/
                  $security=new SecurityController();
                  $contraseña=$security->generarCodigo(8);
                  //$encoder= new UserPasswordEncoderInterface();
                  $password= $encoder->encodePassword($usuario, $contraseña);
                  $usuario->setPassword($password);
                  $entityManager->persist($usuario);
                  $entityManager->flush();
                    
                  $username=$usuario->getUsername();

                  $para = $usuario->getEmail();
                    $message = (new \Swift_Message())
                      ->setSubject('Solicitud de registro de cuenta en sistema clinico.')
                      ->setFrom('anamileidy.rodriguez@upt.edu.co')
                      ->setTo($para)  
                      ->setBody('<html>
                          <head>
                            <style>
                              p{
                                 font-family: "Cerebri Sans,sans-serif";
                                 font-size:15px;
                                 font-style:normal;
                                 color: #6c757d;
                              }
                              b{
                                color:black;
                                font-size:15px;
                                font-style:normal;
                              }
                              img{
                                position: absolute; 
                                align-self: center; 
                                width: 3.5in; 
                                height:1.5in;
                              }
                            </style>
                          </head>
                            <body>                                
                              <p>Buen día</p><br>
                              <p> Te damos la bienvenida al sistema gestión clinico.</p>
                              <p>Sus datos de acceso son:</p>
                              <p> Nombre de usuario:  "<b>'.$username.'</b>"</p>
                              <p> Contraseña: "<b>'.$contraseña.'</b>"</p>
                              <p> Si requiere mayor información por favor comuniquese con el administrador de la plataforma. </p>
                              <p> Cordialmente,</p><br>       
                              <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                              <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br> 
                              </body>
                            </html>', 'text/html') ;                
                          try {
                              $mailer->send($message);  
                          } catch (\Swift_TransportException  $e) {
                              
                               $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                          }
                    $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');   

                    return $this->redirectToRoute('usuario_index');
                }

                return $this->render('usuario/new.html.twig', [
                    'usuario' => $usuario,
                    'form' => $form->createView(),
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    
    }

    /**
     * Metodo utilizado para la visualización de un usuario.
     *
     * @Route("/{idUsuario}", name="usuario_show", methods={"GET","POST"})
     */
    public function show(Usuario $usuario,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                return $this->render('usuario/modalShow.html.twig', [
                    'usuario' => $usuario,
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    
    }

    /**
     * Metodo utilizado para la edición de un usuario y notificación de un correo electronico.
     *
     * @Route("/{idUsuario}/edit", name="usuario_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Usuario $usuario,PerfilGenerator $permisoPerfil,UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
          return $this->redirectToRoute('usuario_logout'); 
      } else{
          // La variable $_SESSION['usuario'] es un ejemplo. 
          //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
          if(isset($_SESSION['time'])){ 
            $tiempo = $_SESSION['time']; 
          }else{ 
            $tiempo = strtotime(date("Y-m-d H:i:s"));
          } 
          $inactividad =1800;   //Exprecion en segundos. 
          $actual =  strtotime(date("Y-m-d H:i:s")); 
          $tiempoTranscurrido=($actual-$tiempo);
          if(  $tiempoTranscurrido >= $inactividad){ 
            $session->invalidate();
            return $this->redirectToRoute('usuario_logout');
           // En caso que este sea mayor del tiempo seteado lo deslogea. 
          }else{ 
            $_SESSION['time'] =$actual; 
            $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
            $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
            //var_dump('expression',sizeof( $permisos));
            if (sizeof($permisos) > 0) {
              if ($this->get('security.token_storage')->getToken()->getUser()->getIdPerfil()->getIdPerfil() == 1 && $usuario->getIdPerfil()->getIdPerfil() == 1 || $usuario->getIdPerfil()->getIdPerfil() != 1 ) {
                $usernameActual=$usuario->getUsername();
               
                $entityManager=$this->getDoctrine()->getManager();
                $dqlPerfil = "SELECT perfil FROM App:Perfil perfil INNER JOIN App:Usuario usuario WITH usuario.idPerfil=perfil.idPerfil  WHERE usuario.idUsuario=:idUsuario";
                $queryPerfil = $entityManager->createQuery($dqlPerfil)->setParameter('idUsuario',$usuario->getIdUsuario());
                $perfilActual = $queryPerfil->getResult();  
                //Generación formulario 
                $form = $this->createForm(UsuarioType::class, $usuario);
                $form->handleRequest($request);
                foreach ($perfilActual as $seleccionPerfil ) {   
                  $nombrePerfil= $seleccionPerfil->getNombre();               
                 if ($nombrePerfil == 'ROLE_SUPERADMINISTRADOR') {
                     $usuario->getIdPerfil($usuario->setIdPerfil($seleccionPerfil));
                  }else{
                     $usuario->getIdPerfil();
                  }   
                }
                if ($form->isSubmitted() && $form->isValid()) {
                  $recoverPass = $this->recoverPass($usuario);
                  $usuario->setPassword($recoverPass[0]['password']); 
                  $entityManager->persist($usuario);
                  $entityManager->flush();
                  $username=$usuario->getUsername();
                  if ($usernameActual != $username){
                    $para = $usuario->getEmail();
                    $message = (new \Swift_Message())
                    ->setSubject('Actualización cuenta del sistema clinico.')
                    ->setFrom('anamileidy.rodriguez@upt.edu.co')
                    ->setTo($para)  
                    ->setBody('<html>
                      <head>
                        <style>
                          p{
                             font-family: "Cerebri Sans,sans-serif";
                             font-size:15px;
                             font-style:normal;
                             color: #6c757d;
                          }
                          b{
                            color:black;
                            font-size:15px;
                            font-style:normal;
                          }
                          img{
                            position: absolute; 
                            align-self: center; 
                            width: 3.5in; 
                            height:1.5in;
                          }
                        </style>
                      </head>
                      <body>                                
                        <p>Buen día</p><br>
                        <p> Se ha actualizado el nombre de usuario del sistema de gestión clinico.</p>
                        <p>Sus datos de acceso son:</p>
                        <p> Nombre de usuario:  "<b>'.$username.'</b>"</p>
                        <p> Si requiere mayor información por favor comuniquese con el administrador de la plataforma. </p>
                        <p> Cordialmente,</p><br>       
                        <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                        <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br> 
                        </body>
                      </html>', 'text/html') ;                
                    try {
                        $mailer->send($message);  
                    } catch (\Swift_TransportException  $e) {
                        
                         $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                    }
                  }
                    $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                    return $this->redirectToRoute('usuario_index');
                }

                return $this->render('usuario/edit.html.twig', [
                    'usuario' => $usuario,
                    'form' => $form->createView(),
                ]);
            }else{
              $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
            return $this->redirectToRoute('usuario_index');
            }
          }else{
            $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
            return $this->redirectToRoute('panel_principal');
          }    
        }
      }    
    }

    /**
     * Metodo utilizado para la eliminación de un usuario.
     *
     * @Route("/{idUsuario}", name="usuario_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Usuario $usuario,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
          return $this->redirectToRoute('usuario_logout'); 
      } else{
        // La variable $_SESSION['usuario'] es un ejemplo. 
        //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
        if(isset($_SESSION['time'])){ 
          $tiempo = $_SESSION['time']; 
        }else{ 
          $tiempo = strtotime(date("Y-m-d H:i:s"));
        } 
        $inactividad =1800;   //Exprecion en segundos. 
        $actual =  strtotime(date("Y-m-d H:i:s")); 
        $tiempoTranscurrido=($actual-$tiempo);
        if(  $tiempoTranscurrido >= $inactividad){ 
          $session->invalidate();
          return $this->redirectToRoute('usuario_logout');
         // En caso que este sea mayor del tiempo seteado lo deslogea. 
        }else{ 
          $_SESSION['time'] =$actual; 
          $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
          $permisos=$permisoPerfil->verificacionPermisos($perfil,'usuarios');  
          if (sizeof($permisos) > 0) {
            //var_dump($esterilizacion->getIdEsterilizacion());
            try{
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($usuario);
                $entityManager->flush();
                $successMessage= 'Se ha eliminado de forma correcta!';
                $this->addFlash('mensaje',$successMessage);
                return $this->redirectToRoute('usuario_index');
                
            }catch (\Doctrine\DBAL\DBALException $e) {
                if ($e->getCode() == 0){
                    if ($e->getPrevious()->getCode() == 23503){
                        $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                        $this->addFlash('error',$successMessage1);
                        return $this->redirectToRoute('usuario_index');
                    }else{
                        throw $e;
                    }
                }else{
                    throw $e;
                }
            } 
            return $this->redirectToRoute('usuario_index');
          }else{
            $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
            return $this->redirectToRoute('panel_principal');
          }     
        }
      } 
    }
    /**
    * Consulta de usuario por numero identificador.
    *
    */
     private function recoverPass($id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT u.password
            FROM App:Usuario u
            WHERE u.idUsuario = :id'    
        )->setParameter('id', $id);
        
        $currentPass = $query->getResult();
        
        return $currentPass;
    }
     
}
