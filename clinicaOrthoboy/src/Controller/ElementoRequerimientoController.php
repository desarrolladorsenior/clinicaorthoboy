<?php

namespace App\Controller;

use App\Entity\ElementoRequerimiento;
use App\Entity\Requerimiento;
use App\Entity\OrdenCompra;
use App\Entity\ElementoOrdenCompra;
use App\Form\ElementoRequerimientoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de elemento requerimiento utilizado para el manejo de los elementos requeridos por las sedes de la clinica.
 *
 * @Route("/elementoRequerimiento")
*/
class ElementoRequerimientoController extends Controller
{
    /**
    * Metodo utilizado para la generación de un pdf que evidencia los requerimientos de una sede de la clinica.
    *
    */
    public function pdfRequerimiento(Requerimiento $requerimiento){
        $entityManager = $this->getDoctrine()->getManager();
        //Consulto a base de datos        
        $dqlElementoRequerimiento ="SELECT eRequerimiento FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento ORDER BY eRequerimiento.idElementoRequerimiento ASC";  
        $queryElementoRequerimiento= $entityManager->createQuery($dqlElementoRequerimiento)->setParameter('idRequerimiento',$requerimiento->getIdRequerimiento());
        $elementoRequerimientos= $queryElementoRequerimiento->getResult();

        

        $html =$this->renderView('elementoRequerimiento/pdfRequerimiento.html.twig',
        array(
        'requerimiento' => $requerimiento,
        'elementoRequerimientos' => $elementoRequerimientos
        ));
         
        $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
        [   'encoding' => 'UTF-8',
            'images' => false,
            'enable-javascript' => true,
            'print-media-type' => true,
            'outline-depth' => 8,
            'orientation' => 'Portrait',
            'header-right'=>'Pag. [page] de [toPage]',
            'page-size' => 'A4',
            'viewport-size' => '1280x1024',
            'margin-left' => '10mm',
            'margin-right' => '10mm',
            'margin-top' => '30mm',
            'margin-bottom' => '25mm',
            'user-style-sheet'=> 'light/dist/assets/css/bootstrap.min.css',
            //'background'     => 'light/dist/assets/images/users/user-1.jpg',
            //--javascript-delay 3000
        ]);
        return $pdf;
    }

    /**
     * Metodo utilizado para el envio de un correo electronico con todos los requerimientos que tiene la sede en el momento.
     * Requerimiento con todos los elementos
     * @Route("/{idRequerimiento}/correo", name="elementoRequerimiento_correo", methods={"GET","POST"})
     */
    public function correoRequerimiento(Requerimiento $requerimiento,Request $request,PerfilGenerator $permisoPerfil, \Swift_Mailer $mailer): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $entityManager=$this->getDoctrine()->getManager();
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes requerimientos');  
                if (sizeof($permisos) > 0) {

                    $filename="Requerimiento_N°_".$requerimiento->getIdRequerimiento()."_fechaSolicitud_". date("Y_m_d") .".pdf";

                    $attach = new \Swift_Attachment(
                    $this->pdfRequerimiento($requerimiento), 
                    $filename, 
                    'application/pdf');

                    $dqlElementoRequerimiento ="SELECT eRequerimiento FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento ORDER BY eRequerimiento.idElementoRequerimiento ASC";  
                    $queryElementoRequerimiento= $entityManager->createQuery($dqlElementoRequerimiento)->setParameter('idRequerimiento',$requerimiento->getIdRequerimiento());
                    $elementoRequerimientos= $queryElementoRequerimiento->getResult();
                    
                    $correoRequerimiento= $this->renderView('elementoRequerimiento/correoRequerimiento.html.twig', array(
                      'requerimiento' => $requerimiento,
                      'elementoRequerimientos' => $elementoRequerimientos
                    ));
                    /** Consulta para conocer el correo del super administrador**/
                    $dqlUsuarioSuperAdmin ="SELECT usuario FROM App:Usuario usuario INNER JOIN App:Perfil perfil WITH perfil.idPerfil=usuario.idPerfil WHERE perfil.nombre = :nombrePerfil ";  
                    $queryUsuarioSuperAdmin = $entityManager->createQuery($dqlUsuarioSuperAdmin)->setParameter('nombrePerfil','ROLE_SUPERADMINISTRADOR');
                    $usuarioSuperAdmin= $queryUsuarioSuperAdmin->getResult();
                    foreach ($usuarioSuperAdmin as $seleccionUsuario) {
                        $emailUsuarioAdministrador=$seleccionUsuario->getEmail();
                        $clinicaNombre=$seleccionUsuario->getIdSucursal();
                    }

                    /** Consulta para conocer el correo de los usuarios que tienen permisos para  ordenes de compra segun la sede de requerimiento**/
                    $dqlUsuarios ="SELECT usuario FROM App:Usuario usuario INNER JOIN App:Perfil perfil WITH perfil.idPerfil=usuario.idPerfil INNER JOIN App:PerfilPermiso perfilP WITH perfil.idPerfil=perfilP.idPerfil WHERE perfilP.idPermiso = :idPermiso and usuario.idSucursal = :idClinica ";  
                    $queryUsuarios = $entityManager->createQuery($dqlUsuarios)->setParameter('idPermiso',311)->setParameter('idClinica',$requerimiento->getIdSedeClinica()->getIdClinica());
                    $usuarios= $queryUsuarios->getResult();
                    $arrayEmailSede=array();
                    foreach ($usuarios as $seleccionUsuario) {
                        $emailUsuarioSede=$seleccionUsuario->getEmail();
                        array_push($arrayEmailSede, $emailUsuarioSede);
                    }
                    $emailUsuarioSesion=$this->get('security.token_storage')->getToken()->getUser()->getEmail();
                    $para=array($emailUsuarioAdministrador,$emailUsuarioSesion);
                    $correoEnviar=array_merge($para,$arrayEmailSede);
                    $message = (new \Swift_Message())
                    ->setSubject('Requerimiento N° '.$requerimiento->getIdRequerimiento().' - Sede: '.$requerimiento->getIdSedeClinica())
                    ->setFrom('anamileidy.rodriguez@upt.edu.co')
                    ->setTo($correoEnviar)  
                    ->setBody($correoRequerimiento, 'text/html')
                    ->attach($attach);              
                    try {
                        $mailer->send($message); 
                        /*Modificación de estado de orden compra */
                        $dqlModificarEstadoRequerimiento="UPDATE App:Requerimiento requerimiento SET requerimiento.estado= :estado WHERE requerimiento.idRequerimiento= :idRequerimiento";
                        $queryModificarEstadoRquerimiento=$entityManager->createQuery($dqlModificarEstadoRequerimiento)->setParameter('estado', 'ENVIADO')->setParameter('idRequerimiento', $requerimiento->getIdRequerimiento());
                        $modifEstadoRequerimiento=$queryModificarEstadoRquerimiento->execute();
                            
                        $this->addFlash('mensaje', 'Se ha realizado el envio del correo electrónico de forma correcta!'); 
                    } catch (\Swift_TransportException  $e) {
                        $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                    }                    
                    //return $this->redirectToRoute('requerimiento_index');
                    return $this->redirectToRoute('elementoRequerimiento_index',['idRequerimiento'=>$requerimiento->getIdRequerimiento()]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     *Funcion utilizada para el cambio de estado del elemento requerido y creacion de order de compara a proveedor o añadir elemento a orden de proveedor ya en ejecución.
     * @Route("/{idElementoRequerimiento}/editarEstadoConfirmacion", name="elementoRequerimiento_editestado", methods={"GET","POST"})
     */
    public function editEstadoEntradaRequerimiento(Request $request,PerfilGenerator $permisoPerfil,ElementoRequerimiento $elementoRequerimiento): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes requerimientos');  
                if (sizeof($permisos) > 0) {
                    $entityManager=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $estado=$request->request->get('estadoValorConfirmacion');
                    $proveedor=$request->request->get('idProveedorOrden');
                    
                    if ($estado == 'CONFIRMADO' and $proveedor != '') {
                        /*CAMBIO DE ESTADO DEL ELEMENTO REQUERIDO*/
                        $dqlModificarEstadoEntradaRequerimiento="UPDATE App:ElementoRequerimiento eRequerimiento SET eRequerimiento.estado= :estado WHERE eRequerimiento.idElementoRequerimiento= :idElementoRequerimiento";
                        $queryModificarEstadoEntradaRequerimiento=$entityManager->createQuery($dqlModificarEstadoEntradaRequerimiento)->setParameter('estado', $estado)->setParameter('idElementoRequerimiento', $elementoRequerimiento->getIdElementoRequerimiento());
                        $modificarEstadoEntradaRequerimiento=$queryModificarEstadoEntradaRequerimiento->execute();

                        $dqlOrdenCompra ="SELECT ordenCompra FROM App:OrdenCompra ordenCompra INNER JOIN App:Proveedor proveedor WITH proveedor.idProveedor= ordenCompra.idProveedor WHERE ordenCompra.estadoEnvioCorreo= :estadoEnvio AND proveedor.idProveedor= :idProveedor";  
                        $queryOrdenCompra = $entityManager->createQuery($dqlOrdenCompra)->setParameter('idProveedor',$proveedor)->setParameter('estadoEnvio',false);
                        $ordenesCompra= $queryOrdenCompra->getResult();
                        if (sizeof($ordenesCompra) == 0) {
                            /*Ingreso orden de compra */
                            $ordenCompra= new OrdenCompra();
                            $dqlProveedor ="SELECT proveedor FROM App:Proveedor proveedor WHERE proveedor.idProveedor= :idProveedor";  
                            $queryProveedor = $entityManager->createQuery($dqlProveedor)->setParameter('idProveedor',$proveedor);
                            $proveedores= $queryProveedor->getResult();
                            foreach ($proveedores as $seleccionProveedores) {
                                $ordenCompra->setIdProveedor($seleccionProveedores);
                            }                            
                            $ordenCompra->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                            $ordenCompra->setEstadoEnvioCorreo(false);
                            $entityManager->persist($ordenCompra);
                            $entityManager->flush();

                            $elementoCompra= new ElementoOrdenCompra();
                            $elementoCompra->setIdOrdenCompra($ordenCompra);
                            $elementoCompra->setIdElementoRequerimiento($elementoRequerimiento);
                            $entityManager->persist($elementoCompra);
                            $entityManager->flush();
                            /**Consulta para cambiar estado de requerimiento a revisado u finalizado segun el caso
                            *Cambio de estado de requerimeinto a reavisado u finalizado segun el caso */
                            $dqlContadorElementoRequerimiento ="SELECT count(eRequerimiento) as contador FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento ";  
                            $queryContadorElementoRequerimiento = $entityManager->createQuery($dqlContadorElementoRequerimiento)->setParameter('idRequerimiento',$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                            $contadorElementoRequerimiento= $queryContadorElementoRequerimiento->getResult();
                            foreach ($contadorElementoRequerimiento as $seleccionElemento) {
                                $totalContadorElementos=$seleccionElemento['contador'];
                            }

                            $dqlContadorElementoRequerimientoNoPendiente ="SELECT count(eRequerimiento) as contador FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento AND eRequerimiento.estado != :estado";  
                            $queryContadorElementoRequerimientoNoPendiente = $entityManager->createQuery($dqlContadorElementoRequerimientoNoPendiente)->setParameter('estado','PENDIENTE')->setParameter('idRequerimiento',$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                            $contadorElementoRequerimientoNoPendiente= $queryContadorElementoRequerimientoNoPendiente->getResult();
                            foreach ($contadorElementoRequerimientoNoPendiente as $seleccionElemento) {
                                $totalContadorElementosNoPendiente=$seleccionElemento['contador'];
                            }

                            if ($totalContadorElementos == $totalContadorElementosNoPendiente) {
                               $dqlModificarEstadoRequerimiento="UPDATE App:Requerimiento requerimiento SET requerimiento.estado= :estado WHERE requerimiento.idRequerimiento= :idRequerimiento";
                                $queryModificarEstadoRquerimiento=$entityManager->createQuery($dqlModificarEstadoRequerimiento)->setParameter('estado', 'FINALIZADO')->setParameter('idRequerimiento', $elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                                $modifEstadoRequerimiento=$queryModificarEstadoRquerimiento->execute();
                            }else{
                                $dqlModificarEstadoRequerimiento="UPDATE App:Requerimiento requerimiento SET requerimiento.estado= :estado WHERE requerimiento.idRequerimiento= :idRequerimiento";
                                $queryModificarEstadoRquerimiento=$entityManager->createQuery($dqlModificarEstadoRequerimiento)->setParameter('estado', 'REVISADO')->setParameter('idRequerimiento', $elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                                $modifEstadoRequerimiento=$queryModificarEstadoRquerimiento->execute();
                            }

                            $this->addFlash('mensaje', 'Se ha cambiado el estado de forma correcta y adicionado orden de compra y/o elemento a orden de compra.');
                            return $this->redirectToRoute('elementoRequerimiento_index',['idRequerimiento'=>$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento()]);

                        }else{                           
                            $elementoCompra= new ElementoOrdenCompra();
                            foreach ($ordenesCompra as $seleccionOrdenCompra) {                                
                                $elementoCompra->setIdOrdenCompra($seleccionOrdenCompra);
                            }
                            $elementoCompra->setIdElementoRequerimiento($elementoRequerimiento);
                            $entityManager->persist($elementoCompra);
                            $entityManager->flush();

                            /**Consulta para cambiar estado de requerimiento a revisado u finalizado segun el caso
                            *Cambio de estado de requerimeinto a reavisado u finalizado segun el caso */
                            $dqlContadorElementoRequerimiento ="SELECT count(eRequerimiento) as contador FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento ";  
                            $queryContadorElementoRequerimiento = $entityManager->createQuery($dqlContadorElementoRequerimiento)->setParameter('idRequerimiento',$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                            $contadorElementoRequerimiento= $queryContadorElementoRequerimiento->getResult();
                            foreach ($contadorElementoRequerimiento as $seleccionElemento) {
                                $totalContadorElementos=$seleccionElemento['contador'];
                            }

                            $dqlContadorElementoRequerimientoNoPendiente ="SELECT count(eRequerimiento) as contador FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento AND eRequerimiento.estado != :estado";  
                            $queryContadorElementoRequerimientoNoPendiente = $entityManager->createQuery($dqlContadorElementoRequerimientoNoPendiente)->setParameter('estado','PENDIENTE')->setParameter('idRequerimiento',$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                            $contadorElementoRequerimientoNoPendiente= $queryContadorElementoRequerimientoNoPendiente->getResult();
                            foreach ($contadorElementoRequerimientoNoPendiente as $seleccionElemento) {
                                $totalContadorElementosNoPendiente=$seleccionElemento['contador'];
                            }

                            if ($totalContadorElementos == $totalContadorElementosNoPendiente) {
                               $dqlModificarEstadoRequerimiento="UPDATE App:Requerimiento requerimiento SET requerimiento.estado= :estado WHERE requerimiento.idRequerimiento= :idRequerimiento";
                                $queryModificarEstadoRquerimiento=$entityManager->createQuery($dqlModificarEstadoRequerimiento)->setParameter('estado', 'FINALIZADO')->setParameter('idRequerimiento', $elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                                $modifEstadoRequerimiento=$queryModificarEstadoRquerimiento->execute();
                            }else{
                                $dqlModificarEstadoRequerimiento="UPDATE App:Requerimiento requerimiento SET requerimiento.estado= :estado WHERE requerimiento.idRequerimiento= :idRequerimiento";
                                $queryModificarEstadoRquerimiento=$entityManager->createQuery($dqlModificarEstadoRequerimiento)->setParameter('estado', 'REVISADO')->setParameter('idRequerimiento', $elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                                $modifEstadoRequerimiento=$queryModificarEstadoRquerimiento->execute();
                            }

                            $this->addFlash('mensaje', 'Se ha cambiado el estado de forma correcta y adicionado elemento a orden de compra!');
                            return $this->redirectToRoute('elementoRequerimiento_index',['idRequerimiento'=>$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento()]);
                        }
                        
                    }elseif ($estado == 'RECHAZADO') {
                        /*CAMBIO DE ESTADO DEL ELEMENTO REQUERIDO*/
                        $dqlModificarEstadoEntradaRequerimiento="UPDATE App:ElementoRequerimiento eRequerimiento SET eRequerimiento.estado= :estado WHERE eRequerimiento.idElementoRequerimiento= :idElementoRequerimiento";
                        $queryModificarEstadoEntradaRequerimiento=$entityManager->createQuery($dqlModificarEstadoEntradaRequerimiento)->setParameter('estado', $estado)->setParameter('idElementoRequerimiento', $elementoRequerimiento->getIdElementoRequerimiento());
                        $modificarEstadoEntradaRequerimiento=$queryModificarEstadoEntradaRequerimiento->execute();
                        /**Consulta para cambiar estado de requerimiento a revisado u finalizado segun el caso
                            *Cambio de estado de requerimeinto a reavisado u finalizado segun el caso */
                        $dqlContadorElementoRequerimiento ="SELECT count(eRequerimiento) as contador FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento ";  
                        $queryContadorElementoRequerimiento = $entityManager->createQuery($dqlContadorElementoRequerimiento)->setParameter('idRequerimiento',$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                        $contadorElementoRequerimiento= $queryContadorElementoRequerimiento->getResult();
                        foreach ($contadorElementoRequerimiento as $seleccionElemento) {
                            $totalContadorElementos=$seleccionElemento['contador'];
                        }

                        $dqlContadorElementoRequerimientoNoPendiente ="SELECT count(eRequerimiento) as contador FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento AND eRequerimiento.estado != :estado";  
                        $queryContadorElementoRequerimientoNoPendiente = $entityManager->createQuery($dqlContadorElementoRequerimientoNoPendiente)->setParameter('estado','PENDIENTE')->setParameter('idRequerimiento',$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                        $contadorElementoRequerimientoNoPendiente= $queryContadorElementoRequerimientoNoPendiente->getResult();
                        foreach ($contadorElementoRequerimientoNoPendiente as $seleccionElemento) {
                            $totalContadorElementosNoPendiente=$seleccionElemento['contador'];
                        }

                        if ($totalContadorElementos == $totalContadorElementosNoPendiente) {
                           $dqlModificarEstadoRequerimiento="UPDATE App:Requerimiento requerimiento SET requerimiento.estado= :estado WHERE requerimiento.idRequerimiento= :idRequerimiento";
                            $queryModificarEstadoRquerimiento=$entityManager->createQuery($dqlModificarEstadoRequerimiento)->setParameter('estado', 'FINALIZADO')->setParameter('idRequerimiento', $elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                            $modifEstadoRequerimiento=$queryModificarEstadoRquerimiento->execute();
                        }else{
                            $dqlModificarEstadoRequerimiento="UPDATE App:Requerimiento requerimiento SET requerimiento.estado= :estado WHERE requerimiento.idRequerimiento= :idRequerimiento";
                            $queryModificarEstadoRquerimiento=$entityManager->createQuery($dqlModificarEstadoRequerimiento)->setParameter('estado', 'REVISADO')->setParameter('idRequerimiento', $elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento());
                            $modifEstadoRequerimiento=$queryModificarEstadoRquerimiento->execute();
                        }

                        $this->addFlash('mensaje', 'Se ha cambiado el estado de forma correcta!');
                        return $this->redirectToRoute('elementoRequerimiento_index',['idRequerimiento'=>$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento()]);
                    }else{
                       $this->addFlash('error', 'No se ha realizado ningún cambio!');
                        return $this->redirectToRoute('elementoRequerimiento_index',['idRequerimiento'=>$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento()]);
                    }                           
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');  
                   
                }    
            }
        }      
    }
     /**
     * Metodo utilizado para la visualización de los elementos requeridos dentro de un requerimiento especifico, además del registro de un elemento 
     * requerido y la visualización de los proveedores.
     * @Route("/{idRequerimiento}/requerimiento", name="elementoRequerimiento_index", methods={"GET","POST"})
     */
    public function index(Requerimiento $requerimiento,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes requerimientos');  
                if (sizeof($permisos) > 0) {
                    $response= new JsonResponse();
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlElementoRequerimiento ="SELECT eRequerimiento FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento ORDER BY eRequerimiento.idElementoRequerimiento DESC";  
                    $queryElementoRequerimiento = $entityManager->createQuery($dqlElementoRequerimiento)->setParameter('idRequerimiento',$requerimiento->getIdRequerimiento());
                    $elementoRequerimientos= $queryElementoRequerimiento->getResult();

                    $elementoRequerimiento=new ElementoRequerimiento();
                    $elementoRequerimiento->setIdRequerimiento($requerimiento);
                    $elementoRequerimiento->setEstado('PENDIENTE');
                    $form = $this->createForm(ElementoRequerimientoType::class, $elementoRequerimiento);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) { 
                        if ($elementoRequerimiento->getIdRequerimiento()->getEstado() == 'EN PROCESO' or $elementoRequerimiento->getIdRequerimiento()->getEstado() == 'ENVIADO' ) {
                                                               
                            $entityManager->persist($elementoRequerimiento);
                            $entityManager->flush(); 

                            $dqlElementoRequerimientosGuardados ="SELECT eRequerimiento FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento ORDER BY eRequerimiento.idElementoRequerimiento DESC";  
                            $queryElementoRequerimientosGuardados = $entityManager->createQuery($dqlElementoRequerimientosGuardados)->setParameter('idRequerimiento',$requerimiento->getIdRequerimiento());
                            $elementoRequerimientosGuardados= $queryElementoRequerimientosGuardados->getResult();
                            $template = $this->render('elementoRequerimiento/datosEntradaRequerimiento.html.twig', [
                            'elementoRequerimientos'=>$elementoRequerimientosGuardados
                            ])->getContent();
                            return new JsonResponse(['mensaje'=> 'Se ha realizado el registro de forma correcta!','elementoRequerimientos'=>$template]); 
                        }else{
                            $dqlElementoRequerimientosGuardados ="SELECT eRequerimiento FROM App:ElementoRequerimiento eRequerimiento WHERE eRequerimiento.idRequerimiento= :idRequerimiento ORDER BY eRequerimiento.idElementoRequerimiento DESC";  
                            $queryElementoRequerimientosGuardados = $entityManager->createQuery($dqlElementoRequerimientosGuardados)->setParameter('idRequerimiento',$requerimiento->getIdRequerimiento());
                            $elementoRequerimientosGuardados= $queryElementoRequerimientosGuardados->getResult();
                            $template = $this->render('elementoRequerimiento/datosEntradaRequerimiento.html.twig', [
                            'elementoRequerimientos'=>$elementoRequerimientosGuardados
                            ])->getContent();
                            return new JsonResponse(['error'=> 'No se ha realizado el registro; el estado del requerimiento no permite nuevos elementos!','elementoRequerimientos'=>$template]);   
                        } 
                    }

                    $dqlProveedores ="SELECT proveedor FROM App:Proveedor proveedor WHERE proveedor.estado= :estado";  
                    $queryProveedor = $entityManager->createQuery($dqlProveedores)->setParameter('estado',TRUE);
                    $proveedores= $queryProveedor->getResult();


                   
                    return $this->render('elementoRequerimiento/index.html.twig', [
                        'elementoRequerimientos' => $elementoRequerimientos,
                        'elementoRequerimiento' => $elementoRequerimiento,
                        'requerimiento'=>$requerimiento,
                        'form'=>$form->createView(),
                        'proveedores'=>$proveedores
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la edición de un elemento requerido solo en el caso que se encuentre en estado pendiente.
     * @Route("/{idElementoRequerimiento}/edit", name="elementoRequerimiento_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ElementoRequerimiento $elementoRequerimiento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes requerimientos');  
                if (sizeof($permisos) > 0) {  
                    if ($elementoRequerimiento->getEstado() == 'PENDIENTE') {                  
                    $entityManager = $this->getDoctrine()->getManager();
                    $form = $this->createForm(ElementoRequerimientoType::class, $elementoRequerimiento);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($elementoRequerimiento);
                        $entityManager->flush();                       
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                       return $this->redirectToRoute('elementoRequerimiento_index',['idRequerimiento'=>$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento()]);
                         
                    }
                    return $this->render('elementoRequerimiento/modalEdit.html.twig', [
                        'elementoRequerimiento' => $elementoRequerimiento,
                        'form' => $form->createView()
                    ]);
                    }else{
                    $this->addFlash('error', 'No se puede realizar cambios a este elemento, el estado cambio!');
                    return $this->redirectToRoute('elementoRequerimiento_index',['idRequerimiento'=>$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento()]);
                    } 
                
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de un elemento requerido.
     * @Route("/{idElementoRequerimiento}", name="elementoRequerimiento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ElementoRequerimiento $elementoRequerimiento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes requerimientos');  
              if (sizeof($permisos) > 0) {
                //var_dump($estadoMantenimiento->getIdEstadoMantenimiento());
                try{
                    $idRequerimiento=$elementoRequerimiento->getIdRequerimiento()->getIdRequerimiento();
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($elementoRequerimiento);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('elementoRequerimiento_index', [
                            'idRequerimiento' => $idRequerimiento ,
                        ]);
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('elementoRequerimiento_index', [
                            'idRequerimiento' => $idRequerimiento ,
                            ]);
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('elementoRequerimiento_index', [
                    'idRequerimiento' => $idRequerimiento ,
                 ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }
}
