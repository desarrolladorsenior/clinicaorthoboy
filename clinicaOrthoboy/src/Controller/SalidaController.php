<?php

namespace App\Controller;

use App\Entity\Salida;
use App\Form\SalidaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

/**
 * Controlador de salida utilizado para el manejo del entradas de un pedido a un lugar espcifico de la clinica es decir una sede mostrando la localización y 
 * permitiendo generar salidas a un paciente o consultorio.
 *
 * @Route("/salida")
 */
class SalidaController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de todas las salidas de un pedido de una sede de la clinica o de toda la clinica dependiendo del perfil 
     * que se este manejando. Además permite el ingreso de salidas segun la clinica muestra las entredas de pedidos y los consultorios pertenecientes 
     * a la sede.
     *
     * @Route("/", name="salida_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Salidas de pedido clinica');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                   $arrayEntradas=array();
                   $arrayConsultorios=array();
                    $sedeClinica=$this->get('security.token_storage')->getToken()->getUser()->getIdSucursal()->getIdClinica();
                    $nombrePerfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil()->getNombre();

                    $dqlConteoDisponibles= "SELECT sum(sc.cantidad) as conteoCantidad, s.idSalida
                         FROM App:SalidaConsultorio sc
                         LEFT JOIN App:Salida s WITH s.idSalida = sc.idSalida
                         GROUP BY s.idSalida ORDER BY s.idSalida DESC";
                    $queryConteoDisponibles=$entityManager->createQuery($dqlConteoDisponibles);
                    $conteoDisponibles= $queryConteoDisponibles->getResult();

                    
                    if ($nombrePerfil != 'ROLE_SUPERADMINISTRADOR') {
                        $dqlSalida ="SELECT salida FROM App:Salida salida INNER JOIN App:EntradaPedido entradaPedido WITH entradaPedido.idEntradaPedido=salida.idEntradaPedido INNER JOIN App:Clinica clinica WITH clinica.idClinica=entradaPedido.idClinicaUbicacion WHERE clinica.idClinica = :idClinica
                        ORDER BY salida.idSalida DESC";  
                        $querySalida = $entityManager->createQuery($dqlSalida)->setParameter('idClinica',$sedeClinica);
                        $salidas= $querySalida->getResult();

                        //Datos de entradas de una sede 
                        $dqlEntradasSede ="SELECT ePedido FROM App:EntradaPedido ePedido INNER JOIN App:Clinica clinica WITH clinica.idClinica=ePedido.idClinicaUbicacion WHERE clinica.idClinica = :idClinica and ePedido.estado = :estado";  
                        $queryEntradasSede = $entityManager->createQuery($dqlEntradasSede)->setParameter('idClinica',$sedeClinica)->setParameter('estado',TRUE);
                        $entradas= $queryEntradasSede->getResult();
                        
                        foreach ($entradas as $seleccionEntradas ) {
                            array_push($arrayEntradas, $seleccionEntradas->getIdEntradaPedido());
                        }

                        //Datos de consultorio de una sede 
                        $dqlConsultorios ="SELECT consultorio FROM App:Consultorio consultorio INNER JOIN App:Clinica clinica WITH clinica.idClinica=consultorio.idClinica WHERE clinica.idClinica = :idClinica and consultorio.estado = :estado";  
                        $queryConsultorios = $entityManager->createQuery($dqlConsultorios)->setParameter('idClinica',$sedeClinica)->setParameter('estado',TRUE);
                        $consultorios= $queryConsultorios->getResult();
                        
                        foreach ($consultorios as $seleccionConsultorio ) {
                            array_push($arrayConsultorios, $seleccionConsultorio->getIdConsultorio());
                        }

                    }else{
                        $dqlSalida ="SELECT salida FROM App:Salida salida ORDER BY salida.idSalida DESC";  
                        $querySalida = $entityManager->createQuery($dqlSalida);
                        $salidas= $querySalida->getResult();

                        //Datos de entradas generales  
                        $dqlEntradas ="SELECT ePedido FROM App:EntradaPedido ePedido WHERE ePedido.estado = :estado";  
                        $queryEntradas = $entityManager->createQuery($dqlEntradas)->setParameter('estado',TRUE);
                        $entradas= $queryEntradas->getResult();
                        foreach ($entradas as $seleccionEntradas ) {
                            array_push($arrayEntradas, $seleccionEntradas->getIdEntradaPedido());
                        }


                        //Datos de consultorio de una sede 
                        $dqlConsultorios ="SELECT consultorio FROM App:Consultorio consultorio WHERE consultorio.estado = :estado";  
                        $queryConsultorios = $entityManager->createQuery($dqlConsultorios)->setParameter('estado',TRUE);
                        $consultorios= $queryConsultorios->getResult();
                        foreach ($consultorios as $seleccionConsultorio ) {
                            array_push($arrayConsultorios, $seleccionConsultorio->getIdConsultorio());
                        }
                    }
                    $salida = new Salida();
                    $salida->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(SalidaType::class,$salida);
                    if (sizeof($arrayEntradas) > 0) {
                       $form->add('idEntradaPedido',EntityType::class,array(
                        'placeholder' => 'Seleccione elemento','class'=>'App:EntradaPedido',
                        'attr'=>array('required'=>'required'),
                        'query_builder' => function(EntityRepository $er)use ($arrayEntradas)
                            {
                                return $er->createQueryBuilder('e')
                                ->where('e.estado = :estado')
                                ->andWhere('e.idEntradaPedido in (:idEntradaPedido)')
                                ->setParameter('estado',TRUE)
                                ->setParameter('idEntradaPedido',$arrayEntradas);
                            }
                        ));
                       $form->add('idConsultorio',EntityType::class,array(
                        'placeholder' => 'Seleccione consultorio','class'=>'App:Consultorio',
                        'attr'=>array('required'=>'required'),
                        'query_builder' => function(EntityRepository $er)use ($arrayConsultorios)
                            {
                                return $er->createQueryBuilder('e')
                                ->where('e.estado = :estado')
                                ->andWhere('e.idConsultorio in (:idConsultorio)')
                                ->setParameter('estado',TRUE)
                                ->setParameter('idConsultorio',$arrayConsultorios);
                            }
                        ));
                    }  
                    
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {

                        $dqlCantidadSalida ="SELECT sum(salida.cantidad) as sumaCantidad FROM App:Salida salida WHERE salida.idEntradaPedido= :idEntradaPedido";  
                        $queryCantidadSalida = $entityManager->createQuery($dqlCantidadSalida)->setParameter('idEntradaPedido',$salida->getIdEntradaPedido()->getIdEntradaPedido());
                        $cantidadSalida= $queryCantidadSalida->getResult();
                        foreach ($cantidadSalida as $seleccionSalida) {
                            $cantidadTotalSalida=$seleccionSalida['sumaCantidad'];
                        }
                        if ($cantidadTotalSalida != '' ){
                            $cantidadDisponibleActual=$salida->getIdEntradaPedido()->getCantidad()-$cantidadTotalSalida;
                            if( $cantidadDisponibleActual == 0) {
                                $this->addFlash('error', 'No se puede generar la salida no tiene cantidades disponibles el elemento.');
                                return $this->redirectToRoute('salida_index'); 
                            }else{
                                if ($salida->getCantidad() == 0) {
                                    $this->addFlash('error', 'No se puede registro una la salida con cantidad igual a cero.');
                                    return $this->redirectToRoute('salida_index'); 
                                }else{
                                    if( $salida->getCantidad() <= $cantidadDisponibleActual) {
                                       $entityManager->persist($salida);
                                        $entityManager->flush();                        
                                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                        return $this->redirectToRoute('salida_index');
                                    }else{
                                        $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                        return $this->redirectToRoute('salida_index'); 
                                    }
                                }
                            }
                        }else{
                            if ($salida->getCantidad() == 0) {
                                $this->addFlash('error', 'No se puede registro una la salida con cantidad igual a cero.');
                                return $this->redirectToRoute('salida_index'); 
                            }else{
                                if( $salida->getCantidad() <= $salida->getIdEntradaPedido()->getCantidad()) {
                                   $entityManager->persist($salida);
                                    $entityManager->flush();                        
                                    $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                    return $this->redirectToRoute('salida_index');
                                }else{
                                    $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                    return $this->redirectToRoute('salida_index'); 
                                }
                            }
                        }
                        
                    }
                    return $this->render('salida/index.html.twig', [
                        'salidas' => $salidas,
                        'salida' => $salida,
                        'form' => $form->createView(),
                        'conteoDisponibles'=>$conteoDisponibles,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la edición de las salidas que se les den a un paciente o  consultorio y que valida que las cantidades a dar salida del 
     * pedido esten disponibles o que el cambio de las cantiddades de salida sean diferentes de cero.
     *
     * @Route("/{idSalida}/edit", name="salida_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Salida $salida,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Salidas de pedido clinica');  
                if (sizeof($permisos) > 0) {                    
                    $entityManager = $this->getDoctrine()->getManager();
                    /*emtradaActual actual*/
                    $entradaPedidoGuardado=$salida->getIdEntradaPedido()->getIdEntradaPedido();
                    /*Cantidad actual*/
                    $cantidadGuardado=$salida->getCantidad();                    
                    

                    /*$form = $this->createForm(SalidaType::class, $salida);
                    $form->handleRequest($request);*/

                    //Datos utilizados para permitir al usuario unicamente ver los datos de acceso a su perfil.
                    $arrayEntradas=array();
                    $arrayConsultorios=array();
                    $sedeClinica=$this->get('security.token_storage')->getToken()->getUser()->getIdSucursal()->getIdClinica();
                    $nombrePerfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil()->getNombre();
                    
                    if ($nombrePerfil != 'ROLE_SUPERADMINISTRADOR') {

                        //Datos de entradas de una sede 
                        $dqlEntradasSede ="SELECT ePedido FROM App:EntradaPedido ePedido INNER JOIN App:Clinica clinica WITH clinica.idClinica=entradaPedido.idClinicaUbicacion WHERE clinica.idClinica = :idClinica and ePedido.estado = :estado";  
                        $queryEntradasSede = $entityManager->createQuery($dqlEntradasSede)->setParameter('idClinica',$sedeClinica)->setParameter('estado',TRUE);
                        $entradas= $queryEntradasSede->getResult();
                        
                        foreach ($entradas as $seleccionEntradas ) {
                            array_push($arrayEntradas, $seleccionEntradas->getIdEntradaPedido());
                        }

                        //Datos de consultorio de una sede 
                        $dqlConsultorios ="SELECT consultorio FROM App:Consultorio consultorio INNER JOIN App:Clinica clinica WITH clinica.idClinica=consultorio.idClinica WHERE clinica.idClinica = :idClinica and consultorio.estado = :estado";  
                        $queryConsultorios = $entityManager->createQuery($dqlConsultorios)->setParameter('idClinica',$sedeClinica)->setParameter('estado',TRUE);
                        $consultorios= $queryConsultorios->getResult();
                        
                        foreach ($consultorios as $seleccionConsultorio ) {
                            array_push($arrayConsultorios, $seleccionConsultorio->getIdConsultorio());
                        }

                    }else{
                        //Datos de entradas generales  
                        $dqlEntradas ="SELECT ePedido FROM App:EntradaPedido ePedido WHERE ePedido.estado = :estado";  
                        $queryEntradas = $entityManager->createQuery($dqlEntradas)->setParameter('estado',TRUE);
                        $entradas= $queryEntradas->getResult();
                        foreach ($entradas as $seleccionEntradas ) {
                            array_push($arrayEntradas, $seleccionEntradas->getIdEntradaPedido());
                        }


                        //Datos de consultorio de una sede 
                        $dqlConsultorios ="SELECT consultorio FROM App:Consultorio consultorio WHERE consultorio.estado = :estado";  
                        $queryConsultorios = $entityManager->createQuery($dqlConsultorios)->setParameter('estado',TRUE);
                        $consultorios= $queryConsultorios->getResult();
                        foreach ($consultorios as $seleccionConsultorio ) {
                            array_push($arrayConsultorios, $seleccionConsultorio->getIdConsultorio());
                        }
                    }
                    //$salida->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(SalidaType::class,$salida);
                    if (sizeof($arrayEntradas) > 0) {
                       $form->add('idEntradaPedido',EntityType::class,array(
                        'placeholder' => 'Seleccione elemento','class'=>'App:EntradaPedido',
                        'attr'=>array('required'=>'required'),
                        'query_builder' => function(EntityRepository $er)use ($arrayEntradas)
                            {
                                return $er->createQueryBuilder('e')
                                ->where('e.estado = :estado')
                                ->andWhere('e.idEntradaPedido in (:idEntradaPedido)')
                                ->setParameter('estado',TRUE)
                                ->setParameter('idEntradaPedido',$arrayEntradas);
                            }
                        ));
                       $form->add('idConsultorio',EntityType::class,array(
                        'placeholder' => 'Seleccione consultorio','class'=>'App:Consultorio',
                        'attr'=>array('required'=>'required'),
                        'query_builder' => function(EntityRepository $er)use ($arrayConsultorios)
                            {
                                return $er->createQueryBuilder('e')
                                ->where('e.estado = :estado')
                                ->andWhere('e.idConsultorio in (:idConsultorio)')
                                ->setParameter('estado',TRUE)
                                ->setParameter('idConsultorio',$arrayConsultorios);
                            }
                        ));
                    }  
                    
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entradaPedidoEditado=$salida->getIdEntradaPedido()->getIdEntradaPedido();
                        $cantidadEditado=$salida->getCantidad();
                        if ($entradaPedidoEditado != $entradaPedidoGuardado ) {
                            $dqlCantidadSalidaConsultorio ="SELECT count(salidaC) as conteoSalida FROM App:SalidaConsultorio salidaC WHERE salidaC.idSalida= :idSalida";  
                            $queryCantidadSalidaConsultorio = $entityManager->createQuery($dqlCantidadSalidaConsultorio)->setParameter('idSalida',$salida->getIdSalida());
                            $cantidadSalidaConsultorio= $queryCantidadSalidaConsultorio->getResult();
                            foreach ($cantidadSalidaConsultorio as $seleccionSalida) {
                                    $conteoSalida=$seleccionSalida['conteoSalida'];
                                }
                            if ($conteoSalida == 0) {
                                $dqlCantidadSalida ="SELECT sum(salida.cantidad) as sumaCantidad FROM App:Salida salida WHERE salida.idEntradaPedido= :idEntradaPedido";  
                                $queryCantidadSalida = $entityManager->createQuery($dqlCantidadSalida)->setParameter('idEntradaPedido',$entradaPedidoEditado);
                                $cantidadSalida= $queryCantidadSalida->getResult();
                                foreach ($cantidadSalida as $seleccionSalida) {
                                    $cantidadTotalSalida=$seleccionSalida['sumaCantidad'];
                                }

                                if ($cantidadTotalSalida != '' ){
                                    $cantidadDisponibleActual=$salida->getIdEntradaPedido()->getCantidad()-$cantidadTotalSalida;
                                    if( $cantidadDisponibleActual == 0) {
                                        $this->addFlash('error', 'No se puede generar la salida no tiene cantidades disponibles el elemento.');
                                        return $this->redirectToRoute('salida_index'); 
                                    }else{
                                        if ($salida->getCantidad() == 0) {
                                            $this->addFlash('error', 'No se puede actualizar la salida a una cantidad igual a cero.');
                                            return $this->redirectToRoute('salida_index'); 
                                        }else{
                                            if( $salida->getCantidad() <= $cantidadDisponibleActual) {
                                               $entityManager->persist($salida);
                                                $entityManager->flush();                        
                                                $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                                return $this->redirectToRoute('salida_index');
                                            }else{
                                                $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                                return $this->redirectToRoute('salida_index'); 
                                            }
                                        }
                                    }
                                }else{
                                    if ($salida->getCantidad() == 0) {
                                        $this->addFlash('error', 'No se puede actualizar la salida a una cantidad igual a cero.');
                                        return $this->redirectToRoute('salida_index'); 
                                    }else{
                                        if( $cantidadEditado <= $salida->getIdEntradaPedido()->getCantidad()) {
                                           $entityManager->persist($salida);
                                            $entityManager->flush();                        
                                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                            return $this->redirectToRoute('salida_index');
                                        }else{
                                            $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                            return $this->redirectToRoute('salida_index'); 
                                        }
                                    }
                                }
                            }else{
                                $this->addFlash('error', 'No se puede generar el cambio de elemento usted tiene salidas a consultorios.');
                                return $this->redirectToRoute('salida_index');
                            }
                        }else{
                            $dqlCantidadSalida ="SELECT sum(salida.cantidad) as sumaCantidad FROM App:Salida salida WHERE salida.idEntradaPedido= :idEntradaPedido and salida.idSalida != :idSalida";  
                            $queryCantidadSalida = $entityManager->createQuery($dqlCantidadSalida)->setParameter('idEntradaPedido',$entradaPedidoEditado)->setParameter('idSalida',$salida->getIdSalida());
                            $cantidadSalida= $queryCantidadSalida->getResult();
                            foreach ($cantidadSalida as $seleccionSalida) {
                                $cantidadTotalSalida=$seleccionSalida['sumaCantidad'];
                            }
                            if ($cantidadGuardado != $cantidadEditado) {
                                if ($cantidadTotalSalida != '' ){
                                    $cantidadDisponibleActual=$salida->getIdEntradaPedido()->getCantidad()-$cantidadTotalSalida;
                                    if( $cantidadDisponibleActual == 0) {
                                        $this->addFlash('error', 'No se puede generar la salida no tiene cantidades disponibles el elemento.');
                                        return $this->redirectToRoute('salida_index'); 
                                    }else{
                                        if ($salida->getCantidad() == 0) {
                                            $this->addFlash('error', 'No se puede actualizar la salida a una cantidad igual a cero.');
                                            return $this->redirectToRoute('salida_index'); 
                                        }else{
                                            if( $cantidadEditado <= $cantidadDisponibleActual) {
                                               $entityManager->persist($salida);
                                                $entityManager->flush();                        
                                                $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                                return $this->redirectToRoute('salida_index');
                                            }else{
                                                $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                                return $this->redirectToRoute('salida_index'); 
                                            }
                                        }
                                    }
                                }else{
                                    if ($salida->getCantidad() == 0) {
                                        $this->addFlash('error', 'No se puede actualizar la salida a una cantidad igual a cero.');
                                        return $this->redirectToRoute('salida_index'); 
                                    }else{
                                        if( $cantidadEditado <= $salida->getIdEntradaPedido()->getCantidad()) {
                                           $entityManager->persist($salida);
                                            $entityManager->flush();                        
                                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                                            return $this->redirectToRoute('salida_index');
                                        }else{
                                            $this->addFlash('error', 'No se puede generar la salida la cantidad disponible es: '.$cantidadDisponibleActual.'.');
                                            return $this->redirectToRoute('salida_index'); 
                                        }
                                    }
                                }
                            }else{
                                if ($salida->getCantidad() == 0) {
                                    $this->addFlash('error', 'No se puede actualizar la salida a una cantidad igual a cero.');
                                    return $this->redirectToRoute('salida_index'); 
                                }else{
                                    $entityManager->persist($salida);
                                    $entityManager->flush();                        
                                    $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');                                
                                    return $this->redirectToRoute('salida_index'); 
                                }
                            }
                        }
                    }
                    return $this->render('salida/modalEdit.html.twig', [
                        'salida' => $salida,
                        'form' => $form->createView()
                    ]);
                
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de una salida.
     *
     * @Route("/{idSalida}", name="salida_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Salida $salida,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'Salidas de pedido clinica');
              if (sizeof($permisos) > 0) {
                //var_dump($estadoMantenimiento->getIdEstadoMantenimiento());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($salida);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('salida_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('salida_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
               return $this->redirectToRoute('salida_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }
}
