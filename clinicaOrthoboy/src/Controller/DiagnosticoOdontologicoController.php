<?php

namespace App\Controller;

use App\Entity\DiagnosticoOdontologico;
use App\Form\DiagnosticoOdontologicoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de diagnostico odontologico utilizado para los diagnosticos molares dentro de la profesión de odontologia lo cual, indica que estos seran utilizados en 
 * el proceso de odontograma.
 *
 * @Route("/diagnosticoOdontologico")
 */
class DiagnosticoOdontologicoController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de un diagnostico odontologico y registro del mismo.
     *
     * @Route("/", name="diagnosticoOdontologico_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'diagnosticos odontologicos');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlDiagnosticoOdontologico ="SELECT diagnosticoOdontologico FROM App:DiagnosticoOdontologico diagnosticoOdontologico
                    ORDER BY diagnosticoOdontologico.idDiagnosticoOdontologico DESC";  
                    $queryDiagnosticoOdontologico = $entityManager->createQuery($dqlDiagnosticoOdontologico);
                    $diagnosticosOdontologicos= $queryDiagnosticoOdontologico->getResult();
                    
                    $diagnosticoOdontologico = new DiagnosticoOdontologico();
                    $form = $this->createForm(DiagnosticoOdontologicoType::class, $diagnosticoOdontologico);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $diagnosticoOdontologico->setEstado(true);
                        $entityManager->persist($diagnosticoOdontologico);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('diagnosticoOdontologico_index');
                    }
                    return $this->render('diagnosticoOdontologico/index.html.twig', [
                        'diagnosticosOdontologicos' => $diagnosticosOdontologicos,
                        'diagnosticoOdontologico' => $diagnosticoOdontologico,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }


    /**
     * Metodo utilizado para la edición de estado de un diagnostico odontologico.
     *
     * @Route("/editarEstadoDiagnosticoOdontologico", name="diagnosticoOdontologico_estadoCambio", methods={"GET","POST"})
     */
    public function editEstadoDiagnosticoOdontologico(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'diagnosticos odontologicos');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $em=$this->getDoctrine()->getManager();
                $response = new JsonResponse();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }
                $dqlModificarEstado="UPDATE App:DiagnosticoOdontologico diagnosticoOdontologico SET diagnosticoOdontologico.estado= :estado WHERE diagnosticoOdontologico.idDiagnosticoOdontologico= :idDiagnosticoOdontologico";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idDiagnosticoOdontologico', $id);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    

    }

    /**
     * Metodo utilizado para la edición de un diagnostico odontologico.
     *
     * @Route("/{idDiagnosticoOdontologico}/edit", name="diagnosticoOdontologico_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DiagnosticoOdontologico $diagnosticoOdontologico,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'diagnosticos odontologicos');  
                if (sizeof($permisos) > 0) {
                    $form = $this->createForm(DiagnosticoOdontologicoType::class, $diagnosticoOdontologico);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('diagnosticoOdontologico_index');
                    }
                    return $this->render('diagnosticoOdontologico/modalEdit.html.twig', [
                        'diagnosticoOdontologico' => $diagnosticoOdontologico,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de un diagnostico odontologico.
     *
     * @Route("/{idDiagnosticoOdontologico}", name="diagnosticoOdontologico_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DiagnosticoOdontologico $diagnosticoOdontologico,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'diagnosticos odontologicos');  
              if (sizeof($permisos) > 0) {
                try{

                    if ($this->isCsrfTokenValid('delete'.$diagnosticoOdontologico->getIdDiagnosticoOdontologico(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($diagnosticoOdontologico);
                        $entityManager->flush();
                        $successMessage= 'Se ha eliminado de forma correcta!';
                        $this->addFlash('mensaje',$successMessage);
                        return $this->redirectToRoute('diagnosticoOdontologico_index');
                    }

                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('diagnosticoOdontologico_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('diagnosticoOdontologico_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }    
    }
}
