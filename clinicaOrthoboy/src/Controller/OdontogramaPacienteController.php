<?php

namespace App\Controller;
use App\Entity\Odontograma;
use App\Entity\OdontogramaPaciente;
use App\Entity\Paciente;
use App\Form\OdontogramaPacienteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de odontograma Paciente utilizado para el manejo  de ingresos de nuevos diagnosticos de superficie o pieza completa de un odontograma.
 *
 * @Route("/odontogramaPaciente")
 */
class OdontogramaPacienteController extends AbstractController
{

    /**
     * Metodo utilizado para la visualización de un odontograma Paciente.
     *
     * @Route("/listadoOdontograma/{idPaciente}", name="odontogramaPaciente_listado", methods={"GET","POST"})
     */
    public function listadoOdontogramas(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'odontograma');  
                if (sizeof($permisos) > 0) { 

                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlOdontograma ="SELECT odonto FROM App:Odontograma odonto WHERE odonto.idPaciente = :idPaciente ORDER BY odonto.idOdontograma ASC";  
                    $queryOdontograma = $entityManager->createQuery($dqlOdontograma)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $odontogramas= $queryOdontograma->getResult();
                    return $this->render('odontogramaPaciente/index.html.twig', [
                        'odontogramas' => $odontogramas,
                        'paciente'=>$paciente
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }


    /**
     * Metodo utilizado para la visualización de un odontograma paciente.
     *
     * @Route("/{idPaciente}/{idOdontograma}/consultaOdontograma", name="odontogramaPaciente_consultaInicial", methods={"GET","POST"})
     */
    public function consultaOdontogramas(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'odontograma');  
                if (sizeof($permisos) > 0) { 

                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlOdontogramaPaciente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente INNER JOIN App:PiezaDental pd WITH pd.idPiezaDental=odontoPaciente.idPieza WHERE odontoPaciente.idOdontograma = :idOdontograma ORDER BY pd.codigoPieza ASC";  
                    $queryOdontogramaPaciente = $entityManager->createQuery($dqlOdontogramaPaciente)->setParameter('idOdontograma',$request->get('idOdontograma'));
                    $odontoPacientes= $queryOdontogramaPaciente->getResult();
                    return $this->render('odontogramaPaciente/show.html.twig', [
                        'odontoPacientes' => $odontoPacientes,
                        'paciente'=>$paciente
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }


     /**
     * Metodo utilizado para la duplicación de un odontograma paciente y registro del mismo.
     *
     * @Route("/{idOdontograma}/automaticoOdontograma", name="odontogramaPaciente_duplicacion", methods={"GET","POST"})
     */
    public function duplicacionOdontograma(Request $request,PerfilGenerator $permisoPerfil,Odontograma $odonto): Response
    {
        Ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'odontograma');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $idOdontograma=$request->get('idOdontograma');                    
                    $idPaciente=$odonto->getIdPaciente()->getIdPaciente();
                    $dqlOdontogramasPaciente="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente INNER JOIN App:Odontograma odonto WITH odonto.idOdontograma=odontoPaciente.idOdontograma WHERE odontoPaciente.idOdontograma= :idOdontograma and odonto.numeroOdontograma = :numeroOdontograma";
                    $queryOdontogramasOdontograma=$entityManager->createQuery($dqlOdontogramasPaciente)->setParameter('idOdontograma', $idOdontograma)->setParameter('numeroOdontograma',1);
                    $odontogramaPacientesIniciales=$queryOdontogramasOdontograma->getResult();

                    if (sizeof($odontogramaPacientesIniciales) > 0){
                        foreach ($odontogramaPacientesIniciales as $odontogramaPacienteSeleccionado) {
                            $odontogramaPaciente= new OdontogramaPaciente();
                            $dqlOdontogramas="SELECT odonto FROM App:Odontograma odonto WHERE odonto.idPaciente= :idPaciente and odonto.numeroOdontograma = :numeroOdontograma";
                            $queryOdontogramas=$entityManager->createQuery($dqlOdontogramas)->setParameter('idPaciente', $odonto->getIdPaciente())->setParameter('numeroOdontograma',2);
                            $odontogramas=$queryOdontogramas->getResult();
                            if (sizeof($odontogramas) > 0) {
                                foreach ($odontogramas as $odontogramaSeleccionado) {
                                    $odontogramaPaciente->setIdOdontograma($odontogramaSeleccionado);
                                }
                            }else{
                                $odontograma= new Odontograma();
                                $odontograma->setIdPaciente($odonto->getIdPaciente());
                                $odontograma->setNumeroOdontograma(2);
                                $entityManager->persist($odontograma);
                                $entityManager->flush();
                                $odontogramaPaciente->setIdOdontograma($odontograma);
                            }
                                      
                            $odontogramaPaciente->setIdUsuarioProfesional($this->get('security.token_storage')->getToken()->getUser());
                                
                            $odontogramaPaciente->setIdPieza($odontogramaPacienteSeleccionado->getIdPieza());
                            $odontogramaPaciente->setIdRipTipoOdon($odontogramaPacienteSeleccionado->getIdRipTipoOdon());
                            $odontogramaPaciente->setIdDiagPrincipal($odontogramaPacienteSeleccionado->getIdDiagPrincipal());
                            $odontogramaPaciente->setIdSuperficie($odontogramaPacienteSeleccionado->getIdSuperficie());
                            $entityManager->persist($odontogramaPaciente);
                            $entityManager->flush();                                   
                        }
                        $this->addFlash('mensaje', 'Se ha terminado el odontograma inicial correctamente!');
                        return $this->redirectToRoute('odontogramaPaciente_new', [
                            'idPaciente' => $odontogramaPaciente->getIdOdontograma()->getIdPaciente()->getIdPaciente(),
                        ]);                                     
                    }else{
                       $this->addFlash('error', 'Usted no cuenta con información de diagnósticos eb el odontogrma verique por favor paraq poder continuar!');
                        return $this->redirectToRoute('odontogramaPaciente_new', [
                            'idPaciente' => $idPaciente,
                        ]);   
                    }                    
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }
    /**
     * Metodo utilizado para la visualización de los diagnosticos actuales de una pieza y la superficie que se ha tomado de un osontograma 
     * especifico.
     *
     * @Route("/datoSuperficie", name="odontogramaPaciente_superficie", methods={"GET","POST"})
     */
    public function datoSuperficie(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        Ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'odontograma');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $idOdontograma=$request->request->get('idOdontograma');
                    $idSuperficie=$request->request->get('idSuperficie');
                    $idPieza=$request->request->get('idPieza');

                    $odontogramaPacientes = $this->getDoctrine()
                        ->getRepository(OdontogramaPaciente::class)
                        ->findAll();
                    $dqlOdontogramasPaciente="SELECT odontograma FROM App:OdontogramaPaciente odontograma WHERE odontograma.idOdontograma= :idOdontograma and odontograma.idSuperficie = :idSuperficie and odontograma.idPieza = :idPieza";
                    $queryOdontogramasOdontograma=$entityManager->createQuery($dqlOdontogramasPaciente)->setParameter('idOdontograma', $idOdontograma)->setParameter('idSuperficie',$idSuperficie)->setParameter('idPieza',$idPieza); 
                    $ultimoOdontogramaPaciente=$queryOdontogramasOdontograma->execute();
                    if (sizeof($ultimoOdontogramaPaciente) > 0){
                        foreach ($ultimoOdontogramaPaciente as $odontogramaSeleccionado) {
                            $diagnostico=$odontogramaSeleccionado->getIdRipTipoOdon()->getIdCieDiagnosticoTipo();
                            $tipo=$odontogramaSeleccionado->getIdDiagPrincipal()->getIdDiagnosticoPrincipal();
                        }
                        return new JsonResponse(['diagnostico'=>$diagnostico,'tipo'=>$tipo]);
                    }else{
                        return new JsonResponse(['diagnostico'=>0,'tipo'=>0]);
                    }
                    
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para el registro de un odontograma en caso que el odontograma inicial para el paciente no exista, además permite el ingreso
     * de nuevas vinculaciones de diagnosticos a una pieza especifica y superficie y/o pieza completa. Dentro del metodo se vinculan la exposición de 
     * datos sobre las piezas dejando en visualización los diagnosticos de las superficies. Se verifica que el momento de la exposición las pieza 
     * manejadas no tengan diagnosticos en un pieza completa y si los llegan a tener se visualiza un mensaje de advertencia el cual indica que no se podran ingresar 
     * diagnosticos a otras superficices de la pieza si se tiene un diagnostico en una pieza completa. Finalmente se envian los datos nuevamente de como queda el 
     * odontograma en concreto y como se manejan cada una de las piezas actualmente.
     *
     * @Route("/new/{idPaciente}", name="odontogramaPaciente_new", methods={"GET","POST"})
     */
    public function new(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {

        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'odontograma');  
                if (sizeof($permisos) > 0) { 

                    $entityManager = $this->getDoctrine()->getManager();
                    $odontogramaPaciente = new OdontogramaPaciente();
                    $dqlOdontogramasExistentes="SELECT odontograma FROM App:Odontograma odontograma WHERE odontograma.idPaciente= :idPaciente order by odontograma.idOdontograma desc";
                    $queryOdontogramasExistentes=$entityManager->createQuery($dqlOdontogramasExistentes)->setParameter('idPaciente', $paciente->getIdPaciente())->setMaxResults(1); 
                    $ultimoOdontogramaPaciente=$queryOdontogramasExistentes->execute();

                    if (sizeof($ultimoOdontogramaPaciente) > 0) {
                        foreach ($ultimoOdontogramaPaciente as $odontogramaSeleccionado) {
                            $odontogramaPaciente->setIdOdontograma($odontogramaSeleccionado);
                        }
                    }else{
                        $odontograma= new Odontograma();
                        $odontograma->setIdPaciente($paciente);
                        $odontograma->setNumeroOdontograma(1);
                        $entityManager->persist($odontograma);
                        $entityManager->flush();
                        $odontogramaPaciente->setIdOdontograma($odontograma);
                    }
                    
                    $odontogramaPaciente->setIdUsuarioProfesional($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(OdontogramaPacienteType::class, $odontogramaPaciente);
                    $form->handleRequest($request);
                    
                    if ($form->isSubmitted() && $form->isValid()) {
                        
                        $dqlPiezaCompleta ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma and odontoPaciente.idSuperficie = :idSuperficie ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                        $queryPiezaCompleta = $entityManager->createQuery($dqlPiezaCompleta)->setParameter('idPieza',$odontogramaPaciente->getIdPieza())->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma())->setParameter('idSuperficie',8);
                        $piezaCompleta= $queryPiezaCompleta->getResult();
                        if (sizeof($piezaCompleta) > 0) {
                            if ($odontogramaPaciente->getIdSuperficie()->getIdSuperficie() == 8) {
                                foreach ($piezaCompleta as $odontoPacienteSeleccionado) {
                                   $id= $odontoPacienteSeleccionado->getIdOdontogramaPaciente();
                                }
                                $cambiOdontogramaPaciente =  $this->getDoctrine()->getRepository(OdontogramaPaciente::class)->find($id);

                                $cambiOdontogramaPaciente->setIdRipTipoOdon($odontogramaPaciente->getIdRipTipoOdon());
                                $cambiOdontogramaPaciente->setIdDiagPrincipal($odontogramaPaciente->getIdDiagPrincipal());
                                $cambiOdontogramaPaciente->setIdUsuarioProfesional($odontogramaPaciente->getIdUsuarioProfesional());
                                $entityManager->persist($cambiOdontogramaPaciente);
                                $entityManager->flush();

                                /**Llamado de partes de un aodontograma en odontograma con el odonto paciente **/
                                $dqlOdontogramaPaciente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idPieza ASC";  
                                $queryOdontogramaPaciente = $entityManager->createQuery($dqlOdontogramaPaciente)->setParameter('idOdontograma',$cambiOdontogramaPaciente->getIdOdontograma());
                                $odontoPacientes= $queryOdontogramaPaciente->getResult();
                                
                                $templateOdontoPaciente = $this->render('odontogramaPaciente/cargaOdontoPaciente.html.twig', [
                                    'odontoPacientes'=>$odontoPacientes
                                    ])->getContent();

                                $dqlPiezaDental ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                                $queryPiezaDental = $entityManager->createQuery($dqlPiezaDental)->setParameter('idPieza',$cambiOdontogramaPaciente->getIdPieza())->setParameter('idOdontograma',$cambiOdontogramaPaciente->getIdOdontograma());
                                $piezaDentalOdontograma= $queryPiezaDental->getResult();
                                $template = $this->render('odontogramaPaciente/datosPiezaDental.html.twig', [
                                'piezaDentalOdontograma'=>$piezaDentalOdontograma
                                ])->getContent();


                                return new JsonResponse(['piezaDentalOdontograma'=>$template,'mensaje'=>'Se ha actualizado de forma correcta!','odontoPacientes'=>$templateOdontoPaciente]);
                            }else{
                                /**Llamado de partes de un aodontogramma en odontograma con el odonto paciente **/
                                $dqlOdontogramaPaciente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idPieza ASC";  
                                $queryOdontogramaPaciente = $entityManager->createQuery($dqlOdontogramaPaciente)->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma());
                                $odontoPacientes= $queryOdontogramaPaciente->getResult();
                                
                                $templateOdontoPaciente = $this->render('odontogramaPaciente/cargaOdontoPaciente.html.twig', [
                                    'odontoPacientes'=>$odontoPacientes
                                    ])->getContent();

                                $dqlPiezaDental ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                                $queryPiezaDental = $entityManager->createQuery($dqlPiezaDental)->setParameter('idPieza',$odontogramaPaciente->getIdPieza())->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma());
                                $piezaDentalOdontograma= $queryPiezaDental->getResult();
                                $template = $this->render('odontogramaPaciente/datosPiezaDental.html.twig', [
                                'piezaDentalOdontograma'=>$piezaDentalOdontograma
                                ])->getContent();

                                return new JsonResponse(['piezaDentalOdontograma'=>$template,'error'=>'No puede ingresar cambios a una superficie diferente de la pieza completa; si desea cambiar este diagnóstico eliminelo!','odontoPacientes'=>$templateOdontoPaciente]);
                            }
                        }else{
                            $dqlPiezaSuperficie ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma and odontoPaciente.idSuperficie = :idSuperficie ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                            $queryPiezaSuperficie = $entityManager->createQuery($dqlPiezaSuperficie)->setParameter('idPieza',$odontogramaPaciente->getIdPieza())->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma())->setParameter('idSuperficie',$odontogramaPaciente->getIdSuperficie());
                            $piezaSuperficie= $queryPiezaSuperficie->getResult();
                            if (sizeof($piezaSuperficie) > 0) {
                                foreach ($piezaSuperficie as $odontoPacienteSeleccionado) {
                                   $id= $odontoPacienteSeleccionado->getIdOdontogramaPaciente();
                                }
                                $cambiOdontogramaPaciente =  $this->getDoctrine()->getRepository(OdontogramaPaciente::class)->find($id);

                                $cambiOdontogramaPaciente->setIdRipTipoOdon($odontogramaPaciente->getIdRipTipoOdon());
                                $cambiOdontogramaPaciente->setIdDiagPrincipal($odontogramaPaciente->getIdDiagPrincipal());
                                $cambiOdontogramaPaciente->setIdUsuarioProfesional($odontogramaPaciente->getIdUsuarioProfesional());
                                $entityManager->persist($cambiOdontogramaPaciente);
                                $entityManager->flush();

                                /**Llamado de partes de un odontograma con el odonto paciente **/
                                $dqlOdontogramaPaciente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idPieza ASC";  
                                $queryOdontogramaPaciente = $entityManager->createQuery($dqlOdontogramaPaciente)->setParameter('idOdontograma',$cambiOdontogramaPaciente->getIdOdontograma());
                                $odontoPacientes= $queryOdontogramaPaciente->getResult();

                                $templateOdontoPaciente = $this->render('odontogramaPaciente/cargaOdontoPaciente.html.twig', [
                                    'odontoPacientes'=>$odontoPacientes
                                    ])->getContent();


                                $dqlPiezaDental ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                                $queryPiezaDental = $entityManager->createQuery($dqlPiezaDental)->setParameter('idPieza',$cambiOdontogramaPaciente->getIdPieza())->setParameter('idOdontograma',$cambiOdontogramaPaciente->getIdOdontograma());
                                $piezaDentalOdontograma= $queryPiezaDental->getResult();
                                $template = $this->render('odontogramaPaciente/datosPiezaDental.html.twig', [
                                'piezaDentalOdontograma'=>$piezaDentalOdontograma
                                ])->getContent();

                                return new JsonResponse(['piezaDentalOdontograma'=>$template,'mensaje'=>'Se ha actualizado de forma correcta!','odontoPacientes'=>$templateOdontoPaciente]);
                            }else{
                                if ($odontogramaPaciente->getIdSuperficie()->getIdSuperficie() == 8) {
                                    /**Llamado de partes de un odontograma con el odonto paciente **/
                                    $dqlOdontogramaPacienteExistente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idOdontograma = :idOdontograma and odontoPaciente.idPieza = :idPieza ORDER BY odontoPaciente.idPieza ASC";  
                                    $queryOdontogramaPacienteExistente = $entityManager->createQuery($dqlOdontogramaPacienteExistente)->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma())->setParameter('idPieza',$odontogramaPaciente->getIdPieza());
                                    $odontoPacientesExistente= $queryOdontogramaPacienteExistente->getResult();

                                    if (sizeof($odontoPacientesExistente) > 0) {
                                        /**Llamado de partes de un odontograma con el odonto paciente **/
                                        $dqlOdontogramaPaciente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idPieza ASC";  
                                        $queryOdontogramaPaciente = $entityManager->createQuery($dqlOdontogramaPaciente)->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma());
                                        $odontoPacientes= $queryOdontogramaPaciente->getResult();

                                        $templateOdontoPaciente = $this->render('odontogramaPaciente/cargaOdontoPaciente.html.twig', [
                                            'odontoPacientes'=>$odontoPacientes
                                            ])->getContent();


                                        $dqlPiezaDental ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                                        $queryPiezaDental = $entityManager->createQuery($dqlPiezaDental)->setParameter('idPieza',$odontogramaPaciente->getIdPieza())->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma());
                                        $piezaDentalOdontograma= $queryPiezaDental->getResult();
                                        $template = $this->render('odontogramaPaciente/datosPiezaDental.html.twig', [
                                        'piezaDentalOdontograma'=>$piezaDentalOdontograma
                                        ])->getContent();

                                        return new JsonResponse(['piezaDentalOdontograma'=>$template,'error'=>'No puede ingresar un diagnóstico a la pieza completa, usted cuenta con diagnósticos individuales en algunas superficies eliminelos e intente de nuevo!','odontoPacientes'=>$templateOdontoPaciente]);
                                    }else{
                                        $entityManager->persist($odontogramaPaciente);
                                        $entityManager->flush();

                                        /**Llamado de partes de un odontograma con el odonto paciente **/
                                        $dqlOdontogramaPaciente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idPieza ASC";  
                                        $queryOdontogramaPaciente = $entityManager->createQuery($dqlOdontogramaPaciente)->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma());
                                        $odontoPacientes= $queryOdontogramaPaciente->getResult();

                                        $templateOdontoPaciente = $this->render('odontogramaPaciente/cargaOdontoPaciente.html.twig', [
                                            'odontoPacientes'=>$odontoPacientes
                                            ])->getContent();


                                        $dqlPiezaDental ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                                        $queryPiezaDental = $entityManager->createQuery($dqlPiezaDental)->setParameter('idPieza',$odontogramaPaciente->getIdPieza())->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma());
                                        $piezaDentalOdontograma= $queryPiezaDental->getResult();
                                        $template = $this->render('odontogramaPaciente/datosPiezaDental.html.twig', [
                                        'piezaDentalOdontograma'=>$piezaDentalOdontograma
                                        ])->getContent();

                                        return new JsonResponse(['piezaDentalOdontograma'=>$template,'mensaje'=>'Se ha realizado el registro de forma correcta!','odontoPacientes'=>$templateOdontoPaciente]);
                                    }
                                }else{
                                    $entityManager->persist($odontogramaPaciente);
                                    $entityManager->flush();

                                    /**Llamado de partes de un odontograma con el odonto paciente **/
                                    $dqlOdontogramaPaciente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idPieza ASC";  
                                    $queryOdontogramaPaciente = $entityManager->createQuery($dqlOdontogramaPaciente)->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma());
                                    $odontoPacientes= $queryOdontogramaPaciente->getResult();

                                    $templateOdontoPaciente = $this->render('odontogramaPaciente/cargaOdontoPaciente.html.twig', [
                                            'odontoPacientes'=>$odontoPacientes
                                            ])->getContent();

                                    
                                    $dqlPiezaDental ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                                    $queryPiezaDental = $entityManager->createQuery($dqlPiezaDental)->setParameter('idPieza',$odontogramaPaciente->getIdPieza())->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma());
                                    $piezaDentalOdontograma= $queryPiezaDental->getResult();
                                    $template = $this->render('odontogramaPaciente/datosPiezaDental.html.twig', [
                                    'piezaDentalOdontograma'=>$piezaDentalOdontograma
                                    ])->getContent();

                                    return new JsonResponse(['piezaDentalOdontograma'=>$template,'mensaje'=>'Se ha realizado el registro de forma correcta!','odontoPacientes'=>$templateOdontoPaciente]);
                                }                                
                            }
                        }
                        //return $this->redirectToRoute('odontogramaPaciente_index');
                    }
                    $dqlOdontogramaPaciente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idPieza ASC";  
                    $queryOdontogramaPaciente = $entityManager->createQuery($dqlOdontogramaPaciente)->setParameter('idOdontograma',$odontogramaPaciente->getIdOdontograma());
                    $odontoPacientes= $queryOdontogramaPaciente->getResult();
                    return $this->render('odontogramaPaciente/new.html.twig', [
                        'odontogramaPaciente' => $odontogramaPaciente,
                        'form' => $form->createView(),
                        'paciente'=>$paciente,
                        'ultimoOdontogramaPaciente'=>$ultimoOdontogramaPaciente,
                        'odontoPacientes'=>$odontoPacientes
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la visualización de los datos actuiales dentro de un odontograma paciente de una pieza dental.
     *
     * @Route("/datospiezaDental", name="odontogramaPaciente_piezaDental", methods={"GET","POST"})
     */
    public function visualizacionPiezaDental(Request $request, PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'odontograma');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $idPieza=$request->get('idPieza');
                    $idOdontograma=$request->get('idOdontograma');
                    $response= new JsonResponse();
                    $dqlPiezaDental ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                    $queryPiezaDental = $entityManager->createQuery($dqlPiezaDental)->setParameter('idPieza',$idPieza)->setParameter('idOdontograma',$idOdontograma);
                    $piezaDentalOdontograma= $queryPiezaDental->getResult();
                    $template = $this->render('odontogramaPaciente/datosPiezaDental.html.twig', [
                    'piezaDentalOdontograma'=>$piezaDentalOdontograma
                    ])->getContent();
                    return new JsonResponse(['piezaDentalOdontograma'=>$template]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    
    /**
     * Metodo utilizado para la eliminación de diagnosticos que se encuentren en una posiciion de un odontograma paciente, ademas de exponer los 
     * datos actuales de un odontograma paciente.
     *
     * @Route("/{idOdontogramaPaciente}", name="odontogramaPaciente_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OdontogramaPaciente $odontogramaPaciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'odontograma');  
                if (sizeof($permisos) > 0) { 
                    $idPaciente=$odontogramaPaciente->getIdOdontograma()->getIdPaciente()->getIdPaciente();
                    $odontograma=$odontogramaPaciente->getIdOdontograma()->getIdOdontograma();
                    $idPieza=$odontogramaPaciente->getIdPieza();
                    if ($this->isCsrfTokenValid('delete'.$odontogramaPaciente->getIdOdontogramaPaciente(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($odontogramaPaciente);
                        $entityManager->flush();

                        /**Llamado de partes de un odontograma con el odonto paciente **/
                        $dqlOdontogramaPaciente ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idPieza ASC";  
                        $queryOdontogramaPaciente = $entityManager->createQuery($dqlOdontogramaPaciente)->setParameter('idOdontograma',$odontograma);
                        $odontoPacientes= $queryOdontogramaPaciente->getResult();

                        $templateOdontoPaciente = $this->render('odontogramaPaciente/cargaOdontoPaciente.html.twig', [
                                'odontoPacientes'=>$odontoPacientes
                                ])->getContent();

                        
                        $dqlPiezaDental ="SELECT odontoPaciente FROM App:OdontogramaPaciente odontoPaciente WHERE odontoPaciente.idPieza = :idPieza and odontoPaciente.idOdontograma = :idOdontograma ORDER BY odontoPaciente.idOdontogramaPaciente DESC";  
                        $queryPiezaDental = $entityManager->createQuery($dqlPiezaDental)->setParameter('idPieza',$idPieza)->setParameter('idOdontograma',$odontograma);
                        $piezaDentalOdontograma= $queryPiezaDental->getResult();
                        $template = $this->render('odontogramaPaciente/datosPiezaDental.html.twig', [
                        'piezaDentalOdontograma'=>$piezaDentalOdontograma
                        ])->getContent();

                        return new JsonResponse(['piezaDentalOdontograma'=>$template,'mensaje'=>'Se ha Eliminado el registro de forma correcta!','odontoPacientes'=>$templateOdontoPaciente]);
                    }

                    return $this->redirectToRoute('odontogramaPaciente_new',['idPaciente'=>$idPaciente]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }
}
