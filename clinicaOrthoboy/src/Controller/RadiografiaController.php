<?php

namespace App\Controller;

use App\Entity\Radiografia;
use App\Entity\TipoRadiografia;
use App\Entity\Paciente;
use App\Form\RadiografiaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de contrato utilizado para el manejo de los documentos radiograficos de cada paciente.
 *
 * @Route("/radiografia")
 */
class RadiografiaController extends Controller
{
    /**
     * Metodo utilizado para la generación de un pdf, sin embargo se encuentra a la espera de los prototipos.
     *
     * @Route("/{idRadiografia}/pdf", name="radiografia_pdf", methods={"GET","POST"})
     */
    public function pdf(Radiografia $radiografia,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Radiografias');  
                if (sizeof($permisos) > 0) { 
                     $html =$this->renderView('radiografia/pdfRadiografia.html.twig',
                    array(
                    'radiografia' => $radiografia
                    ));
                     
                    $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                    [   'encoding' => 'UTF-8',
                        'images' => false,
                        'enable-javascript' => true,
                        'print-media-type' => true,
                        'outline-depth' => 8,
                        'orientation' => 'Portrait',
                        'header-right'=>'Pag. [page] de [toPage]',
                        'page-size' => 'A4',
                        'viewport-size' => '1280x1024',
                        'margin-left' => '10mm',
                        'margin-right' => '10mm',
                        'margin-top' => '30mm',
                        'margin-bottom' => '25mm',
                        'user-style-sheet'=> 'light/dist/assets/css/bootstrap.min.css',
                        //'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
                        //'background'     => 'light/dist/assets/images/users/user-1.jpg',
                        //--javascript-delay 3000
                    ]);
                    $filename="Radiografía_N°_".$radiografia->getIdRadiografia()."Paciente_".$radiografia->getIdPaciente()."_fecha_". date("Y_m_d") .".pdf";
                    $response = new Response ($pdf,
                    200,
                    array('Content-Type' => 'aplicattion/pdf',
                          'Content-Disposition' => 'inline; filename="'.$filename.'"'
                    ,));
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }
    /**
     * @Route("/{idPaciente}", name="radiografia_index", methods={"GET","POST"})
     */
    public function index(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Radiografias');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlRadiografia ="SELECT radiografia FROM App:Radiografia radiografia WHERE radiografia.idPaciente = :idPaciente ORDER BY radiografia.idRadiografia DESC";  
                    $queryRadiografia = $entityManager->createQuery($dqlRadiografia)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $radiografias= $queryRadiografia->getResult();

                    $dqlClinica="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                    $queryClinica=$entityManager->createQuery($dqlClinica);
                    $clinicas=$queryClinica->getResult();

                    $radiografia = new Radiografia();
                    $radiografia->setIdPaciente($paciente);
                    $radiografia->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(RadiografiaType::class, $radiografia);                   
                    $form->handleRequest($request);

                    $fechaActual=date('d M Y H:i:s');
                    $fechaFinal=new  \DateTime($fechaActual);
                    $fechaNacimientoPaciente = $paciente->getFechaNacimiento();
                    $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                    $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';

                    if ($form->isSubmitted() && $form->isValid()) {    
                        $radiografia->uploadUno($form->get('imagen')->getData());
                        $entityManager->persist($radiografia);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('radiografia_show',['idRadiografia'=>$radiografia->getIdRadiografia()]);
                    }

                    return $this->render('radiografia/index.html.twig', [
                        'radiografias' => $radiografias,
                        'paciente'=>$paciente,
                        'form' => $form->createView(),
                        'clinicas'=>$clinicas,
                        'edad'=>$edad,
                        'radiografia' => $radiografia,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    
    /**
     * @Route("/{idRadiografia}/detalle", name="radiografia_show", methods={"GET"})
     */
    public function show(Request $request,PerfilGenerator $permisoPerfil,Radiografia $radiografia): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Radiografias');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();

                    $dqlClinica="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                    $queryClinica=$entityManager->createQuery($dqlClinica);
                    $clinicas=$queryClinica->getResult();

                    $fechaActual=date('d M Y H:i:s');
                    $fechaFinal=new  \DateTime($fechaActual);
                    $fechaNacimientoPaciente = $radiografia->getIdPaciente()->getFechaNacimiento();
                    $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                    $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';
                    return $this->render('radiografia/show.html.twig', [
                        'radiografia' => $radiografia,
                        'clinicas' => $clinicas,
                        'edad'=> $edad
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

}
