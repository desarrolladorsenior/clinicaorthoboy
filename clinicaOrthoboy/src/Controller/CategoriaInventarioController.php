<?php

namespace App\Controller;

use App\Entity\CategoriaInventario;
use App\Form\CategoriaInventarioType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de categoria inventario utilizado para el manejo de categorización de un insumo.
 *
 * @Route("/categoriaInventario")
 */
class CategoriaInventarioController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de las categorias de inventario y registro de nuevas categorias.
     *
     * @Route("/", name="categoriaInventario_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'categorias elementos');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlCategoriaInventario ="SELECT categoriaInventario FROM App:CategoriaInventario categoriaInventario
                    ORDER BY categoriaInventario.idCategoriaInventario DESC";  
                    $queryCategoriaInventario = $entityManager->createQuery($dqlCategoriaInventario);
                    $categoriaInventarios= $queryCategoriaInventario->getResult();

                    $categoriaInventario = new CategoriaInventario();
                    $categoriaInventario->setEstado(TRUE);
                    $form = $this->createForm(CategoriaInventarioType::class, $categoriaInventario);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($categoriaInventario);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('categoriaInventario_index');
                    }
                    return $this->render('categoriaInventario/index.html.twig', [
                        'categoriaInventarios' => $categoriaInventarios,
                        'categoriaInventario' => $categoriaInventario,
                        'form' => $form->createView()
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la edición de categorias de inventario.
     *
     * @Route("/{idCategoriaInventario}/edit", name="categoriaInventario_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CategoriaInventario $categoriaInventario,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'categorias elementos');  
                if (sizeof($permisos) > 0) {                    
                    $entityManager = $this->getDoctrine()->getManager();
                    $form = $this->createForm(CategoriaInventarioType::class, $categoriaInventario);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($categoriaInventario);
                        $entityManager->flush(); 
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('categoriaInventario_index');
                    }
                    return $this->render('categoriaInventario/modalEdit.html.twig', [
                        'categoriaInventario' => $categoriaInventario,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }     
    }     


    /**
     * Metodo utilizado para la edición de estado de las categorias de inventario.
     *
     * @Route("/editarEstadoCategoriaInventario", name="categoriaInventario_editestado", methods={"GET","POST"})
     */
    public function editEstado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'categorias elementos');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $id=$request->request->get('id');
                    $estado=$request->request->get('estado');
                    if($estado == 'ACTIVO'){
                        $estado = true;
                    }else{
                        $estado = false;
                    }
                    $dqlModificarEstadoCategoriaInventario="UPDATE App:CategoriaInventario categoriaInventario SET categoriaInventario.estado= :estado WHERE categoriaInventario.idCategoriaInventario= :idCategoriaInventario";
                    $queryModificarEstadoCategoriaInventario=$em->createQuery($dqlModificarEstadoCategoriaInventario)->setParameter('estado', $estado)->setParameter('idCategoriaInventario', $id);
                    $modificarEstadoCategoriaInventario=$queryModificarEstadoCategoriaInventario->execute();
                    $response->setData('Se ha cambiado el estado de forma correcta!');
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la eliminación de una categoria de inventario.
     *
     * @Route("/{idCategoriaInventario}", name="categoriaInventario_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CategoriaInventario $categoriaInventario,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'categorias elementos');  
              if (sizeof($permisos) > 0) {
                //var_dump($esterilizacion->getIdEsterilizacion());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($categoriaInventario);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('categoriaInventario_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('categoriaInventario_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('categoriaInventario_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        } 
    }
}
