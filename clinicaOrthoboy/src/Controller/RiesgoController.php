<?php

namespace App\Controller;

use App\Entity\Riesgo;
use App\Form\RiesgoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de riesgo utilizado para el manejo del tipo de consentimiento.
 *
 * @Route("/riesgo")
 */
class RiesgoController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de los riesgos.
     *
     * @Route("/", name="riesgo_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'riesgos');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlRiesgo ="SELECT riesgo FROM App:Riesgo riesgo
                    ORDER BY riesgo.idRiesgo DESC";  
                    $queryRiesgo = $entityManager->createQuery($dqlRiesgo);
                    $riesgos= $queryRiesgo->getResult();
                    
                    $riesgo = new Riesgo();
                    $form = $this->createForm(RiesgoType::class, $riesgo);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $riesgo->setEstado(true);
                        $entityManager->persist($riesgo);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('riesgo_index');
                    }
                    return $this->render('riesgo/index.html.twig', [
                        'riesgos' => $riesgos,
                        'riesgo' => $riesgo,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }     
            }
        }    
    }

    /**
     * Metodo utilizado para la edición del estado de un riesgo.
     *
     * @Route("/editarEstadoRiesgo", name="riesgo_editarEstadoRiesgo", methods={"GET","POST"})
     */
    public function editEstadoRiesgo(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'riesgos');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $em=$this->getDoctrine()->getManager();
                $response = new JsonResponse();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }
                $dqlModificarEstado="UPDATE App:Riesgo riesgo SET riesgo.estado= :estado WHERE riesgo.idRiesgo= :idRiesgo";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idRiesgo', $id);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    

    }

    /**
     * Metodo utilizado para la edición de un riesgo.
     *
     * @Route("/{idRiesgo}/edit", name="riesgo_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Riesgo $riesgo,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'riesgos');  
                if (sizeof($permisos) > 0) {
                    $form = $this->createForm(RiesgoType::class, $riesgo);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('riesgo_index', [
                            'idRiesgo' => $riesgo->getIdRiesgo(),
                        ]);
                    }
                    return $this->render('riesgo/modalEdit.html.twig', [
                        'riesgo' => $riesgo,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }     
            }
        }    
    }

    /**
     * Metodo utilizado para la eliminación de un riesgo.
     *
     * @Route("/{idRiesgo}", name="riesgo_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Riesgo $riesgo,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'riesgos');  
              if (sizeof($permisos) > 0) {
                try{

                    if ($this->isCsrfTokenValid('delete'.$riesgo->getIdRiesgo(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($riesgo);
                        $entityManager->flush();
                        $successMessage= 'Se ha eliminado de forma correcta!';
                        $this->addFlash('mensaje',$successMessage);
                        return $this->redirectToRoute('riesgo_index');
                    }

                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('riesgo_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('riesgo_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }    
    }
}
