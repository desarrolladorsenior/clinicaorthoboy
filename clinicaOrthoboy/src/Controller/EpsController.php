<?php

namespace App\Controller;

use App\Entity\Eps;
use App\Entity\Convenio;
use App\Form\EpsType;
use App\Form\ConvenioType;
use App\Repository\EpsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\PerfilGenerator;

/**
 * Controlador de eps utilizado para el manejo de las mismas que se encuentren disponibles en el momento, utilizadas para los convenios y dentro de los datos de los 
 * pacientes.
 * @Route("/eps")
 */
class EpsController extends AbstractController
{ 

    /**
     * Metodo utilizado para la visualización de una eps y sus sedes, ademas del ingreso de nuevas eps y sedes.
     * 
     * @Route("/", name="eps_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'EPS y sedes');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dql ="SELECT eps FROM App:Eps eps
                    ORDER BY eps.idEps DESC";  
                    $queryEps = $entityManager->createQuery($dql);
                    $epss= $queryEps->getResult();

                    $dqlEpsSede ="SELECT eps FROM App:Eps eps WHERE eps.idSedeEps is not null
                    ORDER BY eps.idEps DESC";  
                    $queryEpsSede = $entityManager->createQuery($dqlEpsSede);
                    $epsSede= $queryEpsSede->getResult();


                    $eps= new Eps();
                     /*Formulario Eps para crear a nivel modal*/
                    $eps->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(EpsType::class, $eps);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $eps->setEstado(true);
                        $entityManager->persist($eps);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('eps_index');
                    }

                    return $this->render('eps/index.html.twig', [
                        'epss' => $epss,
                        'eps' => $eps,
                        'form' => $form->createView(),     
                        'epsSede' =>$epsSede  
                    ]);
                }else{
                        $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                        return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }  


     /**
     * Metodo utilizado para la visualización de una eps.
     * 
     * @Route("/{idEps}/show", name="eps_show", methods={"GET","POST"})
     */
    public function show(Eps $eps,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'EPS y sedes');  
                if (sizeof($permisos) > 0) {
                    return $this->render('eps/modalVerDetalleEps.html.twig', [
                        'eps' => $eps,

                    ]);
                }else{
                        $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                        return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }   

    /**
     * Metodo utilizado para el registro de una nueva sede.
     *
     * @Route("/newSedeEps", name="eps_newSedeEps", methods={"GET","POST"})
     */
    public function newSubtipo(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'EPS y sedes');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $eps= new Eps();
                    $idEps=$request->request->get('idEps');
                    $dql ="SELECT eps FROM App:Eps eps
                    WHERE eps.idEps = :idEps";  
                    $queryEps = $entityManager->createQuery($dql)->setParameter('idEps',$idEps);
                    $tipoEps= $queryEps->getResult();
                    foreach ($tipoEps as $tipoE ) {             
                       $eps->getidSedeEps($eps->setidSedeEps($tipoE));
                    }
                    $form = $this->createForm(EpsType::class, $eps);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) { 
                        $eps->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser()); 
                        $eps->getEstado($eps->setEstado(true));        
                        $entityManager->persist($eps);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('eps_index');
                    }

                    return $this->render('eps/modalNuevaSedeEps.html.twig', [
                        'eps' => $eps,
                        'formGeneralEps' => $form->createView(),
                    ]);
                }else{
                        $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                        return $this->redirectToRoute('panel_principal');
                }    
            }
        }          
    }


     /**
     * Metodo utilizado para el registro de un convenio solo en el caso que la eps no tenga un convenio vigente.
     *
     * @Route("/newConvenio", name="eps_newConvenio", methods={"GET","POST"})
     */
    public function newConvenio(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Convenio');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $convenio= new Convenio();
                    $idEps=$request->request->get('idEps');        
                    $dql ="SELECT eps FROM App:Eps eps WHERE eps.idEps = :idEps";
                    $queryEps = $entityManager->createQuery($dql)->setParameter('idEps',$idEps);
                    $Epss= $queryEps->getResult();
                    foreach ($Epss as $epsSeleccionado ) {             
                       $convenio->getIdEps($convenio->setIdEps($epsSeleccionado));
                    }
                    $dqlClinica ="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                    $queryClinica = $entityManager->createQuery($dqlClinica);
                    $clinicas= $queryClinica->getResult();
                    foreach ($clinicas as $clinicaSeleccionado ) {             
                       $convenio->getIdClinica($convenio->setIdClinica($clinicaSeleccionado));
                    }
                   // $convenio->setIdClinica($this->get('security.token_storage')->getToken()->getUser()->getIdSucursal());
                    $convenio->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(ConvenioType::class, $convenio);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {  
                        $dqlConvenio ="SELECT convenio FROM App:Convenio convenio WHERE convenio.idEps = :idEps and convenio.estado =:estado";
                        $queryConvenio = $entityManager->createQuery($dqlConvenio)->setParameter('idEps',$convenio->getIdEps())->setParameter('estado',true);
                        $convenios= $queryConvenio->getResult();
                        
                        if (sizeof($convenios) > 0) {
                            $this->addFlash('error', 'En este momento se encuentra un convenio activo. No es posible crear un nuevo convenio con esta eps!');
                            return $this->redirectToRoute('eps_index');
                        }else{
                            $convenio->getEstado($convenio->setEstado(true));             
                            $entityManager->persist($convenio); 
                            $entityManager->flush();
                            $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                            return $this->redirectToRoute('eps_index');
                        }
                    }

                    return $this->render('eps/modalNewConvenio.html.twig', [
                        'convenio' => $convenio,
                        'form' => $form->createView(),
                    ]);
                }else{
                        $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                        return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }



    

     /**
     * Metodo utilizado para la edición de estado de una eps y sus sedes.
     *
     * @Route("/editarestadoEps", name="eps_editestado", methods={"GET","POST"})
     */
    public function editEstadoEps(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'EPS y sedes');  
              if (sizeof($permisos) > 0) {
                $em=$this->getDoctrine()->getManager();
                $response = new JsonResponse();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }        
                $dql ="SELECT eps FROM App:Eps eps WHERE eps.idEps = :idEps or eps.idSedeEps = :idEps ";  
                $queryEps = $em->createQuery($dql)->setParameter('idEps',$id);
                $epss= $queryEps->getResult();
                $arrayIDS=array();
                foreach ($epss as $epSede ) {            
                   $ids= $epSede->getIdEps();
                   array_push($arrayIDS, $ids);
                }  
                $dqlModificarEstado="UPDATE App:Eps eps SET eps.estado= :estado WHERE eps.idEps IN(:idEps) ";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idEps', $arrayIDS);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }   
    }

    /**
    * Metodo utilizado para la edición de una eps.
    *
    * @Route("/{idEps}/edit", name="eps_edit", methods={"GET","POST"})
    */
    public function edit(Request $request, Eps $ep,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'EPS y sedes');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $eps= new Eps();
                    $idEps=$request->request->get('idEps');
                    $dql ="SELECT eps FROM App:Eps eps
                    WHERE eps.idSedeEps = :idEps";  
                    $queryEps = $entityManager->createQuery($dql)->setParameter('idEps',$idEps);
                    $tipoEps= $queryEps->getResult();
                    foreach ($tipoEps as $tipoE ) {             
                       $eps->getIdSedeEps($eps->setIdSedeEps($tipoE));
                    }

                    $editForm = $this->createForm(EpsType::class, $ep);
                    $editForm->handleRequest($request);
                    if ($editForm->isSubmitted() && $editForm->isValid()) {
                        $response = new JsonResponse();
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('eps_index');
                    }
                    return $this->render('eps/modalEditEps.html.twig', [
                        'eps' => $ep,
                        'formEditE' => $editForm->createView(),
                    ]);
                }else{
                        $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                        return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
    * Metodo utilizado para la edición de una sede de la eps.
    *
    * @Route("/{idEps}/editSede", name="eps_editSede", methods={"GET","POST"})
    */
    public function editSede(Request $request, Eps $eps,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'EPS y sedes');  
                if (sizeof($permisos) > 0) {
                    $editForm = $this->createForm(EpsType::class, $eps);
                    $editForm->handleRequest($request);
                    if ($editForm->isSubmitted() && $editForm->isValid()) {
                        $response = new JsonResponse();
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');

                        return $this->redirectToRoute('eps_index');
                    }
                    return $this->render('eps/modalEditSede.html.twig', [
                        'eps' => $eps,
                        'formEdit' => $editForm->createView(),
                    ]);
                }else{
                        $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                        return $this->redirectToRoute('panel_principal');
                }    
            }
        }          
    }


    /**
     * Metodo utilizado para la eliminación de una eps.
     *
     * @Route("/{idEps}", name="eps_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Eps $eps,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'EPS y sedes');  
                if (sizeof($permisos) > 0) {
                    try{
                        if ($this->isCsrfTokenValid('delete'.$eps->getIdEps(), $request->request->get('_token'))) {
                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->remove($eps);
                            $entityManager->flush();
                            $successMessage= 'Se ha eliminado de forma correcta!';
                            $this->addFlash('mensaje',$successMessage);
                            return $this->redirectToRoute('eps_index');
                        }
                    }catch (\Doctrine\DBAL\DBALException $e) {
                        if ($e->getCode() == 0){
                            if ($e->getPrevious()->getCode() == 23503){
                                $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                                $this->addFlash('error',$successMessage1);
                                return $this->redirectToRoute('eps_index');
                            }else{
                                throw $e;
                            }
                        }else{
                            throw $e;
                        }
                    } 
                    return $this->redirectToRoute('eps_index');
                }else{
                        $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                        return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }
    
}