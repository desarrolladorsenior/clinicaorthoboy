<?php

namespace App\Controller;

use App\Entity\Departamento;
use App\Entity\Municipio;
use App\Form\DepartamentoType;
use App\Form\MunicipioType;
use App\Service\PerfilGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de departamento utilizado para el manejo de los costados departamentales y municipales.
 *
 * @Route("/departamento")
 */
class DepartamentoController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de un departamento y sus municipios, además del registro de los departamentos.
     *
     * @Route("/", name="departamento_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'departamentos');  
                if (sizeof($permisos) > 0) {

                    $entityManager = $this->getDoctrine()->getManager();
                    $dql ="SELECT departamento FROM App:Departamento departamento
                    ORDER BY departamento.idDepartamento ASC";  
                    $queryDepartamento = $entityManager->createQuery($dql);
                    $departamentos= $queryDepartamento->getResult();


                    $departamento = new Departamento();
                    $form = $this->createForm(DepartamentoType::class, $departamento);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($departamento);
                        $entityManager->flush();
                         $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('departamento_index');
                    }

                    $municipios = $this->getDoctrine()
                        ->getRepository(Municipio::class)
                        ->findAll();

                    return $this->render('departamento/index.html.twig', [
                        'departamentos' => $departamentos,
                        'departamento' => $departamento,
                        'form' => $form->createView(),
                        'municipios' => $municipios,

                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
        
    }

    /**
     * Metodo utilizado para la edición de un departamento.
     *
     * @Route("/{idDepartamento}/edit", name="departamento_edit", methods={"GET","POST"})
     */
    public function editDepartamento(Request $request, Departamento $departament,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'departamentos');  
                if (sizeof($permisos) > 0) {
                    $edittForm = $this->createForm(DepartamentoType::class, $departament);
                    $edittForm->handleRequest($request);

                    if ($edittForm->isSubmitted() && $edittForm->isValid()) {
                        $response = new JsonResponse();
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('departamento_index');
                    }
                    return $this->render('departamento/modalEditDepartamento.html.twig', [
                        'departamento' => $departament,
                        'edicion_form' => $edittForm->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
        
    }



    /**
     * Metodo utilizado para el registro de un municipio.
     *
     * @Route("/newMunicipio", name="departamento_newMunicipio", methods={"GET","POST"})
     */
    public function newMunicipio(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'departamentos');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $municipio= new Municipio();
                    $idMunicipio=$request->request->get('idDepartamento');

                    $dql ="SELECT departamento FROM App:Departamento departamento WHERE departamento.idDepartamento = :idDepartamento";

                    $queryMunicipio = $entityManager->createQuery($dql)->setParameter('idDepartamento',$idMunicipio);
                    
                    $tipoMunicipio= $queryMunicipio->getResult();
                    foreach ($tipoMunicipio as $tipoE ) {             
                       $municipio->getIdDepartamento($municipio->setIdDepartamento($tipoE));
                    }
                    $form = $this->createForm(MunicipioType::class, $municipio);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {        
                        $entityManager->persist($municipio);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('departamento_index');
                    }
                    return $this->render('departamento/modalCrearMunicipio.html.twig', [
                        'municipio' => $municipio,
                        'formGeneralMunicipio' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }


    /**
     * Metodo utilizado para la edición de un municipio.
     *
     * @Route("/{id}/editMunicipio", name="departamento_editM", methods={"GET","POST"})
     */
    public function editMunicipio(Request $request, Municipio $municipio,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'departamentos');  
                if (sizeof($permisos) > 0) {
                    $editForm = $this->createForm(MunicipioType::class, $municipio);
                    $editForm->handleRequest($request);
                    if ($editForm->isSubmitted() && $editForm->isValid()) {
                        $response = new JsonResponse();
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('departamento_index');
                    }
                    return $this->render('departamento/modalEditMunicipio.html.twig', [
                        'municipio' => $municipio,
                        'edicion_form_municipio' => $editForm->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }


    /**
     * Metodo utilizado para la eliminación de un departamento.
     *
     * @Route("/{id}", name="departamento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Departamento $departamento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'departamentos');  
                if (sizeof($permisos) > 0) {
                    try{

                    if ($this->isCsrfTokenValid('delete'.$departamento->getIdDepartamento(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($departamento);
                        $entityManager->flush();
                        $successMessage= 'Se ha eliminado de forma correcta!';
                        $this->addFlash('mensaje',$successMessage);
                        return $this->redirectToRoute('departamento_index');

                    }

                    }catch (\Doctrine\DBAL\DBALException $e) {
                        if ($e->getCode() == 0){
                            if ($e->getPrevious()->getCode() == 23503){
                                $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                                $this->addFlash('error',$successMessage1);
                                return $this->redirectToRoute('departamento_index');
                            }else{
                                throw $e;
                            }
                        }else{
                            throw $e;
                        }
                    } 
                    
                    return $this->redirectToRoute('departamento_index');
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    

}
