<?php

namespace App\Controller;

use App\Entity\PaqueteEsterilizacion;
use App\Form\PaqueteEsterilizacionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de paquete esterilizacion utilizado para el manejo de los tipo de paquetes que tenga una esterilización especificando que elementos se cargan en cada 
 * uno de estos.
 *
 * @Route("/paqueteEsterilizacion")
 */
class PaqueteEsterilizacionController extends AbstractController
{

    /**
     * Metodo utilizado para la edición de un paquete Esterilizacion.
     *
     * @Route("/{idPaqueteEsterilizacion}/edit", name="paqueteEsterilizacion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PaqueteEsterilizacion $paqueteEsterilizacion,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'esterilización principal');  
                if (sizeof($permisos) > 0) {
                    $ruta=$request->request->get('ruta');
                    $entityManager = $this->getDoctrine()->getManager();
                    
                    $form = $this->createForm(PaqueteEsterilizacionType::class, $paqueteEsterilizacion);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {          
                        $entityManager->persist($paqueteEsterilizacion);
                        $entityManager->flush();  
                        if ($ruta == 'VISTA') {
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                            return $this->redirectToRoute('esterilizacion_index');      
                        }else{
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                            return $this->redirectToRoute('esterilizacion_nuevoPaquetEsterilizacion', array('idEsterilizacion' => $paqueteEsterilizacion->getIdEsterilizacion()->getIdEsterilizacion()));
                        }   
                    }

                    return $this->render('paqueteEsterilizacion/modalEdit.html.twig', [
                        'paqueteEsterilizacion' => $paqueteEsterilizacion,
                        'form' => $form->createView(),
                        'ruta'=>$ruta
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la edición del estado de un paquete Esterilizacion.
     *
     * @Route("/editarEstadoPaqueteEsterilizacion", name="paqueteEsterilizacion_editestado", methods={"GET","POST"})
     */
    public function editEstado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'esterilización principal');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $id=$request->request->get('id');
                    $estado=$request->request->get('estado');
                    if($estado == 'ACTIVO'){
                        $estado = true;
                    }else{
                        $estado = false;
                    }
                    $dqlModificarEstadoTratamiento="UPDATE App:PaqueteEsterilizacion pEsterilizacion SET pEsterilizacion.estado= :estado WHERE pEsterilizacion.idPaqueteEsterilizacion= :idPaqueteEsterilizacion";
                    $queryModificarEstadoPaqueteEsterilizacion=$em->createQuery($dqlModificarEstadoTratamiento)->setParameter('estado', $estado)->setParameter('idPaqueteEsterilizacion', $id);
                    $modificarEstadoPaqueteEsterilizacion=$queryModificarEstadoPaqueteEsterilizacion->execute();
                    $response->setData('Se ha cambiado el estado de forma correcta!');
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la eliminación de un paquete Esterilizacion.
     *
     * @Route("/{idPaqueteEsterilizacion}", name="paqueteEsterilizacion_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PaqueteEsterilizacion $paqueteEsterilizacion,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'esterilización principal');  
              if (sizeof($permisos) > 0) {
                //var_dump($esterilizacion->getIdEsterilizacion());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($paqueteEsterilizacion);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('esterilizacion_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('esterilizacion_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('esterilizacion_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }
}
