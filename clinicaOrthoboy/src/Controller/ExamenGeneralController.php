<?php

namespace App\Controller;

use App\Entity\ExamenGeneral;
use App\Entity\Paciente;
use App\Controller\PacienteController;
use App\Form\ExamenGeneralType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de examen general utilizado para el manejo del examen general de un paciente.
 *
 * @Route("/examenGeneral")
 */
class ExamenGeneralController extends Controller
{
    
    /**
     * Metodo utilizado para la visualización de todos los examenes generales de un paciente.
     *
     * @Route("/total/{idPaciente}", name="examenGeneral_total", methods={"GET","POST"})
     */
    public function examenGeneralTotalActualw(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'examenes generales');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlExamenGeneralTotales ="SELECT examenGeneral FROM App:ExamenGeneral examenGeneral
                    WHERE examenGeneral.idPaciente = :idPaciente ORDER BY examenGeneral.idExamenGeneral DESC";  
                    $queryExamenGeneralTotales = $entityManager->createQuery($dqlExamenGeneralTotales)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $examenGeneralTotales= $queryExamenGeneralTotales->getResult();     
                    return $this->render('examenGeneral/modalConsultaExamenGeneral.html.twig', [
                        'examenGeneralTotales' => $examenGeneralTotales,
                        'paciente'=>$paciente,
                    ]);  
                 }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la visualización del ultimo examen general del paciente si lo tiene, ademas visualiza los datos de los antecedentes, 
     * medicamento, alergias , habitos, sellantes, fluor, detartraje, remoción de placa. Igualmente general el registro de un nuevo examen general.
     *
     * @Route("/new/{idPaciente}", name="examenGeneral_new", methods={"GET","POST"})
     */
    public function new(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{                 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'examenes generales');  
                if (sizeof($permisos) > 0) {   
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlExamenGeneralUltimo ="SELECT examenGeneral FROM App:ExamenGeneral examenGeneral
                    WHERE examenGeneral.idPaciente = :idPaciente ORDER BY examenGeneral.idExamenGeneral DESC";  
                    $queryExamenGeneralUltimo = $entityManager->createQuery($dqlExamenGeneralUltimo)->setParameter('idPaciente',$paciente->getIdPaciente())->setMaxResults(1);
                    $examenGeneralUltimo= $queryExamenGeneralUltimo->getResult();                    
                    $examenGeneral = new ExamenGeneral();
                    if (sizeof($examenGeneralUltimo) > 0) {
                        foreach ($examenGeneralUltimo as $seleccionUltimoExGe ) {
                            $examenGeneral->setMotivoConsulta($seleccionUltimoExGe->getMotivoConsulta());   
                            $examenGeneral->setAntecedenteMedico($seleccionUltimoExGe->getAntecedenteMedico());   
                            $examenGeneral->setAntecedenteFamiliar($seleccionUltimoExGe->getAntecedenteFamiliar());   
                            $examenGeneral->setMedicamento($seleccionUltimoExGe->getMedicamento());  
                            $examenGeneral->setAlergia($seleccionUltimoExGe->getAlergia());   
                            $examenGeneral->setFactorRiesgo($seleccionUltimoExGe->getFactorRiesgo());   
                            $examenGeneral->setDeterminanteSocial($seleccionUltimoExGe->getDeterminanteSocial());   
                            $examenGeneral->setPoblacionVulnerable($seleccionUltimoExGe->getPoblacionVulnerable());   
                            $examenGeneral->setAntecedenteSistemico($seleccionUltimoExGe->getAntecedenteSistemico());   
                            $examenGeneral->setHabito($seleccionUltimoExGe->getHabito());   
                            $examenGeneral->setOtro($seleccionUltimoExGe->getOtro());   
                            $examenGeneral->setEmbarazo($seleccionUltimoExGe->getEmbarazo());   
                            $examenGeneral->setCara($seleccionUltimoExGe->getCara());   
                            $examenGeneral->setGanglios($seleccionUltimoExGe->getGanglios());   
                            $examenGeneral->setMaxilarMandibula($seleccionUltimoExGe->getMaxilarMandibula());   
                            $examenGeneral->setMusculo($seleccionUltimoExGe->getMusculo());   
                            $examenGeneral->setLabio($seleccionUltimoExGe->getLabio());   
                            $examenGeneral->setCarillo($seleccionUltimoExGe->getCarillo());   
                            $examenGeneral->setEncia($seleccionUltimoExGe->getEncia());   
                            $examenGeneral->setLengua($seleccionUltimoExGe->getLengua());   
                            $examenGeneral->setPisoBocal($seleccionUltimoExGe->getPisoBocal());   
                            $examenGeneral->setPaladar($seleccionUltimoExGe->getPaladar());   
                            $examenGeneral->setOrfaringe($seleccionUltimoExGe->getOrfaringe());   
                            $examenGeneral->setDolorMuscular($seleccionUltimoExGe->getDolorMuscular());   
                            $examenGeneral->setDolorArtilar($seleccionUltimoExGe->getDolorArtilar());
                            $examenGeneral->setRuidoArtilar($seleccionUltimoExGe->getRuidoArtilar());                               
                            $examenGeneral->setAlteracionMovimiento($seleccionUltimoExGe->getAlteracionMovimiento());   
                            $examenGeneral->setMalOclusion($seleccionUltimoExGe->getMalOclusion()); 
                            $examenGeneral->setCrecimientoDesarrollo($seleccionUltimoExGe->getCrecimientoDesarrollo());  
                            $examenGeneral->setFactorRiesgo($seleccionUltimoExGe->getFactorRiesgo());   
                            $examenGeneral->setFasetaDesgaste($seleccionUltimoExGe->getFasetaDesgaste());   
                            $examenGeneral->setNDienteTP($seleccionUltimoExGe->getNDienteTP());   
                            $examenGeneral->setEstadoEstructural($seleccionUltimoExGe->getEstadoEstructural()); 
                            $examenGeneral->setAlteracionFlurosis($seleccionUltimoExGe->getAlteracionFlurosis());   
                            $examenGeneral->setRehabilitacion($seleccionUltimoExGe->getRehabilitacion());
                            $examenGeneral->setSaliva($seleccionUltimoExGe->getSaliva());   
                            $examenGeneral->setFuncionTejido($seleccionUltimoExGe->getFuncionTejido());   
                            $examenGeneral->setPlaca($seleccionUltimoExGe->getPlaca());   
                            $examenGeneral->setRemocionPlaca($seleccionUltimoExGe->getRemocionPlaca());   
                            $examenGeneral->setFluor($seleccionUltimoExGe->getFluor());  
                            $examenGeneral->setDetartraje($seleccionUltimoExGe->getDetartraje());   
                            $examenGeneral->setSellante($seleccionUltimoExGe->getSellante()); 
                            $examenGeneral->setValoracionMedicina($seleccionUltimoExGe->getValoracionMedicina());   
                            $examenGeneral->setEducacionGrupal($seleccionUltimoExGe->getEducacionGrupal());                              
                        }
                        
                    }
                    $examenGeneral->setIdPaciente($paciente);                      
                    $examenGeneral->setIdUsuarioProfesional($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(ExamenGeneralType::class, $examenGeneral);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {      
                        
                        $entityManager->persist($examenGeneral);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('examenGeneral_new',['idPaciente'=>$paciente->getIdPaciente()]);
                    }
                    $dqlAntecedentes ="SELECT antecedente FROM App:Antecedente antecedente
                    WHERE antecedente.estado = :estado ORDER BY antecedente.idAntecedente DESC";  
                    $queryAntecedentes = $entityManager->createQuery($dqlAntecedentes)->setParameter('estado',true);
                    $antecedenteM= $queryAntecedentes->getResult();     
                    $antecedenteF= $queryAntecedentes->getResult();     
                    $antecedenteS= $queryAntecedentes->getResult();     

                    $dqlMedicamentos ="SELECT medicamento FROM App:Medicamento medicamento
                    WHERE medicamento.estado = :estado ORDER BY medicamento.idMedicamento DESC";  
                    $queryMedicamentos = $entityManager->createQuery($dqlMedicamentos)->setParameter('estado',true);
                    $medicamentos= $queryMedicamentos->getResult();     

                    $dqlAlergia ="SELECT alergia FROM App:Alergia alergia
                    WHERE alergia.estado = :estado ORDER BY alergia.idAlergia DESC";  
                    $queryAlergia = $entityManager->createQuery($dqlAlergia)->setParameter('estado',true);
                    $alergias= $queryAlergia->getResult();     

                    $dqlHabito ="SELECT habito FROM App:Habito habito
                    WHERE habito.estado = :estado ORDER BY habito.idHabito DESC";  
                    $queryHabito = $entityManager->createQuery($dqlHabito)->setParameter('estado',true);
                    $habitos= $queryHabito->getResult();     
                    
                    $dqlSellantes="SELECT date_part('year',exG.fechaApertura) as anio, count(exG) as cantidadRegistros,'Aplicación Sellante' as tipo  FROM App:ExamenGeneral exG WHERE  exG.sellante != :estado and exG.idPaciente = :idPaciente GROUP BY anio ORDER BY anio DESC";
                    $querySellantes = $entityManager->createQuery($dqlSellantes)->setParameter('estado',false)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $sellantes= $querySellantes->getResult(); 

                    $dqlFluor="SELECT date_part('year',exG.fechaApertura) as anio, count(exG) as cantidadRegistros ,'Aplicación Fluor' as tipo FROM App:ExamenGeneral exG WHERE  exG.fluor != :estado and exG.idPaciente = :idPaciente GROUP BY anio ORDER BY anio DESC";
                    $queryFluor = $entityManager->createQuery($dqlFluor)->setParameter('estado',false)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $fluors= $queryFluor->getResult();

                    $dqlDetartraje="SELECT date_part('year',exG.fechaApertura) as anio, count(exG)  as cantidadRegistros ,'Aplicación Detartraje' as tipo FROM App:ExamenGeneral exG WHERE  exG.detartraje != :estado and exG.idPaciente = :idPaciente GROUP BY anio ORDER BY anio DESC";
                    $queryDetartraje = $entityManager->createQuery($dqlDetartraje)->setParameter('estado',false)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $detartrajes= $queryDetartraje->getResult();  

                    $dqlRemocionPlaca="SELECT date_part('year',exG.fechaApertura) as anio, count(exG)  as cantidadRegistros ,'Remosión placa bacteriana' as tipo FROM App:ExamenGeneral exG WHERE  exG.remocionPlaca != :estado and exG.idPaciente = :idPaciente GROUP BY anio ORDER BY anio DESC";
                    $queryRemocionPlaca = $entityManager->createQuery($dqlRemocionPlaca)->setParameter('estado',false)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $remocionPlacas= $queryRemocionPlaca->getResult();  
                    $pacienteCon=new PacienteController();
                    return $this->render('examenGeneral/new.html.twig', [
                        'examenGeneral' => $examenGeneral,
                        'form' => $form->createView(),
                        'paciente'=>$paciente,
                        'ultimoDato' => $examenGeneralUltimo,
                        'antecedenteM'=>$antecedenteM,
                        'antecedenteF'=>$antecedenteF,
                        'antecedenteS'=>$antecedenteS,
                        'medicamentos' => $medicamentos,
                        'alergias'=>$alergias,
                        'habitos'=>$habitos,
                        'sellantes'=>$sellantes,
                        'fluors'=>$fluors,
                        'detartrajes'=>$detartrajes,
                        'remocionPlacas'=>$remocionPlacas,
                        'edad'=>$pacienteCon->calcularEdad($examenGeneral->getIdPaciente()->getFechaNacimiento()->format('Y-m-d'))
                    ]);  
                    
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }


    /**
     * Metodo utilizado para la visualización del examen general deseado de un paciente, ademas visualiza de los datos de los antecedentes, 
     * medicamento, alergias , habitos, sellantes, fluor, detartraje, remoción de placa.
     *
     * @Route("/ver/{idPaciente}", name="examenGeneral_examenVisual", methods={"GET","POST"})
     */
    public function verExamen(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{                 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'examenes generales');  
                if (sizeof($permisos) > 0) {   
                    $entityManager = $this->getDoctrine()->getManager();
                    $idExamen=$request->request->get('idExamen');
                    $dqlExamenGeneralUltimo ="SELECT examenGeneral FROM App:ExamenGeneral examenGeneral
                    WHERE examenGeneral.idPaciente = :idPaciente and  examenGeneral.idExamenGeneral =:idExamenGeneral";  
                    $queryExamenGeneralUltimo = $entityManager->createQuery($dqlExamenGeneralUltimo)->setParameter('idPaciente',$paciente->getIdPaciente())->setParameter('idExamenGeneral',$idExamen);
                    $examenGeneralUltimo= $queryExamenGeneralUltimo->getResult();                    
                    $examenGeneral = new ExamenGeneral();
                    if (sizeof($examenGeneralUltimo) > 0) {
                        foreach ($examenGeneralUltimo as $seleccionUltimoExGe ) {
                            $examenGeneral->setMotivoConsulta($seleccionUltimoExGe->getMotivoConsulta());   
                            $examenGeneral->setAntecedenteMedico($seleccionUltimoExGe->getAntecedenteMedico());   
                            $examenGeneral->setAntecedenteFamiliar($seleccionUltimoExGe->getAntecedenteFamiliar());   
                            $examenGeneral->setMedicamento($seleccionUltimoExGe->getMedicamento());  
                            $examenGeneral->setAlergia($seleccionUltimoExGe->getAlergia());   
                            $examenGeneral->setFactorRiesgo($seleccionUltimoExGe->getFactorRiesgo());   
                            $examenGeneral->setDeterminanteSocial($seleccionUltimoExGe->getDeterminanteSocial());   
                            $examenGeneral->setPoblacionVulnerable($seleccionUltimoExGe->getPoblacionVulnerable());   
                            $examenGeneral->setAntecedenteSistemico($seleccionUltimoExGe->getAntecedenteSistemico());   
                            $examenGeneral->setHabito($seleccionUltimoExGe->getHabito());   
                            $examenGeneral->setOtro($seleccionUltimoExGe->getOtro());   
                            $examenGeneral->setEmbarazo($seleccionUltimoExGe->getEmbarazo());   
                            $examenGeneral->setCara($seleccionUltimoExGe->getCara());   
                            $examenGeneral->setGanglios($seleccionUltimoExGe->getGanglios());   
                            $examenGeneral->setMaxilarMandibula($seleccionUltimoExGe->getMaxilarMandibula());   
                            $examenGeneral->setMusculo($seleccionUltimoExGe->getMusculo());   
                            $examenGeneral->setLabio($seleccionUltimoExGe->getLabio());   
                            $examenGeneral->setCarillo($seleccionUltimoExGe->getCarillo());   
                            $examenGeneral->setEncia($seleccionUltimoExGe->getEncia());   
                            $examenGeneral->setLengua($seleccionUltimoExGe->getLengua());   
                            $examenGeneral->setPisoBocal($seleccionUltimoExGe->getPisoBocal());   
                            $examenGeneral->setPaladar($seleccionUltimoExGe->getPaladar());   
                            $examenGeneral->setOrfaringe($seleccionUltimoExGe->getOrfaringe());   
                            $examenGeneral->setDolorMuscular($seleccionUltimoExGe->getDolorMuscular());   
                            $examenGeneral->setDolorArtilar($seleccionUltimoExGe->getDolorArtilar());
                            $examenGeneral->setRuidoArtilar($seleccionUltimoExGe->getRuidoArtilar());                               
                            $examenGeneral->setAlteracionMovimiento($seleccionUltimoExGe->getAlteracionMovimiento());   
                            $examenGeneral->setMalOclusion($seleccionUltimoExGe->getMalOclusion()); 
                            $examenGeneral->setCrecimientoDesarrollo($seleccionUltimoExGe->getCrecimientoDesarrollo());  
                            $examenGeneral->setFactorRiesgo($seleccionUltimoExGe->getFactorRiesgo());   
                            $examenGeneral->setFasetaDesgaste($seleccionUltimoExGe->getFasetaDesgaste());   
                            $examenGeneral->setNDienteTP($seleccionUltimoExGe->getNDienteTP());   
                            $examenGeneral->setEstadoEstructural($seleccionUltimoExGe->getEstadoEstructural()); 
                            $examenGeneral->setAlteracionFlurosis($seleccionUltimoExGe->getAlteracionFlurosis());   
                            $examenGeneral->setRehabilitacion($seleccionUltimoExGe->getRehabilitacion());
                            $examenGeneral->setSaliva($seleccionUltimoExGe->getSaliva());   
                            $examenGeneral->setFuncionTejido($seleccionUltimoExGe->getFuncionTejido());   
                            $examenGeneral->setPlaca($seleccionUltimoExGe->getPlaca());   
                            $examenGeneral->setRemocionPlaca($seleccionUltimoExGe->getRemocionPlaca());   
                            $examenGeneral->setFluor($seleccionUltimoExGe->getFluor());  
                            $examenGeneral->setDetartraje($seleccionUltimoExGe->getDetartraje());   
                            $examenGeneral->setSellante($seleccionUltimoExGe->getSellante()); 
                            $examenGeneral->setValoracionMedicina($seleccionUltimoExGe->getValoracionMedicina());   
                            $examenGeneral->setEducacionGrupal($seleccionUltimoExGe->getEducacionGrupal());                              
                        }
                        
                    }
                    $examenGeneral->setIdPaciente($paciente);                      
                    $examenGeneral->setIdUsuarioProfesional($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(ExamenGeneralType::class, $examenGeneral);
                    $form->handleRequest($request);
                    
                    $dqlAntecedentes ="SELECT antecedente FROM App:Antecedente antecedente
                    WHERE antecedente.estado = :estado ORDER BY antecedente.idAntecedente DESC";  
                    $queryAntecedentes = $entityManager->createQuery($dqlAntecedentes)->setParameter('estado',true);
                    $antecedenteM= $queryAntecedentes->getResult();     
                    $antecedenteF= $queryAntecedentes->getResult();     
                    $antecedenteS= $queryAntecedentes->getResult();     

                    $dqlMedicamentos ="SELECT medicamento FROM App:Medicamento medicamento
                    WHERE medicamento.estado = :estado ORDER BY medicamento.idMedicamento DESC";  
                    $queryMedicamentos = $entityManager->createQuery($dqlMedicamentos)->setParameter('estado',true);
                    $medicamentos= $queryMedicamentos->getResult();     

                    $dqlAlergia ="SELECT alergia FROM App:Alergia alergia
                    WHERE alergia.estado = :estado ORDER BY alergia.idAlergia DESC";  
                    $queryAlergia = $entityManager->createQuery($dqlAlergia)->setParameter('estado',true);
                    $alergias= $queryAlergia->getResult();     

                    $dqlHabito ="SELECT habito FROM App:Habito habito
                    WHERE habito.estado = :estado ORDER BY habito.idHabito DESC";  
                    $queryHabito = $entityManager->createQuery($dqlHabito)->setParameter('estado',true);
                    $habitos= $queryHabito->getResult();     
                    
                    $dqlSellantes="SELECT date_part('year',exG.fechaApertura) as anio, count(exG) as cantidadRegistros,'Aplicación Sellante' as tipo  FROM App:ExamenGeneral exG WHERE  exG.sellante != :estado and exG.idPaciente = :idPaciente GROUP BY anio ORDER BY anio DESC";
                    $querySellantes = $entityManager->createQuery($dqlSellantes)->setParameter('estado',false)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $sellantes= $querySellantes->getResult(); 

                    $dqlFluor="SELECT date_part('year',exG.fechaApertura) as anio, count(exG) as cantidadRegistros ,'Aplicación Fluor' as tipo FROM App:ExamenGeneral exG WHERE  exG.fluor != :estado and exG.idPaciente = :idPaciente GROUP BY anio ORDER BY anio DESC";
                    $queryFluor = $entityManager->createQuery($dqlFluor)->setParameter('estado',false)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $fluors= $queryFluor->getResult();

                    $dqlDetartraje="SELECT date_part('year',exG.fechaApertura) as anio, count(exG)  as cantidadRegistros ,'Aplicación Detartraje' as tipo FROM App:ExamenGeneral exG WHERE  exG.detartraje != :estado and exG.idPaciente = :idPaciente GROUP BY anio ORDER BY anio DESC";
                    $queryDetartraje = $entityManager->createQuery($dqlDetartraje)->setParameter('estado',false)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $detartrajes= $queryDetartraje->getResult();  

                    $dqlRemocionPlaca="SELECT date_part('year',exG.fechaApertura) as anio, count(exG)  as cantidadRegistros ,'Remosión placa bacteriana' as tipo FROM App:ExamenGeneral exG WHERE  exG.remocionPlaca != :estado and exG.idPaciente = :idPaciente GROUP BY anio ORDER BY anio DESC";
                    $queryRemocionPlaca = $entityManager->createQuery($dqlRemocionPlaca)->setParameter('estado',false)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $remocionPlacas= $queryRemocionPlaca->getResult();  
                    $pacienteCon=new PacienteController();
                    return $this->render('examenGeneral/new.html.twig', [
                        'examenGeneral' => $examenGeneral,
                        'form' => $form->createView(),
                        'paciente'=>$paciente,
                        'ultimoDato' => $examenGeneralUltimo,
                        'antecedenteM'=>$antecedenteM,
                        'antecedenteF'=>$antecedenteF,
                        'antecedenteS'=>$antecedenteS,
                        'medicamentos' => $medicamentos,
                        'alergias'=>$alergias,
                        'habitos'=>$habitos,
                        'sellantes'=>$sellantes,
                        'fluors'=>$fluors,
                        'detartrajes'=>$detartrajes,
                        'remocionPlacas'=>$remocionPlacas,
                        'edad'=>$pacienteCon->calcularEdad($examenGeneral->getIdPaciente()->getFechaNacimiento()->format('Y-m-d'))
                    ]);  
                      
                    
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    

    /**
     * Metodo utilizado para la generación de un pdf de un examen general del paciente.
     *
     * @Route("/{idExamenGeneral}/pdf", name="examenGeneral_pdf", methods={"GET","POST"})
     */
    public function pdfExamenGeneral(Request $request, ExamenGeneral $examenGeneral,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'examenes generales');  
                if (sizeof($permisos) > 0) {  
                    $pacienteCon=new PacienteController();
                    $filename="ExamenGeneral_N°_".$examenGeneral->getIdExamenGeneral()."_fecha_". date("Y_m_d") ."_Paciente_".$examenGeneral->getIdPaciente().".pdf";

                    $html =$this->renderView('examenGeneral/pdf.html.twig',
                    array(
                    'examenGeneral' => $examenGeneral,
                    'edad'=>$pacienteCon->calcularEdad($examenGeneral->getIdPaciente()->getFechaNacimiento()->format('Y-m-d'))
                    //'elementoOrdenCompras' => $elementoOrdenCompras,
                    //'idRequerimiento'=>$idRequerimiento
                    ));
                     
                    $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                    [   'encoding' => 'UTF-8',
                        'images' => false,
                        'enable-javascript' => true,
                        'print-media-type' => true,
                        'outline-depth' => 8,
                        'orientation' => 'Portrait',
                        'header-right'=>'Pag. [page] de [toPage]',
                        'page-size' => 'A4',
                        'viewport-size' => '1280x1024',
                        'margin-left' => '10mm',
                        'margin-right' => '10mm',
                        'margin-top' => '30mm',
                        'margin-bottom' => '25mm',
                        'user-style-sheet'=> 'light/dist/assets/css/bootstrap.min.css',
                        //'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
                        //'background'     => 'light/dist/assets/images/users/user-1.jpg',
                        //--javascript-delay 3000
                    ]);
                        $response = new Response ($pdf,
                        200,
                        array('Content-Type' => 'aplicattion/pdf',
                              'Content-Disposition' => 'inline; filename="'.$filename.'"'
                        ,));
                    
                    return $response;
                    //return $this->render('examenGeneral/pdf.html.twig', [
                    //    'examenGeneral' => $examenGeneral
                    //]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la eliminación de un examen general del paciente.
     *
     * @Route("/{idExamenGeneral}", name="examenGeneral_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ExamenGeneral $examenGeneral,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'examenes generales');  
              if (sizeof($permisos) > 0) {
                //var_dump($esterilizacion->getIdEsterilizacion());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($examenGeneral);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('examenGeneral_new',['idPaciente'=>$examenGeneral->getIdPaciente()->getIdPaciente()]);
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('examenGeneral_new',['idPaciente'=>$examenGeneral->getIdPaciente()->getIdPaciente()]);
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('examenGeneral_new',['idPaciente'=>$examenGeneral->getIdPaciente()->getIdPaciente()]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        } 
    }
}
