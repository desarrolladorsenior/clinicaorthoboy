<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/*
* Controlador de panel principal utilizado para redireccionar las pantallas principales.
*
*/
class PanelPrincipalController extends AbstractController
{
    /**
     * Metodo utilizado para la redirección de la primera pantalla de visualización de los usuaqrios segun sus permisos y el estado del sistema.
     *
     * @Route("/panelPrincipal", name="panel_principal")
     */
    public function index(Request $request)
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $em = $this->getDoctrine()->getManager();

                $dqlCantidadUsuario = "SELECT count(usuario) FROM App:usuario usuario where usuario.estado = :estado ";
                $queryCantidadUsuario = $em->createQuery($dqlCantidadUsuario)->setParameter('estado',true);
                $cantidadUsuarios = $queryCantidadUsuario->getResult();
                foreach ($cantidadUsuarios as $key => $value) {
                     foreach ($value as $can ) {
                        $numUsuarios= $can;  
                     }                
                }

                $dqlClinica = "SELECT clinica FROM App:Clinica clinica";
                $queryClinica = $em->createQuery($dqlClinica);
                $clinicas = $queryClinica->getResult();
                foreach ($clinicas as $clinicaSeleccionada) {
                    $direccion=$clinicaSeleccionada->getDireccion();
                    $idClinica=$clinicaSeleccionada->getIdClinica();
                } 
                if ($numUsuarios == 1 and $direccion == '') {
                   $nombres=$this->get('security.token_storage')->getToken()->getUser()->getNombres();
                   $this->addFlash('mensaje','Bienvenid@  '.$nombres.' a la plataforma. Es necesario que personalice los datos de la clinica!');
                    return $this->redirectToRoute('clinica_edit',[
                            'idClinica' => $idClinica,
                         ]);      
                }else{
                    $tipoUsuario=$this->get('security.token_storage')->getToken()->getUser()->getTipoUsuario();
                    if ($tipoUsuario == 'EXTERNO') {
                        return $this->redirectToRoute('externo_newAnexo');
                    }else{
                        return $this->render('panel_principal/index.html.twig', [
                            'controller_name' => 'PanelPrincipalController',
                         ]);
                    }    
                }
                
                
                
            }
        }        
    }

     /**
     * Metodo utilizado para mostrar iconos, puede borrarce el metodo como las pantallas.
     *
     * @Route("/iconos", name="iconos_principal")
     */
    public function iconos()
    {
        return $this->render('panel_principal/iconos.html.twig');
    }

    /**
     * Metodo utilizado para mostrar iconos, puede borrarce el metodo como las pantallas.
     *
     * @Route("/iconosfa", name="iconosfa_principal")
     */
    public function iconosfa()
    {
        return $this->render('panel_principal/iconosFab.html.twig');
    }

    /**
     * Metodo utilizado para mostrar iconos, puede borrarce el metodo como las pantallas
     *
     * @Route("/iconosdri", name="iconosdri_principal")
     */
    public function iconosdri()
    {
        return $this->render('panel_principal/iconosDri.html.twig');
    }

     /**
     * Metodo utilizado para mostrar iconos, puede borrarce el metodo como las pantallas
     *
     * @Route("/iconosfe", name="iconosfe_principal")
     */
    public function iconosfe()
    {
        return $this->render('panel_principal/iconosFe.html.twig');
    }

    /**
     * Metodo utilizado para mostrar iconos, puede borrarce el metodo como las pantallas
     *
     * @Route("/iconosIcon", name="iconosIcon_principal")
     */
    public function iconosIcon()
    {
        return $this->render('panel_principal/iconosIcon.html.twig');
    }

    /**
     * Metodo utilizado para mostrar iconos, puede borrarce el metodo como las pantallas
     *
     * @Route("/iconosWi", name="iconosWi_principal")
     */
    public function iconosWI()
    {
        return $this->render('panel_principal/iconosWi.html.twig');
    }

    /**
     * Metodo utilizado para mostrar iconos, puede borrarce el metodo como las pantallas
     *
     * @Route("/iconosTi", name="iconosTi_principal")
     */
    public function iconosTi()
    {
        return $this->render('panel_principal/iconsTI.html.twig');
    }

     /**
     * Metodo utilizado para mostrar iconos, puede borrarce el metodo como las pantallas
     *
     * @Route("/botones", name="botones_principal")
     */
    public function botones()
    {
        return $this->render('panel_principal/botones.html.twig');
    }

    
}
