<?php

namespace App\Controller;

use App\Entity\Tratamiento;
use App\Form\TratamientoType;
use App\Entity\CategoriaTratamiento;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de tratamiento utilizado para el manejo de los mismos asociados a un paciente y para generar planes de tratamientos.
 *
 * @Route("/tratamiento")
 */
class TratamientoController extends AbstractController
{

    /**
     * Metodo utilizado para la edición del estado de un tratamiento.
     *
     * @Route("/editarEstadoTratamiento", name="tratamiento_editestado", methods={"GET","POST"})
     */
    public function editEstado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tratamientos principal');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $id=$request->request->get('id');
                    $estado=$request->request->get('estado');
                    if($estado == 'ACTIVO'){
                        $estado = true;
                    }else{
                        $estado = false;
                    }
                    $dqlModificarEstadoTratamiento="UPDATE App:Tratamiento tratamiento SET tratamiento.estado= :estado WHERE tratamiento.idTratamiento= :idTratamiento";
                    $queryModificarEstadoTratamiento=$em->createQuery($dqlModificarEstadoTratamiento)->setParameter('estado', $estado)->setParameter('idTratamiento', $id);
                    $modificarEstadoTratamiento=$queryModificarEstadoTratamiento->execute();
                    $response->setData('Se ha cambiado el estado de forma correcta!');
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para el registro de un tratamiento contando que la categoria de tratamientos no tengan el mismo nombre de tratamiento.
     *
     * @Route("/{idCategoriaTratamiento}/new", name="tratamiento_new", methods={"GET","POST"})
     */
    public function new(Request $request,PerfilGenerator $permisoPerfil,CategoriaTratamiento $categoriaTratamiento): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Expresión en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tratamientos principal'); 
                if (sizeof($permisos) > 0 ) {
                    $response= new JsonResponse();
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlTratamientos ="SELECT tratamiento FROM App:Tratamiento tratamiento WHERE tratamiento.idCategoriaTratamiento= :idCategoriaTratamiento ORDER BY tratamiento.idTratamiento DESC";  
                    $queryTratamientos = $entityManager->createQuery($dqlTratamientos)->setParameter('idCategoriaTratamiento',$categoriaTratamiento->getIdCategoriaTratamiento());
                    $tratamientos= $queryTratamientos->getResult();

                    $tratamiento = new Tratamiento();
                    $tratamiento->setIdCategoriaTratamiento($categoriaTratamiento);
                    $tratamiento->setEstado(TRUE);
                    $tratamiento->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(TratamientoType::class, $tratamiento);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $dqlTratamientoExistente ="SELECT tratamiento FROM App:Tratamiento tratamiento WHERE  tratamiento.nombre = :nombre and tratamiento.idCategoriaTratamiento = :idCategoriaTratamiento";  
                        $queryTratamientoExistente = $entityManager->createQuery($dqlTratamientoExistente)->setParameter('nombre',strtoupper($tratamiento->getNombre()))->setParameter('idCategoriaTratamiento',$tratamiento->getIdCategoriaTratamiento()->getIdCategoriaTratamiento());
                        $tratamientoExistente =$queryTratamientoExistente->getResult();
                        if (sizeof($tratamientoExistente) == 0 ) {
                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->persist($tratamiento);
                            $entityManager->flush();
                        
                            $dqlTratamiento ="SELECT tratamiento FROM App:Tratamiento tratamiento WHERE tratamiento.idCategoriaTratamiento= :idCategoriaTratamiento ORDER BY tratamiento.idTratamiento DESC";  
                            $queryTratamiento = $entityManager->createQuery($dqlTratamiento)->setParameter('idCategoriaTratamiento',$categoriaTratamiento->getIdCategoriaTratamiento());
                            $tratamientoDos= $queryTratamiento->getResult();
                            $template = $this->render('tratamiento/datos.html.twig', [
                            'tratamientos'=>$tratamientoDos
                            ])->getContent();
                            return new JsonResponse(['mensaje'=> 'Se ha realizado el registro de forma correcta!','tratamientos'=>$template,'error'=>'vacio']); 
                        }else{
                            return new JsonResponse(['error'=>'No se ha realizado el registro, usted cuenta con un tratamiento con este nombre en la categoría!.']); 
                        }
                    }

                    return $this->render('tratamiento/new.html.twig', [
                        'tratamiento' => $tratamiento,
                        'form' => $form->createView(),
                        'categoriaTratamiento'=>$categoriaTratamiento,
                        'tratamientos'=>$tratamientos
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la edición de un tratamiento.
     *
     * @Route("/{idTratamiento}/edit", name="tratamiento_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tratamiento $tratamiento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Expresión en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'categoria de tratamiento'); 
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $form = $this->createForm(TratamientoType::class, $tratamiento);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $dqlTratamientoExistente ="SELECT tratamiento FROM App:Tratamiento tratamiento WHERE  tratamiento.nombre = :nombre and tratamiento.idCategoriaTratamiento = :idCategoriaTratamiento and tratamiento.idTratamiento != :idTratamiento";  
                        $queryTratamientoExistente = $entityManager->createQuery($dqlTratamientoExistente)->setParameter('nombre',strtoupper($tratamiento->getNombre()))->setParameter('idTratamiento',$tratamiento->getIdTratamiento())->setParameter('idCategoriaTratamiento',$tratamiento->getIdCategoriaTratamiento()->getIdCategoriaTratamiento());
                        $tratamientoExistente =$queryTratamientoExistente->getResult();
                        if (sizeof($tratamientoExistente) == 0 ) {
                            $entityManager->persist($categoriaTratamiento);
                            $entityManager->flush();
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                            return $this->redirectToRoute('categoriaTratamiento_index');
                        }else{
                            $this->addFlash('error', 'No se ha realizado la actualización, usted cuenta con un tratamiento con este nombre en la categoría!');
                            return $this->redirectToRoute('categoriaTratamiento_index');  
                        }
                    }

                    return $this->render('tratamiento/modalEdit.html.twig', [
                        'tratamiento' => $tratamiento,
                        'form' => $form->createView(),
                    ]);
                 }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de un tratamiento.
     *
     * @Route("/{idTratamiento}", name="tratamiento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tratamiento $tratamiento,PerfilGenerator $permisoPerfil): Response
    {
       ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'tratamientos principal');  
              if (sizeof($permisos) > 0) {
                //var_dump($esterilizacion->getIdEsterilizacion());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($tratamiento);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    if ($request->request->get('ruta') == 'nuevo') {
                        return $this->redirectToRoute('tratamiento_new',['idCategoriaTratamiento'=>$tratamiento->getIdCategoriaTratamiento()->getIdCategoriaTratamiento()]);
                    }else{
                        return $this->redirectToRoute('categoriaTratamiento_index');
                    }
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('categoriaTratamiento_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('categoriaTratamiento_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        } 
    }
}
