<?php

namespace App\Controller;

use App\Entity\Propiedad;
use App\Form\PropiedadType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de propiedad utilizado para el manejo del examen de endodoncia de un paciente.
 *
 * @Route("/propiedad")
 */
class PropiedadController extends AbstractController
{
    /**
    * Metodo utilizado para la visualización de una propiedad y registro de la misma.
    *
     * @Route("/", name="propiedad_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'propiedades');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlPropiedad="SELECT propiedad FROM App:Propiedad propiedad
                    ORDER BY propiedad.idPropiedad DESC";  
                    $queryPropiedad = $entityManager->createQuery($dqlPropiedad);
                    $propiedades= $queryPropiedad->getResult();
                    
                    $propiedad = new Propiedad();
                    $form = $this->createForm(PropiedadType::class, $propiedad);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $propiedad->setEstado(true);
                        $entityManager->persist($propiedad);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('propiedad_index');
                    }
                    return $this->render('propiedad/index.html.twig', [
                        'propiedades' => $propiedades,
                        'propiedad' => $propiedad,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la edición del estado de una propiedad.
     * 
     * @Route("/editarEstadoPropiedad", name="propiedad__editarEstado", methods={"GET","POST"})
     */
    public function editEstadoEvolucionPropiedad(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'propiedades');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $em=$this->getDoctrine()->getManager();
                $response = new JsonResponse();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }
                $dqlModificarEstado="UPDATE App:Propiedad propiedades SET propiedades.estado= :estado WHERE propiedades.idPropiedad= :idPropiedades";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idPropiedades', $id);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    

    }

    /**
     * Metodo utilizado para la edición de una propiedad.
     * 
     * @Route("/{idPropiedad}/edit", name="propiedad_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Propiedad $propiedad,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'propiedades');  
                if (sizeof($permisos) > 0) {
                    $form = $this->createForm(PropiedadType::class, $propiedad);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('propiedad_index');
                    }
                    return $this->render('propiedad/modalEdit.html.twig', [
                        'propiedad' => $propiedad,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de una propiedad.
     * 
     * @Route("/{idPropiedad}", name="propiedad_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Propiedad $propiedad,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'propiedades');  
              if (sizeof($permisos) > 0) {
                try{

                    if ($this->isCsrfTokenValid('delete'.$propiedad->getIdPropiedad(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($propiedad);
                        $entityManager->flush();
                        $successMessage= 'Se ha eliminado de forma correcta!';
                        $this->addFlash('mensaje',$successMessage);
                        return $this->redirectToRoute('propiedad_index');
                    }

                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('propiedad_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('propiedad_index|');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }    
    }
}
