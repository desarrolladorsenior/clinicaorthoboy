<?php

namespace App\Controller;

use App\Entity\Perfil;
use App\Entity\PerfilPermiso;
use App\Form\PerfilType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\PerfilGenerator;

/**
 * Controlador de perfil utilizado para el manejo de permisos de un perfil y de aqui se descompone todos los permisos que puede llegar a tener un usuario, según los permisos que tengan asignados a su perfil.
 *
 * @Route("/perfil")
 */
class PerfilController extends AbstractController
{
    /**
     * Metodo utilizado para la edición del estado de un perfil.
     *
     * @Route("/editarEstadoPerfil", name="perfil_editEstado", methods={"GET","POST"})
     */
    public function editEstado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfiles=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfiles,'perfil');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();  
                        $id=$request->request->get('id');
                        $estado=$request->request->get('estado');
                        if($estado == 'ACTIVO'){
                            $estado = true;
                        }else{
                            $estado = false;
                        }
                        $entityManager = $this->getDoctrine()->getManager();
                        $dqlPerfil ="SELECT perfil FROM App:Perfil perfil
                        WHERE perfil.idPerfil = :perfil";  
                        $queryPerfil= $entityManager->createQuery($dqlPerfil)->setParameter('perfil',$id);
                        $perfiles= $queryPerfil->getResult();
                        foreach ($perfiles as $seleccionPerfil) {
                            $nombrePerfil=$seleccionPerfil->getNombre();
                        }
                        if ($nombrePerfil != 'ROLE_SUPERADMINISTRADOR') {
                         
                            $dqlModificarEstado="UPDATE App:Perfil perfil SET perfil.estado= :estado WHERE perfil.idPerfil= :idPerfil";
                            $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idPerfil', $id);
                            $modificarEstado=$queryModificarEstado->execute();
                            $response->setData('Se ha cambiado el estado de forma correcta!');
                            return $response;
                        }else{
                            $response->setData('No se permiten modificaciones al perfil, predeterminado por el sistema!');
                            return $response;
                        }
                }else{
                    $response->setData(['error'=>'Este recurso no es permitido para su perfil!']);
                    return $response;
                }     
            }
        }        


    }
    
    /**
     * Metodo utilizado para la visualización de todos los perfiles.
     *
     * @Route("/", name="perfil_index", methods={"GET"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfiles=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfiles,'perfil');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlPerfil ="SELECT perfil FROM App:Perfil perfil
                    ORDER BY perfil.idPerfil DESC";  
                    $queryPerfil= $entityManager->createQuery($dqlPerfil);
                    $perfiles= $queryPerfil->getResult();
                   
                    return $this->render('perfil/index.html.twig', [
                        'perfiles' => $perfiles,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }     
            }
        }           
    }


    /**
     * Metodo utilizado para el registro de perfiles.
     *
     * @Route("/new", name="perfil_new", methods={"GET","POST"})
     */
    public function new(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfiles=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfiles,'perfil');  
                if (sizeof($permisos) > 0) {
                    $perfil = new Perfil();
                    $form = $this->createForm(PerfilType::class, $perfil);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $perfil->getEstado($perfil->setEstado(true));
                        $entityManager->persist($perfil);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('perfil_permisos',[
                        'idPerfil' => $perfil->getIdPerfil()]);
                    }

                    return $this->render('perfil/new.html.twig', [
                        'perfil' => $perfil,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                } 
            }
        }        
    }

    /**
     * Metodo utilizado para la visualización de un perfil y sus permisos actuales.
     *
     * @Route("/{idPerfil}", name="perfil_show", methods={"GET","POST"})
     */
    public function show(Perfil $perfil,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfiles=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfiles,'perfil');  
                if (sizeof($permisos) > 0) {
                    $idPerfil=$perfil->getIdPerfil();
                    $entityManager = $this->getDoctrine()->getManager();

                    $dqlPerfilPermisoAgenda ="SELECT perfilPermiso FROM App:PerfilPermiso perfilPermiso INNER JOIN App:Permiso permiso WITH  perfilPermiso.idPermiso=permiso.idPermiso 
                    where perfilPermiso.idPerfil = :idPerfil and permiso.modulo = :modulo";  
                    $queryPerfilPermisoAgenda= $entityManager->createQuery($dqlPerfilPermisoAgenda)->setParameter('idPerfil',$idPerfil)->setParameter('modulo','agenda');
                    $perfilPermisosAgenda= $queryPerfilPermisoAgenda->getResult();
                   
                    $dqlPerfilPermisoPaciente ="SELECT perfilPermiso FROM App:PerfilPermiso perfilPermiso INNER JOIN App:Permiso permiso WITH  perfilPermiso.idPermiso=permiso.idPermiso 
                    where perfilPermiso.idPerfil = :idPerfil and permiso.modulo = :modulo";  
                    $queryPerfilPermisoPaciente= $entityManager->createQuery($dqlPerfilPermisoPaciente)->setParameter('idPerfil',$idPerfil)->setParameter('modulo','paciente');
                    $perfilPermisosPaciente= $queryPerfilPermisoPaciente->getResult();

                    $dqlPerfilPermisoReporte ="SELECT perfilPermiso FROM App:PerfilPermiso perfilPermiso INNER JOIN App:Permiso permiso WITH  perfilPermiso.idPermiso=permiso.idPermiso 
                    where perfilPermiso.idPerfil = :idPerfil and permiso.modulo = :modulo";  
                    $queryPerfilPermisoReportes= $entityManager->createQuery($dqlPerfilPermisoReporte)->setParameter('idPerfil',$idPerfil)->setParameter('modulo','reportes');
                    $perfilPermisosReportes= $queryPerfilPermisoReportes->getResult();

                    $dqlPerfilPermisoAdministracion ="SELECT perfilPermiso FROM App:PerfilPermiso perfilPermiso INNER JOIN App:Permiso permiso WITH  perfilPermiso.idPermiso=permiso.idPermiso 
                    where perfilPermiso.idPerfil = :idPerfil and permiso.modulo = :modulo";  
                    $queryPerfilPermisoAdministracion =$entityManager->createQuery($dqlPerfilPermisoAdministracion)->setParameter('idPerfil',$idPerfil)->setParameter('modulo','administracion');
                    $perfilPermisosAdministracion= $queryPerfilPermisoAdministracion->getResult();
                   
                    $dqlPerfilPermisoConfiguracion ="SELECT perfilPermiso FROM App:PerfilPermiso perfilPermiso INNER JOIN App:Permiso permiso WITH  perfilPermiso.idPermiso=permiso.idPermiso 
                    where perfilPermiso.idPerfil = :idPerfil and permiso.modulo = :modulo";  
                    $queryPerfilPermisoConfiguracion= $entityManager->createQuery($dqlPerfilPermisoConfiguracion)->setParameter('idPerfil',$idPerfil)->setParameter('modulo','configuracion');
                    $perfilPermisosConfiguracion= $queryPerfilPermisoConfiguracion->getResult();
                   
                    return $this->render('perfil/modalShow.html.twig', [
                        'perfil' => $perfil,
                        'perfilPermisosAgenda'=>$perfilPermisosAgenda,
                        'perfilPermisosPaciente'=>$perfilPermisosPaciente,
                        'perfilPermisosReportes'=>$perfilPermisosReportes,
                        'perfilPermisosAdministracion'=>$perfilPermisosAdministracion,
                        'perfilPermisosConfiguracion'=>$perfilPermisosConfiguracion
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                } 
            }
        }        
    }

    /**
     * Metodo utilizado para el registro de permisos a un perfil especifico.
     *
     * @Route("/{idPerfil}/permiso", name="perfil_permisos", methods={"GET","POST"})
     */
    public function perfilPermiso(Perfil $perfil,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfiles=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfiles,'perfil');
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $arrayPermisos=array();
                    if (!empty($request->request->get('agenda'))) {
                        $datosAgenda= $request->request->get('agenda');
                        if (!empty($datosAgenda['principal'])) {
                            $dqlAgenda="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo ORDER BY permiso.idPermiso ASC";
                            $queryAgenda=$entityManager->createQuery($dqlAgenda)->setParameter('modulo','agenda');
                            $agendas=$queryAgenda->getResult();
                            foreach ($agendas as $agendaSeleccionada ) {             
                               $permisoAgenda=$agendaSeleccionada->getIdPermiso();
                               array_push($arrayPermisos,$permisoAgenda);
                            }
                        }else{
                            if (!empty($datosAgenda['secundaria'])) {
                                $dqlAgenda="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                $queryAgenda=$entityManager->createQuery($dqlAgenda)->setParameter('modulo','agenda')->setParameter('nombre','%'.$datosAgenda['secundaria'].'%');
                                $agendas=$queryAgenda->getResult();
                                foreach ($agendas as $agendaSeleccionada ) {             
                                   $permisoAgenda=$agendaSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoAgenda);
                                }
                            }
                        }
                    }

                    if (!empty($request->request->get('paciente'))) {
                        $datosPaciente= $request->request->get('paciente');
                        if (!empty($datosPaciente['principal'])) {
                            $dqlPaciente="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo ORDER BY permiso.idPermiso ASC";
                            $queryPaciente=$entityManager->createQuery($dqlPaciente)->setParameter('modulo','paciente');
                            $pacientes=$queryPaciente->getResult();
                            foreach ($pacientes as $pacienteSeleccionado ) {             
                               $permisoAgenda=$pacienteSeleccionado->getIdPermiso();
                               array_push($arrayPermisos,$permisoAgenda);
                            }
                        }else{
                            if (!empty($datosPaciente['fichaPaciente'])) {
                                $arrayPosiblesNombre=array($datosPaciente['datosPersonal'],$datosPaciente['examenGeneral'],$datosPaciente['odontograma'],$datosPaciente['periodontograma'],$datosPaciente['endodoncia'],$datosPaciente['cirugiaOral'],$datosPaciente['operatoriaEstetica'],$datosPaciente['odontoPediatria'],$datosPaciente['pacientePlanTratamiento'],$datosPaciente['documentoConsentimiento'],$datosPaciente['documentoFormulaMedica'],$datosPaciente['documentoRemision'],$datosPaciente['documentoLaboratorio'],$datosPaciente['documentoRadiografia'],$datosPaciente['documentoArchivo'],$datosPaciente['documentoAnexo'],$datosPaciente['imagenes']);
                                 /*$dqlPacienteFicha=$entityManager->createQueryBuilder();
                                  $dqlPacienteFicha->select('permiso')
                                   ->from('App:Permiso', 'permiso')->Where("permiso.nombre like :nombre");*/
                                $dqlPacienteFicha="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayPosiblesNombre as $i => $nombre) {
                                    $queryPacienteFicha=$entityManager->createQuery($dqlPacienteFicha)->setParameter("nombre",'%'.$nombre.'%')->setParameter('modulo','paciente');
                                    $pacientesFichas=$queryPacienteFicha->getResult();
                                    foreach ($pacientesFichas as $pacienteFichaSeleccionada ) {             
                                    $permisoPacienteFicha=$pacienteFichaSeleccionada->getIdPermiso();
                                    array_push($arrayPermisos,$permisoPacienteFicha);
                                    }
                                }
                            }else{
                                if (!empty($datosPaciente['datosPersonal'])) {
                                    $dqlPacienteDatosPersonales="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryPacienteDatosPersonal=$entityManager->createQuery($dqlPacienteDatosPersonales)->setParameter("nombre",'%'.$datosPaciente['datosPersonal'].'%')->setParameter('modulo','paciente');
                                    $pacientesPersonalesDatos=$queryPacienteDatosPersonal->getResult();
                                    foreach ($pacientesPersonalesDatos as $pacienteDatoPersonalSeleccionada ) {             
                                    $permisoPacienteDP=$pacienteDatoPersonalSeleccionada->getIdPermiso();
                                    array_push($arrayPermisos,$permisoPacienteDP);
                                    }
                                }
                                if (!empty($datosPaciente['examenGeneral'])) {
                                    $dqlPacienteExamenGeneral="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                    $queryPacienteExamenGeneral=$entityManager->createQuery($dqlPacienteExamenGeneral)->setParameter("nombre",'%'.$datosPaciente['examenGeneral'].'%')->setParameter('modulo','paciente');
                                    $pacientesExamenGeneral=$queryPacienteExamenGeneral->getResult();
                                    foreach ($pacientesExamenGeneral as $pacienteExamenGeneralSeleccionada ) {             
                                    $permisoPacienteExamenGeneral=$pacienteExamenGeneralSeleccionada->getIdPermiso();
                                    array_push($arrayPermisos,$permisoPacienteExamenGeneral);
                                    }
                                }
                                if (!empty($datosPaciente['examenes'])) {
                                    $arrayPosiblesNombreExamen=array($datosPaciente['odontograma'],$datosPaciente['ortodoncia'],$datosPaciente['periodontograma'],$datosPaciente['endodoncia'],$datosPaciente['cirugiaOral'],$datosPaciente['operatoriaEstetica'],$datosPaciente['odontoPediatria']);
                                    $dqlPacienteExamenes="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayPosiblesNombreExamen as $i => $nombre) {
                                        $queryPacienteExamenes=$entityManager->createQuery($dqlPacienteExamenes)->setParameter("nombre",'%'.$nombre.'%')->setParameter('modulo','paciente');
                                        $pacientesExamenes=$queryPacienteExamenes->getResult();
                                        foreach ($pacientesExamenes as $pacienteExamenSeleccionado ) {             
                                        $permisoPacienteExamen=$pacienteExamenSeleccionado->getIdPermiso();
                                        array_push($arrayPermisos,$permisoPacienteExamen);
                                        }
                                    }
                                }else{
                                    if (!empty($datosPaciente['odontograma'])) {
                                        $dqlPacienteOdontograma="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacienteOdontograma=$entityManager->createQuery($dqlPacienteOdontograma)->setParameter("nombre",'%'.$datosPaciente['odontograma'].'%')->setParameter('modulo','paciente');
                                        $pacientesOdontograma=$queryPacienteOdontograma->getResult();
                                        foreach ($pacientesOdontograma as $pacienteOdontogramaSeleccionada ) {             
                                            $permisoPacienteOdontograma=$pacienteOdontogramaSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteOdontograma);
                                        }
                                    }

                                    if (!empty($datosPaciente['ortodoncia'])) {
                                        $dqlPacienteOdontograma="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacienteOdontograma=$entityManager->createQuery($dqlPacienteOdontograma)->setParameter("nombre",'%'.$datosPaciente['ortodoncia'].'%')->setParameter('modulo','paciente');
                                        $pacientesOdontograma=$queryPacienteOdontograma->getResult();
                                        foreach ($pacientesOdontograma as $pacienteOdontogramaSeleccionada ) {             
                                            $permisoPacienteOdontograma=$pacienteOdontogramaSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteOdontograma);
                                        }
                                    }

                                    if (!empty($datosPaciente['periodontograma'])) {
                                        $dqlPacientePeriodontograma="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacientePeriodontograma=$entityManager->createQuery($dqlPacientePeriodontograma)->setParameter("nombre",'%'.$datosPaciente['periodontograma'].'%')->setParameter('modulo','paciente');
                                        $pacientesPeriodontograma=$queryPacientePeriodontograma->getResult();
                                        foreach ($pacientesPeriodontograma as $pacientePeriodontogramaSeleccionada ) {             
                                            $permisoPacientePeriodontograma=$pacientePeriodontogramaSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacientePeriodontograma);
                                        }
                                    }
                                    if (!empty($datosPaciente['endodoncia'])) {
                                        $dqlPacienteEndodoncia="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                        $queryPacienteEndodoncia=$entityManager->createQuery($dqlPacienteEndodoncia)->setParameter("nombre",'%'.$datosPaciente['endodoncia'].'%')->setParameter('modulo','paciente');
                                        $pacientesEndodoncia=$queryPacienteEndodoncia->getResult();
                                        foreach ($pacientesEndodoncia as $pacienteEndodonciaSeleccionada ) {             
                                            $permisoPacienteEndodoncia=$pacienteEndodonciaSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteEndodoncia);
                                        }
                                    }
                                    if (!empty($datosPaciente['cirugiaOral'])) {
                                        $dqlPacienteCirugia="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacienteCirugia=$entityManager->createQuery($dqlPacienteCirugia)->setParameter("nombre",'%'.$datosPaciente['cirugiaOral'].'%')->setParameter('modulo','paciente');
                                        $pacientesCirugia=$queryPacienteCirugia->getResult();
                                        foreach ($pacientesCirugia as $pacienteCirugiaSeleccionada ) {             
                                            $permisoPacienteCirugia=$pacienteCirugiaSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteCirugia);
                                        }
                                    }
                                    if (!empty($datosPaciente['operatoriaEstetica'])) {
                                        $dqlPacienteOperatoria="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacienteOperatoria=$entityManager->createQuery($dqlPacienteOperatoria)->setParameter("nombre",'%'.$datosPaciente['operatoriaEstetica'].'%')->setParameter('modulo','paciente');
                                        $pacientesOperatoria=$queryPacienteOperatoria->getResult();
                                        foreach ($pacientesOperatoria as $pacienteOperatoriaSeleccionada ) {             
                                            $permisoPacienteOperatoria=$pacienteOperatoriaSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteOperatoria);
                                        }
                                    }
                                    if (!empty($datosPaciente['odontoPediatria'])) {
                                        $dqlPacienteOdontoPediatria="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacienteOdontoPediatria=$entityManager->createQuery($dqlPacienteOdontoPediatria)->setParameter("nombre",'%'.$datosPaciente['odontoPediatria'].'%')->setParameter('modulo','paciente');
                                        $pacientesOdontoPediatria=$queryPacienteOdontoPediatria->getResult();
                                        foreach ($pacientesOdontoPediatria as $pacienteOdontoPediatriaSeleccionada ) {
                                            $permisoPacienteOdontoPediatria=$pacienteOdontoPediatriaSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteOdontoPediatria);
                                        }
                                    }
                                }

                                if (!empty($datosPaciente['pacientePlanTratamiento'])) {
                                    $dqlPacientePlan="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                         
                                    $queryPacientePlan=$entityManager->createQuery($dqlPacientePlan)->setParameter("nombre",'%'.$datosPaciente['pacientePlanTratamiento'].'%')->setParameter('modulo','paciente');
                                    $pacientesPlan=$queryPacientePlan->getResult();
                                    foreach ($pacientesPlan as $pacientePlanSeleccionada ) {
                                        $permisoPacientePlan=$pacientePlanSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoPacientePlan);
                                    }
                                }

                                if (!empty($datosPaciente['documento'])) {
                                    $arrayPosiblesNombreDoc=array($datosPaciente['documentoConsentimiento'],$datosPaciente['documentoFormulaMedica'],$datosPaciente['documentoRemision'],$datosPaciente['documentoLaboratorio'],$datosPaciente['documentoRadiografia'],$datosPaciente['documentoArchivo'],$datosPaciente['documentoAnexo']);

                                    $dqlPacienteDoc="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayPosiblesNombreDoc as $i => $nombre) {
                                        $queryPacienteDoc=$entityManager->createQuery($dqlPacienteDoc)->setParameter("nombre",'%'.$nombre.'%')->setParameter('modulo','paciente');
                                        $pacientesDoc=$queryPacienteDoc->getResult();
                                        foreach ($pacientesDoc as $pacienteDocSeleccionada ) {             
                                            $permisoPacienteDoc=$pacienteDocSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteDoc);
                                        }
                                    }
                                }else{
                                    if (!empty($datosPaciente['documentoConsentimiento'])) {
                                        $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoConsentimiento'].'%')->setParameter('modulo','paciente');
                                        $pacientesConsens=$queryPacienteDocConse->getResult();
                                        foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                            $permisoPacienteConsentimiento=$pacienteConseSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteConsentimiento);
                                        }
                                    }
                                    if (!empty($datosPaciente['documentoFormulaMedica'])) {
                                        $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoFormulaMedica'].'%')->setParameter('modulo','paciente');
                                        $pacientesConsens=$queryPacienteDocConse->getResult();
                                        foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                            $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteDoc);
                                        }
                                    }
                                    if (!empty($datosPaciente['documentoRemision'])) {
                                        $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                        $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoRemision'].'%')->setParameter('modulo','paciente');
                                        $pacientesConsens=$queryPacienteDocConse->getResult();
                                        foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                            $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteDoc);
                                        }
                                    }
                                    if (!empty($datosPaciente['documentoLaboratorio'])) {
                                        $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                        $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoLaboratorio'].'%')->setParameter('modulo','paciente');
                                        $pacientesConsens=$queryPacienteDocConse->getResult();
                                        foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                            $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteDoc);
                                        }
                                    }
                                    if (!empty($datosPaciente['documentoRadiografia'])) {
                                        $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoRadiografia'].'%')->setParameter('modulo','paciente');
                                        $pacientesConsens=$queryPacienteDocConse->getResult();
                                        foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                            $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteDoc);
                                        }
                                    }
                                    if (!empty($datosPaciente['documentoArchivo'])) {
                                        $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                        $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoArchivo'].'%')->setParameter('modulo','paciente');
                                        $pacientesConsens=$queryPacienteDocConse->getResult();
                                        foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                            $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteDoc);
                                        }
                                    }
                                    if (!empty($datosPaciente['documentoAnexo'])) {
                                        $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                          
                                        $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoAnexo'].'%')->setParameter('modulo','paciente');
                                        $pacientesConsens=$queryPacienteDocConse->getResult();
                                        foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                            $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteDoc);
                                        }
                                    }
                                }

                                if (!empty($datosPaciente['imagenes'])) {
                                    $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                    $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['imagenes'].'%')->setParameter('modulo','paciente');
                                    $pacientesConsens=$queryPacienteDocConse->getResult();
                                    foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                        $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoPacienteDoc);
                                    }
                                }
                            }

                            if (!empty($datosPaciente['historialPacienteEvolucion'])) {
                                $dqlPacienteEvolucion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                          
                                $queryPacienteEvolucion=$entityManager->createQuery($dqlPacienteEvolucion)->setParameter("nombre",'%'.$datosPaciente['historialPacienteEvolucion'].'%')->setParameter('modulo','paciente');
                                $pacientesEvolucion=$queryPacienteEvolucion->getResult();
                                foreach ($pacientesEvolucion as $pacienteEvoluSeleccionada ) {
                                    $permisoPacienteEvo=$pacienteEvoluSeleccionada->getIdPermiso();
                                    array_push($arrayPermisos,$permisoPacienteEvo);
                                }
                            }

                            if (!empty($datosPaciente['pagos'])) {
                                $dqlPacientePago="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                $queryPacientePago=$entityManager->createQuery($dqlPacientePago)->setParameter("nombre",'%'.$datosPaciente['pagos'].'%')->setParameter('modulo','paciente');
                                $pacientesPago=$queryPacientePago->getResult();
                                foreach ($pacientesPago as $pacientePagoSeleccionada ) {
                                    $permisoPacientePago=$pacientePagoSeleccionada->getIdPermiso();
                                    array_push($arrayPermisos,$permisoPacientePago);
                                }
                            }
                            if (!empty($datosPaciente['historialCitas']) ){
                                $dqlPacienteHistoCita="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                $queryPacienteHistoCita=$entityManager->createQuery($dqlPacienteHistoCita)->setParameter("nombre",'%'.$datosPaciente['historialCitas'].'%')->setParameter('modulo','paciente');
                                $pacientesHistoCita=$queryPacienteHistoCita->getResult();
                                foreach ($pacientesHistoCita as $pacienteHCSeleccionada ) {
                                    $permisoPacienteHC=$pacienteHCSeleccionada->getIdPermiso();
                                    array_push($arrayPermisos,$permisoPacienteHC);
                                }
                            }
                        }

                    }

                    if (!empty($request->request->get('administracion'))) {
                        $datosAdministracion= $request->request->get('administracion');
                        if (!empty($datosAdministracion['principal'])) {
                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo ORDER BY permiso.idPermiso ASC";
                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion');
                            $administracionDatos=$queryAdministracion->getResult();
                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                               array_push($arrayPermisos,$permisoAdministracion);
                            }
                        }else{
                            if (!empty($datosAdministracion['configuracionGeneral'])) {
                                $arrayNombresAdmiConfiguracionGeneral=array($datosAdministracion['confiMediAreaMedica'],$datosAdministracion['confiMediEspecialida'],$datosAdministracion['departamento'],$datosAdministracion['eps']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiConfiguracionGeneral as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }else{
                                if (!empty($datosAdministracion['configuracionMedica'])) {
                                    $arrayNombresAdmiConfiguracionMedica=array($datosAdministracion['confiMediAreaMedica'],$datosAdministracion['confiMediEspecialida']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConfiguracionMedica as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                    
                                }else{
                                    if (!empty($datosAdministracion['confiMediAreaMedica'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiMediAreaMedica'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['confiMediEspecialida'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiMediEspecialida'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['departamento'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['departamento'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['eps'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['eps'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }

                            if (!empty($datosAdministracion['gestionConsulta'])) {
                                $arrayNombresAdmiConsulta=array($datosAdministracion['finalidadConsulta'],$datosAdministracion['motivoConsulta']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiConsulta as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }else{
                                
                                if (!empty($datosAdministracion['finalidadConsulta'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['finalidadConsulta'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['motivoConsulta'])) {
                                   $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['motivoConsulta'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }
                            
                            if (!empty($datosAdministracion['gestionConvenio'])) {
                                $arrayNombresAdmiConvenio=array($datosAdministracion['convenio'],$datosAdministracion['tratamientoEps']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiConvenio as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                            }else{

                                if (!empty($datosAdministracion['convenio'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['convenio'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['tratamientoEps'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tratamientoEps'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }

                            if (!empty($datosAdministracion['gestionDatosClinicos'])) {
                                $arrayNombresAdmiDatosClinicos=array($datosAdministracion['confiExamGeAlergia'],$datosAdministracion['confiExamGeAntecedente'],$datosAdministracion['confiExamGeHabito'],$datosAdministracion['confiExamGeMedicamento'],$datosAdministracion['confiExamGeRiesgo'],$datosAdministracion['evolucionEndodoncia'],$datosAdministracion['propiedades'],$datosAdministracion['provocado'],$datosAdministracion['superficie']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiDatosClinicos as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }else{
                                if (!empty($datosAdministracion['configuracionExamenGeneral'])) {
                                    $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['confiExamGeAlergia'],$datosAdministracion['confiExamGeAntecedente'],$datosAdministracion['confiExamGeHabito'],$datosAdministracion['confiExamGeMedicamento'],$datosAdministracion['confiExamGeRiesgo']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                    
                                }else{
                                    if (!empty($datosAdministracion['confiExamGeAlergia'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeAlergia'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['confiExamGeAntecedente'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeAntecedente'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['confiExamGeHabito'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeHabito'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['confiExamGeMedicamento'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeMedicamento'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['confiExamGeRiesgo'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeRiesgo'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['confiDatosExamenEndododoncia'])) {
                                    $arrayNombresAdmiConfiExamenEndodoncia=array($datosAdministracion['evolucionEndodoncia'],$datosAdministracion['propiedades'],$datosAdministracion['provocado']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConfiExamenEndodoncia as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                    
                                }else{
                                    if (!empty($datosAdministracion['evolucionEndodoncia'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['evolucionEndodoncia'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['propiedades'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['propiedades'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['provocado'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['provocado'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['superficie'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['superficie'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }

                            if (!empty($datosAdministracion['gestionDiagnostico'])) {
                                $arrayNombresAdmiDiagnostico=array($datosAdministracion['categoriaDiagnostico'],$datosAdministracion['diagnosticoOdontologico'],$datosAdministracion['diagnosticoRipOdontologico'],$datosAdministracion['tipoDiagnosticoPrincipal']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiDiagnostico as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                            }else{

                                if (!empty($datosAdministracion['categoriaDiagnostico'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['categoriaDiagnostico'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['diagnosticoOdontologico'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['diagnosticoOdontologico'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['diagnosticoRipOdontologico'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['diagnosticoRipOdontologico'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['tipoDiagnosticoPrincipal'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoDiagnosticoPrincipal'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }

                            if (!empty($datosAdministracion['gestionDocumentacion'])) {
                                $arrayNombresAdmiDocumentacion=array($datosAdministracion['tipoArchivo'],$datosAdministracion['tipoConsentimiento'],$datosAdministracion['tipoContrato'],$datosAdministracion['tipoRadiografia']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiDocumentacion as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                            }else{

                                if (!empty($datosAdministracion['tipoArchivo'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoArchivo'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['tipoConsentimiento'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoConsentimiento'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['tipoContrato'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoContrato'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['tipoRadiografia'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoRadiografia'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }

                            if (!empty($datosAdministracion['gestionEstados'])) {
                                $arrayNombresAdmiEstados=array($datosAdministracion['estadoCita'],$datosAdministracion['estadoGeneral'],$datosAdministracion['estadoPago'],$datosAdministracion['estadoMantenimiento']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiEstados as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                            }else{

                                if (!empty($datosAdministracion['estadoCita'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['estadoCita'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['estadoGeneral'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['estadoGeneral'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['estadoPago'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['estadoPago'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['estadoMantenimiento'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['estadoMantenimiento'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }

                            if (!empty($datosAdministracion['gestionEsterilizacion'])) {
                                $arrayNombresAdmiEsterilizacion=array($datosAdministracion['esterilizacion'],$datosAdministracion['tipoEsterilizacion']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiEsterilizacion as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                            }else{

                                if (!empty($datosAdministracion['esterilizacion'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['esterilizacion'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['tipoEsterilizacion'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoEsterilizacion'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }


                            if (!empty($datosAdministracion['gestionFinanciera'])) {
                                $arrayNombresAdmiFinanzas=array($datosAdministracion['acuerdoPagoUsuario'],$datosAdministracion['conceptoPago'],$datosAdministracion['formaPago'],$datosAdministracion['soportePago']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiFinanzas as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                            }else{

                                if (!empty($datosAdministracion['acuerdoPagoUsuario'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['acuerdoPagoUsuario'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['conceptoPago'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['conceptoPago'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['formaPago'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['formaPago'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['soportePago'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['soportePago'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                        $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }

                            if (!empty($datosAdministracion['gestionInventario'])) {
                                $arrayNombresAdmiDatosClinicos=array($datosAdministracion['categoriaInventario'],$datosAdministracion['insumo'],$datosAdministracion['marca'],$datosAdministracion['nombreInventario'],$datosAdministracion['presentacionInventario'],$datosAdministracion['proveedor'],$datosAdministracion['pedido'],$datosAdministracion['salida'],$datosAdministracion['ordenes'],$datosAdministracion['stock']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiDatosClinicos as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }else{
                                if (!empty($datosAdministracion['configuracionInventario'])) {
                                    $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['categoriaInventario'],$datosAdministracion['insumo'],$datosAdministracion['marca'],$datosAdministracion['nombreInventario'],$datosAdministracion['presentacionInventario'],$datosAdministracion['proveedor']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                    
                                }else{
                                    if (!empty($datosAdministracion['categoriaInventario'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['categoriaInventario'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['insumo'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['insumo'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['marca'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['marca'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['nombreInventario'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['nombreInventario'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['presentacionInventario'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['presentacionInventario'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['proveedor'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['proveedor'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['ordenes'])) {
                                   $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['ordenes'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }else{
                                    if (!empty($datosAdministracion['requerimientos'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['requerimientos'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['compras'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['compras'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                }

                                if (!empty($datosAdministracion['pedido'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['pedido'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['salida'])) {
                                   $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['salida'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }else{
                                    if (!empty($datosAdministracion['salidaClinica'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['salidaClinica'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['salidaConsultorio'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['salidaConsultorio'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                }

                                if (!empty($datosAdministracion['stock'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['stock'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                               
                            }

                            if (!empty($datosAdministracion['gestionMantenimiento'])) {
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['gestionMantenimiento'].'%');
                                $administracionDatos=$queryAdministracion->getResult();
                                foreach ($administracionDatos as $administracionSeleccionada ) {             
                                   $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoAdministracion);
                                }
                            }

                            if (!empty($datosAdministracion['gestionSGSS'])) {
                                $arrayNombresAdmiDatosClinicos=array($datosAdministracion['CIE11'],$datosAdministracion['ambitoRealizacion'],$datosAdministracion['causaExterna'],$datosAdministracion['cups'],$datosAdministracion['finalidadProcedimiento'],$datosAdministracion['formaRealizacion'],$datosAdministracion['personAtiende'],$datosAdministracion['cupsDiagnostico']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiDatosClinicos as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }else{
                                if (!empty($datosAdministracion['CIE11'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['CIE11'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                } 

                                if (!empty($datosAdministracion['configuracionCups'])) {
                                    $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['ambitoRealizacion'],$datosAdministracion['causaExterna'],$datosAdministracion['cups'],$datosAdministracion['finalidadProcedimiento'],$datosAdministracion['formaRealizacion'],$datosAdministracion['personAtiende']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                    
                                }else{
                                    if (!empty($datosAdministracion['ambitoRealizacion'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['ambitoRealizacion'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['causaExterna'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['causaExterna'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['cups'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['cups'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['finalidadProcedimiento'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['finalidadProcedimiento'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['formaRealizacion'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['formaRealizacion'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['personAtiende'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['personAtiende'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['cupsDiagnostico'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['cupsDiagnostico'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }                       
                            }

                            if (!empty($datosAdministracion['gestionTratamiento'])) {
                                $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['categoriaTratamiento'],$datosAdministracion['tratamiento']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                                
                            }else{
                                if (!empty($datosAdministracion['categoriaTratamiento'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['categoriaTratamiento'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['tratamiento'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tratamiento'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }

                             if (!empty($datosAdministracion['gestionUsuario'])) {
                                $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['agendaUsuario'],$datosAdministracion['tipoDocumento'],$datosAdministracion['tipoUsuario'],$datosAdministracion['usuario']);
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                                
                            }else{
                                if (!empty($datosAdministracion['agendaUsuario'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['agendaUsuario'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['tipoDatos'])) {
                                    $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['tipoDocumento'],$datosAdministracion['tipoUsuario']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                    
                                }else{
                                    if (!empty($datosAdministracion['tipoDocumento'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoDocumento'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['tipoUsuario'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoUsuario'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['usuario'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['usuario'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }
                            }
                        }
                    }

                    if (!empty($request->request->get('reporte'))) {
                        $datosReporte= $request->request->get('reporte');
                        if (!empty($datosReporte['principal'])) {
                            $dqlReporte="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo ORDER BY permiso.idPermiso ASC";
                            $queryReporte=$entityManager->createQuery($dqlReporte)->setParameter('modulo','reportes');
                            $reportes=$queryReporte->getResult();
                            foreach ($reportes as $reporteSeleccionada ) {             
                               $permisoReporte=$reporteSeleccionada->getIdPermiso();
                               array_push($arrayPermisos,$permisoReporte);
                            }
                        }else{
                            if (!empty($datosReporte['reporteConsulta'])) {
                                $dqlReporte="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                $queryReporte=$entityManager->createQuery($dqlReporte)->setParameter('modulo','reportes')->setParameter('nombre','%'.$datosReporte['reporteConsulta'].'%');
                                $reportes=$queryReporte->getResult();
                                foreach ($reportes as $reporteSeleccionada ) {             
                                   $permisoReporte=$reporteSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoReporte);
                                }
                            }
                        }
                    }
                    if (!empty($request->request->get('configuracion'))) {
                       $datosConfiguracion= $request->request->get('configuracion');
                        if (!empty($datosConfiguracion['principal'])) {
                            $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo ORDER BY permiso.idPermiso ASC";
                            $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion');
                            $configuraciones=$queryConfiguraciones->getResult();
                            foreach ($configuraciones as $configuracionSeleccionada ) {             
                               $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                               array_push($arrayPermisos,$permisoConfiguracion);
                            }
                        }else{
                            if (!empty($datosConfiguracion['clinica'])) {
                                $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion')->setParameter('nombre','%'.$datosConfiguracion['clinica'].'%');
                                $configuraciones=$queryConfiguraciones->getResult();
                                foreach ($configuraciones as $configuracionSeleccionada ) {             
                                   $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoConfiguracion);
                                }
                            }

                            if (!empty($datosConfiguracion['sede'])) {
                                $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion')->setParameter('nombre','%'.$datosConfiguracion['sede'].'%');
                                $configuraciones=$queryConfiguraciones->getResult();
                                foreach ($configuraciones as $configuracionSeleccionada ) {             
                                   $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoConfiguracion);
                                }
                            }

                            if (!empty($datosConfiguracion['consultorio'])) {
                                $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion')->setParameter('nombre','%'.$datosConfiguracion['consultorio'].'%');
                                $configuraciones=$queryConfiguraciones->getResult();
                                foreach ($configuraciones as $configuracionSeleccionada ) {             
                                   $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoConfiguracion);
                                }
                            }

                            if (!empty($datosConfiguracion['perfiles'])) {
                                $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion')->setParameter('nombre','%'.$datosConfiguracion['perfiles'].'%');
                                $configuraciones=$queryConfiguraciones->getResult();
                                foreach ($configuraciones as $configuracionSeleccionada ) {             
                                   $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoConfiguracion);
                                }
                            }
                        }
                    }
                    if (sizeof($arrayPermisos) > 0) {
                        
                        foreach ($arrayPermisos as $id) {
                            $dqlPermisos="SELECT permiso FROM App:Permiso permiso where permiso.idPermiso=:idPermiso ORDER BY permiso.idPermiso ASC";                            
                            $queryPermisos=$entityManager->createQuery($dqlPermisos)->setParameter('idPermiso',$id);
                            $permisos=$queryPermisos->getResult();
                            $arrayPermisosFinales=array();
                            foreach ($permisos as $permisoSelecionado ) {
                               $permisoPerfil=new PerfilPermiso();
                               $permisoPerfil->setIdPerfil($perfil); 
                               $permisoPerfil->setIdPermiso($permisoSelecionado);
                               $entityManager->persist($permisoPerfil);
                               $entityManager->flush();
                            }
                        }
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('perfil_index');
                    }
                    
                    return $this->render('perfil/perfilPermiso.html.twig', [
                        'perfil' => $perfil,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }
            }
       }     
    }



    /**
     * Metodo utilizado para la edición de un perfil diferente de super administrador.
     *
     * @Route("/{idPerfil}/edit", name="perfil_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Perfil $perfil,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfiles=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfiles,'perfil');
                if (sizeof($permisos) > 0) {

                    if ($perfil->getNombre() != 'ROLE_SUPERADMINISTRADOR' ) {                    
                        $entityManager=$this->getDoctrine()->getManager();
                        $form = $this->createForm(PerfilType::class, $perfil);
                        $form->handleRequest($request);

                        if ($form->isSubmitted() && $form->isValid()) {
                            $entityManager->flush();
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                            return $this->redirectToRoute('perfil_edit', [
                                'idPerfil' => $perfil->getIdPerfil(),
                            ]);
                        }
                        $dqlConfiguracion="SELECT permisoPerfil FROM App:PerfilPermiso permisoPerfil where permisoPerfil.idPerfil=:idPerfil ORDER BY permisoPerfil.idPerfilPermiso ASC";
                        $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('idPerfil',$perfil->getIdPerfil());
                        $configuraciones=$queryConfiguraciones->getResult();
                        $arrayPermisoSeleccionado=array();
                        foreach ($configuraciones as $permiso) {
                            $idPermiso=$permiso->getIdPermiso()->getIdPermiso();
                            array_push($arrayPermisoSeleccionado,$idPermiso);
                        }
                        return $this->render('perfil/edit.html.twig', [
                            'perfil' => $perfil,
                            'form' => $form->createView(),
                            'configuraciones'=> $configuraciones,
                            'arrs'=>$arrayPermisoSeleccionado
                        ]);
                    }else{
                        $this->addFlash('error', 'No se permiten modificaciones al perfil, predeterminado por el sistema!'.$perfil->getNombre());
                        return $this->redirectToRoute('perfil_index');
                    }
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }
            }
        }        
    }


    /**
     * Metodo utilizado para la edición de los permisos de un perfil, donde se llevan a cabo la eliminavión de todos los permisos existentes y se 
     * ingresan los nuevos permisos que se considera que el perfil debe asociar.
     *
     * @Route("/{idPerfil}/permisoedit", name="perfil_permisos_edit", methods={"GET","POST","DELETE"})
     */
    public function perfilPermisoEdit(Perfil $perfil,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfiles=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfiles,'perfil');
                if (sizeof($permisos) > 0) {
                    if ($perfil->getNombre() != 'ROLE_SUPERADMINISTRADOR' ) {      
                        $entityManager = $this->getDoctrine()->getManager();
                        $arrayPermisos=array();
                        if (!empty($request->request->get('agenda'))) {
                            $datosAgenda= $request->request->get('agenda');
                            if (!empty($datosAgenda['principal'])) {
                                $dqlAgenda="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo ORDER BY permiso.idPermiso ASC";
                                $queryAgenda=$entityManager->createQuery($dqlAgenda)->setParameter('modulo','agenda');
                                $agendas=$queryAgenda->getResult();
                                foreach ($agendas as $agendaSeleccionada ) {             
                                   $permisoAgenda=$agendaSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoAgenda);
                                }
                            }else{
                                if (!empty($datosAgenda['secundaria'])) {
                                    $dqlAgenda="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAgenda=$entityManager->createQuery($dqlAgenda)->setParameter('modulo','agenda')->setParameter('nombre','%'.$datosAgenda['secundaria'].'%');
                                    $agendas=$queryAgenda->getResult();
                                    foreach ($agendas as $agendaSeleccionada ) {             
                                       $permisoAgenda=$agendaSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAgenda);
                                    }
                                }
                            }
                        }

                        if (!empty($request->request->get('paciente'))) {
                            $datosPaciente= $request->request->get('paciente');
                            if (!empty($datosPaciente['principal'])) {
                                $dqlPaciente="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo ORDER BY permiso.idPermiso ASC";
                                $queryPaciente=$entityManager->createQuery($dqlPaciente)->setParameter('modulo','paciente');
                                $pacientes=$queryPaciente->getResult();
                                foreach ($pacientes as $pacienteSeleccionado ) {             
                                   $permisoAgenda=$pacienteSeleccionado->getIdPermiso();
                                   array_push($arrayPermisos,$permisoAgenda);
                                }
                            }else{
                                if (!empty($datosPaciente['fichaPaciente'])) {
                                    $arrayPosiblesNombre=array($datosPaciente['datosPersonal'],$datosPaciente['examenGeneral'],$datosPaciente['odontograma'],$datosPaciente['periodontograma'],$datosPaciente['endodoncia'],$datosPaciente['cirugiaOral'],$datosPaciente['operatoriaEstetica'],$datosPaciente['odontoPediatria'],$datosPaciente['pacientePlanTratamiento'],$datosPaciente['documentoConsentimiento'],$datosPaciente['documentoFormulaMedica'],$datosPaciente['documentoRemision'],$datosPaciente['documentoLaboratorio'],$datosPaciente['documentoRadiografia'],$datosPaciente['documentoArchivo'],$datosPaciente['documentoAnexo'],$datosPaciente['imagenes']);
                                     /*$dqlPacienteFicha=$entityManager->createQueryBuilder();
                                      $dqlPacienteFicha->select('permiso')
                                       ->from('App:Permiso', 'permiso')->Where("permiso.nombre like :nombre");*/
                                    $dqlPacienteFicha="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayPosiblesNombre as $i => $nombre) {
                                        $queryPacienteFicha=$entityManager->createQuery($dqlPacienteFicha)->setParameter("nombre",'%'.$nombre.'%')->setParameter('modulo','paciente');
                                        $pacientesFichas=$queryPacienteFicha->getResult();
                                        foreach ($pacientesFichas as $pacienteFichaSeleccionada ) {             
                                        $permisoPacienteFicha=$pacienteFichaSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoPacienteFicha);
                                        }
                                    }
                                }else{
                                    if (!empty($datosPaciente['datosPersonal'])) {
                                        $dqlPacienteDatosPersonales="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryPacienteDatosPersonal=$entityManager->createQuery($dqlPacienteDatosPersonales)->setParameter("nombre",'%'.$datosPaciente['datosPersonal'].'%')->setParameter('modulo','paciente');
                                        $pacientesPersonalesDatos=$queryPacienteDatosPersonal->getResult();
                                        foreach ($pacientesPersonalesDatos as $pacienteDatoPersonalSeleccionada ) {             
                                        $permisoPacienteDP=$pacienteDatoPersonalSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoPacienteDP);
                                        }
                                    }
                                    if (!empty($datosPaciente['examenGeneral'])) {
                                        $dqlPacienteExamenGeneral="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                        $queryPacienteExamenGeneral=$entityManager->createQuery($dqlPacienteExamenGeneral)->setParameter("nombre",'%'.$datosPaciente['examenGeneral'].'%')->setParameter('modulo','paciente');
                                        $pacientesExamenGeneral=$queryPacienteExamenGeneral->getResult();
                                        foreach ($pacientesExamenGeneral as $pacienteExamenGeneralSeleccionada ) {             
                                        $permisoPacienteExamenGeneral=$pacienteExamenGeneralSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoPacienteExamenGeneral);
                                        }
                                    }
                                    if (!empty($datosPaciente['examenes'])) {
                                        $arrayPosiblesNombreExamen=array($datosPaciente['odontograma'],$datosPaciente['ortodoncia'],$datosPaciente['periodontograma'],$datosPaciente['endodoncia'],$datosPaciente['cirugiaOral'],$datosPaciente['operatoriaEstetica'],$datosPaciente['odontoPediatria']);
                                        $dqlPacienteExamenes="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        foreach ($arrayPosiblesNombreExamen as $i => $nombre) {
                                            $queryPacienteExamenes=$entityManager->createQuery($dqlPacienteExamenes)->setParameter("nombre",'%'.$nombre.'%')->setParameter('modulo','paciente');
                                            $pacientesExamenes=$queryPacienteExamenes->getResult();
                                            foreach ($pacientesExamenes as $pacienteExamenSeleccionado ) {             
                                            $permisoPacienteExamen=$pacienteExamenSeleccionado->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteExamen);
                                            }
                                        }
                                    }else{
                                        if (!empty($datosPaciente['odontograma'])) {
                                            $dqlPacienteOdontograma="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                            $queryPacienteOdontograma=$entityManager->createQuery($dqlPacienteOdontograma)->setParameter("nombre",'%'.$datosPaciente['odontograma'].'%')->setParameter('modulo','paciente');
                                            $pacientesOdontograma=$queryPacienteOdontograma->getResult();
                                            foreach ($pacientesOdontograma as $pacienteOdontogramaSeleccionada ) {             
                                                $permisoPacienteOdontograma=$pacienteOdontogramaSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteOdontograma);
                                            }
                                        }
                                        if (!empty($datosPaciente['ortodoncia'])) {
                                            $dqlPacienteOdontograma="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                            $queryPacienteOdontograma=$entityManager->createQuery($dqlPacienteOdontograma)->setParameter("nombre",'%'.$datosPaciente['ortodoncia'].'%')->setParameter('modulo','paciente');
                                            $pacientesOdontograma=$queryPacienteOdontograma->getResult();
                                            foreach ($pacientesOdontograma as $pacienteOdontogramaSeleccionada ) {             
                                                $permisoPacienteOdontograma=$pacienteOdontogramaSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteOdontograma);
                                            }
                                        }
                                        if (!empty($datosPaciente['periodontograma'])) {
                                            $dqlPacientePeriodontograma="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                            $queryPacientePeriodontograma=$entityManager->createQuery($dqlPacientePeriodontograma)->setParameter("nombre",'%'.$datosPaciente['periodontograma'].'%')->setParameter('modulo','paciente');
                                            $pacientesPeriodontograma=$queryPacientePeriodontograma->getResult();
                                            foreach ($pacientesPeriodontograma as $pacientePeriodontogramaSeleccionada ) {             
                                                $permisoPacientePeriodontograma=$pacientePeriodontogramaSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacientePeriodontograma);
                                            }
                                        }
                                        if (!empty($datosPaciente['endodoncia'])) {
                                            $dqlPacienteEndodoncia="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                            $queryPacienteEndodoncia=$entityManager->createQuery($dqlPacienteEndodoncia)->setParameter("nombre",'%'.$datosPaciente['endodoncia'].'%')->setParameter('modulo','paciente');
                                            $pacientesEndodoncia=$queryPacienteEndodoncia->getResult();
                                            foreach ($pacientesEndodoncia as $pacienteEndodonciaSeleccionada ) {             
                                                $permisoPacienteEndodoncia=$pacienteEndodonciaSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteEndodoncia);
                                            }
                                        }
                                        if (!empty($datosPaciente['cirugiaOral'])) {
                                            $dqlPacienteCirugia="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                            $queryPacienteCirugia=$entityManager->createQuery($dqlPacienteCirugia)->setParameter("nombre",'%'.$datosPaciente['cirugiaOral'].'%')->setParameter('modulo','paciente');
                                            $pacientesCirugia=$queryPacienteCirugia->getResult();
                                            foreach ($pacientesCirugia as $pacienteCirugiaSeleccionada ) {             
                                                $permisoPacienteCirugia=$pacienteCirugiaSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteCirugia);
                                            }
                                        }
                                        if (!empty($datosPaciente['operatoriaEstetica'])) {
                                            $dqlPacienteOperatoria="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                            $queryPacienteOperatoria=$entityManager->createQuery($dqlPacienteOperatoria)->setParameter("nombre",'%'.$datosPaciente['operatoriaEstetica'].'%')->setParameter('modulo','paciente');
                                            $pacientesOperatoria=$queryPacienteOperatoria->getResult();
                                            foreach ($pacientesOperatoria as $pacienteOperatoriaSeleccionada ) {             
                                                $permisoPacienteOperatoria=$pacienteOperatoriaSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteOperatoria);
                                            }
                                        }
                                        if (!empty($datosPaciente['odontoPediatria'])) {
                                            $dqlPacienteOdontoPediatria="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                            $queryPacienteOdontoPediatria=$entityManager->createQuery($dqlPacienteOdontoPediatria)->setParameter("nombre",'%'.$datosPaciente['odontoPediatria'].'%')->setParameter('modulo','paciente');
                                            $pacientesOdontoPediatria=$queryPacienteOdontoPediatria->getResult();
                                            foreach ($pacientesOdontoPediatria as $pacienteOdontoPediatriaSeleccionada ) {
                                                $permisoPacienteOdontoPediatria=$pacienteOdontoPediatriaSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteOdontoPediatria);
                                            }
                                        }
                                    }

                                    if (!empty($datosPaciente['pacientePlanTratamiento'])) {
                                        $dqlPacientePlan="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                         
                                        $queryPacientePlan=$entityManager->createQuery($dqlPacientePlan)->setParameter("nombre",'%'.$datosPaciente['pacientePlanTratamiento'].'%')->setParameter('modulo','paciente');
                                        $pacientesPlan=$queryPacientePlan->getResult();
                                        foreach ($pacientesPlan as $pacientePlanSeleccionada ) {
                                            $permisoPacientePlan=$pacientePlanSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacientePlan);
                                        }
                                    }

                                    if (!empty($datosPaciente['documento'])) {
                                        $arrayPosiblesNombreDoc=array($datosPaciente['documentoConsentimiento'],$datosPaciente['documentoFormulaMedica'],$datosPaciente['documentoRemision'],$datosPaciente['documentoLaboratorio'],$datosPaciente['documentoRadiografia'],$datosPaciente['documentoArchivo'],$datosPaciente['documentoAnexo']);

                                        $dqlPacienteDoc="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        foreach ($arrayPosiblesNombreDoc as $i => $nombre) {
                                            $queryPacienteDoc=$entityManager->createQuery($dqlPacienteDoc)->setParameter("nombre",'%'.$nombre.'%')->setParameter('modulo','paciente');
                                            $pacientesDoc=$queryPacienteDoc->getResult();
                                            foreach ($pacientesDoc as $pacienteDocSeleccionada ) {             
                                                $permisoPacienteDoc=$pacienteDocSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteDoc);
                                            }
                                        }
                                    }else{
                                        if (!empty($datosPaciente['documentoConsentimiento'])) {
                                            $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                            $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoConsentimiento'].'%')->setParameter('modulo','paciente');
                                            $pacientesConsens=$queryPacienteDocConse->getResult();
                                            foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                                $permisoPacienteConsentimiento=$pacienteConseSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteConsentimiento);
                                            }
                                        }
                                        if (!empty($datosPaciente['documentoFormulaMedica'])) {
                                            $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                            $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoFormulaMedica'].'%')->setParameter('modulo','paciente');
                                            $pacientesConsens=$queryPacienteDocConse->getResult();
                                            foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                                $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteDoc);
                                            }
                                        }
                                        if (!empty($datosPaciente['documentoRemision'])) {
                                            $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                            $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoRemision'].'%')->setParameter('modulo','paciente');
                                            $pacientesConsens=$queryPacienteDocConse->getResult();
                                            foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                                $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteDoc);
                                            }
                                        }
                                        if (!empty($datosPaciente['documentoLaboratorio'])) {
                                            $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                            $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoLaboratorio'].'%')->setParameter('modulo','paciente');
                                            $pacientesConsens=$queryPacienteDocConse->getResult();
                                            foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                                $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteDoc);
                                            }
                                        }
                                        if (!empty($datosPaciente['documentoRadiografia'])) {
                                            $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                            $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoRadiografia'].'%')->setParameter('modulo','paciente');
                                            $pacientesConsens=$queryPacienteDocConse->getResult();
                                            foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                                $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteDoc);
                                            }
                                        }
                                        if (!empty($datosPaciente['documentoArchivo'])) {
                                            $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                            $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoArchivo'].'%')->setParameter('modulo','paciente');
                                            $pacientesConsens=$queryPacienteDocConse->getResult();
                                            foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                                $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteDoc);
                                            }
                                        }
                                        if (!empty($datosPaciente['documentoAnexo'])) {
                                            $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                          
                                            $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['documentoAnexo'].'%')->setParameter('modulo','paciente');
                                            $pacientesConsens=$queryPacienteDocConse->getResult();
                                            foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                                $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                                array_push($arrayPermisos,$permisoPacienteDoc);
                                            }
                                        }
                                    }

                                    if (!empty($datosPaciente['imagenes'])) {
                                        $dqlPacienteDocCons="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                           
                                        $queryPacienteDocConse=$entityManager->createQuery($dqlPacienteDocCons)->setParameter("nombre",'%'.$datosPaciente['imagenes'].'%')->setParameter('modulo','paciente');
                                        $pacientesConsens=$queryPacienteDocConse->getResult();
                                        foreach ($pacientesConsens as $pacienteConseSeleccionada ) {
                                            $permisoPacienteDoc=$pacienteConseSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoPacienteDoc);
                                        }
                                    }
                                }

                                if (!empty($datosPaciente['historialPacienteEvolucion'])) {
                                    $dqlPacienteEvolucion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                          
                                    $queryPacienteEvolucion=$entityManager->createQuery($dqlPacienteEvolucion)->setParameter("nombre",'%'.$datosPaciente['historialPacienteEvolucion'].'%')->setParameter('modulo','paciente');
                                    $pacientesEvolucion=$queryPacienteEvolucion->getResult();
                                    foreach ($pacientesEvolucion as $pacienteEvoluSeleccionada ) {
                                        $permisoPacienteEvo=$pacienteEvoluSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoPacienteEvo);
                                    }
                                }

                                if (!empty($datosPaciente['pagos'])) {
                                    $dqlPacientePago="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryPacientePago=$entityManager->createQuery($dqlPacientePago)->setParameter("nombre",'%'.$datosPaciente['pagos'].'%')->setParameter('modulo','paciente');
                                    $pacientesPago=$queryPacientePago->getResult();
                                    foreach ($pacientesPago as $pacientePagoSeleccionada ) {
                                        $permisoPacientePago=$pacientePagoSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoPacientePago);
                                    }
                                }
                                if (!empty($datosPaciente['historialCitas']) ){
                                    $dqlPacienteHistoCita="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";                            
                                    $queryPacienteHistoCita=$entityManager->createQuery($dqlPacienteHistoCita)->setParameter("nombre",'%'.$datosPaciente['historialCitas'].'%')->setParameter('modulo','paciente');
                                    $pacientesHistoCita=$queryPacienteHistoCita->getResult();
                                    foreach ($pacientesHistoCita as $pacienteHCSeleccionada ) {
                                        $permisoPacienteHC=$pacienteHCSeleccionada->getIdPermiso();
                                        array_push($arrayPermisos,$permisoPacienteHC);
                                    }
                                }
                            }

                        }

                        if (!empty($request->request->get('administracion'))) {
                            $datosAdministracion= $request->request->get('administracion');
                            if (!empty($datosAdministracion['principal'])) {
                                $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo ORDER BY permiso.idPermiso ASC";
                                $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion');
                                $administracionDatos=$queryAdministracion->getResult();
                                foreach ($administracionDatos as $administracionSeleccionada ) {             
                                   $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoAdministracion);
                                }
                            }else{
                                if (!empty($datosAdministracion['configuracionGeneral'])) {
                                    $arrayNombresAdmiConfiguracionGeneral=array($datosAdministracion['confiMediAreaMedica'],$datosAdministracion['confiMediEspecialida'],$datosAdministracion['departamento'],$datosAdministracion['eps']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConfiguracionGeneral as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }else{
                                    if (!empty($datosAdministracion['configuracionMedica'])) {
                                        $arrayNombresAdmiConfiguracionMedica=array($datosAdministracion['confiMediAreaMedica'],$datosAdministracion['confiMediEspecialida']);
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        foreach ($arrayNombresAdmiConfiguracionMedica as $id => $nombre) {
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                        
                                    }else{
                                        if (!empty($datosAdministracion['confiMediAreaMedica'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiMediAreaMedica'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['confiMediEspecialida'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiMediEspecialida'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                    }

                                    if (!empty($datosAdministracion['departamento'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['departamento'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['eps'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['eps'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['gestionConsulta'])) {
                                    $arrayNombresAdmiConsulta=array($datosAdministracion['finalidadConsulta'],$datosAdministracion['motivoConsulta']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConsulta as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }else{
                                    
                                    if (!empty($datosAdministracion['finalidadConsulta'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['finalidadConsulta'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['motivoConsulta'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['motivoConsulta'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }
                                
                                if (!empty($datosAdministracion['gestionConvenio'])) {
                                    $arrayNombresAdmiConvenio=array($datosAdministracion['convenio'],$datosAdministracion['tratamientoEps']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConvenio as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                }else{

                                    if (!empty($datosAdministracion['convenio'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['convenio'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['tratamientoEps'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tratamientoEps'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['gestionDatosClinicos'])) {
                                    $arrayNombresAdmiDatosClinicos=array($datosAdministracion['confiExamGeAlergia'],$datosAdministracion['confiExamGeAntecedente'],$datosAdministracion['confiExamGeHabito'],$datosAdministracion['confiExamGeMedicamento'],$datosAdministracion['confiExamGeRiesgo'],$datosAdministracion['evolucionEndodoncia'],$datosAdministracion['propiedades'],$datosAdministracion['provocado'],$datosAdministracion['superficie']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiDatosClinicos as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }else{
                                    if (!empty($datosAdministracion['configuracionExamenGeneral'])) {
                                        $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['confiExamGeAlergia'],$datosAdministracion['confiExamGeAntecedente'],$datosAdministracion['confiExamGeHabito'],$datosAdministracion['confiExamGeMedicamento'],$datosAdministracion['confiExamGeRiesgo']);
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                        
                                    }else{
                                        if (!empty($datosAdministracion['confiExamGeAlergia'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeAlergia'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['confiExamGeAntecedente'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeAntecedente'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['confiExamGeHabito'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeHabito'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['confiExamGeMedicamento'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeMedicamento'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['confiExamGeRiesgo'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['confiExamGeRiesgo'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                    }

                                    if (!empty($datosAdministracion['confiDatosExamenEndododoncia'])) {
                                        $arrayNombresAdmiConfiExamenEndodoncia=array($datosAdministracion['evolucionEndodoncia'],$datosAdministracion['propiedades'],$datosAdministracion['provocado']);
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        foreach ($arrayNombresAdmiConfiExamenEndodoncia as $id => $nombre) {
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                        
                                    }else{
                                        if (!empty($datosAdministracion['evolucionEndodoncia'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['evolucionEndodoncia'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['propiedades'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['propiedades'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['provocado'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['provocado'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                    }

                                    if (!empty($datosAdministracion['superficie'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['superficie'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['gestionDiagnostico'])) {
                                    $arrayNombresAdmiDiagnostico=array($datosAdministracion['categoriaDiagnostico'],$datosAdministracion['diagnosticoOdontologico'],$datosAdministracion['diagnosticoRipOdontologico'],$datosAdministracion['tipoDiagnosticoPrincipal']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiDiagnostico as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                }else{

                                    if (!empty($datosAdministracion['categoriaDiagnostico'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['categoriaDiagnostico'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['diagnosticoOdontologico'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['diagnosticoOdontologico'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['diagnosticoRipOdontologico'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['diagnosticoRipOdontologico'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['tipoDiagnosticoPrincipal'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoDiagnosticoPrincipal'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['gestionDocumentacion'])) {
                                    $arrayNombresAdmiDocumentacion=array($datosAdministracion['tipoArchivo'],$datosAdministracion['tipoConsentimiento'],$datosAdministracion['tipoContrato'],$datosAdministracion['tipoRadiografia']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiDocumentacion as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                }else{

                                    if (!empty($datosAdministracion['tipoArchivo'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoArchivo'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['tipoConsentimiento'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoConsentimiento'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['tipoContrato'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoContrato'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['tipoRadiografia'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoRadiografia'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['gestionEstados'])) {
                                    $arrayNombresAdmiEstados=array($datosAdministracion['estadoCita'],$datosAdministracion['estadoGeneral'],$datosAdministracion['estadoPago'],$datosAdministracion['estadoMantenimiento']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiEstados as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                }else{

                                    if (!empty($datosAdministracion['estadoCita'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['estadoCita'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['estadoGeneral'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['estadoGeneral'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['estadoPago'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['estadoPago'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['estadoMantenimiento'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['estadoMantenimiento'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['gestionEsterilizacion'])) {
                                    $arrayNombresAdmiEsterilizacion=array($datosAdministracion['esterilizacion'],$datosAdministracion['tipoEsterilizacion']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiEsterilizacion as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                }else{

                                    if (!empty($datosAdministracion['esterilizacion'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['esterilizacion'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['tipoEsterilizacion'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoEsterilizacion'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }


                                if (!empty($datosAdministracion['gestionFinanciera'])) {
                                    $arrayNombresAdmiFinanzas=array($datosAdministracion['acuerdoPagoUsuario'],$datosAdministracion['conceptoPago'],$datosAdministracion['formaPago'],$datosAdministracion['soportePago']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiFinanzas as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                }else{

                                    if (!empty($datosAdministracion['acuerdoPagoUsuario'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['acuerdoPagoUsuario'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['conceptoPago'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['conceptoPago'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['formaPago'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['formaPago'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['soportePago'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['soportePago'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                            $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                            array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                if (!empty($datosAdministracion['gestionInventario'])) {
                                    $arrayNombresAdmiDatosClinicos=array($datosAdministracion['categoriaInventario'],$datosAdministracion['insumo'],$datosAdministracion['marca'],$datosAdministracion['nombreInventario'],$datosAdministracion['presentacionInventario'],$datosAdministracion['proveedor'],$datosAdministracion['pedido'],$datosAdministracion['salida'],$datosAdministracion['ordenes'],$datosAdministracion['stock']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiDatosClinicos as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }else{
                                    if (!empty($datosAdministracion['configuracionInventario'])) {
                                        $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['categoriaInventario'],$datosAdministracion['insumo'],$datosAdministracion['marca'],$datosAdministracion['nombreInventario'],$datosAdministracion['presentacionInventario'],$datosAdministracion['proveedor']);
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                        
                                    }else{
                                        if (!empty($datosAdministracion['categoriaInventario'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['categoriaInventario'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['insumo'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['insumo'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['marca'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['marca'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['nombreInventario'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['nombreInventario'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['presentacionInventario'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['presentacionInventario'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['proveedor'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['proveedor'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                    }


                                    if (!empty($datosAdministracion['ordenes'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['ordenes'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }else{
                                        if (!empty($datosAdministracion['requerimientos'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['requerimientos'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['compras'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['compras'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                    }

                                    if (!empty($datosAdministracion['pedido'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['pedido'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['salida'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['salida'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }else{
                                        if (!empty($datosAdministracion['salidaClinica'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['salidaClinica'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['salidaConsultorio'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['salidaConsultorio'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                    }


                                   /* if (!empty($datosAdministracion['pedido'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['pedido'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['salida'])) {
                                       $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['propiedades'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }*/

                                    if (!empty($datosAdministracion['stock'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['stock'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                   
                                }

                                if (!empty($datosAdministracion['gestionMantenimiento'])) {
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['gestionMantenimiento'].'%');
                                    $administracionDatos=$queryAdministracion->getResult();
                                    foreach ($administracionDatos as $administracionSeleccionada ) {             
                                       $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoAdministracion);
                                    }
                                }

                                if (!empty($datosAdministracion['gestionSGSS'])) {
                                    $arrayNombresAdmiDatosClinicos=array($datosAdministracion['CIE11'],$datosAdministracion['ambitoRealizacion'],$datosAdministracion['causaExterna'],$datosAdministracion['cups'],$datosAdministracion['finalidadProcedimiento'],$datosAdministracion['formaRealizacion'],$datosAdministracion['personAtiende'],$datosAdministracion['cupsDiagnostico']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiDatosClinicos as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }else{
                                    if (!empty($datosAdministracion['CIE11'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['CIE11'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    } 

                                    if (!empty($datosAdministracion['configuracionCups'])) {
                                        $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['ambitoRealizacion'],$datosAdministracion['causaExterna'],$datosAdministracion['cups'],$datosAdministracion['finalidadProcedimiento'],$datosAdministracion['formaRealizacion'],$datosAdministracion['personAtiende']);
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                        
                                    }else{
                                        if (!empty($datosAdministracion['ambitoRealizacion'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['ambitoRealizacion'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['causaExterna'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['causaExterna'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['cups'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['cups'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['finalidadProcedimiento'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['finalidadProcedimiento'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['formaRealizacion'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['formaRealizacion'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['personAtiende'])) {
                                           $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['personAtiende'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                    }

                                    if (!empty($datosAdministracion['cupsDiagnostico'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['cupsDiagnostico'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }                       
                                }

                                if (!empty($datosAdministracion['gestionTratamiento'])) {
                                    $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['categoriaTratamiento'],$datosAdministracion['tratamiento']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                    
                                }else{
                                    if (!empty($datosAdministracion['categoriaTratamiento'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['categoriaTratamiento'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['tratamiento'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tratamiento'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }

                                 if (!empty($datosAdministracion['gestionUsuario'])) {
                                    $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['agendaUsuario'],$datosAdministracion['tipoDocumento'],$datosAdministracion['tipoUsuario'],$datosAdministracion['usuario']);
                                    $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                    
                                }else{
                                    if (!empty($datosAdministracion['agendaUsuario'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['agendaUsuario'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }

                                    if (!empty($datosAdministracion['tipoDatos'])) {
                                        $arrayNombresAdmiConfiExamenGeneral=array($datosAdministracion['tipoDocumento'],$datosAdministracion['tipoUsuario']);
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        foreach ($arrayNombresAdmiConfiExamenGeneral as $id => $nombre) {
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$nombre.'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                        
                                    }else{
                                        if (!empty($datosAdministracion['tipoDocumento'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoDocumento'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }

                                        if (!empty($datosAdministracion['tipoUsuario'])) {
                                            $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                            $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['tipoUsuario'].'%');
                                            $administracionDatos=$queryAdministracion->getResult();
                                            foreach ($administracionDatos as $administracionSeleccionada ) {             
                                               $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                               array_push($arrayPermisos,$permisoAdministracion);
                                            }
                                        }
                                    }

                                    if (!empty($datosAdministracion['usuario'])) {
                                        $dqlAdministracion="SELECT permiso FROM App:Permiso permiso WHERE permiso.modulo =:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                        $queryAdministracion=$entityManager->createQuery($dqlAdministracion)->setParameter('modulo','administracion')->setParameter('nombre', '%'.$datosAdministracion['usuario'].'%');
                                        $administracionDatos=$queryAdministracion->getResult();
                                        foreach ($administracionDatos as $administracionSeleccionada ) {             
                                           $permisoAdministracion=$administracionSeleccionada->getIdPermiso();
                                           array_push($arrayPermisos,$permisoAdministracion);
                                        }
                                    }
                                }
                            }
                        }

                        if (!empty($request->request->get('reporte'))) {
                            $datosReporte= $request->request->get('reporte');
                            if (!empty($datosReporte['principal'])) {
                                $dqlReporte="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo ORDER BY permiso.idPermiso ASC";
                                $queryReporte=$entityManager->createQuery($dqlReporte)->setParameter('modulo','reportes');
                                $reportes=$queryReporte->getResult();
                                foreach ($reportes as $reporteSeleccionada ) {             
                                   $permisoReporte=$reporteSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoReporte);
                                }
                            }else{
                                if (!empty($datosReporte['reporteConsulta'])) {
                                    $dqlReporte="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryReporte=$entityManager->createQuery($dqlReporte)->setParameter('modulo','reportes')->setParameter('nombre','%'.$datosReporte['reporteConsulta'].'%');
                                    $reportes=$queryReporte->getResult();
                                    foreach ($reportes as $reporteSeleccionada ) {             
                                       $permisoReporte=$reporteSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoReporte);
                                    }
                                }
                            }
                        }
                        if (!empty($request->request->get('configuracion'))) {
                           $datosConfiguracion= $request->request->get('configuracion');
                            if (!empty($datosConfiguracion['principal'])) {
                                $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo ORDER BY permiso.idPermiso ASC";
                                $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion');
                                $configuraciones=$queryConfiguraciones->getResult();
                                foreach ($configuraciones as $configuracionSeleccionada ) {             
                                   $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                                   array_push($arrayPermisos,$permisoConfiguracion);
                                }
                            }else{
                                if (!empty($datosConfiguracion['clinica'])) {
                                    $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion')->setParameter('nombre','%'.$datosConfiguracion['clinica'].'%');
                                    $configuraciones=$queryConfiguraciones->getResult();
                                    foreach ($configuraciones as $configuracionSeleccionada ) {             
                                       $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoConfiguracion);
                                    }
                                }

                                if (!empty($datosConfiguracion['sede'])) {
                                    $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion')->setParameter('nombre','%'.$datosConfiguracion['sede'].'%');
                                    $configuraciones=$queryConfiguraciones->getResult();
                                    foreach ($configuraciones as $configuracionSeleccionada ) {             
                                       $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoConfiguracion);
                                    }
                                }

                                if (!empty($datosConfiguracion['consultorio'])) {
                                    $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion')->setParameter('nombre','%'.$datosConfiguracion['consultorio'].'%');
                                    $configuraciones=$queryConfiguraciones->getResult();
                                    foreach ($configuraciones as $configuracionSeleccionada ) {             
                                       $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoConfiguracion);
                                    }
                                }

                                if (!empty($datosConfiguracion['perfiles'])) {
                                    $dqlConfiguracion="SELECT permiso FROM App:Permiso permiso where permiso.modulo=:modulo and permiso.nombre like :nombre ORDER BY permiso.idPermiso ASC";
                                    $queryConfiguraciones=$entityManager->createQuery($dqlConfiguracion)->setParameter('modulo','configuracion')->setParameter('nombre','%'.$datosConfiguracion['perfiles'].'%');
                                    $configuraciones=$queryConfiguraciones->getResult();
                                    foreach ($configuraciones as $configuracionSeleccionada ) {             
                                       $permisoConfiguracion=$configuracionSeleccionada->getIdPermiso();
                                       array_push($arrayPermisos,$permisoConfiguracion);
                                    }
                                }
                            }
                        }
                        if (sizeof($arrayPermisos) > 0) {
                            $dqlEliminarPermisosPerfil="DELETE FROM App:PerfilPermiso perfilPermiso WHERE perfilPermiso.idPerfil = :idPerfil ";
                            $queryEliminarPerfilPermiso=$entityManager->createQuery($dqlEliminarPermisosPerfil)->setParameter('idPerfil',$perfil->getIdPerfil());
                            $eliminarPerfilPermiso=$queryEliminarPerfilPermiso->execute();
                            
                            foreach ($arrayPermisos as $id) {
                                $dqlPermisos="SELECT permiso FROM App:Permiso permiso where permiso.idPermiso=:idPermiso ORDER BY permiso.idPermiso ASC";                            
                                $queryPermisos=$entityManager->createQuery($dqlPermisos)->setParameter('idPermiso',$id);
                                $permisos=$queryPermisos->getResult();
                                $arrayPermisosFinales=array();
                                foreach ($permisos as $permisoSelecionado ) {
                                   $permisoPerfil=new PerfilPermiso();
                                   $permisoPerfil->setIdPerfil($perfil); 
                                   $permisoPerfil->setIdPermiso($permisoSelecionado);
                                   $entityManager->persist($permisoPerfil);
                                   $entityManager->flush();
                                }
                            }
                            
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                            return $this->redirectToRoute('perfil_index');
                        }
                    }else{
                        $this->addFlash('error', 'No se permiten modificaciones al perfil, predeterminado por el sistema!'.$perfil->getNombre());
                        return $this->redirectToRoute('perfil_index');
                    }
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }
                /*return $this->render('perfil/perfilPermiso.html.twig', [
                    'perfil' => $perfil,
                ]);*/
            }
       }     
    }

    /**
     * Metodo utilizado para la eliminación de un perfil.
     *
     * @Route("/{idPerfil}", name="perfil_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Perfil $perfil,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
        return $this->redirectToRoute('usuario_logout'); 
      } else{
        // La variable $_SESSION['usuario'] es un ejemplo. 
        //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
        if(isset($_SESSION['time'])){ 
          $tiempo = $_SESSION['time']; 
        }else{ 
          $tiempo = strtotime(date("Y-m-d H:i:s"));
        } 
        $inactividad =1800;   //Exprecion en segundos. 
        $actual =  strtotime(date("Y-m-d H:i:s")); 
        $tiempoTranscurrido=($actual-$tiempo);
        if(  $tiempoTranscurrido >= $inactividad){ 
          $session->invalidate();
          return $this->redirectToRoute('usuario_logout');
         // En caso que este sea mayor del tiempo seteado lo deslogea. 
        }else{ 
            $_SESSION['time'] =$actual; 
            $perfiles=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
            $permisos=$permisoPerfil->verificacionPermisos($perfiles,'perfil');  
            if (sizeof($permisos) > 0) {
                try{

                    if ($this->isCsrfTokenValid('delete'.$perfil->getIdPerfil(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($perfil);
                        $entityManager->flush();
                        $successMessage= 'Se ha eliminado de forma correcta!';
                        $this->addFlash('mensaje',$successMessage);
                        return $this->redirectToRoute('perfil_index');
                    }

                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('perfil_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('perfil_index');
            }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
            }
        }
      }        
    }
}
