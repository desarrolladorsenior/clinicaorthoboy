<?php

namespace App\Controller;

use App\Entity\Clinica;
use App\Form\ClinicaType;
use App\Service\PerfilGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controlador de clinica utilizado para el ingreso princiapl al sistema y generar sedes dentro de una clinica, igualmente para la disposición de 
 * todos los usuarios que se manejaran para organizar tosdos los tratamientos o eventualidadesde la clinica,acorde a que todo el sistema manejara una 
 * clinica.
 * @Route("/clinica")
 */
class ClinicaController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de clinica y sus sedes.
     *
     * @Route("/", name="clinica_index", methods={"GET"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual;               
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'clinica');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $entityManager = $this->getDoctrine()->getManager();
                $dqlClinicaPrincipal ="SELECT clinica FROM App:Clinica clinica where clinica.idSedeClinica is null
                ORDER BY clinica.idClinica DESC";  
                $queryClinicaPrincipal = $entityManager->createQuery($dqlClinicaPrincipal);
                $clinicaPrincipal= $queryClinicaPrincipal->getResult();
                /*Selecciona todas las sedes de la clinica*/
                $dql ="SELECT clinica FROM App:Clinica clinica where clinica.idSedeClinica is not null
                ORDER BY clinica.idClinica DESC";  
                $queryClinicas = $entityManager->createQuery($dql);
                $clinicas= $queryClinicas->getResult();

                return $this->render('clinica/index.html.twig', [
                    'clinicas' => $clinicas,
                    'clinicaPrincipal'=>$clinicaPrincipal
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }  
                
            }
        }    
    }

    /**
     * Metodo utilizado para la visualización de los datos de la clinica y sus sedes.
     *
     * @Route("/sedes", name="clinicasedes_index", methods={"GET"})
     */
    public function indexSedes(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual;               
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'sede');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $entityManager = $this->getDoctrine()->getManager();
                $dqlClinicaPrincipal ="SELECT clinica FROM App:Clinica clinica where clinica.idSedeClinica is null
                ORDER BY clinica.idClinica DESC";  
                $queryClinicaPrincipal = $entityManager->createQuery($dqlClinicaPrincipal);
                $clinicaPrincipal= $queryClinicaPrincipal->getResult();
                /*Selecciona todas las sedes de la clinica*/
                $dql ="SELECT clinica FROM App:Clinica clinica where clinica.idSedeClinica is not null
                ORDER BY clinica.idClinica DESC";  
                $queryClinicas = $entityManager->createQuery($dql);
                $clinicas= $queryClinicas->getResult();

                return $this->render('clinica/indexSedes.html.twig', [
                    'clinicas' => $clinicas,
                    'clinicaPrincipal'=>$clinicaPrincipal
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }  
                
            }
        }    
    }

     /**
     * Metodo utilizado para el registro de sedes, el cual redirecciona segun los permisos que tenga el usuario.
     *
     * @Route("/newSedeClinica", name="clinica_newSedeClinica", methods={"GET","POST"})
     */
    public function newSedeClinica(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisosClinica=$permisoPerfil->verificacionPermisos($perfil,'clinica');  
              $permisosSede=$permisoPerfil->verificacionPermisos($perfil,'sede');  
              $entityManager = $this->getDoctrine()->getManager();
              $clinica= new Clinica();
              $idClinica=$request->request->get('idClinica');
              $response = new JsonResponse();
              if (sizeof($permisosSede) > 0 and sizeof($permisosClinica) > 0){      
                $dql ="SELECT clinica FROM App:Clinica clinica
                WHERE clinica.idClinica = :idClinica";  
                $queryClinica = $entityManager->createQuery($dql)->setParameter('idClinica',$idClinica);
                $clinicaP= $queryClinica->getResult();
                foreach ($clinicaP as $clinicaSeleccionada ) {             
                  $clinica->getIdSedeClinica($clinica->setIdSedeClinica($clinicaSeleccionada));
                  $clinica->getNit($clinica->setNit($clinicaSeleccionada->getNit()));
                }
                $form = $this->createForm(ClinicaType::class, $clinica);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) { 
                    $clinica->getEstadoSede($clinica->setEstadoSede(true));   
                    $file =  $form->get('logo')->getData();
                    $clinica->uploadUno($file);     
                    $entityManager->persist($clinica);
                    $entityManager->flush();
                    $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');

                  if (sizeof($permisosClinica) <= 0 and $permisosSede > 0) { 
                    return $this->redirectToRoute('clinicasedes_index');
                  }else if(sizeof($permisosClinica) > 0 and $permisosSede > 0){
                    return $this->redirectToRoute('clinica_index');
                  }
                }

                return $this->render('clinica/modalNewSede.html.twig', [
                    'clinica' => $clinica,
                    'form' => $form->createView(),
                ]);
              }else if (sizeof($permisosClinica) > 0 and sizeof($permisosSede) == 0) {  
                $response->setData('Este recurso no es permitido para su perfil!');
                return $response;
              }else{
                $response->setData('Este recurso no es permitido para su perfil!');
                return $response;
              }    
            }
        }     
        
    }

     /**
     * Metodo utilizado para la edición del estado de las sedes.
     *
     * @Route("/editarestadoClinica", name="clinica_editestado", methods={"GET","POST"})
     */
    public function editEstadoClinica(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisosSede=$permisoPerfil->verificacionPermisos($perfil,'sede');  
              //var_dump('expression',sizeof( $permisos));
              $response = new JsonResponse();
              if (sizeof( $permisosSede) > 0 ) {  
                $em=$this->getDoctrine()->getManager();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }
                $dqlModificarEstado="UPDATE App:Clinica clinica SET clinica.estadoSede= :estado WHERE clinica.idClinica= :idClinica";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idClinica', $id);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $response->setData(['error'=>'Este recurso no es permitido para su perfil!']);
                return $response;
              }    
            }
        }    

    }

    /**
     * Metodo utilizado para la visualización de los datos de la clinica y/o sus sedes.
     *
     * @Route("/{idClinica}", name="clinica_show", methods={"GET","POST"})
     */
    public function show(Clinica $clinica,Request $request,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
        return $this->redirectToRoute('usuario_logout'); 
      } else{
        // La variable $_SESSION['usuario'] es un ejemplo. 
        //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
        if(isset($_SESSION['time'])){ 
          $tiempo = $_SESSION['time']; 
        }else{ 
          $tiempo = strtotime(date("Y-m-d H:i:s"));
        } 
        $inactividad =1800;   //Exprecion en segundos. 
        $actual =  strtotime(date("Y-m-d H:i:s")); 
        $tiempoTranscurrido=($actual-$tiempo);
        if(  $tiempoTranscurrido >= $inactividad){ 
          $session->invalidate();
          return $this->redirectToRoute('usuario_logout');
         // En caso que este sea mayor del tiempo seteado lo deslogea. 
        }else{ 
          $response = new JsonResponse();
          $_SESSION['time'] =$actual; 
          $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
          $permisosSede=$permisoPerfil->verificacionPermisos($perfil,'sede');  
          $permisosClinica=$permisoPerfil->verificacionPermisos($perfil,'clinica');  
              //var_dump('expression',sizeof( $permisos));
          if (sizeof( $permisosSede) > 0 or sizeof($permisosClinica) >0 ) {  
            return $this->render('clinica/modalShow.html.twig', [
              'clinica' => $clinica,
            ]);
          }else{
            $response->setData('Este recurso no es permitido para su perfil!');
            return $response;
          }    
        }
      }    
    }

    /**
     * Metodo utilizado para la edición de clinica y sus sedes segun los permisos que tenga cada usuario.
     *
     * @Route("/{idClinica}/edit", name="clinica_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Clinica $clinica,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual;
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisosClinica=$permisoPerfil->verificacionPermisos($perfil,'clinica');  
              $permisosSede=$permisoPerfil->verificacionPermisos($perfil,'sede');  
              $response= new JsonResponse();
                  //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisosClinica) > 0 or sizeof($permisosSede) > 0) {
                $em=$this->getDoctrine()->getManager();
                $idClinica=$clinica->getIdClinica();
                $dqlClinica ="SELECT clinica FROM App:Clinica clinica WHERE clinica.idClinica = :idClinica ";  
                $query = $em->createQuery($dqlClinica)->setParameter('idClinica',$idClinica);
                $clinicas= $query->getResult();
                foreach ($clinicas as $clinicaSelec) {
                  $adj1= $clinicaSelec->getLogo();
                  $sede=$clinicaSelec->getIdSedeClinica();
                }
                if (!empty($sede)) {
                  $sede='sede';
                }else{
                  $sede='principal';
                }
                
                $form = $this->createForm(ClinicaType::class, $clinica);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    
                    $file =  $form->get('logo')->getData();
                    $clinica->uploadUno($file); 
                    if (empty($clinica->getLogo())) {
                        $clinica->getLogo($clinica->setLogo($adj1));
                    } 
                    $em->persist($clinica);
                    $em->flush($clinica);
                    $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                    if (sizeof($permisosClinica) <= 0 and $permisosSede > 0) { 
                      return $this->redirectToRoute('clinicasedes_index');
                    }else if(sizeof($permisosClinica) > 0 and $permisosSede > 0){
                      return $this->redirectToRoute('clinica_index');
                    }
                }
                if (sizeof($permisosClinica) > 0  and sizeof($permisosSede) > 0  and $sede == 'principal') {
                  return $this->render('clinica/modalEdit.html.twig', [
                    'clinica' => $clinica,
                    'form' => $form->createView(),
                    'sede'=>$sede
                  ]);
                }elseif (sizeof($permisosClinica) > 0 and sizeof($permisosSede) == 0 and $sede == 'principal') {
                  return $this->render('clinica/modalEdit.html.twig', [
                    'clinica' => $clinica,
                    'form' => $form->createView(),
                    'sede'=>$sede
                  ]);
                }elseif (sizeof($permisosClinica) == 0 and sizeof($permisosSede) > 0 and $sede == 'sede'  ) {
                  return $this->render('clinica/modalEdit.html.twig', [
                    'clinica' => $clinica,
                    'form' => $form->createView(),
                    'sede'=>$sede
                  ]);
                }elseif (sizeof($permisosClinica) > 0 and sizeof($permisosSede) == 0 and $sede == 'sede') {
                  $response->setData('Este recurso no es permitido para su perfil!');
                  return $response;
                }
                
              }else{
                $response->setData('Este recurso no es permitido para su perfil!');
                return $response;
              }  
            }
        }
    }

    /**
     * Metodo utilizado para la eliminación de sedes.
     *
     * @Route("/{idClinica}", name="clinica_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Clinica $clinica,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
        return $this->redirectToRoute('usuario_logout'); 
      } else{
        // La variable $_SESSION['usuario'] es un ejemplo. 
        //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
        if(isset($_SESSION['time'])){ 
          $tiempo = $_SESSION['time']; 
        }else{ 
          $tiempo = strtotime(date("Y-m-d H:i:s"));
        } 
        $inactividad =1800;   //Exprecion en segundos. 
        $actual =  strtotime(date("Y-m-d H:i:s")); 
        $tiempoTranscurrido=($actual-$tiempo);
        if(  $tiempoTranscurrido >= $inactividad){ 
          $session->invalidate();
          return $this->redirectToRoute('usuario_logout');
         // En caso que este sea mayor del tiempo seteado lo deslogea. 
        }else{ 
          $_SESSION['time'] =$actual; 
          $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
          $permisosClinica=$permisoPerfil->verificacionPermisos($perfil,'clinica');  
          $permisosSede=$permisoPerfil->verificacionPermisos($perfil,'sede');  
              //var_dump('expression',sizeof( $permisos));
          if (sizeof($permisosClinica) > 0 or sizeof($permisosSede) > 0) {
            try{

                if ($this->isCsrfTokenValid('delete'.$clinica->getIdClinica(), $request->request->get('_token'))) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($clinica);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    if (sizeof($permisosClinica) <= 0 and $permisosSede > 0) { 
                      return $this->redirectToRoute('clinicasedes_index');
                    }else if(sizeof($permisosClinica) > 0 and $permisosSede > 0){
                      return $this->redirectToRoute('clinica_index');
                    }
                }

            }catch (\Doctrine\DBAL\DBALException $e) {
                if ($e->getCode() == 0){
                    if ($e->getPrevious()->getCode() == 23503){
                        $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                        $this->addFlash('error',$successMessage1);
                        return $this->redirectToRoute('clinica_index');
                        if (sizeof($permisosClinica) <= 0 and $permisosSede > 0) { 
                          return $this->redirectToRoute('clinicasedes_index');
                        }else if(sizeof($permisosClinica) > 0 and $permisosSede > 0){
                          return $this->redirectToRoute('clinica_index');
                        }
                    }else{
                        throw $e;
                    }
                }else{
                    throw $e;
                }
            } 
            if (sizeof($permisosClinica) <= 0 and $permisosSede > 0) { 
              return $this->redirectToRoute('clinicasedes_index');
            }else if(sizeof($permisosClinica) > 0 and $permisosSede > 0){
              return $this->redirectToRoute('clinica_index');
            }
          }else{
            $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
            return $this->redirectToRoute('panel_principal');
          }    
        }
      } 
    }  
}
