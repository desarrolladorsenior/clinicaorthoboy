<?php

namespace App\Controller;

use App\Entity\Stock;
use App\Form\StockType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de stock utilizado para el manejo de las cantidades minimas de un elemento.
 *
 * @Route("/stock")
 */
class StockController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de un stock.
     *
     * @Route("/", name="stock_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Stock inventario');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    ///Seleccion de los datos de la tabla stock 
                    $dqlStockCompleto="SELECT stock FROM App:Stock stock ORDER BY stock.idStock DESC";
                    $queryStockCompleto = $entityManager->createQuery($dqlStockCompleto);
                    $stocksCompleto= $queryStockCompleto->getResult();
                    //Seleccion de entradas por elemento y clinica del stock
                    $dqlStockEntradas="SELECT nomInsumo.nombre as nombreInsumo,nomInsumo.idNombreInsumo,IDENTITY(entrada.idClinicaUbicacion) as idClinica, sum(entrada.cantidad) as totalEntrada, stock.idStock FROM App:EntradaPedido entrada INNER JOIN App:Insumo insumo WITH insumo.idInsumo=entrada.idInsumo INNER JOIN App:NombreInsumo nomInsumo WITH nomInsumo.idNombreInsumo=insumo.idNombreInsumo INNER JOIN App:Stock stock WITH nomInsumo.idNombreInsumo=stock.idNombreInsumo where stock.idClinica = entrada.idClinicaUbicacion GROUP BY entrada.idClinicaUbicacion, nomInsumo.idNombreInsumo,nomInsumo.idNombreInsumo,stock.idStock";
                    $queryStockEntradas = $entityManager->createQuery($dqlStockEntradas);
                    $stocksEntradas= $queryStockEntradas->getResult();
                    //Seleccion de salidas por elemento y clinica del stock
                    $dqlTotalSalidas="SELECT nomInsumo.nombre as nombreInsumo,IDENTITY(entrada.idClinicaUbicacion) as sede,sum(salida.cantidad) as totalSalidas,stock.idStock FROM App:EntradaPedido entrada INNER JOIN App:Insumo insumo WITH insumo.idInsumo=entrada.idInsumo INNER JOIN App:NombreInsumo nomInsumo WITH nomInsumo.idNombreInsumo=insumo.idNombreInsumo INNER JOIN App:Stock stock WITH nomInsumo.idNombreInsumo=stock.idNombreInsumo inner JOIN App:Salida salida WITH salida.idEntradaPedido=entrada.idEntradaPedido where entrada.idClinicaUbicacion=stock.idClinica GROUP BY nomInsumo.nombre,entrada.idClinicaUbicacion,stock.idStock";
                    $queryTotalSalidas = $entityManager->createQuery($dqlTotalSalidas);
                    $totalSalidas= $queryTotalSalidas->getResult();

                    //Selección de todas las sedes de la clinica
                    $dqlClinicas ="SELECT clinica FROM App:Clinica clinica
                    WHERE clinica.idSedeClinica is not null ORDER BY clinica.idClinica ASC";  
                    $queryClinicas = $entityManager->createQuery($dqlClinicas);
                    $clinicas= $queryClinicas->getResult();
                    /*Pruebas iniciales de stock
                    ///Seleccion de los datos de la tabla stock que no tengan convenio con entradas a pedido o ingreso de elementos 
                    $dqlStockPrincipal="SELECT stock FROM App:Stock stock WHERE stock.idNombreInsumo not in (SELECT nomInsumo.idNombreInsumo FROM App:EntradaPedido entrada INNER JOIN App:Insumo insumo WITH insumo.idInsumo=entrada.idInsumo INNER JOIN App:NombreInsumo nomInsumo WITH nomInsumo.idNombreInsumo=insumo.idNombreInsumo 
                        INNER JOIN App:Stock stocks WITH nomInsumo.idNombreInsumo=stocks.idNombreInsumo 
                        where entrada.idClinicaUbicacion=stocks.idClinica) ORDER BY stock.idStock DESC";
                    $queryStockPrincipal = $entityManager->createQuery($dqlStockPrincipal);
                    $stocksPrincipal= $queryStockPrincipal->getResult();
                    //Selección de todos los datos en la tabla stock que tengan ingreso de elementos o entradas de pedidos.
                    $dqlStock="SELECT nomInsumo.nombre as nombreInsumo,IDENTITY(entrada.idClinicaUbicacion) as sedeEntrada ,sum(entrada.cantidad) as totalEntradas,stock.idStock ,IDENTITY(stock.idClinica) as sede,stock.cantidad,stock.estado FROM App:EntradaPedido entrada INNER JOIN App:Insumo insumo WITH insumo.idInsumo=entrada.idInsumo INNER JOIN App:NombreInsumo nomInsumo WITH nomInsumo.idNombreInsumo=insumo.idNombreInsumo INNER JOIN App:Stock stock WITH nomInsumo.idNombreInsumo=stock.idNombreInsumo where entrada.idClinicaUbicacion=stock.idClinica GROUP BY nomInsumo.nombre,entrada.idClinicaUbicacion,stock.idStock ORDER BY stock.idStock DESC";
                    $queryStock = $entityManager->createQuery($dqlStock);
                    $stocks= $queryStock->getResult();*/
                    
                    //Inicialización de objeto
                    $stock = new Stock();
                    $stock->setEstado(TRUE);
                    $form = $this->createForm(StockType::class, $stock);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $dqlStockClinica ="SELECT stock FROM App:Stock stock WHERE stock.idNombreInsumo = :idNombreInsumo and stock.idClinica = :idClinica";  
                        $queryStockClinica = $entityManager->createQuery($dqlStockClinica)->setParameter('idNombreInsumo',$stock->getIdNombreInsumo()->getIdNombreInsumo())->setParameter('idClinica',$stock->getIdClinica()->getIdClinica());
                        $stocksClinica= $queryStockClinica->getResult();
                        if (sizeof($stocksClinica) >0) {
                           $this->addFlash('error', '¡No se ha realizado el ingreso, usted cuenta con stock del elemento para la sede!');
                            return $this->redirectToRoute('stock_index');
                        }else{
                            $entityManager->persist($stock);
                            $entityManager->flush();                        
                            $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                            return $this->redirectToRoute('stock_index');
                        }                        
                    }
                    return $this->render('stock/index.html.twig', [
                        'clinicas'=>$clinicas,
                        'totalSalidas'=>$totalSalidas,
                        'stocksCompleto'=>$stocksCompleto,
                        'stocksEntradas'=>$stocksEntradas,
                        'stock' => $stock,
                        'form' => $form->createView()
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }


    /**
     * Metodo utilizado para la edición de un stock.
     *
     * @Route("/{idStock}/edit", name="stock_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Stock $stock,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Stock inventario');  
                if (sizeof($permisos) > 0) {                    
                    $entityManager = $this->getDoctrine()->getManager();
                    $form = $this->createForm(StockType::class, $stock);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $dqlStockClinica ="SELECT stock FROM App:Stock stock WHERE stock.idNombreInsumo = :idNombreInsumo and stock.idClinica = :idClinica and stock.idStock != :idStock";  
                        $queryStockClinica = $entityManager->createQuery($dqlStockClinica)->setParameter('idNombreInsumo',$stock->getIdNombreInsumo()->getIdNombreInsumo())->setParameter('idClinica',$stock->getIdClinica()->getIdClinica())->setParameter('idStock',$stock->getIdStock());
                        $stocksClinica= $queryStockClinica->getResult();
                        if (sizeof($stocksClinica) >0) {
                           $this->addFlash('error', '¡No se ha realizado la modificación, usted cuenta con stock del elemento para la sede!');
                            return $this->redirectToRoute('stock_index');
                        }else{
                            $entityManager->persist($stock);
                            $entityManager->flush();                        
                            $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                            return $this->redirectToRoute('stock_index');
                        } 
                    }
                    return $this->render('stock/modalEdit.html.twig', [
                        'stock' => $stock,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }     
    }



    /**
     * Metodo utilizado para la edición del estado de un stock.
     *
     * @Route("/editarEstadoStock", name="stock_editestado", methods={"GET","POST"})
     */
    public function editEstado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Stock inventario');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $id=$request->request->get('id');
                    $estado=$request->request->get('estado');
                    if($estado == 'ACTIVO'){
                        $estado = true;
                    }else{
                        $estado = false;
                    }
                    $dqlModificarEstadoStock="UPDATE App:Stock stock SET stock.estado= :estado WHERE stock.idStock= :idStock";
                    $queryModificarEstadoStock=$em->createQuery($dqlModificarEstadoStock)->setParameter('estado', $estado)->setParameter('idStock', $id);
                    $modificarEstadoStock=$queryModificarEstadoStock->execute();
                    $response->setData('Se ha cambiado el estado de forma correcta!');
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }


    /**
     * Metodo utilizado para la eliminación de un stock.
     *
     * @Route("/{idStock}", name="stock_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Stock $stock,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'Stock inventario');  
              if (sizeof($permisos) > 0) {
                //var_dump($esterilizacion->getIdEsterilizacion());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($stock);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('stock_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('stock_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('stock_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        } 
    }
}
