<?php

namespace App\Controller;

use App\Entity\MotivoConsulta;
use App\Form\MotivoConsultaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de motivo consulta utilizado para el manejo de citas de un paciente.
 *
 * @Route("/motivoConsulta")
 */
class MotivoConsultaController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de un motivo Consulta y registro de la misma.
     *
     * @Route("/", name="motivoConsulta_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual;               
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'motivos de consulta');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $entityManager = $this->getDoctrine()->getManager();
                $dqlMotivoConsulta ="SELECT motivoConsulta FROM App:MotivoConsulta motivoConsulta
                ORDER BY motivoConsulta.idMotivoConsulta DESC";  
                $queryMotivoConsulta = $entityManager->createQuery($dqlMotivoConsulta);
                $motivosConsultas= $queryMotivoConsulta->getResult();
                /*Creación de una nuevo motivo de consulta*/
                $motivoConsulta = new MotivoConsulta();
                $form = $this->createForm(MotivoConsultaType::class, $motivoConsulta);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $motivoConsulta->setEstado(true);
                    $entityManager->persist($motivoConsulta);
                    $entityManager->flush();
                    $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                    return $this->redirectToRoute('motivoConsulta_index');
                }
                return $this->render('motivoConsulta/index.html.twig', [
                    'motivosConsultas' => $motivosConsultas,
                    'motivoConsulta' => $motivoConsulta,
                    'form' => $form->createView(),
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }  
                
            }
        }  
    }

     /**
     * Metodo utilizado para la edición del estado de un motivo Consulta.
     *
     * @Route("/editarestadoMotivoConsulta", name="motivoConsulta_editestado", methods={"GET","POST"})
     */
    public function editEstadoMotivoConsulta(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'motivos de consulta');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $em=$this->getDoctrine()->getManager();
                $response = new JsonResponse();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }
                $dqlModificarEstado="UPDATE App:MotivoConsulta motivoConsulta SET motivoConsulta.estado= :estado WHERE motivoConsulta.idMotivoConsulta= :idMotivoConsulta";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idMotivoConsulta', $id);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }    

    }


    /**
     * Metodo utilizado para la edición de un motivo Consulta.
     *
     * @Route("/{idMotivoConsulta}/edit", name="motivoConsulta_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MotivoConsulta $motivoConsulta,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual;               
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'motivos de consulta');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $form = $this->createForm(MotivoConsultaType::class, $motivoConsulta);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $this->getDoctrine()->getManager()->flush();
                    $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                    return $this->redirectToRoute('motivoConsulta_index');
                }

                return $this->render('motivoConsulta/modalEdit.html.twig', [
                    'motivoConsulta' => $motivoConsulta,
                    'form' => $form->createView(),
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }  
                
            }
        }    
    }

    /**
     * Metodo utilizado para la eliminación de un motivo Consulta.
     *
     * @Route("/{idMotivoConsulta}", name="motivoConsulta_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MotivoConsulta $motivoConsulta,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'motivos de consulta');  
              if (sizeof($permisos) > 0) {
                try{
                  if ($this->isCsrfTokenValid('delete'.$motivoConsulta->getIdMotivoConsulta(), $request->request->get('_token'))) {
                      $entityManager = $this->getDoctrine()->getManager();
                      $entityManager->remove($motivoConsulta);
                      $entityManager->flush();
                      $successMessage= 'Se ha eliminado de forma correcta!';
                      $this->addFlash('mensaje',$successMessage);
                      return $this->redirectToRoute('motivoConsulta_index');
                  }
                }catch (\Doctrine\DBAL\DBALException $e) {
                  if ($e->getCode() == 0){
                      if ($e->getPrevious()->getCode() == 23503){
                          $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                          $this->addFlash('error',$successMessage1);
                          return $this->redirectToRoute('finalidadconsulta_index');
                      }else{
                          throw $e;
                      }
                  }else{
                      throw $e;
                  }
                } 
                return $this->redirectToRoute('motivoConsulta_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }      
    }
}
