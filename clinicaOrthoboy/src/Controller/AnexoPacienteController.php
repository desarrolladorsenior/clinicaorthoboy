<?php

namespace App\Controller;

use App\Entity\AnexoPaciente;
use App\Entity\Paciente;
use App\Form\AnexoPacienteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

/**
 * Controlador de anexo paciente utilizo para dar consencuencia a una remisión donde se puede anexar datos o examenes solicitados al paciente, este 
 * busca que usuarios internos como externos puedan tener un usuario que incorpore examenes respecto a una remisión o incluso para los usuarios 
 * internos puedan ingresar anexos sin necesidad de tener una remisión.

 * @Route("/anexoPaciente")
 */
class AnexoPacienteController extends Controller
{
    /**
     * Metodo utilizado para la visualización de un anexo paciente y registro del mismo desde el metodo.
     *
     * @Route("/{idPaciente}", name="anexoPaciente_index", methods={"GET","POST"})
     */
    public function index(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'anexos');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlAnexosPaciente ="SELECT anex FROM App:AnexoPaciente anex WHERE anex.idPaciente = :idPaciente ORDER BY anex.idAnexoPaciente,anex.idRemision ASC";  
                    $queryAnexosPaciente = $entityManager->createQuery($dqlAnexosPaciente)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $anexosPacientes= $queryAnexosPaciente->getResult();
                    $idPac=$paciente->getIdPaciente();
                    $anexoPaciente = new AnexoPaciente();
                    $anexoPaciente->setIdPaciente($paciente);
                    $anexoPaciente->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(AnexoPacienteType::class, $anexoPaciente);
                    $form->add('idRemision',EntityType::class, array('placeholder' => 'Seleccionar remisión','class' => 'App:Remision','required'=>false,'query_builder' => function(EntityRepository $er)use ($idPac){
                          return $er->createQueryBuilder('e')
                          ->where('e.idPaciente = :idPaciente')
                          ->setParameter('idPaciente',$idPac)
                          ->orderBy('e.idRemision', 'Asc');
                        },))
                    ;
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $file =  $form->get('archivo')->getData();
                        //var_dump($file);
                        $anexoPaciente->uploadUno($file);
                        $entityManager->persist($anexoPaciente);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('anexoPaciente_index',['idPaciente'=>$paciente->getIdPaciente()]);
                    }
                    return $this->render('anexoPaciente/index.html.twig', [
                        'anexosPacientes' => $anexosPacientes,
                        'paciente'=>$paciente,
                        'anexoPaciente' => $anexoPaciente,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }
    

    /**
     * Metodo utilizado para la exposición de un pdf de un anexo de paciente.
     *
     * @Route("/{idAnexoPaciente}/pdf", name="anexoPaciente_pdf", methods={"GET","POST"})
     */
    public function pdf(AnexoPaciente $anexoPaciente,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'anexos');  
                if (sizeof($permisos) > 0) { 
                     $html =$this->renderView('anexoPaciente/pdfAnexo.html.twig',
                    array(
                    'anexoPaciente' => $anexoPaciente
                    ));
                     
                    $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                    [   'encoding' => 'UTF-8',
                        'images' => false,
                        'enable-javascript' => true,
                        'print-media-type' => true,
                        'outline-depth' => 8,
                        'orientation' => 'Portrait',
                        'header-right'=>'Pag. [page] de [toPage]',
                        'page-size' => 'A4',
                        'viewport-size' => '1280x1024',
                        'margin-left' => '10mm',
                        'margin-right' => '10mm',
                        'margin-top' => '30mm',
                        'margin-bottom' => '25mm',
                        'user-style-sheet'=> 'light/dist/assets/css/bootstrap.min.css',
                        //'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
                        //'background'     => 'light/dist/assets/images/users/user-1.jpg',
                        //--javascript-delay 3000
                    ]);
                    $filename="Anexo_N°_".$anexoPaciente->getIdAnexoPaciente()."Paciente_".$anexoPaciente->getIdPaciente()."_fecha_". date("Y_m_d") .".pdf";
                    $response = new Response ($pdf,
                    200,
                    array('Content-Type' => 'aplicattion/pdf',
                          'Content-Disposition' => 'inline; filename="'.$filename.'"'
                    ,));
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la visualización de un anexo de paciente.
     *
     * @Route("/{idAnexoPaciente}/show", name="anexoPaciente_show", methods={"GET","POST"})
     */
    public function show(AnexoPaciente $anexoPaciente,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'anexos');  
                if (sizeof($permisos) > 0) { 
                    /*Visualización edad del paciente*/
                    $fechaActual=date('d M Y H:i:s');
                    $fechaFinal=new  \DateTime($fechaActual);
                    $fechaNacimientoPaciente = $anexoPaciente->getIdPaciente()->getFechaNacimiento();
                    $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                    $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';
                    return $this->render('anexoPaciente/modalShow.html.twig', [
                        'anexoPaciente' => $anexoPaciente,
                        'edad'=>$edad
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la edición de un anexo de paciente.
     *
     * @Route("/{idAnexoPaciente}/edit", name="anexoPaciente_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnexoPaciente $anexoPaciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'anexos');  
                if (sizeof($permisos) > 0) { 
                    if ($anexoPaciente->getArchivo() != '') {
                        $archivoEliminar='/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/anexoPaciente/'.$anexoPaciente->getArchivo();
                    }else{
                        $archivoEliminar='';
                    }
                    $entityManager=$this->getDoctrine()->getManager();
                    $idPac=$anexoPaciente->getIdPaciente()->getIdPaciente();
                    $form = $this->createForm(AnexoPacienteType::class, $anexoPaciente);
                     $form->add('idRemision',EntityType::class, array('placeholder' => 'Seleccionar remisión','class' => 'App:Remision','required'=>false,'query_builder' => function(EntityRepository $er)use ($idPac){
                          return $er->createQueryBuilder('e')
                          ->where('e.idPaciente = :idPaciente')
                          ->setParameter('idPaciente',$idPac)
                          ->orderBy('e.idRemision', 'Asc');
                        },))
                    ;
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $file =  $form->get('archivo')->getData();
                        //var_dump($file);
                        $anexoPaciente->uploadUno($file);
                        $entityManager->persist($anexoPaciente);
                        $entityManager->flush();
                        if ($archivoEliminar != '') {
                            unlink($archivoEliminar);
                        }
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('anexoPaciente_index', [
                            'idPaciente' => $anexoPaciente->getIdPaciente()->getIdPaciente(),
                        ]);
                    }

                    return $this->render('anexoPaciente/modalEdit.html.twig', [
                        'anexoPaciente' => $anexoPaciente,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
        
    }

    /**
     * Metodo utilizado para la eliminación de un anexo de paciente.
     *
     * @Route("/{idAnexoPaciente}", name="anexoPaciente_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnexoPaciente $anexoPaciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'anexos');  
                if (sizeof($permisos) > 0) { 
                    $idPaciente=$anexoPaciente->getIdPaciente()->getIdPaciente();
                    try{
                        if ($this->isCsrfTokenValid('delete'.$anexoPaciente->getIdAnexoPaciente(), $request->request->get('_token'))) {
                            if ($anexoPaciente->getArchivo() != '') {
                                $archivoEliminar='/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/anexoPaciente/'.$anexoPaciente->getArchivo();
                            }else{
                                $archivoEliminar='';
                            }
                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->remove($anexoPaciente);
                            $entityManager->flush();
                            if ($archivoEliminar != '') {
                                unlink($archivoEliminar);
                            }
                            $successMessage= 'Se ha eliminado de forma correcta!';
                            $this->addFlash('mensaje',$successMessage);
                            return $this->redirectToRoute('anexoPaciente_index',['idPaciente'=>$idPaciente]);
                        }

                    }catch (\Doctrine\DBAL\DBALException $e) {
                        if ($e->getCode() == 0){
                            if ($e->getPrevious()->getCode() == 23503){
                                $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                                $this->addFlash('error',$successMessage1);
                                return $this->redirectToRoute('anexoPaciente_index',['idPaciente'=>$idPaciente]);
                            }else{
                                throw $e;
                            }
                        }else{
                            throw $e;
                        }
                    } 
                    /*if ($this->isCsrfTokenValid('delete'.$imagen->getIdImagen(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($imagen);
                        $entityManager->flush();
                    }*/

                    return $this->redirectToRoute('anexoPaciente_index',['idPaciente',$idPaciente]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }
}
