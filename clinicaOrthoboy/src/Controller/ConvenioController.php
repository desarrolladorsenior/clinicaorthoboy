<?php

namespace App\Controller;

use App\Entity\Convenio;
use App\Form\ConvenioType;
use App\Entity\TratamientoConvenio;
use App\Form\TratamientoConvenioType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\PerfilGenerator;


/**
 * Controlador de convenio utilizado para el manejo de los mismos con la eps.
 *
 * @Route("/convenio")
 */
class ConvenioController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de convenios y registro de convenios.
     *
     * @Route("/", name="convenio_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'EPS y sedes');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlConvenio ="SELECT convenio FROM App:Convenio convenio
                    ORDER BY convenio.idConvenio DESC";  
                    $queryConvenios = $entityManager->createQuery($dqlConvenio);
                    $convenios= $queryConvenios->getResult();

                    $dqlConvenioTratamiento ="SELECT tratamientoConvenio FROM App:TratamientoConvenio tratamientoConvenio ORDER BY tratamientoConvenio.idTratamientoConvenio DESC";  
                    $queryConvenioTratamiento = $entityManager->createQuery($dqlConvenioTratamiento);
                    $conveniosTratamientos= $queryConvenioTratamiento->getResult();


                    $convenio = new Convenio();
                    /*Formulario Eps para crear a nivel modal*/
                    $form = $this->createForm(ConvenioType::class, $convenio);
                    $form->handleRequest($request);
                    $dqlClinica ="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                    $queryClinica = $entityManager->createQuery($dqlClinica);
                    $clinicas= $queryClinica->getResult();
                    foreach ($clinicas as $clinicaSeleccionado ) {             
                       $convenio->getIdClinica($convenio->setIdClinica($clinicaSeleccionado));
                    }
                    $convenio->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    if ($form->isSubmitted() && $form->isValid()) {
                        $dqlConvenio ="SELECT convenio FROM App:Convenio convenio WHERE convenio.idEps = :idEps and convenio.estado =:estado";
                        $queryConvenio = $entityManager->createQuery($dqlConvenio)->setParameter('idEps',$convenio->getIdEps())->setParameter('estado',true);
                        $convenios= $queryConvenio->getResult();
                        
                        if (sizeof($convenios) >0) {
                            $this->addFlash('error', 'En este momento se encuentra un convenio activo. No es posible crear un nuevo convenio con esta eps!');
                            return $this->redirectToRoute('convenio_index');
                        }else{
                            $convenio->getEstado($convenio->setEstado(true));             
                            $entityManager->persist($convenio); 
                            $entityManager->flush();
                            $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                            return $this->redirectToRoute('convenio_index');
                        }
                    }

                    return $this->render('convenio/index.html.twig', [
                        'convenios' => $convenios,
                        'convenio' => $convenio,
                        'form' => $form->createView(), 
                        'conveniosTratamientos'=>$conveniosTratamientos
                    ]);
                }else{
                        $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                        return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }


    /**
     * Metodo utilizado para la visualización  de un convenio.
     *
     * @Route("/{idConvenio}/show", name="convenio_show", methods={"GET","POST"})
     */
    public function show(Convenio $convenio,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Convenio');  
                if (sizeof($permisos) > 0) {
                    return $this->render('convenio/modalVerDetalleConvenio.html.twig', [
                        'convenio' => $convenio,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }


     /**
     * Metodo utilizado para el registro de un convenio.
     *
     * @Route("/newTratamientoConvenio", name="convenio_newTratamientoConvenio", methods={"GET","POST"})
     */
    public function newConvenioTratamiento(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'tratamientos - EPS');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $tratamientoConvenio= new TratamientoConvenio();
                    $idConvenio=$request->request->get('idConvenio');        
                    $dql ="SELECT convenio FROM App:Convenio convenio WHERE convenio.idConvenio = :idConvenio and convenio.estado = :estado";
                    $queryConvenio = $entityManager->createQuery($dql)->setParameter('idConvenio',$idConvenio)->setParameter('estado',true);
                    $convenios= $queryConvenio->getResult();
                    foreach ($convenios as $convenioSeleccionado ) {             
                        $tratamientoConvenio->getIdConvenio($tratamientoConvenio->setIdConvenio($convenioSeleccionado));
                    }                    
                    $tratamientoConvenio->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(TratamientoConvenioType::class, $tratamientoConvenio);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {  
                        $tratamientoConvenio->getEstado($tratamientoConvenio->setEstado(true));             
                        $entityManager->persist($tratamientoConvenio); 
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('convenio_index');
                    }

                    return $this->render('convenio/modalNewConvenioTratamiento.html.twig', [
                        'tratamientoConvenio' => $tratamientoConvenio,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la edición de un convenio.
     *
     * @Route("/{idConvenio}/edit", name="convenio_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Convenio $convenio,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Convenio');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlClinica ="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                    $queryClinica = $entityManager->createQuery($dqlClinica);
                    $clinicas= $queryClinica->getResult();
                    foreach ($clinicas as $clinicaSeleccionado ) {             
                       $convenio->getIdClinica($convenio->setIdClinica($clinicaSeleccionado));
                    }
                    $convenio->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $editForm = $this->createForm(ConvenioType::class, $convenio);
                    $editForm->handleRequest($request);
                    if ($editForm->isSubmitted() && $editForm->isValid()) {
                        $dqlConvenio ="SELECT convenio FROM App:Convenio convenio WHERE convenio.idEps = :idEps and convenio.estado =:estado and convenio.idConvenio != :idConvenio";
                        $queryConvenio = $entityManager->createQuery($dqlConvenio)->setParameter('idEps',$convenio->getIdEps())->setParameter('estado',true)->setParameter('idConvenio',$convenio->getIdConvenio());
                        $convenios= $queryConvenio->getResult();                        
                        if (sizeof($convenios) >0) {
                            $this->addFlash('error', 'En este momento se encuentra un convenio activo. No es posible crear un nuevo convenio con esta eps!');
                            return $this->redirectToRoute('convenio_index');
                        }else{       
                            $entityManager->persist($convenio); 
                            $entityManager->flush();
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                            return $this->redirectToRoute('convenio_index');
                        }
                    }
                    return $this->render('convenio/modalEdit.html.twig', [
                        'convenio' => $convenio,
                        'form' => $editForm->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

     
     /**
     * Metodo utilizado para la edición de estado de un convenio.
     *
     * @Route("/editarEstadoConvenio", name="convenio_editestado", methods={"GET","POST"})
     */
    public function editEstado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Convenio');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $id=$request->request->get('id');
                    $estado=$request->request->get('estado');
                    if($estado == 'ACTIVO'){
                        $estado = true;
                    }else{
                        $estado = false;
                    }

                    $dqlConvenio ="SELECT convenio FROM App:Convenio convenio WHERE convenio.idConvenio = :idConvenio";
                    $queryConvenio = $em->createQuery($dqlConvenio)->setParameter('idConvenio',$id);
                    $convenios= $queryConvenio->getResult();  
                    foreach ($convenios as $selecioneConvenio) {
                        $idEps=$selecioneConvenio->getIdEps()->getIdEps();
                        $fechaFinalizacion=$selecioneConvenio->getFechaFinalizacion();
                    }

                    $fechaActual= date('Y-m-d');
                    $fechaFinalizacionFinal=$fechaFinalizacion->format('Y-m-d');
                    if ($fechaActual < $fechaFinalizacionFinal) {
                        $dqlConvenios ="SELECT convenio FROM App:Convenio convenio WHERE convenio.idEps = :idEps and convenio.estado =:estado and convenio.idConvenio != :idConvenio";
                        $queryConvenios = $em->createQuery($dqlConvenios)->setParameter('idEps',$idEps)->setParameter('estado',true)->setParameter('idConvenio',$id);
                        $convenioTotal= $queryConvenios->getResult();                        
                        if (sizeof($convenioTotal) >0) {
                            $response->setData('En este momento se encuentra un convenio activo. No es posible cambiar el estado.');
                            return $response;
                        }else{   
                            $dqlModificarEstado="UPDATE App:Convenio convenio SET convenio.estado= :estado WHERE convenio.idConvenio= :idConvenio";
                            $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idConvenio', $id);
                            $modificarEstado=$queryModificarEstado->execute();

                            $dqlModificarEstadoTratamiento="UPDATE App:TratamientoConvenio tConvenio SET tConvenio.estado= :estado WHERE tConvenio.idConvenio= :idConvenio";
                            $queryModificarEstadoTratamiento=$em->createQuery($dqlModificarEstadoTratamiento)->setParameter('estado', $estado)->setParameter('idConvenio', $id);
                            $modificarEstadoCTratamiento=$queryModificarEstadoTratamiento->execute();
                            $response->setData('Se ha cambiado el estado de forma correcta!');
                            return $response;
                        }
                    }else{                        
                        $response->setData('El estado no se puede cambiar a causa de que la fecha de finalizacion es inferior a la fecha actual.');
                        return $response;
                    }                   
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la eliminación de un convenio.
     *
     * @Route("/{idConvenio}", name="convenio_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Convenio $convenio,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
        return $this->redirectToRoute('usuario_logout'); 
      } else{
        // La variable $_SESSION['usuario'] es un ejemplo. 
        //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
        if(isset($_SESSION['time'])){ 
          $tiempo = $_SESSION['time']; 
        }else{ 
          $tiempo = strtotime(date("Y-m-d H:i:s"));
        } 
        $inactividad =1800;   //Exprecion en segundos. 
        $actual =  strtotime(date("Y-m-d H:i:s")); 
        $tiempoTranscurrido=($actual-$tiempo);
        if(  $tiempoTranscurrido >= $inactividad){ 
          $session->invalidate();
          return $this->redirectToRoute('usuario_logout');
         // En caso que este sea mayor del tiempo seteado lo deslogea. 
        }else{ 
          $_SESSION['time'] =$actual; 
          $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
          $permisos=$permisoPerfil->verificacionPermisos($perfil,'Convenio');  
          if (sizeof($permisos) > 0) {
            try{
              if ($this->isCsrfTokenValid('delete'.$convenio->getIdConvenio(), $request->request->get('_token'))) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $entityManager->remove($convenio);
                  $entityManager->flush();
                  $successMessage= 'Se ha eliminado de forma correcta!';
                  $this->addFlash('mensaje',$successMessage);
                  return $this->redirectToRoute('convenio_index');
              }
            }catch (\Doctrine\DBAL\DBALException $e) {
              if ($e->getCode() == 0){
                  if ($e->getPrevious()->getCode() == 23503){
                      $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                      $this->addFlash('error',$successMessage1);
                      return $this->redirectToRoute('convenio_index');
                  }else{
                      throw $e;
                  }
              }else{
                  throw $e;
              }
            } 
            return $this->redirectToRoute('convenio_index');
          }else{
            $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
            return $this->redirectToRoute('panel_principal');
          }    
        }
      }      
    }
}