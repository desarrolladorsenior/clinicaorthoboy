<?php

namespace App\Controller;

use App\Entity\DiagnosticoPrincipal;
use App\Form\DiagnosticoPrincipalType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de diagnostico Principal utilizado para el manejo del odontograma.
 *
 * @Route("/diagnosticoPrincipal")
 */
class DiagnosticoPrincipalController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización diagnostico principal y registro del mismo.
     *
     * @Route("/", name="diagnosticoPrincipal_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'diagnosticos principales');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlDiagnosticoPrincipal ="SELECT diagnosticoPrincipal FROM App:DiagnosticoPrincipal diagnosticoPrincipal
                    ORDER BY diagnosticoPrincipal.idDiagnosticoPrincipal DESC";  
                    $queryDiagnosticoPrincipal = $entityManager->createQuery($dqlDiagnosticoPrincipal);
                    $diagnosticoPrincipales= $queryDiagnosticoPrincipal->getResult();
                    
                    $diagnosticoPrincipal = new DiagnosticoPrincipal();
                    $form = $this->createForm(DiagnosticoPrincipalType::class, $diagnosticoPrincipal);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($diagnosticoPrincipal);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('diagnosticoPrincipal_index');
                    }
                    return $this->render('diagnosticoPrincipal/index.html.twig', [
                        'diagnosticoPrincipales' => $diagnosticoPrincipales,
                        'diagnosticoPrincipal' => $diagnosticoPrincipal,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }
    /**
     * Metodo utilizado para la edición de un diagnostico principal.
     *
     * @Route("/{idDiagnosticoPrincipal}/edit", name="diagnosticoPrincipal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DiagnosticoPrincipal $diagnosticoPrincipal,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'diagnosticos principales');  
                if (sizeof($permisos) > 0) {
                    $form = $this->createForm(DiagnosticoPrincipalType::class, $diagnosticoPrincipal);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('diagnosticoPrincipal_index');
                    }
                    return $this->render('diagnosticoPrincipal/modalEdit.html.twig', [
                        'diagnosticoPrincipal' => $diagnosticoPrincipal,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de un diagnostico principal.
     *
     * @Route("/{idDiagnosticoPrincipal}", name="diagnosticoPrincipal_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DiagnosticoPrincipal $diagnosticoPrincipal,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'diagnosticos principales');  
              if (sizeof($permisos) > 0) {
                try{
                    if ($diagnosticoPrincipal->getIdDiagnosticoPrincipal() == 1 | $diagnosticoPrincipal->getIdDiagnosticoPrincipal() == 2 | $diagnosticoPrincipal->getIdDiagnosticoPrincipal() == 3 ){
                        $successMessage= '¡.. Error al eliminar el registro. Este es predeterminado por el sistema.!';
                        $this->addFlash('error',$successMessage);
                    }else{

                        if ($this->isCsrfTokenValid('delete'.$diagnosticoPrincipal->getIdDiagnosticoPrincipal(), $request->request->get('_token'))) {
                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->remove($diagnosticoPrincipal);
                            $entityManager->flush();
                            $successMessage= 'Se ha eliminado de forma correcta!';
                            $this->addFlash('mensaje',$successMessage);
                            return $this->redirectToRoute('diagnosticoPrincipal_index');
                        }
                    }
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('diagnosticoPrincipal_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('diagnosticoPrincipal_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }    
    }
}
