<?php

namespace App\Controller;

use App\Entity\Remision;
use App\Entity\Usuario;
use App\Entity\Paciente;
use App\Form\RemisionType;
use App\Service\PerfilGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Bundle\SnappyBundle\Snappy\Response\JpegResponse;

/**
 * Controlador de remisión utilizado para el manejo de solicitude de anexos sobre un paciente dependiendo del diagnostico que tenga cada uno de los pacientes.
 *
 * @Route("/remision")
 */
class RemisionController extends Controller
{
   
    /**
     * Metodo utilizado para el registro de remisiones, donde se evalua que si la remisión es externa que el correo ingresado no pertenezca a ningun 
     * usuario de la compañia con perfil interno, ademas que se notifica al usuario interno o externo de la remisión que se acaba de generar.
     *
     * @Route("/{idPaciente}/new", name="remision_new", methods={"GET","POST"})
     */
    public function new(Request $request,PerfilGenerator $permisoPerfil,UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer,Paciente $paciente): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'remisiones');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $entityManager = $this->getDoctrine()->getManager();                
                $remision = new Remision();
                /*$remisiones='';
                $pacientes='';
                $edad='';
                $idPaciente='';
                //if ($request->get('dato')) {
                $idPaciente=$paciente->getIdPaciente();
                $dqlPaciente="SELECT paciente FROM App:Paciente paciente WHERE paciente.idPaciente = :paciente";
                $queryPaciente=$entityManager->createQuery($dqlPaciente)->setParameter('paciente',$idPaciente);
                $pacientes=$queryPaciente->getResult();
                foreach ($pacientes as $pacienteSeleccionado) {
                }*/
                $remision->setIdPaciente($paciente);
                $fechaActual=date('d M Y H:i:s');
                $fechaFinal=new  \DateTime($fechaActual);
                $fechaNacimientoPaciente = $paciente->getFechaNacimiento();
                $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';

                $dqlRemisiones="SELECT remision FROM App:Remision remision WHERE remision.idPaciente = :paciente ORDER BY remision.fechaApertura DESC";
                $queryRemisiones=$entityManager->createQuery($dqlRemisiones)->setParameter('paciente',$paciente->getIdPaciente());
                $remisiones=$queryRemisiones->getResult();
                //}
                $dqlClinica="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                $queryClinica=$entityManager->createQuery($dqlClinica);
                $clinicas=$queryClinica->getResult();
                $remision->setIdUsuarioRemite($this->get('security.token_storage')->getToken()->getUser());
                $form = $this->createForm(RemisionType::class, $remision);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $fechaVe=$request->request->get('remision_fechaRealizar_final');
                    $dt = new \DateTime($fechaVe);
                    $remision->setFechaRealizar($dt);
                    
                    if(strtolower($remision->getEmail()) != '' and $remision->getIdUsuarioRemitido() == ''){    
                        
                        $entityManager->persist($remision);
                        $entityManager->flush();

                        $dql ="SELECT usuario FROM App:Usuario usuario
                        WHERE usuario.email = :email and usuario.tipoUsuario = :tipoUsuario";  
                        $queryUsuario = $entityManager->createQuery($dql)->setParameter('email',strtolower($remision->getEmail()))->setParameter('tipoUsuario','INTERNO');
                        $usuarios= $queryUsuario->getResult();
                        if (sizeof($usuarios) > 0) {
                            $this->addFlash('error', 'Esta dirección corresponde a un usuario interno del sistema, si desea enviar una remisión externa debe ingresar otro e-mail.');
                        }else{
                            $dql ="SELECT usuario FROM App:Usuario usuario
                            WHERE usuario.email = :email and usuario.tipoUsuario = :tipoUsuario";  
                            $queryUsuario = $entityManager->createQuery($dql)->setParameter('email',strtolower($remision->getEmail()))->setParameter('tipoUsuario','EXTERNO');
                            $usuarios= $queryUsuario->getResult();
                            if (sizeof($usuarios) > 0) {
                                $fechaActual=date('d M Y H:i:s');
                                $fechaFinal=new  \DateTime($fechaActual);
                                $fechaNacimientoPaciente = $remision->getIdPaciente()->getFechaNacimiento();
                                $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                                $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';
                                $correoRemisionExterna= $this->renderView('remision/correoRemisionExterna.html.twig', array('remision' => $remision,
                                          'username' => '',
                                          'password' => '',
                                          'edad'=> $edad,
                                        ));
                                $dqlUsuario ="SELECT usuario FROM App:Usuario usuario WHERE usuario.email =:email";
                                $queryUsuario = $entityManager->createQuery($dqlUsuario)->setParameter('email',$remision->getEmail());
                                $usuarios= $queryUsuario->getResult();
                                foreach ($usuarios as $usuarioSeleccionado) {
                                    $sucursalUsuario=$usuarioSeleccionado->getIdSucursal()->getNombre();   
                                    $idUsuario=$usuarioSeleccionado->getIdUsuario();    
                                }
                                $dqlModificarUsuario="UPDATE App:Usuario usuario SET usuario.estado =:estado WHERE usuario.idUsuario=:id";
                                $queryModificarUsuario=$entityManager->createQuery($dqlModificarUsuario)->setParameter('estado',true)->setParameter('id',$idUsuario);
                                $modificarUsuario=$queryModificarUsuario->execute();
                                $para = $remision->getEmail();
                                $clinicaNombre=$sucursalUsuario;
                                
                                $message = (new \Swift_Message())
                                    ->setSubject('Remisión N° ' .$remision->getIdRemision().' '.$clinicaNombre)
                                    ->setFrom('anamileidy.rodriguez@upt.edu.co')
                                    ->setTo($para)  
                                    ->setBody($correoRemisionExterna, 'text/html');                
                                try {
                                    $mailer->send($message);  
                                } catch (\Swift_TransportException  $e) {
                                    
                                     $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                                }
                                $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!'); 
                                return $this->redirectToRoute('remision_new',['idPaciente'=>$paciente->getIdPAciente()]);
                            }else{
                                $usuario= new Usuario();
                                $dqlClinica ="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                                $queryClinica = $entityManager->createQuery($dqlClinica);
                                $clinicas= $queryClinica->getResult();
                                foreach ($clinicas as $clinica) {
                                    $usuario->setIdSucursal($clinica);       
                                }
                                $dqlPerfil = "SELECT perfil FROM App:Perfil perfil where perfil.idPerfil= :idPerfil";
                                $queryPerfil = $entityManager->createQuery($dqlPerfil)->setParameter('idPerfil',2);
                                $perfiles = $queryPerfil->getResult(); 
                                foreach ($perfiles as $perfilSelecciado ) {   
                                    $usuario->setIdPerfil($perfilSelecciado);
                                }

                                $dqlTipoDc = "SELECT tipoDoc FROM App:TipoDocumento tipoDoc where tipoDoc.id= :id";
                                $queryTipoDoc = $entityManager->createQuery($dqlTipoDc)->setParameter('id',1);
                                $tipoDocumentos = $queryTipoDoc->getResult(); 
                                foreach ($tipoDocumentos as $tipoDocSeleccionado ) {   
                                    $usuario->setTipoentificacion($tipoDocSeleccionado);
                                }

                                $dqlAgendaUsuario = "SELECT agenda FROM App:AgendaUsuario agenda where agenda.idAgendaUsuario= :id";
                                $queryAgendaUsuario = $entityManager->createQuery($dqlAgendaUsuario)->setParameter('id',1);
                                $agendaUsuarios = $queryAgendaUsuario->getResult(); 
                                foreach ($agendaUsuarios as $agendaUsuarioSeleccionado ) {   
                                   $usuario->setIdAgendaUsuario($agendaUsuarioSeleccionado);
                                }
                                //Trabajo de username con email
                                $divicion=$remision->getEmail();
                                $user = explode("@", $divicion);

                                $usuario->setNombres($user[0]);
                                $usuario->setApellidos($user[0]);
                                $usuario->setNumeroIdentificacion($user[0]);
                                $fechaActual=date('d M Y H:i:s');
                                $fechaFinal=new  \DateTime($fechaActual);
                                $usuario->setFechaNacimiento($fechaFinal);
                                $usuario->setEmail($remision->getEmail());
                                
                                $usuario->setUsername($user[0]);
                                /*Trato Contraseña genracin y encriptación*/
                                $contraseña=$permisoPerfil->generarCodigo(8);
                                //$encoder= new UserPasswordEncoderInterface();
                                $password= $encoder->encodePassword($usuario, $contraseña);
                                $usuario->setPassword($password);
                                $usuario->setGenero('NO ESPECIFICA');
                                $usuario->setColorAgenda('#FFFFFF');
                                $usuario->setDireccion('calle 0 N° 0-0');
                                $usuario->setMovil(0000000000);
                                $usuario->setEstado(true);                                         
                                $usuario->setIntento(0);
                                $usuario->setTipoUsuario('EXTERNO');
                                $entityManager->persist($usuario);
                                $entityManager->flush();
                                $username=$usuario->getUsername();
                                $fechaNacimientoPaciente = $remision->getIdPaciente()->getFechaNacimiento();
                                $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                                $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';
                                $correoRemisionExterna= $this->renderView('remision/correoRemisionExterna.html.twig', array('remision' => $remision,
                                          'username' => $usuario->getUsername(),
                                          'password' => $contraseña,
                                          'edad'=> $edad,
                                        ));
                                $para = $usuario->getEmail();
                                $clinicaNombre=$usuario->getIdSucursal()->getNombre();
                                
                                $message = (new \Swift_Message())
                                    ->setSubject('Remisión N° '.$remision->getIdRemision().' '.$clinicaNombre)
                                    ->setFrom('anamileidy.rodriguez@upt.edu.co')
                                    ->setTo($para)  
                                    ->setBody($correoRemisionExterna, 'text/html');                
                                try {
                                    $mailer->send($message);  
                                } catch (\Swift_TransportException  $e) {
                                    
                                     $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                                }
                                $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');   
                                return $this->redirectToRoute('remision_new',['idPaciente'=>$paciente->getIdPaciente()]); 
                            }
                        }
                    }else if($remision->getEmail() == '' and $remision->getIdUsuarioRemitido() != ''){ 
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($remision);
                        $entityManager->flush();
                        $fechaActual=date('d M Y H:i:s');
                        $fechaFinal=new  \DateTime($fechaActual);
                        $fechaNacimientoPaciente = $remision->getIdPaciente()->getFechaNacimiento();
                        $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                        $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';
                        $correoRemisionExterna= $this->renderView('remision/correoRemisionExterna.html.twig', array('remision' => $remision,
                            'username' => '',
                            'password' => '',
                            'edad'=> $edad,
                        ));
                        $para = $remision->getIdUsuarioRemitido()->getEmail();
                        if($this->get('security.token_storage')->getToken()->getUser()->getIdSucursal()->getIdSedeClinica() != ''){
                            $clinicaNombre=$this->get('security.token_storage')->getToken()->getUser()->getIdSucursal()->getIdSedeClinica()->getNombre();
                        }else{
                            $clinicaNombre=$this->get('security.token_storage')->getToken()->getUser()->getIdSucursal()->getNombre();
                        }
                        
                        $message = (new \Swift_Message())
                            ->setSubject('Remisión N° '.$remision->getIdRemision().' '.$clinicaNombre)
                            ->setFrom('anamileidy.rodriguez@upt.edu.co')
                            ->setTo($para)  
                            ->setBody($correoRemisionExterna, 'text/html');                
                        try {
                            $mailer->send($message);  
                        } catch (\Swift_TransportException  $e) {
                            
                             $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                        }
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');   
                        return $this->redirectToRoute('remision_new',['idPaciente'=>$paciente->getIdPaciente()]); 
                    }else{
                        $this->addFlash('error', 'No es posible realizar los dos tipos de remisión para el mismo usuario.');   
                    }                   
                }

                return $this->render('remision/new.html.twig', [
                    'remision' => $remision,
                    'form' => $form->createView(),
                    'remisiones'=>$remisiones,
                    'clinicas'=>$clinicas,
                   // 'pacientes'=>$pacientes,
                    'edad'=> $edad,
                    'paciente'=> $paciente
                ]);
                
                //$idClinica=$request->request->get('idClinica');
               
               //     $clinica->getEstadoSede($clinica->setEstadoSede(true));   
                 
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }     
       
    }

    /**
     * Metodo utilizado para la visualización de una remisión.
     *
     * @Route("/{idRemision}", name="remision_show", methods={"GET","POST"})
     */
    public function show(Remision $remision,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'remisiones');  
                if (sizeof($permisos) > 0) {
                $fechaActual=date('d M Y H:i:s');
                $fechaFinal=new  \DateTime($fechaActual);
                $fechaNacimientoPaciente = $remision->getIdPaciente()->getFechaNacimiento();
                $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';
                return $this->render('remision/modalShow.html.twig',[
                    'remision' => $remision,
                    'username' => '',
                    'password' => '',
                    'edad'=> $edad,
                ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }   
    }

    /**
     * Metodo utilizado para la generar pdf que permita la impresión de un documento donde se visualice la remisión.
     *
     * @Route("/{idRemision}/imprimir", name="remision_imprimir", methods={"GET","POST"})
     */
    public function imprimir(Request $request, Remision $remision,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'remisiones');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $entityManager = $this->getDoctrine()->getManager(); 
                $fechaActual=date('d M Y H:i:s');
                $fechaFinal=new  \DateTime($fechaActual);
                $fechaNacimientoPaciente = $remision->getIdPaciente()->getFechaNacimiento();
                $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';
                $filename='Remisión_N°'.$remision->getIdRemision().'_Paciente_'.$remision->getIdPaciente().'.pdf';
                $html= $this->renderView('remision/prueba.html.twig', array('remision' => $remision,
                    'username' => '',
                    'password' => '',
                    'edad'=> $edad,
                ));
                
                $response = new Response ($this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                  /*  array(
                    'print-media-type' => true,
                    'encoding' => 'UTF-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Formulario',
                    // sirve para llamar estilos bootstrap
                    //'user-style-sheet'=> 'light/dist/assets/css/bootstrap.min.css',            
                    'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                    )),
                    200,
                    array('Content-Type' => 'aplicattion/pdf',
                          'Content-Disposition' => 'inline; filename="'.$filename.'"'
                    ,
                    )
                );*/
                [
                    'encoding' => 'UTF-8',
                    'images' => false,
                    'enable-javascript' => true,
                    'print-media-type' => true,
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'page-size' => 'A4',
                    'viewport-size' => '1280x1024',
                    'margin-left' => '10mm',
                    'margin-right' => '10mm',
                    'margin-top' => '30mm',
                    'margin-bottom' => '25mm',
                    'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
                    //'background'     => 'light/dist/assets/images/users/user-1.jpg',
                    //--javascript-delay 3000
                ]),
                200,
                     [
                     'Content-Type' => 'application/pdf',
                     'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
                     ]
                 );
                

                 /*$response= new Response ($this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                             
                    'ignore-load-errors'           => null, // old v0.9
                    'lowquality'                   => false,
                    'collate'                      => null,
                    'no-collate'                   => null,
                    'cookie-jar'                   => null,
                    'copies'                       => null,
                    'dpi'                          => null,
                    'extended-help'                => null,
                    'grayscale'                    => false,
                    'help'                         => null,
                    'htmldoc'                      => null,
                    'image-dpi'                    => null,
                    'image-quality'                => null,
                    'manpage'                      => null,
                    'margin-bottom'                => 0,
                    'margin-left'                  => 0,
                    'margin-right'                 => 0,
                    'margin-top'                   => 0,
                    'orientation'                  => null,
                    'output-format'                => null,
                    'page-height'                  => null,
                    'page-size'                    => "A4",
                    'page-width'                   => null,
                    'no-pdf-compression'           => null,
                    'quiet'                        => null,
                    'read-args-from-stdin'         => null,
                    'title'                        => null,
                    'use-xserver'                  => null,
                    'version'                      => null,
                    'dump-default-toc-xsl'         => null,
                    'dump-outline'                 => null,
                    'outline'                      => null,
                    'no-outline'                   => null,
                    'outline-depth'                => null,
                    'allow'                        => null,
                    'background'                   => null,
                    'no-background'                => null,
                    'checkbox-checked-svg'         => null,
                    'checkbox-svg'                 => null,
                    'cookie'                       => null,
                    'custom-header'                => null,
                    'custom-header-propagation'    => null,
                    'no-custom-header-propagation' => null,
                    'debug-javascript'             => null,
                    'no-debug-javascript'          => null,
                    'default-header'               => null,
                    'encoding'                     => null,
                    'disable-external-links'       => null,
                    'enable-external-links'        => null,
                    'disable-forms'                => null,
                    'enable-forms'                 => null,
                    'images'                       => true,
                    'no-images'                    => null,
                    'disable-internal-links'       => null,
                    'enable-internal-links'        => null,
                    'disable-javascript'           => null,
                    'enable-javascript'            => null,
                    'javascript-delay'             => null,
                    'load-error-handling'          => null,
                    'disable-local-file-access'    => null,
                    'enable-local-file-access'     => null,
                    'minimum-font-size'            => null,
                    'exclude-from-outline'         => null,
                    'include-in-outline'           => null,
                    'page-offset'                  => null,
                    'password'                     => null,
                    'disable-plugins'              => null,
                    'enable-plugins'               => null,
                    'post'                         => null,
                    'post-file'                    => null,
                    'print-media-type'             => null,
                    'no-print-media-type'          => null,
                    'proxy'                        => null,
                    'radiobutton-checked-svg'      => null,
                    'radiobutton-svg'              => null,
                    'run-script'                   => null,
                    'disable-smart-shrinking'      => true,
                    'enable-smart-shrinking'       => null,
                    'stop-slow-scripts'            => null,
                    'no-stop-slow-scripts'         => null,
                    'disable-toc-back-links'       => null,
                    'enable-toc-back-links'        => null,
                    'user-style-sheet'             => null,
                    'username'                     => null,
                    'window-status'                => null,
                    'zoom'                         => 1.04,
                    'footer-center'                => null,
                    'footer-font-name'             => null,
                    'footer-font-size'             => null,
                    'footer-html'                  => "<h1>aqui</h1>",
                    'footer-left'                  => null,
                    'footer-line'                  => null,
                    'no-footer-line'               => null ,
                    'footer-right'                 => null,
                    'footer-spacing'               => null,
                    'header-center'                => null,
                    'header-font-name'             => null,
                    'header-font-size'             => null,
                    'header-html'                  => null,
                    'header-left'                  => null,
                    'header-line'                  => null,
                    'no-header-line'               => null,
                    'header-right'                 => null,
                    'header-spacing'               => null,
                    'replace'                      => null,
                    'disable-dotted-lines'         => null,
                    'cover'                        => null,
                    'toc'                          => null,
                    'toc-depth'                    => null,
                    'toc-font-name'                => null,
                    'toc-l1-font-size'             => null,
                    'toc-header-text'              => null,
                    'toc-header-font-name'         => null,
                    'toc-header-font-size'         => null,
                    'toc-level-indentation'        => null,
                    'disable-toc-links'            => null,
                    'toc-text-size-shrink'         => null,
                    'xsl-style-sheet'              => null,
                    'redirect-delay'               => null, // old v0.9
                    )),
                    200,
                         [
                         'Content-Type' => 'application/pdf',
                         'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
                         ]
                     );*/
                    
               
                
                
                
                return $response;

                /*return new JpegResponse(
                    $this->get('knp_snappy.image')->getOutputFromHtml($html),
                    'image.jpg'
                );*/

                /* ($this->get('knp_snappy.pdf')->generateFromHtml(
                    $this->renderView(
                        'remision/impresionRemision.html.twig', array('remision' => $remision,
                                'username' => '',
                                'password' => '',
                                'edad'=> $edad,
                            )
                    ),
                    '/path/to/the/file.pdf'));*/
             }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }
    }         


    /**
     * Metodo utilizado para el envio de correos electronicos al paciente al que se le genro la solicitud.
     *
     * @Route("/{idRemision}/envioCorreo", name="remision_correo", methods={"GET","POST"})
     */
    public function correo(Request $request, Remision $remision,PerfilGenerator $permisoPerfil,\Swift_Mailer $mailer): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'remisiones');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $entityManager = $this->getDoctrine()->getManager(); 
                $fechaActual=date('d M Y H:i:s');
                $fechaFinal=new  \DateTime($fechaActual);
                $fechaNacimientoPaciente = $remision->getIdPaciente()->getFechaNacimiento();
                $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';
                $filename='Remisión_N°'.$remision->getIdRemision().'_Paciente_'.$remision->getIdPaciente().'.pdf';
                $html= $this->renderView('remision/impresionRemision.html.twig', array('remision' => $remision,
                    'username' => '',
                    'password' => '',
                    'edad'=> $edad,
                ));
                
                $archivoAdjunto = $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                [
                    /*'print-media-type' => true,
                    'encoding' => 'UTF-8',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    // sirve para llamar estilos bootstrap          
                    //'user-style-sheet'=> 'css/verticalEMSOLIS.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                    'images' => true,
                    'enable-javascript' => true,
                    'page-size' => 'A4',
                    'viewport-size' => '1280x1024',
                    'margin-left' => '10mm',
                    'margin-right' => '10mm',
                    'margin-top' => '30mm',
                    'margin-bottom' => '25mm',*/
                    'encoding' => 'UTF-8',
                    'images' => false,
                    'enable-javascript' => true,
                    'print-media-type' => true,
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'page-size' => 'A4',
                    'viewport-size' => '1280x1024',
                    'margin-left' => '10mm',
                    'margin-right' => '10mm',
                    'margin-top' => '30mm',
                    'margin-bottom' => '25mm',
                    'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
                ]);

                $attach = new \Swift_Attachment(
                    $archivoAdjunto, 
                    $filename, 
                    'application/pdf');


                $para = $remision->getIdPaciente()->getEmail();
                $message = (new \Swift_Message())
                    ->setSubject('Remisión N°'.$remision->getIdRemision().' '.$remision->getIdPaciente())
                    ->setFrom('anamileidy.rodriguez@upt.edu.co')
                    ->setTo($para)  
                    ->setBody('<html>
                          <head>
                            <style>
                              p{
                                 font-family: "Cerebri Sans,sans-serif";
                                 font-size:15px;
                                 font-style:normal;
                                 color: #6c757d;
                              }
                              b{
                                color:black;
                                font-size:15px;
                                font-style:normal;
                              }
                              img{
                                position: absolute; 
                                align-self: center; 
                                width: 3.5in; 
                                height:1.5in;
                              }
                            </style>
                          </head>
                            <body>                                
                              <p>Buen día</p><br>
                              <p> Señor@ '.$remision->getIdPaciente().'</p>
                              <p> El sistema a generado una nueva remisión, por favor verifique el archivo adjunto.</p>

                               <p>Si presenta algun inconveniente comuniqiese con el administrador de la aplicación</p>
                              <p> Cordialmente,</p><br>       

                              <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                                                  
                              <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br>                                           
                              </body>
                            </html>', 'text/html')
                            ->attach($attach);                
                try {
                    $mailer->send($message);  
                } catch (\Swift_TransportException  $e) {
                    
                     $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                }
                $this->addFlash('mensaje', 'Se ha realizado el envio de la remisión de forma correcta!');   
                return $this->redirectToRoute('remision_new',['idPaciente'=>$remision->getIdPaciente()->getIdPaciente()]); 

                return $response;
             }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }
    }            

}
