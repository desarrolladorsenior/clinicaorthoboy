<?php

namespace App\Controller;

use App\Entity\AnexoPaciente;
use App\Form\AnexoPacienteType;
use App\Service\PerfilGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de externo utilizado para el manejo del de los antecedentes que crean usuarios con tipo externo es decir usuarios que no cuentan con ninguna 
 * vinculación laboral, si no unicamente dan servicio a los pacientes que ellos asumen los gastos, igualmente este controlador permite que se suban archivos sobre el 
 * antedente que se generara.
 *
 * @Route("/externo")
 */
class ExternoController extends AbstractController
{
    

     /**
     * Metodo utilizado para registrar un anexo a un remision de un paciente a traves de un usuario externo. Donde se verifican los datos que se 
     * requieren para lograr realizar el registro de este, igualmente se valida que la fecha de la remision no halla expedido y si es el caso se
     * envia un correo al paciente indicando el caso.
     *
     * @Route("/newExterno", name="externo_newAnexo", methods={"GET","POST"})
     */
    public function newExterno(Request $request,PerfilGenerator $permisoPerfil, \Swift_Mailer $mailer): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'anexos');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $externo=$this->get('security.token_storage')->getToken()->getUser()->getTipoUsuario();
                if ($externo == 'EXTERNO' ) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $idUsuario=$this->get('security.token_storage')->getToken()->getUser()->getIdUsuario();
                  $anexos='';
                  $anexoPaciente= new AnexoPaciente();
                   $idPaciente= '';
                  if (empty($request->get('dato'))) {
                    $this->addFlash('error', 'Dirijase a su correo electrónico y de clic en el link asignado para redirigir al área del paciente');
                  }else{
                    $idPaciente=$request->get('dato');
                    $dqlPaciente = "SELECT paciente FROM App:Paciente paciente where paciente.idPaciente= :id and paciente.estado = :estadoPaciente";
                    $queryPaciente = $entityManager->createQuery($dqlPaciente)->setParameter('id',$idPaciente)->setParameter('estadoPaciente',true);
                    $pacientes = $queryPaciente->getResult(); 
                    foreach ($pacientes as $pacienteSeleccionado) {
                      $anexoPaciente->setIdPaciente($pacienteSeleccionado);
                    }
                    $anexoPaciente->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                  }
                  $fechaV='';                 
                  if (!empty($request->get('dato2'))) {
                    $idRemision=$request->get('dato2');
                    $dqlRemision= "SELECT remision FROM App:Remision remision where remision.idRemision= :id";
                    $queryRemision = $entityManager->createQuery($dqlRemision)->setParameter('id',$idRemision);
                    $remisiones = $queryRemision->getResult(); 
                    foreach ($remisiones as $remisionSeleccionado) {
                      $anexoPaciente->setIdRemision($remisionSeleccionado);
                      $fechaV=$remisionSeleccionado->getFechaRealizar();
                      $formFechaV=$fechaV->format('d M Y');
                    }

                    $fechaActual=date('Y-m-d');
                    $dqlAnexos = "SELECT anexo FROM App:AnexoPaciente anexo where anexo.idUsuarioIngresa= :id and anexo.fechaApertura >= :fechaApertura and anexo.idRemision =:remision";
                    $queryAnexo = $entityManager->createQuery($dqlAnexos)->setParameter('id',$idUsuario)->setParameter('fechaApertura',$fechaActual)->setParameter('remision',$idRemision);
                    $anexos = $queryAnexo->getResult(); 
                  }  

                  $form = $this->createForm(AnexoPacienteType::class, $anexoPaciente);
                  $form->handleRequest($request);
                  
                  //$file =  $form->get('archivo')->getData();
                  //var_dump($fechaV);
                  if ($form->isSubmitted() && $form->isValid()) { 
                    $fechaActual=date('d M Y');
                    $formFechaActual = strtotime($fechaActual);
                    $formFechaVencimientof=strtotime($formFechaV);
                    if ($formFechaActual <= $formFechaVencimientof) {
                      $file =  $form->get('archivo')->getData();
                      $anexoPaciente->uploadUno($file);     
                      $entityManager->persist($anexoPaciente);
                      $entityManager->flush();
                      $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                      return $this->redirectToRoute('externo_newAnexo', [
                        'dato' => $idPaciente,
                        'dato2' => $idRemision,
                        
                      ]);
                    }else{
                      $clinica=$this->get('security.token_storage')->getToken()->getUser()->getIdSucursal()->getNombre();
                      $message = (new \Swift_Message())
                      ->setSubject('Notificación remisión vencida.')
                      ->setFrom('anamileidy.rodriguez@upt.edu.co')
                      ->setTo($anexoPaciente->getIdPaciente()->getEmail())  
                      ->setBody('<html>
                          <head>
                            <style>
                              p{
                                 font-family: "Cerebri Sans,sans-serif";
                                 font-size:15px;
                                 font-style:normal;
                                 color: #6c757d;
                              }
                              b{
                                color:black;
                                font-size:15px;
                                font-style:normal;
                              }
                              img{
                                position: absolute; 
                                align-self: center; 
                                width: 3.5in; 
                                height:1.5in;
                              }
                            </style>
                          </head>
                            <body>                                
                              <p>Buen día</p><br>
                              <p>'.$anexoPaciente->getIdPaciente()->getNombres().' '.$anexoPaciente->getIdPaciente()->getPrimerApellido().' '.$anexoPaciente->getIdPaciente()->getSegundoApellido().' el sistema de <b>'.$clinica.'</b> detecto que su remisión número <b>'.$anexoPaciente->getIdRemision().'</b> a vencido. Se solicita se acerque a la sede mas cercana.</p>
                              <p> Cordialmente,</p><br>       
                              <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                              <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br> 
                              </body>
                            </html>', 'text/html') ;                
                          try {
                              $mailer->send($message);  
                          } catch (\Swift_TransportException  $e) {
                              
                               $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                          }
                      $this->addFlash('error', 'La fecha de la remisión se ha vencido, ya no es posible gestionar anexos al paciente.');
                    }
                  }
                  return $this->render('externo/newAnexoExterno.html.twig', [
                      'anexoPacienteActual' => $anexoPaciente,
                      'anexos' => $anexos,
                      'form' => $form->createView(),
                      'dato' => $idPaciente,
                      'fechaV'=>$fechaV,
                  ]);
                }else{
                  $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                  return $this->redirectToRoute('panel_principal');
                } 
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }     
        
    }


     /**
     * Metodo utilizado para la finalización de los anexos necesarios para la remisión expuesta al usuario lo que indica que el usuario quedaria 
     * desctivado y enviaria un correo indicando que la remisión ya fue solucionada.
     *
     * @Route("/finalizar", name="externo_finalizar", methods={"GET","POST"})
     */
    public function finalizar(Request $request,PerfilGenerator $permisoPerfil, \Swift_Mailer $mailer): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'anexos');  
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {
                $externo=$this->get('security.token_storage')->getToken()->getUser()->getTipoUsuario();
                if ($externo == 'EXTERNO' ) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $idUsuario=$this->get('security.token_storage')->getToken()->getUser()->getIdUsuario();
                  $dqlModificarUsuario="UPDATE App:Usuario usuario SET usuario.estado =:estado WHERE usuario.idUsuario=:id";
                  $queryModificarUsuario=$entityManager->createQuery($dqlModificarUsuario)->setParameter('estado',false)->setParameter('id',$idUsuario);
                  $modificarUsuario=$queryModificarUsuario->execute();
                  $anexo= $request->get('anexo');
                  $dqlAnexos= "SELECT anexo FROM App:AnexoPaciente anexo where anexo.idAnexoPaciente= :id";
                  $queryAnexos = $entityManager->createQuery($dqlAnexos)->setParameter('id',$anexo);
                  $anexos = $queryAnexos->getResult(); 
                  foreach ($anexos as $anexoSeleccionado) {
                    $nombreCompleto=$anexoSeleccionado->getIdPaciente()->getNombres().' '.$anexoSeleccionado->getIdPaciente()->getPrimerApellido().' '.$anexoSeleccionado->getIdPaciente()->getSegundoApellido();
                    $email=$anexoSeleccionado->getIdPaciente()->getEmail();
                    $remision=$anexoSeleccionado->getIdRemision();
                  }
                  $clinica=$this->get('security.token_storage')->getToken()->getUser()->getIdSucursal()->getNombre();
                  $message = (new \Swift_Message())
                  ->setSubject('Notificación remisión finalizada.')
                  ->setFrom('anamileidy.rodriguez@upt.edu.co')
                  ->setTo($email)  
                  ->setBody('<html>
                      <head>
                        <style>
                          p{
                             font-family: "Cerebri Sans,sans-serif";
                             font-size:15px;
                             font-style:normal;
                             color: #6c757d;
                          }
                          b{
                            color:black;
                            font-size:15px;
                            font-style:normal;
                          }
                          img{
                            position: absolute; 
                            align-self: center; 
                            width: 3.5in; 
                            height:1.5in;
                          }
                        </style>
                      </head>
                        <body>                                
                          <p>Buen día</p><br>
                          <p>'.$nombreCompleto.' el sistema de <b>'.$clinica.'</b> detecto  que se ha  dado respuesta a su remisión número <br>'.$remision.'</b>, esta información ya se encuentra en la plataforma de la clinica para continuar con su tratamiento se solicita se acerque a la sede mas cercana.</p>
                          <p> Cordialmente,</p><br>       
                          <img src="https://i.imgur.com/mXwYAiH.png" alt="Image"/><br>
                          <p style="font-size:11px;">El contenido de este mensaje y de los archivos adjuntos están dirigidos exclusivamente a sus destinatarios y puede contener información privilegiada o confidencial. Si usted no es el destinatario real, por favor informe de ello al remitente y elimine el mensaje de inmediato, de tal manera que no pueda acceder a él de nuevo. Está prohibida su retención, grabación, utilización o divulgación con cualquier propósito.</p><br> 
                          </body>
                        </html>', 'text/html') ;                
                      try {
                          $mailer->send($message);  
                      } catch (\Swift_TransportException  $e) {
                          
                           $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                      }
                  return $this->redirectToRoute('usuario_logout');
                }else{
                  $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                  return $this->redirectToRoute('panel_principal');
                } 
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }     
        
    }
    /**
     * Metodo utilizado para la eliminación de los anexos generados, además de eliminación de un archivo.
     *
     * @Route("/{idAnexoPaciente}/delete", name="externo_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnexoPaciente $anexoPaciente,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
        return $this->redirectToRoute('usuario_logout'); 
      } else{
        // La variable $_SESSION['usuario'] es un ejemplo. 
        //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
        if(isset($_SESSION['time'])){ 
          $tiempo = $_SESSION['time']; 
        }else{ 
          $tiempo = strtotime(date("Y-m-d H:i:s"));
        } 
        $inactividad =1800;   //Exprecion en segundos. 
        $actual =  strtotime(date("Y-m-d H:i:s")); 
        $tiempoTranscurrido=($actual-$tiempo);
        if(  $tiempoTranscurrido >= $inactividad){ 
          $session->invalidate();
          return $this->redirectToRoute('usuario_logout');
         // En caso que este sea mayor del tiempo seteado lo deslogea. 
        }else{ 
          $_SESSION['time'] =$actual; 
          $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
          $permisos=$permisoPerfil->verificacionPermisos($perfil,'anexos');  
              //var_dump('expression',sizeof( $permisos));
          if (sizeof($permisos) > 0) {
            $externo=$this->get('security.token_storage')->getToken()->getUser()->getTipoUsuario();
            if ($externo == 'EXTERNO' ) {
                try{

                    if ($this->isCsrfTokenValid('delete'.$anexoPaciente->getIdAnexoPaciente(), $request->request->get('_token'))) {
                      if ($anexoPaciente->getArchivo() != '') {
                        $archivoEliminar='/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/anexoPaciente/'.$anexoPaciente->getArchivo();
                      }else{
                          $archivoEliminar='';
                      }
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($anexoPaciente);
                        $entityManager->flush();
                        if ($archivoEliminar != '') {
                          unlink($archivoEliminar);
                        }
                        $successMessage= 'Se ha eliminado de forma correcta!';
                        $this->addFlash('mensaje',$successMessage);
                        return $this->redirectToRoute('externo_newAnexo', [
                            'dato' => $request->get('dato'),
                            'dato2' => $request->get('dato2'),
                          ]);
                    }

                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('externo_newAnexo', [
                              'dato' => $request->get('dato'),
                              'dato2' => $request->get('dato2'),
                            ]);
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('externo_newAnexo', [
                  'dato' => $request->get('dato'),
                  'dato2' => $request->get('dato2'),
                ]);
            }else{
              $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
              return $this->redirectToRoute('panel_principal');
            }     
          }else{
            $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
            return $this->redirectToRoute('panel_principal');
          }    
        }
      } 
    }  
}
