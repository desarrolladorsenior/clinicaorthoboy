<?php

namespace App\Controller;

use App\Entity\Imagen;
use App\Entity\Paciente;
use App\Form\ImagenType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de imagen utilizado para el manejo de las mismas para un paciente.
 *
 * @Route("/imagen")
 */
class ImagenController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de una imagen y registro del mismo.
     *
     * @Route("/{idPaciente}", name="imagen_index", methods={"GET","POST"})
     */
    public function index(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'imagenes');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlImagenesPaciente ="SELECT img FROM App:Imagen img WHERE img.idPaciente = :idPaciente ORDER BY img.idImagen ASC";  
                    $queryImagenesPaciente = $entityManager->createQuery($dqlImagenesPaciente)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $imagenesPacientes= $queryImagenesPaciente->getResult();

                    $imagen = new Imagen();
                    $imagen->setIdPaciente($paciente);
                    $imagen->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(ImagenType::class, $imagen);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $file =  $form->get('nombre')->getData();
                        $imagen->uploadUno($file);
                        $entityManager->persist($imagen);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('imagen_index',['idPaciente'=>$paciente->getIdPaciente()]);
                    }
                    return $this->render('imagen/index.html.twig', [
                        'imagenesPacientes' => $imagenesPacientes,
                        'paciente'=>$paciente,
                        'imagen' => $imagen,
                        'form' => $form->createView(),
                    ]);
                    /*$imagens = $this->getDoctrine()
                        ->getRepository(Imagen::class)
                        ->findAll();

                    return $this->render('imagen/index.html.twig', [
                        'imagens' => $imagens,
                        'paciente'=> $paciente
                    ]);*/
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    

    /**
     * Metodo utilizado para la edición de una imagen.
     *
     * @Route("/{idImagen}/edit", name="imagen_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Imagen $imagen): Response
    {
        $form = $this->createForm(ImagenType::class, $imagen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('imagen_index', [
                'idImagen' => $imagen->getIdImagen(),
            ]);
        }

        return $this->render('imagen/edit.html.twig', [
            'imagen' => $imagen,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Metodo utilizado para la eliminación de una imagen.
     *
     * @Route("/{idImagen}", name="imagen_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Imagen $imagen,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'imagenes');  
                if (sizeof($permisos) > 0) { 
                    $idPaciente=$imagen->getIdPaciente()->getIdPaciente();
                    try{
                        if ($this->isCsrfTokenValid('delete'.$imagen->getIdImagen(), $request->request->get('_token'))) {
                            $archivoEliminar='/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/imagen/'.$imagen->getNombre();
                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->remove($imagen);
                            $entityManager->flush();
                            unlink($archivoEliminar);
                            $successMessage= 'Se ha eliminado de forma correcta!';
                            $this->addFlash('mensaje',$successMessage);
                            return $this->redirectToRoute('imagen_index',['idPaciente'=>$idPaciente]);
                        }

                    }catch (\Doctrine\DBAL\DBALException $e) {
                        if ($e->getCode() == 0){
                            if ($e->getPrevious()->getCode() == 23503){
                                $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                                $this->addFlash('error',$successMessage1);
                                return $this->redirectToRoute('imagen_index',['idPaciente'=>$idPaciente]);
                            }else{
                                throw $e;
                            }
                        }else{
                            throw $e;
                        }
                    } 
                    /*if ($this->isCsrfTokenValid('delete'.$imagen->getIdImagen(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($imagen);
                        $entityManager->flush();
                    }*/

                    return $this->redirectToRoute('imagen_index',['idPaciente',$idPaciente]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }    
    }
}
