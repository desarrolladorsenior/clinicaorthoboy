<?php

namespace App\Controller;

use App\Entity\Contrato;
use App\Entity\TipoContrato;
use App\Entity\Paciente;
use App\Form\ContratoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de contrato utilizado para el manejo de la confirmación o aprobación de los archivos de la clinica que se le vallan a 
 * efectuar a un paciente el cual busca, a traves de una firma capturada en una tableta y/o pad, huellero se apruebe el archivo
 * a realizar.
 *
 * @Route("/contrato")
 */
class ContratoController extends Controller
{
    /**
     * Metodo utilizado para el ingreso de huella al consentimiento. Además o tiene  los parametros deseguridad , ni de cambio de tiempo de sesionamiento a causa que 
     * no cuenta con sesionaniento, ni cuenta con autenticación previa.
     *
     * @Route("/huella", name="contrato_huella", methods={"GET","POST"})
     */
    public function huellaPacienteContrato(Request $request,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
          return $this->redirectToRoute('usuario_logout'); 
      } else{
          // La variable $_SESSION['usuario'] es un ejemplo. 
          //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
          if(isset($_SESSION['time'])){ 
            $tiempo = $_SESSION['time']; 
          }else{ 
            $tiempo = strtotime(date("Y-m-d H:i:s"));
          } 
          $inactividad =1800;   //Exprecion en segundos. 
          $actual =  strtotime(date("Y-m-d H:i:s")); 
          $tiempoTranscurrido=($actual-$tiempo);
          if(  $tiempoTranscurrido >= $inactividad){ 
            $session->invalidate();
            return $this->redirectToRoute('usuario_logout');
           // En caso que este sea mayor del tiempo seteado lo deslogea. 
          }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'contratos');  
              if (sizeof($permisos) > 0) { 
                //$idUsuario=$request->get('inf');
                $huellaActual=$request->get('huellaActual');
                $idContrato=$request->get('idContrato');
                //$idUsuarioForm=$request->request->get('idUsuario');
                $huellaForm=$request->request->get('huella');
                if (isset($huellaForm) && isset($idContrato)) {
                 // $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                  //return $this->redirectToRoute('usuario_index');
                  $entityManager=$this->getDoctrine()->getManager();        
                  $dqlModificarContrato="UPDATE App:Contrato contrato SET contrato.huellaPaciente= :huella WHERE contrato.idContrato= :idContrato";
                  $queryModificarContrato=$entityManager->createQuery($dqlModificarContrato)->setParameter('huella', $huellaForm)->setParameter('idContrato', $idContrato);
                  $modificarContrato=$queryModificarContrato->execute();
                  //echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
                  $this->addFlash('mensaje', 'Se ha realizado el registro de la huella correctamente!');
                  return $this->redirectToRoute('contrato_show',['idContrato'=>$idContrato]);
                }
                return $this->render('contrato/capturaHuella.html.twig', [
                    //'idUsuario' => $idUsuario,
                    'huellaActual'=>$huellaActual,
                    'dato'=>$idContrato
                ]);
          }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
          }    
        }
      }      
    }

    /**
     * Metodo utilizado para el ingreso de firma al consentimiento. Además o tiene  los parametros deseguridad , ni de cambio de tiempo de sesionamiento a causa que 
     * no cuenta con sesionaniento, ni cuenta con autenticación previa.
     *
     * @Route("/firma", name="contrato_firma", methods={"GET","POST"})
     */
    public function firmaPacienteContrato(Request $request): Response
    {
      //$idUsuario=$request->get('inf');
      $firmaActual=$request->get('firmaActual');

      $dato=$request->get('dato');
      $idContrato=$request->get('idContrato');
      //$idUsuarioForm=$request->request->get('idUsuario');
      $firmaForm=$request->request->get('firmaPaciente');
      if (isset($firmaForm) && isset($idContrato)) {
       // $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
        //return $this->redirectToRoute('usuario_index');
        $entityManager=$this->getDoctrine()->getManager();        
        $dqlModificarContrato="UPDATE App:Contrato contrato SET contrato.firmaPaciente= :firma WHERE contrato.idContrato= :idContrato";
        $queryModificarContrato=$entityManager->createQuery($dqlModificarContrato)->setParameter('firma', $firmaForm)->setParameter('idContrato', $idContrato);
        $modificarContrato=$queryModificarContrato->execute();
        echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
      }
      return $this->render('contrato/firmaPaciente.html.twig', [
          //'idUsuario' => $idUsuario,
          'firmaActual'=>$firmaActual,
          'dato'=>$dato
      ]);
    }


    /**
     * Metodo utilizado para la generación de un pdf, sin embargo se encuentra a la espera de los prototipos.
     *
     * @Route("/{idContrato}/pdf", name="contrato_pdf", methods={"GET","POST"})
     */
    public function pdf(Contrato $contrato,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'contratos');  
                if (sizeof($permisos) > 0) { 
                     $html =$this->renderView('contrato/pdfContrato.html.twig',
                    array(
                    'contrato' => $contrato
                    ));
                     
                    $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                    [   'encoding' => 'UTF-8',
                        'images' => false,
                        'enable-javascript' => true,
                        'print-media-type' => true,
                        'outline-depth' => 8,
                        'orientation' => 'Portrait',
                        'header-right'=>'Pag. [page] de [toPage]',
                        'page-size' => 'A4',
                        'viewport-size' => '1280x1024',
                        'margin-left' => '10mm',
                        'margin-right' => '10mm',
                        'margin-top' => '30mm',
                        'margin-bottom' => '25mm',
                        'user-style-sheet'=> 'light/dist/assets/css/bootstrap.min.css',
                        //'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
                        //'background'     => 'light/dist/assets/images/users/user-1.jpg',
                        //--javascript-delay 3000
                    ]);
                    $filename="Documento_N°_".$contrato->getIdContrato()."Paciente_".$contrato->getIdPaciente()."_fecha_". date("Y_m_d") .".pdf";
                    $response = new Response ($pdf,
                    200,
                    array('Content-Type' => 'aplicattion/pdf',
                          'Content-Disposition' => 'inline; filename="'.$filename.'"'
                    ,));
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

     /**
     * Metodo utilizado para obtener el contenido del tipo de contrato 'para la visualización en el registro de un contrato.
     * 
     * @Route("/{idTipoContrato}/contenido", name="contrato_contenido", methods={"GET","POST"})
    */
    public function contenidoTipoContrato(TipoContrato $tipoContrato,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'contratos');  
                if (sizeof($permisos) > 0) { 
                    $response = new JsonResponse(['contenido'=>ucwords(strtolower($tipoContrato->getContenido()))]);
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }


    /**
     * @Route("/{idPaciente}", name="contrato_index", methods={"GET","POST"})
     */
    public function index(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'contratos');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlContrato ="SELECT contrato FROM App:Contrato contrato WHERE contrato.idPaciente = :idPaciente ORDER BY contrato.idContrato DESC";  
                    $queryContrato = $entityManager->createQuery($dqlContrato)->setParameter('idPaciente',$paciente->getIdPaciente());
                    $contratos= $queryContrato->getResult();

                    $dqlClinica="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                    $queryClinica=$entityManager->createQuery($dqlClinica);
                    $clinicas=$queryClinica->getResult();

                    $contrato = new Contrato();
                    $contrato->setIdPaciente($paciente);
                    $contrato->setIdUsuario($this->get('security.token_storage')->getToken()->getUser());
                    $form = $this->createForm(ContratoType::class, $contrato);                   
                    $form->handleRequest($request);

                    $fechaActual=date('d M Y H:i:s');
                    $fechaFinal=new  \DateTime($fechaActual);
                    $fechaNacimientoPaciente = $paciente->getFechaNacimiento();
                    $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                    $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';

                    if ($form->isSubmitted() && $form->isValid()) {  
                        $contrato->setEstado(true);                     
                        $entityManager->persist($contrato);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('contrato_show',['idContrato'=>$contrato->getIdContrato()]);
                    }

                    return $this->render('contrato/index.html.twig', [
                        'contratos' => $contratos,
                        'paciente'=>$paciente,
                        'form' => $form->createView(),
                        'clinicas'=>$clinicas,
                        'edad'=>$edad,
                        'contrato' => $contrato,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * @Route("/{idContrato}/detalle", name="contrato_show", methods={"GET"})
     */
    public function show(Request $request,PerfilGenerator $permisoPerfil,Contrato $contrato): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'contratos');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();

                    $dqlClinica="SELECT clinica FROM App:Clinica clinica WHERE clinica.idSedeClinica is null";
                    $queryClinica=$entityManager->createQuery($dqlClinica);
                    $clinicas=$queryClinica->getResult();

                    $fechaActual=date('d M Y H:i:s');
                    $fechaFinal=new  \DateTime($fechaActual);
                    $fechaNacimientoPaciente = $contrato->getIdPaciente()->getFechaNacimiento();
                    $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                    $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';
                    return $this->render('contrato/show.html.twig', [
                        'contrato' => $contrato,
                        'clinicas' => $clinicas,
                        'edad'=> $edad
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * @Route("/{idContrato}/edit", name="contrato_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Contrato $contrato): Response
    {
        $form = $this->createForm(ContratoType::class, $contrato);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contrato_index', [
                'idContrato' => $contrato->getIdContrato(),
            ]);
        }

        return $this->render('contrato/edit.html.twig', [
            'contrato' => $contrato,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idContrato}", name="contrato_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Contrato $contrato): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contrato->getIdContrato(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contrato);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contrato_index');
    }
}
