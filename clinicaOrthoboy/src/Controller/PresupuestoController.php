<?php

namespace App\Controller;

use App\Entity\Presupuesto;
use App\Form\PresupuestoType;
use App\Entity\Paciente;
use App\Entity\PlanTratamiento;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

/**
 * Controlador de presupuestos utilizado para la generación de planes de tratamientos,presupuestoa dichos planes y ademas para genración 
 * de simulador de planes para los pacientes interesados.
 *
 * @Route("/plan")
 */
class PresupuestoController extends AbstractController
{
    /**
     * Metodo utilizado para cambiar un plan a estado en proceso, ya que este se aprobo para iniciar este con el paciente del mismo.
     * 
     * @Route("/cambioPlanEstado", name="presupuesto_cambioPlanProceso", methods={"GET","POST"})
    */
    public function cambioPlanTratamientoEstadoProceso(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'plan de tratamiento');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $idPlan =$request->get('idPlan');
                    $numeroCuotas =$request->get('numeroCuotas');
                    $planTratamiento = $this->getDoctrine()
                    ->getRepository('App:PlanTratamiento')
                    ->find($idPlan);
                    //Modificacion de estado de plan tratamiento a en proceso
                    $dqlModificarEstadoPlan="UPDATE App:PlanTratamiento plan SET plan.idEstadoGeneral= :estado, plan.numeroCuota= :numeroCuota WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                    $queryModificarEstadoPlan=$entityManager->createQuery($dqlModificarEstadoPlan)->setParameter('estado', 2)->setParameter('idPlanTratamiento', $idPlan)->setParameter('numeroCuota',$numeroCuotas);
                    $modificarEstadoPlan=$queryModificarEstadoPlan->execute();

                    //Modificacion de estado de presupuestos del plan a en proceso
                    $dqlModificarEstadoPresupuestoDePlan="UPDATE App:Presupuesto presupuesto SET presupuesto.idEstadoGeneral= :estado WHERE presupuesto.idPlanTratamiento= :idPlanTratamiento";
                    $queryModificarEstadoPresupuestoDePlan=$entityManager->createQuery($dqlModificarEstadoPresupuestoDePlan)->setParameter('estado', 2)->setParameter('idPlanTratamiento', $idPlan);
                    $modificarEstadoPresupuestoDePlan=$queryModificarEstadoPresupuestoDePlan->execute();
                    //var_dump($planTratamiento->getIdPaciente());
                    $this->addFlash('mensaje', 'Se ha confirmado exitosamente el plan de tratamiento!');
                    return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$planTratamiento->getIdPaciente()->getIdPaciente()]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para obtener el contenido del pieza dental para escoger en proximo pre4supuesto que evalua un tratamiento 
     * completo.
     * 
     * @Route("/datoPiezaDental", name="presupuesto_contenidopieza", methods={"GET","POST"})
    */
    public function contenidoPiezaDental(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'plan de tratamiento');  
                if (sizeof($permisos) > 0) { 
                    $response = new JsonResponse();
                    $entityManager = $this->getDoctrine()->getManager();
                    $dato =$request->get('dato');
                    if ($dato == 'pieza') {
                        $arrayPiezas=array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52);
                        $dqlPieza ="SELECT pieza FROM App:PiezaDental pieza WHERE pieza.idPiezaDental in (:idPiezas)";  
                        $queryPieza = $entityManager->createQuery($dqlPieza)->setParameter('idPiezas',$arrayPiezas);
                        $piezas= $queryPieza->getResult();
                        $arrayPieza=array();
                        foreach ($piezas as $seleccionPieza ) {
                            $id=$seleccionPieza->getIdPiezaDental();
                            $codigoPieza=$seleccionPieza->getCodigoPieza();
                            $docpieza = $id.";".$codigoPieza;
                            array_push($arrayPieza, $docpieza);
                        }
                    }else if ($dato == 'maxilar') {
                        $arrayPiezas=array(53,54,55,56,57,58);
                        $dqlPieza ="SELECT pieza FROM App:PiezaDental pieza WHERE pieza.idPiezaDental in (:idPiezas)";  
                        $queryPieza = $entityManager->createQuery($dqlPieza)->setParameter('idPiezas',$arrayPiezas);
                        $piezas= $queryPieza->getResult();
                        $arrayPieza=array();
                        foreach ($piezas as $seleccionPieza ) {
                            $id=$seleccionPieza->getIdPiezaDental();
                            $codigoPieza=$seleccionPieza->getCodigoPieza();
                            $docpieza = $id.";".$codigoPieza;
                            array_push($arrayPieza, $docpieza);
                        }
                    }
                    $response->setData($arrayPieza);
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la visualización de los antecedentes que pueden llegar a tener una persona, 
     * además del registro de un nuevo antecedente.

     * @Route("/{idPaciente}", name="presupuesto_index", methods={"GET","POST"})
     */
    public function index(Request $request,Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'plan de tratamiento');  
                if (sizeof($permisos) > 0) { 
                    $entityManager = $this->getDoctrine()->getManager();
                    $presupuesto = new Presupuesto();
                    $dqlTrataConvenio ="SELECT trataConvenio FROM App:TratamientoConvenio trataConvenio INNER JOIN App:Convenio convenio WITH convenio.idConvenio= trataConvenio.idConvenio INNER JOIN App:Eps eps WITH convenio.idEps= eps.idEps INNER JOIN App:Paciente paciente WITH paciente.idEps= eps.idEps WHERE paciente.idPaciente= :idPaciente and convenio.estado =:estado and trataConvenio.estado =:estado";  
                    $queryTrataConvenio = $entityManager->createQuery($dqlTrataConvenio)->setParameter('idPaciente',$paciente->getIdPaciente())->setParameter('estado',true);
                    $trataConvenios= $queryTrataConvenio->getResult();

                    //Presupuestos de plan en estado de proyeccion
                    $dqlPresupuestoPlan ="SELECT presupuesto FROM App:Presupuesto presupuesto INNER JOIN App:PlanTratamiento plan WITH plan.idPlanTratamiento= presupuesto.idPlanTratamiento WHERE plan.idPaciente= :idPaciente AND plan.idEstadoGeneral=:estadoGeneral";  
                    $queryPresupuestoPlan = $entityManager->createQuery($dqlPresupuestoPlan)->setParameter('idPaciente',$paciente->getIdPaciente())->setParameter('estadoGeneral',5);
                    $presupuestos= $queryPresupuestoPlan->getResult();
                    $totalIvaSimulacion=0;
                    foreach ($presupuestos as $seleccionPresupuestos) {
                        $totalIvaSimulacion=$totalIvaSimulacion+$seleccionPresupuestos->getTotalValorIva();
                    }

                    //Planes tratamientos paciente
                    $dqlPlanTratamientos ="SELECT plan FROM App:PlanTratamiento plan WHERE plan.idPaciente= :idPaciente and plan.idEstadoGeneral != :estadoGeneral";  
                    $queryPlanTratamientos = $entityManager->createQuery($dqlPlanTratamientos)->setParameter('idPaciente',$paciente->getIdPaciente())->setParameter('estadoGeneral',5);
                    $planTratamientos= $queryPlanTratamientos->getResult();

                    
                    $idEps=$paciente->getIdEps();
                    $form = $this->createForm(PresupuestoType::class, $presupuesto);
                    $form->add('idConvenio',EntityType::class, array('placeholder' => 'Seleccionar convenio','class' => 'App:Convenio','required'=>false,'query_builder' => function(EntityRepository $er)use ($idEps){
                      return $er->createQueryBuilder('e')
                      ->where('e.idEps = :idEps and e.estado= :estado')
                      ->setParameter('idEps',$idEps)
                      ->setParameter('estado',TRUE)
                      ->orderBy('e.idConvenio', 'DESC');
                    },));
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        //Estado general de simulación
                        $estadoGeneralSimulador = $this->getDoctrine()
                                ->getRepository('App:EstadoGeneral')
                                ->find(5);
                        $dqlPlanTratamientoSimulador ="SELECT plan FROM App:PlanTratamiento plan WHERE plan.idPaciente= :idPaciente and plan.idEstadoGeneral = :idEstado";  
                        $queryPlanTratamientoSimulador = $entityManager->createQuery($dqlPlanTratamientoSimulador)->setParameter('idPaciente',$paciente->getIdPaciente())->setParameter('idEstado',5);
                        $planTratamientoSimulador= $queryPlanTratamientoSimulador->getResult();
                        if (sizeof($planTratamientoSimulador) > 0) {
                            foreach ($planTratamientoSimulador as $seleccionPlanSimulador) {
                                $presupuesto->setIdPlanTratamiento($seleccionPlanSimulador);
                            }
                        }else{
                            $planTratamiento = new PlanTratamiento();
                            $planTratamiento->setIdPaciente($paciente);                            
                            $planTratamiento->setIdEstadoGeneral($estadoGeneralSimulador);
                            $planTratamiento->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                            $entityManager->persist($planTratamiento);
                            $entityManager->flush();
                            $presupuesto->setIdPlanTratamiento($planTratamiento);
                        }
                        //SELECCION DE TODOS LOS PRESUPUESTOS ACTUALES PARA COMPARAR QUE NO EXISTA UN PRESUPUESTO DENTRO DEL PLAN QUE
                        // TENGA LAS MISMAS CARACTERISTICAS DEL PRESUPUESTO EVALUADO.
                        if (sizeof($presupuestos) >0) {
                            foreach ($presupuestos as $seleccionPresupuesto) {
                                $idPlanTratamientoActual=$seleccionPresupuesto->getIdPlanTratamiento();
                                $idTratamientoActual=$seleccionPresupuesto->getIdTratamiento();
                                $idPiezaActual=$seleccionPresupuesto->getIdPieza();
                                $idSuperficieActual=$seleccionPresupuesto->getIdSuperficie();
                                $idProcedimientoActual=$seleccionPresupuesto->getIdProcedimiento();

                                $idPlanTratamientoEnProceso=$presupuesto->getIdPlanTratamiento();
                                $idTratamientoEnProceso=$presupuesto->getIdTratamiento();
                                $idPiezaEnProceso=$presupuesto->getIdPieza();
                                $idSuperficieEnProceso=$presupuesto->getIdSuperficie();
                                $idProcedimientoEnProceso=$presupuesto->getIdProcedimiento();

                                if ($idPlanTratamientoActual == $idPlanTratamientoEnProceso and $idTratamientoActual == $idTratamientoEnProceso and $idPiezaActual == $idPiezaEnProceso and $idSuperficieActual == $idSuperficieEnProceso and $idProcedimientoActual == $idProcedimientoEnProceso) 
                                {
                                    $this->addFlash('error', 'No se puede ingresar el mismo tratamiento en un plan, verifique los otros tratamientos ya ingresados!');
                                    return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);
                                }else{
                                    $presupuesto->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                                    $presupuesto->setIdEstadoGeneral($estadoGeneralSimulador);
                                    $entityManager->persist($presupuesto);
                                    //$this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!'.$presupuesto->getValorAprobado());
                                    //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);
                                    $entityManager->flush();

                                     /*Modificación de valor plan tratamiento*/
                                    $porcentajePresupuesto=$presupuesto->getPorcentajeDescuento()/100;
                                    if ($presupuesto->getIdConvenio() != '') {
                                        if ($presupuesto->getIdConvenio()->getPorcentajeDescuento() != '') {
                                           $descConvenio=$presupuesto->getIdConvenio()->getPorcentajeDescuento()/100;
                                        }else{
                                            $descConvenio=0;
                                        }
                                        //validación para saber si el tratamiento tiene descuento segun el convenio verificando si este 
                                        //cuenta con porcentajes de descuento en tratamiento para el mismo y a su vez si el tratamiento 
                                        //que se esta evaluando en el presupuesto tiene un descuento adicional.
                                        $dqlTrataConvenioValor ="SELECT trataConve FROM App:TratamientoConvenio trataConve WHERE trataConve.idConvenio= :idConvenio and trataConve.idTratamiento = :idTratamiento"; 
                                        $queryTrataConvenio = $entityManager->createQuery($dqlTrataConvenioValor)->setParameter('idConvenio',$presupuesto->getIdConvenio()->getIdConvenio())->setParameter('idTratamiento',$presupuesto->getIdTratamiento()->getIdTratamiento());
                                        $trataConvenioValor= $queryTrataConvenio->getResult(); 
                                        foreach ($trataConvenioValor as $seleccionTrataconvenio) {
                                            $trataConvenioDescuento=$seleccionTrataconvenio->getPorcentajeTratamiento();
                                        }
                                        if (isset($trataConvenioDescuento)) {
                                            if ($trataConvenioDescuento != '') {
                                                $descTrataConvenio=$trataConvenioDescuento/100;
                                            }else{
                                                $descTrataConvenio=0;
                                            }
                                        }else{
                                            $descTrataConvenio=0;
                                        }
                                        ///Adicionadno descuentos al valor aprobado
                                        $valorDescuentoTotalConvenios=$descConvenio+$descTrataConvenio;
                                        $valorDescuentoTotal=$valorDescuentoTotalConvenios+$porcentajePresupuesto;
                                        $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$valorDescuentoTotal;
                                        $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;
                                        
                                        //Adicionando iva a valor de presupuesto 
                                        $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                                        $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                                        //Suma total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                                        // generales, realizando adición al total actual del plan 
                                        $sumaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()+$valorTotalPresupuestoConIva;


                                        // Modificación de el valor del plan de tratamiento a cobrar
                                        $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                        $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$sumaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                        $modificarPlanValor=$queryModificarPlanValor->execute();

                                        // Modificación del valor del total valor iva  de un presupuesto
                                        $dqlModificarPresupuestoTotalValorIva="UPDATE App:Presupuesto presupuesto SET presupuesto.totalValorIva= :totalValorIva WHERE presupuesto.idPresupuesto= :idPresupuesto";
                                        $queryModificarPresupuestoTotalValorIva=$entityManager->createQuery($dqlModificarPresupuestoTotalValorIva)->setParameter('totalValorIva',$valorAumentoIva)->setParameter('idPresupuesto', $presupuesto->getIdPresupuesto());
                                        $modificarPresupuestoTotalValorIva=$queryModificarPresupuestoTotalValorIva->execute();
                                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                        return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);
                                    }else{
                                        //Manejo de descuentos 
                                        $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$porcentajePresupuesto;
                                        $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;

                                        //Adicionando iva a valor de presupuesto 
                                        $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                                        $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                                        //Suma total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                                        // generales, realizando adición al total actual del plan 
                                        $sumaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()+$valorTotalPresupuestoConIva;

                                        // Modificación del valor del plan de tratamiento a cobrar
                                        $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                        $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$sumaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                        $modificarPlanValor=$queryModificarPlanValor->execute();
                                        //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);

                                        // Modificación del valor del total valor iva  de un presupuesto
                                        $dqlModificarPresupuestoTotalValorIva="UPDATE App:Presupuesto presupuesto SET presupuesto.totalValorIva= :totalValorIva WHERE presupuesto.idPresupuesto= :idPresupuesto";
                                        $queryModificarPresupuestoTotalValorIva=$entityManager->createQuery($dqlModificarPresupuestoTotalValorIva)->setParameter('totalValorIva',$valorAumentoIva)->setParameter('idPresupuesto', $presupuesto->getIdPresupuesto());
                                        $modificarPresupuestoTotalValorIva=$queryModificarPresupuestoTotalValorIva->execute();
                                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                        return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);
                                    }
                                }
                            }
                            
                        }else{
                            $presupuesto->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                            $presupuesto->setIdEstadoGeneral($estadoGeneralSimulador);
                            $entityManager->persist($presupuesto);
                            //$this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!'.$presupuesto->getValorAprobado());
                            //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);
                            $entityManager->flush();

                             /*Modificación de valor plan tratamiento*/
                            $porcentajePresupuesto=$presupuesto->getPorcentajeDescuento()/100;
                            if ($presupuesto->getIdConvenio() != '') {
                                if ($presupuesto->getIdConvenio()->getPorcentajeDescuento() != '') {
                                   $descConvenio=$presupuesto->getIdConvenio()->getPorcentajeDescuento()/100;
                                }else{
                                    $descConvenio=0;
                                }
                                //validación para saber si el tratamiento tiene descuento segun el convenio verificando si este cuenta 
                                //con porcentajes de descuento en tratamiento para el mismo y a su vez si el tratamiento que se esta 
                                //evaluando en el presupuesto tiene un descuento adicional.
                                $dqlTrataConvenioValor ="SELECT trataConve FROM App:TratamientoConvenio trataConve WHERE trataConve.idConvenio= :idConvenio and trataConve.idTratamiento = :idTratamiento"; 
                                $queryTrataConvenio = $entityManager->createQuery($dqlTrataConvenioValor)->setParameter('idConvenio',$presupuesto->getIdConvenio()->getIdConvenio())->setParameter('idTratamiento',$presupuesto->getIdTratamiento()->getIdTratamiento());
                                $trataConvenioValor= $queryTrataConvenio->getResult(); 
                                foreach ($trataConvenioValor as $seleccionTrataconvenio) {
                                    $trataConvenioDescuento=$seleccionTrataconvenio->getPorcentajeTratamiento();
                                }
                                if (isset($trataConvenioDescuento)) {
                                    if ($trataConvenioDescuento != '') {
                                       $descTrataConvenio=$trataConvenioDescuento/100;
                                    }else{
                                        $descTrataConvenio=0;
                                    }
                                }else{
                                    $descTrataConvenio=0;
                                }
                                ///Adicionadno descuentos al valor aprobado
                                $valorDescuentoTotalConvenios=$descConvenio+$descTrataConvenio;
                                $valorDescuentoTotal=$valorDescuentoTotalConvenios+$porcentajePresupuesto;
                                $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$valorDescuentoTotal;
                                $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;
                                
                                //Adicionando iva a valor de presupuesto 
                                $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                                $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                                //Suma total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                                // generales, realizando adición al total actual del plan 
                                $sumaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()+$valorTotalPresupuestoConIva;


                                // Modificación de el valor del plan de tratamiento a cobrar
                                $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$sumaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                $modificarPlanValor=$queryModificarPlanValor->execute();

                                // Modificación del valor del total valor iva  de un presupuesto
                                $dqlModificarPresupuestoTotalValorIva="UPDATE App:Presupuesto presupuesto SET presupuesto.totalValorIva= :totalValorIva WHERE presupuesto.idPresupuesto= :idPresupuesto";
                                $queryModificarPresupuestoTotalValorIva=$entityManager->createQuery($dqlModificarPresupuestoTotalValorIva)->setParameter('totalValorIva',$valorAumentoIva)->setParameter('idPresupuesto', $presupuesto->getIdPresupuesto());
                                $modificarPresupuestoTotalValorIva=$queryModificarPresupuestoTotalValorIva->execute();
                                $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);
                            }else{
                                //Manejo de descuentos 
                                $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$porcentajePresupuesto;
                                $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;

                                //Adicionando iva a valor de presupuesto 
                                $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                                $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                                //Suma total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                                // generales, realizando adición al total actual del plan 
                                $sumaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()+$valorTotalPresupuestoConIva;

                                // Modificación del valor del plan de tratamiento a cobrar
                                $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$sumaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                $modificarPlanValor=$queryModificarPlanValor->execute();
                                //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);

                                // Modificación del valor del total valor iva  de un presupuesto
                                $dqlModificarPresupuestoTotalValorIva="UPDATE App:Presupuesto presupuesto SET presupuesto.totalValorIva = :totalValorIva WHERE presupuesto.idPresupuesto= :idPresupuesto";
                                $queryModificarPresupuestoTotalValorIva=$entityManager->createQuery($dqlModificarPresupuestoTotalValorIva)->setParameter('totalValorIva',$valorAumentoIva)->setParameter('idPresupuesto', $presupuesto->getIdPresupuesto());
                                $modificarPresupuestoTotalValorIva=$queryModificarPresupuestoTotalValorIva->execute();
                                $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);
                            }
                            
                        }
                    }
                    return $this->render('presupuesto/index.html.twig', [
                        'presupuestos' => $presupuestos,                        
                        'paciente'=>$paciente,
                        'trataConvenios'=>$trataConvenios,
                        'presupuesto' => $presupuesto,
                        'form' => $form->createView(),
                        'planTratamientos'=> $planTratamientos,
                        'totalIvaSimulacion'=>$totalIvaSimulacion
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * @Route("/{idPresupuesto}", name="presupuesto_show", methods={"GET"})
     */
    public function show(Presupuesto $presupuesto): Response
    {
        return $this->render('presupuesto/show.html.twig', [
            'presupuesto' => $presupuesto,
        ]);
    }

    /**
     * Visualización de plan de tratamiento  y manejo de visualización para actualización del plan de tratamiento.
     * @Route("/{idPlanTratamiento}/{tipo}/visual", name="plan_show", methods={"GET","POST"})
     */
    public function showPlan(PlanTratamiento $planTratamiento,PerfilGenerator $permisoPerfil,Request $request ): Response
    {        
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'plan de tratamiento');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $fechaActual=date('d M Y H:i:s');
                    $fechaFinal=new  \DateTime($fechaActual);
                    $fechaNacimientoPaciente = $planTratamiento->getIdPaciente()->getFechaNacimiento();
                    $intervalo =date_diff($fechaFinal,$fechaNacimientoPaciente);
                    $edad=$intervalo->format('%Y').' años y '.$intervalo->format('%m').' meses';

                    $dqlTrataConvenio ="SELECT trataConvenio FROM App:TratamientoConvenio trataConvenio INNER JOIN App:Convenio convenio WITH convenio.idConvenio= trataConvenio.idConvenio INNER JOIN App:Eps eps WITH convenio.idEps= eps.idEps INNER JOIN App:Paciente paciente WITH paciente.idEps= eps.idEps WHERE paciente.idPaciente= :idPaciente and convenio.estado =:estado and trataConvenio.estado =:estado";  
                    $queryTrataConvenio = $entityManager->createQuery($dqlTrataConvenio)->setParameter('idPaciente',$planTratamiento->getIdPaciente()->getIdPaciente())->setParameter('estado',true);
                    $trataConvenios= $queryTrataConvenio->getResult();

                    //Presupuestos de plan en estado de proyeccion
                    $dqlPresupuestoPlan ="SELECT presupuesto FROM App:Presupuesto presupuesto INNER JOIN App:PlanTratamiento plan WITH plan.idPlanTratamiento= presupuesto.idPlanTratamiento WHERE plan.idPlanTratamiento= :idPlanTratamiento";  
                    $queryPresupuestoPlan = $entityManager->createQuery($dqlPresupuestoPlan)->setParameter('idPlanTratamiento',$planTratamiento->getIdPlanTratamiento());
                    $presupuestos= $queryPresupuestoPlan->getResult();
                    $totalIvaSimulacion=0;
                    foreach ($presupuestos as $seleccionPresupuestos) {
                        $totalIvaSimulacion=$totalIvaSimulacion+$seleccionPresupuestos->getTotalValorIva();
                    }

                    $tipo=$request->get('tipo') ;

                    $presupuesto= new Presupuesto();
                    $presupuesto->setIdPlanTratamiento($planTratamiento);
                    $form = $this->createForm(PresupuestoType::class, $presupuesto);
                    $form->handleRequest($request);
                    return $this->render('presupuesto/show.html.twig', [
                        'planTratamiento' => $planTratamiento,
                        'edad'=>$edad,
                        'presupuestos'=>$presupuestos,
                        'totalIvaSimulacion'=>$totalIvaSimulacion,
                        'trataConvenios'=>$trataConvenios,
                        'tipo'=>$tipo,
                        'presupuesto'=>$presupuesto,
                        'form'=>$form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      

    }

    /**
     * Manejo de nuevo presupuesto dispuesto para la edición del plan de tratamiento.
     * @Route("/nuevo/presupuesto", name="presupuesto_nuevo", methods={"GET","POST"})
     */
    public function nuevoPresupuestoAprobado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'plan de tratamiento');  
                if (sizeof($permisos) > 0) { 
                    $entityManager=$this->getDoctrine()->getManager();                    
                    //Envio de formulario a pantalla
                    $presupuesto= new Presupuesto();
                    $form = $this->createForm(PresupuestoType::class, $presupuesto);
                    $form->handleRequest($request);   

                    if ($form->isSubmitted() && $form->isValid()) {
                        //Presupuestos del plan actual
                        $dqlPresupuestoPlan ="SELECT presupuesto FROM App:Presupuesto presupuesto INNER JOIN App:PlanTratamiento plan WITH plan.idPlanTratamiento= presupuesto.idPlanTratamiento WHERE plan.idPlanTratamiento= :idPlanTratamiento";  
                        $queryPresupuestoPlan = $entityManager->createQuery($dqlPresupuestoPlan)->setParameter('idPlanTratamiento',$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                        $presupuestos= $queryPresupuestoPlan->getResult();

                        //Estado general de simulación
                        $estadoGeneralSimulador = $this->getDoctrine()
                        ->getRepository('App:EstadoGeneral')
                        ->find(2);
                        //SELECCION DE TODOS LOS PRESUPUESTOS ACTUALES PARA COMPARAR QUE NO EXISTA UN PRESUPUESTO DENTRO DEL PLAN QUE
                        // TENGA LAS MISMAS CARACTERISTICAS DEL PRESUPUESTO EVALUADO.
                        if (sizeof($presupuestos) >0) {
                            foreach ($presupuestos as $seleccionPresupuesto) {
                                $idPlanTratamientoActual=$seleccionPresupuesto->getIdPlanTratamiento();
                                $idTratamientoActual=$seleccionPresupuesto->getIdTratamiento();
                                $idPiezaActual=$seleccionPresupuesto->getIdPieza();
                                $idSuperficieActual=$seleccionPresupuesto->getIdSuperficie();
                                $idProcedimientoActual=$seleccionPresupuesto->getIdProcedimiento();

                                $idPlanTratamientoEnProceso=$presupuesto->getIdPlanTratamiento();
                                $idTratamientoEnProceso=$presupuesto->getIdTratamiento();
                                $idPiezaEnProceso=$presupuesto->getIdPieza();
                                $idSuperficieEnProceso=$presupuesto->getIdSuperficie();
                                $idProcedimientoEnProceso=$presupuesto->getIdProcedimiento();

                                if ($idPlanTratamientoActual == $idPlanTratamientoEnProceso and $idTratamientoActual == $idTratamientoEnProceso and $idPiezaActual == $idPiezaEnProceso and $idSuperficieActual == $idSuperficieEnProceso and $idProcedimientoActual == $idProcedimientoEnProceso) 
                                {
                                    $this->addFlash('error', 'No se puede ingresar el mismo tratamiento en un plan, verifique los otros tratamientos ya ingresados!');
                                    return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                                }else{
                                    $presupuesto->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                                    $presupuesto->setIdEstadoGeneral($estadoGeneralSimulador);
                                    $entityManager->persist($presupuesto);
                                    //$this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!'.$presupuesto->getValorAprobado());
                                    //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);
                                    $entityManager->flush();

                                     /*Modificación de valor plan tratamiento*/
                                    $porcentajePresupuesto=$presupuesto->getPorcentajeDescuento()/100;
                                    if ($presupuesto->getIdConvenio() != '') {
                                        if ($presupuesto->getIdConvenio()->getPorcentajeDescuento() != '') {
                                           $descConvenio=$presupuesto->getIdConvenio()->getPorcentajeDescuento()/100;
                                        }else{
                                            $descConvenio=0;
                                        }
                                        //validación para saber si el tratamiento tiene descuento segun el convenio verificando si este 
                                        //cuenta con porcentajes de descuento en tratamiento para el mismo y a su vez si el tratamiento 
                                        //que se esta evaluando en el presupuesto tiene un descuento adicional.
                                        $dqlTrataConvenioValor ="SELECT trataConve FROM App:TratamientoConvenio trataConve WHERE trataConve.idConvenio= :idConvenio and trataConve.idTratamiento = :idTratamiento"; 
                                        $queryTrataConvenio = $entityManager->createQuery($dqlTrataConvenioValor)->setParameter('idConvenio',$presupuesto->getIdConvenio()->getIdConvenio())->setParameter('idTratamiento',$presupuesto->getIdTratamiento()->getIdTratamiento());
                                        $trataConvenioValor= $queryTrataConvenio->getResult(); 
                                        foreach ($trataConvenioValor as $seleccionTrataconvenio) {
                                            $trataConvenioDescuento=$seleccionTrataconvenio->getPorcentajeTratamiento();
                                        }
                                        if (isset($trataConvenioDescuento)) {
                                            if ($trataConvenioDescuento != '') {
                                                $descTrataConvenio=$trataConvenioDescuento/100;
                                            }else{
                                                $descTrataConvenio=0;
                                            }
                                        }else{
                                            $descTrataConvenio=0;
                                        }
                                        ///Adicionadno descuentos al valor aprobado
                                        $valorDescuentoTotalConvenios=$descConvenio+$descTrataConvenio;
                                        $valorDescuentoTotal=$valorDescuentoTotalConvenios+$porcentajePresupuesto;
                                        $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$valorDescuentoTotal;
                                        $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;
                                        
                                        //Adicionando iva a valor de presupuesto 
                                        $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                                        $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                                        //Suma total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                                        // generales, realizando adición al total actual del plan 
                                        $sumaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()+$valorTotalPresupuestoConIva;


                                        // Modificación de el valor del plan de tratamiento a cobrar
                                        $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                        $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$sumaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                        $modificarPlanValor=$queryModificarPlanValor->execute();

                                        // Modificación del valor del total valor iva  de un presupuesto
                                        $dqlModificarPresupuestoTotalValorIva="UPDATE App:Presupuesto presupuesto SET presupuesto.totalValorIva= :totalValorIva WHERE presupuesto.idPresupuesto= :idPresupuesto";
                                        $queryModificarPresupuestoTotalValorIva=$entityManager->createQuery($dqlModificarPresupuestoTotalValorIva)->setParameter('totalValorIva',$valorAumentoIva)->setParameter('idPresupuesto', $presupuesto->getIdPresupuesto());
                                        $modificarPresupuestoTotalValorIva=$queryModificarPresupuestoTotalValorIva->execute();
                                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                        return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                                    }else{
                                        //Manejo de descuentos 
                                        $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$porcentajePresupuesto;
                                        $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;

                                        //Adicionando iva a valor de presupuesto 
                                        $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                                        $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                                        //Suma total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                                        // generales, realizando adición al total actual del plan 
                                        $sumaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()+$valorTotalPresupuestoConIva;

                                        // Modificación del valor del plan de tratamiento a cobrar
                                        $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                        $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$sumaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                        $modificarPlanValor=$queryModificarPlanValor->execute();
                                        //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);

                                        // Modificación del valor del total valor iva  de un presupuesto
                                        $dqlModificarPresupuestoTotalValorIva="UPDATE App:Presupuesto presupuesto SET presupuesto.totalValorIva= :totalValorIva WHERE presupuesto.idPresupuesto= :idPresupuesto";
                                        $queryModificarPresupuestoTotalValorIva=$entityManager->createQuery($dqlModificarPresupuestoTotalValorIva)->setParameter('totalValorIva',$valorAumentoIva)->setParameter('idPresupuesto', $presupuesto->getIdPresupuesto());
                                        $modificarPresupuestoTotalValorIva=$queryModificarPresupuestoTotalValorIva->execute();
                                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                        return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                                    }
                                }
                            }
                            
                        }else{
                        
                            //SELECCION DE TODOS LOS PRESUPUESTOS ACTUALES PARA COMPARAR QUE NO EXISTA UN PRESUPUESTO DENTRO DEL PLAN QUE
                            // TENGA LAS MISMAS CARACTERISTICAS DEL PRESUPUESTO EVALUADO.
                            $presupuesto->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                            $presupuesto->setIdEstadoGeneral($estadoGeneralSimulador);
                            $entityManager->persist($presupuesto);
                            $entityManager->flush();

                            /*Modificación de valor plan tratamiento*/
                            $porcentajePresupuesto=$presupuesto->getPorcentajeDescuento()/100;
                            if ($presupuesto->getIdConvenio() != '') {
                                if ($presupuesto->getIdConvenio()->getPorcentajeDescuento() != '') {
                                   $descConvenio=$presupuesto->getIdConvenio()->getPorcentajeDescuento()/100;
                                }else{
                                    $descConvenio=0;
                                }
                                //validación para saber si el tratamiento tiene descuento segun el convenio verificando si este cuenta 
                                //con porcentajes de descuento en tratamiento para el mismo y a su vez si el tratamiento que se esta 
                                //evaluando en el presupuesto tiene un descuento adicional.
                                $dqlTrataConvenioValor ="SELECT trataConve FROM App:TratamientoConvenio trataConve WHERE trataConve.idConvenio= :idConvenio and trataConve.idTratamiento = :idTratamiento"; 
                                $queryTrataConvenio = $entityManager->createQuery($dqlTrataConvenioValor)->setParameter('idConvenio',$presupuesto->getIdConvenio()->getIdConvenio())->setParameter('idTratamiento',$presupuesto->getIdTratamiento()->getIdTratamiento());
                                $trataConvenioValor= $queryTrataConvenio->getResult(); 
                                foreach ($trataConvenioValor as $seleccionTrataconvenio) {
                                    $trataConvenioDescuento=$seleccionTrataconvenio->getPorcentajeTratamiento();
                                }
                                if (isset($trataConvenioDescuento)) {
                                    if ($trataConvenioDescuento != '') {
                                       $descTrataConvenio=$trataConvenioDescuento/100;
                                    }else{
                                        $descTrataConvenio=0;
                                    }
                                }else{
                                    $descTrataConvenio=0;
                                }
                                ///Adicionadno descuentos al valor aprobado
                                $valorDescuentoTotalConvenios=$descConvenio+$descTrataConvenio;
                                $valorDescuentoTotal=$valorDescuentoTotalConvenios+$porcentajePresupuesto;
                                $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$valorDescuentoTotal;
                                $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;
                                
                                //Adicionando iva a valor de presupuesto 
                                $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                                $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                                //Suma total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                                // generales, realizando adición al total actual del plan 
                                $sumaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()+$valorTotalPresupuestoConIva;


                                // Modificación de el valor del plan de tratamiento a cobrar
                                $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$sumaTotalPlanActual)->setParameter('idPlanTratamiento',$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                $modificarPlanValor=$queryModificarPlanValor->execute();

                                // Modificación del valor del total valor iva  de un presupuesto
                                $dqlModificarPresupuestoTotalValorIva="UPDATE App:Presupuesto presupuesto SET presupuesto.totalValorIva= :totalValorIva WHERE presupuesto.idPresupuesto= :idPresupuesto";
                                $queryModificarPresupuestoTotalValorIva=$entityManager->createQuery($dqlModificarPresupuestoTotalValorIva)->setParameter('totalValorIva',$valorAumentoIva)->setParameter('idPresupuesto', $presupuesto->getIdPresupuesto());
                                $modificarPresupuestoTotalValorIva=$queryModificarPresupuestoTotalValorIva->execute();
                                // $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);
                                $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                            }else{
                                //Manejo de descuentos 
                                $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$porcentajePresupuesto;
                                $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;

                                //Adicionando iva a valor de presupuesto 
                                $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                                $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                                //Suma total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                                // generales, realizando adición al total actual del plan 
                                $sumaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()+$valorTotalPresupuestoConIva;

                                // Modificación del valor del plan de tratamiento a cobrar
                                $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$sumaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                $modificarPlanValor=$queryModificarPlanValor->execute();
                                //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$paciente->getIdPaciente()]);

                                // Modificación del valor del total valor iva  de un presupuesto
                                $dqlModificarPresupuestoTotalValorIva="UPDATE App:Presupuesto presupuesto SET presupuesto.totalValorIva = :totalValorIva WHERE presupuesto.idPresupuesto= :idPresupuesto";
                                $queryModificarPresupuestoTotalValorIva=$entityManager->createQuery($dqlModificarPresupuestoTotalValorIva)->setParameter('totalValorIva',$valorAumentoIva)->setParameter('idPresupuesto', $presupuesto->getIdPresupuesto());
                                $modificarPresupuestoTotalValorIva=$queryModificarPresupuestoTotalValorIva->execute();
                                $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                                return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                            }
                        }
                        //$this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        //return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                    }
                    
                    return $this->render('presupuesto/edit.html.twig', [
                        'presupuesto' => $presupuesto,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }    
    }

    /**
     * Proceso de eliminación de presupuestos de un plan de tratamiento
     * @Route("/{idPresupuesto}", name="presupuesto_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Presupuesto $presupuesto,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'plan de tratamiento');  
              if (sizeof($permisos) > 0) {
                $idPaciente=$presupuesto->getIdPlanTratamiento()->getIdPaciente()->getIdPaciente();
                try{
                    if ($this->isCsrfTokenValid('delete'.$presupuesto->getIdPresupuesto(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        
                        //Total abono por cliente
                        $sumaAbonosCliente=$presupuesto->getIdPlanTratamiento()->getAbonoTotal()+
                        $presupuesto->getIdPlanTratamiento()->getCuotaInicial();
                        

                        /*Modificación de valor plan tratamiento*/
                        $porcentajePresupuesto=$presupuesto->getPorcentajeDescuento()/100;
                        if ($presupuesto->getIdConvenio() != '') {
                            if ($presupuesto->getIdConvenio()->getPorcentajeDescuento() != '') {
                               $descConvenio=$presupuesto->getIdConvenio()->getPorcentajeDescuento()/100;
                            }else{
                                $descConvenio=0;
                            }
                            //validación para saber si el tratamiento tiene descuento segun el convenio verificando si este cuenta 
                            //con porcentajes de descuento en tratamiento para el mismo y a su vez si el tratamiento que se esta 
                            //evaluando en el presupuesto tiene un descuento adicional.
                            $dqlTrataConvenioValor ="SELECT trataConve FROM App:TratamientoConvenio trataConve WHERE trataConve.idConvenio= :idConvenio and trataConve.idTratamiento = :idTratamiento"; 
                            $queryTrataConvenio = $entityManager->createQuery($dqlTrataConvenioValor)->setParameter('idConvenio',$presupuesto->getIdConvenio()->getIdConvenio())->setParameter('idTratamiento',$presupuesto->getIdTratamiento()->getIdTratamiento());
                            $trataConvenioValor= $queryTrataConvenio->getResult(); 
                            foreach ($trataConvenioValor as $seleccionTrataconvenio) {
                                $trataConvenioDescuento=$seleccionTrataconvenio->getPorcentajeTratamiento();
                            }
                            if (isset($trataConvenioDescuento)) {
                                if ($trataConvenioDescuento != '') {
                                   $descTrataConvenio=$trataConvenioDescuento/100;
                                }else{
                                    $descTrataConvenio=0;
                                }
                            }else{
                                $descTrataConvenio=0;
                            }
                            ///Adicionadno descuentos al valor aprobado
                            $valorDescuentoTotalConvenios=$descConvenio+$descTrataConvenio;
                            $valorDescuentoTotal=$valorDescuentoTotalConvenios+$porcentajePresupuesto;
                            $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$valorDescuentoTotal;
                            $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;
                            
                            //Adicionando iva a valor de presupuesto 
                            $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                            $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                            //Resta total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                            // generales, realizando adición al total actual del plan 
                            $restaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()-$valorTotalPresupuestoConIva;
                            if ($sumaAbonosCliente != '') {
                                if ($sumaAbonosCliente <=  $restaTotalPlanActual) {
                                   // Modificación de el valor del plan de tratamiento a cobrar
                                    $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                    $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$restaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                    $modificarPlanValor=$queryModificarPlanValor->execute();

                                    // Eliminación de presupuesto perteneciente al plan
                                    $entityManager->remove($presupuesto);
                                    $entityManager->flush();
                                    $this->addFlash('mensaje', 'Se ha eliminado de forma correcta!');
                                    return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                                }else{
                                    $this->addFlash('error', 'El monto abonado supera el valor del tratamiento!');
                                    //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$idPaciente]);
                                    return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                                }
                            }else{
                                // Modificación de el valor del plan de tratamiento a cobrar
                                $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$restaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                $modificarPlanValor=$queryModificarPlanValor->execute();

                                // Eliminación de presupuesto perteneciente al plan
                                $entityManager->remove($presupuesto);
                                $entityManager->flush();
                                $this->addFlash('mensaje', 'Se ha eliminado de forma correcta!');
                                return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$idPaciente]);
                            }

                            
                        }else{
                            //Manejo de descuentos 
                            $valorDescuentoPresupuesto=$presupuesto->getValorAprobado()*$porcentajePresupuesto;
                            $valorFinalPresupuestoConDescuentos=$presupuesto->getValorAprobado()-$valorDescuentoPresupuesto;

                            //Adicionando iva a valor de presupuesto 
                            $valorAumentoIva=  $valorFinalPresupuestoConDescuentos*($presupuesto->getIva()/100);
                            $valorTotalPresupuestoConIva= $valorFinalPresupuestoConDescuentos+$valorAumentoIva;

                            //Resta total del valor de plan de tratamiento a cobrar con datos de aumentos de iva y descuentos 
                            // generales, realizando adición al total actual del plan 
                            $restaTotalPlanActual=$presupuesto->getIdPlanTratamiento()->getValorTotal()-$valorTotalPresupuestoConIva;
                            /*if ($sumaAbonosCliente != '') {
                                if ($sumaAbonosCliente <=  $restaTotalPlanActual) {
                                    // Modificación del valor del plan de tratamiento a cobrar
                                    $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                    $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$restaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                    $modificarPlanValor=$queryModificarPlanValor->execute();

                                    // Eliminación de presupuesto perteneciente al plan
                                    $entityManager->remove($presupuesto);
                                    $entityManager->flush();
                                    $this->addFlash('mensaje', 'Se ha eliminado de forma correcta!');
                                    return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                                }else{
                                    $this->addFlash('error', 'El monto abonado supera el valor del tratamiento!');
                                    return $this->redirectToRoute('plan_show',['idPlanTratamiento'=>$presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento(),'tipo'=>'edit']);
                                    //return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$idPaciente]);
                                }
                            }else{*/
                                // Modificación de el valor del plan de tratamiento a cobrar
                                $dqlModificarPlanValor="UPDATE App:PlanTratamiento plan SET plan.valorTotal= :valor WHERE plan.idPlanTratamiento= :idPlanTratamiento";
                                $queryModificarPlanValor=$entityManager->createQuery($dqlModificarPlanValor)->setParameter('valor',$restaTotalPlanActual)->setParameter('idPlanTratamiento', $presupuesto->getIdPlanTratamiento()->getIdPlanTratamiento());
                                $modificarPlanValor=$queryModificarPlanValor->execute();

                                // Eliminación de presupuesto perteneciente al plan
                                $entityManager->remove($presupuesto);
                                $entityManager->flush();
                                $this->addFlash('mensaje', 'Se ha eliminado de forma correcta!');
                                return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$idPaciente]);
                           // }
                        }
                    }
                }catch (\Doctrine\DBAL\DBALException $e) {
                  if ($e->getCode() == 0){
                      if ($e->getPrevious()->getCode() == 23503){
                          $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                          $this->addFlash('error',$successMessage1);
                          return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$idPaciente]);
                      }else{
                          throw $e;
                      }
                  }else{
                      throw $e;
                  }
                } 
                return $this->redirectToRoute('presupuesto_index',['idPaciente'=>$idPaciente]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }      
    }
}
