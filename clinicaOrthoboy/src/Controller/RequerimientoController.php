<?php

namespace App\Controller;

use App\Entity\Requerimiento;
use App\Form\RequerimientoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de requerimiento utilizado para el manejo de las solicitudes de elementos que las sedes consideran que necesitan pero que requieren que sean aprobadas 
 * por otros funcionarios para que se lleven al proceso de compra.
 *
 * @Route("/requerimiento")
 */
class RequerimientoController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de los requerimientos de la clinica y sus sedes, además del registro de requerimientos.
     *
     * @Route("/", name="requerimiento_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes requerimientos');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                   $arrayEntradas=array();
                   $arrayConsultorios=array();
                    $sedeClinica=$this->get('security.token_storage')->getToken()->getUser()->getIdSucursal()->getIdClinica();
                    $nombrePerfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil()->getNombre();

                    if ($nombrePerfil != 'ROLE_SUPERADMINISTRADOR') {
                        $dqlRequerimiento ="SELECT requerimiento FROM App:Requerimiento requerimiento INNER JOIN App:Clinica clinica WITH clinica.idClinica=requerimiento.idSedeClinica WHERE clinica.idClinica = :idClinica
                        ORDER BY requerimiento.idRequerimiento DESC";  
                        $queryRequerimiento = $entityManager->createQuery($dqlRequerimiento)->setParameter('idClinica',$sedeClinica);
                        $requerimientos= $queryRequerimiento->getResult();

                    }else{
                        $dqlRequerimiento ="SELECT requerimiento FROM App:Requerimiento requerimiento INNER JOIN App:Clinica clinica WITH clinica.idClinica=requerimiento.idSedeClinica ORDER BY requerimiento.idRequerimiento DESC";  
                        $queryRequerimiento = $entityManager->createQuery($dqlRequerimiento);
                        $requerimientos= $queryRequerimiento->getResult();
                    }
                    $requerimiento = new Requerimiento();
                    $requerimiento->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                    $requerimiento->setEstado('EN PROCESO');
                    $form = $this->createForm(RequerimientoType::class,$requerimiento);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($requerimiento);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('elementoRequerimiento_index',['idRequerimiento'=>$requerimiento->getIdRequerimiento()]);
                    }
                    return $this->render('requerimiento/index.html.twig', [
                        'requerimientos' => $requerimientos,
                        'requerimiento' => $requerimiento,
                        'form' => $form->createView()
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

     /**
     * Metodo utilizado para la visualización de un requerimiento y los elementos a solicitar en el mismo.
     *
     * @Route("/{idRequerimiento}", name="requerimiento_show", methods={"GET","POST"})
     */
    public function show(Requerimiento $requerimiento,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $entityManager=$this->getDoctrine()->getManager();
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes requerimientos');  
                if (sizeof($permisos) > 0) {
                    $dqlElementosRequerimiento ="SELECT oRequerimiento FROM App:ElementoRequerimiento oRequerimiento WHERE oRequerimiento.idRequerimiento = :idRequerimiento ORDER BY oRequerimiento.idRequerimiento DESC";  
                    $queryElementosRequerimiento = $entityManager->createQuery($dqlElementosRequerimiento)->setParameter('idRequerimiento',$requerimiento->getIdRequerimiento());
                    $elementosRequerimiento= $queryElementosRequerimiento->getResult();
                    return $this->render('requerimiento/modalShow.html.twig', [
                        'requerimiento' => $requerimiento,
                        'elementosRequerimiento'=> $elementosRequerimiento
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }
    /**
     * Metodo utilizado para la edición de un requerimiento de la clinica y sus sedes.
     *
     * @Route("/{idRequerimiento}/edit", name="requerimiento_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Requerimiento $requerimiento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes requerimientos');  
                if (sizeof($permisos) > 0) {                      
                    $entityManager = $this->getDoctrine()->getManager();
                    $form = $this->createForm(RequerimientoType::class, $requerimiento);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($requerimiento);
                        $entityManager->flush();                       
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                       return $this->redirectToRoute('requerimiento_index');
                         
                    }
                    return $this->render('requerimiento/modalEdit.html.twig', [
                        'requerimiento' => $requerimiento,
                        'form' => $form->createView()
                    ]);                                    
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de un requerimiento de la clinica y sus sedes.
     *
     * @Route("/{idRequerimiento}", name="requerimiento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Requerimiento $requerimiento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes requerimientos');  
              if (sizeof($permisos) > 0) {
                //var_dump($estadoMantenimiento->getIdEstadoMantenimiento());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($requerimiento);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('requerimiento_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('requerimiento_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('requerimiento_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }
}
