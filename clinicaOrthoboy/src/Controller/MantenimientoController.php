<?php

namespace App\Controller;

use App\Entity\Mantenimiento;
use App\Form\MantenimientoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

/**
 * Controlador de mantenimiento utilizado para el manejo de la evidencia de los mantenimientos de cada uno de los elementos que requuieran, soluciones preventivas o 
 * finales de un producto.
 *
 * @Route("/mantenimiento")
 */
class MantenimientoController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de todos los mantenimientos y registro de nuevos mantenimientos a entradas de pedido que no tengan 
     * mantenimiento inicial programado.
     *
     * @Route("/", name="mantenimiento_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Expresión en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'mantenimientos de equipos');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlMantenimiento ="SELECT mantenimiento FROM App:Mantenimiento mantenimiento
                    ORDER BY mantenimiento.idMantenimiento DESC";  
                    $queryMantenimiento = $entityManager->createQuery($dqlMantenimiento);
                    $mantenimientos= $queryMantenimiento->getResult();
                    $arrayEntradas=array();
                    //Datos de entradas generales que nunca han tenido mantenimiento 
                    $dqlEntradaSinMantenimiento ="SELECT ePedido FROM App:EntradaPedido ePedido WHERE ePedido.idEntradaPedido not in (SELECT ep.idEntradaPedido FROM App:EntradaPedido ep inner join App:Mantenimiento mantenimiento WITH mantenimiento.idEntradaPedido=ep.idEntradaPedido)"; 
                    $queryEntradaSinMantenimiento = $entityManager->createQuery($dqlEntradaSinMantenimiento);
                    $entradaSinMantenimiento= $queryEntradaSinMantenimiento->getResult();
                    foreach ($entradaSinMantenimiento as $seleccionEntradas ) {
                        array_push($arrayEntradas, $seleccionEntradas->getIdEntradaPedido());
                    }
                    /*Query que muestra las entradas de pedido que no tienen actualmente un manteniento en estado programado */
                    //Listado de ultimos mantenimientos por entrada de pedido
                    $manteniminetosXEntradaPedido = $entityManager->createQueryBuilder();
                    $manteniminetosXEntradaPedido->select('max(mantenimiento.idMantenimiento)')
                        ->from('App:Mantenimiento', 'mantenimiento')                          
                        ->groupBy('mantenimiento.idEntradaPedido')
                          ;
                    //Listado de id_entrada_pedido que pertenecen a los mantenimientos finales de una misma entrada de pedido y que su estado no es programado
                    $dqlEntradaPedidosMantenimiento = $entityManager->createQueryBuilder();
                    $dqlEntradaPedidosMantenimiento->select('IDENTITY(man.idEntradaPedido)')
                        ->from('App:Mantenimiento', 'man')
                        ->where($dqlEntradaPedidosMantenimiento->expr()->in('man.idMantenimiento ',$manteniminetosXEntradaPedido->getDQL()))
                        ->andWhere('man.idEstadoMantenimiento != :estado');

                    //Mostrar las entradas de pedido que actualmente no tienen mantenimientos en estado programado 
                    $dqlEntradaConMantenimiento=$entityManager->createQueryBuilder();
                    $dqlEntradaConMantenimiento->select('entradaPedido')
                       ->from('App:EntradaPedido','entradaPedido')
                       ->where($dqlEntradaConMantenimiento->expr()->in('entradaPedido.idEntradaPedido ',$dqlEntradaPedidosMantenimiento->getDQL()))
                       ;
                    // $queryUsuario=$em->createQuery($dqlUsuarioactivo)->setParameter('estado',True); 
                    // $usuariosActivos= $queryUsuario->getResult();


                    /*$dqlEntradaConMantenimiento="SELECT ePedido FROM App:EntradaPedido ePedido WHERE ePedido.idEntradaPedido  in (SELECT  m.idEntradaPedido FROM App:Mantenimiento m WHERE m.idMantenimiento in (SELECT max(man.idMantenimiento) FROM App:Mantenimiento man WHERE man.idEstadoMantenimiento != :estadoMantenimiento GROUP BY man.idEntradaPedido))";*/
                    $queryEntradaConMantenimiento = $entityManager->createQuery($dqlEntradaConMantenimiento)->setParameter('estado',1);
                    $entradaConMantenimiento= $queryEntradaConMantenimiento->getResult();
                    foreach ($entradaConMantenimiento as $seleccionEntradas ) {
                        array_push($arrayEntradas, $seleccionEntradas->getIdEntradaPedido());
                    }


                    $mantenimiento = new Mantenimiento();
                    $form = $this->createForm(MantenimientoType::class, $mantenimiento);
                    $form->add('idEntradaPedido',EntityType::class,array(
                        'placeholder' => 'Seleccione elemento','class'=>'App:EntradaPedido',
                        'attr'=>array('required'=>'required'),
                        'query_builder' => function(EntityRepository $er)use ($arrayEntradas)
                            {
                                return $er->createQueryBuilder('e')
                                ->where('e.estado = :estado')
                                ->andWhere('e.idEntradaPedido in (:idEntradaPedido)')
                                ->setParameter('estado',TRUE)
                                ->setParameter('idEntradaPedido',$arrayEntradas);
                            }
                        ));
                    $form->add('idEstadoMantenimiento',EntityType::class,array(
                        'placeholder' => 'Seleccione estado mantenimiento','class'=>'App:EstadoMantenimiento',
                        'attr'=>array('required'=>'required'),
                        'query_builder' => function(EntityRepository $er)
                            {
                                return $er->createQueryBuilder('e')
                                ->where('e.idEstadoMantenimiento != :id')
                                ->setParameter('id',3);
                            }
                        ));
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($mantenimiento);
                        $entityManager->flush();                        
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('mantenimiento_index');
                    }
                    return $this->render('mantenimiento/index.html.twig', [
                        'mantenimientos' => $mantenimientos,
                        'mantenimiento' => $mantenimiento,
                        'form' => $form->createView()
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }
   
    /**
     * Metodo utilizado para la visualización de un mantenimiento.
     *
     * @Route("/{idMantenimiento}", name="mantenimiento_show", methods={"GET","POST"})
     */
    public function show(Mantenimiento $mantenimiento,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'mantenimientos de equipos');  
                if (sizeof($permisos) > 0) {
                    return $this->render('mantenimiento/modalShow.html.twig', [
                        'mantenimiento' => $mantenimiento,
                    ]); 
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la edición de un mantenimiento.
     *
     * @Route("/{idMantenimiento}/edit", name="mantenimiento_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Mantenimiento $mantenimiento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'mantenimientos de equipos');  
                if (sizeof($permisos) > 0) {
                    $estadoIngreso=$mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento();
                    $entityManager = $this->getDoctrine()->getManager();
                    $form = $this->createForm(MantenimientoType::class, $mantenimiento);
                    $form->add('idEstadoMantenimiento',EntityType::class,array(
                        'placeholder' => 'Seleccione estado mantenimiento','class'=>'App:EstadoMantenimiento',
                        'attr'=>array('required'=>'required'),
                        'query_builder' => function(EntityRepository $er)
                            {
                                return $er->createQueryBuilder('e')
                                ->where('e.idEstadoMantenimiento != :id')
                                ->setParameter('id',1);
                            }
                        ));
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        if ($mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento() == 2 and $estadoIngreso == $mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento()) {
                            $entityManager->persist($mantenimiento);
                            $entityManager->flush();                        
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                             return $this->redirectToRoute('mantenimiento_index');
                        }else if ($mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento() == 3 and $estadoIngreso != $mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento()) {
                            $entityManager->persist($mantenimiento);
                            $entityManager->flush();                        
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                             return $this->redirectToRoute('mantenimiento_index');
                            
                        }else if ($mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento() == 3 and $estadoIngreso == $mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento()) {
                            $this->addFlash('error', 'No se puede actualizar el mantenimiento, esta cancelado!');
                             return $this->redirectToRoute('mantenimiento_index');
                        }else if ($mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento() != 1 and $estadoIngreso != $mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento() and  $mantenimiento->getIdEstadoMantenimiento()->getIdEstadoMantenimiento() != 3) {
                            $entityManager->persist($mantenimiento);
                            $entityManager->flush();        

                            $mantenimientoDos=new Mantenimiento();  
                            $fechaEjecucionUltima = $mantenimiento->getFechaEjecucion()->format('d M Y H:i:s');
                            if ($mantenimiento->getIdEntradaPedido()->getIntervaloMantenimiento() != '') {
                                $separacionIntervalo = explode("_", $mantenimiento->getIdEntradaPedido()->getIntervaloMantenimiento());
                                if ($separacionIntervalo[0]== 'DIA') {
                                    //sumo cantidad dias
                                    $dias=strtotime($fechaEjecucionUltima."+ ".$separacionIntervalo[1]." days");
                                    $fecha=new \DateTime(date("m/d/Y h:i:s A T",$dias)); 
                                    $mantenimientoDos->setFechaProgramada($fecha);
                                }else if ($separacionIntervalo[0]=='MES') {
                                    //sumo cantidad meses
                                    $meses=strtotime($fechaEjecucionUltima."+ ".$separacionIntervalo[1]." month");
                                    $fecha=new \DateTime(date("m/d/Y h:i:s A T",$meses)); 
                                    $mantenimientoDos->setFechaProgramada($fecha);
                                }else if ($separacionIntervalo[0] == 'ANIO') {
                                    //sumo cantidad años
                                    $anios=strtotime($fechaEjecucionUltima."+ ".$separacionIntervalo[1]." year");
                                     $fecha=new \DateTime(date("m/d/Y h:i:s A T",$anios)); 
                                    $mantenimientoDos->setFechaProgramada($fecha);
                                }
                                $mantenimientoDos->setIdEntradaPedido($mantenimiento->getIdEntradaPedido());
                                $mantenimientoDos->setTipoMantenimiento('PREVENTIVO');
                                $dqlEstadoMantenimiento ="SELECT eMantenimiento FROM App:EstadoMantenimiento eMantenimiento WHERE eMantenimiento.idEstadoMantenimiento= :idEstadoMantenimiento";  
                                $queryEstadoMantenimiento = $entityManager->createQuery($dqlEstadoMantenimiento)->setParameter('idEstadoMantenimiento',1);
                                $EstadoMantenimientos= $queryEstadoMantenimiento->getResult();
                                foreach ($EstadoMantenimientos as $seleccionEstado ) {
                                    $mantenimientoDos->setIdEstadoMantenimiento($seleccionEstado);
                                }                                                   
                                $entityManager->persist($mantenimientoDos);
                                $entityManager->flush();  
                            }else{
                                
                               $fechaAperturaEntrada=new \DateTime($mantenimiento->getIdEntradaPedido()->getFechaApertura()); 

                               $dqlPrimerMantenimientoEntrada ="SELECT man FROM App:Mantenimiento man WHERE man.idMantenimiento in (SELECT min(eMantenimiento.idMantenimiento) as mantenimientoUno FROM App:Mantenimiento eMantenimiento WHERE eMantenimiento.idEntradaPedido= :idEntradaPedido)";  
                                $queryPrimerMantenimientoEntrada = $entityManager->createQuery($dqlPrimerMantenimientoEntrada)->setParameter('idEntradaPedido',$mantenimiento->getIdEntradaPedido()->getIdEntradaPedido());
                                $primerMantenimientoEntradas= $queryPrimerMantenimientoEntrada->getResult();
                                foreach ($primerMantenimientoEntradas as $seleccionMantenimiento) {
                                    $fechaProgramadaInicial=new \DateTime($seleccionMantenimiento->getFechaProgramada());
                                }

                                $diferenciaEntreFechas= $fechaAperturaEntrada->diff($fechaProgramadaInicial);
                                $intervaloDias= $diferenciaEntreFechas->days();
                                
                                $dias=strtotime($fechaEjecucionUltima."+ ".$intervaloDias." days");
                                $fecha=new \DateTime(date("m/d/Y h:i:s A T",$dias)); 
                                $mantenimientoDos->setFechaProgramada($fecha);

                                $mantenimientoDos->setIdEntradaPedido($mantenimiento->getIdEntradaPedido());
                                $mantenimientoDos->setTipoMantenimiento('PREVENTIVO');
                                $dqlEstadoMantenimiento ="SELECT eMantenimiento FROM App:EstadoMantenimiento eMantenimiento WHERE eMantenimiento.idEstadoMantenimiento= :idEstadoMantenimiento";  
                                $queryEstadoMantenimiento = $entityManager->createQuery($dqlEstadoMantenimiento)->setParameter('idEstadoMantenimiento',1);
                                $EstadoMantenimientos= $queryEstadoMantenimiento->getResult();
                                foreach ($EstadoMantenimientos as $seleccionEstado ) {
                                    $mantenimientoDos->setIdEstadoMantenimiento($seleccionEstado);
                                }                                                   
                                $entityManager->persist($mantenimientoDos);
                                $entityManager->flush(); 
                            }     
                            
                            $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                             return $this->redirectToRoute('mantenimiento_index');
                        }
                        
                    }

                    return $this->render('mantenimiento/modalEdit.html.twig', [
                        'mantenimiento' => $mantenimiento,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la eliminación de un mantenimiento.
     *
     * @Route("/{idMantenimiento}", name="mantenimiento_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Mantenimiento $mantenimiento,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'mantenimientos de equipos');  
              if (sizeof($permisos) > 0) {
                //var_dump($esterilizacion->getIdEsterilizacion());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($mantenimiento);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('mantenimiento_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('mantenimiento_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('mantenimiento_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        } 
    }
}
