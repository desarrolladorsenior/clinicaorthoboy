<?php

namespace App\Controller;

use App\Entity\Consultorio;
use App\Form\ConsultorioType;
use App\Service\PerfilGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Controlador de consultorio utilizado para el manejo de los mismos en diferentes sedes, se evidenciaran los lugares de atención de las citas, el manejo de elementos 
 * utilizados en un consultorio.
 *
 * @Route("/consultorio")
 */
class ConsultorioController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización  de los consultorios que tiene una sede.
     *
     * @Route("/", name="consultorio_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
          ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'consultorio');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlConsultorio ="SELECT consultorio FROM App:Consultorio consultorio 
                    ORDER BY consultorio.idClinica DESC";  
                    $queryConsultorio = $entityManager->createQuery($dqlConsultorio);
                    $consultorios= $queryConsultorio->getResult();
                    
                    $consultorio = new Consultorio();
                    $usuarioIng=$this->get('security.token_storage')->getToken()->getUser()->getIdUsuario();
                    $dqlUsuarioIngresa ="SELECT usuario FROM App:Usuario usuario WHERE usuario.username = :idUsuario"; 
                    $queryUsuarioIngresa = $entityManager->createQuery($dqlUsuarioIngresa)->setParameter('idUsuario',$login);
                    $usuarioIngreso= $queryUsuarioIngresa->getResult();
                    foreach ($usuarioIngreso as $usuarioSeleccionado) {

                      $consultorio->getIdUsuarioIngresa($consultorio->setIdUsuarioIngresa($usuarioSeleccionado));
                    }
                    $form = $this->createForm(ConsultorioType::class, $consultorio);
                    $form->handleRequest($request);

                    return $this->render('consultorio/index.html.twig', [
                        'consultorios' => $consultorios,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para el resgistro de los consultorios.
     *
     * @Route("/new", name="consultorio_new", methods={"GET","POST"})
     */
    public function new(Request $request,PerfilGenerator $permisoPerfil): Response
    {
          ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'consultorio');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $consultorio = new Consultorio();
                    $form = $this->createForm(ConsultorioType::class, $consultorio);
                    $form->handleRequest($request);

                    if ($form->isSubmitted()){
                        $dqlConsultorio ="SELECT consultorio FROM App:Consultorio consultorio WHERE consultorio.nombre = :nombre and consultorio.idClinica = :idClinica";  
                        $queryConsultorio = $entityManager->createQuery($dqlConsultorio)->setParameter('nombre',strtoupper($consultorio->getNombre()))->setParameter('idClinica',$consultorio->getIdClinica()->getIdClinica());
                        $consultorios= $queryConsultorio->getResult();
                        if (!empty($consultorios)) {
                            $response->setData(array('error'=>'Ya existe un registro con este nombre asignado a esta sede.'));
                            return $response;
                        }else{
                            if($form->isValid()) {
                                $consultorio->getEstado($consultorio->setEstado(true));   
                                $entityManager->persist($consultorio);
                                $entityManager->flush();
                                $response->setData(array('mensaje'=>'Se ha realizado el registro de forma correcta!'));
                                return $response;
                            }    
                        }                        
                    }
                    return $response;
                 }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    

     /**
     * Metodo utilizado para la edición de estado de los consultorio.
     *
     * @Route("/editarestadoConsultorio", name="consultorio_editestado", methods={"GET","POST"})
     */
    public function editEstadoConsultorio(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'consultorio');  
              $response = new JsonResponse();
              //var_dump('expression',sizeof( $permisos));
              if (sizeof($permisos) > 0) {  
                $em=$this->getDoctrine()->getManager();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }
                $dqlModificarEstado="UPDATE App:Consultorio consultorio SET consultorio.estado= :estado WHERE consultorio.idConsultorio= :idConsultorio";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idConsultorio', $id);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $response->setData(['error'=>'Este recurso no es permitido para su perfil!']);
                return $response;
              }    
            }
        }    

    }

    /**
     * Metodo utilizado para el envio del formulario de edición de los consultorios.
     *
     * @Route("/{idConsultorio}/edit", name="consultorio_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Consultorio $consultorio,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'consultorio');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $usuarioIng=$this->get('security.token_storage')->getToken()->getUser()->getIdUsuario();
                    $dqlUsuarioIngresa ="SELECT usuario FROM App:Usuario usuario WHERE usuario.username = :idUsuario"; 
                    $queryUsuarioIngresa = $entityManager->createQuery($dqlUsuarioIngresa)->setParameter('idUsuario',$login);
                    $usuarioIngreso= $queryUsuarioIngresa->getResult();
                    foreach ($usuarioIngreso as $usuarioSeleccionado) {
                      $consultorio->getIdUsuarioIngresa($consultorio->setIdUsuarioIngresa($usuarioSeleccionado));
                    }
                    $form = $this->createForm(ConsultorioType::class, $consultorio);
                    $form->handleRequest($request);
                    return $this->render('consultorio/modalEdit.html.twig', [
                        'consultorio' => $consultorio,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

     /**
     * Metodo utilizado para la edición de los consultorios.
     *
     * @Route("/{idConsultorio}/editar", name="consultorio_editar", methods={"GET","POST"})
     */
    public function editar(Request $request, Consultorio $consultorio,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'consultorio');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $form = $this->createForm(ConsultorioType::class, $consultorio);
                    $form->handleRequest($request);

                    if ($form->isSubmitted()){
                        $dqlConsultorio ="SELECT consultorio FROM App:Consultorio consultorio WHERE consultorio.nombre = :nombre and consultorio.idClinica = :idClinica and consultorio.idConsultorio != :idConsultorio";  
                        $queryConsultorio = $entityManager->createQuery($dqlConsultorio)->setParameter('nombre',strtoupper($consultorio->getNombre()))->setParameter('idClinica',$consultorio->getIdClinica()->getIdClinica())->setParameter('idConsultorio',$consultorio->getIdConsultorio());
                        $consultorios= $queryConsultorio->getResult();
                       
                        if (!empty($consultorios)) {
                            $response->setData(array('error'=>'Ya existe un registro con este nombre asignado a esta sede.'));
                            return $response;
                        }else{
                            if($form->isValid()) {
                                $consultorio->getEstado($consultorio->setEstado(true));   
                                $entityManager->persist($consultorio);
                                $entityManager->flush();
                                $response->setData(array('mensaje'=>'Se ha actualizado de forma correcta!'));
                                return $response;
                            }    
                        }                        
                    }
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la eliminación de un consultorio.
     *
     * @Route("/{idConsultorio}", name="consultorio_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Consultorio $consultorio,PerfilGenerator $permisoPerfil): Response
    {
      ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
      ini_set('session.gc_maxlifetime', 1800);
      $session=$request->getSession();
      $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
      if (!isset($login)) { 
        return $this->redirectToRoute('usuario_logout'); 
      } else{
        // La variable $_SESSION['usuario'] es un ejemplo. 
        //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
        if(isset($_SESSION['time'])){ 
          $tiempo = $_SESSION['time']; 
        }else{ 
          $tiempo = strtotime(date("Y-m-d H:i:s"));
        } 
        $inactividad =1800;   //Exprecion en segundos. 
        $actual =  strtotime(date("Y-m-d H:i:s")); 
        $tiempoTranscurrido=($actual-$tiempo);
        if(  $tiempoTranscurrido >= $inactividad){ 
          $session->invalidate();
          return $this->redirectToRoute('usuario_logout');
         // En caso que este sea mayor del tiempo seteado lo deslogea. 
        }else{ 
          $_SESSION['time'] =$actual; 
          $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
          $permisos=$permisoPerfil->verificacionPermisos($perfil,'consultorio');  
          if (sizeof($permisos) > 0) {
            try{
              if ($this->isCsrfTokenValid('delete'.$consultorio->getIdConsultorio(), $request->request->get('_token'))) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $entityManager->remove($consultorio);
                  $entityManager->flush();
                  $successMessage= 'Se ha eliminado de forma correcta!';
                  $this->addFlash('mensaje',$successMessage);
                  return $this->redirectToRoute('consultorio_index');
              }
            }catch (\Doctrine\DBAL\DBALException $e) {
              if ($e->getCode() == 0){
                  if ($e->getPrevious()->getCode() == 23503){
                      $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                      $this->addFlash('error',$successMessage1);
                      return $this->redirectToRoute('consultorio_index');
                  }else{
                      throw $e;
                  }
              }else{
                  throw $e;
              }
            } 
            return $this->redirectToRoute('consultorio_index');
          }else{
            $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
            return $this->redirectToRoute('panel_principal');
          }    
        }
      }      
    }
}
