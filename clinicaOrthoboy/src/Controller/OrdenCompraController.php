<?php

namespace App\Controller;

use App\Entity\ElementoOrdenCompra;
use App\Form\ElementoOrdenCompraType;
use App\Entity\OrdenCompra;
use App\Form\OrdenCompraType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Controlador de orden compra utilizado para el manejo de las compras que lleguena necesitar las sedes, llevando la orden a probada hasta el proveedor al que se le 
 * asigne dicha orden.
 *
 * @Route("/ordenCompra")
 */
class OrdenCompraController extends Controller
{
    /**
    * Metodo utilizado para la generación de un pdf  sobre los elementos que tiene en el momento expuestos a una orden de compra.
    *
    */
    public function pdfOrdenCompra(OrdenCompra $ordenCompra){
        $entityManager = $this->getDoctrine()->getManager();
                    //Consulto a base de datos        
        $dqlElementoOrdenCompra ="SELECT eOrdenCompra FROM App:ElementoOrdenCompra eOrdenCompra WHERE eOrdenCompra.idOrdenCompra= :idOrdenCompra ORDER BY eOrdenCompra.idElementoOrdenCompra ASC";  
        $queryElementoOrdenCompra= $entityManager->createQuery($dqlElementoOrdenCompra)->setParameter('idOrdenCompra',$ordenCompra->getIdOrdenCompra());
        $elementoOrdenCompras= $queryElementoOrdenCompra->getResult();
        foreach ($elementoOrdenCompras as $seleccionElementos) {
            $idRequerimiento=$seleccionElementos->getIdElementoRequerimiento()->getIdRequerimiento()->getIdRequerimiento();
        }

        $html =$this->renderView('ordenCompra/pdfOrdenCompra.html.twig',
        array(
        'ordenCompra' => $ordenCompra,
        'elementoOrdenCompras' => $elementoOrdenCompras,
        'idRequerimiento'=>$idRequerimiento
        ));
         
        $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
        [   'encoding' => 'UTF-8',
            'images' => false,
            'enable-javascript' => true,
            'print-media-type' => true,
            'outline-depth' => 8,
            'orientation' => 'Portrait',
            'header-right'=>'Pag. [page] de [toPage]',
            'page-size' => 'A4',
            'viewport-size' => '1280x1024',
            'margin-left' => '10mm',
            'margin-right' => '10mm',
            'margin-top' => '30mm',
            'margin-bottom' => '25mm',
            'user-style-sheet'=> 'light/dist/assets/css/bootstrap.min.css',
            //'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
            //'background'     => 'light/dist/assets/images/users/user-1.jpg',
            //--javascript-delay 3000
        ]);
        return $pdf;
    }

    
    /**
     * Metodo utilizado para la generación de un pdf  sobre una orden de Compra con todos los elementos requeridos.
     *
     * @Route("/{idOrdenCompra}/pdf", name="ordenCompra_pdf", methods={"GET","POST"})
     */
    public function pdf(OrdenCompra $ordenCompra,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{                 
                $entityManager=$this->getDoctrine()->getManager();
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes compra');  
                if (sizeof($permisos) > 0) {
                    $dqlElementoOrdenCompra ="SELECT eOrdenCompra FROM App:ElementoOrdenCompra eOrdenCompra WHERE eOrdenCompra.idOrdenCompra= :idOrdenCompra ORDER BY eOrdenCompra.idElementoOrdenCompra ASC";  
                    $queryElementoOrdenCompra= $entityManager->createQuery($dqlElementoOrdenCompra)->setParameter('idOrdenCompra',$ordenCompra->getIdOrdenCompra());
                    $elementoOrdenCompras= $queryElementoOrdenCompra->getResult();
                    if (sizeof($elementoOrdenCompras) > 0) {
                        $filename="OrdenCompra_N°_".$ordenCompra->getIdOrdenCompra()."_fecha_". date("Y_m_d") .".pdf";
                        $response = new Response ($this->pdfOrdenCompra($ordenCompra),
                        200,
                        array('Content-Type' => 'aplicattion/pdf',
                              'Content-Disposition' => 'inline; filename="'.$filename.'"'
                        ,));
                        return $response;
                    }else{
                        $this->addFlash('error','No tiene una orden para descargar, usted no cuenta con elementos a solicitar.');
                        return $this->redirectToRoute('ordenCompra_index');
                    }
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para el envio de un correo electronico el cual adjunta un pdf y expone el cuerpo del correo dentro de una vista. Visualizando 
     * los elementos que se requieren dentro de la orden de compra.
     *
     * @Route("/{idOrdenCompra}/correo", name="ordenCompra_correo", methods={"GET","POST"})
     */
    public function correoOrdenCompra(OrdenCompra $ordenCompra,Request $request,PerfilGenerator $permisoPerfil, \Swift_Mailer $mailer): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $entityManager=$this->getDoctrine()->getManager();
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes compra');  
                if (sizeof($permisos) > 0) {
                    $dqlElementoOrdenCompra ="SELECT eOrdenCompra FROM App:ElementoOrdenCompra eOrdenCompra WHERE eOrdenCompra.idOrdenCompra= :idOrdenCompra ORDER BY eOrdenCompra.idElementoOrdenCompra ASC";  
                    $queryElementoOrdenCompra= $entityManager->createQuery($dqlElementoOrdenCompra)->setParameter('idOrdenCompra',$ordenCompra->getIdOrdenCompra());
                    $elementoOrdenCompras= $queryElementoOrdenCompra->getResult();
                    foreach ($elementoOrdenCompras as $seleccionElementos) {
                        $idRequerimiento=$seleccionElementos->getIdElementoRequerimiento()->getIdRequerimiento()->getIdRequerimiento();
                    }
                    if (sizeof($elementoOrdenCompras) > 0) {
                        $filename="OrdenCompra_N°_".$ordenCompra->getIdOrdenCompra()."_fecha_". date("Y_m_d") .".pdf";

                        $attach = new \Swift_Attachment(
                        $this->pdfOrdenCompra($ordenCompra), 
                        $filename, 
                        'application/pdf');

                        
                        $correoOrdenCompra= $this->renderView('ordenCompra/correoOrdenCompra.html.twig', array(
                          'ordenCompra' => $ordenCompra,
                          'elementoOrdenCompras'=>$elementoOrdenCompras,
                          'idRequerimiento'=>$idRequerimiento
                        ));
                        /** Consulta para conocer el correo del super administrador**/
                        $dqlUsuarios ="SELECT usuario FROM App:Usuario usuario INNER JOIN App:Perfil perfil WITH perfil.idPerfil=usuario.idPerfil WHERE perfil.nombre = :nombrePerfil";  
                        $queryUsuarios = $entityManager->createQuery($dqlUsuarios)->setParameter('nombrePerfil','ROLE_SUPERADMINISTRADOR');
                        $usuarios= $queryUsuarios->getResult();
                        foreach ($usuarios as $seleccionUsuario) {
                            $emailUsuarioAdministrador=$seleccionUsuario->getEmail();
                            $clinicaNombre=$seleccionUsuario->getIdSucursal();
                        }
                        $emailUsuarioSesion=$this->get('security.token_storage')->getToken()->getUser()->getEmail();
                        $emailProveedor=$ordenCompra->getIdProveedor()->getEmail();
                        $para=array($emailUsuarioAdministrador,$emailUsuarioSesion,$emailProveedor);
                        $message = (new \Swift_Message())
                        ->setSubject('Orden Compra N° '.$ordenCompra->getIdOrdenCompra().' - Clinica: '.$clinicaNombre)
                        ->setFrom('anamileidy.rodriguez@upt.edu.co')
                        ->setTo($para)  
                        ->setBody($correoOrdenCompra, 'text/html')
                        ->attach($attach); ;                
                        try {
                            $mailer->send($message);  
                            $entityManager = $this->getDoctrine()->getManager();
                            /*Modificación de estado de orden compra */
                            $dqlModificarEstadoOrdenCompra="UPDATE App:OrdenCompra ordenCompra SET ordenCompra.estadoEnvioCorreo= :estado WHERE ordenCompra.idOrdenCompra= :idOrdenCompra";
                            $queryModificarEstadoOrdenCompra=$entityManager->createQuery($dqlModificarEstadoOrdenCompra)->setParameter('estado', TRUE)->setParameter('idOrdenCompra', $ordenCompra->getIdOrdenCompra());
                            $modifEstadoOrdenCompra=$queryModificarEstadoOrdenCompra->execute();

                            $this->addFlash('mensaje', 'Se ha realizado el envio del correo electrónico de forma correcta!'); 
                        } catch (\Swift_TransportException  $e) {
                            $this->addFlash('error','No se pudo establecer comunicación con el servidor intente nuevamente, para enviar información vía electrónica. Comuniquese con el administrador de la aplicación.');
                        }                    
                        return $this->redirectToRoute('ordenCompra_index');
                    }else{
                        $this->addFlash('error','No puede enviar e-mail a proveedor, la orden no cuenta con elementos a solicitar.');
                        return $this->redirectToRoute('ordenCompra_index');
                    }
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }


    /**
     * Metodo utilizado para la eliminación de un elemento de una orden de compra y modificación de un elemento requerido a estado de rechazado.
     *
     * @Route("/{idElementoOrdenCompra}", name="elementoOrdenCompra_delete", methods={"DELETE"})
     */
    public function deleteElementoOrdenCompra(Request $request, ElementoOrdenCompra $elementoOrdenCompra,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes compra');  
              if (sizeof($permisos) > 0) {
                //var_dump($estadoMantenimiento->getIdEstadoMantenimiento());
                try{
                    $idOrdenCompra=$elementoOrdenCompra->getIdOrdenCompra()->getIdOrdenCompra();
                    $entityManager = $this->getDoctrine()->getManager();
                    /*Modificación de estado de elemento requerido */
                    $dqlModificarEstadoEntradaRequerimiento="UPDATE App:ElementoRequerimiento eRequerimiento SET eRequerimiento.estado= :estado WHERE eRequerimiento.idElementoRequerimiento= :idElementoRequerimiento";
                    $queryModificarEstadoEntradaRequerimiento=$entityManager->createQuery($dqlModificarEstadoEntradaRequerimiento)->setParameter('estado', 'RECHAZADO')->setParameter('idElementoRequerimiento', $elementoOrdenCompra->getIdElementoRequerimiento()->getIdElementoRequerimiento());
                    $modificarEstadoEntradaRequerimiento=$queryModificarEstadoEntradaRequerimiento->execute();


                    $entityManager->remove($elementoOrdenCompra);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('ordenCompra_edit', [
                            'idOrdenCompra' => $idOrdenCompra ,
                        ]);
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('ordenCompra_edit', [
                            'idOrdenCompra' => $idOrdenCompra ,
                            ]);
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('ordenCompra_edit', [
                    'idOrdenCompra' => $idOrdenCompra ,
                 ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }

    /**
     * Metodo utilizado para visualizar todas las ordenes de compra.
     *
     * @Route("/", name="ordenCompra_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $entityManager=$this->getDoctrine()->getManager();
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes compra');  
                if (sizeof($permisos) > 0) {
                    $dqlOrdenCompras ="SELECT oCompra FROM App:OrdenCompra oCompra ORDER BY oCompra.idOrdenCompra DESC";  
                    $queryOrdenCompra = $entityManager->createQuery($dqlOrdenCompras);
                    $ordenCompras= $queryOrdenCompra->getResult();
                    return $this->render('ordenCompra/index.html.twig', [
                        'ordenCompras' => $ordenCompras,
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para visualizar una orden de compra junto con todos los elemntos que tenga ingresados a comprar.
     *
     * @Route("/{idOrdenCompra}", name="ordenCompra_show", methods={"GET","POST"})
     */
    public function show(OrdenCompra $ordenCompra,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $entityManager=$this->getDoctrine()->getManager();
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes compra');  
                if (sizeof($permisos) > 0) {
                    $dqlElementosOrdenCompra ="SELECT oCompra FROM App:ElementoOrdenCompra oCompra WHERE oCompra.idOrdenCompra = :idOrdenCompra ORDER BY oCompra.idOrdenCompra DESC";  
                    $queryElementosOrdenCompra = $entityManager->createQuery($dqlElementosOrdenCompra)->setParameter('idOrdenCompra',$ordenCompra->getIdOrdenCompra());
                    $elementosOrdenCompras= $queryElementosOrdenCompra->getResult();
                    return $this->render('ordenCompra/modalShow.html.twig', [
                        'ordenCompra' => $ordenCompra,
                        'elementosOrdenCompras'=> $elementosOrdenCompras
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la edición de una orden de compra.
     *
     * @Route("/{idOrdenCompra}/edit", name="ordenCompra_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OrdenCompra $ordenCompra,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $entityManager=$this->getDoctrine()->getManager();
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes compra');  
                if (sizeof($permisos) > 0) {
                    $response= new JsonResponse();
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlElementoOrdenCompra ="SELECT eOrdenCompra FROM App:ElementoOrdenCompra eOrdenCompra WHERE eOrdenCompra.idOrdenCompra= :idOrdenCompra ORDER BY eOrdenCompra.idElementoOrdenCompra DESC";  
                    $queryElementoOrdenCompra= $entityManager->createQuery($dqlElementoOrdenCompra)->setParameter('idOrdenCompra',$ordenCompra->getIdOrdenCompra());
                    $elementoOrdenCompras= $queryElementoOrdenCompra->getResult();

                    $form = $this->createForm(OrdenCompraType::class, $ordenCompra);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $ordenCompra->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('ordenCompra_index');
                    }

                    return $this->render('ordenCompra/edit.html.twig', [
                        'ordenCompra' => $ordenCompra,
                        'form' => $form->createView(),
                        'elementoOrdenCompras'=> $elementoOrdenCompras
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la eliminación de una orden de compra.
     *
     * @Route("/{idOrdenCompra}/delete", name="ordenCompra_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OrdenCompra $ordenCompra,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'ordenes compra');  
              if (sizeof($permisos) > 0) {
                //var_dump($estadoMantenimiento->getIdEstadoMantenimiento());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($ordenCompra);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('ordenCompra_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('ordenCompra_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('ordenCompra_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
    }


    
}
