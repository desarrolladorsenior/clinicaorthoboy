<?php

namespace App\Controller;

use App\Entity\Paciente;
use App\Form\PacienteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\PerfilGenerator;

/**
 * Controlador de paciente utilizado para el manejo de los datos de todos los pacientes que se encuentran en la clinica.
 *
 * @Route("/paciente")
 */
class PacienteController extends Controller
{
    /**
    * Metodo utilizado para calcular la edad del paciente segun la fecha de nacimiento.
    *
    */
    public function calcularEdad($fechaNacimiento){
        

        $fechaActual = date ("Y-m-d");
        // separamos en partes las fechas
        $array_nacimiento = explode ( "-", $fechaNacimiento );
        $array_actual = explode ( "-", $fechaActual );

        $anos =  $array_actual[0] - $array_nacimiento[0]; // calculamos años
        $meses = $array_actual[1] - $array_nacimiento[1]; // calculamos meses
        $dias =  $array_actual[2] - $array_nacimiento[2]; // calculamos días

        //ajuste de posible negativo en $días
        if ($dias < 0)
        {
            --$meses;

            //ahora hay que sumar a $dias los dias que tiene el mes anterior de la fecha actual
            switch ($array_actual[1]) {
                   case 1:     $dias_mes_anterior=31; break;
                   case 2:     $dias_mes_anterior=31; break;
                   case 3: 
                        if (bisiesto($array_actual[0]))
                        {
                            $dias_mes_anterior=29; break;
                        } else {
                            $dias_mes_anterior=28; break;
                        }
                   case 4:     $dias_mes_anterior=31; break;
                   case 5:     $dias_mes_anterior=30; break;
                   case 6:     $dias_mes_anterior=31; break;
                   case 7:     $dias_mes_anterior=30; break;
                   case 8:     $dias_mes_anterior=31; break;
                   case 9:     $dias_mes_anterior=31; break;
                   case 10:     $dias_mes_anterior=30; break;
                   case 11:     $dias_mes_anterior=31; break;
                   case 12:     $dias_mes_anterior=30; break;
            }

            $dias=$dias + $dias_mes_anterior;
        }

        //ajuste de posible negativo en $meses
        if ($meses < 0)
        {
            --$anos;
            $meses=$meses + 12;
        }

        return "$anos años con $meses meses y $dias días";
    }
    
    /*
    * Metodo utilizado para el manejo de los años bisiestos.
    *
    */
    public function bisiesto($anio_actual){
        $bisiesto=false;
        //probamos si el mes de febrero del año actual tiene 29 días
          if (checkdate(2,29,$anio_actual))
          {
            $bisiesto=true;
        }
        return $bisiesto;
    }

    /**
    * Metodo utilizado para la generación de un pdf sobre la información de los datos de un paciente.
    *
    */
    public function datosPaciente(Paciente $paciente){
        $entityManager = $this->getDoctrine()->getManager();
        //Consulto a base de datos        
        /*$dqlPaciente="SELECT paciente FROM App:Paciente paciente WHERE paciente.idPaciente= :idPaciente";  
        $queryPaciente= $entityManager->createQuery($dqlPaciente)->setParameter('idPaciente',$paciente->getIdPaciente());
        $pacientes= $queryPaciente->getResult();
        foreach ($pacientes as $seleccionPacientes) {
            $idRequerimiento=$seleccionPacientes->getIdElementoRequerimiento()->getIdRequerimiento()->getIdRequerimiento();
        }*/

        $html =$this->renderView('paciente/pdfDatosPaciente.html.twig',
        array(
        'paciente' => $paciente,
        'edad'=>$this->calcularEdad($paciente->getFechaNacimiento()->format('Y-m-d'))
        ));
         
        $pdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
        [   'encoding' => 'UTF-8',
            'images' => false,
            'enable-javascript' => true,
            'print-media-type' => true,
            'outline-depth' => 8,
            'orientation' => 'Portrait',
            'header-right'=>'Pag. [page] de [toPage]',
            'page-size' => 'A4',
            'viewport-size' => '1280x1024',
            'margin-left' => '10mm',
            'margin-right' => '10mm',
            'margin-top' => '30mm',
            'margin-bottom' => '25mm',
            'user-style-sheet'=> 'light/dist/assets/css/bootstrap.min.css',
            //'user-style-sheet'=> 'light/dist/assets/css/stylePdf.css',
            //'background'     => 'light/dist/assets/images/users/user-1.jpg',
            //--javascript-delay 3000
        ]);
        return $pdf;
    }

    
    /**
     * Metodo utilizado para la generar un pdf sobre los datos de un paciente.
     *
     * @Route("/{idPaciente}/pdf", name="paciente_pdfDatos", methods={"GET","POST"})
     */
    public function pdfDatosPaciente(Paciente $paciente,Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{                 
                $entityManager=$this->getDoctrine()->getManager();
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pacientes');  
                if (sizeof($permisos) > 0) {
                    /*$dqlElementoOrdenCompra ="SELECT eOrdenCompra FROM App:ElementoOrdenCompra eOrdenCompra WHERE eOrdenCompra.idOrdenCompra= :idOrdenCompra ORDER BY eOrdenCompra.idElementoOrdenCompra ASC";  
                    $queryElementoOrdenCompra= $entityManager->createQuery($dqlElementoOrdenCompra)->setParameter('idOrdenCompra',$ordenCompra->getIdOrdenCompra());
                    $elementoOrdenCompras= $queryElementoOrdenCompra->getResult();
                    if (sizeof($elementoOrdenCompras) > 0) {*/
                        $filename="Paciente_N°_".$paciente->getIdPaciente()."_".$paciente->__toString()."_fecha_". date("Y_m_d") .".pdf";
                        $response = new Response ($this->datosPaciente($paciente),
                        200,
                        array('Content-Type' => 'aplicattion/pdf',
                              'Content-Disposition' => 'inline; filename="'.$filename.'"'
                        ,));
                        return $response;
                    /*}else{
                        $this->addFlash('error','No tiene una orden para descargar, usted no cuenta con elementos a solicitar.');
                        return $this->redirectToRoute('ordenCompra_index');
                    }*/
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para la edición deel estado de un paciente.
     *
     * @Route("/editarEstadoPaciente", name="paciente_editestado", methods={"GET","POST"})
     */
    public function editEstadoPaciente(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pacientes');  
                if (sizeof($permisos) > 0) {
                    $em=$this->getDoctrine()->getManager();
                    $response = new JsonResponse();
                    $id=$request->request->get('id');
                    $estado=$request->request->get('estado');
                    if($estado == 'ACTIVO'){
                        $estado = true;
                    }else{
                        $estado = false;
                    }
                    $dqlModificarEstadopaciente="UPDATE App:Paciente paciente SET paciente.estado= :estado WHERE paciente.idPaciente= :idPaciente";
                    $queryModificarEstadopaciente=$em->createQuery($dqlModificarEstadopaciente)->setParameter('estado', $estado)->setParameter('idPaciente', $id);
                    $modificarEstadopaciente=$queryModificarEstadopaciente->execute();
                    $response->setData('Se ha cambiado el estado de forma correcta!');
                    return $response;
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la visualización de los tipos de ndentificación que se pueden observar segun la edad del paciente.
     *
     * @Route("/tipoIdentificacion", name="paciente_tipoIdentificacion", methods={"GET","POST"})
    */
    public function seleccionIdentificacion(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pacientes');  
                if (sizeof($permisos) > 0) {

                    $response = new JsonResponse();
                    $entityManager = $this->getDoctrine()->getManager();
                    $fechaN =$request->request->get('fechaN');
                    $dateFechaNacimiento = new \DateTime($fechaN);
                    $dateActual= new \DateTime();
                    $diferenciaDias=$dateFechaNacimiento->diff($dateActual);
                    if ($diferenciaDias->days >= 0 and $diferenciaDias->days <= 2556) {
                        $arrayTipoDoc=array(1,2,6);
                        $dqlTipoDocumento = "SELECT tipD FROM App:TipoDocumento tipD where tipD.id not in(:id)";
                        $queryTipoDocumento= $entityManager->createQuery($dqlTipoDocumento)->setParameter('id',$arrayTipoDoc);
                        $tipoDocumento= $queryTipoDocumento->getResult();
                        $arrayTipoDocumento=array();
                        foreach ($tipoDocumento as $seleccionTipoDoc ) {
                            $id=$seleccionTipoDoc->getId();
                            $nomenclaturaDoc=$seleccionTipoDoc->getNomenclatura();
                            $docFinal = $id.";".$nomenclaturaDoc;
                            array_push($arrayTipoDocumento, $docFinal);
                        }
                        $response->setData($arrayTipoDocumento);
                        return $response;
                    }else if ($diferenciaDias->days >= 2557 and $diferenciaDias->days <= 6207) {
                        $arrayTipoDoc=array(1,2,5);
                        $dqlTipoDocumento = "SELECT tipD FROM App:TipoDocumento tipD where tipD.id not in(:id)";
                        $queryTipoDocumento= $entityManager->createQuery($dqlTipoDocumento)->setParameter('id',$arrayTipoDoc);
                        $tipoDocumento= $queryTipoDocumento->getResult();
                        $arrayTipoDocumento=array();
                        foreach ($tipoDocumento as $seleccionTipoDoc ) {
                            $id=$seleccionTipoDoc->getId();
                            $nomenclaturaDoc=$seleccionTipoDoc->getNomenclatura();
                            $docFinal = $id.";".$nomenclaturaDoc;
                            array_push($arrayTipoDocumento, $docFinal);
                        }
                        $response->setData($arrayTipoDocumento);
                        return $response;
                    }else if ($diferenciaDias->days >= 6208) {
                        $arrayTipoDoc=array(3,5,6);
                        $dqlTipoDocumento = "SELECT tipD FROM App:TipoDocumento tipD where tipD.id not in(:id)";
                        $queryTipoDocumento= $entityManager->createQuery($dqlTipoDocumento)->setParameter('id',$arrayTipoDoc);
                        $tipoDocumento= $queryTipoDocumento->getResult();
                        $arrayTipoDocumento=array();
                        foreach ($tipoDocumento as $seleccionTipoDoc ) {
                            $id=$seleccionTipoDoc->getId();
                            $nomenclaturaDoc=$seleccionTipoDoc->getNomenclatura();
                            $docFinal = $id.";".$nomenclaturaDoc;
                            array_push($arrayTipoDocumento, $docFinal);
                        }
                        $response->setData($arrayTipoDocumento);
                        return $response;
                    }else{
                        $dqlTipoDocumento = "SELECT tipD FROM App:TipoDocumento tipD ";
                        $queryTipoDocumento= $entityManager->createQuery($dqlTipoDocumento);
                        $tipoDocumento= $queryTipoDocumento->getResult();
                        $arrayTipoDocumento=array();
                        foreach ($tipoDocumento as $seleccionTipoDoc ) {
                            $id=$seleccionTipoDoc->getId();
                            $nomenclaturaDoc=$seleccionTipoDoc->getNomenclatura();
                            $docFinal = $id.";".$nomenclaturaDoc;
                            array_push($arrayTipoDocumento, $docFinal);
                        }
                        $response->setData($arrayTipoDocumento);
                        return $response;
                    }
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }


    /**
     * Metodo utilizado para la visualización de todos los pacientes.
     *
     * @Route("/", name="paciente_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pacientes');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlPaciente ="SELECT paciente FROM App:Paciente paciente
                    ORDER BY paciente.idPaciente DESC";  
                    $queryPaciente = $entityManager->createQuery($dqlPaciente);
                    $pacientes= $queryPaciente->getResult();
                    /*$pacientes = $this->getDoctrine()
                        ->getRepository(Paciente::class)
                        ->findAll()
                        ->addOrderBy('idPaciente Desc');*/

                    return $this->render('paciente/index.html.twig', [
                        'pacientes' => $pacientes,
                    ]);

                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }        
    }

    /**
     * Metodo utilizado para registrar los datos de un paciente.
     *
     * @Route("/new", name="paciente_new", methods={"GET","POST"})
     */
    public function new(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pacientes');  
                if (sizeof($permisos) > 0) {
                    $paciente = new Paciente();
                    $paciente->setEstado(TRUE);
                    $form = $this->createForm(PacienteType::class, $paciente);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $paciente->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($paciente);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('paciente_edit',['idPaciente' => $paciente->getIdPaciente()]);
                       // $this->addFlash('info', 'Registro creado correctamente!');
                       // return $this->redirectToRoute('paciente_index');
                    }

                    return $this->render('paciente/new.html.twig', [
                        'paciente' => $paciente,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la visualización de los datos de un paciente.
     *
     * @Route("/{idPaciente}", name="paciente_show", methods={"GET","POST"})
     */
    public function show(Request $request, Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pacientes');  
                if (sizeof($permisos) > 0) {
                    return $this->render('paciente/modalShow.html.twig', [
                        'paciente' => $paciente,
                        'edad'=>$this->calcularEdad($paciente->getFechaNacimiento()->format('Y-m-d'))
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la edición de los datos de un paciente.
     *
     * @Route("/{idPaciente}/edit", name="paciente_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'pacientes');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $form = $this->createForm(PacienteType::class, $paciente);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $paciente->setIdUsuarioIngresa($this->get('security.token_storage')->getToken()->getUser());
                        $entityManager->persist($paciente);
                        $entityManager->flush(); 
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('paciente_edit',['idPaciente' => $paciente->getIdPaciente()]);
                    }

                    return $this->render('paciente/edit.html.twig', [
                        'paciente' => $paciente,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }       
    }

    /**
     * Metodo utilizado para la eliminación de un paciente.
     *
     * @Route("/{idPaciente}", name="paciente_delete", methods={"DELETE"})
     */
    public function delete(Request $request, paciente $paciente,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'pacientes');  
              if (sizeof($permisos) > 0) {
                //var_dump($esterilizacion->getIdEsterilizacion());
                try{
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->remove($paciente);
                    $entityManager->flush();
                    $successMessage= 'Se ha eliminado de forma correcta!';
                    $this->addFlash('mensaje',$successMessage);
                    return $this->redirectToRoute('paciente_index');
                    
                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('error',$successMessage1);
                            return $this->redirectToRoute('paciente_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('paciente_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        } 
    }

    
}
