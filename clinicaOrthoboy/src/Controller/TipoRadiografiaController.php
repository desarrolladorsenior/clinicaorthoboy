<?php

namespace App\Controller;
use App\Entity\TipoRadiografia;
use App\Form\TipoRadiografiaType;
use App\Service\PerfilGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de tipo Radiografia utilizado para el manejo delos tipos de radiografias que se pueden ingresar de un paciente.
 *
 * @Route("/tipoRadiografia")
 */
class TipoRadiografiaController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de un tipo Radiografia.
     *
     * @Route("/", name="tipo_radiografia_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'Tipo Radiografia');  
                if (sizeof($permisos) > 0) {
                  $entityManager = $this->getDoctrine()->getManager();
                  $dql ="SELECT tipoRadiografia FROM App:TipoRadiografia tipoRadiografia
                  ORDER BY tipoRadiografia.idTipoRadiografia DESC";  
                  $queryTipoRadiografia = $entityManager->createQuery($dql);
                  $tipoRadiografias= $queryTipoRadiografia->getResult();

                  $tipoRadiografia = new TipoRadiografia();
                  /*Formulario tipo radiografia para crear a nivel modal*/
                  $form = $this->createForm(TipoRadiografiaType::class, $tipoRadiografia);
                  $form->handleRequest($request);

                  $form = $this->createForm(TipoRadiografiaType::class, $tipoRadiografia);
                  $form->handleRequest($request);

                  if ($form->isSubmitted() && $form->isValid()) {
                      $tipoRadiografia->getEstado($tipoRadiografia->setEstado(true));
                          $entityManager = $this->getDoctrine()->getManager();
                          $entityManager->persist($tipoRadiografia);
                          $entityManager->flush();
                          $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                          return $this->redirectToRoute('tipo_radiografia_index');
                  }
                  return $this->render('tipo_radiografia/index.html.twig', [
                      'tipo_radiografias' => $tipoRadiografias,
                      'form' => $form->createView()
                  ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }        
        
    }

     /**
     * Metodo utilizado para el registro de subtipo de tipo Radiografia.
     *
     * @Route("/newSubtipo", name="tipo_radiografia_newSubtipo", methods={"GET","POST"})
     */
    public function newSubtipo(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'Tipo Radiografia');  
              if (sizeof($permisos) > 0) {
                
                $entityManager = $this->getDoctrine()->getManager();
                $tipoRadiografia= new TipoRadiografia();
                $idTipoRadiografia=$request->request->get('idTipoRadiografia');
                $dql ="SELECT tipoRadiografia FROM App:TipoRadiografia tipoRadiografia
                WHERE tipoRadiografia.idTipoRadiografia = :idTipoRadiografia";  
                $queryTipoRadiografia = $entityManager->createQuery($dql)->setParameter('idTipoRadiografia',$idTipoRadiografia);
                $tipoRadiografias= $queryTipoRadiografia->getResult();
                foreach ($tipoRadiografias as $tipoR ) {             
                   $tipoRadiografia->getIdSedeTipoRadiografia($tipoRadiografia->setIdSedeTipoRadiografia($tipoR));
                }
                $form = $this->createForm(TipoRadiografiaType::class, $tipoRadiografia);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) { 
                    $tipoRadiografia->getEstado($tipoRadiografia->setEstado(true));        
                    $entityManager->persist($tipoRadiografia);
                    $entityManager->flush();
                    $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                    return $this->redirectToRoute('tipo_radiografia_index');

                }

                return $this->render('tipo_radiografia/modalNewSubtipo.html.twig', [
                    'tipo_radiografia' => $tipoRadiografia,
                    'formGeneral' => $form->createView(),
                ]);
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }     
        
    }

    /**
     * Metodo utilizado para la edición del estado de un tipo Radiografia y sus subtipos.
     *
     * @Route("/editarestadotipoR", name="tipoRa_editestado", methods={"GET","POST"})
     */
    public function editEstado(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'Tipo Radiografia');  
              if (sizeof($permisos) > 0) {
                $em=$this->getDoctrine()->getManager();
                $response = new JsonResponse();
                $id=$request->request->get('id');
                $estado=$request->request->get('estado');
                if($estado == 'ACTIVO'){
                    $estado = true;
                }else{
                    $estado = false;
                }        
                $dql ="SELECT tipoRadiografia FROM App:TipoRadiografia tipoRadiografia WHERE tipoRadiografia.idTipoRadiografia = :idTipoRadiografia or tipoRadiografia.idSedeTipoRadiografia = :idTipoRadiografia ";  
                $queryTipoRadiografia = $em->createQuery($dql)->setParameter('idTipoRadiografia',$id);
                $tipoRadiografias= $queryTipoRadiografia->getResult();
                $arrayIDS=array();
                foreach ($tipoRadiografias as $tipoR ) {            
                   $ids= $tipoR->getIdTipoRadiografia();
                   array_push($arrayIDS, $ids);
                }  
                $dqlModificarEstado="UPDATE App:TipoRadiografia tipoR SET tipoR.estado= :estado WHERE tipoR.idTipoRadiografia IN(:idTipoRadiografias) ";
                $queryModificarEstado=$em->createQuery($dqlModificarEstado)->setParameter('estado', $estado)->setParameter('idTipoRadiografias', $arrayIDS);
                $modificarEstado=$queryModificarEstado->execute();
                $response->setData('Se ha cambiado el estado de forma correcta!');
                return $response;
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }   
    }

    /**
     * Metodo utilizado para la edición de un tipo Radiografia.
     *
     * @Route("/{idTipoRadiografia}/edit", name="tipo_radiografia_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TipoRadiografia $tipoRadiografium,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'Tipo Radiografia');  
              if (sizeof($permisos) > 0) {
                $entityManager = $this->getDoctrine()->getManager();
                $tipo=$request->request->get('tipo');
                $form = $this->createForm(TipoRadiografiaType::class, $tipoRadiografium);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager->persist($tipoRadiografium);
                    $entityManager->flush($tipoRadiografium);
                    $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                    return $this->redirectToRoute('tipo_radiografia_index');
                }
                if ($tipo == 'principal') {
                    return $this->render('tipo_radiografia/modalEditPrincipal.html.twig', [
                    'tipo_radiografium' => $tipoRadiografium,
                    'formGeneral' => $form->createView(),
                    ]);
                }else{
                   return $this->render('tipo_radiografia/modalEdit.html.twig', [
                    'tipo_radiografium' => $tipoRadiografium,
                    'formGeneral' => $form->createView(),
                    ]); 
                }  
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }        
            }
        } 
           
    }

    /**
     * Metodo utilizado para la eliminación de un tipo Radiografia.
     *
     * @Route("/{idTipoRadiografia}", name="tipo_radiografia_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TipoRadiografia $tipoRadiografium,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
          ini_set('session.gc_maxlifetime', 1800);
          $session=$request->getSession();
          $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
          if (!isset($login)) { 
            return $this->redirectToRoute('usuario_logout'); 
          } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'Tipo Radiografia');  
              if (sizeof($permisos) > 0) {
                try{

                    if ($this->isCsrfTokenValid('delete'.$tipoRadiografium->getIdTipoRadiografia(), $request->request->get('_token'))) {
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->remove($tipoRadiografium);
                        $entityManager->flush();
                        $successMessage= 'Se ha eliminado de forma correcta!';
                        $this->addFlash('mensaje',$successMessage);
                        return $this->redirectToRoute('tipo_radiografia_index');
                    }

                }catch (\Doctrine\DBAL\DBALException $e) {
                    if ($e->getCode() == 0){
                        if ($e->getPrevious()->getCode() == 23503){
                            $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                            $this->addFlash('errorTPR',$successMessage1);
                            return $this->redirectToRoute('tipo_radiografia_index');
                        }else{
                            throw $e;
                        }
                    }else{
                        throw $e;
                    }
                } 
                return $this->redirectToRoute('tipo_radiografia_index');
              }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }     
            }
        }        
        
    }
}
