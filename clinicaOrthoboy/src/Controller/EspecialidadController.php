<?php

namespace App\Controller;

use App\Entity\Especialidad;
use App\Form\EspecialidadType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PerfilGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controlador de especialidad utilizado para el manejo de la profesiones de los usuarios del sistema.
 *
 * @Route("/especialidad")
 */
class EspecialidadController extends AbstractController
{
    /**
     * Metodo utilizado para la visualización de una especialización y registro del mismo.
     *
     * @Route("/", name="especialidad_index", methods={"GET","POST"})
     */
    public function index(Request $request,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'especilidades');  
                if (sizeof($permisos) > 0) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $dqlEspecialidads ="SELECT especialidad FROM App:Especialidad especialidad
                    ORDER BY especialidad.idEspecialidad DESC";  
                    $queryEspecialidads = $entityManager->createQuery($dqlEspecialidads);
                    $especialidades= $queryEspecialidads->getResult();

                    $especialidad = new Especialidad();
                    $form = $this->createForm(EspecialidadType::class, $especialidad);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $entityManager->persist($especialidad);
                        $entityManager->flush();
                        $this->addFlash('mensaje', 'Se ha realizado el registro de forma correcta!');
                        return $this->redirectToRoute('especialidad_index');
                    }

                    return $this->render('especialidad/index.html.twig', [
                        'especialidades' => $especialidades,
                        'especialidad' => $especialidad,
                        'form' => $form->createView(),
                    ]);
                }else{
                $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                return $this->redirectToRoute('panel_principal');
              }    
            }
        }      
    }

    /**
     * Metodo utilizado para la visualización de uns especialidad sin embargo no se esta utilizando.
     *
     * @Route("/{idEspecialidad}", name="especialidad_show", methods={"GET"})
     */
    public function show(Especialidad $especialidad): Response
    {
        return $this->render('especialidad/show.html.twig', [
            'especialidad' => $especialidad,
        ]);
    }

    /**
     * Metodo utilizado para la edición de una eps.
     *
     * @Route("/{idEspecialidad}/edit", name="especialidad_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Especialidad $especialidad,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
              $_SESSION['time'] =$actual; 
              $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
              $permisos=$permisoPerfil->verificacionPermisos($perfil,'especilidades');  
                if (sizeof($permisos) > 0) {
                    $form = $this->createForm(EspecialidadType::class, $especialidad);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('mensaje', 'Se ha actualizado de forma correcta!');
                        return $this->redirectToRoute('especialidad_index');
                    }
                    return $this->render('especialidad/modalEdit.html.twig', [
                        'especialidad' => $especialidad,
                        'form' => $form->createView(),
                    ]);
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }

    /**
     * Metodo utilizado para la eliminación de una especialización o profesión.
     *
     * @Route("/{idEspecialidad}", name="especialidad_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Especialidad $especialidad,PerfilGenerator $permisoPerfil): Response
    {
        ini_set('session.cookie_lifetime',1800);  //El tiempo viene dado en segundos
        ini_set('session.gc_maxlifetime', 1800);
        $session=$request->getSession();
        $login=$this->get('security.token_storage')->getToken()->getUser()->getUserName();
        if (!isset($login)) { 
           return $this->redirectToRoute('usuario_logout'); 
        } else{
            // La variable $_SESSION['usuario'] es un ejemplo. 
            //Verifico el tiempo si esta seteado, caso contrario lo seteo. 
            if(isset($_SESSION['time'])){ 
              $tiempo = $_SESSION['time']; 
            }else{ 
              $tiempo = strtotime(date("Y-m-d H:i:s"));
            } 
            $inactividad =1800;   //Exprecion en segundos. 
            $actual =  strtotime(date("Y-m-d H:i:s")); 
            $tiempoTranscurrido=($actual-$tiempo);
            if(  $tiempoTranscurrido >= $inactividad){ 
              $session->invalidate();
              return $this->redirectToRoute('usuario_logout');
             // En caso que este sea mayor del tiempo seteado lo deslogea. 
            }else{ 
                $_SESSION['time'] =$actual; 
                $perfil=$this->get('security.token_storage')->getToken()->getUser()->getIdPerfil();
                $permisos=$permisoPerfil->verificacionPermisos($perfil,'especilidades');  
                if (sizeof($permisos) > 0) {
                    try{
                      if ($this->isCsrfTokenValid('delete'.$especialidad->getIdEspecialidad(), $request->request->get('_token'))) {
                          $entityManager = $this->getDoctrine()->getManager();
                          $entityManager->remove($especialidad);
                          $entityManager->flush();
                          $successMessage= 'Se ha eliminado de forma correcta!';
                          $this->addFlash('mensaje',$successMessage);
                          return $this->redirectToRoute('especialidad_index');
                      }
                    }catch (\Doctrine\DBAL\DBALException $e) {
                      if ($e->getCode() == 0){
                          if ($e->getPrevious()->getCode() == 23503){
                              $successMessage1= '¡..Error al eliminar el registro. Contiene vinculos con otros procesos...!';
                              $this->addFlash('error',$successMessage1);
                              return $this->redirectToRoute('especialidad_index');
                          }else{
                              throw $e;
                          }
                      }else{
                          throw $e;
                      }
                    } 
                    return $this->redirectToRoute('especialidad_index');
                }else{
                    $this->addFlash('error', 'Este recurso no es permitido para su perfil!');
                    return $this->redirectToRoute('panel_principal');
                }    
            }
        }      
    }
}
