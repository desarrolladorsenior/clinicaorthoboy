<?php

namespace App\Form;

use App\Entity\TipoConsentimiento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TipoConsentimientoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('contenido',TextareaType::class,array('attr' => array('class' => 'tinymce', 'required' => ''),))
            ->add('riesgo',TextareaType::class,array('attr' => array('class' => 'tinymce','required' => ''),))
            ->add('idCie',EntityType::class,array(
                'placeholder' => 'Seleccione un diagnóstico radiografía','class'=>'App:CieRip',
                'attr'=>array('required'=>'required')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TipoConsentimiento::class,
        ]);
    }
}
