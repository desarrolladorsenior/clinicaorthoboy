<?php

namespace App\Form;

use App\Entity\CieRip;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CieRipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = array('role1', 'role2', 'role3');
        $builder
            ->add('codigo',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('nombreCieRip',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('desripcion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true'),))
            ->add('sexo',ChoiceType::class,array(
                'required' => true,
                'choices' => array(
                    'Femenino'=>"FEMENINO  ",
                    'Masculino'=>"MASCULINO ",
                    'Ambos'=>"AMBOS     "),
                'multiple'=>FALSE,'expanded'=>TRUE))                
            ->add('limiteSuperior',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('limiteInferior',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CieRip::class,
        ]);
    }
}
