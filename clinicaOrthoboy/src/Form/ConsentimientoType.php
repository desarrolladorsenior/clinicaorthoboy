<?php

namespace App\Form;

use App\Entity\Consentimiento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class ConsentimientoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firmaPaciente',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('huellaPaciente',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('nombreAcudiente',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('cedularAcudiente',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('parentesco',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('cedularResponsable',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('idUsuario',EntityType::class, array(
                'placeholder' => 'Seleccione un usuario',
                'class' => 'App:Usuario',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione usuario.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idPaciente',EntityType::class, array(
                'placeholder' => 'Seleccione paciente',
                'class' => 'App:Paciente',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione paciente.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idTipoConsentimiento',EntityType::class, array(
                'placeholder' => 'Seleccione tipo consentimiento',
                'class' => 'App:TipoConsentimiento',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione tipo consentimiento.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Consentimiento::class,
        ]);
    }
}
