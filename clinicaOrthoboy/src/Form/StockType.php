<?php

namespace App\Form;

use App\Entity\Stock;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Doctrine\ORM\EntityRepository;

class StockType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cantidad',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true','required'=>'required'),))
            ->add('estado')
            ->add('idNombreInsumo',EntityType::class,array(
                'placeholder' => 'Seleccione nombre insumo','class'=>'App:NombreInsumo',
                'attr'=>array('required'=>'required'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))           
            ->add('idClinica',EntityType::class,array(
                'placeholder' => 'Seleccione una sede','class'=>'App:Clinica',
                'attr'=>array('required'=>'required'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estadoSede = :estado')
                    ->andWhere('e.idSedeClinica is not null')
                    ->setParameter('estado',true);
                },
            ))           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stock::class,
        ]);
    }
}
