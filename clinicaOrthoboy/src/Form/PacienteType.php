<?php

namespace App\Form;

use App\Entity\Paciente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
class PacienteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numeroIdentificacion',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),))

            ->add('expedicionDocumento',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),))

            ->add('nombres',TextType::class, array('attr' => array('class' => 'tinymce'),))

            ->add('primerApellido',TextType::class, array('attr' => array('class' => 'tinymce'),))

            ->add('segundoApellido',TextType::class, array('attr' => array('class' => 'tinymce'),))

            ->add('fechaNacimiento',DateType::class,array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'nacimiento','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),
            ))        
            
            ->add('lugarNacimiento',TextType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('estadoCivil', ChoiceType::class, [
            'choices'  => [
                'Solter@' => 'SOLTERO',
                'Comprometid@' => 'COMPROMETIDO',
                'En Relación ( más de 1 Año de noviazgo)' => 'EN_RELACION',
                'Casad@' => 'CASADO',
                'Unión libre' => 'UNION_LIBRE',
                'Separad@' => 'SEPARADO',
                'Divorsiad@' => 'DIVORSIADO',
                'Viud@' => 'VIUDO',
            ],'placeholder' => 'Seleccione estado civil'
            ])

            ->add('sexo', ChoiceType::class, [
            'choices'  => [
                'Masculino' => 'MASCULINO',
                'Femenino' => 'FEMENINO',
                'Ambos' => 'AMBOS',
            ],'placeholder' => 'Seleccione sexo'
            ])

            ->add('email',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),))
            ->add('dirrecionDomicilio',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),))
            ->add('telefonoDomicilio',TextType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('celular',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('nombrePResponsable',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),))
            ->add('telefonoPersonaResponsable',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),))
            ->add('parentesco',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),))
            ->add('ocupacion',TextType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('regimen', EntityType::class,array(
                'placeholder' => 'Seleccione régimen',
                'class' => 'App:TipoUsuario',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Pendiente',
                    'data-placement'=>'right',
                    'data-html'=>'true'
            )))
            ->add('nombreAcompanante',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),))
            ->add('movilAcompanante',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'bottom','data-html'=>'true'),))
            ->add('idMunicipio',EntityType::class, array(
                'placeholder' => 'Seleccione municipio',
                'class' => 'App:Municipio',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Pendiente.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
            ->add('idEps',EntityType::class, array(
                'placeholder' => 'Seleccione eps',
                'class' => 'App:Eps',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Pendiente',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado', TRUE);
                },
            ))
            ->add('tipoIdentificacion',EntityType::class, array(
                'placeholder' => 'Seleccione un tipo documento',
                'class' => 'App:TipoDocumento',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Pendiente.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Paciente::class,
        ]);
    }
}
