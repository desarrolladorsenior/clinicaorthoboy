<?php

namespace App\Form;

use App\Entity\Requerimiento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class RequerimientoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idSedeClinica',EntityType::class,array(
            'placeholder' => 'Seleccione ubicación','class'=>'App:Clinica',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estadoSede = :estado')
                    ->andWhere('e.idSedeClinica is not null')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('idUsuarioIngresa',EntityType::class,array(
            'placeholder' => 'Seleccione usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('estado')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Requerimiento::class,
        ]);
    }
}
