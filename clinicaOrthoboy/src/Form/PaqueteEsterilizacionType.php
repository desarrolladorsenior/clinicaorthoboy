<?php

namespace App\Form;

use App\Entity\PaqueteEsterilizacion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityRepository;

class PaqueteEsterilizacionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codigo',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('estado')
            ->add('idTipoPaquete',EntityType::class,array(
            'placeholder' => 'Seleccione tipo paquete','class'=>'App:TipoPaquete',
            'attr'=>array('required'=>'required',
                'data-toggle'=>"tooltip" ,
                'title'=> 'Pendiente.',
                'data-placement'=>'right',
                'data-html'=>'true')
            ))
            ->add('idEsterilizacion',EntityType::class,array(
            'placeholder' => 'Seleccione esterilización','class'=>'App:Esterilizacion',
            'attr'=>array('required'=>'required')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PaqueteEsterilizacion::class,
        ]);
    }
}
