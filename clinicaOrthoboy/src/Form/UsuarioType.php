<?php

namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombres',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el campo permite mínimo 3 y máximo 150 caracteres.','data-placement'=>'right','data-html'=>'true'),))
            ->add('apellidos',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el campo permite mínimo 3 y máximo 150 caracteres.','data-placement'=>'right','data-html'=>'true'),))
            ->add('numeroIdentificacion',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el campo permite mínimo 3 y máximo 150 caracteres.','data-placement'=>'right','data-html'=>'true'),))
            ->add('fechaNacimiento',DateType::class, array(
                'attr' => array(
                    'class' => 'nacimiento','data-toggle'=>"tooltip",
                    'title'=> 'Recuerde que la fecha debe ser diferente a la fecha actual.',
                    'data-placement'=>'right',
                    'data-html'=>'true'),
                'widget' => 'single_text',
            ))
            ->add('email',EmailType::class, array('label' => false,'attr'=>array('data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el email permite mínimo 10 y máximo 75 caracteres. El formato es: “ejemplo@mail.com”.','data-placement'=>'right','data-color'=> 'black', 'data-html'=>'true')))
            ->add('username')
            ->add('genero',ChoiceType::class, array('placeholder' => 'Seleccione genero', 
                'choices' => array('M'=> 'MASCULINO', 'F'=> 'FEMENINO','N/A'=> 'NO ESPECIFICA'),'attr'=>array('data-toggle'=>"tooltip" ,'title'=> 'Seleccione un genero.','data-placement'=>'right','data-html'=>'true')))
            ->add('colorAgenda',ColorType::class, array('attr' => array('title'=>'Seleccione el color que identificara al usuario en la agenda de trabajo.','required'=>true ),)) 
            ->add('direccion',TextType::class, array('attr'=>array('data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el campo permite mínimo 3 y máximo 75 caracteres. Formato: "Calle 2 N° 20-32".','data-placement'=>'right','data-color'=> 'black', 'data-html'=>'true')))
            ->add('numeroContacto',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el campo permite mínimo 10 y máximo 15 caracteres.','data-placement'=>'right','data-html'=>'true'),))
            ->add('nombreContacto',TextType::class, array('attr'=>array('data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el campo permite mínimo 3 y máximo 75 caracteres. Formato: "Calle 2 N° 20-32".','data-placement'=>'right','data-color'=> 'black', 'data-html'=>'true')))
            ->add('movil',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Recuerde que el campo permite mínimo 10 y máximo 15 caracteres.','data-placement'=>'right','data-html'=>'true'),))
            ->add('firmaProfesional')
            ->add('imagenUsuario')
            ->add('estado')
            ->add('tipoentificacion',EntityType::class, array(
                'placeholder' => 'Seleccione un tipo documento',
                'class' => 'App:TipoDocumento',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione un tipo documento.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
            ->add('idEspecialidad',EntityType::class, array(
                'placeholder' => 'Seleccione una especialidad',
                'class' => 'App:Especialidad',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione una especialidad.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
            ->add('idAcuerdoPorc',EntityType::class, array(
                'placeholder' => 'Seleccione un acuerdo pago (%)',
                'class' => 'App:AcuerdoPago',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione acuerdo pago (%)',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.valor is null')
                    ->orderBy('e.idAcuerdoPago', 'DESC');
                },
            ))
            ->add('idAcuerdoValor',EntityType::class, array(
                'placeholder' => 'Seleccione acuerdo pago ($)',
                'class' => 'App:AcuerdoPago',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione acuerdo pago ($)',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.porcentaje is null')
                    ->orderBy('e.idAcuerdoPago', 'DESC');
                },
            ))
            ->add('idAgendaUsuario',EntityType::class, array(
                'placeholder' => 'Seleccione una agenda',
                'class' => 'App:AgendaUsuario',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione una agenda.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                )
            ))
            ->add('idPerfil',EntityType::class, array(
                'placeholder' => 'Seleccione un perfil',
                'class' => 'App:Perfil',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione un perfil.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->andWhere('e.idPerfil != :idPerfil')
                    ->orderBy('e.idPerfil', 'DESC')
                    ->setParameter('estado',TRUE)
                    ->setParameter('idPerfil',1);
                }
            ))
            ->add('idSucursal',EntityType::class, array(
                'placeholder' => 'Seleccione una sede',
                'class' => 'App:Clinica',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione una sede.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                )
            ))
            ->add('tipoUsuario', ChoiceType::class, array(
                'choices' => array(
                    'Interno' => 'INTERNO',
                    'Externo' => 'EXTERNO',
                ),
                'required'    => false,
                'placeholder' => 'Seleccione el tipo de usuario',
                'empty_data'  => null,
                'attr'=>array('data-toggle'=>"tooltip" ,'title'=> 'Seleccione el tipo de usuario a registrar.','data-placement'=>'right','data-color'=> 'black', 'data-html'=>'true')
             ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}
