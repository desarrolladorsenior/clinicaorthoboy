<?php

namespace App\Form;

use App\Entity\Pedido;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PedidoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numeroFactura',TextType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('fechaFactura',DateType::class,array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'js-datepicker'),
            ))        
            ->add('estado')
            ->add('idProveedor',EntityType::class,array(
            'placeholder' => 'Seleccione proveedor','class'=>'App:Proveedor',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('idUsuarioIngresa',EntityType::class,array(
            'placeholder' => 'Seleccione usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('imagen', FileType::class,array('required'=> false,'attr' =>array('accept'=> 'image/png,image/jpg,image/jpeg,image/vmp,image/tif,image/tiff,.pdf,.xml,.xsl,.xlsx,.doc,.docx,.txt,.odt,.ppt,.pptx,.ods,.zip,.tar,.rar,.mp4,.mpeg,.war,.au','data-toggle'=>"tooltip",'data-placement'=>'right', 'data-html'=>'true'),'data_class' => null, 'property_path' => 'imagen'))

            ->add('idOrdenCompra',EntityType::class,array(
            'placeholder' => 'Seleccione orden compra','class'=>'App:OrdenCompra',
            'attr'=>array('required'=> false),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estadoEnvioCorreo = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pedido::class,
        ]);
    }
}
