<?php

namespace App\Form;

use App\Entity\CieDiagnosticoTipo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CieDiagnosticoTipoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $arrayIconos=['fas fa-circle','fas fa-circle-notch','fas fa-hospital-alt','fas fa-square-full','fas fa-exclamation-triangle','fas fa-ambulance','fas fa-times','fas fa-ban','fas fa-band-aid','fas fa-bars','fas fa-blind','fas fa-bomb','fas fa-briefcase-medical','fas fa-capsules','fas fa-burn','fas fa-check','fas fa-check-circle','fas fa-compact-disc','fas fa-crop','fas fa-crosshairs','fas fa-diagnoses','fas fa-dot-circle','fas fa-exclamation-circle','fas fa-feather','fas fa-fire','fas fa-gem','fas fa-genderless','fas fa-i-cursor','fas fa-magic','fas fa-long-arrow-alt-up','fas fa-not-equal','fas fa-notes-medical','fas fa-paint-brush','fas fa-people-carry','fas fa-procedures','fas fa-quidditch','fas fa-screwdriver','fas fa-stethoscope','fas fa-star','fas fa-star-half','fas fa-stop-circle','fas fa-syringe','fas fa-thermometer','fas fa-thermometer-empty','fas fa-times-circle','fas fa-unlink','fas fa-user-md','fas fa-x-ray','far fa-circle','far fa-clipboard','far fa-compass','far fa-dot-circle','far fa-heart','far fa-square','far fa-star','far fa-star-half','far fa-stop-circle','far fa-times-circle','fab fa-fulcrum','fab fa-asymmetrik','fab fa-autoprefixer','fab fa-bity','fab fa-centercode','fab fa-chrome','fab fa-connectdevelop','fab fa-digital-ocean','fab fa-discourse','fab fa-empire','fab fa-envira','fab fa-ethereum','fab fa-gratipay','fab fa-medrt','fab fa-quinscape','fab fa-ravelry','fab fa-react','fab fa-schlix','fab fa-scribd','fab fa-superpowers','fab fa-themeisle','fab fa-yelp','fab fa-wpexplorer'];
        $builder
            ->add('simbolo', ChoiceType::class, [
                'placeholder' => 'Seleccione un icono.',
                'choices' => [$arrayIconos]])
            ->add('color',ColorType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('estado')
            ->add('idCieRip',EntityType::class, array(
                'placeholder' => 'Seleccione un cie (enfermedad)',
                'class' => 'App:CieRip',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione una opción.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                )))
            ->add('idDiagnosticoOdontologico',EntityType::class, array(
                'placeholder' => 'Seleccione un diagnóstico odontológico',
                'class' => 'App:DiagnosticoOdontologico',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione una opción.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idTipoDiagnostico',EntityType::class, array(
                'placeholder' => 'Seleccione un categoría',
                'class' => 'App:TipoDiagnostico',
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.idTipoDiagnostico = :idUno')
                    ->orWhere('e.idTipoDiagnostico = :idDos')
                    ->setParameter('idUno',1)
                    ->setParameter('idDos',2);
                },
            ))
            ->add('idUsuarioIngresa',EntityType::class, array(
                'placeholder' => 'Seleccione un usuario',
                'class' => 'App:Usuario',
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CieDiagnosticoTipo::class,
        ]);
    }
}
