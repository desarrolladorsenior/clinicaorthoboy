<?php

namespace App\Form;

use App\Entity\Imagen;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class ImagenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', FileType::class,array('required'=> false,'attr' =>array('accept'=> 'image/png,image/jpg,image/jpeg,image/vmp,image/tif,image/tiff','data-toggle'=>"tooltip",'data-placement'=>'right', 'data-html'=>'true'),'data_class' => null, 'property_path' => 'nombre'))
            ->add('observacion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => ''),))
            ->add('idPaciente',EntityType::class,array(
            'placeholder' => 'Seleccione un paciente','class'=>'App:Paciente',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idUsuarioIngresa',EntityType::class,array(
            'placeholder' => 'Seleccione un usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Imagen::class,
        ]);
    }
}
