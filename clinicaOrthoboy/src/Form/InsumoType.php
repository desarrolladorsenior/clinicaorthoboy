<?php

namespace App\Form;

use App\Entity\Insumo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class InsumoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descripcion',TextareaType::class,array('attr' => array('class' => 'tinymce', 'required' => ''),))
            ->add('fuente',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Ingrese antecedente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('voltaje',IntegerType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('potencia',IntegerType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('corriente',IntegerType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('estado')
            ->add('idCategoria',EntityType::class,array(
                'placeholder' => 'Seleccione una categoría','class'=>'App:CategoriaInventario',
                'attr'=>array('required'=>'required'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))          
            ->add('idMarca',EntityType::class,array(
                'placeholder' => 'Seleccione una marca','class'=>'App:Marca',
                'attr'=>array('required'=>'required'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))          
            ->add('idPresentacion',EntityType::class,array(
                'placeholder' => 'Seleccione una presentación','class'=>'App:Presentacion',
                'attr'=>array('required'=>'required'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))          
            ->add('idNombreInsumo',EntityType::class,array(
                'placeholder' => 'Seleccione un nombre','class'=>'App:NombreInsumo',
                'attr'=>array('required'=>'required'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))          
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Insumo::class,
        ]);
    }
}
