<?php

namespace App\Form;

use App\Entity\Clinica;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class ClinicaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nit',TextType::class)
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('email',EmailType::class, array('label' => false,'attr'=>array('data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-color'=> 'black', 'data-html'=>'true')))
            ->add('movil',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('direccion',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('telefono',TextType::class)
            ->add('direccionWeb',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('logo', FileType::class,array('required'=> false,'attr' =>array('accept'=> 'image/png,image/jpg,image/jpeg,image/vmp,image/tif,image/tiff','data-toggle'=>"tooltip",'data-placement'=>'right', 'data-html'=>'true'),'data_class' => null, 'property_path' => 'logo'))
            ->add('estadoSede')
            ->add('idMunicipio',EntityType::class, array(
                'placeholder' => 'Seleccione un municipio',
                'class' => 'App:Municipio',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Pendiente.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
            ->add('idSedeClinica',EntityType::class, array(
                'placeholder' => 'Seleccione una clinica',
                'class' => 'App:Clinica',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Pendiente.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Clinica::class,
        ]);
    }
}
