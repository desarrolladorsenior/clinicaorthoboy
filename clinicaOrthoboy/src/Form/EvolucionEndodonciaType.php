<?php

namespace App\Form;

use App\Entity\EvolucionEndodoncia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EvolucionEndodonciaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Ingrese nombre del criterio de la evolución de la sintomatología dolosa.','data-placement'=>'right','data-html'=>'true'),))
            ->add('estado')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EvolucionEndodoncia::class,
        ]);
    }
}
