<?php

namespace App\Form;

use App\Entity\AnexoPaciente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class AnexoPacienteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('descripcion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'title'=> 'Recuerde que el campo permite mínimo 3 y máximo 2000 caracteres.','data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => ''),))

           
            ->add('archivo', FileType::class,array('required'=> false,'attr' =>array('accept'=> 'image/png,image/jpg,image/jpeg,image/vmp,image/tif,image/tiff,.pdf,.xml,.xsl,.xlsx,.doc,.docx,.txt,.odt,.ppt,.pptx,.ods,.zip,.tar,.rar,.mp4,.mpeg,.war,.au','data-toggle'=>"tooltip",'data-placement'=>'right', 'data-html'=>'true'),'data_class' => null, 'property_path' => 'archivo'))
            ->add('idPaciente',EntityType::class, array(
                'placeholder' => 'Seleccione paciente',
                'class' => 'App:Paciente',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione paciente.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idUsuarioIngresa',EntityType::class, array(
                'placeholder' => 'Seleccione un usuario',
                'class' => 'App:Usuario',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione usuario.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idRemision',EntityType::class,array(
                'placeholder' => 'Seleccione un remisión',
                'class' => 'App:Remision',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione remisión.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
            ;    
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AnexoPaciente::class,
        ]);
    }
}
