<?php

namespace App\Form;

use App\Entity\TipoPaquete;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TipoPaqueteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('descripcion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'title'=> 'Pendiente.','data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => ''),))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TipoPaquete::class,
        ]);
    }
}
