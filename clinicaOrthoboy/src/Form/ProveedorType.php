<?php

namespace App\Form;

use App\Entity\Proveedor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Length;

class ProveedorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identificacion',TextType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('direccion',TextType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('movil',IntegerType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('telefono',TextType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('email',EmailType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('nombreContacto',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true','required'=>'required'),))
            ->add('representanteLegal',TextType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))
            ->add('idTipoDocumento',EntityType::class,array(
            'placeholder' => 'Seleccione tipo documento','class'=>'App:TipoDocumento',
            'attr'=>array('required'=>'required')))
            ->add('idMunicipio',EntityType::class,array(
            'placeholder' => 'Seleccione un municipio','class'=>'App:Municipio',
            'attr'=>array('required'=>'required')))
            ->add('estado')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Proveedor::class,
        ]);
    }
}
