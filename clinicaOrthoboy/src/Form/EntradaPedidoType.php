<?php

namespace App\Form;

use App\Entity\EntradaPedido;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EntradaPedidoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cantidad',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('unidadMedida',ChoiceType::class, [
                'choices' => [
                    'Unidad' => 'unid'
                   /* 'Miligramo (mg)' => 'mg',
                    'Gramo (gr)' => 'gr',
                    'Kilogramo (kg)' => 'kg',
                    'Mililitro (ml)' => 'ml',
                    'Litro (l)' => 'l',
                    'Kilolitro (kl)' => 'kl',
                    'Milímetro (mm)' => 'mm',
                    'Centímetro (cm)' => 'cm',
                    'Decímetro (dm)' => 'dm',
                    'Metro (m)' => 'm',
                    'Kilómetro (km)' => 'km'*/
                ],
                'group_by' => function($choice, $key, $value) {
                    if ($choice == 'unid') {
                        return 'Unidades';
                    }/* else if($choice == 'mg' || $choice == 'gr' ||$choice == 'kg') {
                        return 'Masa';
                    }else if($choice == 'ml' || $choice == 'l' ||$choice == 'kl') {
                        return 'Capacidad';
                    }else if($choice == 'mm' || $choice == 'cm' ||$choice == 'dm' ||$choice == 'm' ||$choice == 'km') {
                        return 'Longitud';
                    }*/
                },'placeholder' => 'Seleccione unidad'
            ])
            ->add('fechaVencimiento',DateTimeType::class,array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'dateInitial','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),
            )) 
            ->add('registroInvima',TextType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('lote',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('modelo',TextType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('intervaloMantenimiento',TextType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('garantia',DateTimeType::class,array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'dateInitial','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),
            )) 
            ->add('estado')
            ->add('idPedido',EntityType::class,array(
            'placeholder' => 'Seleccione pedido','class'=>'App:Pedido',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('idInsumo',EntityType::class,array(
            'placeholder' => 'Seleccione insumo','class'=>'App:Insumo',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('idClinicaUbicacion',EntityType::class,array(
            'placeholder' => 'Seleccione ubicación','class'=>'App:Clinica',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estadoSede = :estado')
                    ->andWhere('e.idSedeClinica is not null')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('idUsuarioIngresa',EntityType::class,array(
            'placeholder' => 'Seleccione usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EntradaPedido::class,
        ]);
    }
}
