<?php

namespace App\Form;

use App\Entity\AcuerdoPago;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AcuerdoPagoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipo', ChoiceType::class, [
            'choices'  => [
                'Valor' => 'VALOR',
                'Porcentaje' => 'PORCENTAJE'
            ],'placeholder' => 'Seleccione tipo'
            ])
            ->add('porcentaje',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('valor',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('estado')
            ->add('idTratamiento',EntityType::class,array(
            'placeholder' => 'Seleccione tratamiento','class'=>'App:Tratamiento',
            'attr'=>array('required'=>'required')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AcuerdoPago::class,
        ]);
    }
}
