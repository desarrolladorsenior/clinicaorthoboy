<?php

namespace App\Form;

use App\Entity\Habito;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HabitoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Ingrese hábito.','data-placement'=>'right','data-html'=>'true'),))
            ->add('descripcion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'title'=> 'Ingrese descripción del hábito si considera que es requerida.','data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => ''),))
            ->add('estado')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Habito::class,
        ]);
    }
}
