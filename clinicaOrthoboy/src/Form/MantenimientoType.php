<?php

namespace App\Form;

use App\Entity\Mantenimiento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MantenimientoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipoMantenimiento', ChoiceType::class, [
            'choices'  => [
                'Preventivo' => 'PREVENTIVO',
                'Correctivo' => 'CORRECTIVO'
            ],'placeholder' => 'Seleccione carga'
            ])
            ->add('responsable',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('fechaProgramada',DateType::class,array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'js-datepicker','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),
            ))        
            ->add('fechaEjecucion',DateTimeType::class,array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'dateInitial','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),
            ))        
            ->add('descripcion',TextareaType::class,array('attr' => array('class' => 'tinymce', 'required' => ''),))
            ->add('observacion',TextareaType::class,array('attr' => array('class' => 'tinymce', 'required' => ''),))
            ->add('idEntradaPedido',EntityType::class,array(
            'placeholder' => 'Seleccione elemento','class'=>'App:EntradaPedido',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('idEstadoMantenimiento',EntityType::class,array(
            'placeholder' => 'Seleccione elemento','class'=>'App:EstadoMantenimiento',
            'attr'=>array('required'=>'required','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true')))

            ->add('idUsuarioAprueba',EntityType::class,array(
            'placeholder' => 'Seleccione usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mantenimiento::class,
        ]);
    }
}
