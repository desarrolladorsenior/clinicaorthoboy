<?php

namespace App\Form;

use App\Entity\Radiografia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
class RadiografiaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('hallazgo',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'title'=> 'Pendiente.','data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => ''),))
            ->add('imagen', FileType::class,array('required'=> false,'attr' =>array('accept'=> 'image/png,image/jpg,image/jpeg,image/vmp,image/tif,image/tiff','data-toggle'=>"tooltip",'data-placement'=>'right', 'data-html'=>'true'),'data_class' => null, 'property_path' => 'imagen'))
            ->add('idPaciente',EntityType::class,array(
            'placeholder' => 'Seleccione un paciente','class'=>'App:Paciente',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idTipoRadiografia',EntityType::class,array(
            'placeholder' => 'Seleccione tipo radiografía','class'=>'App:TipoRadiografia',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->andWhere('e.idSedeTipoRadiografia is not null')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idUsuario',EntityType::class,array(
            'placeholder' => 'Seleccione un usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idUsuarioIngresa',EntityType::class,array(
            'placeholder' => 'Seleccione un usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('fechaApertura',DateTimeType::class,array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'dateAprobation','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),
            ))  
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Radiografia::class,
        ]);
    }
}
