<?php

namespace App\Form;

use App\Entity\TipoRadiografia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityRepository;

class TipoRadiografiaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('descripcion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'title'=> 'Pendiente.','data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => ''),))
            ->add('estado')
            ->add('idSedeTipoRadiografia',EntityType::class,array(
            'placeholder' => 'Seleccione tipo radiografía','class'=>'App:TipoRadiografia',
            'attr'=>array('required'=>'required',
                'data-toggle'=>"tooltip" ,
                'title'=> 'Pendiente.',
                'data-placement'=>'right',
                'data-html'=>'true'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.idSedeTipoRadiografia is null')
                    ->andWhere('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TipoRadiografia::class,
        ]);
    }
}
