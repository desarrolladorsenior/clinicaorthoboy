<?php

namespace App\Form;

use App\Entity\Remision;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RemisionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('observacion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'title'=> 'Pendiente.','data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => ''),))
            ->add('email',EmailType::class, array('label' => false,'attr'=>array('data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-color'=> 'black', 'data-html'=>'true')))
            ->add('idPaciente',EntityType::class, array(
                'placeholder' => 'Seleccione un paciente',
                'class' => 'App:Paciente',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione paciente.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
            ->add('idUsuarioRemite',EntityType::class, array(
                'placeholder' => 'Seleccione un usuario',
                'class' => 'App:Usuario',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione un usuario.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
            ->add('idUsuarioRemitido',EntityType::class, array(
                'placeholder' => 'Seleccione un usuario',
                'class' => 'App:Usuario',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Pendiente.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
            ->add('idProcedimiento',EntityType::class, array(
                'placeholder' => 'Seleccione un procedimiento.',
                'class' => 'App:Cups',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Pendiente.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Remision::class,
        ]);
    }
}
