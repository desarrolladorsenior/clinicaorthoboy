<?php

namespace App\Form;

use App\Entity\MotivoConsulta;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MotivoConsultaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Ingrese el motivo por la que se requiere la atención del paciente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('tiempoEstimado',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Ingrese el tiempo estimado que considera durara la atención de la consulta.','data-placement'=>'right','data-html'=>'true'),))
            ->add('estado')
            ->add('idProcedimiento',EntityType::class, array(
                'placeholder' => 'Seleccione un procedimiento',
                'class' => 'App:Cups',
                'attr'=>array(
                    'data-toggle'=>"tooltip" ,
                    'title'=> 'Asocie el procedimiento identificado para el motivo de consulta.',
                    'data-placement'=>'right',
                    'data-html'=>'true'
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MotivoConsulta::class,
        ]);
    }
}
