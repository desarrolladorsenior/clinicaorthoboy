<?php

namespace App\Form;

use App\Entity\CupDiagnostico;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class CupDiagnosticoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idCups',EntityType::class,array(
            'placeholder' => 'Seleccione cup','class'=>'App:Cups',
            'attr'=>array('required'=>'required')))  
            ->add('idRips',EntityType::class,array(
            'placeholder' => 'Seleccione cie','class'=>'App:CieRip',
            'attr'=>array('required'=>''))) 
            ->add('idDxr1',EntityType::class,array(
            'placeholder' => 'Seleccione cie','class'=>'App:CieRip',
            'attr'=>array('required'=>''))) 
            ->add('idDxr2',EntityType::class,array(
            'placeholder' => 'Seleccione cie','class'=>'App:CieRip',
            'attr'=>array('required'=>''))) 
            ->add('idDxr3',EntityType::class,array(
            'placeholder' => 'Seleccione cie','class'=>'App:CieRip',
            'attr'=>array('required'=>''))) 
            ->add('idCausaExterna',EntityType::class,array(
            'placeholder' => 'Seleccione causa externa','class'=>'App:CausaExterna',
            'attr'=>array('required'=>''))) 
            ->add('idAmbitoRealizacion',EntityType::class,array(
            'placeholder' => 'Seleccione ambito realización','class'=>'App:AmbitoRealizacion','attr'=>array('required'=>''))) 
            ->add('idFormaRealizacion',EntityType::class,array(
            'placeholder' => 'Seleccione forma realización ','class'=>'App:FormaRealizacion','attr'=>array('required'=>'')))
            ->add('idPersonaAtiende',EntityType::class,array(
            'placeholder' => 'Seleccione persona atiende','class'=>'App:PersonaAtiende','attr'=>array('required'=>'')))
            ->add('idFinalidadProcedimiento',EntityType::class,array(
            'placeholder' => 'Seleccione finalidad procedimiento','class'=>'App:FinalidadProcedimiento','attr'=>array('required'=>'')))
            ->add('idUsuarioIngresa',EntityType::class,array(
            'placeholder' => 'Seleccione usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CupDiagnostico::class,
        ]);
    }
}
