<?php

namespace App\Form;

use App\Entity\Presupuesto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class PresupuestoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('iva',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('valorAprobado',NumberType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('porcentajeDescuento',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('idConvenio',EntityType::class,array(
            'placeholder' => 'Seleccione convenio','class'=>'App:Convenio',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idTratamiento',EntityType::class,array(
            'placeholder' => 'Seleccione tratamiento','class'=>'App:Tratamiento',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idPieza')
            ->add('idSuperficie',EntityType::class,array(
            'placeholder' => 'Seleccione superficie','class'=>'App:Superficie',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idProcedimiento',EntityType::class,array(
            'placeholder' => 'Seleccione procedimiento','class'=>'App:Cups',
            'attr'=>array('required'=>'required')
            ))
            ->add('idPlanTratamiento',EntityType::class,array(
            'placeholder' => 'Seleccione plan','class'=>'App:PlanTratamiento',
            'attr'=>array('required'=>'required')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Presupuesto::class,
        ]);
    }
}
