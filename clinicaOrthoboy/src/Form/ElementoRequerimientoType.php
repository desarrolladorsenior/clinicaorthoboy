<?php

namespace App\Form;

use App\Entity\ElementoRequerimiento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ElementoRequerimientoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cantidad',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('estado')
            ->add('idRequerimiento',EntityType::class,array(
            'placeholder' => 'Seleccione requerimiento','class'=>'App:Requerimiento',
            'attr'=>array('required'=>'required')
            ))
            ->add('idNombreInsumo',EntityType::class,array(
                'placeholder' => 'Seleccione un nombre','class'=>'App:NombreInsumo',
                'attr'=>array('required'=>'required'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))          
            ->add('idMarca',EntityType::class,array(
                'placeholder' => 'Seleccione una marca','class'=>'App:Marca',
                'attr'=>array('required'=>'required'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))          
            ->add('idPresentacion',EntityType::class,array(
                'placeholder' => 'Seleccione una presentación','class'=>'App:Presentacion',
                'attr'=>array('required'=>'required'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))          
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ElementoRequerimiento::class,
        ]);
    }
}
