<?php

namespace App\Form;

use App\Entity\ExamenGeneral;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
class ExamenGeneralType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('motivoConsulta',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => true,'minlength'=>'10','maxlength'=>'2000'),))

            ->add('antecedenteMedico',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'1','maxlength'=>'2000'),))

            ->add('antecedenteFamiliar',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'1','maxlength'=>'2000'),))

            ->add('medicamento',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'1','maxlength'=>'2000'),))

            ->add('alergia',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'1','maxlength'=>'2000'),))

            ->add('factorRiesgo',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'1','maxlength'=>'2000'),))

            ->add('determinanteSocial',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'1','maxlength'=>'2000'),))

            ->add('poblacionVulnerable',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'1','maxlength'=>'2000'),))

            ->add('antecedenteSistemico',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'1','maxlength'=>'2000'),))

            ->add('habito',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'1','maxlength'=>'2000'),))

            ->add('otro',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'4','maxlength'=>'2000'),))

            ->add('embarazo',CheckboxType::class,array(
                'label'=>'Paciente en estado de embarazo',
                'required' => false,
                ))  

             ->add('cara',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true','required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Cara: Identificar ausencia de simetría....'),))

            ->add('ganglios',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true','required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Glanglios linfáticos: Identificar variaciones de volumen, consitencia, forma....'),))

            ->add('maxilarMandibula',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Maxilar y Mandíbula: Verificar ausencia de simetría....'),))

            ->add('musculo',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Músculos: Valoraciones funcionales....'),))

            ->add('labio',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Labios'),))

            ->add('carillo',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true','required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Carillos'),))

            ->add('encia',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Encia'),))

            ->add('lengua',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true','required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Lengua'),))

            ->add('pisoBocal',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Piso Boca'),))

            ->add('paladar',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Paladar'),))

            ->add('orfaringe',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Orfaringe'),))

            ->add('dolorMuscular',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Dolor múscular'),))

            ->add('dolorArtilar',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Dolor articular'),))

            ->add('ruidoArtilar',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Ruido Articular'),))

            ->add('alteracionMovimiento',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'alteracione movimiento'),))

            ->add('malOclusion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Mal oclusiones'),))

            ->add('crecimientoDesarrollo',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Crecimiento y desarrollo'),))

            ->add('fasetaDesgaste',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Fasetas de desgaste'),))

            ->add('nDienteTP',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true','required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Número total de dientes temporales y permanentes.'),))

            ->add('estadoEstructural', ChoiceType::class, [
                'choices'  => [
                    '0. Sano' => 'Sano',
                    '1. Mancha blanca / marrón en esmalte seco' => 'MANCHA BLANCA / MARRÓN EN ESMALTE SECO',
                    '2. Mancha blanca / marrón  en esmalte húmedo' => 'MANCHA BLANCA / MARRÓN  EN ESMALTE HÚMEDO',
                    '3. Microcavidad en esmalte seco < 0.5mm sin dentina visible' => 'MICROCAVIDAD EN ESMALTE SECO < 0.5MM SIN DENTINA VISIBLE',
                    '4. Sombra oscura de dentina vista a través del esmalte húmedo con o sin microcavidad' => 'SOMBRA OSCURA DE DENTINA VISTA A TRAVÉS DEL ESMALTE HÚMEDO CON O SIN MICROCAVIDAD',

                ],'placeholder' => 'Seleccione estado estructural dental ICDAS'
            ])

            ->add('alteracionFlurosis',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Alteraciones flurosiis dental'),))

            ->add('rehabilitacion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Rehabilitaciones'),))

            ->add('saliva',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Saliva'),))

            ->add('funcionTejido',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Funciones tejidos y estructuras'),))
            ->add('placa',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => false,'minlength'=>'10','maxlength'=>'2000','placeholder'=>'Placa'),))

            ->add('remocionPlaca',CheckboxType::class,array(
                'label'=>'Remoción placa',
                'required' => false,
                ))   

            ->add('fluor',CheckboxType::class,array(
                'label'=>'Aplicación fluor',
                'required' => false,
                ))    

            ->add('detartraje',CheckboxType::class,array(
                'label'=>'Detartraje suplagingival',
                'required' => false,
                ))    

            ->add('sellante',CheckboxType::class,array(
                'label'=>'Aplicación sellante',
                'required' => false,
                )) 

            ->add('valoracionMedicina',CheckboxType::class,array(
                'label'=>'Valoración por medicina o enfermeria',
                'required' => false,
                )) 
            ->add('educacionGrupal',CheckboxType::class,array(
                'label'=>'Educación grupal',
                'required' => false,
                )) 
            ->add('idPaciente',EntityType::class,array(
            'placeholder' => 'Seleccione un paciente','class'=>'App:Paciente',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('idUsuarioProfesional',EntityType::class,array(
            'placeholder' => 'Seleccione un usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExamenGeneral::class,
        ]);
    }
}
