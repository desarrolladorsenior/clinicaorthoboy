<?php

namespace App\Form;

use App\Entity\AgendaUsuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;

class AgendaUsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('horaInicio', TimeType::class, [
                'input'  => 'datetime',
                'widget' =>  'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => true,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'clockTime','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true','disabled'=> true),
                
            ])
            ->add('horaFin', TimeType::class, [
                'input'  => 'datetime',
                'widget' =>  'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => true,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'clockTime','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true','disabled'=> true),
                'placeholder' => [ 'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',],
            ])
            ->add('dia', ChoiceType::class, [
            'choices'  => [
                'Lunes' => '1',
                'Martes' => '2',
                'Miercoles' => '3',
                'Jueves' => '4',
                'Viernes' => '5',
                'Sabado' => '6',
                'Domingo' => '7',
                'Lunes - Martes' => '12',
                'Lunes - Miercoles' => '13',
                'Lunes - Jueves' => '14',
                'Lunes - Viernes' => '15',
                'Lunes - Sabado' => '16',
                'Lunes - Domingo' => '17',
                'Martes - Miercoles' => '23',
                'Martes - Jueves' => '24',
                'Martes - Viernes' => '25',
                'Martes - Sabado' => '26',
                'Martes - Domingo' => '27',
                'Miercoles - Jueves' => '34',
                'Miercoles - Viernes' => '35',
                'Miercoles - Sabado' => '36',
                'Miercoles - Domingo' => '37',
                'Jueves - Viernes' => '45',
                'Jueves - Sabado' => '46',
                'Jueves - Domingo' => '47',
                'Viernes - Sabado' => '56',
                'Viernes - Domingo' => '57',
                'Sabado - Domingo' => '67',
                'Lunes a Miercoles' => '123',
                'Lunes a Jueves' => '1234',
                'Lunes a Viernes' => '12345',
                'Lunes a Sabado' => '123456',
                'Lunes a Domingo' => '1234567',
                'Martes a Jueves' => '234',
                'Martes a Viernes' => '2345',
                'Martes a Sabado' => '23456',
                'Martes a Domingo' => '234567',
                'Miercoles a Viernes' => '345',
                'Miercoles a Sabado' => '3456',
                'Miercoles a Domingo' => '34567',
                'Jueves a Sabado' => '456',
                'Jueves a Domingo' => '4567',
                'Viernes a Domingo' => '567',
            ],'placeholder' => 'Seleccione horario'
            ])
            ->add('estado')
            ->add('idUsuarioIngresa',EntityType::class,array(
            'placeholder' => 'Seleccione un usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
            ->add('horaAlmuerzo',  DateIntervalType::class, [
                'attr'=>array('required'=>false),
                'widget'      => 'choice', // render a text field for each part
                // 'input'    => 'string',  // if you want the field to return a ISO 8601 string back to you

                // customize which text boxes are shown
                'with_years'  => false,
                'with_months' => false,
                'with_days'   => false,
                'with_hours'  => true,
                'with_minutes'=> true,
                'minutes' => range(0, 60),
                //'minutes' => array_combine(range(1, 60), range(1, 60)),
                'hours' => range(0, 24),
                //'hours' => array_combine(range(1, 24), range(1, 24)),
                'labels' => [
                    'hours' => 'H',
                    'minutes' => 'M'
                ]

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AgendaUsuario::class,
        ]);
    }
}
