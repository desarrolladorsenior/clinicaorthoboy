<?php

namespace App\Form;

use App\Entity\Eps;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\Length;

class EpsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true','required'=>'required'),))

            ->add('nit',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true','required'=>'required'),))
            ->add('descripcion',TextareaType::class,array('attr' => array('class' => 'tinymce'),))

            ->add('estado')

            ->add('telefono',TextType::class,array('attr' => array('class' => 'tinymce'),))

            ->add('movil',IntegerType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))

            ->add('nombreContacto',TextType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))

            ->add('direccion',TextType::class, array('attr' => array('class' => 'tinymce','required'=>'required','required'=>'required'),))

            ->add('email',EmailType::class, array('attr' => array('class' => 'tinymce','required'=>'required'),))

            ->add('idMunicipio',EntityType::class,array(
            'placeholder' => 'Seleccione un municipio','class'=>'App:Municipio',
            'attr'=>array('required'=>'required')))
            
            ->add('idSedeEps',EntityType::class,array(
            'placeholder' => 'Seleccione una sede','class'=>'App:Eps',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.idSedeEps is null')
                    ->andWhere('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))           
            
            ->add('idUsuarioIngresa',EntityType::class,array(
            'placeholder' => 'Seleccione un usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Eps::class,
        ]);
    }
}
