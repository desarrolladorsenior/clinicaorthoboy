<?php

namespace App\Form;

use App\Entity\OdontogramaPaciente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class OdontogramaPacienteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idOdontograma')
            ->add('idPieza')
            ->add('idSuperficie')
            ->add('idRipTipoOdon',EntityType::class,array(
            'placeholder' => 'Seleccione diagnóstico','class'=>'App:CieDiagnosticoTipo',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('idDiagPrincipal',EntityType::class,array(
            'placeholder' => 'Seleccione tipo','class'=>'App:DiagnosticoPrincipal',
            'attr'=>array('required'=>'required')
            ))
            ->add('idUsuarioProfesional')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OdontogramaPaciente::class,
        ]);
    }
}
