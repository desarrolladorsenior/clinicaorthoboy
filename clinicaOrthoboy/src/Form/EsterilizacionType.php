<?php

namespace App\Form;

use App\Entity\Esterilizacion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
class EsterilizacionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaApertura',DateTimeType::class,array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'dateInitial','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),
            ))        
            ->add('fechaVecimiento',DateTimeType::class,array(
                'widget' => 'single_text',
                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,
                // adds a class that can be selected in JavaScript
                'attr' => array('class' => 'dateEnd','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),
            )) 
            ->add('carga', ChoiceType::class, [
            'choices'  => [
                '1' => '1',
                '2' => '2'
            ],'placeholder' => 'Seleccione carga'
            ])
            ->add('loteProveta',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Pendiente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('cantidadPaquete',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('tiempoEsterilizacion',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('tiempoSecado',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('temperatura',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('presion',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('tipoEsterilizacion', ChoiceType::class, [
            'choices'  => [
                'Liquido' => 'LIQUIDO',
                'Autoclave' => 'AUTOCLAVE'
            ],'placeholder' => 'Seleccione tipo esterilización'
            ])
            ->add('estado')
            ->add('idUsuarioReporta',EntityType::class,array(
            'placeholder' => 'Seleccione persona responsable','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                },
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Esterilizacion::class,
        ]);
    }
}
