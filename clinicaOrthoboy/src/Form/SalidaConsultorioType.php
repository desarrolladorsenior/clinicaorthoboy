<?php

namespace App\Form;

use App\Entity\SalidaConsultorio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class SalidaConsultorioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idSalida',EntityType::class,array(
            'placeholder' => 'Seleccione salida','class'=>'App:Salida',
            'attr'=>array('required'=>'required')
            ))
            ->add('cantidad',IntegerType::class, array('attr' => array('class' => 'tinymce'),))
            ->add('idConsultorio',EntityType::class,array(
            'placeholder' => 'Seleccione consultorio','class'=>'App:Consultorio',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
            ->add('idUsuarioIngresa',EntityType::class,array(
            'placeholder' => 'Seleccione usuario','class'=>'App:Usuario',
            'attr'=>array('required'=>'required'),
            'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',TRUE);
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SalidaConsultorio::class,
        ]);
    }
}
