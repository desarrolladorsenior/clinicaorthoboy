<?php

namespace App\Form;

use App\Entity\DiagnosticoOdontologico;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class DiagnosticoOdontologicoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codigo',IntegerType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Ingrese el código de codificación del diagnóstico.','data-placement'=>'right','data-html'=>'true'),))
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Ingrese nombre del diagnóstico o la afección principal de solicitud de atención del paciente.','data-placement'=>'right','data-html'=>'true'),))
            ->add('descripcion',TextareaType::class,array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip",'title'=> 'Ingrese descripción del diagnóstico odontológico si considera que es requerido.','data-placement'=>'right','data-color'=>'PALETURQUOISE','data-html'=>'true', 'required' => ''),))
            ->add('estado')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DiagnosticoOdontologico::class,
        ]);
    }
}
