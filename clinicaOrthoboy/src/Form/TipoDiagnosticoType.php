<?php

namespace App\Form;

use App\Entity\TipoDiagnostico;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TipoDiagnosticoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',TextType::class, array('attr' => array('class' => 'tinymce','data-toggle'=>"tooltip" ,'title'=> 'Ingrese nombre de la categoría de diagnóstico de la pieza dental.','data-placement'=>'right','data-html'=>'true'),))
            ->add('idArea',EntityType::class, array(
                'placeholder' => 'Seleccione una área',
                'class' => 'App:Area',
                'attr'=>array('required'=>'required', 'data-toggle'=>"tooltip" ,
                    'title'=> 'Seleccione el área medica donde se asocia la categoría.',
                    'data-placement'=>'right',
                    'data-html'=>'true'),
                'query_builder' => function(EntityRepository $er)
                {
                    return $er->createQueryBuilder('e')
                    ->where('e.estado = :estado')
                    ->setParameter('estado',true);
                },
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TipoDiagnostico::class,
        ]);
    }
}
