<?php

namespace App\Repository;

use App\Entity\Eps;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Eps|null find($id, $lockMode = null, $lockVersion = null)
 * @method Eps|null findOneBy(array $criteria, array $orderBy = null)
 * @method Eps[]    findAll()
 * @method Eps[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Eps[]    findAllGreater(array $id)
 */

class EpsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Eps::class);
    }

    /**
     * @return Eps[] Returns an array of Eps objects
     */

    /**
     * @param $price
     * @return Eps[]
     */
    public function findByGreaterThanPrice($price): array
    {
        // automatically knows to select Products
        // the "p" is an alias you'll use in the rest of the query
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.nombre > :price')
            ->setParameter('nombre', $price)
            ->orderBy('p.nombre', 'ASC')
            ->getQuery();

        return $qb->execute();

        // to get just one result:
        // $product = $qb->setMaxResults(1)->getOneOrNullResult();
    }

    /**
     * @param $id
     * @param integer $id
     * @param findByGreater($id)
     * @return Eps[]
     */
    public function findByGreater($price): array
    {
     $conn = $this->getEntityManager()->getConnection();


    $sql = '
        SELECT * FROM eps p
        WHERE p.nombre > :price
        ORDER BY p.nombre ASC
        ';
    $stmt = $conn->prepare($sql);
    $stmt->execute(['price' => $price]);

    // returns an array of arrays (i.e. a raw data set)
    return $stmt->fetchAll();
    }

    /**
     * @param $id
     * @param integer $id
     * @param findByGreat($id)
     * @return Eps[]
     */
    public function findByGreat($id): array
    {
    $conn = $this->getEntityManager()->getConnection();

    $sql = '
        SELECT * FROM eps p
        WHERE p.id_eps > :eps';

    $stmt = $conn->prepare($sql);
    $stmt->execute(['eps' => $id]);

    // returns an array of arrays (i.e. a raw data set)
    return $stmt->fetchAll();
    }



    
    // public function findByExampleField($value)
    // {
    //     return $this->createQueryBuilder('e')
    //         ->andWhere('e.exampleField = :val')
    //         ->setParameter('val', $value)
    //         ->orderBy('e.id', 'ASC')
    //         ->setMaxResults(10)
    //         ->getQuery()
    //         ->getResult()
    //     ;
    // }

    // public function findAllOrderedByName(){

    // return $this->getManager()->createQuery(
    //     'SELECT * from eps ORDER BY COALESCE(id_sede_eps, id_eps), id_eps, id_sede_eps'
    // )->getResult();
    // }

    
    // public function findOneBySomeField($value): ?Eps
    // {
    //     return $this->createQueryBuilder('e')
    //         ->andWhere('e.exampleField = :val')
    //         ->setParameter('val', $value)
    //         ->getQuery()
    //         ->getOneOrNullResult()
    //     ;
    // }
    
}
