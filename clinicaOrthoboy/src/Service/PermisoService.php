<?php
namespace App\Service;

use Doctrine\ORM\EntityManager;
use App\Entity\PerfilPermiso;

class PermisoService
{

    protected $em;

    public function __construct( EntityManager $em )
    {
        $this->em = $em;
    }

    public function datosGenerales( $idPerfil )
    {
        //$item = $this->em->getRepository(PerfilPermiso::class)->findOneBy(array('idPerfil' => $idPerfil));
        //return $item ? $item->getIdPermiso() : null;

        $dql = "SELECT b FROM App:PerfilPermiso b WHERE b.idPerfil= :idPerfil";

        $query = $this->em->createQuery($dql)->setParameter('idPerfil',$idPerfil);
        return $query->getResult();
    }
}