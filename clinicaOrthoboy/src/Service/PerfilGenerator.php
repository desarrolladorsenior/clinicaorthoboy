<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class PerfilGenerator extends AbstractController
{
    public function verificacionPermisos($perfil,$nombre)
    {
    	$entityManager = $this->getDoctrine()->getManager();
      	$dqlPerfil="SELECT  perfilPermiso FROM App:PerfilPermiso perfilPermiso INNER JOIN App:Permiso permiso WITH permiso.idPermiso=perfilPermiso.idPermiso WHERE  perfilPermiso.idPerfil = :perfil and permiso.nombre like :nombre";
      	$queryPerfil=$entityManager->createQuery($dqlPerfil)->setParameter('perfil',$perfil)->setParameter('nombre', '%'.$nombre.'%');
      	$perfiles=$queryPerfil->getResult();
        return $perfiles;
    }

    function generarCodigo($longitud) {
       $key = '';
       $pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNQRSTUVWXYZ';
       $max = strlen($pattern)-1;
       for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
       return $key;
    } 

    public function datosGenerales($perfil): Response
    {
        $$dqlPerfil ="SELECT permiso FROM App:Permiso permiso INNER JOIN App:PerfilPermiso perfilPermiso with perfilPermiso.idPermiso=permiso.idPermiso  WHERE perfilPermiso.idPerfil = :perfil";  
        $queryPerfil= $entityManager->createQuery($dqlPerfil)->setParameter('perfil',$perfil);
        $perfiles= $queryPerfil->getResult();
       
        return $perfiles;                   
    }
}