<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Presupuesto
 */     

class Presupuesto
{
    /**
     * @var integer
    */
    private $iva;
    /**
     * @var float
    */
    private $totalValorIva;
    /**
     * @var float
    */
    private $valorAprobado;
    /**
     * @var integer
    */
    private $porcentajeDescuento;
    /**
     * @var integer
    */
    private $idPresupuesto;
    /**
    * @var \App\Entity\PlanTratamiento
    */
    private $idPlanTratamiento;
    /**
    * @var \App\Entity\EstadoGeneral
    */
    private $idEstadoGeneral;
    /**
    * @var \App\Entity\Convenio
    */
    private $idConvenio;
    /**
    * @var \App\Entity\Tratamiento
    */
    private $idTratamiento;
    /**
    * @var \App\Entity\PiezaDental
    */
    private $idPieza;
    /**
    * @var \App\Entity\Superficie
    */
    private $idSuperficie;
    /**
    * @var \App\Entity\Cups
    */
    private $idProcedimiento;
    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    public function getIva(): ?int
    {
        return $this->iva;
    }

    public function setIva(int $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getTotalValorIva(): ?float
    {
        return $this->totalValorIva;
    }

    public function setTotalValorIva(float $totalValorIva): self
    {
        $this->totalValorIva = $totalValorIva;

        return $this;
    }

    public function getValorAprobado(): ?float
    {
        return $this->valorAprobado;
    }

    public function setValorAprobado(float $valorAprobado): self
    {
        $this->valorAprobado = $valorAprobado;

        return $this;
    }

    public function getPorcentajeDescuento(): ?int
    {
        return $this->porcentajeDescuento;
    }

    public function setPorcentajeDescuento(int $porcentajeDescuento): self
    {
        $this->porcentajeDescuento = $porcentajeDescuento;

        return $this;
    }

    public function getIdPresupuesto(): ?int
    {
        return $this->idPresupuesto;
    }

    public function getIdPlanTratamiento(): ?PlanTratamiento
    {
        return $this->idPlanTratamiento;
    }

    public function setIdPlanTratamiento(?PlanTratamiento $idPlanTratamiento): self
    {
        $this->idPlanTratamiento = $idPlanTratamiento;

        return $this;
    }

    public function getIdEstadoGeneral(): ?EstadoGeneral
    {
        return $this->idEstadoGeneral;
    }

    public function setIdEstadoGeneral(?EstadoGeneral $idEstadoGeneral): self
    {
        $this->idEstadoGeneral = $idEstadoGeneral;

        return $this;
    }

    public function getIdConvenio(): ?Convenio
    {
        return $this->idConvenio;
    }

    public function setIdConvenio(?Convenio $idConvenio): self
    {
        $this->idConvenio = $idConvenio;

        return $this;
    }

    public function getIdTratamiento(): ?Tratamiento
    {
        return $this->idTratamiento;
    }

    public function setIdTratamiento(?Tratamiento $idTratamiento): self
    {
        $this->idTratamiento = $idTratamiento;

        return $this;
    }

    public function getIdPieza(): ?PiezaDental
    {
        return $this->idPieza;
    }

    public function setIdPieza(?PiezaDental $idPieza): self
    {
        $this->idPieza = $idPieza;

        return $this;
    }

    public function getIdSuperficie(): ?Superficie
    {
        return $this->idSuperficie;
    }

    public function setIdSuperficie(?Superficie $idSuperficie): self
    {
        $this->idSuperficie = $idSuperficie;

        return $this;
    }

    public function getIdProcedimiento(): ?Cups
    {
        return $this->idProcedimiento;
    }

    public function setIdProcedimiento(?Cups $idProcedimiento): self
    {
        $this->idProcedimiento = $idProcedimiento;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

    public function __toString()
    {
        return $this->getIdPlanTratamiento();  
    } 
}
