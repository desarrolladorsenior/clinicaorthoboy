<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Mantenimiento
 */     
class Mantenimiento
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 75,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $tipoMantenimiento;
    /**
    * @var string
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $responsable;
    /**
    * @var \DateTime
    * @Assert\NotBlank(message="Este campo es obligatorio.")    
    */
    private $fechaProgramada;
    /**
    * @var \DateTime
    */
    private $fechaEjecucion;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $descripcion;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $observacion;
    /**
     * @var integer
    */
    private $idMantenimiento;
    /**
     * @var \App\Entity\EntradaPedido
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idEntradaPedido;
    /**
     * @var \App\Entity\EstadoMantenimiento
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idEstadoMantenimiento;
    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioAprueba;

    public function getTipoMantenimiento(): ?string
    {
        return $this->tipoMantenimiento;
    }

    public function setTipoMantenimiento(string $tipoMantenimiento): self
    {
        $this->tipoMantenimiento = $tipoMantenimiento;

        return $this;
    }

    public function getResponsable(): ?string
    {
        return $this->responsable;
    }

    public function setResponsable(?string $responsable): self
    {
        $this->responsable = $responsable;

        return $this;
    }

    public function getFechaProgramada(): ?\DateTimeInterface
    {
        return $this->fechaProgramada;
    }

    public function setFechaProgramada(\DateTimeInterface $fechaProgramada): self
    {
        $this->fechaProgramada = $fechaProgramada;

        return $this;
    }

    public function getFechaEjecucion(): ?\DateTimeInterface
    {
        return $this->fechaEjecucion;
    }

    public function setFechaEjecucion(?\DateTimeInterface $fechaEjecucion): self
    {
        $this->fechaEjecucion = $fechaEjecucion;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getIdMantenimiento(): ?int
    {
        return $this->idMantenimiento;
    }

    public function getIdEntradaPedido(): ?EntradaPedido
    {
        return $this->idEntradaPedido;
    }

    public function setIdEntradaPedido(?EntradaPedido $idEntradaPedido): self
    {
        $this->idEntradaPedido = $idEntradaPedido;

        return $this;
    }

    public function getIdEstadoMantenimiento(): ?EstadoMantenimiento
    {
        return $this->idEstadoMantenimiento;
    }

    public function setIdEstadoMantenimiento(?EstadoMantenimiento $idEstadoMantenimiento): self
    {
        $this->idEstadoMantenimiento = $idEstadoMantenimiento;

        return $this;
    }

    public function getIdUsuarioAprueba(): ?Usuario
    {
        return $this->idUsuarioAprueba;
    }

    public function setIdUsuarioAprueba(?Usuario $idUsuarioAprueba): self
    {
        $this->idUsuarioAprueba = $idUsuarioAprueba;

        return $this;
    }

    public function __toString()
    {
        return 'Mantenimiento N°: '.$this->getIdMantenimiento().' - Elemento: '.$this->getIdEntradaPedido()->getIdInsumo();
    }
}
