<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Clinica
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */     
class Clinica 
{
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 9,
     *      max = 17,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    * @Assert\Regex(
     * pattern="/^[0-9 -]*$/", 
     * message="Se permiten unicamente números y guión."), 
     */
    private $nit;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $nombre;
    /**
     * @var string
     * @Assert\Length(
     *      min = 6,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $direccion;

     /**
     * @var string
     * @Assert\Length(
     *      min = 11,
     *      max = 11,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $telefono;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 10,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es valido.",
     * )
     */
    private $email;

    /**
     * @var string
     * @Assert\Length(
     *      min = 10,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $direccionWeb;

    /**
     * @var string
     * @Assert\File(
     *maxSize = "1m",
     *mimeTypes={ "image/png","image/jpg","image/jpeg","image/vmp","image/tif","image/tiff"},
     * mimeTypesMessage = "")
     */
    private $logo;

    /**
     * @var boolean
    */
    private $estadoSede;
    /**
     * @var integer
    */
    private $idClinica;

    /**
     * @var \App\Entity\Clinica
    */
    private $idSedeClinica;
    /**
    * @var bigint
    * @Assert\Length(
    *      min = 10,
    *      max = 15,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $movil;

    /**
     * @var \App\Entity\Municipio
    */
    private $idMunicipio;

    public function getNit(): ?string
    {
        return $this->nit;
    }

    public function setNit(string $nit): self
    {
        $this->nit = $nit;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(?string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = strtolower($email);

        return $this;
    }

    public function getDireccionWeb(): ?string
    {
        return $this->direccionWeb;
    }

    public function setDireccionWeb(?string $direccionWeb): self
    {
        $this->direccionWeb = $direccionWeb;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    /* con este metodo
        Subir un unico archivo*/
    public function uploadUno($archivo){
    // la propiedad file puede quedar vacía
        if(null === $archivo){
            return;
        }

        // se usa el nombre original aquí pero debería sanitizarse
        // el método move recibe el directorio y el archivo
        $fileName=md5(uniqid()).'.'.$archivo->guessExtension();
        $archivo->move('/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/clinica/',$fileName);
        $this->path=$fileName;
        $this->setLogo($this->path);
    }
    public function getEstadoSede(): ?bool
    {
        return $this->estadoSede;
    }

    public function setEstadoSede(bool $estadoSede): self
    {
        $this->estadoSede = $estadoSede;

        return $this;
    }

    public function getIdClinica(): ?int
    {
        return $this->idClinica;
    }

    public function getIdSedeClinica(): ?self
    {
        return $this->idSedeClinica;
    }

    public function setIdSedeClinica(?self $idSedeClinica): self
    {
        $this->idSedeClinica = $idSedeClinica;

        return $this;
    }
    

    public function getMovil(): ?int
    {
        return $this->movil;
    }

    public function setMovil(int $movil): self
    {
        $this->movil = $movil;

        return $this;
    }

    public function getIdMunicipio(): ?Municipio
    {
        return $this->idMunicipio;
    }

    public function setIdMunicipio(?Municipio $idMunicipio): self
    {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }

    public function __toString()
    {
        if ($this->getIdSedeClinica() != '') {
           return $this->getNombre().' - '.$this->getIdSedeClinica()->getNombre();
        }else{
          return $this->getNombre();  
        }
        
    } 
}
