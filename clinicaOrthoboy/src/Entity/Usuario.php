<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
    
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
/**
 * Usuario
 * @UniqueEntity(fields={"email"}, message="Este dato ya se encuentra registrado.")
 * @UniqueEntity(fields={"numeroIdentificacion"}, message="Este dato ya se encuentra registrado.")
 */ 

class Usuario implements AdvancedUserInterface, \Serializable
{
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $nombres;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $apellidos;
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 9,
     *      max = 17,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    * @Assert\Regex(
     * pattern="/^[0-9 -]*$/", 
     * message="Se permiten unicamente números y guión."), 
     */
    private $numeroIdentificacion;
    /**
    * @var \DateTime
    * @Assert\NotBlank(message="Este campo es obligatorio.")    
    */
    private $fechaNacimiento;
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 10,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es valido.",
     * )
     */
    private $email;
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 1,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $username;
    /**
     * @var string  
     */
    private $password;
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 1,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $genero;
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 1,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $colorAgenda;
     /**
     * @var string
     * @Assert\Length(
     *      min = 6,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $direccion;
    /**
     * @var string
     * @Assert\Length(
     *      min = 1,
     *      max = 155,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $nombreContacto;
    /**
    * @var bigint
    * @Assert\Length(
    *      min = 10,
    *      max = 15,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $numeroContacto;

    private $fechaApertura;
    /**
    * @var bigint
    * @Assert\Length(
    *      min = 10,
    *      max = 15,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $movil;
    /**
     * @var string
    */
    private $firmaProfesional;
    /**
     * @var string
     * @Assert\File(
     *maxSize = "1m",
     *mimeTypes={ "image/png","image/jpg","image/jpeg","image/vmp","image/tif","image/tiff"},
     * mimeTypesMessage = "")
    */
    private $imagenUsuario;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $intento;
    /**
     * @var integer
    */
    private $idUsuario;
    /**
     * @var \App\Entity\TipoDocumento
    */
    private $tipoentificacion;
    /**
     * @var \App\Entity\Especialidad
    */
    private $idEspecialidad;
    /**
     * @var \App\Entity\AcuerdoPago
    */
    private $idAcuerdoPorc;
    /**
     * @var \App\Entity\AcuerdoPago
    */
    private $idAcuerdoValor;
    /**
     * @var \App\Entity\AgendaUsuario
    */
    private $idAgendaUsuario;
    /**
     * @var \App\Entity\Perfil
    */
    private $idPerfil;
    /**
     * @var \App\Entity\Clinica
    */
    private $idSucursal;
    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;
    /**
     * @var string
    */
    private $tipoUsuario;

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getNumeroIdentificacion(): ?string
    {
        return $this->numeroIdentificacion;
    }

    public function setNumeroIdentificacion(string $numeroIdentificacion): self
    {
        $this->numeroIdentificacion = $numeroIdentificacion;

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento(\DateTimeInterface $fechaNacimiento): self
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email =strtolower($email);

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = strtoUpper($username);

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getGenero(): ?string
    {
        return $this->genero;
    }

    public function setGenero(string $genero): self
    {
        $this->genero = $genero;

        return $this;
    }

    public function getColorAgenda(): ?string
    {
        return $this->colorAgenda;
    }

    public function setColorAgenda(string $colorAgenda): self
    {
        $this->colorAgenda = $colorAgenda;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getNumeroContacto(): ?int
    {
        return $this->numeroContacto;
    }

    public function setNumeroContacto(?int $numeroContacto): self
    {
        $this->numeroContacto = $numeroContacto;

        return $this;
    }

    public function getNombreContacto(): ?string
    {
        return $this->nombreContacto;
    }

    public function setNombreContacto(?string $nombreContacto): self
    {
        $this->nombreContacto = $nombreContacto;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

     /* Añadida por mi*/
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }
    public function getMovil(): ?int
    {
        return $this->movil;
    }

    public function setMovil(int $movil): self
    {
        $this->movil = $movil;

        return $this;
    }

    public function getFirmaProfesional(): ?string
    {
        return $this->firmaProfesional;
    }

    public function setFirmaProfesional(?string $firmaProfesional): self
    {
        $this->firmaProfesional = $firmaProfesional;

        return $this;
    }

    public function getImagenUsuario(): ?string
    {
        return $this->imagenUsuario;
    }

    public function setImagenUsuario(?string $imagenUsuario): self
    {
        $this->imagenUsuario = $imagenUsuario;

        return $this;
    }
    /* con este metodo
        Subir un unico archivo*/
    public function uploadUno($archivo){
    // la propiedad file puede quedar vacía
        if(null === $archivo){
            return;
        }

        // se usa el nombre original aquí pero debería sanitizarse
        // el método move recibe el directorio y el archivo
        $fileName=md5(uniqid()).'.'.$archivo->guessExtension();
        $archivo->move('/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/usuario/',$fileName);
        //$this->path=$fileName;
        //$this->setImagenUsuario($this->path);
        $this->setImagenUsuario($fileName);
    }
    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIntento(): ?int
    {
        return $this->intento;
    }

    public function setIntento(int $intento): self
    {
        $this->intento = $intento;

        return $this;
    }

    public function getIdUsuario(): ?int
    {
        return $this->idUsuario;
    }

    public function getTipoentificacion(): ?TipoDocumento
    {
        return $this->tipoentificacion;
    }

    public function setTipoentificacion(?TipoDocumento $tipoentificacion): self
    {
        $this->tipoentificacion = $tipoentificacion;

        return $this;
    }

    public function getIdEspecialidad(): ?Especialidad
    {
        return $this->idEspecialidad;
    }

    public function setIdEspecialidad(?Especialidad $idEspecialidad): self
    {
        $this->idEspecialidad = $idEspecialidad;

        return $this;
    }

    public function getIdAcuerdoPorc(): ?AcuerdoPago
    {
        return $this->idAcuerdoPorc;
    }

    public function setIdAcuerdoPorc(?AcuerdoPago $idAcuerdoPorc): self
    {
        $this->idAcuerdoPorc = $idAcuerdoPorc;

        return $this;
    }

    public function getIdAcuerdoValor(): ?AcuerdoPago
    {
        return $this->idAcuerdoValor;
    }

    public function setIdAcuerdoValor(?AcuerdoPago $idAcuerdoValor): self
    {
        $this->idAcuerdoValor = $idAcuerdoValor;

        return $this;
    }

    public function getIdAgendaUsuario(): ?AgendaUsuario
    {
        return $this->idAgendaUsuario;
    }

    public function setIdAgendaUsuario(?AgendaUsuario $idAgendaUsuario): self
    {
        $this->idAgendaUsuario = $idAgendaUsuario;

        return $this;
    }

    public function getIdPerfil(): ?Perfil
    {
        return $this->idPerfil;
    }

    public function setIdPerfil(?Perfil $idPerfil): self
    {
        $this->idPerfil = $idPerfil;

        return $this;
    }

    public function getIdSucursal(): ?Clinica
    {
        return $this->idSucursal;
    }

    public function setIdSucursal(?Clinica $idSucursal): self
    {
        $this->idSucursal = $idSucursal;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?self
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?self $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

    public function getTipoUsuario(): ?string
    {
        return $this->tipoUsuario;
    }

    public function setTipoUsuario(string $tipoUsuario): self
    {
        $this->tipoUsuario = strtoupper($tipoUsuario);

        return $this;
    }

    /* Adicionadas por mi para cumplir con los 5 metodos necesarios para implementar la securidad con UserInterface*/
   
    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->estado;
    }

    // serialize and unserialize must be updated - see below
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            // ...
            $this->idUsuario,
            $this->username,
            $this->password,
            $this->estado
        ));
    }
     /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            // ...
            $this->idUsuario,
            $this->username,
            $this->password,
            $this->estado
        ) = unserialize($serialized);
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

     public function getRoles()
    {
        return array($this->getIdPerfil()->getNombre());
    }
     public function __toString()
    {
        
          return $this->getNombres().' '. $this->getApellidos();  
        
    } 
}
