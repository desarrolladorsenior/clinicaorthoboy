<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Presentacion
 */     
class Presentacion
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombrePresentacion;
    /**
     * @var boolean
    */
    private $estado;
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 4,
    *      max = 55,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $unidad;
    /**
     * @var integer
    */
    private $idPresentacion;

    public function getNombrePresentacion(): ?string
    {
        return $this->nombrePresentacion;
    }

    public function setNombrePresentacion(string $nombrePresentacion): self
    {
        $this->nombrePresentacion = strtoupper($nombrePresentacion);
        return $this;
    }

    public function getUnidad(): ?string
    {
        return $this->unidad;
    }

    public function setUnidad(string $unidad): self
    {
        $this->unidad = $unidad;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdPresentacion(): ?int
    {
        return $this->idPresentacion;
    }

    public function __toString()
    {
        return ucwords(strtolower($this->getNombrePresentacion())).' ('.$this->getUnidad().')';
    } 
}
