<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Superficie
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
* @UniqueEntity(fields={"convencion"}, message="Este dato ya se encuentra registrado.")
 */  
class Superficie
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;

    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 10,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $convencion;

    /**
     * @var boolean
    */
    private $estado;

    /**
     * @var integer
    */
    private $idSuperficie;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getConvencion(): ?string
    {
        return $this->convencion;
    }

    public function setConvencion(string $convencion): self
    {
        $this->convencion = strtoupper($convencion);

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdSuperficie(): ?int
    {
        return $this->idSuperficie;
    }

    public function __toString()
    {
        return $this->getNombre();  
    } 
}
