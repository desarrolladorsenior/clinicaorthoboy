<?php

namespace App\Entity;

class ExamenGeneral
{
    private $fechaApertura;

    private $educacionGrupal;

    private $valoracionMedicina;

    private $sellante;

    private $detartraje;

    private $fluor;

    private $remocionPlaca;

    private $motivoConsulta;

    private $embarazo;

    private $dolorMuscular;

    private $dolorArtilar;

    private $ruidoArtilar;

    private $alteracionMovimiento;

    private $malOclusion;

    private $crecimientoDesarrollo;

    private $fasetaDesgaste;

    private $nDienteTP;

    private $estadoEstructural;

    private $alteracionFlurosis;

    private $rehabilitacion;

    private $saliva;

    private $placa;

    private $funcionTejido;

    private $antecedenteMedico;

    private $antecedenteFamiliar;

    private $medicamento;

    private $alergia;

    private $factorRiesgo;

    private $determinanteSocial;

    private $poblacionVulnerable;

    private $antecedenteSistemico;

    private $habito;

    private $otro;

    private $cara;

    private $ganglios;

    private $maxilarMandibula;

    private $musculo;

    private $labio;

    private $carillo;

    private $encia;

    private $lengua;

    private $pisoBocal;

    private $paladar;

    private $orfaringe;

    private $idExamenGeneral;

    private $idPaciente;

    private $idUsuarioProfesional;

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }
    
    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }
    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getEducacionGrupal(): ?bool
    {
        return $this->educacionGrupal;
    }

    public function setEducacionGrupal(bool $educacionGrupal): self
    {
        $this->educacionGrupal = $educacionGrupal;

        return $this;
    }

    public function getValoracionMedicina(): ?bool
    {
        return $this->valoracionMedicina;
    }

    public function setValoracionMedicina(bool $valoracionMedicina): self
    {
        $this->valoracionMedicina = $valoracionMedicina;

        return $this;
    }

    public function getSellante(): ?bool
    {
        return $this->sellante;
    }

    public function setSellante(bool $sellante): self
    {
        $this->sellante = $sellante;

        return $this;
    }

    public function getDetartraje(): ?bool
    {
        return $this->detartraje;
    }

    public function setDetartraje(bool $detartraje): self
    {
        $this->detartraje = $detartraje;

        return $this;
    }

    public function getFluor(): ?bool
    {
        return $this->fluor;
    }

    public function setFluor(bool $fluor): self
    {
        $this->fluor = $fluor;

        return $this;
    }

    public function getRemocionPlaca(): ?bool
    {
        return $this->remocionPlaca;
    }

    public function setRemocionPlaca(bool $remocionPlaca): self
    {
        $this->remocionPlaca = $remocionPlaca;

        return $this;
    }

    public function getMotivoConsulta(): ?string
    {
        return $this->motivoConsulta;
    }

    public function setMotivoConsulta(string $motivoConsulta): self
    {
        $this->motivoConsulta = $motivoConsulta;

        return $this;
    }

    public function getEmbarazo(): ?bool
    {
        return $this->embarazo;
    }

    public function setEmbarazo(bool $embarazo): self
    {
        $this->embarazo = $embarazo;

        return $this;
    }

    public function getDolorMuscular(): ?string
    {
        return $this->dolorMuscular;
    }

    public function setDolorMuscular(?string $dolorMuscular): self
    {
        $this->dolorMuscular = $dolorMuscular;

        return $this;
    }

    public function getDolorArtilar(): ?string
    {
        return $this->dolorArtilar;
    }

    public function setDolorArtilar(?string $dolorArtilar): self
    {
        $this->dolorArtilar = $dolorArtilar;

        return $this;
    }

    public function getRuidoArtilar(): ?string
    {
        return $this->ruidoArtilar;
    }

    public function setRuidoArtilar(?string $ruidoArtilar): self
    {
        $this->ruidoArtilar = $ruidoArtilar;

        return $this;
    }

    public function getAlteracionMovimiento(): ?string
    {
        return $this->alteracionMovimiento;
    }

    public function setAlteracionMovimiento(?string $alteracionMovimiento): self
    {
        $this->alteracionMovimiento = $alteracionMovimiento;

        return $this;
    }

    public function getMalOclusion(): ?string
    {
        return $this->malOclusion;
    }

    public function setMalOclusion(?string $malOclusion): self
    {
        $this->malOclusion = $malOclusion;

        return $this;
    }

    public function getCrecimientoDesarrollo(): ?string
    {
        return $this->crecimientoDesarrollo;
    }

    public function setCrecimientoDesarrollo(?string $crecimientoDesarrollo): self
    {
        $this->crecimientoDesarrollo = $crecimientoDesarrollo;

        return $this;
    }

    public function getFasetaDesgaste(): ?string
    {
        return $this->fasetaDesgaste;
    }

    public function setFasetaDesgaste(?string $fasetaDesgaste): self
    {
        $this->fasetaDesgaste = $fasetaDesgaste;

        return $this;
    }

    public function getNDienteTP(): ?string
    {
        return $this->nDienteTP;
    }

    public function setNDienteTP(?string $nDienteTP): self
    {
        $this->nDienteTP = $nDienteTP;

        return $this;
    }

    public function getEstadoEstructural(): ?string
    {
        return $this->estadoEstructural;
    }

    public function setEstadoEstructural(?string $estadoEstructural): self
    {
        $this->estadoEstructural = $estadoEstructural;

        return $this;
    }

    public function getAlteracionFlurosis(): ?string
    {
        return $this->alteracionFlurosis;
    }

    public function setAlteracionFlurosis(?string $alteracionFlurosis): self
    {
        $this->alteracionFlurosis = $alteracionFlurosis;

        return $this;
    }

    public function getRehabilitacion(): ?string
    {
        return $this->rehabilitacion;
    }

    public function setRehabilitacion(?string $rehabilitacion): self
    {
        $this->rehabilitacion = $rehabilitacion;

        return $this;
    }

    public function getSaliva(): ?string
    {
        return $this->saliva;
    }

    public function setSaliva(?string $saliva): self
    {
        $this->saliva = $saliva;

        return $this;
    }

    public function getPlaca(): ?string
    {
        return $this->placa;
    }

    public function setPlaca(?string $placa): self
    {
        $this->placa = $placa;

        return $this;
    }

    public function getFuncionTejido(): ?string
    {
        return $this->funcionTejido;
    }

    public function setFuncionTejido(?string $funcionTejido): self
    {
        $this->funcionTejido = $funcionTejido;

        return $this;
    }

    public function getAntecedenteMedico(): ?string
    {
        return $this->antecedenteMedico;
    }

    public function setAntecedenteMedico(?string $antecedenteMedico): self
    {
        $this->antecedenteMedico = $antecedenteMedico;

        return $this;
    }

    public function getAntecedenteFamiliar(): ?string
    {
        return $this->antecedenteFamiliar;
    }

    public function setAntecedenteFamiliar(?string $antecedenteFamiliar): self
    {
        $this->antecedenteFamiliar = $antecedenteFamiliar;

        return $this;
    }

    public function getMedicamento(): ?string
    {
        return $this->medicamento;
    }

    public function setMedicamento(?string $medicamento): self
    {
        $this->medicamento = $medicamento;

        return $this;
    }

    public function getAlergia(): ?string
    {
        return $this->alergia;
    }

    public function setAlergia(?string $alergia): self
    {
        $this->alergia = $alergia;

        return $this;
    }

    public function getFactorRiesgo(): ?string
    {
        return $this->factorRiesgo;
    }

    public function setFactorRiesgo(?string $factorRiesgo): self
    {
        $this->factorRiesgo = $factorRiesgo;

        return $this;
    }

    public function getDeterminanteSocial(): ?string
    {
        return $this->determinanteSocial;
    }

    public function setDeterminanteSocial(?string $determinanteSocial): self
    {
        $this->determinanteSocial = $determinanteSocial;

        return $this;
    }

    public function getPoblacionVulnerable(): ?string
    {
        return $this->poblacionVulnerable;
    }

    public function setPoblacionVulnerable(?string $poblacionVulnerable): self
    {
        $this->poblacionVulnerable = $poblacionVulnerable;

        return $this;
    }

    public function getAntecedenteSistemico(): ?string
    {
        return $this->antecedenteSistemico;
    }

    public function setAntecedenteSistemico(?string $antecedenteSistemico): self
    {
        $this->antecedenteSistemico = $antecedenteSistemico;

        return $this;
    }

    public function getHabito(): ?string
    {
        return $this->habito;
    }

    public function setHabito(?string $habito): self
    {
        $this->habito = $habito;

        return $this;
    }

    public function getOtro(): ?string
    {
        return $this->otro;
    }

    public function setOtro(?string $otro): self
    {
        $this->otro = $otro;

        return $this;
    }

    public function getCara(): ?string
    {
        return $this->cara;
    }

    public function setCara(?string $cara): self
    {
        $this->cara = $cara;

        return $this;
    }

    public function getGanglios(): ?string
    {
        return $this->ganglios;
    }

    public function setGanglios(?string $ganglios): self
    {
        $this->ganglios = $ganglios;

        return $this;
    }

    public function getMaxilarMandibula(): ?string
    {
        return $this->maxilarMandibula;
    }

    public function setMaxilarMandibula(?string $maxilarMandibula): self
    {
        $this->maxilarMandibula = $maxilarMandibula;

        return $this;
    }

    public function getMusculo(): ?string
    {
        return $this->musculo;
    }

    public function setMusculo(?string $musculo): self
    {
        $this->musculo = $musculo;

        return $this;
    }

    public function getLabio(): ?string
    {
        return $this->labio;
    }

    public function setLabio(?string $labio): self
    {
        $this->labio = $labio;

        return $this;
    }

    public function getCarillo(): ?string
    {
        return $this->carillo;
    }

    public function setCarillo(?string $carillo): self
    {
        $this->carillo = $carillo;

        return $this;
    }

    public function getEncia(): ?string
    {
        return $this->encia;
    }

    public function setEncia(?string $encia): self
    {
        $this->encia = $encia;

        return $this;
    }

    public function getLengua(): ?string
    {
        return $this->lengua;
    }

    public function setLengua(?string $lengua): self
    {
        $this->lengua = $lengua;

        return $this;
    }

    public function getPisoBocal(): ?string
    {
        return $this->pisoBocal;
    }

    public function setPisoBocal(?string $pisoBocal): self
    {
        $this->pisoBocal = $pisoBocal;

        return $this;
    }

    public function getPaladar(): ?string
    {
        return $this->paladar;
    }

    public function setPaladar(?string $paladar): self
    {
        $this->paladar = $paladar;

        return $this;
    }

    public function getOrfaringe(): ?string
    {
        return $this->orfaringe;
    }

    public function setOrfaringe(?string $orfaringe): self
    {
        $this->orfaringe = $orfaringe;

        return $this;
    }

    public function getIdExamenGeneral(): ?int
    {
        return $this->idExamenGeneral;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdUsuarioProfesional(): ?Usuario
    {
        return $this->idUsuarioProfesional;
    }

    public function setIdUsuarioProfesional(?Usuario $idUsuarioProfesional): self
    {
        $this->idUsuarioProfesional = $idUsuarioProfesional;

        return $this;
    }
}
