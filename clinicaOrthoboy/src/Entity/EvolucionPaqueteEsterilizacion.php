<?php

namespace App\Entity;

class EvolucionPaqueteEsterilizacion
{
    private $idEvolucionPaqueteEsterilizacion;

    private $idEvolucion;

    private $idPaqueteEsterilizacion;

    public function getIdEvolucionPaqueteEsterilizacion(): ?int
    {
        return $this->idEvolucionPaqueteEsterilizacion;
    }

    public function getIdEvolucion(): ?Evolucion
    {
        return $this->idEvolucion;
    }

    public function setIdEvolucion(?Evolucion $idEvolucion): self
    {
        $this->idEvolucion = $idEvolucion;

        return $this;
    }

    public function getIdPaqueteEsterilizacion(): ?PaqueteEsterilizacion
    {
        return $this->idPaqueteEsterilizacion;
    }

    public function setIdPaqueteEsterilizacion(?PaqueteEsterilizacion $idPaqueteEsterilizacion): self
    {
        $this->idPaqueteEsterilizacion = $idPaqueteEsterilizacion;

        return $this;
    }
}
