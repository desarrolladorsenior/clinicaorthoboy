<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Tratamiento
 */     
class Tratamiento
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;
    /**
    * @var bigint
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 10,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $valor;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idTratamiento;
    /**
     * @var \App\Entity\CategoriaTratamiento
    */
    private $idCategoriaTratamiento;
    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre =  strtoupper($nombre);

        return $this;
    }

    public function getValor(): ?int
    {
        return $this->valor;
    }

    public function setValor(int $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdTratamiento(): ?int
    {
        return $this->idTratamiento;
    }

    public function getIdCategoriaTratamiento(): ?CategoriaTratamiento
    {
        return $this->idCategoriaTratamiento;
    }

    public function setIdCategoriaTratamiento(?CategoriaTratamiento $idCategoriaTratamiento): self
    {
        $this->idCategoriaTratamiento = $idCategoriaTratamiento;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre().'- Categoría: '.$this->getIdCategoriaTratamiento();
    }
}
