<?php

namespace App\Entity;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
/**
 * Salida
 */     
class Salida
{
    /**
     * @var integer
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Se permiten unicamente números"),
    */
    private $cantidad;
    /**
     * @var dataTime
    */
    private $fechaApertura;
    /**
     * @var integer
    */
    private $idSalida;
    /**
     * @var \App\Entity\EntradaPedido
    */
    private $idEntradaPedido;
    /**
     * @var \App\Entity\Consultorio
    */
    private $idConsultorio;
    /**
     * @var \App\Entity\Paciente
    */
    private $idPaciente;
    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }
    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getIdSalida(): ?int
    {
        return $this->idSalida;
    }

    public function getIdEntradaPedido(): ?EntradaPedido
    {
        return $this->idEntradaPedido;
    }

    public function setIdEntradaPedido(?EntradaPedido $idEntradaPedido): self
    {
        $this->idEntradaPedido = $idEntradaPedido;

        return $this;
    }

    public function getIdConsultorio(): ?Consultorio
    {
        return $this->idConsultorio;
    }

    public function setIdConsultorio(?Consultorio $idConsultorio): self
    {
        $this->idConsultorio = $idConsultorio;

        return $this;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }
    public function __toString()
    {
         return 'Salida N°: '.$this->getIdSalida().' - '.$this->getIdEntradaPedido();
    } 
}
