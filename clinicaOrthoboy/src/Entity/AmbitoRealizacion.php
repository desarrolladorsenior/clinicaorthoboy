<?php

namespace App\Entity;


use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * AmbitoRealizacion
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */     

class AmbitoRealizacion
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;
    /**
     * @var integer
    */
    private $idAmbitoRealizacion;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getIdAmbitoRealizacion(): ?int
    {
        return $this->idAmbitoRealizacion;
    }

     public function __toString()
    {
        return $this->getNombre();  
    } 
}
