<?php

namespace App\Entity;

class OdontogramaPaciente
{
    private $idOdontogramaPaciente;

    private $idOdontograma;

    private $idPieza;

    private $idSuperficie;

    private $idRipTipoOdon;

    private $idDiagPrincipal;

    private $idUsuarioProfesional;

    public function getIdOdontogramaPaciente(): ?int
    {
        return $this->idOdontogramaPaciente;
    }

    public function getIdOdontograma(): ?Odontograma
    {
        return $this->idOdontograma;
    }

    public function setIdOdontograma(?Odontograma $idOdontograma): self
    {
        $this->idOdontograma = $idOdontograma;

        return $this;
    }

    public function getIdPieza(): ?PiezaDental
    {
        return $this->idPieza;
    }

    public function setIdPieza(?PiezaDental $idPieza): self
    {
        $this->idPieza = $idPieza;

        return $this;
    }

    public function getIdSuperficie(): ?Superficie
    {
        return $this->idSuperficie;
    }

    public function setIdSuperficie(?Superficie $idSuperficie): self
    {
        $this->idSuperficie = $idSuperficie;

        return $this;
    }

    public function getIdRipTipoOdon(): ?CieDiagnosticoTipo
    {
        return $this->idRipTipoOdon;
    }

    public function setIdRipTipoOdon(?CieDiagnosticoTipo $idRipTipoOdon): self
    {
        $this->idRipTipoOdon = $idRipTipoOdon;

        return $this;
    }

    public function getIdDiagPrincipal(): ?DiagnosticoPrincipal
    {
        return $this->idDiagPrincipal;
    }

    public function setIdDiagPrincipal(?DiagnosticoPrincipal $idDiagPrincipal): self
    {
        $this->idDiagPrincipal = $idDiagPrincipal;

        return $this;
    }

    public function getIdUsuarioProfesional(): ?Usuario
    {
        return $this->idUsuarioProfesional;
    }

    public function setIdUsuarioProfesional(?Usuario $idUsuarioProfesional): self
    {
        $this->idUsuarioProfesional = $idUsuarioProfesional;

        return $this;
    }
}
