<?php

namespace App\Entity;

class Pago
{
    private $valorPago;

    private $observacion;

    private $fechaPago;

    private $idPago;

    private $idPaciente;

    private $idPlanTratamiento;

    private $idSoportePago;

    private $idFormaPago;

    private $idConceptoPago;

    private $idEstadoPago;

    private $idUsuarioIngresa;

    public function getValorPago(): ?int
    {
        return $this->valorPago;
    }

    public function setValorPago(int $valorPago): self
    {
        $this->valorPago = $valorPago;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getFechaPago(): ?\DateTimeInterface
    {
        return $this->fechaPago;
    }

    public function setFechaPago(\DateTimeInterface $fechaPago): self
    {
        $this->fechaPago = $fechaPago;

        return $this;
    }

    public function getIdPago(): ?int
    {
        return $this->idPago;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdPlanTratamiento(): ?PlanTratamiento
    {
        return $this->idPlanTratamiento;
    }

    public function setIdPlanTratamiento(?PlanTratamiento $idPlanTratamiento): self
    {
        $this->idPlanTratamiento = $idPlanTratamiento;

        return $this;
    }

    public function getIdSoportePago(): ?SoportePago
    {
        return $this->idSoportePago;
    }

    public function setIdSoportePago(?SoportePago $idSoportePago): self
    {
        $this->idSoportePago = $idSoportePago;

        return $this;
    }

    public function getIdFormaPago(): ?FormaPago
    {
        return $this->idFormaPago;
    }

    public function setIdFormaPago(?FormaPago $idFormaPago): self
    {
        $this->idFormaPago = $idFormaPago;

        return $this;
    }

    public function getIdConceptoPago(): ?ConceptoPago
    {
        return $this->idConceptoPago;
    }

    public function setIdConceptoPago(?ConceptoPago $idConceptoPago): self
    {
        $this->idConceptoPago = $idConceptoPago;

        return $this;
    }

    public function getIdEstadoPago(): ?EstadoPago
    {
        return $this->idEstadoPago;
    }

    public function setIdEstadoPago(?EstadoPago $idEstadoPago): self
    {
        $this->idEstadoPago = $idEstadoPago;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }
}
