<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * OrdenCompra
 */
class OrdenCompra
{
    
    /**    
     * @var \DateTime 
     */
    private $fechaApertura;
    /**
     * @var boolean
    */
    private $estadoEnvioCorreo;
    /**
     * @var integer
    */
    private $idOrdenCompra;
    /**
     * @var \App\Entity\Proveedor
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idProveedor;
    /**
     * @var \App\Entity\Usuario
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idUsuarioIngresa;

    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getEstadoEnvioCorreo(): ?bool
    {
        return $this->estadoEnvioCorreo;
    }

    public function setEstadoEnvioCorreo(bool $estadoEnvioCorreo): self
    {
        $this->estadoEnvioCorreo = $estadoEnvioCorreo;

        return $this;
    }

    public function getIdOrdenCompra(): ?int
    {
        return $this->idOrdenCompra;
    }

    public function getIdProveedor(): ?Proveedor
    {
        return $this->idProveedor;
    }

    public function setIdProveedor(?Proveedor $idProveedor): self
    {
        $this->idProveedor = $idProveedor;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }


    public function __toString()
    {
        return 'Orden N°: '.$this->getIdOrdenCompra().' - Proveedor: '.$this->getIdProveedor();
    }
}
