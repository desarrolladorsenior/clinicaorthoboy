<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Remision
 */
class Remision
{
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $observacion;
    /**
     * @var \DateTime
    */
    private $fechaApertura;
    /**
     * @var \DateTime
    */
    private $fechaRealizar;
    /**
     * @var integer
    */
    private $idRemision;

    /**
     * @var \App\Entity\Paciente
    */
    private $idPaciente;

    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioRemite;
    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioRemitido;
    /**
     * @var \App\Entity\Cups
    */
    private $idProcedimiento;

    private $email;

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

     /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }
    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getFechaRealizar(): ?\DateTimeInterface
    {
        return $this->fechaRealizar;
    }

    public function setFechaRealizar(\DateTimeInterface $fechaRealizar): self
    {
        $this->fechaRealizar = $fechaRealizar;

        return $this;
    }
     public function getIdUsuarioRemite(): ?Usuario
    {
        return $this->idUsuarioRemite;
    }

    public function setIdUsuarioRemite(?USuario $idUsuarioRemite): self
    {
        $this->idUsuarioRemite = $idUsuarioRemite;

        return $this;
    }

    public function getIdUsuarioRemitido(): ?Usuario
    {
        return $this->idUsuarioRemitido;
    }

    public function setIdUsuarioRemitido(?Usuario $idUsuarioRemitido): self
    {
        $this->idUsuarioRemitido = $idUsuarioRemitido;

        return $this;
    }


    public function getIdRemision(): ?int
    {
        return $this->idRemision;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

   

    public function getIdProcedimiento(): ?Cups
    {
        return $this->idProcedimiento;
    }

    public function setIdProcedimiento(?Cups $idProcedimiento): self
    {
        $this->idProcedimiento = $idProcedimiento;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function __toString()
    {
        
          return $this->getIdRemision().' - '. $this->getIdProcedimiento();  
        
    } 
}
