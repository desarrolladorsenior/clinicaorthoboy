<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Consultorio
 */     

class Consultorio
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;

    /**
     * @var boolean
    */
    private $estado;

    /**
     * @var integer
    */
    private $idConsultorio;

    /**
     * @var \App\Entity\Clinica
    */
    private $idClinica;

    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;


    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdConsultorio(): ?int
    {
        return $this->idConsultorio;
    }

    public function getIdClinica(): ?Clinica
    {
        return $this->idClinica;
    }

    public function setIdClinica(?Clinica $idClinica): self
    {
        $this->idClinica = $idClinica;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?USuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }
    public function __toString()
    {
        if ($this->getIdClinica()->getIdSedeClinica() != '') {
            return $this->getNombre().' - Sede: '.$this->getIdClinica()->getIdSedeClinica().' - '.$this->getIdClinica()->getIdMunicipio();
        }else{
            return $this->getNombre().' - Sede: '.$this->getIdClinica().' - '.$this->getIdClinica()->getIdMunicipio();  
        }
    } 
}
