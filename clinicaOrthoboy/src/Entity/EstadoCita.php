<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
/**
 * EstadoCita
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */
class EstadoCita
{
     /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )  
     */ 
    private $nombre;
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )  
     */ 
    private $icono;
    /**
     * @var boolean
    */
    private $enviarCorreo;
    /**
     * @var integer
    */
    private $idEstadoCita;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getIcono(): ?string
    {
        return $this->icono;
    }

    public function setIcono(string $icono): self
    {
        $this->icono = $icono;

        return $this;
    }

    public function getEnviarCorreo(): ?bool
    {
        return $this->enviarCorreo;
    }

    public function setEnviarCorreo(bool $enviarCorreo): self
    {
        $this->enviarCorreo = $enviarCorreo;

        return $this;
    }

    public function getIdEstadoCita(): ?int
    {
        return $this->idEstadoCita;
    }
    
    public function __toString()
    {
        return $this->getNombre();  
    } 
}
