<?php

namespace App\Entity;

class PeriodontogramaPaciente
{
    private $sangramiento;

    private $supuracion;

    private $movilidad;

    private $furca;

    private $posicionEncia;

    private $profundidadSaco;

    private $factor;

    private $efectoProvocado;

    private $tamano;

    private $posicion;

    private $forma;

    private $color;

    private $nivelInsercion;

    private $idPeriodontogramaPaciente;

    private $idPeriodontograma;

    private $idPieza;

    public function getSangramiento(): ?string
    {
        return $this->sangramiento;
    }

    public function setSangramiento(?string $sangramiento): self
    {
        $this->sangramiento = $sangramiento;

        return $this;
    }

    public function getSupuracion(): ?string
    {
        return $this->supuracion;
    }

    public function setSupuracion(?string $supuracion): self
    {
        $this->supuracion = $supuracion;

        return $this;
    }

    public function getMovilidad(): ?int
    {
        return $this->movilidad;
    }

    public function setMovilidad(?int $movilidad): self
    {
        $this->movilidad = $movilidad;

        return $this;
    }

    public function getFurca(): ?string
    {
        return $this->furca;
    }

    public function setFurca(?string $furca): self
    {
        $this->furca = $furca;

        return $this;
    }

    public function getPosicionEncia(): ?string
    {
        return $this->posicionEncia;
    }

    public function setPosicionEncia(?string $posicionEncia): self
    {
        $this->posicionEncia = $posicionEncia;

        return $this;
    }

    public function getProfundidadSaco(): ?string
    {
        return $this->profundidadSaco;
    }

    public function setProfundidadSaco(?string $profundidadSaco): self
    {
        $this->profundidadSaco = $profundidadSaco;

        return $this;
    }

    public function getFactor(): ?string
    {
        return $this->factor;
    }

    public function setFactor(?string $factor): self
    {
        $this->factor = $factor;

        return $this;
    }

    public function getEfectoProvocado(): ?string
    {
        return $this->efectoProvocado;
    }

    public function setEfectoProvocado(?string $efectoProvocado): self
    {
        $this->efectoProvocado = $efectoProvocado;

        return $this;
    }

    public function getTamano(): ?string
    {
        return $this->tamano;
    }

    public function setTamano(?string $tamano): self
    {
        $this->tamano = $tamano;

        return $this;
    }

    public function getPosicion(): ?string
    {
        return $this->posicion;
    }

    public function setPosicion(?string $posicion): self
    {
        $this->posicion = $posicion;

        return $this;
    }

    public function getForma(): ?string
    {
        return $this->forma;
    }

    public function setForma(?string $forma): self
    {
        $this->forma = $forma;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getNivelInsercion(): ?string
    {
        return $this->nivelInsercion;
    }

    public function setNivelInsercion(?string $nivelInsercion): self
    {
        $this->nivelInsercion = $nivelInsercion;

        return $this;
    }

    public function getIdPeriodontogramaPaciente(): ?int
    {
        return $this->idPeriodontogramaPaciente;
    }

    public function getIdPeriodontograma(): ?Periodontograma
    {
        return $this->idPeriodontograma;
    }

    public function setIdPeriodontograma(?Periodontograma $idPeriodontograma): self
    {
        $this->idPeriodontograma = $idPeriodontograma;

        return $this;
    }

    public function getIdPieza(): ?PiezaDental
    {
        return $this->idPieza;
    }

    public function setIdPieza(?PiezaDental $idPieza): self
    {
        $this->idPieza = $idPieza;

        return $this;
    }
}
