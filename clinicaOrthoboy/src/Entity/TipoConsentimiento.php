<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Tipo Consentimiento
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */     
class TipoConsentimiento
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $contenido;
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 300,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $riesgo;
    /**
     * @var integer
    */
    private $idTipoConsentimiento;
    /**
     * @var \App\Entity\TipoRadiografia
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idCie;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getContenido(): ?string
    {
        return $this->contenido;
    }

    public function setContenido(string $contenido): self
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getRiesgo(): ?string
    {
        return $this->riesgo;
    }

    public function setRiesgo(string $riesgo): self
    {
        $this->riesgo = $riesgo;

        return $this;
    }

    public function getIdTipoConsentimiento(): ?int
    {
        return $this->idTipoConsentimiento;
    }

    public function getIdCie(): ?CieRip
    {
        return $this->idCie;
    }

    public function setIdCie(?CieRip $idCie): self
    {
        $this->idCie = $idCie;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();        
    }
}
