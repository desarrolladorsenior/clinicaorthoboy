<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Convenio
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */
class Convenio
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $nombre;

    /**
    * @var Date
    */
    private $fechaApertura;

    /**
    * @var Date
    * @Assert\GreaterThan(propertyPath="fechaApertura")
    */
    private $fechaFinalizacion;
    /**
     * @var integer
     * @Assert\Length(
     *      min = 1,
     *      max = 3,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $porcentajeDescuento;

     /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $observacion;

    /**
     * @var boolean
    */
    private $estado;
    
    /**
     * @var \App\Entity\Eps
    */
    private $idEps;
    /**
     * @var integer
     * @Assert\Length(
     *      min = 1,
     *      max = 3,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $porcentajeRemision;
    /**
     * @var \App\Entity\Clinica
    */
    private $idClinica;

    /**
     * @var integer
    */
    private $idConvenio;

    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    /*public function __construct()
    {
        $this->fechaApertura = new \DateTime();
    }*/

    public function __toString()
    {
        return $this->getNombre();
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getPorcentajeRemision(): ?int
    {
        return $this->porcentajeRemision;
    }

    public function setPorcentajeRemision(?int $porcentajeRemision): self
    {
        $this->porcentajeRemision = $porcentajeRemision;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getPorcentajeDescuento(): ?int
    {
        return $this->porcentajeDescuento;
    }

    public function setPorcentajeDescuento(?int $porcentajeDescuento): self
    {
        $this->porcentajeDescuento = $porcentajeDescuento;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaFinalizacion(): ?\DateTimeInterface
    {
        return $this->fechaFinalizacion;
    }

    public function setFechaFinalizacion(\DateTimeInterface $fechaFinalizacion): self
    {
        $this->fechaFinalizacion = $fechaFinalizacion;

        return $this;
    }

    public function getIdConvenio(): ?int
    {
        return $this->idConvenio;
    }

    public function getIdClinica(): ?Clinica
    {
        return $this->idClinica;
    }

    public function setIdClinica(?Clinica $idClinica): self
    {
        $this->idClinica = $idClinica;

        return $this;
    }

    public function getIdEps(): ?Eps
    {
        return $this->idEps;
    }

    public function setIdEps(?Eps $idEps): self
    {
        $this->idEps = $idEps;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }
}

   

