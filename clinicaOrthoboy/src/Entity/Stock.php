<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Stock
 */     
class Stock
{
    /**
    * @var integer
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 10,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $cantidad;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idStock;
    /**
     * @var \App\Entity\NombreInsumo
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idNombreInsumo;
    /**
     * @var \App\Entity\Ckinica
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idClinica;

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdStock(): ?int
    {
        return $this->idStock;
    }

    public function getIdNombreInsumo(): ?NombreInsumo
    {
        return $this->idNombreInsumo;
    }

    public function setIdNombreInsumo(?NombreInsumo $idNombreInsumo): self
    {
        $this->idNombreInsumo = $idNombreInsumo;

        return $this;
    }

    public function getIdClinica(): ?Clinica
    {
        return $this->idClinica;
    }

    public function setIdClinica(?Clinica $idClinica): self
    {
        $this->idClinica = $idClinica;

        return $this;
    }
    public function __toString()
    {
        return $this->getIdNombreInsumo();
    } 
}
