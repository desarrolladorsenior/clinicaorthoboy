<?php

namespace App\Entity;

class DiagnosticoPeriodontograma
{
    private $diagnostico;

    private $fundamento;

    private $planTratamiento;

    private $idDiagnosticoPeriodontograma;

    private $idPeriodontograma;

    public function getDiagnostico(): ?string
    {
        return $this->diagnostico;
    }

    public function setDiagnostico(string $diagnostico): self
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    public function getFundamento(): ?string
    {
        return $this->fundamento;
    }

    public function setFundamento(string $fundamento): self
    {
        $this->fundamento = $fundamento;

        return $this;
    }

    public function getPlanTratamiento(): ?string
    {
        return $this->planTratamiento;
    }

    public function setPlanTratamiento(string $planTratamiento): self
    {
        $this->planTratamiento = $planTratamiento;

        return $this;
    }

    public function getIdDiagnosticoPeriodontograma(): ?int
    {
        return $this->idDiagnosticoPeriodontograma;
    }

    public function getIdPeriodontograma(): ?Periodontograma
    {
        return $this->idPeriodontograma;
    }

    public function setIdPeriodontograma(?Periodontograma $idPeriodontograma): self
    {
        $this->idPeriodontograma = $idPeriodontograma;

        return $this;
    }
}
