<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Perfil
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */     
class Perfil 
{

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $nombre;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idPerfil;

    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;
   

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

   
    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdPerfil(): ?int
    {
        return $this->idPerfil;
    }

    public function getIdUsuarioIngresa(): ?self
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?self $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }


    public function __toString()
    {
        $separacion= explode ('_' , $this->getNombre());
        return $separacion[1];  
        
    } 

    


}
