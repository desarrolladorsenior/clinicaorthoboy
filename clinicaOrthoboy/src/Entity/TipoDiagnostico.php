<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Superficie
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */  

class TipoDiagnostico
{
     /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;
     /**
     * @var integer
    */
    private $idTipoDiagnostico;

    /**
     * @var \App\Entity\Area
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idArea;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getIdTipoDiagnostico(): ?int
    {
        return $this->idTipoDiagnostico;
    }

    public function getIdArea(): ?Area
    {
        return $this->idArea;
    }

    public function setIdArea(?Area $idArea): self
    {
        $this->idArea = $idArea;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();  
    } 
}
