<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Requerimiento
 */
class Requerimiento
{
    /**
     * @var dataTime
    */
    private $fechaApertura;
    /**
     * @var integer
    */
    private $idRequerimiento;
    /**
     * @var \App\Entity\Clinica
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idSedeClinica;
    /**
     * @var \App\Entity\Usuario
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idUsuarioIngresa;
    
    /**
    * @var string
    */
    private $estado;

    public function getIdRequerimiento(): ?int
    {
        return $this->idRequerimiento;
    }
   
    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }
     public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = strtoupper($estado);

        return $this;
    }

 /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getIdSedeClinica(): ?Clinica
    {
        return $this->idSedeClinica;
    }

    public function setIdSedeClinica(?Clinica $idSedeClinica): self
    {
        $this->idSedeClinica = $idSedeClinica;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

    public function __toString()
    {
         return 'Pedido N°: '.$this->getIdRequerimiento().' - '.$this->getIdSedeClinica();
    } 
}
