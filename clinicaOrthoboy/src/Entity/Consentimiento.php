<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Consentimiento
 */     
class Consentimiento
{
    private $fechaApertura;
    /**
     * @var string
    */
    private $firmaPaciente;
    /**
     * @var string
    */
    private $huellaPaciente;
    /**
     * @var integer
    */
    private $idConsentimiento;
    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuario;
    /**
    * @var \App\Entity\Paciente
    */
    private $idPaciente;
    /**
    * @var \App\Entity\TipoConsentimiento
    */
    private $idTipoConsentimiento;
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )   
     */
    private $nombreAcudiente;
    /**
     * @var integer
    */
    private $cedularAcudiente;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * ) 
    */
    private $parentesco;
    /**
     * @var integer
    */
    private $cedularResponsable;

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }
    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getFirmaPaciente(): ?string
    {
        return $this->firmaPaciente;
    }

    public function setFirmaPaciente(string $firmaPaciente): self
    {
        $this->firmaPaciente = $firmaPaciente;

        return $this;
    }

    public function getHuellaPaciente(): ?string
    {
        return $this->huellaPaciente;
    }

    public function setHuellaPaciente(string $huellaPaciente): self
    {
        $this->huellaPaciente = $huellaPaciente;

        return $this;
    }

    public function getIdConsentimiento(): ?int
    {
        return $this->idConsentimiento;
    }

    public function getIdUsuario(): ?Usuario
    {
        return $this->idUsuario;
    }

    public function setIdUsuario(?Usuario $idUsuario): self
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdTipoConsentimiento(): ?TipoConsentimiento
    {
        return $this->idTipoConsentimiento;
    }

    public function setIdTipoConsentimiento(?TipoConsentimiento $idTipoConsentimiento): self
    {
        $this->idTipoConsentimiento = $idTipoConsentimiento;

        return $this;
    }

    public function getNombreAcudiente(): ?string
    {
        return $this->nombreAcudiente;
    }

    public function setNombreAcudiente(?string $nombreAcudiente): self
    {
        $this->nombreAcudiente = $nombreAcudiente;

        return $this;
    }

    public function getCedularAcudiente(): ?string
    {
        return $this->cedularAcudiente;
    }

    public function setCedularAcudiente(?string $cedularAcudiente): self
    {
        $this->cedularAcudiente = $cedularAcudiente;

        return $this;
    }

    public function getParentesco(): ?string
    {
        return $this->parentesco;
    }

    public function setParentesco(?string $parentesco): self
    {
        $this->parentesco = $parentesco;

        return $this;
    }

    public function getCedularResponsable(): ?string
    {
        return $this->cedularResponsable;
    }

    public function setCedularResponsable(?string $cedularResponsable): self
    {
        $this->cedularResponsable = $cedularResponsable;

        return $this;
    }
}
