<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
/**
 * AnexoPaciente
 */     

class AnexoPaciente
{
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $nombre;

    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $descripcion;

    /**
     * @var \DateTime
     */
    private $fechaApertura;

    /**
     * @var integer
    */
    private $idAnexoPaciente;
    /**
     * @var \App\Entity\Paciente
    */
    private $idPaciente;
    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    /**
     * @var \App\Entity\Remision
    */
    private $idRemision;

    /**
     * @var string
     * @Assert\File(
     *maxSize = "25m",
     *mimeTypes={ "application/pdf","application/xml","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.oasis.opendocument.spreadsheet","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/msword","text/plain","application/vnd.oasis.opendocument.text","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/zip","application/x-rar","application/x-tar","image/*","video/mp4","video/mpeg","audio/x-wav","audio/basic","docx":"application/wps-office.docx","pptx":"application/wps-office.pptx","application/wps-office.xlsx"},
     * mimeTypesMessage = "")
     */
    private $archivo;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }

    public function getIdAnexoPaciente(): ?int
    {
        return $this->idAnexoPaciente;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

    public function getIdRemision(): ?Remision
    {
        return $this->idRemision;
    }

    public function setIdRemision(?Remision $idRemision): self
    {
        $this->idRemision = $idRemision;

        return $this;
    }

    public function getArchivo(): ?string
    {
        return $this->archivo;
    }

    public function setArchivo(?string $archivo): self
    {
        $this->archivo = $archivo;

        return $this;
    }

    /* con este metodo
        Subir un unico archivo*/
    public function uploadUno($archivo){
    // la propiedad file puede quedar vacía
        if(null === $archivo){
            return;
        }

        // se usa el nombre original aquí pero debería sanitizarse
        // el método move recibe el directorio y el archivo
        //$fileName=md5(uniqid()).'.'.$archivo->guessExtension();
        $fileName=$archivo->getClientOriginalName();
        $archivo->move('/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/anexoPaciente/',$fileName);
        $this->path=$fileName;
        $this->setArchivo($this->path);
    }

    public function __toString()
    {
        return $this->getNombre(); 
    } 
}
