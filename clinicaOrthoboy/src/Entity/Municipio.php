<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Municipio
 * @UniqueEntity(fields={"id"}, message="Este dato ya se encuentra registrado.")
 */
class Municipio 
{

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )    
     */
    private $nombre;
    
    /**
     * @var integer
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 1,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )  
    */
    private $id;

    /**
     * @var \App\Entity\Departamento
    */
    private $idDepartamento;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre =  ucwords(strtolower($nombre));

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id)
    {
    $this->id = $id;
    }
    
    public function getIdDepartamento(): ?Departamento
    {
        return $this->idDepartamento;
    }

    public function setIdDepartamento(?Departamento $idDepartamento): self
    {
        $this->idDepartamento = $idDepartamento;

        return $this;
    }
    
    public function __toString()
    {
    return $this->getNombre().' ( '.$this->getIdDepartamento().' )';
    }

}
