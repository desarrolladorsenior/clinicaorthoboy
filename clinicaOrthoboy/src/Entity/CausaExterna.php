<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * CausaExterna
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */  
class CausaExterna
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;
    /**
     * @var integer
    */
    private $idCausaExterna;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getIdCausaExterna(): ?int
    {
        return $this->idCausaExterna;
    }

     public function __toString()
    {
        return $this->getNombre();  
    } 
}
