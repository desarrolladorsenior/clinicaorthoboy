<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * PaqueteEsterilizacion
 */
class PaqueteEsterilizacion
{
     /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     */
    private $codigo;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idPaqueteEsterilizacion;
    /**
     * @var \App\Entity\Esterilizacion
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idEsterilizacion;
    /**
     * @var \App\Entity\TipoPaquete
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idTipoPaquete;

    public function getCodigo(): ?int
    {
        return $this->codigo;
    }

    public function setCodigo(int $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdPaqueteEsterilizacion(): ?int
    {
        return $this->idPaqueteEsterilizacion;
    }

    public function getIdEsterilizacion(): ?Esterilizacion
    {
        return $this->idEsterilizacion;
    }

    public function setIdEsterilizacion(?Esterilizacion $idEsterilizacion): self
    {
        $this->idEsterilizacion = $idEsterilizacion;

        return $this;
    }

    public function getIdTipoPaquete(): ?TipoPaquete
    {
        return $this->idTipoPaquete;
    }

    public function setIdTipoPaquete(?TipoPaquete $idTipoPaquete): self
    {
        $this->idTipoPaquete = $idTipoPaquete;

        return $this;
    }

    public function __toString()
    {
        return 'Esterilización N° '.$this->getIdEsterilizacion()->getIdEsterilizacion().' - N° Paquete'.$this->getCodigo().' - '.$this->getIdTipoPaquete();  
    } 
}
