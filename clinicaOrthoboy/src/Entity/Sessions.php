<?php

namespace App\Entity;

class Sessions
{
    private $id;

    private $sessData;

    private $sessLifetime;

    private $sessTime;

    private $sessUserNameCliente;

    private $sessId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getSessData()
    {
        return $this->sessData;
    }

    public function setSessData($sessData): self
    {
        $this->sessData = $sessData;

        return $this;
    }

    public function getSessLifetime(): ?int
    {
        return $this->sessLifetime;
    }

    public function setSessLifetime(int $sessLifetime): self
    {
        $this->sessLifetime = $sessLifetime;

        return $this;
    }

    public function getSessTime(): ?int
    {
        return $this->sessTime;
    }

    public function setSessTime(int $sessTime): self
    {
        $this->sessTime = $sessTime;

        return $this;
    }

    public function getSessUserNameCliente(): ?string
    {
        return $this->sessUserNameCliente;
    }

    public function setSessUserNameCliente(string $sessUserNameCliente): self
    {
        $this->sessUserNameCliente = $sessUserNameCliente;

        return $this;
    }

    public function getSessId(): ?string
    {
        return $this->sessId;
    }
}
