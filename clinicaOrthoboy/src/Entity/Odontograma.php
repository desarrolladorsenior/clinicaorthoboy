<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Odontograma
 */     
class Odontograma
{
    private $fechaApertura;
    /**
    * @var integer
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 1,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $numeroOdontograma;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $observacion;
    /**
     * @var integer
    */
    private $idOdontograma;
    /**
     * @var \App\Entity\Paciente
    */
    private $idPaciente;

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }
    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getNumeroOdontograma(): ?int
    {
        return $this->numeroOdontograma;
    }

    public function setNumeroOdontograma(int $numeroOdontograma): self
    {
        $this->numeroOdontograma = $numeroOdontograma;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getIdOdontograma(): ?int
    {
        return $this->idOdontograma;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function __toString()
    {
        return $this->getIdOdontograma().' - '.$this->getIdPaciente();  
    } 
}
