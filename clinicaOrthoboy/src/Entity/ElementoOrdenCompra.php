<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * ElementoOrdenCompra
 */

class ElementoOrdenCompra
{

    /**
     * @var integer
    */
    private $idElementoOrdenCompra;

    /**
    * @var \App\Entity\ElementoRequerimiento
    */
    private $idElementoRequerimiento;

    /**
    * @var \App\Entity\OrdenCompra
    */
    private $idOrdenCompra;

   
    public function getIdElementoOrdenCompra(): ?int
    {
        return $this->idElementoOrdenCompra;
    }

    public function getIdElementoRequerimiento(): ?ElementoRequerimiento
    {
        return $this->idElementoRequerimiento;
    }

    public function setIdElementoRequerimiento(?ElementoRequerimiento $idElementoRequerimiento): self
    {
        $this->idElementoRequerimiento = $idElementoRequerimiento;

        return $this;
    } 

    public function getIdOrdenCompra(): ?OrdenCompra
    {
        return $this->idOrdenCompra;
    }

    public function setIdOrdenCompra(?OrdenCompra $idOrdenCompra): self
    {
        $this->idOrdenCompra = $idOrdenCompra;

        return $this;
    }     

    public function __toString()
    {
        return 'Orden N°: '.$this->getIdOrderCompra().' - Elemento: '.$this->getIdElementoRequerimiento();
    }
}
