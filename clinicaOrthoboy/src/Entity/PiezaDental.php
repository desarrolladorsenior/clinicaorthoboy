<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * PiezaDental
 * @UniqueEntity(fields={"codigoPieza"}, message="Este dato ya se encuentra registrado.")
 */  
class PiezaDental
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 10,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $codigoPieza;
    /**
     * @var integer
    */
    private $idPiezaDental;

    public function getCodigoPieza(): ?string
    {
        return $this->codigoPieza;
    }

    public function setCodigoPieza(string $codigoPieza): self
    {
        $this->codigoPieza = $codigoPieza;

        return $this;
    }

    public function getIdPiezaDental(): ?int
    {
        return $this->idPiezaDental;
    }
     public function __toString()
    {
        return  ucwords(strtolower($this->getCodigoPieza()));  
    }
}
