<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Area
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */
class Area
{
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )  
     */ 
    private $nombre;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idArea;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdArea(): ?int
    {
        return $this->idArea;
    }

    public function __toString()
    {
        return $this->getNombre();  
    }
}
