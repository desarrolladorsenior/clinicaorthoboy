<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * MotivoConsulta
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */   
class MotivoConsulta
{
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $nombre;

    /**
    * @var bigint
    * @Assert\Length(
    *      min = 1,
    *      max = 5,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $tiempoEstimado;

    /**
     * @var boolean
    */
    private $estado;

    /**
     * @var integer
    */
    private $idMotivoConsulta;

    /**
     * @var \App\Entity\Cups
    */
    private $idProcedimiento;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getTiempoEstimado(): ?int
    {
        return $this->tiempoEstimado;
    }

    public function setTiempoEstimado(int $tiempoEstimado): self
    {
        $this->tiempoEstimado = $tiempoEstimado;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdMotivoConsulta(): ?int
    {
        return $this->idMotivoConsulta;
    }

    public function getIdProcedimiento(): ?Cups
    {
        return $this->idProcedimiento;
    }

    public function setIdProcedimiento(?Cups $idProcedimiento): self
    {
        $this->idProcedimiento = $idProcedimiento;

        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();  
        
    } 
}
