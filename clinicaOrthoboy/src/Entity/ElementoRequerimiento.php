<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * ElementoRequerimiento
 */
class ElementoRequerimiento
{
    /**
     * @var integer
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Se permiten unicamente números"),
    */
    private $cantidad;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
    */
    private $estado;
    /**
     * @var integer
    */
    private $idElementoRequerimiento;
    /**
     * @var \App\Entity\NombreInsumo
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idNombreInsumo;
    /**
     * @var \App\Entity\Marca
    */
    private $idMarca;
    /**
     * @var \App\Entity\Presentacion
    */
    private $idPresentacion;
    /**
     * @var \App\Entity\Requerimiento
    */
    private $idRequerimiento;

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdElementoRequerimiento(): ?int
    {
        return $this->idElementoRequerimiento;
    }

    public function getIdPedido(): ?Pedido
    {
        return $this->idPedido;
    }

    public function setIdPedido(?Pedido $idPedido): self
    {
        $this->idPedido = $idPedido;

        return $this;
    }

    public function getIdNombreInsumo(): ?NombreInsumo
    {
        return $this->idNombreInsumo;
    }

    public function setIdNombreInsumo(?NombreInsumo $idNombreInsumo): self
    {
        $this->idNombreInsumo = $idNombreInsumo;

        return $this;
    }

    public function getIdMarca(): ?Marca
    {
        return $this->idMarca;
    }

    public function setIdMarca(?Marca $idMarca): self
    {
        $this->idMarca = $idMarca;

        return $this;
    }

    public function getIdPresentacion(): ?Presentacion
    {
        return $this->idPresentacion;
    }

    public function setIdPresentacion(?Presentacion $idPresentacion): self
    {
        $this->idPresentacion = $idPresentacion;

        return $this;
    }

     public function getIdRequerimiento(): ?Requerimiento
    {
        return $this->idRequerimiento;
    }

    public function setIdRequerimiento(?Requerimiento $idRequerimiento): self
    {
        $this->idRequerimiento = $idRequerimiento;

        return $this;
    }

    public function __toString()
    {
         return 'Elemento N°: '.$this->getIdElementoRequerimiento().' - '.$this->getIdNombreInsumo();
    } 
}
