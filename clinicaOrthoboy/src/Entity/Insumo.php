<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Insumo
 */     
class Insumo
{
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $descripcion;
    /**
     * @var string
     * @Assert\Length(
     *      min = 1,
     *      max = 110,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $fuente;
    /**
     * @var integer
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $voltaje;
    /**
     * @var integer
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $potencia;
    /**
     * @var integer
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $corriente;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idInsumo;
    /**
     * @var \App\Entity\CategoriaInventario
    */
    private $idCategoria;
    /**
     * @var \App\Entity\Marca
    */
    private $idMarca;
    /**
     * @var \App\Entity\Presentacion
    */
    private $idPresentacion;
    /**
     * @var \App\Entity\NombreInsumo
    */
    private $idNombreInsumo;

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getFuente(): ?string
    {
        return $this->fuente;
    }

    public function setFuente(?string $fuente): self
    {
        $this->fuente = $fuente;

        return $this;
    }

    public function getVoltaje(): ?int
    {
        return $this->voltaje;
    }

    public function setVoltaje(?int $voltaje): self
    {
        $this->voltaje = $voltaje;

        return $this;
    }

    public function getPotencia(): ?int
    {
        return $this->potencia;
    }

    public function setPotencia(?int $potencia): self
    {
        $this->potencia = $potencia;

        return $this;
    }

    public function getCorriente(): ?int
    {
        return $this->corriente;
    }

    public function setCorriente(?int $corriente): self
    {
        $this->corriente = $corriente;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdInsumo(): ?int
    {
        return $this->idInsumo;
    }

    public function getIdCategoria(): ?CategoriaInventario
    {
        return $this->idCategoria;
    }

    public function setIdCategoria(?CategoriaInventario $idCategoria): self
    {
        $this->idCategoria = $idCategoria;

        return $this;
    }

    public function getIdMarca(): ?Marca
    {
        return $this->idMarca;
    }

    public function setIdMarca(?Marca $idMarca): self
    {
        $this->idMarca = $idMarca;

        return $this;
    }

    public function getIdPresentacion(): ?Presentacion
    {
        return $this->idPresentacion;
    }

    public function setIdPresentacion(?Presentacion $idPresentacion): self
    {
        $this->idPresentacion = $idPresentacion;

        return $this;
    }

    public function getIdNombreInsumo(): ?NombreInsumo
    {
        return $this->idNombreInsumo;
    }

    public function setIdNombreInsumo(?NombreInsumo $idNombreInsumo): self
    {
        $this->idNombreInsumo = $idNombreInsumo;

        return $this;
    }

    public function __toString()
    {
        return $this->getIdNombreInsumo().' - '.$this->getIdPresentacion().' - Marca: '.$this->getIdMarca();  
    } 
}
