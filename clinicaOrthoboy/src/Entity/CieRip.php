<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * CieRip
 * @UniqueEntity(fields={"codigo"}, message="Este dato ya se encuentra registrado.")
 * @UniqueEntity(fields={"nombreCieRip"}, message="Este dato ya se encuentra registrado.")
 */
class CieRip
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 2,
    *      max = 75,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $codigo;
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 2,
    *      max = 250,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombreCieRip;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $desripcion;
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 10,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $sexo;
    /**
    * @var bigint
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 10,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $limiteSuperior;
    /**
    * @var bigint
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 10,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $limiteInferior;
    /**
     * @var integer
    */
    private $idCieRip;

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = strtoupper($codigo);

        return $this;
    }

    public function getNombreCieRip(): ?string
    {
        return $this->nombreCieRip;
    }

    public function setNombreCieRip(string $nombreCieRip): self
    {
        $this->nombreCieRip = strtoupper($nombreCieRip);

        return $this;
    }

    public function getDesripcion(): ?string
    {
        return $this->desripcion;
    }

    public function setDesripcion(?string $desripcion): self
    {
        $this->desripcion = $desripcion;

        return $this;
    }

    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    public function setSexo(string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getLimiteSuperior(): ?int
    {
        return $this->limiteSuperior;
    }

    public function setLimiteSuperior(int $limiteSuperior): self
    {
        $this->limiteSuperior = $limiteSuperior;

        return $this;
    }

    public function getLimiteInferior(): ?int
    {
        return $this->limiteInferior;
    }

    public function setLimiteInferior(int $limiteInferior): self
    {
        $this->limiteInferior = $limiteInferior;

        return $this;
    }

    public function getIdCieRip(): ?int
    {
        return $this->idCieRip;
    }

     public function __toString()
    {
        return $this->getCodigo().' - '.$this->getNombreCieRip();  
    }
}
