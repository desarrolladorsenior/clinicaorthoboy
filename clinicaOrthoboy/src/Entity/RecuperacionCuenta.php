<?php

namespace App\Entity;

class RecuperacionCuenta
{
    private $username;

    private $email;

    private $codigoverificacion;

    private $createdAt;

    private $id;

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCodigoverificacion(): ?string
    {
        return $this->codigoverificacion;
    }

    public function setCodigoverificacion(string $codigoverificacion): self
    {
        $this->codigoverificacion = $codigoverificacion;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /* Añadida por mi*/
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }
}
