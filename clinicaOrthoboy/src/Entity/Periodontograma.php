<?php

namespace App\Entity;

class Periodontograma
{
    private $fechaApertura;

    private $estado;

    private $idPeriodontograma;

    private $idPaciente;

    private $idUsuarioProfesional;

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdPeriodontograma(): ?int
    {
        return $this->idPeriodontograma;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdUsuarioProfesional(): ?Usuario
    {
        return $this->idUsuarioProfesional;
    }

    public function setIdUsuarioProfesional(?Usuario $idUsuarioProfesional): self
    {
        $this->idUsuarioProfesional = $idUsuarioProfesional;

        return $this;
    }
}
