<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Cups
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 * @UniqueEntity(fields={"idCups"}, message="Este dato ya se encuentra registrado.")
 */
class Cups
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;
    /**
    * @var integer
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 10,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )    
    */
    private $idCups;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getIdCups(): ?int
    {
        return $this->idCups;
    }

    public function setIdCups(int $idCups): self
    {
        $this->idCups = strtoupper($idCups);

        return $this;
    }

     public function __toString()
    {
        return 'N° '.$this->getIdCups().' - '.$this->getNombre();
    } 
}
