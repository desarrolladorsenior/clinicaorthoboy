<?php

namespace App\Entity;


use Symfony\Component\Validator\Constraints as Assert;
/**
 * Radiografia
 */     
class Radiografia
{
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )   
     */
    private $nombre;
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 10,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )   
     */
    private $hallazgo;

    /**
     * @var string
     * @Assert\File(
     *maxSize = "1m",
     *mimeTypes={ "image/png","image/jpg","image/jpeg","image/vmp","image/tif","image/tiff"},
     * mimeTypesMessage = "")
     */
    private $imagen;

    private $fechaApertura;

    /**
     * @var integer
    */
    private $idRadiografia;
    /**
    * @var \App\Entity\Paciente
    */
    private $idPaciente;
    /**
    * @var \App\Entity\TipoRadiografia
    */
    private $idTipoRadiografia;
    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuario;
    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getHallazgo(): ?string
    {
        return $this->hallazgo;
    }

    public function setHallazgo(string $hallazgo): self
    {
        $this->hallazgo = $hallazgo;

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    
    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getIdRadiografia(): ?int
    {
        return $this->idRadiografia;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdTipoRadiografia(): ?TipoRadiografia
    {
        return $this->idTipoRadiografia;
    }

    public function setIdTipoRadiografia(?TipoRadiografia $idTipoRadiografia): self
    {
        $this->idTipoRadiografia = $idTipoRadiografia;

        return $this;
    }

    public function getIdUsuario(): ?Usuario
    {
        return $this->idUsuario;
    }

    public function setIdUsuario(?Usuario $idUsuario): self
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

    /* con este metodo
        Subir un unico archivo*/
    public function uploadUno($archivo){
    // la propiedad file puede quedar vacía
        if(null === $archivo){
            return;
        }

        // se usa el nombre original aquí pero debería sanitizarse
        // el método move recibe el directorio y el archivo
        $fileName=md5(uniqid()).'.'.$archivo->guessExtension();
        $archivo->move('/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/radiografias/',$fileName);
        $this->path=$fileName;
        $this->setImagen($this->path);
    }

    public function __toString()
    {
    return $this->getNombre();
    }
}
