<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * PlanTratamiento
 */     

class PlanTratamiento
{   
    private $fechaApertura;
    /**
     * @var integer
    */
    private $numeroCuota;
    /**
     * @var float
    */
    private $abonoTotal;
    /**
     * @var float
    */
    private $valorTotal;
    /**
     * @var integer
    */
    private $idPlanTratamiento;
    /**
    * @var \App\Entity\EstadoGeneral
    */
    private $idEstadoGeneral;
    /**
    * @var \App\Entity\Paciente
    */
    private $idPaciente;
    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;
    /**
     * @var float
    */
    private $cuotaInicial;

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }
    
    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getNumeroCuota(): ?int
    {
        return $this->numeroCuota;
    }

    public function setNumeroCuota(?int $numeroCuota): self
    {
        $this->numeroCuota = $numeroCuota;

        return $this;
    }

    public function getAbonoTotal(): ?float
    {
        return $this->abonoTotal;
    }

    public function setAbonoTotal(?float $abonoTotal): self
    {
        $this->abonoTotal = $abonoTotal;

        return $this;
    }

    public function getCuotaInicial(): ?float
    {
        return $this->cuotaInicial;
    }

    public function setCuotaInicial(?float $cuotaInicial): self
    {
        $this->cuotaInicial = $cuotaInicial;

        return $this;
    }

    public function getValorTotal(): ?float
    {
        return $this->valorTotal;
    }

    public function setValorTotal(?float $valorTotal): self
    {
        $this->valorTotal = $valorTotal;

        return $this;
    }

    public function getIdPlanTratamiento(): ?int
    {
        return $this->idPlanTratamiento;
    }

    public function getIdEstadoGeneral(): ?EstadoGeneral
    {
        return $this->idEstadoGeneral;
    }

    public function setIdEstadoGeneral(?EstadoGeneral $idEstadoGeneral): self
    {
        $this->idEstadoGeneral = $idEstadoGeneral;

        return $this;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }
    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }
    
    public function __toString()
    {
        return $this->getIdPlanTratamiento().' - '.$this->getIdPaciente();  
    } 
}
