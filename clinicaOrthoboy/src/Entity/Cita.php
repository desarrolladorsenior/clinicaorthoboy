<?php

namespace App\Entity;

class Cita
{
    private $fechaApertura;

    private $fechaFin;

    private $idCita;

    private $idPaciente;

    private $idClinica;

    private $idUsuarioProfesional;

    private $idEstadoCita;

    private $idMotivoConsulta;

    private $idConsultorio;

    private $idUsuarioGestiona;

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getFechaFin(): ?\DateTimeInterface
    {
        return $this->fechaFin;
    }

    public function setFechaFin(\DateTimeInterface $fechaFin): self
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    public function getIdCita(): ?int
    {
        return $this->idCita;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdClinica(): ?Clinica
    {
        return $this->idClinica;
    }

    public function setIdClinica(?Clinica $idClinica): self
    {
        $this->idClinica = $idClinica;

        return $this;
    }

    public function getIdUsuarioProfesional(): ?Usuario
    {
        return $this->idUsuarioProfesional;
    }

    public function setIdUsuarioProfesional(?Usuario $idUsuarioProfesional): self
    {
        $this->idUsuarioProfesional = $idUsuarioProfesional;

        return $this;
    }

    public function getIdEstadoCita(): ?EstadoCita
    {
        return $this->idEstadoCita;
    }

    public function setIdEstadoCita(?EstadoCita $idEstadoCita): self
    {
        $this->idEstadoCita = $idEstadoCita;

        return $this;
    }

    public function getIdMotivoConsulta(): ?MotivoConsulta
    {
        return $this->idMotivoConsulta;
    }

    public function setIdMotivoConsulta(?MotivoConsulta $idMotivoConsulta): self
    {
        $this->idMotivoConsulta = $idMotivoConsulta;

        return $this;
    }

    public function getIdConsultorio(): ?Consultorio
    {
        return $this->idConsultorio;
    }

    public function setIdConsultorio(?Consultorio $idConsultorio): self
    {
        $this->idConsultorio = $idConsultorio;

        return $this;
    }

    public function getIdUsuarioGestiona(): ?Usuario
    {
        return $this->idUsuarioGestiona;
    }

    public function setIdUsuarioGestiona(?Usuario $idUsuarioGestiona): self
    {
        $this->idUsuarioGestiona = $idUsuarioGestiona;

        return $this;
    }
}
