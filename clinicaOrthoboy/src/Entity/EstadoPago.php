<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
/**
 * EstadoPago
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */
class EstadoPago
{
     /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )  
     */ 
    private $nombre;
    /**
     * @var integer
    */
    private $idEstadoPago;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getIdEstadoPago(): ?int
    {
        return $this->idEstadoPago;
    }

    public function __toString()
    {
        return $this->getNombre();  
    } 
}
