<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * TratamientoConvenio
 */

class TratamientoConvenio
{
    /**
     * @var integer
     * @Assert\Length(
     *      min = 1,
     *      max = 3,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $porcentajeTratamiento;

    /**
     * @var integer
     * @Assert\Length(
     *      min = 1,
     *      max = 3,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $porcentajeRemision;

    /**
     * @var boolean
    */
    private $estado;

     /**
     * @var integer
    */
    private $idTratamientoConvenio;

    /**
    * @var \App\Entity\Tratamiento
    */
    private $idTratamiento;

    /**
    * @var \App\Entity\Convenio
    */
    private $idConvenio;

    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    private $fechaConvenio;
    
    /**
    * @var Date
    * @Assert\GreaterThan(propertyPath="fechaConvenio")
    */
    private $fechaApertura;


    public function getFechaConvenio(): ?\DateTimeInterface
    {
        return $this->idConvenio->getFechaApertura();
    }


    public function getPorcentajeTratamiento(): ?int
    {
        return $this->porcentajeTratamiento;
    }

    public function setPorcentajeTratamiento(int $porcentajeTratamiento): self
    {
        $this->porcentajeTratamiento = $porcentajeTratamiento;

        return $this;
    }

    public function getPorcentajeRemision(): ?int
    {
        return $this->porcentajeRemision;
    }

    public function setPorcentajeRemision(int $porcentajeRemision): self
    {
        $this->porcentajeRemision = $porcentajeRemision;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getIdTratamientoConvenio(): ?int
    {
        return $this->idTratamientoConvenio;
    }

    public function getIdTratamiento(): ?Tratamiento
    {
        return $this->idTratamiento;
    }

    public function setIdTratamiento(?Tratamiento $idTratamiento): self
    {
        $this->idTratamiento = $idTratamiento;

        return $this;
    }

    public function getIdConvenio(): ?Convenio
    {
        return $this->idConvenio;
    }

    public function setIdConvenio(?Convenio $idConvenio): self
    {
        $this->idConvenio = $idConvenio;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }



    
}
