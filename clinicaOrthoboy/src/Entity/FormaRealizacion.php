<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * FormaREalizacion
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */  
class FormaRealizacion
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;
    /**
     * @var integer
    */
    private $idFormaRealizacion;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getIdFormaRealizacion(): ?int
    {
        return $this->idFormaRealizacion;
    }

     public function __toString()
    {
        return $this->getNombre();  
    } 
}
