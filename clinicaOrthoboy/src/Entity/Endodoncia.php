<?php

namespace App\Entity;

class Endodoncia
{
    private $tipo;

    private $derivadoPor;

    private $fechaApertura;

    private $motivoConsulta;

    private $observacionHistorica;

    private $propiedad;

    private $provocado;

    private $evolucionEnd;

    private $otroSintomaDoloso;

    private $aumentoVolumen;

    private $fistulo;

    private $endenopatia;

    private $examenIntraoral;

    private $examenPeriodontal;

    private $espacioPeriodontalApical;

    private $huesoAlveolarApical;

    private $ausente;

    private $sintomatologiaDCalor;

    private $sintomatologiaDFrio;

    private $sintomatologiaDElectrico;

    private $sintomatologiaDPercusion;

    private $sintomatologiaDExploracion;

    private $sintomatologiaDCavitaria;

    private $sintomatologiaCConducto;

    private $sintomatologiaCTentativa;

    private $sintomatologiaCDefinitiva;

    private $sintomatologiaCReferencia;

    private $sintomatologiaCLimapical;

    private $sintomatologiaCDesobturacion;

    private $sintomatologiaCOtros;

    private $diagnostico;

    private $tratamiento;

    private $fechaAlta;

    private $idEndodoncia;

    private $idPaciente;

    private $idPieza;

    private $idUsuarioProfesional;

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getDerivadoPor(): ?string
    {
        return $this->derivadoPor;
    }

    public function setDerivadoPor(?string $derivadoPor): self
    {
        $this->derivadoPor = $derivadoPor;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getMotivoConsulta(): ?string
    {
        return $this->motivoConsulta;
    }

    public function setMotivoConsulta(?string $motivoConsulta): self
    {
        $this->motivoConsulta = $motivoConsulta;

        return $this;
    }

    public function getObservacionHistorica(): ?string
    {
        return $this->observacionHistorica;
    }

    public function setObservacionHistorica(?string $observacionHistorica): self
    {
        $this->observacionHistorica = $observacionHistorica;

        return $this;
    }

    public function getPropiedad(): ?string
    {
        return $this->propiedad;
    }

    public function setPropiedad(?string $propiedad): self
    {
        $this->propiedad = $propiedad;

        return $this;
    }

    public function getProvocado(): ?string
    {
        return $this->provocado;
    }

    public function setProvocado(?string $provocado): self
    {
        $this->provocado = $provocado;

        return $this;
    }

    public function getEvolucionEnd(): ?string
    {
        return $this->evolucionEnd;
    }

    public function setEvolucionEnd(?string $evolucionEnd): self
    {
        $this->evolucionEnd = $evolucionEnd;

        return $this;
    }

    public function getOtroSintomaDoloso(): ?string
    {
        return $this->otroSintomaDoloso;
    }

    public function setOtroSintomaDoloso(?string $otroSintomaDoloso): self
    {
        $this->otroSintomaDoloso = $otroSintomaDoloso;

        return $this;
    }

    public function getAumentoVolumen(): ?bool
    {
        return $this->aumentoVolumen;
    }

    public function setAumentoVolumen(bool $aumentoVolumen): self
    {
        $this->aumentoVolumen = $aumentoVolumen;

        return $this;
    }

    public function getFistulo(): ?bool
    {
        return $this->fistulo;
    }

    public function setFistulo(bool $fistulo): self
    {
        $this->fistulo = $fistulo;

        return $this;
    }

    public function getEndenopatia(): ?bool
    {
        return $this->endenopatia;
    }

    public function setEndenopatia(bool $endenopatia): self
    {
        $this->endenopatia = $endenopatia;

        return $this;
    }

    public function getExamenIntraoral(): ?string
    {
        return $this->examenIntraoral;
    }

    public function setExamenIntraoral(?string $examenIntraoral): self
    {
        $this->examenIntraoral = $examenIntraoral;

        return $this;
    }

    public function getExamenPeriodontal(): ?string
    {
        return $this->examenPeriodontal;
    }

    public function setExamenPeriodontal(?string $examenPeriodontal): self
    {
        $this->examenPeriodontal = $examenPeriodontal;

        return $this;
    }

    public function getEspacioPeriodontalApical(): ?string
    {
        return $this->espacioPeriodontalApical;
    }

    public function setEspacioPeriodontalApical(?string $espacioPeriodontalApical): self
    {
        $this->espacioPeriodontalApical = $espacioPeriodontalApical;

        return $this;
    }

    public function getHuesoAlveolarApical(): ?string
    {
        return $this->huesoAlveolarApical;
    }

    public function setHuesoAlveolarApical(?string $huesoAlveolarApical): self
    {
        $this->huesoAlveolarApical = $huesoAlveolarApical;

        return $this;
    }

    public function getAusente(): ?string
    {
        return $this->ausente;
    }

    public function setAusente(?string $ausente): self
    {
        $this->ausente = $ausente;

        return $this;
    }

    public function getSintomatologiaDCalor(): ?string
    {
        return $this->sintomatologiaDCalor;
    }

    public function setSintomatologiaDCalor(?string $sintomatologiaDCalor): self
    {
        $this->sintomatologiaDCalor = $sintomatologiaDCalor;

        return $this;
    }

    public function getSintomatologiaDFrio(): ?string
    {
        return $this->sintomatologiaDFrio;
    }

    public function setSintomatologiaDFrio(?string $sintomatologiaDFrio): self
    {
        $this->sintomatologiaDFrio = $sintomatologiaDFrio;

        return $this;
    }

    public function getSintomatologiaDElectrico(): ?string
    {
        return $this->sintomatologiaDElectrico;
    }

    public function setSintomatologiaDElectrico(?string $sintomatologiaDElectrico): self
    {
        $this->sintomatologiaDElectrico = $sintomatologiaDElectrico;

        return $this;
    }

    public function getSintomatologiaDPercusion(): ?string
    {
        return $this->sintomatologiaDPercusion;
    }

    public function setSintomatologiaDPercusion(?string $sintomatologiaDPercusion): self
    {
        $this->sintomatologiaDPercusion = $sintomatologiaDPercusion;

        return $this;
    }

    public function getSintomatologiaDExploracion(): ?string
    {
        return $this->sintomatologiaDExploracion;
    }

    public function setSintomatologiaDExploracion(?string $sintomatologiaDExploracion): self
    {
        $this->sintomatologiaDExploracion = $sintomatologiaDExploracion;

        return $this;
    }

    public function getSintomatologiaDCavitaria(): ?string
    {
        return $this->sintomatologiaDCavitaria;
    }

    public function setSintomatologiaDCavitaria(?string $sintomatologiaDCavitaria): self
    {
        $this->sintomatologiaDCavitaria = $sintomatologiaDCavitaria;

        return $this;
    }

    public function getSintomatologiaCConducto(): ?string
    {
        return $this->sintomatologiaCConducto;
    }

    public function setSintomatologiaCConducto(?string $sintomatologiaCConducto): self
    {
        $this->sintomatologiaCConducto = $sintomatologiaCConducto;

        return $this;
    }

    public function getSintomatologiaCTentativa(): ?string
    {
        return $this->sintomatologiaCTentativa;
    }

    public function setSintomatologiaCTentativa(?string $sintomatologiaCTentativa): self
    {
        $this->sintomatologiaCTentativa = $sintomatologiaCTentativa;

        return $this;
    }

    public function getSintomatologiaCDefinitiva(): ?string
    {
        return $this->sintomatologiaCDefinitiva;
    }

    public function setSintomatologiaCDefinitiva(?string $sintomatologiaCDefinitiva): self
    {
        $this->sintomatologiaCDefinitiva = $sintomatologiaCDefinitiva;

        return $this;
    }

    public function getSintomatologiaCReferencia(): ?string
    {
        return $this->sintomatologiaCReferencia;
    }

    public function setSintomatologiaCReferencia(?string $sintomatologiaCReferencia): self
    {
        $this->sintomatologiaCReferencia = $sintomatologiaCReferencia;

        return $this;
    }

    public function getSintomatologiaCLimapical(): ?string
    {
        return $this->sintomatologiaCLimapical;
    }

    public function setSintomatologiaCLimapical(?string $sintomatologiaCLimapical): self
    {
        $this->sintomatologiaCLimapical = $sintomatologiaCLimapical;

        return $this;
    }

    public function getSintomatologiaCDesobturacion(): ?string
    {
        return $this->sintomatologiaCDesobturacion;
    }

    public function setSintomatologiaCDesobturacion(?string $sintomatologiaCDesobturacion): self
    {
        $this->sintomatologiaCDesobturacion = $sintomatologiaCDesobturacion;

        return $this;
    }

    public function getSintomatologiaCOtros(): ?string
    {
        return $this->sintomatologiaCOtros;
    }

    public function setSintomatologiaCOtros(?string $sintomatologiaCOtros): self
    {
        $this->sintomatologiaCOtros = $sintomatologiaCOtros;

        return $this;
    }

    public function getDiagnostico(): ?string
    {
        return $this->diagnostico;
    }

    public function setDiagnostico(?string $diagnostico): self
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    public function getTratamiento(): ?string
    {
        return $this->tratamiento;
    }

    public function setTratamiento(?string $tratamiento): self
    {
        $this->tratamiento = $tratamiento;

        return $this;
    }

    public function getFechaAlta(): ?\DateTimeInterface
    {
        return $this->fechaAlta;
    }

    public function setFechaAlta(?\DateTimeInterface $fechaAlta): self
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    public function getIdEndodoncia(): ?int
    {
        return $this->idEndodoncia;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdPieza(): ?PiezaDental
    {
        return $this->idPieza;
    }

    public function setIdPieza(?PiezaDental $idPieza): self
    {
        $this->idPieza = $idPieza;

        return $this;
    }

    public function getIdUsuarioProfesional(): ?Usuario
    {
        return $this->idUsuarioProfesional;
    }

    public function setIdUsuarioProfesional(?Usuario $idUsuarioProfesional): self
    {
        $this->idUsuarioProfesional = $idUsuarioProfesional;

        return $this;
    }
}
