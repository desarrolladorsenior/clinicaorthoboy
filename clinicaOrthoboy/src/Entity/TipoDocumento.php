<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * class TipoDocumento
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 * @UniqueEntity(fields={"nomenclatura"}, message="Este dato ya se encuentra registrado.")
 */     

class TipoDocumento
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 1,
    *      max = 20,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nomenclatura;
    /**
     * @var integer
    */
    private $id;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getNomenclatura(): ?string
    {
        return $this->nomenclatura;
    }

    public function setNomenclatura(string $nomenclatura): self
    {
        $this->nomenclatura = strtoupper($nomenclatura);

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function __toString()
    {
    return $this->getNomenclatura();
    }
}
