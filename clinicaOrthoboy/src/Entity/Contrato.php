<?php

namespace App\Entity;


use Symfony\Component\Validator\Constraints as Assert;
/**
 * Contrato
 */     
class Contrato
{
    private $fechaApertura;
    /**
     * @var string
    */
    private $firmaPaciente;
    /**
     * @var string
    */
    private $huellaPaciente;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )   
     */
    private $nombreAcudiente;
    /**
     * @var integer
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 7,
     *      max = 15,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )   
     */
    private $cedularAcudiente;
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )   
     */
    private $parentesco;
    /**
     * @var integer
     * @Assert\Length(
     *      min = 7,
     *      max = 15,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )   
     */
    private $cedularResponsable;
    /**
     * @var integer
    */
    private $idContrato;
    /**
    * @var \App\Entity\Paciente
    */
    private $idPaciente;
    /**
    * @var \App\Entity\TipoArchivo
    */
    private $idTipoArchivo;
    /**
    * @var \App\Entity\TipoContrato
    */
    private $idTipoContrato;
    /**
    * @var \App\Entity\PlanTratamiento
    */
    private $idPlanTratamiento;
    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuario;

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }
     /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }
    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getFirmaPaciente(): ?string
    {
        return $this->firmaPaciente;
    }

    public function setFirmaPaciente(string $firmaPaciente): self
    {
        $this->firmaPaciente = $firmaPaciente;

        return $this;
    }

    public function getHuellaPaciente(): ?string
    {
        return $this->huellaPaciente;
    }

    public function setHuellaPaciente(string $huellaPaciente): self
    {
        $this->huellaPaciente = $huellaPaciente;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getNombreAcudiente(): ?string
    {
        return $this->nombreAcudiente;
    }

    public function setNombreAcudiente(?string $nombreAcudiente): self
    {
        $this->nombreAcudiente = $nombreAcudiente;

        return $this;
    }

    public function getCedularAcudiente(): ?string
    {
        return $this->cedularAcudiente;
    }

    public function setCedularAcudiente(?string $cedularAcudiente): self
    {
        $this->cedularAcudiente = $cedularAcudiente;

        return $this;
    }

    public function getParentesco(): ?string
    {
        return $this->parentesco;
    }

    public function setParentesco(?string $parentesco): self
    {
        $this->parentesco = $parentesco;

        return $this;
    }

    public function getCedularResponsable(): ?string
    {
        return $this->cedularResponsable;
    }

    public function setCedularResponsable(?string $cedularResponsable): self
    {
        $this->cedularResponsable = $cedularResponsable;

        return $this;
    }

    public function getIdContrato(): ?int
    {
        return $this->idContrato;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdTipoArchivo(): ?TipoArchivo
    {
        return $this->idTipoArchivo;
    }

    public function setIdTipoArchivo(?TipoArchivo $idTipoArchivo): self
    {
        $this->idTipoArchivo = $idTipoArchivo;

        return $this;
    }

    public function getIdTipoContrato(): ?TipoContrato
    {
        return $this->idTipoContrato;
    }

    public function setIdTipoContrato(?TipoContrato $idTipoContrato): self
    {
        $this->idTipoContrato = $idTipoContrato;

        return $this;
    }

    public function getIdPlanTratamiento(): ?PlanTratamiento
    {
        return $this->idPlanTratamiento;
    }

    public function setIdPlanTratamiento(?PlanTratamiento $idPlanTratamiento): self
    {
        $this->idPlanTratamiento = $idPlanTratamiento;

        return $this;
    }

    public function getIdUsuario(): ?Usuario
    {
        return $this->idUsuario;
    }

    public function setIdUsuario(?Usuario $idUsuario): self
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    public function __toString()
    {
    return $this->getIdTipoContrato().' - Paciente: '.$this->getIdPaciente();
    }
}
