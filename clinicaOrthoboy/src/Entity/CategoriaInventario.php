<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * CategoriaInventario
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */     
class CategoriaInventario
{
    /**
    * @var string
    * @Assert\NotBlank(message="Este campo es obligatorio.")
    * @Assert\Length(
    *      min = 3,
    *      max = 150,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $nombre;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idCategoriaInventario;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre =  strtoupper($nombre);

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdCategoriaInventario(): ?int
    {
        return $this->idCategoriaInventario;
    }
    public function __toString()
    {
        return ucwords(strtolower($this->getNombre()));
    } 
}
