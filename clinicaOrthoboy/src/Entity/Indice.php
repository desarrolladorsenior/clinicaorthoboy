<?php

namespace App\Entity;

class Indice
{
    private $d18gi;

    private $d18hi;

    private $d17gi;

    private $d17hi;

    private $d16gi;

    private $d16hi;

    private $d15gi;

    private $d15hi;

    private $d14gi;

    private $d14hi;

    private $d13gi;

    private $d13hi;

    private $d12gi;

    private $d12hi;

    private $d11gi;

    private $d11hi;

    private $d21gi;

    private $d21hi;

    private $d22gi;

    private $d22hi;

    private $d23gi;

    private $d23hi;

    private $d24gi;

    private $d24hi;

    private $d25gi;

    private $d25hi;

    private $d26gi;

    private $d26hi;

    private $d27gi;

    private $d27hi;

    private $d28gi;

    private $d28hi;

    private $d55gi;

    private $d55hi;

    private $d54gi;

    private $d54hi;

    private $d53gi;

    private $d53hi;

    private $d52gi;

    private $d52hi;

    private $d51gi;

    private $d51hi;

    private $d61gi;

    private $d61hi;

    private $d62gi;

    private $d62hi;

    private $d63gi;

    private $d63hi;

    private $d64gi;

    private $d64hi;

    private $d65gi;

    private $d65hi;

    private $d85gi;

    private $d85hi;

    private $d84gi;

    private $d84hi;

    private $d83gi;

    private $d83hi;

    private $d82gi;

    private $d82hi;

    private $d81gi;

    private $d81hi;

    private $d71gi;

    private $d71hi;

    private $d72gi;

    private $d72hi;

    private $d73gi;

    private $d73hi;

    private $d74gi;

    private $d74hi;

    private $d75gi;

    private $d75hi;

    private $d48gi;

    private $d48hi;

    private $d47gi;

    private $d47hi;

    private $d46gi;

    private $d46hi;

    private $d45gi;

    private $d45hi;

    private $d44gi;

    private $d44hi;

    private $d43gi;

    private $d43hi;

    private $d42gi;

    private $d42hi;

    private $d41gi;

    private $d41hi;

    private $d31gi;

    private $d31hi;

    private $d32gi;

    private $d32hi;

    private $d33gi;

    private $d33hi;

    private $d34gi;

    private $d34hi;

    private $d35gi;

    private $d35hi;

    private $d36gi;

    private $d36hi;

    private $d37gi;

    private $d37hi;

    private $d38gi;

    private $d38hi;

    private $idIndice;

    private $idPeriodontograma;

    public function getD18gi(): ?string
    {
        return $this->d18gi;
    }

    public function setD18gi(?string $d18gi): self
    {
        $this->d18gi = $d18gi;

        return $this;
    }

    public function getD18hi(): ?string
    {
        return $this->d18hi;
    }

    public function setD18hi(?string $d18hi): self
    {
        $this->d18hi = $d18hi;

        return $this;
    }

    public function getD17gi(): ?string
    {
        return $this->d17gi;
    }

    public function setD17gi(?string $d17gi): self
    {
        $this->d17gi = $d17gi;

        return $this;
    }

    public function getD17hi(): ?string
    {
        return $this->d17hi;
    }

    public function setD17hi(?string $d17hi): self
    {
        $this->d17hi = $d17hi;

        return $this;
    }

    public function getD16gi(): ?string
    {
        return $this->d16gi;
    }

    public function setD16gi(?string $d16gi): self
    {
        $this->d16gi = $d16gi;

        return $this;
    }

    public function getD16hi(): ?string
    {
        return $this->d16hi;
    }

    public function setD16hi(?string $d16hi): self
    {
        $this->d16hi = $d16hi;

        return $this;
    }

    public function getD15gi(): ?string
    {
        return $this->d15gi;
    }

    public function setD15gi(?string $d15gi): self
    {
        $this->d15gi = $d15gi;

        return $this;
    }

    public function getD15hi(): ?string
    {
        return $this->d15hi;
    }

    public function setD15hi(?string $d15hi): self
    {
        $this->d15hi = $d15hi;

        return $this;
    }

    public function getD14gi(): ?string
    {
        return $this->d14gi;
    }

    public function setD14gi(?string $d14gi): self
    {
        $this->d14gi = $d14gi;

        return $this;
    }

    public function getD14hi(): ?string
    {
        return $this->d14hi;
    }

    public function setD14hi(?string $d14hi): self
    {
        $this->d14hi = $d14hi;

        return $this;
    }

    public function getD13gi(): ?string
    {
        return $this->d13gi;
    }

    public function setD13gi(?string $d13gi): self
    {
        $this->d13gi = $d13gi;

        return $this;
    }

    public function getD13hi(): ?string
    {
        return $this->d13hi;
    }

    public function setD13hi(?string $d13hi): self
    {
        $this->d13hi = $d13hi;

        return $this;
    }

    public function getD12gi(): ?string
    {
        return $this->d12gi;
    }

    public function setD12gi(?string $d12gi): self
    {
        $this->d12gi = $d12gi;

        return $this;
    }

    public function getD12hi(): ?string
    {
        return $this->d12hi;
    }

    public function setD12hi(?string $d12hi): self
    {
        $this->d12hi = $d12hi;

        return $this;
    }

    public function getD11gi(): ?string
    {
        return $this->d11gi;
    }

    public function setD11gi(?string $d11gi): self
    {
        $this->d11gi = $d11gi;

        return $this;
    }

    public function getD11hi(): ?string
    {
        return $this->d11hi;
    }

    public function setD11hi(?string $d11hi): self
    {
        $this->d11hi = $d11hi;

        return $this;
    }

    public function getD21gi(): ?string
    {
        return $this->d21gi;
    }

    public function setD21gi(?string $d21gi): self
    {
        $this->d21gi = $d21gi;

        return $this;
    }

    public function getD21hi(): ?string
    {
        return $this->d21hi;
    }

    public function setD21hi(?string $d21hi): self
    {
        $this->d21hi = $d21hi;

        return $this;
    }

    public function getD22gi(): ?string
    {
        return $this->d22gi;
    }

    public function setD22gi(?string $d22gi): self
    {
        $this->d22gi = $d22gi;

        return $this;
    }

    public function getD22hi(): ?string
    {
        return $this->d22hi;
    }

    public function setD22hi(?string $d22hi): self
    {
        $this->d22hi = $d22hi;

        return $this;
    }

    public function getD23gi(): ?string
    {
        return $this->d23gi;
    }

    public function setD23gi(?string $d23gi): self
    {
        $this->d23gi = $d23gi;

        return $this;
    }

    public function getD23hi(): ?string
    {
        return $this->d23hi;
    }

    public function setD23hi(?string $d23hi): self
    {
        $this->d23hi = $d23hi;

        return $this;
    }

    public function getD24gi(): ?string
    {
        return $this->d24gi;
    }

    public function setD24gi(?string $d24gi): self
    {
        $this->d24gi = $d24gi;

        return $this;
    }

    public function getD24hi(): ?string
    {
        return $this->d24hi;
    }

    public function setD24hi(?string $d24hi): self
    {
        $this->d24hi = $d24hi;

        return $this;
    }

    public function getD25gi(): ?string
    {
        return $this->d25gi;
    }

    public function setD25gi(?string $d25gi): self
    {
        $this->d25gi = $d25gi;

        return $this;
    }

    public function getD25hi(): ?string
    {
        return $this->d25hi;
    }

    public function setD25hi(?string $d25hi): self
    {
        $this->d25hi = $d25hi;

        return $this;
    }

    public function getD26gi(): ?string
    {
        return $this->d26gi;
    }

    public function setD26gi(?string $d26gi): self
    {
        $this->d26gi = $d26gi;

        return $this;
    }

    public function getD26hi(): ?string
    {
        return $this->d26hi;
    }

    public function setD26hi(?string $d26hi): self
    {
        $this->d26hi = $d26hi;

        return $this;
    }

    public function getD27gi(): ?string
    {
        return $this->d27gi;
    }

    public function setD27gi(?string $d27gi): self
    {
        $this->d27gi = $d27gi;

        return $this;
    }

    public function getD27hi(): ?string
    {
        return $this->d27hi;
    }

    public function setD27hi(?string $d27hi): self
    {
        $this->d27hi = $d27hi;

        return $this;
    }

    public function getD28gi(): ?string
    {
        return $this->d28gi;
    }

    public function setD28gi(?string $d28gi): self
    {
        $this->d28gi = $d28gi;

        return $this;
    }

    public function getD28hi(): ?string
    {
        return $this->d28hi;
    }

    public function setD28hi(?string $d28hi): self
    {
        $this->d28hi = $d28hi;

        return $this;
    }

    public function getD55gi(): ?string
    {
        return $this->d55gi;
    }

    public function setD55gi(?string $d55gi): self
    {
        $this->d55gi = $d55gi;

        return $this;
    }

    public function getD55hi(): ?string
    {
        return $this->d55hi;
    }

    public function setD55hi(?string $d55hi): self
    {
        $this->d55hi = $d55hi;

        return $this;
    }

    public function getD54gi(): ?string
    {
        return $this->d54gi;
    }

    public function setD54gi(?string $d54gi): self
    {
        $this->d54gi = $d54gi;

        return $this;
    }

    public function getD54hi(): ?string
    {
        return $this->d54hi;
    }

    public function setD54hi(?string $d54hi): self
    {
        $this->d54hi = $d54hi;

        return $this;
    }

    public function getD53gi(): ?string
    {
        return $this->d53gi;
    }

    public function setD53gi(?string $d53gi): self
    {
        $this->d53gi = $d53gi;

        return $this;
    }

    public function getD53hi(): ?string
    {
        return $this->d53hi;
    }

    public function setD53hi(?string $d53hi): self
    {
        $this->d53hi = $d53hi;

        return $this;
    }

    public function getD52gi(): ?string
    {
        return $this->d52gi;
    }

    public function setD52gi(?string $d52gi): self
    {
        $this->d52gi = $d52gi;

        return $this;
    }

    public function getD52hi(): ?string
    {
        return $this->d52hi;
    }

    public function setD52hi(?string $d52hi): self
    {
        $this->d52hi = $d52hi;

        return $this;
    }

    public function getD51gi(): ?string
    {
        return $this->d51gi;
    }

    public function setD51gi(?string $d51gi): self
    {
        $this->d51gi = $d51gi;

        return $this;
    }

    public function getD51hi(): ?string
    {
        return $this->d51hi;
    }

    public function setD51hi(?string $d51hi): self
    {
        $this->d51hi = $d51hi;

        return $this;
    }

    public function getD61gi(): ?string
    {
        return $this->d61gi;
    }

    public function setD61gi(?string $d61gi): self
    {
        $this->d61gi = $d61gi;

        return $this;
    }

    public function getD61hi(): ?string
    {
        return $this->d61hi;
    }

    public function setD61hi(?string $d61hi): self
    {
        $this->d61hi = $d61hi;

        return $this;
    }

    public function getD62gi(): ?string
    {
        return $this->d62gi;
    }

    public function setD62gi(?string $d62gi): self
    {
        $this->d62gi = $d62gi;

        return $this;
    }

    public function getD62hi(): ?string
    {
        return $this->d62hi;
    }

    public function setD62hi(?string $d62hi): self
    {
        $this->d62hi = $d62hi;

        return $this;
    }

    public function getD63gi(): ?string
    {
        return $this->d63gi;
    }

    public function setD63gi(?string $d63gi): self
    {
        $this->d63gi = $d63gi;

        return $this;
    }

    public function getD63hi(): ?string
    {
        return $this->d63hi;
    }

    public function setD63hi(?string $d63hi): self
    {
        $this->d63hi = $d63hi;

        return $this;
    }

    public function getD64gi(): ?string
    {
        return $this->d64gi;
    }

    public function setD64gi(?string $d64gi): self
    {
        $this->d64gi = $d64gi;

        return $this;
    }

    public function getD64hi(): ?string
    {
        return $this->d64hi;
    }

    public function setD64hi(?string $d64hi): self
    {
        $this->d64hi = $d64hi;

        return $this;
    }

    public function getD65gi(): ?string
    {
        return $this->d65gi;
    }

    public function setD65gi(?string $d65gi): self
    {
        $this->d65gi = $d65gi;

        return $this;
    }

    public function getD65hi(): ?string
    {
        return $this->d65hi;
    }

    public function setD65hi(?string $d65hi): self
    {
        $this->d65hi = $d65hi;

        return $this;
    }

    public function getD85gi(): ?string
    {
        return $this->d85gi;
    }

    public function setD85gi(?string $d85gi): self
    {
        $this->d85gi = $d85gi;

        return $this;
    }

    public function getD85hi(): ?string
    {
        return $this->d85hi;
    }

    public function setD85hi(?string $d85hi): self
    {
        $this->d85hi = $d85hi;

        return $this;
    }

    public function getD84gi(): ?string
    {
        return $this->d84gi;
    }

    public function setD84gi(?string $d84gi): self
    {
        $this->d84gi = $d84gi;

        return $this;
    }

    public function getD84hi(): ?string
    {
        return $this->d84hi;
    }

    public function setD84hi(?string $d84hi): self
    {
        $this->d84hi = $d84hi;

        return $this;
    }

    public function getD83gi(): ?string
    {
        return $this->d83gi;
    }

    public function setD83gi(?string $d83gi): self
    {
        $this->d83gi = $d83gi;

        return $this;
    }

    public function getD83hi(): ?string
    {
        return $this->d83hi;
    }

    public function setD83hi(?string $d83hi): self
    {
        $this->d83hi = $d83hi;

        return $this;
    }

    public function getD82gi(): ?string
    {
        return $this->d82gi;
    }

    public function setD82gi(?string $d82gi): self
    {
        $this->d82gi = $d82gi;

        return $this;
    }

    public function getD82hi(): ?string
    {
        return $this->d82hi;
    }

    public function setD82hi(?string $d82hi): self
    {
        $this->d82hi = $d82hi;

        return $this;
    }

    public function getD81gi(): ?string
    {
        return $this->d81gi;
    }

    public function setD81gi(?string $d81gi): self
    {
        $this->d81gi = $d81gi;

        return $this;
    }

    public function getD81hi(): ?string
    {
        return $this->d81hi;
    }

    public function setD81hi(?string $d81hi): self
    {
        $this->d81hi = $d81hi;

        return $this;
    }

    public function getD71gi(): ?string
    {
        return $this->d71gi;
    }

    public function setD71gi(?string $d71gi): self
    {
        $this->d71gi = $d71gi;

        return $this;
    }

    public function getD71hi(): ?string
    {
        return $this->d71hi;
    }

    public function setD71hi(?string $d71hi): self
    {
        $this->d71hi = $d71hi;

        return $this;
    }

    public function getD72gi(): ?string
    {
        return $this->d72gi;
    }

    public function setD72gi(?string $d72gi): self
    {
        $this->d72gi = $d72gi;

        return $this;
    }

    public function getD72hi(): ?string
    {
        return $this->d72hi;
    }

    public function setD72hi(?string $d72hi): self
    {
        $this->d72hi = $d72hi;

        return $this;
    }

    public function getD73gi(): ?string
    {
        return $this->d73gi;
    }

    public function setD73gi(?string $d73gi): self
    {
        $this->d73gi = $d73gi;

        return $this;
    }

    public function getD73hi(): ?string
    {
        return $this->d73hi;
    }

    public function setD73hi(?string $d73hi): self
    {
        $this->d73hi = $d73hi;

        return $this;
    }

    public function getD74gi(): ?string
    {
        return $this->d74gi;
    }

    public function setD74gi(?string $d74gi): self
    {
        $this->d74gi = $d74gi;

        return $this;
    }

    public function getD74hi(): ?string
    {
        return $this->d74hi;
    }

    public function setD74hi(?string $d74hi): self
    {
        $this->d74hi = $d74hi;

        return $this;
    }

    public function getD75gi(): ?string
    {
        return $this->d75gi;
    }

    public function setD75gi(?string $d75gi): self
    {
        $this->d75gi = $d75gi;

        return $this;
    }

    public function getD75hi(): ?string
    {
        return $this->d75hi;
    }

    public function setD75hi(?string $d75hi): self
    {
        $this->d75hi = $d75hi;

        return $this;
    }

    public function getD48gi(): ?string
    {
        return $this->d48gi;
    }

    public function setD48gi(?string $d48gi): self
    {
        $this->d48gi = $d48gi;

        return $this;
    }

    public function getD48hi(): ?string
    {
        return $this->d48hi;
    }

    public function setD48hi(?string $d48hi): self
    {
        $this->d48hi = $d48hi;

        return $this;
    }

    public function getD47gi(): ?string
    {
        return $this->d47gi;
    }

    public function setD47gi(?string $d47gi): self
    {
        $this->d47gi = $d47gi;

        return $this;
    }

    public function getD47hi(): ?string
    {
        return $this->d47hi;
    }

    public function setD47hi(?string $d47hi): self
    {
        $this->d47hi = $d47hi;

        return $this;
    }

    public function getD46gi(): ?string
    {
        return $this->d46gi;
    }

    public function setD46gi(?string $d46gi): self
    {
        $this->d46gi = $d46gi;

        return $this;
    }

    public function getD46hi(): ?string
    {
        return $this->d46hi;
    }

    public function setD46hi(?string $d46hi): self
    {
        $this->d46hi = $d46hi;

        return $this;
    }

    public function getD45gi(): ?string
    {
        return $this->d45gi;
    }

    public function setD45gi(?string $d45gi): self
    {
        $this->d45gi = $d45gi;

        return $this;
    }

    public function getD45hi(): ?string
    {
        return $this->d45hi;
    }

    public function setD45hi(?string $d45hi): self
    {
        $this->d45hi = $d45hi;

        return $this;
    }

    public function getD44gi(): ?string
    {
        return $this->d44gi;
    }

    public function setD44gi(?string $d44gi): self
    {
        $this->d44gi = $d44gi;

        return $this;
    }

    public function getD44hi(): ?string
    {
        return $this->d44hi;
    }

    public function setD44hi(?string $d44hi): self
    {
        $this->d44hi = $d44hi;

        return $this;
    }

    public function getD43gi(): ?string
    {
        return $this->d43gi;
    }

    public function setD43gi(?string $d43gi): self
    {
        $this->d43gi = $d43gi;

        return $this;
    }

    public function getD43hi(): ?string
    {
        return $this->d43hi;
    }

    public function setD43hi(?string $d43hi): self
    {
        $this->d43hi = $d43hi;

        return $this;
    }

    public function getD42gi(): ?string
    {
        return $this->d42gi;
    }

    public function setD42gi(?string $d42gi): self
    {
        $this->d42gi = $d42gi;

        return $this;
    }

    public function getD42hi(): ?string
    {
        return $this->d42hi;
    }

    public function setD42hi(?string $d42hi): self
    {
        $this->d42hi = $d42hi;

        return $this;
    }

    public function getD41gi(): ?string
    {
        return $this->d41gi;
    }

    public function setD41gi(?string $d41gi): self
    {
        $this->d41gi = $d41gi;

        return $this;
    }

    public function getD41hi(): ?string
    {
        return $this->d41hi;
    }

    public function setD41hi(?string $d41hi): self
    {
        $this->d41hi = $d41hi;

        return $this;
    }

    public function getD31gi(): ?string
    {
        return $this->d31gi;
    }

    public function setD31gi(?string $d31gi): self
    {
        $this->d31gi = $d31gi;

        return $this;
    }

    public function getD31hi(): ?string
    {
        return $this->d31hi;
    }

    public function setD31hi(?string $d31hi): self
    {
        $this->d31hi = $d31hi;

        return $this;
    }

    public function getD32gi(): ?string
    {
        return $this->d32gi;
    }

    public function setD32gi(?string $d32gi): self
    {
        $this->d32gi = $d32gi;

        return $this;
    }

    public function getD32hi(): ?string
    {
        return $this->d32hi;
    }

    public function setD32hi(?string $d32hi): self
    {
        $this->d32hi = $d32hi;

        return $this;
    }

    public function getD33gi(): ?string
    {
        return $this->d33gi;
    }

    public function setD33gi(?string $d33gi): self
    {
        $this->d33gi = $d33gi;

        return $this;
    }

    public function getD33hi(): ?string
    {
        return $this->d33hi;
    }

    public function setD33hi(?string $d33hi): self
    {
        $this->d33hi = $d33hi;

        return $this;
    }

    public function getD34gi(): ?string
    {
        return $this->d34gi;
    }

    public function setD34gi(?string $d34gi): self
    {
        $this->d34gi = $d34gi;

        return $this;
    }

    public function getD34hi(): ?string
    {
        return $this->d34hi;
    }

    public function setD34hi(?string $d34hi): self
    {
        $this->d34hi = $d34hi;

        return $this;
    }

    public function getD35gi(): ?string
    {
        return $this->d35gi;
    }

    public function setD35gi(?string $d35gi): self
    {
        $this->d35gi = $d35gi;

        return $this;
    }

    public function getD35hi(): ?string
    {
        return $this->d35hi;
    }

    public function setD35hi(?string $d35hi): self
    {
        $this->d35hi = $d35hi;

        return $this;
    }

    public function getD36gi(): ?string
    {
        return $this->d36gi;
    }

    public function setD36gi(?string $d36gi): self
    {
        $this->d36gi = $d36gi;

        return $this;
    }

    public function getD36hi(): ?string
    {
        return $this->d36hi;
    }

    public function setD36hi(?string $d36hi): self
    {
        $this->d36hi = $d36hi;

        return $this;
    }

    public function getD37gi(): ?string
    {
        return $this->d37gi;
    }

    public function setD37gi(?string $d37gi): self
    {
        $this->d37gi = $d37gi;

        return $this;
    }

    public function getD37hi(): ?string
    {
        return $this->d37hi;
    }

    public function setD37hi(?string $d37hi): self
    {
        $this->d37hi = $d37hi;

        return $this;
    }

    public function getD38gi(): ?string
    {
        return $this->d38gi;
    }

    public function setD38gi(?string $d38gi): self
    {
        $this->d38gi = $d38gi;

        return $this;
    }

    public function getD38hi(): ?string
    {
        return $this->d38hi;
    }

    public function setD38hi(?string $d38hi): self
    {
        $this->d38hi = $d38hi;

        return $this;
    }

    public function getIdIndice(): ?int
    {
        return $this->idIndice;
    }

    public function getIdPeriodontograma(): ?Periodontograma
    {
        return $this->idPeriodontograma;
    }

    public function setIdPeriodontograma(?Periodontograma $idPeriodontograma): self
    {
        $this->idPeriodontograma = $idPeriodontograma;

        return $this;
    }
}
