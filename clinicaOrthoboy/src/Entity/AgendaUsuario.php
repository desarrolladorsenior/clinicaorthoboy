<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * AgendaUsuario
 */     

class AgendaUsuario
{
    private $fechaApertura;
    /**
     * @var time A "H:i" formatted value
     * @Assert\Time
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
    */
    private $horaInicio;
    /**
     * @var time A "H:i" formatted value
     * @Assert\Time
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
    */
    private $horaFin;
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 1,
     *      max = 20,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )   
     */
    private $dia;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idAgendaUsuario;
    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;
    /**
    * @var dateinterval
    */
    private $horaAlmuerzo;

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getHoraInicio(): ?\DateTimeInterface
    {
        return $this->horaInicio;
    }

    public function setHoraInicio(\DateTimeInterface $horaInicio): self
    {
        $this->horaInicio = $horaInicio;

        return $this;
    }

     public function getHoraAlmuerzo(): ?\DateInterval
    {
        return $this->horaAlmuerzo;
    }

    public function setHoraAlmuerzo(\DateInterval $horaAlmuerzo): self
    {
        $this->horaAlmuerzo = $horaAlmuerzo;

        return $this;
    }

    public function getHoraFin(): ?\DateTimeInterface
    {
        return $this->horaFin;
    }

    public function setHoraFin(\DateTimeInterface $horaFin): self
    {
        $this->horaFin = $horaFin;

        return $this;
    }

    public function getDia(): ?string
    {
        return $this->dia;
    }

    public function setDia(string $dia): self
    {
        $this->dia = $dia;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdAgendaUsuario(): ?int
    {
        return $this->idAgendaUsuario;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

    public function __toString()
    {
        if ($this->getDia() == '1') {
            return 'Lunes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '2') {
            return 'Martes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '3') {
            return 'Miercoles ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '4') {
            return 'Jueves ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '5') {
            return 'Viernes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '6') {
            return 'Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '7') {
            return 'Domingo - ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '12') {
            return 'Lunes - Martes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '13') {
            return 'Lunes - Miercoles ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')';
        }else if ($this->getDia() == '14') {
            return 'Lunes - Jueves ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '15') {
            return 'Lunes - Viernes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '16') {
            return 'Lunes - Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '17') {
            return 'Lunes - Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '23') {
           return 'Martes - Miercoles ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')';
        }else if ($this->getDia() == '24') {
            return 'Martes - Jueves ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '25') {
            return 'Martes - Viernes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '26') {
            return 'Martes - Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '27') {
            return 'Martes - Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '34') {
            return 'Miercoles - Jueves ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '35') {
            return 'Miercoles - Viernes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '36') {
            return 'Miercoles - Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '37') {
            return 'Miercoles - Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '45') {
            return 'Jueves - Viernes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '46') {
            return 'Jueves - Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '47') {
            return 'Jueves - Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '56') {
            return 'Viernes -Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '57') {
            return 'Viernes -Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '67') {
            return 'Sabado - Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '123') {
            return 'Lunes a Miercoles ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')';
        }else if ($this->getDia() == '1234') {
          return 'Lunes a Jueves ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')';  
        }else if ($this->getDia() == '12345') {
          return 'Lunes a Viernes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '123456') {
          return 'Lunes a Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '1234567') {
          return 'Lunes a Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '234') {
          return 'Martes a Jueves ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '2345') {
          return 'Martes a Viernes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '23456') {                              
          return 'Martes a Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '234567') { 
          return 'Martes a Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '345') {
          return 'Miercoles a Viernes ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')';
        }else if ($this->getDia() == '3456') {
          return 'Miercoles a Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '34567') {
          return 'Miercoles a Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')';
        }else if ($this->getDia() == '456') {
          return 'Jueves a Sabado ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '4567') {
          return 'Jueves a Domingo ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else if ($this->getDia() == '567') {
          return 'Viernes a Domingo  ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }else {
            return ucwords(strtolower($this->getDia())).' ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')'; 
        }
        //return $this->getDia();//.' - ('.$this->getHoraInicio()->format('H:i').' - '.$this->getHoraFin()->format('H:i').')';        
    } 
}
