<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
/**
 * Paciente
 * @UniqueEntity(fields={"numeroIdentificacion"}, message="Este dato ya se encuentra registrado.")
 */ 
class Paciente
{
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 8,
     *      max = 20,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     *     pattern     = "/^[a-zA-Z0-9]+$/i",
     *     message="Se admiten números y letras."
     * )  
     */
    private $numeroIdentificacion;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $expedicionDocumento;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     *     pattern     = "/^[a-z A-Z]+$/i",
     *     message="Se admiten letras."
     * )    
     */
    private $nombres;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     *     pattern     = "/^[a-z A-Z]+$/i",
     *     message="Se admiten letras."
     * )    
     */
    private $primerApellido;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     *     pattern     = "/^[a-z A-Z]+$/i",
     *     message="Se admiten letras."
     * )    
     */
    private $segundoApellido;

    private $fechaNacimiento;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 0,
     *      max = 750,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     *     pattern     = "/^[a-z A-Z]+$/i",
     *     message="Se admiten letras."
     * )    
     */    
    private $lugarNacimiento;
    /**
     * @var string
     * @Assert\NotBlank
    */
    private $estadoCivil;
    /**
     * @var string
     * @Assert\NotBlank
    */
    private $sexo;
    /**
     * @var string
     * @Assert\Length(
     *      min = 10,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es valido.",
     * )
     */
    private $email;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 6,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $dirrecionDomicilio;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 11,
     *      max = 11,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $telefonoDomicilio;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 10,
     *      max = 15,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Solo admite números"),
     */
    private $celular;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     *     pattern     = "/^[a-z A-Z]+$/i",
     *     message="Se admiten letras."
     * )    
     */
    private $nombrePResponsable;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 10,
     *      max = 15,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Solo admite números"), 
     */
    private $telefonoPersonaResponsable;
    /**
     * @Assert\NotBlank
     */
    private $parentesco;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     *     pattern     = "/^[a-z A-Z]+$/i",
     *     message="Se admiten letras."
     * )    
     */
    private $ocupacion;
    /**
    * @var \App\Entity\TipoUsuario
    */
    private $regimen;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     *     pattern     = "/^[a-z A-Z]+$/i",
     *     message="Se admiten letras."
     * )    
     */
    private $nombreAcompanante;
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 10,
     *      max = 15,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Solo admite números"),
     */
    private $movilAcompanante;
    
    private $fechaApertura;
    /**
     * @var boolean
    **/ 
    private $estado;
    /**
     * @var integer
    */
    private $idPaciente;
    /**
     * @var \App\Entity\Municipio
     * @Assert\NotBlank
     */
    private $idMunicipio;
    /**
     * @var \App\Entity\Eps
     */
    private $idEps;
    /**
     * @var \App\Entity\TipoDocumento
     * @Assert\NotBlank
     */
    private $tipoIdentificacion;    
    /**
    * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    public function __construct()
    {
        $this->fechaApertura = new \DateTime();
    }

    public function getNumeroIdentificacion(): ?string
    {
        return $this->numeroIdentificacion;
    }

    public function setNumeroIdentificacion(string $numeroIdentificacion): self
    {
        $this->numeroIdentificacion = strtoupper($numeroIdentificacion);

        return $this;
    }

    public function getExpedicionDocumento(): ?string
    {
        return $this->expedicionDocumento;
    }

    public function setExpedicionDocumento(string $expedicionDocumento): self
    {
        $this->expedicionDocumento = $expedicionDocumento;

        return $this;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getPrimerApellido(): ?string
    {
        return $this->primerApellido;
    }

    public function setPrimerApellido(string $primerApellido): self
    {
        $this->primerApellido = $primerApellido;

        return $this;
    }

    public function getSegundoApellido(): ?string
    {
        return $this->segundoApellido;
    }

    public function setSegundoApellido(string $segundoApellido): self
    {
        $this->segundoApellido = $segundoApellido;

        return $this;
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento(\DateTimeInterface $fechaNacimiento): self
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    public function getLugarNacimiento(): ?string
    {
        return $this->lugarNacimiento;
    }

    public function setLugarNacimiento(string $lugarNacimiento): self
    {
        $this->lugarNacimiento = $lugarNacimiento;

        return $this;
    }

    public function getEstadoCivil(): ?string
    {
        return $this->estadoCivil;
    }

    public function setEstadoCivil(string $estadoCivil): self
    {
        $this->estadoCivil = $estadoCivil;

        return $this;
    }

    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    public function setSexo(string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDirrecionDomicilio(): ?string
    {
        return $this->dirrecionDomicilio;
    }

    public function setDirrecionDomicilio(string $dirrecionDomicilio): self
    {
        $this->dirrecionDomicilio = $dirrecionDomicilio;

        return $this;
    }

    public function getTelefonoDomicilio(): ?string
    {
        return $this->telefonoDomicilio;
    }

    public function setTelefonoDomicilio(string $telefonoDomicilio): self
    {
        $this->telefonoDomicilio = $telefonoDomicilio;

        return $this;
    }

    public function getCelular(): ?string
    {
        return $this->celular;
    }

    public function setCelular(string $celular): self
    {
        $this->celular = $celular;

        return $this;
    }

    public function getNombrePResponsable(): ?string
    {
        return $this->nombrePResponsable;
    }

    public function setNombrePResponsable(string $nombrePResponsable): self
    {
        $this->nombrePResponsable = $nombrePResponsable;

        return $this;
    }

    public function getTelefonoPersonaResponsable(): ?int
    {
        return $this->telefonoPersonaResponsable;
    }

    public function setTelefonoPersonaResponsable(int $telefonoPersonaResponsable): self
    {
        $this->telefonoPersonaResponsable = $telefonoPersonaResponsable;

        return $this;
    }

    public function getParentesco(): ?string
    {
        return $this->parentesco;
    }

    public function setParentesco(string $parentesco): self
    {
        $this->parentesco = $parentesco;

        return $this;
    }

    public function getOcupacion(): ?string
    {
        return $this->ocupacion;
    }

    public function setOcupacion(string $ocupacion): self
    {
        $this->ocupacion = $ocupacion;

        return $this;
    }
    public function getRegimen(): ?TipoUsuario
    {
        return $this->regimen;
    }

    public function setRegimen(?TipoUsuario $regimen): self
    {
        $this->regimen = $regimen;

        return $this;
    }

    public function getNombreAcompanante(): ?string
    {
        return $this->nombreAcompanante;
    }

    public function setNombreAcompanante(string $nombreAcompanante): self
    {
        $this->nombreAcompanante = $nombreAcompanante;

        return $this;
    }

    public function getMovilAcompanante(): ?int
    {
        return $this->movilAcompanante;
    }

    public function setMovilAcompanante(int $movilAcompanante): self
    {
        $this->movilAcompanante = $movilAcompanante;

        return $this;
    }

    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }
    
    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdPaciente(): ?int
    {
        return $this->idPaciente;
    }

    public function getIdMunicipio(): ?Municipio
    {
        return $this->idMunicipio;
    }

    public function setIdMunicipio(?Municipio $idMunicipio): self
    {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }

    public function getIdEps(): ?Eps
    {
        return $this->idEps;
    }

    public function setIdEps(?Eps $idEps): self
    {
        $this->idEps = $idEps;

        return $this;
    }

    public function getTipoIdentificacion(): ?TipoDocumento
    {
        return $this->tipoIdentificacion;
    }

    public function setTipoIdentificacion(?TipoDocumento $tipoIdentificacion): self
    {
        $this->tipoIdentificacion = $tipoIdentificacion;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }
    public function __toString()
    {
        
          return $this->getTipoIdentificacion().': '.$this->getNumeroIdentificacion().' - '.$this->getNombres().' '.$this->getPrimerApellido().' '.$this->getSegundoApellido();  
        
    } 
}
