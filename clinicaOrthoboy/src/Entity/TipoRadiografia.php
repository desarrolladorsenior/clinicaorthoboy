<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * TipoRadiografia
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 */
class TipoRadiografia
{
     /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $nombre;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $descripcion;
    /**
     * @var boolean
    */
    private $estado;

    /**
     * @var integer
    */
    private $idTipoRadiografia;
    
    /**
     * @var \App\Entity\TipoRadiografia
    */
    private $idSedeTipoRadiografia;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = strtoupper($nombre);

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdTipoRadiografia(): ?int
    {
        return $this->idTipoRadiografia;
    }

    public function getIdSedeTipoRadiografia(): ?self
    {
        return $this->idSedeTipoRadiografia;
    }

    public function setIdSedeTipoRadiografia(?self $idSedeTipoRadiografia): self
    {
        $this->idSedeTipoRadiografia = $idSedeTipoRadiografia;

        return $this;
    }
    public function __toString()
    {
        if ($this->getIdSedeTipoRadiografia() != "") {
          return $this->getNombre().' - '.$this->getIdSedeTipoRadiografia()->getNombre();
        }else{
            return $this->getNombre();
        }
        
    } 
}
