<?php

namespace App\Entity;

class PerfilPermiso
{
    /**
     * @var integer
    */
    private $idPerfilPermiso;

    /**
     * @var \App\Entity\Perfil
    */
    private $idPerfil;

    /**
     * @var \App\Entity\Permiso
    */
    private $idPermiso;

    public function getIdPerfilPermiso(): ?int
    {
        return $this->idPerfilPermiso;
    }

    public function getIdPerfil(): ?Perfil
    {
        return $this->idPerfil;
    }

    public function setIdPerfil(?Perfil $idPerfil): self
    {
        $this->idPerfil = $idPerfil;

        return $this;
    }

    public function getIdPermiso(): ?Permiso
    {
        return $this->idPermiso;
    }

    public function setIdPermiso(?Permiso $idPermiso): self
    {
        $this->idPermiso = $idPermiso;

        return $this;
    }

    public function __toString()
    {   
     return $this->getIdPerfil()->getNombre().' - '.$this->getIdPermiso()->getNombre();
    } 
}
