<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Pedido
 * @UniqueEntity(fields={"numeroFactura"}, message="Este dato ya se encuentra registrado.")
 */
class Pedido
{
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 9,
     *      max = 17,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $numeroFactura;
    /**    
     * @var \DateTime 
     */
    private $fechaApertura;
    /**    
     * @var \DateTime 
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     */
    private $fechaFactura;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idPedido;
    /**
     * @var \App\Entity\Proveedor
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idProveedor;
    /**
     * @var \App\Entity\Usuario
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idUsuarioIngresa;

    /**
     * @var \App\Entity\OrdenCompra
    */
    private $idOrdenCompra;

    /**
     * @var string
     * @Assert\File(
     *maxSize = "25m",
     *mimeTypes={ "application/pdf","application/xml","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.oasis.opendocument.spreadsheet","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/msword","text/plain","application/vnd.oasis.opendocument.text","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/zip","application/x-rar","application/x-tar","image/*","video/mp4","video/mpeg","audio/x-wav","audio/basic","docx":"application/wps-office.docx","pptx":"application/wps-office.pptx","application/wps-office.xlsx"},
     * mimeTypesMessage = "")
     */
    private $imagen;

    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }
    /*
     * Set imagen
     *
     * @param string $imagen
     *
     *  @return Pedido
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }
    /*
     * Get imagen
     *
     * @return string
     */
      public function getImagen()
    {
        return $this->imagen;
    }

    public function getNumeroFactura(): ?string
    {
        return $this->numeroFactura;
    }

    public function setNumeroFactura(string $numeroFactura): self
    {
        $this->numeroFactura = strtoupper($numeroFactura);

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getFechaFactura(): ?\DateTimeInterface
    {
        return $this->fechaFactura;
    }

    public function setFechaFactura(\DateTimeInterface $fechaFactura): self
    {
        $this->fechaFactura = $fechaFactura;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdPedido(): ?int
    {
        return $this->idPedido;
    }

    public function getIdProveedor(): ?Proveedor
    {
        return $this->idProveedor;
    }

    public function setIdProveedor(?Proveedor $idProveedor): self
    {
        $this->idProveedor = $idProveedor;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

     public function getIdOrdenCompra(): ?OrdenCompra
    {
        return $this->idOrdenCompra;
    }

    public function setIdOrdenCompra(?OrdenCompra $idOrdenCompra): self
    {
        $this->idOrdenCompra = $idOrdenCompra;

        return $this;
    }

    /* con este metodo
        Subir un unico archivo*/
    public function uploadUno($archivo){
    // la propiedad file puede quedar vacía
        if(null === $archivo){
            return;
        }

        // se usa el nombre original aquí pero debería sanitizarse
        // el método move recibe el directorio y el archivo
        $fileName=md5(uniqid()).'.'.$archivo->guessExtension();
        $archivo->move('/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/pedido/',$fileName);
        $this->path=$fileName;
        $this->setImagen($this->path);
    }


    public function __toString()
    {
        return 'Pedido N°: '.$this->getIdPedido().' - Factura: '.$this->getNumeroFactura();
    }
}
