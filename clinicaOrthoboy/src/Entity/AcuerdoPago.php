<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * AcuerdoPago
 */     
class AcuerdoPago
{
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )   
     */
    private $tipo;
    /**
    * @var bigint
    * @Assert\Length(
    *      min = 1,
    *      max = 20,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $porcentaje;
    /**
    * @var bigint
    * @Assert\Length(
    *      min = 1,
    *      max = 20,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
    */
    private $valor;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idAcuerdoPago;
    /**
     * @var \App\Entity\Tratamiento
    */
    private $idTratamiento;

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getPorcentaje(): ?int
    {
        return $this->porcentaje;
    }

    public function setPorcentaje(?int $porcentaje): self
    {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    public function getValor(): ?int
    {
        return $this->valor;
    }

    public function setValor(?int $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdAcuerdoPago(): ?int
    {
        return $this->idAcuerdoPago;
    }

    public function getIdTratamiento(): ?Tratamiento
    {
        return $this->idTratamiento;
    }

    public function setIdTratamiento(?Tratamiento $idTratamiento): self
    {
        $this->idTratamiento = $idTratamiento;

        return $this;
    }
    public function __toString()
    {
        if ($this->getPorcentaje() != '') {
            if ($this->getIdTratamiento() != '') {
                return $this->getTipo().' ('.$this->getPorcentaje().'%) - Tratamiento'.$this->getIdTratamiento();
            }else{
                return $this->getTipo().' ('.$this->getPorcentaje().'%)';
            }
        }elseif($this->getValor() != ''){
            if ($this->getIdTratamiento() != '') {
                return $this->getTipo().' ( $'.$this->getValor().') - Tratamiento'.$this->getIdTratamiento();
            }else{
                return $this->getTipo().' ( $'.$this->getValor().')';
            }
        }
       
    } 
}
