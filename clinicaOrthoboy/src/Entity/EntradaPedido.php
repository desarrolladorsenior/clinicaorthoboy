<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * EntradaPedido
 */
class EntradaPedido
{
    /**
     * @var integer
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Se permiten unicamente números"),
    */
    private $cantidad;
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
    */
    private $unidadMedida;
    /**
     * @var dataTime
    */
    private $fechaApertura;
    /**
     * @var dataTime
    */
    private $fechaVencimiento;
    /**
     * @var string
     * @Assert\Length(
     *      min = 9,
     *      max = 50,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[a-zA-Z0-9-]*$/", 
     * message="Se permiten unicamente letras, números, espacios y guiones."),
    */
    private $registroInvima;
    /**
     * @var string
     * @Assert\Length(
     *      min = 4,
     *      max = 20,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[A-Za-z0-9-]*$/", 
     * message="Se permiten unicamente números."),
    */
    private $lote;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
    */
    private $modelo;
    /**
     * @var string
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
    */
    private $intervaloMantenimiento;
    /**
     * @var dataTime
    */
    private $garantia;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idEntradaPedido;
    /**
     * @var \App\Entity\Pedido
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idPedido;
    /**
     * @var \App\Entity\Insumo
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idInsumo;
    /**
     * @var \App\Entity\Clinica
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idClinicaUbicacion;
    /**
     * @var \App\Entity\Usuario
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idUsuarioIngresa;

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getUnidadMedida(): ?string
    {
        return $this->unidadMedida;
    }

    public function setUnidadMedida(string $unidadMedida): self
    {
        $this->unidadMedida = $unidadMedida;

        return $this;
    }
   
    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

 /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getFechaVencimiento(): ?\DateTimeInterface
    {
        return $this->fechaVencimiento;
    }

    public function setFechaVencimiento(?\DateTimeInterface $fechaVencimiento): self
    {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    public function getRegistroInvima(): ?string
    {
        return $this->registroInvima;
    }

    public function setRegistroInvima(?string $registroInvima): self
    {
        $this->registroInvima = $registroInvima;

        return $this;
    }

    public function getLote(): ?string
    {
        return $this->lote;
    }

    public function setLote(?string $lote): self
    {
        $this->lote = $lote;

        return $this;
    }

    public function getModelo(): ?string
    {
        return $this->modelo;
    }

    public function setModelo(?string $modelo): self
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function getIntervaloMantenimiento(): ?string
    {
        return $this->intervaloMantenimiento;
    }

    public function setIntervaloMantenimiento(?string $intervaloMantenimiento): self
    {
        $this->intervaloMantenimiento = $intervaloMantenimiento;

        return $this;
    }

    public function getGarantia(): ?\DateTimeInterface
    {
        return $this->garantia;
    }

    public function setGarantia(?\DateTimeInterface $garantia): self
    {
        $this->garantia = $garantia;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdEntradaPedido(): ?int
    {
        return $this->idEntradaPedido;
    }

    public function getIdPedido(): ?Pedido
    {
        return $this->idPedido;
    }

    public function setIdPedido(?Pedido $idPedido): self
    {
        $this->idPedido = $idPedido;

        return $this;
    }

    public function getIdInsumo(): ?Insumo
    {
        return $this->idInsumo;
    }

    public function setIdInsumo(?Insumo $idInsumo): self
    {
        $this->idInsumo = $idInsumo;

        return $this;
    }

    public function getIdClinicaUbicacion(): ?Clinica
    {
        return $this->idClinicaUbicacion;
    }

    public function setIdClinicaUbicacion(?Clinica $idClinicaUbicacion): self
    {
        $this->idClinicaUbicacion = $idClinicaUbicacion;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

    public function __toString()
    {
         return $this->getIdPedido().' - '.$this->getIdInsumo().' - Sede : '.$this->getIdClinicaUbicacion();
    } 
}
