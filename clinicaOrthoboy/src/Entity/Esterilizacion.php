<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Esterilizacion
 * @UniqueEntity(fields={"loteProveta"}, message="Este dato ya se encuentra registrado.")
*/
class Esterilizacion
{
    /**
     * @var dataTime
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * )
     */
    private $fechaApertura;
    /**
     * @var dataTime
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * )
     */
    private $fechaVecimiento;
    /**
     * @var integer
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 1,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Se permiten unicamente números"),
     */
    private $carga;
     /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 8,
     *      max = 30,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * ) 
     */
    private $loteProveta;
    /**
     * @var integer
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Se permiten unicamente números"),
     */
    private $cantidadPaquete;
    /**
     * @var integer
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Se permiten unicamente números"),
     */
    private $tiempoEsterilizacion;
    /**
     * @var integer
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Se permiten unicamente números"),
     */
    private $tiempoSecado;
    /**
     * @var integer     
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Se permiten unicamente números"),
     */
    private $temperatura;

    /**
     * @var integer     
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "El limite mínimo de dígitos requeridos es {{ limit }}.",
     *      maxMessage = "El limite máximo de dígitos permitidos es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Se permiten unicamente números"),
     */
    private $presion;
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 5,
     *      max = 10,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * ) 
     */
    private $tipoEsterilizacion;
    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var integer
    */
    private $idEsterilizacion;
     /**
     * @var \App\Entity\Usuario
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idUsuarioReporta;

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getFechaVecimiento(): ?\DateTimeInterface
    {
        return $this->fechaVecimiento;
    }

    public function setFechaVecimiento(\DateTimeInterface $fechaVecimiento): self
    {
        $this->fechaVecimiento = $fechaVecimiento;

        return $this;
    }

    public function getCarga(): ?int
    {
        return $this->carga;
    }

    public function setCarga(int $carga): self
    {
        $this->carga = $carga;

        return $this;
    }

    public function getLoteProveta(): ?string
    {
        return $this->loteProveta;
    }

    public function setLoteProveta(string $loteProveta): self
    {
        $this->loteProveta = $loteProveta;

        return $this;
    }

    public function getCantidadPaquete(): ?int
    {
        return $this->cantidadPaquete;
    }

    public function setCantidadPaquete(int $cantidadPaquete): self
    {
        $this->cantidadPaquete = $cantidadPaquete;

        return $this;
    }

    public function getTiempoEsterilizacion(): ?string
    {
        return $this->tiempoEsterilizacion;
    }

    public function setTiempoEsterilizacion(string $tiempoEsterilizacion): self
    {
        $this->tiempoEsterilizacion = $tiempoEsterilizacion;

        return $this;
    }

    public function getTiempoSecado(): ?string
    {
        return $this->tiempoSecado;
    }

    public function setTiempoSecado(string $tiempoSecado): self
    {
        $this->tiempoSecado = $tiempoSecado;

        return $this;
    }

    public function getTemperatura(): ?string
    {
        return $this->temperatura;
    }

    public function setTemperatura(?string $temperatura): self
    {
        $this->temperatura = $temperatura;

        return $this;
    }

    public function getPresion(): ?string
    {
        return $this->presion;
    }

    public function setPresion(?string $presion): self
    {
        $this->presion = $presion;

        return $this;
    }

    public function getTipoEsterilizacion(): ?string
    {
        return $this->tipoEsterilizacion;
    }

    public function setTipoEsterilizacion(string $tipoEsterilizacion): self
    {
        $this->tipoEsterilizacion = $tipoEsterilizacion;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdEsterilizacion(): ?int
    {
        return $this->idEsterilizacion;
    }

    public function getIdUsuarioReporta(): ?Usuario
    {
        return $this->idUsuarioReporta;
    }

    public function setIdUsuarioReporta(?Usuario $idUsuarioReporta): self
    {
        $this->idUsuarioReporta = $idUsuarioReporta;

        return $this;
    }

    public function __toString()
    {
        return 'Esterilización N° '.$this->getIdEsterilizacion().' - N° Probeta'.$this->getLoteProveta();  
    } 
}
