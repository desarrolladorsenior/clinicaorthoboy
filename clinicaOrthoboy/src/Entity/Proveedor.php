<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Proveedor
 * @UniqueEntity(fields={"nombre"}, message="Este dato ya se encuentra registrado.")
 * @UniqueEntity(fields={"identificacion"}, message="Este dato ya se encuentra registrado.")
 */
class Proveedor
{
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 9,
     *      max = 17,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9 -]*$/", 
     * message="Solo admite numeros"), 
    */
    private $identificacion;
    /** @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )    
     */
    private $nombre;
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * )
     * @Assert\Length(
     *      min = 6,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $direccion;
    /**
     * @var integer
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * )
     * @Assert\Length(
     *      min = 6,
     *      max = 20,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     * @Assert\Regex(
     * pattern="/^[0-9]*$/", 
     * message="Solo admite numeros"),
     */
    private $movil;
    /**
     * @var string
     * @Assert\Length(
     *      min = 11,
     *      max = 11,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     *
     * )
     * @Assert\Regex(
     * pattern="/^[0-9 -]*$/", 
     * message="Solo admite numeros"), 
     */
    private $telefono;
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 10,
     *      max = 75,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * ) 
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es valido.",
     * )
     */
    private $email;
    /** 
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * ) 
     */
    private $nombreContacto;
     /** 
     * @var string
     * @Assert\NotBlank(message="Este campo es obligatorio.")
     * @Assert\Length(
     *      min = 3,
     *      max = 150,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * ) 
     */
    private $representanteLegal;
    /**
     * @var integer
    */
    private $idProveedor;
    /**
     * @var \App\Entity\TipoDocumento
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idTipoDocumento;
    /**
     * @var \App\Entity\Municipio
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idMunicipio;
    /**
     * @var boolean
    */
    private $estado;

    
    public function getIdentificacion(): ?string
    {
        return $this->identificacion;
    }

    public function setIdentificacion(string $identificacion): self
    {
        $this->identificacion =  strtoupper($identificacion);

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre =  strtoupper($nombre);

        return $this;
    }

    public function getRepresentanteLegal(): ?string
    {
        return $this->representanteLegal;
    }

    public function setRepresentanteLegal(string $representanteLegal): self
    {
        $this->representanteLegal = $representanteLegal;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getMovil(): ?int
    {
        return $this->movil;
    }

    public function setMovil(int $movil): self
    {
        $this->movil = $movil;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNombreContacto(): ?string
    {
        return $this->nombreContacto;
    }

    public function setNombreContacto(?string $nombreContacto): self
    {
        $this->nombreContacto = $nombreContacto;

        return $this;
    }

    public function getIdProveedor(): ?int
    {
        return $this->idProveedor;
    }

    public function getIdTipoDocumento(): ?TipoDocumento
    {
        return $this->idTipoDocumento;
    }

    public function setIdTipoDocumento(?TipoDocumento $idTipoDocumento): self
    {
        $this->idTipoDocumento = $idTipoDocumento;

        return $this;
    }

    public function getIdMunicipio(): ?Municipio
    {
        return $this->idMunicipio;
    }

    public function setIdMunicipio(?Municipio $idMunicipio): self
    {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }
    public function __toString()
    {
        return $this->getNombre();
    }
}
