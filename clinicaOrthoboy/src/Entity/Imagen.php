<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Imagen
 */     
class Imagen
{
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\File(
     * maxSize = "1m",
     * mimeTypes={ "image/png","image/jpg","image/jpeg","image/vmp","image/tif","image/tiff"},
     * mimeTypesMessage = "")
     */
    private $nombre;
    /**
     * @var string
     * @Assert\NotBlank(
     *     message="Este campo es obligatorio."
     * ) 
     * @Assert\Length(
     *      min = 3,
     *      max = 2000,
     *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
     *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
     * )
     */
    private $observacion;

    private $fechaApertura;
    /**
     * @var integer
    */
    private $idImagen;
    /**
     * @var \App\Entity\Paciente
    */
    private $idPaciente;
    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    /* Añadida */
    public function setCreatedAtValue()
    {
        $this->fechaApertura = new \DateTime();
    }
    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getIdImagen(): ?int
    {
        return $this->idImagen;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

    /* con este metodo subir un unico archivo*/
    public function uploadUno($archivo){
    // la propiedad file puede quedar vacía
        if(null === $archivo){
            return;
        }
        // se usa el nombre original aquí pero debería sanitizarse
        // el método move recibe el directorio y el archivo
        $fileName=md5(uniqid()).'.'.$archivo->guessExtension();
        $archivo->move('/var/www/proyectoOdontologia/repositorio-clinica-odontologica/clinicaOrthoboy/public/light/dist/assets/uploads/imagen/',$fileName);
        $this->path=$fileName;
        $this->setNombre($this->path);
    }
    public function __toString()
    {
        return $this->getNombre();  
    } 
}
