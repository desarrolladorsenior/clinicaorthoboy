<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * CupDiagnostico
 */
class CupDiagnostico
{
    /**
    * @var integer
    */
    private $idCupDiagnostico;
    /**
     * @var \App\Entity\Cups
    */
    private $idCups;
    /**
     * @var \App\Entity\CieRip
    */
    private $idRips;
    /**
     * @var \App\Entity\CieRip
    */
    private $idDxr1;
    /**
     * @var \App\Entity\CieRip
    */
    private $idDxr2;
    /**
     * @var \App\Entity\CieRip
    */
    private $idDxr3;
    /**
     * @var \App\Entity\CausaExterna
    */
    private $idCausaExterna;
    /**
     * @var \App\Entity\AmbitoRealizacion
    */
    private $idAmbitoRealizacion;
    /**
     * @var \App\Entity\FormaRealizacion
    */
    private $idFormaRealizacion;
    /**
     * @var \App\Entity\PersonaAtiende
    */
    private $idPersonaAtiende;
    /**
     * @var \App\Entity\FinalidadProcedimiento
    */
    private $idFinalidadProcedimiento;
    /**
     * @var \App\Entity\Usuario
    */
    private $idUsuarioIngresa;

    public function getIdCupDiagnostico(): ?int
    {
        return $this->idCupDiagnostico;
    }

    public function getIdCups(): ?Cups
    {
        return $this->idCups;
    }

    public function setIdCups(?Cups $idCups): self
    {
        $this->idCups = $idCups;

        return $this;
    }

    public function getIdRips(): ?CieRip
    {
        return $this->idRips;
    }

    public function setIdRips(?CieRip $idRips): self
    {
        $this->idRips = $idRips;

        return $this;
    }

    public function getIdDxr1(): ?CieRip
    {
        return $this->idDxr1;
    }

    public function setIdDxr1(?CieRip $idDxr1): self
    {
        $this->idDxr1 = $idDxr1;

        return $this;
    }

    public function getIdDxr2(): ?CieRip
    {
        return $this->idDxr2;
    }

    public function setIdDxr2(?CieRip $idDxr2): self
    {
        $this->idDxr2 = $idDxr2;

        return $this;
    }

    public function getIdDxr3(): ?CieRip
    {
        return $this->idDxr3;
    }

    public function setIdDxr3(?CieRip $idDxr3): self
    {
        $this->idDxr3 = $idDxr3;

        return $this;
    }

    public function getIdCausaExterna(): ?CausaExterna
    {
        return $this->idCausaExterna;
    }

    public function setIdCausaExterna(?CausaExterna $idCausaExterna): self
    {
        $this->idCausaExterna = $idCausaExterna;

        return $this;
    }

    public function getIdAmbitoRealizacion(): ?AmbitoRealizacion
    {
        return $this->idAmbitoRealizacion;
    }

    public function setIdAmbitoRealizacion(?AmbitoRealizacion $idAmbitoRealizacion): self
    {
        $this->idAmbitoRealizacion = $idAmbitoRealizacion;

        return $this;
    }

    public function getIdFormaRealizacion(): ?FormaRealizacion
    {
        return $this->idFormaRealizacion;
    }

    public function setIdFormaRealizacion(?FormaRealizacion $idFormaRealizacion): self
    {
        $this->idFormaRealizacion = $idFormaRealizacion;

        return $this;
    }

    public function getIdPersonaAtiende(): ?PersonaAtiende
    {
        return $this->idPersonaAtiende;
    }

    public function setIdPersonaAtiende(?PersonaAtiende $idPersonaAtiende): self
    {
        $this->idPersonaAtiende = $idPersonaAtiende;

        return $this;
    }

    public function getIdFinalidadProcedimiento(): ?FinalidadProcedimiento
    {
        return $this->idFinalidadProcedimiento;
    }

    public function setIdFinalidadProcedimiento(?FinalidadProcedimiento $idFinalidadProcedimiento): self
    {
        $this->idFinalidadProcedimiento = $idFinalidadProcedimiento;

        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }

     public function __toString()
    {
        return 'Cup N° '.$this->getIdCups().' - Rip principal'.$this->getIdRips();
    } 
}
