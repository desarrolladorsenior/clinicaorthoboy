<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
/**
 * CieDiagnosticoTipo
 */
class CieDiagnosticoTipo
{
    /**
    * @var string
    * @Assert\Length(
    *      min = 3,
    *      max = 250,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $simbolo;
    /**
    * @var string
    * @Assert\Length(
    *      min = 3,
    *      max = 50,
    *      minMessage = "El limite mínimo de caracteres requerido es {{ limit }}.",
    *      maxMessage = "El limite máximo de caracteres permitido es {{ limit }}."
    * )
    */
    private $color;

    /**
     * @var boolean
    */
    private $estado;
    /**
     * @var interger
    */
    private $idCieDiagnosticoTipo;
    /**
     * @var \App\Entity\CieRip
    */
    private $idCieRip;
    /**
     * @var \App\Entity\DiagnosticoOdontologico
    */
    private $idDiagnosticoOdontologico;
    /**
     * @var \App\Entity\TipoDiagnostico
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idTipoDiagnostico;
    /**
     * @var \App\Entity\Usuario
     * @Assert\NotBlank(message="Este campo es obligatorio.")
    */
    private $idUsuarioIngresa;

    public function getSimbolo(): ?string
    {
        return $this->simbolo;
    }

    public function setSimbolo(?string $simbolo): self
    {
        $this->simbolo = $simbolo;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getIdCieDiagnosticoTipo(): ?int
    {
        return $this->idCieDiagnosticoTipo;
    }

    public function getIdCieRip(): ?CieRip
    {
        return $this->idCieRip;
    }

    public function setIdCieRip(?CieRip $idCieRip): self
    {
        $this->idCieRip = $idCieRip;

        return $this;
    }

    public function getIdDiagnosticoOdontologico(): ?DiagnosticoOdontologico
    {
        return $this->idDiagnosticoOdontologico;
    }

    public function setIdDiagnosticoOdontologico(?DiagnosticoOdontologico $idDiagnosticoOdontologico): self
    {
        $this->idDiagnosticoOdontologico = $idDiagnosticoOdontologico;

        return $this;
    }

    public function getIdTipoDiagnostico(): ?TipoDiagnostico
    {
        return $this->idTipoDiagnostico;
    }

    public function setIdTipoDiagnostico(?TipoDiagnostico $idTipoDiagnostico): self
    {
        $this->idTipoDiagnostico = $idTipoDiagnostico;
        return $this;
    }

    public function getIdUsuarioIngresa(): ?Usuario
    {
        return $this->idUsuarioIngresa;
    }

    public function setIdUsuarioIngresa(?Usuario $idUsuarioIngresa): self
    {
        $this->idUsuarioIngresa = $idUsuarioIngresa;

        return $this;
    }
     public function __toString()
    {
        if ($this->getIdTipoDiagnostico()->getIdTipoDiagnostico() == 2) {
            return $this->getIdCieDiagnosticoTipo().' - '.$this->getIdDiagnosticoOdontologico();  
        }else if ($this->getIdTipoDiagnostico()->getIdTipoDiagnostico() == 1) {
           return $this->getIdCieDiagnosticoTipo().' - '.$this->getIdCieRip();  
        } else{
            return $this->getIdCieDiagnosticoTipo().' - odontologico '.$this->getIdDiagnosticoOdontologico().' - rip '.$this->getIdCieRip();  
        }
    } 
}
