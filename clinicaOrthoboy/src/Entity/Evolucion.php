<?php

namespace App\Entity;

class Evolucion
{
    private $nombreAcompanante;

    private $movilAcompanante;

    private $firmaPaciente;

    private $evolucion;

    private $fechaApertura;

    private $idEvolucion;

    private $idPaciente;

    private $idPlanTratamiento;

    private $idUsuarioProfesional;

    public function getNombreAcompanante(): ?string
    {
        return $this->nombreAcompanante;
    }

    public function setNombreAcompanante(?string $nombreAcompanante): self
    {
        $this->nombreAcompanante = $nombreAcompanante;

        return $this;
    }

    public function getMovilAcompanante(): ?string
    {
        return $this->movilAcompanante;
    }

    public function setMovilAcompanante(?string $movilAcompanante): self
    {
        $this->movilAcompanante = $movilAcompanante;

        return $this;
    }

    public function getFirmaPaciente(): ?string
    {
        return $this->firmaPaciente;
    }

    public function setFirmaPaciente(string $firmaPaciente): self
    {
        $this->firmaPaciente = $firmaPaciente;

        return $this;
    }

    public function getEvolucion(): ?string
    {
        return $this->evolucion;
    }

    public function setEvolucion(?string $evolucion): self
    {
        $this->evolucion = $evolucion;

        return $this;
    }

    public function getFechaApertura(): ?\DateTimeInterface
    {
        return $this->fechaApertura;
    }

    public function setFechaApertura(\DateTimeInterface $fechaApertura): self
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    public function getIdEvolucion(): ?int
    {
        return $this->idEvolucion;
    }

    public function getIdPaciente(): ?Paciente
    {
        return $this->idPaciente;
    }

    public function setIdPaciente(?Paciente $idPaciente): self
    {
        $this->idPaciente = $idPaciente;

        return $this;
    }

    public function getIdPlanTratamiento(): ?PlanTratamiento
    {
        return $this->idPlanTratamiento;
    }

    public function setIdPlanTratamiento(?PlanTratamiento $idPlanTratamiento): self
    {
        $this->idPlanTratamiento = $idPlanTratamiento;

        return $this;
    }

    public function getIdUsuarioProfesional(): ?Usuario
    {
        return $this->idUsuarioProfesional;
    }

    public function setIdUsuarioProfesional(?Usuario $idUsuarioProfesional): self
    {
        $this->idUsuarioProfesional = $idUsuarioProfesional;

        return $this;
    }
}
