<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PanelPrincipalController extends AbstractController
{
    /**
     * @Route("/panelPrincipal", name="panelPrincipal")
     */
    public function index()
    {
        return $this->render('panelPrincipal/index.html.twig');
    }
}
